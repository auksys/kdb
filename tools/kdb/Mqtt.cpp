#include <clog_print>
#include <clog_qt>

#include <QCoreApplication>
#include <QDir>

#include <knowCore/Uri.h>

#include <kDB/Repository/Connection.h>

#include <kDBMQTT/EndPoint.h>

#include "AbstractCommand.h"
#include "ConnectionOptions.h"

class MQTTCommand : public AbstractCommand
{
public:
  MQTTCommand()
      : prefixOption("prefix", "The prefix for the mqtt topic", "prefix"),
        addressOption("address", "The address for the mqtt broker", "address")
  {
  }
  void setup(QCommandLineParser* _parser) override
  {
    connectionOptions.setup(_parser);
    _parser->addOption(prefixOption);
    _parser->addOption(addressOption);
  }
  int execute(QCommandLineParser* _parser) override
  {
    QString mqtt_prefix = _parser->isSet(prefixOption) ? _parser->value(prefixOption) : "kdb";
    QString mqtt_broker
      = _parser->isSet(addressOption) ? _parser->value(addressOption) : "mqtt://localhost:1883";

    kDB::Repository::Connection connection = connectionOptions.createConnection(_parser);
    cres_qresult<void> connect_rv = connection.connect();
    if(not connect_rv.is_successful())
    {
      clog_print<clog_print_flag::red>("Failed to connect with error '{}'.",
                                       connect_rv.get_error());
      return -1;
    }

    kDBMQTT::EndPoint ep(connection, mqtt_prefix, mqtt_broker);
    cres_qresult<void> ep_start_rv = ep.start();
    if(not ep_start_rv.is_successful())
    {
      clog_print<clog_print_flag::red>("Failed to start MQTT End Point, with error: {}",
                                       ep_start_rv.get_error());
      return -1;
    }

    return QCoreApplication::instance()->exec();
  }
private:
  ConnectionOptions connectionOptions = {false};
  QCommandLineOption prefixOption, addressOption;
};

REGISTER_COMMAND(MQTTCommand, web_server)
