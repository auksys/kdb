#include <cres_qt>

#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QDir>

#include <kDB/Repository/Store.h>
#include <kDB/Repository/TemporaryStore.h>

namespace
{
  struct ConnectionOptions
  {
    ConnectionOptions(bool has_store_options)
        : hasStoreOptions(has_store_options),
          temporaryStoreOption("temporary-store",
                               "If set, this program creates a temporary store and use that as a "
                               "end-point. Useful for tests and benchmarks."),
          sectionOption("section", "The section for use as path for the store.", "store"),
          nameOption("name", "The name for the store.", "store"),
          storePathOption("path", "The path for the store.", "store"),
          storePortOption("port", "The port for the store, default to 1242.", "port")
    {
    }

    void setup(QCommandLineParser* _parser)
    {
      if(hasStoreOptions)
      {
        _parser->addOption(temporaryStoreOption);
      }
      _parser->addOption(sectionOption);
      _parser->addOption(nameOption);
      _parser->addOption(storePathOption);
      _parser->addOption(storePortOption);
    }

    cres_qresult<std::tuple<kDB::Repository::TemporaryStore*, kDB::Repository::Store*>>
      startStore(QCommandLineParser* _parser)
    {
      bool temporaryStore = _parser->isSet(temporaryStoreOption);
      int kDBPort
        = _parser->isSet(storePortOption) ? _parser->value(storePortOption).toInt() : 1242;

      if(temporaryStore)
      {
        kDB::Repository::TemporaryStore* ts = new kDB::Repository::TemporaryStore();
        cres_try(cres_ignore, ts->start(), on_failure(delete ts;));
        return cres_success(std::make_tuple(ts, ts->store()));
      }
      else
      {
        kDB::Repository::Store* store;
        if(_parser->isSet(storePathOption))
        {
          store = new kDB::Repository::Store(QDir(_parser->value(storePathOption)), kDBPort);
        }
        else
        {
          store = new kDB::Repository::Store(
            kDB::Repository::SectionName(_parser->value(sectionOption)),
            kDB::Repository::StoreName(_parser->value(nameOption)), kDBPort);
        }
        cres_try(cres_ignore, store->start(), on_failure(delete store;));
        return cres_success(std::make_tuple(nullptr, store));
      }
    }

    kDB::Repository::Connection createConnection(QCommandLineParser* _parser)
    {
      QString kDBHostName = _parser->value(storePathOption);
      int kDBPort
        = _parser->isSet(storePortOption) ? _parser->value(storePortOption).toInt() : 1242;
      kDB::Repository::Connection connection;

      if(_parser->isSet(storePathOption))
      {
        connection = kDB::Repository::Connection::create(
          kDB::Repository::HostName(_parser->value(storePathOption)), kDBPort);
      }
      else
      {
        connection = kDB::Repository::Connection::create(
          kDB::Repository::SectionName(_parser->value(sectionOption)),
          kDB::Repository::StoreName(_parser->value(nameOption)), kDBPort);
      }
      return connection;
    }

    bool hasStoreOptions;
    QCommandLineOption temporaryStoreOption, sectionOption, nameOption, storePathOption,
      storePortOption;
  };
} // namespace