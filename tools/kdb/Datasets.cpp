#include <clog_print>

#include <QCborArray>
#include <QCborMap>
#include <QCborStreamReader>
#include <QCommandLineParser>
#include <QFile>

#include <knowCore/Uris/askcore_graph.h>
#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/Connection.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>
#include <kDBDatasets/Statistics.h>

#include "AbstractCommand.h"
#include "ConnectionOptions.h"

void print_dataset_info(const kDB::Repository::Connection& _connection,
                        const kDBDatasets::Dataset& _ds)
{
  std::string stats_str;
  cres_qresult<kDBDatasets::Statistics> stats
    = kDBDatasets::DataInterfaceRegistry::statistics(_connection, _ds);
  if(stats.is_successful())
  {
    stats_str = std::format("{} data points", stats.get_value().datapointsCount());
  }
  else
  {
    stats_str = "dataset is not local.";
  }
  clog_print(R"(uri: {}
  - type : {}
  - geometry : {}
  - agents: {}
  - status: {}
  - statitics: {}
)",
             _ds.uri(), _ds.type(), _ds.geometry(), _ds.associatedAgents().expect_success(),
             _ds.status().expect_success(), stats_str);
}

class DatasetsCommand : public AbstractCommand
{
public:
  DatasetsCommand()
      : filenameOption("filename", "The filename for import/export/fileinfo.", "filename")
  {
  }
  void setup(QCommandLineParser* _parser)
  {
    _parser->addOption(filenameOption);
    _parser->addPositionalArgument(
      "command", "command among 'enable', 'export', 'fileinfo', 'import', 'list', 'show'.");
    _parser->addPositionalArgument("[dataset]", "URI for a dataset for 'export', 'show' commands.");
    _parser->addPositionalArgument(
      "[list of datasets]", "URIs for a list of set of datasets for 'list', 'import' commands.");
  }
  int execute(QCommandLineParser* _parser) override
  {
    // Commands
    QStringList positionalArguments = _parser->positionalArguments();
    if(positionalArguments.size() != 1)
    {
      clog_print<clog_print_flag::stderr | clog_print_flag::red>(
        "Expected one positional argument with the command name.");
      return -1;
    }
    QString command = positionalArguments.takeFirst();
    if(command == "fileinfo")
    {
      QString fn = _parser->value(filenameOption);
      QFile file(fn);
      if(file.open(QIODevice::ReadOnly))
      {

        QCborStreamReader reader(&file);
        if(not reader.isMap() or not reader.enterContainer()
           or reader.readString().data != "metadata" or not reader.next() or not reader.isMap()
           or not reader.enterContainer())
        {
          clog_print<clog_print_flag::stderr | clog_print_flag::red>("Invalid file.");
          return -1;
        }

        // Read metadata
        while(reader.hasNext())
        {
          QString key = reader.readString().data;
          reader.next();
          if(key == "uri")
          {
            clog_print("Uri: {}", reader.readString().data);
            reader.next();
          }
          else if(key == "kdb_version")
          {
            clog_print("kDB Version: {}", reader.toInteger());
            reader.next();
          }
          else if(key == "definition")
          {
            QByteArray definition = reader.readByteArray().data;
            QCborValue value = QCborValue::fromCbor(definition);
            QCborMap map = value.toMap();
            clog_print("Type: {}", value.toMap()["type_uri"_kCs].toString());
            clog_print("Properties:");
            QCborArray properties_array = value.toMap()["properties"_kCs].toArray();
            for(qsizetype i = 0; i < properties_array.size(); ++i)
            {
              QCborMap pm = properties_array[i].toMap();
              knowCore::Value value
                = knowCore::Value::fromCborMap(pm["value"_kCs].toMap()).expect_success();
              clog_print(" - {}: {}", pm["path"_kCs].toString(), value);
            }
            reader.next();
          }
          else
          {
            clog_print<clog_print_flag::stderr | clog_print_flag::red>("Unknown key: {}.", key);
          }
        }
        return 0;
      }
      else
      {
        clog_print<clog_print_flag::stderr | clog_print_flag::red>(
          "Cannot open file {} for reading.", fn);
        return -1;
      }
    }

    // other commands need a connection to a database

    // Connect to database
    kDB::Repository::Connection connection = connectionOptions.createConnection(_parser);
    cres_qresult<void> connect_rv = connection.connect();
    if(not connect_rv.is_successful())
    {
      clog_print<clog_print_flag::red>("Failed to connect with error '{}'.",
                                       connect_rv.get_error());
      return -1;
    }

    if(command == "enable")
    {
      connection.enableExtension("kDBDatasets");
    }
    else if(command == "import")
    {
      if(_parser->isSet(filenameOption))
      {
        QString fn = _parser->value(filenameOption);
        QFile file(fn);
        if(file.open(QIODevice::ReadOnly))
        {
          cres_qresult<kDBDatasets::Collection> dss_rv
            = kDBDatasets::Collection::get(connection, positionalArguments.first());
          if(not dss_rv.is_successful())
          {
            clog_print("There are no set of datasets with uri: {}", positionalArguments.first());
            return -1;
          }
          cres_qresult<void> rv
            = kDBDatasets::DataInterfaceRegistry::importFrom(connection, dss_rv.get_value(), &file);
          if(not rv.is_successful())
          {
            clog_print<clog_print_flag::stderr | clog_print_flag::red>(
              "Failed to import from file {} with error {}.", fn, rv.get_error());
            return -1;
          }
          return 0;
        }
        else
        {
          clog_print<clog_print_flag::stderr | clog_print_flag::red>(
            "Cannot open file {} for reading.", fn);
          return -1;
        }
      }
      else
      {
        clog_print<clog_print_flag::stderr | clog_print_flag::red>("Missing filename argument.");
        return -1;
      }
    }
    else if(command == "export")
    {
      if(_parser->isSet(filenameOption))
      {
        QString fn = _parser->value(filenameOption);
        QFile file(fn);
        if(file.open(QIODevice::WriteOnly))
        {
          QList<kDBDatasets::Collection> dsss;
          if(positionalArguments.size() != 1)
          {
            clog_print<clog_print_flag::stderr | clog_print_flag::red>(
              "Expected a single uri for a dataset, but got {}.", positionalArguments.size());
            return -1;
          }
          cres_qresult<kDBDatasets::Dataset> ds_rv
            = kDBDatasets::Collection::allDatasets(connection).dataset(positionalArguments.first());
          if(ds_rv.is_successful())
          {
            kDBDatasets::Dataset ds = ds_rv.get_value();
            cres_qresult<void> rv
              = kDBDatasets::DataInterfaceRegistry::exportTo(connection, ds, &file);
            if(not rv.is_successful())
            {
              clog_print<clog_print_flag::stderr | clog_print_flag::red>(
                "Failed to export to file {} with error {}.", fn, rv.get_error());
              return -1;
            }
            return 0;
          }
          else
          {
            clog_print("There is no dataset with uri: {}", positionalArguments.first());
            return -1;
          }
        }
        else
        {
          clog_print<clog_print_flag::stderr | clog_print_flag::red>(
            "Cannot open file {} for writting.", fn);
          return -1;
        }
      }
      else
      {
        clog_print<clog_print_flag::stderr | clog_print_flag::red>("Missing filename argument.");
        return -1;
      }
    }
    else if(command == "show")
    {
      if(positionalArguments.size() < 1)
      {
        clog_error("Expected at least one dataset uri for statistics.");
        return -1;
      }
      kDBDatasets::Collection dss = kDBDatasets::Collection::allDatasets(connection);
      for(const QString& ds_uri : positionalArguments)
      {
        cres_qresult<kDBDatasets::Dataset> ds_rv = dss.dataset(ds_uri);
        if(ds_rv.is_successful())
        {
          kDBDatasets::Dataset ds = ds_rv.get_value();
          print_dataset_info(connection, ds);
        }
        else
        {
          clog_print<clog_print_flag::stderr | clog_print_flag::red>(
            "There is no dataset with uri: {}", ds_uri);
          return -1;
        }
      }
    }
    else if(command == "list")
    {
      knowCore::UriList datasets_uris = positionalArguments;
      if(datasets_uris.isEmpty())
      {
        datasets_uris.append(knowCore::Uris::askcore_graph::all_focus_nodes);
      }
      for(const knowCore::Uri& datasets_uri : datasets_uris)
      {
        cres_qresult<kDBDatasets::Collection> dss_rv
          = kDBDatasets::Collection::get(connection, datasets_uri);
        if(dss_rv.is_successful())
        {
          kDBDatasets::Collection dss = dss_rv.get_value();
          cres_qresult<QList<kDBDatasets::Dataset>> all_focus_nodes_rv = dss.all();
          if(all_focus_nodes_rv.is_successful())
          {
            QList<kDBDatasets::Dataset> all_focus_nodes = all_focus_nodes_rv.get_value();
            clog_print("'{}' has {} datasets:", datasets_uris, all_focus_nodes.size());
            for(const kDBDatasets::Dataset& ds : all_focus_nodes)
            {
              print_dataset_info(connection, ds);
            }
          }
          else
          {
            clog_error("Failed to retrieve all datasets with error: {}",
                       all_focus_nodes_rv.get_error());
            return -1;
          }
        }
        else
        {
          clog_print<clog_print_flag::stderr | clog_print_flag::red>(
            "There are no set of datasets with uri: {}", datasets_uri);
          return -1;
        }
      }
    }
    else
    {
      clog_error("Unknown command: {}", command);
      return -1;
    }

    return -1;
  }
private:
  ConnectionOptions connectionOptions = {false};
  QCommandLineOption filenameOption;
};

REGISTER_COMMAND(DatasetsCommand, datasets)
