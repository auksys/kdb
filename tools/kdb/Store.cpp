#include <signal.h>

#include <clog_print>
#include <clog_qt>
#include <cres_qt>

#include <QCoreApplication>

#include <knowCore/Uri.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/Store.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Version.h>

#include "config.h"

#ifdef BUILD_KDB_MQTT
#include <kDBMQTT/EndPoint.h>
#endif
#ifdef BUILD_KDB_WEB_SERVER
#include <kDBWebServer/EndPoint.h>
#endif

#include "AbstractCommand.h"
#include "ConnectionOptions.h"

// TODO use a portable signal handler, maybe from
// https://stackoverflow.com/questions/7581343/how-to-catch-ctrlc-on-windows-and-linux-with-qt

void my_handler(int)
{
  clog_info("Termination requested...");
  QCoreApplication::quit();
}

class StoreCommand : public AbstractCommand
{
public:
  StoreCommand()
      : extensionOption("extension", "the name of an extension to load", "extension")
#ifdef BUILD_KDB_MQTT
        ,
        mqttEndPointOption("mqtt", "enable the mqtt end point"),
        mqttPrefixOption("mqtt-prefix", "The prefix for the mqtt topic", "mqtt-prefix"),
        mqttAddressOption("mqtt-address", "The address for the mqtt broker", "mqtt-address")
#endif
#ifdef BUILD_KDB_WEB_SERVER
        ,
        webEndPointOption("web", "enable the web server end point"),
        webEndPointPort("web-port", "The port for the web server, default to 8888", "web-port")
#endif
  {
  }
  void setup(QCommandLineParser* _parser)
  {
    connectionOptions.setup(_parser);
    _parser->addOption(extensionOption);
#ifdef BUILD_KDB_MQTT
    _parser->addOption(mqttEndPointOption);
#endif
#ifdef BUILD_KDB_WEB_SERVER
    _parser->addOption(webEndPointOption);
    _parser->addOption(webEndPointPort);
#endif
  }
  int execute(QCommandLineParser* _parser) override
  {
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    cres_qresult<std::tuple<kDB::Repository::TemporaryStore*, kDB::Repository::Store*>>
      created_store = connectionOptions.startStore(_parser);

    if(not created_store.is_successful())
    {
      clog_error("Failed to start store with error '{}'.", created_store.get_error());
      return -1;
    }

    kDB::Repository::Store* store = std::get<1>(created_store.get_value());
    if(_parser->isSet(extensionOption))
    {
      kDB::Repository::Connection c = store->createConnection();
      c.connect();
      for(const QString& ex : _parser->values(extensionOption))
      {
        cres_qresult<void> rv = c.enableExtension(ex);
        if(not rv.is_successful())
        {
          clog_error("Failed to enable extension with error: {}", rv.get_error());
          return -1;
        }
      }
    }

    clog_info("Store is running at '{}:{}' use CTR+C to terminate.",
              QString(store->directory().absolutePath()), store->port());
#ifdef BUILD_KDB_MQTT
    QSharedPointer<kDBMQTT::EndPoint> mqtt_end_point;
    if(_parser->isSet(mqttEndPointOption))
    {
      QString mqtt_prefix
        = _parser->isSet(mqttPrefixOption) ? _parser->value(mqttPrefixOption) : "kdb";
      QString mqtt_broker = _parser->isSet(mqttAddressOption) ? _parser->value(mqttAddressOption)
                                                              : "mqtt://localhost:1883";
      kDB::Repository::Connection c = store->createConnection();
      c.connect();
      mqtt_end_point = QSharedPointer<kDBMQTT::EndPoint>::create(c, mqtt_prefix, mqtt_broker);
      cres_qresult<void> rv = mqtt_end_point->start();
      if(not rv.is_successful())
      {
        clog_print<clog_print_flag::red>("Failed to start MQTT End Point, with error: {}",
                                         rv.get_error());
        return -1;
      }
    }
#endif
#ifdef BUILD_KDB_WEB_SERVER
    QSharedPointer<kDBWebServer::EndPoint> web_end_point;
    if(_parser->isSet(webEndPointOption))
    {
      int webPort
        = _parser->isSet(webEndPointPort) ? _parser->value(webEndPointPort).toInt() : 8888;
      web_end_point = QSharedPointer<kDBWebServer::EndPoint>::create(
        store->directory().absolutePath(), store->port());
      web_end_point->setPort(webPort);
      if(not web_end_point->start())
      {
        clog_print<clog_print_flag::red>("Failed to start WebServer End Point.");
        return -1;
      }
    }
#endif

    int a = QCoreApplication::instance()->exec();
    store->stop();
    delete store;
    delete std::get<0>(created_store.get_value());
    return a;
  }
private:
  ConnectionOptions connectionOptions = {true};
  QCommandLineOption extensionOption;
#ifdef BUILD_KDB_MQTT
  QCommandLineOption mqttEndPointOption, mqttPrefixOption, mqttAddressOption;
#endif
#ifdef BUILD_KDB_WEB_SERVER
  QCommandLineOption webEndPointOption, webEndPointPort;
#endif
};

REGISTER_COMMAND(StoreCommand, store)
