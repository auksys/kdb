#include <iostream>

#include <QCommandLineParser>
#include <QCoreApplication>

#include "AbstractCommand.h"

#include <kDB/Version.h>

AbstractCommand::~AbstractCommand() {}

QMap<QString, AbstractCommand*> AbstractCommand::commands;

int main(int _argc, char** _argv)
{
  if(_argc < 2)
  {
    std::cerr << "Missing command, use 'kdb help' for more information\n";
    return -1;
  }

  QString command_name = QString::fromLocal8Bit(_argv[1]);

  if(not AbstractCommand::commands.contains(command_name))
  {
    std::cerr << "Unknown command, use 'kdb help' for the list of commands\n";
    return -1;
  }

  std::string command_executable = std::string("kdb ") + _argv[1];
  _argv[1] = const_cast<char*>(command_executable.c_str());

  int argc = _argc - 1;
  QCoreApplication app(argc, _argv + 1);
  QCoreApplication::setApplicationName("kdb");
  QCoreApplication::setApplicationVersion(KDB_VERSION_STRING);

  QCommandLineParser cmd_parser;
  cmd_parser.addHelpOption();
  cmd_parser.addVersionOption();

  AbstractCommand* command = AbstractCommand::commands[command_name];

  command->setup(&cmd_parser);

  cmd_parser.process(app);

  return command->execute(&cmd_parser);
}
