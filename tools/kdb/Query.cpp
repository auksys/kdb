#include <iostream>

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDir>

#include <clog_print>
#include <clog_qt>
#include <cres_qt>
#include <knowCore/Uri.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/RDFDataset.h>
#include <kDB/Version.h>

#include "AbstractCommand.h"
#include "ConnectionOptions.h"

#define CHECK_RESULT(_expr_, _str_)                                                                \
  {                                                                                                \
    auto rv = _expr_;                                                                              \
    if(not rv.is_successful())                                                                     \
    {                                                                                              \
      clog_error(_str_, rv.get_error());                                                           \
      return -1;                                                                                   \
    }                                                                                              \
  }

class QueryCommand : public AbstractCommand
{
public:
  QueryCommand()
      : sqlOption("sql", "Execute an SQL query."),
        sparqlOption("sparql", "Execute an SPARQL query."),
        outputOption("output", "Output file.", "output"),
        namedDatasetsOption("named-dataset", "Named RDF Datasets used for SPARQL Query.",
                            "named-datasets"),
        defaultDatasetsOption("default-dataset", "Default RDF Dataset used for SPARQL Query.",
                              "default-dataset")
  {
  }
  void setup(QCommandLineParser* _parser) override
  {
    connectionOptions.setup(_parser);
    _parser->addOption(sqlOption);
    _parser->addOption(sparqlOption);
    _parser->addOption(outputOption);
    _parser->addOption(namedDatasetsOption);
    _parser->addOption(defaultDatasetsOption);
    _parser->addPositionalArgument("query", "text of query.");
  }
  int execute(QCommandLineParser* _parser) override
  {
    bool sqlQ = _parser->isSet(sqlOption);
    bool sparqlQ = _parser->isSet(sparqlOption);

    if(sqlQ and sparqlQ)
    {
      clog_error("Cannot specify --sql and --sparql at the same time.");
      return -1;
    }
    if(not sqlQ and not sparqlQ)
    {
      clog_error("Need to specify either --sql and --sparql.");
      return -1;
    }

    if(_parser->positionalArguments().size() == 0)
    {
      clog_error("No query was specified.");
      return -1;
    }
    if(_parser->positionalArguments().size() > 1)
    {
      clog_error("Too many queries were specified.");
      return -1;
    }

    // Connect to database
    kDB::Repository::Connection connection = connectionOptions.createConnection(_parser);
    cres_qresult<void> connect_rv = connection.connect();
    if(not connect_rv.is_successful())
    {
      clog_print<clog_print_flag::red>("Failed to connect with error '{}'.",
                                       connect_rv.get_error());
      return -1;
    }

    knowDBC::Query q;

    if(sqlQ)
    {
      q = connection.createSQLQuery(_parser->positionalArguments().first());
    }
    else if(sparqlQ)
    {
      kDB::Repository::RDFDataset defaultDS;
      QList<kDB::Repository::RDFDataset> namedDS;
      if(_parser->isSet(defaultDatasetsOption))
      {
        cres_qresult<kDB::Repository::RDFDataset> ds
          = connection.graphsManager()->getDataset(_parser->value(defaultDatasetsOption));
        CHECK_RESULT(ds, "When retrieving dataset: {}");
        defaultDS = ds.get_value();
      }
      for(const QString& uri : _parser->values(namedDatasetsOption))
      {
        cres_qresult<kDB::Repository::RDFDataset> ds = connection.graphsManager()->getDataset(uri);
        CHECK_RESULT(ds, "When retrieving dataset: {}");
        namedDS.append(ds.get_value());
      }
      q = connection.createSPARQLQuery({defaultDS, namedDS},
                                       _parser->positionalArguments().first());
    }

    knowDBC::Result r = q.execute();

    if(r)
    {
      if(_parser->isSet(outputOption))
      {
        QFile file(_parser->value(outputOption));
        if(file.open(QIODevice::WriteOnly))
        {
          CHECK_RESULT(r.write(&file), "Failed to write results with error {}.");
          return 0;
        }
        else
        {
          clog_error("Failed to open file {} for writting", file.fileName());
          return -1;
        }
      }
      else
      {
        for(int i = 0; i < r.tuples(); ++i)
        {
          std::cout << i;
          for(int j = 0; j < r.fields(); ++j)
          {
            std::cout << " " << r.value(i, j).printable().expect_success().toStdString();
          }
          std::cout << std::endl;
        }
      }
      return 0;
    }
    else
    {
      clog_error("Failed to execute query with error: {}", r.error());
      return -1;
    }
  }
private:
  ConnectionOptions connectionOptions = {false};
  QCommandLineOption sqlOption, sparqlOption, outputOption, namedDatasetsOption,
    defaultDatasetsOption;
};

REGISTER_COMMAND(QueryCommand, query)
