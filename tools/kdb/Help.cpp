#include <clog_print>
#include <clog_qt>

#include <QCommandLineParser>

#include "AbstractCommand.h"

class HelpCommand : public AbstractCommand
{
public:
  HelpCommand() {}
  void setup(QCommandLineParser* _parser) override
  {
    _parser->addPositionalArgument("cmd_name", "Name of the command to get help from.");
  }
  int execute(QCommandLineParser* _parser) override
  {
    if(_parser->positionalArguments().isEmpty())
    {
      clog_print("{}Commands:{}{}", clog_print_flag::bold, clog_print_flag::reset,
                 AbstractCommand::commands.keys());
    }
    else
    {
      QString cmd_name = _parser->positionalArguments().first();
      if(AbstractCommand::commands.contains(cmd_name))
      {
        QCommandLineParser cmd_parser;
        cmd_parser.addHelpOption();
        cmd_parser.addVersionOption();
        AbstractCommand::commands.value(cmd_name)->setup(&cmd_parser);
        cmd_parser.showHelp();
      }
    }
    return 0;
  }
private:
};

REGISTER_COMMAND(HelpCommand, help)
