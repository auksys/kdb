#include <clog_print>
#include <clog_qt>
#include <cres_qt>

#include <QCoreApplication>
#include <QDir>

#include <knowCore/FileFormat.h>
#include <knowCore/Uri.h>
#include <knowCore/UriManager.h>

#include <knowRDF/Serialiser.h>
#include <knowRDF/Triple.h>
#include <knowRDF/TripleStream.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/RDFDataset.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/TripleStreamInserter.h>
#include <kDB/Version.h>

#include "AbstractCommand.h"
#include "ConnectionOptions.h"

#define CHECK_RESULT(_expr_, _str_)                                                                \
  {                                                                                                \
    auto rv = _expr_;                                                                              \
    if(not rv.success())                                                                           \
    {                                                                                              \
      clog_error(_str_, rv.message());                                                             \
      return -1;                                                                                   \
    }                                                                                              \
  }

class TripleStoreCommand : public AbstractCommand
{
public:
  TripleStoreCommand()
      : ttlOption("ttl", "Use turtle format."), loadOption("load", "Load file.", "load_file"),
        saveOption("save", "Save file.", "save_file")
  {
  }
  void setup(QCommandLineParser* _parser) override
  {
    connectionOptions.setup(_parser);
    _parser->addOption(ttlOption);
    _parser->addOption(loadOption);
    _parser->addOption(saveOption);
    _parser->addPositionalArgument("triplestore", "URI of the triple store.");
  }
  int execute(QCommandLineParser* _parser) override
  {
    QString format = knowCore::FileFormat::Turtle;
    if(_parser->isSet(ttlOption))
    {
      format = knowCore::FileFormat::Turtle;
    }
    // Connect to database
    kDB::Repository::Connection connection = connectionOptions.createConnection(_parser);
    cres_qresult<void> connect_rv = connection.connect();
    if(not connect_rv.is_successful())
    {
      clog_print<clog_print_flag::red>("Failed to connect with error '{}'.",
                                       connect_rv.get_error());
      return -1;
    }
    if(_parser->positionalArguments().size() != 1)
    {
      clog_error("Expected a single positional argument with the URI for a triple store.");
      return -1;
    }
    knowCore::Uri ts_uri = _parser->positionalArguments().first();
    auto const& [success_ts, ts, message_ts]
      = connection.graphsManager()->getOrCreateTripleStore(ts_uri);
    if(not success_ts)
    {
      clog_print<clog_print_flag::red>("Failed to get triple store '{}' with error '{}'.", ts_uri,
                                       message_ts.value());
      return -1;
    }
    if(_parser->isSet(loadOption))
    {
      // Setup triple stream
      kDB::Repository::TripleStreamInserter inserter(ts.value());

      // Setup triple stream
      knowRDF::TripleStream stream;
      stream.addListener(&inserter);

      // Reading from a file
      QString fn = _parser->value(loadOption);
      QFile data_ttl_file(fn);
      if(not data_ttl_file.open(QIODevice::ReadOnly))
      {
        clog_print<clog_print_flag::red>("Failed open file '{}' for reading.", fn);
        return -1;
      }

      // Insertion the triples in the database
      cres_qresult<void> rv = stream.start(&data_ttl_file, nullptr, knowCore::FileFormat::Turtle);
      if(not rv.is_successful())
      {
        clog_print<clog_print_flag::red>("Insertion from file {} failed with error {}.", fn,
                                         rv.get_error());
        return -1;
      }
    }
    if(_parser->isSet(saveOption))
    {
      cres_qresult<QList<knowRDF::Triple>> triples_rv = ts.value().triples();
      if(not triples_rv.is_successful())
      {
        clog_print<clog_print_flag::red>("Failure to get triples with error: {}",
                                         triples_rv.get_error());
        return -1;
      }
      QList<knowRDF::Triple> triples = triples_rv.get_value();
      ;

      QString fn = _parser->value(saveOption);
      QFile all_ttl_file(fn);
      if(not all_ttl_file.open(QIODevice::WriteOnly))
      {
        clog_print<clog_print_flag::red>("Failed open file '{}' for writing.", fn);
        return -1;
      }

      knowRDF::Serialiser serialiser(&all_ttl_file, knowCore::UriManager());
      serialiser.serialise(triples);
    }
    return 0;
  }
private:
  ConnectionOptions connectionOptions = {false};
  QCommandLineOption ttlOption, loadOption, saveOption;
};

// clang-format off
REGISTER_COMMAND(TripleStoreCommand, triple-store)
