#include <knowCore/Global.h>

#include <QMap>

class QCommandLineParser;

class AbstractCommand
{
public:
  virtual ~AbstractCommand();
  virtual void setup(QCommandLineParser* _parser) = 0;
  virtual int execute(QCommandLineParser* _parser) = 0;
  static QMap<QString, AbstractCommand*> commands;
};

#define REGISTER_COMMAND(_NAME_, _CMD_NAME_)                                                       \
  struct Factory##_NAME_                                                                           \
  {                                                                                                \
    Factory##_NAME_() { AbstractCommand::commands[#_CMD_NAME_] = new _NAME_(); }                   \
  };                                                                                               \
  Factory##_NAME_ factory##_NAME_;
