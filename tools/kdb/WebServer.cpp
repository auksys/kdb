#include <clog_print>
#include <clog_qt>

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDir>

#include <knowCore/Uri.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/Store.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Version.h>

#include <kDBWebServer/EndPoint.h>

#include "AbstractCommand.h"

class WebServerCommand : public AbstractCommand
{
public:
  WebServerCommand()
      : storePathOption("path", "The path for the store.", "store"),
        storePortOption("port", "The port for the store, default to 1242.", "port"),
        webEndPointPort("web-port", "The port for the web server, default to 8888.", "web-port")
  {
  }
  void setup(QCommandLineParser* _parser) override
  {
    _parser->addOption(storePathOption);
    _parser->addOption(storePortOption);
    _parser->addOption(webEndPointPort);
  }
  int execute(QCommandLineParser* _parser) override
  {
    QString kDBHostName = _parser->value(storePathOption);
    int kDBPort = _parser->isSet(storePortOption) ? _parser->value(storePortOption).toInt() : 1242;
    int webPort = _parser->isSet(webEndPointPort) ? _parser->value(webEndPointPort).toInt() : 8888;

    kDBWebServer::EndPoint ep(kDBHostName, kDBPort);
    ep.setPort(webPort);
    if(not ep.start())
    {
      clog_print<clog_print_flag::red>("Failed to start web server.");
      return -1;
    }

    return QCoreApplication::instance()->exec();
  }
private:
  QCommandLineOption storePathOption, storePortOption, webEndPointPort;
};

// clang-format off
REGISTER_COMMAND(WebServerCommand, web-server)
