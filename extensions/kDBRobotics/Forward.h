#include <kDBGIS/Forward.h>

namespace kDBRobotics
{
  namespace Agents
  {
    class Agent;
    class Collection;
    class Stream;
  } // namespace Agents
} // namespace kDBRobotics
