@base <http://askco.re/robotics/agents_queries#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix askcore_graph: <http://askco.re/graph#> .
@prefix askcore_db_queries: <http://askco.re/db/queries#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix xsi: <http://www.w3.org/2001/XMLSchema-instance#> .


<agent_insert> dc:description "Add an agent description" ;
            foaf:name "agent_insert" ;
            askcore_db_queries:query """
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX askcore_agent:      <http://askco.re/agent#>     # namespace of the vocabulary for describing agents

BEGIN <http://askco.re/db/query_language#pl/sparql>
{
  UNLESS ASK { GRAPH %graph { %agent_uri a askcore_agent:agent . } } THEN
  {
    INSERT DATA { GRAPH %graph {
      %agent_uri a askcore_agent:agent ;
           askcore_agent:agent_type %type ;
           foaf:name %name .
    } }
  }
}""" ;
                 askcore_db_queries:binding [ foaf:name "graph"        ; xsi:type xsd:anyURI ; dc:description "URI for the graph where to insert this agent" ] ;
                 askcore_db_queries:binding [ foaf:name "agent_uri"    ; xsi:type xsd:anyURI ; dc:description "Uri of the agent" ] ;
                 askcore_db_queries:binding [ foaf:name "type"         ; xsi:type xsd:anyURI ; dc:description "Type of agent" ] ;
                 askcore_db_queries:binding [ foaf:name "name"         ; xsi:type xsd:string ; dc:description "Name of the agent" ] ;
                 askcore_db_queries:graph "%graph"
                 .

<agent_select> dc:description "Get information for a specific agent" ;
                foaf:name "agent_select" ;
                askcore_db_queries:query """PREFIX askcore_agent: <http://askco.re/agent#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

SELECT ?type ?name FROM %graph WHERE {
  %agent_uri a askcore_agent:agent ;
             askcore_agent:agent_type ?type ;
             foaf:name ?name .
}""" ;
                askcore_db_queries:binding [ foaf:name "graph"     ; xsi:type xsd:anyURI   ; dc:description "URI for the graph where to get all the agents." ] ;
                askcore_db_queries:binding [ foaf:name "agent_uri" ; xsi:type xsd:anyURI   ; dc:description "Uri of the agent" ] ;
                askcore_db_queries:result  [ foaf:name "type"      ; xsi:type xsd:anyURI   ; dc:description "Type of agent" ] ;
                askcore_db_queries:result  [ foaf:name "name"      ; xsi:type xsd:string   ; dc:description "Name of agent" ] ;
                askcore_db_queries:graph "%graph" .

<agents_select_all> dc:description "List agents" ;
                foaf:name "agents_select_all" ;
                askcore_db_queries:query """PREFIX askcore_agent: <http://askco.re/agent#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

SELECT ?uri ?name ?type FROM %graph WHERE {
  ?uri a askcore_agent:agent ;
       foaf:name ?name ;
       askcore_agent:agent_type ?type .
}""" ;
                askcore_db_queries:binding [ foaf:name "graph"     ; xsi:type xsd:anyURI   ; dc:description "URI for the graph where to get all the agents" ] ;
                askcore_db_queries:result  [ foaf:name "uri"       ; xsi:type xsd:anyURI   ; dc:description "URI of the graph" ] ;
                askcore_db_queries:result  [ foaf:name "type"      ; xsi:type xsd:anyURI   ; dc:description "Type of data" ] ;
                askcore_db_queries:result  [ foaf:name "name"      ; xsi:type xsd:string   ; dc:description "Name of agent" ] ;
                askcore_db_queries:graph "%graph" .

<agent_exists> dc:description "Return true if an agent exists" ;
                 foaf:name "agent_exists" ;
                 askcore_db_queries:query """PREFIX askcore_agent: <http://askco.re/agent#>

ASK { GRAPH %graph { %agent_uri a askcore_agent:agent } }""" ;
                 askcore_db_queries:binding [ foaf:name "graph"        ; xsi:type xsd:anyURI   ; dc:description "URI for the graph where to get all the agents." ] ;
                 askcore_db_queries:binding [ foaf:name "agent_uri"  ; xsi:type xsd:anyURI   ; dc:description "URI for the agent" ] ;
                 askcore_db_queries:result  [ xsi:type xsd:boolean     ; dc:description "True if the agent exists or false otherwise" ] ;
                askcore_db_queries:graph "%graph" .

<agents_count> dc:description "Count agents" ;
                foaf:name "agents_count" ;
                askcore_db_queries:query """PREFIX askcore_agent: <http://askco.re/agent#>

SELECT (COUNT(?uri) AS ?count) FROM %graph WHERE {
  ?uri a askcore_agent:agent .
}""" ;
                askcore_db_queries:binding [ foaf:name "graph"     ; xsi:type xsd:anyURI   ; dc:description "URI for the graph where to get all the agents" ] ;
                askcore_db_queries:result  [ foaf:name "count"     ; xsi:type xsd:integer  ; dc:description "Number of agents" ] ;
                askcore_db_queries:graph "%graph" .

###################################
# Streams

<stream_insert> dc:description "Create a stream" ;
              foaf:name "stream_insert" ;
              askcore_db_queries:query """
PREFIX askcore_stream: <http://askco.re/agent/stream#>     # namespace of the vocabulary that contains the streams concepts

INSERT DATA {
  GRAPH %graph {    
    %stream_uri a askcore_stream:stream ;
                askcore_stream:identifier %identifier ;
                askcore_stream:contenttype %contenttype ;
                askcore_stream:datatype %datatype .
  }
}
""" ;
                askcore_db_queries:binding [ foaf:name "graph"        ; xsi:type xsd:anyURI ; dc:description "URI for the graph where to insert this agent" ] ;
                askcore_db_queries:binding [ foaf:name "stream_uri"   ; xsi:type xsd:anyURI ; dc:description "Uri of the stream" ] ;
                askcore_db_queries:binding [ foaf:name "identifier"   ; xsi:type xsd:string ; dc:description "Identifier of the stream (e.g. could be a topic name)" ] ;
                askcore_db_queries:binding [ foaf:name "contenttype"  ; xsi:type xsd:anyURI ; dc:description "Uri of the content type" ] ;
                askcore_db_queries:binding [ foaf:name "datatype"     ; xsi:type xsd:anyURI ; dc:description "Uri of the data type" ] ;
                askcore_db_queries:graph "%graph"
                .

<stream_exists> dc:description "Return true if an strean exists" ;
                 foaf:name "stream_exists" ;
                 askcore_db_queries:query """PREFIX askcore_stream: <http://askco.re/agent/stream#>

ASK { GRAPH %graph { %stream_uri a askcore_stream:stream } }""" ;
                askcore_db_queries:binding [ foaf:name "graph"        ; xsi:type xsd:anyURI   ; dc:description "URI for the graph where to get all the streams." ] ;
                askcore_db_queries:binding [ foaf:name "stream_uri"  ; xsi:type xsd:anyURI   ; dc:description "URI for the stream" ] ;
                askcore_db_queries:result  [ xsi:type xsd:boolean     ; dc:description "True if the stream exists or false otherwise" ] ;
                askcore_db_queries:graph "%graph"
                .

<stream_select> dc:description "Get information for a specific stream" ;
                foaf:name "stream_select" ;
                askcore_db_queries:query """PREFIX askcore_stream: <http://askco.re/agent/stream#>

SELECT ?datatype ?identifier ?contenttype FROM %graph WHERE {
  %stream_uri a askcore_stream:stream ;
             askcore_stream:datatype ?datatype ;
             askcore_stream:contenttype ?contenttype ;
             askcore_stream:identifier ?identifier .
}""" ;
                askcore_db_queries:binding [ foaf:name "graph"       ; xsi:type xsd:anyURI   ; dc:description "URI for the graph where to get all the agents." ] ;
                askcore_db_queries:binding [ foaf:name "stream_uri"  ; xsi:type xsd:anyURI   ; dc:description "Uri of the stream" ] ;
                askcore_db_queries:result  [ foaf:name "contenttype" ; xsi:type xsd:anyURI   ; dc:description "contenttype of stream" ] ;
                askcore_db_queries:result  [ foaf:name "datatype"    ; xsi:type xsd:anyURI   ; dc:description "Datatype of stream" ] ;
                askcore_db_queries:result  [ foaf:name "identifier"  ; xsi:type xsd:string   ; dc:description "Identifier of agent" ] ;
                askcore_db_queries:graph "%graph" .

<stream_select_all> dc:description "Get information for all streams" ;
                foaf:name "stream_select_all" ;
                askcore_db_queries:query """PREFIX askcore_stream: <http://askco.re/agent/stream#>

SELECT ?stream_uri ?datatype ?identifier ?contenttype FROM %graph WHERE {
  ?stream_uri a askcore_stream:stream ;
             askcore_stream:datatype ?datatype ;
             askcore_stream:contenttype ?contenttype ;
             askcore_stream:identifier ?identifier .
}""" ;
                askcore_db_queries:binding [ foaf:name "graph"       ; xsi:type xsd:anyURI   ; dc:description "URI for the graph where to get all the agents." ] ;
                askcore_db_queries:result  [ foaf:name "stream_uri"  ; xsi:type xsd:anyURI   ; dc:description "Uri of the stream" ] ;
                askcore_db_queries:result  [ foaf:name "contenttype" ; xsi:type xsd:anyURI   ; dc:description "contenttype of stream" ] ;
                askcore_db_queries:result  [ foaf:name "datatype"    ; xsi:type xsd:anyURI   ; dc:description "Datatype of stream" ] ;
                askcore_db_queries:result  [ foaf:name "identifier"  ; xsi:type xsd:string   ; dc:description "Identifier of agent" ] ;
                askcore_db_queries:graph "%graph" .

<stream_select_all_for> dc:description "Get information for all streams" ;
                foaf:name "stream_select_all_for" ;
                askcore_db_queries:query """PREFIX askcore_stream: <http://askco.re/agent/stream#>

SELECT ?stream_uri ?datatype ?identifier ?contenttype FROM %graph WHERE {
  %resource_uri askcore_stream:has_stream ?stream_uri .
  ?stream_uri a askcore_stream:stream ;
             askcore_stream:datatype ?datatype ;
             askcore_stream:contenttype ?contenttype ;
             askcore_stream:identifier ?identifier .
}""" ;
                askcore_db_queries:binding [ foaf:name "graph"       ; xsi:type xsd:anyURI   ; dc:description "URI for the graph where to get all the agents." ] ;
                askcore_db_queries:binding [ foaf:name "resource_uri"; xsi:type xsd:anyURI   ; dc:description "Uri of the resource" ] ;
                askcore_db_queries:result  [ foaf:name "stream_uri"  ; xsi:type xsd:anyURI   ; dc:description "Uri of the stream" ] ;
                askcore_db_queries:result  [ foaf:name "contenttype" ; xsi:type xsd:anyURI   ; dc:description "contenttype of stream" ] ;
                askcore_db_queries:result  [ foaf:name "datatype"    ; xsi:type xsd:anyURI   ; dc:description "Datatype of stream" ] ;
                askcore_db_queries:result  [ foaf:name "identifier"  ; xsi:type xsd:string   ; dc:description "Identifier of agent" ] ;
                askcore_db_queries:graph "%graph" .

<stream_add_to> dc:description "Add a stream" ;
              foaf:name "stream_add_to" ;
              askcore_db_queries:query """
PREFIX askcore_stream: <http://askco.re/agent/stream#>     # namespace of the vocabulary that contains the streams concepts

INSERT DATA {
  GRAPH %graph {
    %resource_uri askcore_stream:has_stream %stream_uri .
  }
}
""" ;
                 askcore_db_queries:binding [ foaf:name "graph"        ; xsi:type xsd:anyURI ; dc:description "URI for the graph where to insert this agent" ] ;
                 askcore_db_queries:binding [ foaf:name "resource_uri" ; xsi:type xsd:anyURI ; dc:description "Uri of the resource" ] ;
                 askcore_db_queries:binding [ foaf:name "stream_uri"   ; xsi:type xsd:anyURI ; dc:description "Uri of the stream" ] ;
                 askcore_db_queries:graph "%graph"
                 .


###################################
# Capabilities

<capability_insert> dc:description "Insert a basic capability" ;
                          foaf:name "capability_insert_basic" ;
                          askcore_db_queries:query """
PREFIX askcore_capabilities: <http://askco.re/agent/capability#>     # namespace of the vocabulary that contains the core concepts

INSERT DATA { GRAPH %graph {

    %agent_uri  askcore_capabilities:hasCapability  %capability_uri .

    %capability_uri a  askcore_capabilities:TaskExecCapability ;
                a  %capability_type ;
                askcore_capabilities:status askcore_capabilities:enabled ;
                askcore_capabilities:tasktype  %task_type .
} }
""" ;
                  askcore_db_queries:binding [ foaf:name "graph"             ; xsi:type xsd:anyURI ; dc:description "URI for the graph where to insert this agent" ] ;
                  askcore_db_queries:binding [ foaf:name "agent_uri"         ; xsi:type xsd:anyURI   ; dc:description "URI for the platform"   ] ;
                  askcore_db_queries:binding [ foaf:name "capability_uri"    ; xsi:type xsd:anyURI   ; dc:description "URI for the capability" ] ;
                  askcore_db_queries:binding [ foaf:name "capability_type"   ; xsi:type xsd:anyURI   ; dc:description "URI for the type of capability i.e. SeqCapability, ConCapability..."   ] ;
                  askcore_db_queries:binding [ foaf:name "task_type"         ; xsi:type xsd:anyURI   ; dc:description "Type of capability i.e. Seq, Con..."   ] ;
                  askcore_db_queries:graph "%graph" .

<resource_insert> dc:description "Insert a resource" ;
                        foaf:name "resource_insert" ;
                        askcore_db_queries:query """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  # standard RDF namespace
PREFIX askcore_capabilities: <http://askco.re/agent/capability#>     # namespace of the vocabulary that contains the core concepts
PREFIX askcore_resources:      <http://askco.re/resources>     # namespace of the vocabulary for describing resources

INSERT DATA { GRAPH %graph {

  %resource rdf:type %resource_type } }
""" ;
                             askcore_db_queries:binding [ foaf:name "graph"             ; xsi:type xsd:anyURI ; dc:description "URI for the graph where to insert this resource" ] ;
                             askcore_db_queries:binding [ foaf:name "resource"          ; xsi:type xsd:anyURI   ; dc:description "URI for the resource"   ] ;
                             askcore_db_queries:binding [ foaf:name "resource_type"     ; xsi:type xsd:anyURI   ; dc:description "URI for the resource"   ] ;
                             askcore_db_queries:graph "%graph" .

<agent_add_resource> dc:description "Add a resource to an agent" ;
                        foaf:name "agent_add_resource" ;
                        askcore_db_queries:query """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  # standard RDF namespace
PREFIX askcore_capabilities: <http://askco.re/agent/capability#>     # namespace of the vocabulary that contains the core concepts

INSERT DATA { GRAPH %graph {
  %platform askcore_capabilities:hasResource  %resource .
            } }
""" ;
                             askcore_db_queries:binding [ foaf:name "graph"             ; xsi:type xsd:anyURI ; dc:description "URI for the graph where to insert this resource" ] ;
                             askcore_db_queries:binding [ foaf:name "platform"           ; xsi:type xsd:anyURI   ; dc:description "URI for the platform"   ] ;
                             askcore_db_queries:binding [ foaf:name "resource"           ; xsi:type xsd:anyURI   ; dc:description "URI for the resource"   ] ;
                             askcore_db_queries:graph "%graph" .
