#include "AgentPosition.h"

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/QueryConnectionInfo.h>

using namespace kDBRobotics;

cres_qresult<void> AgentPositionRecord::updateNextInformation(
  const kDB::Repository::QueryConnectionInfo& _connection, const knowCore::Timestamp& _min,
  const knowCore::Timestamp& _max)
{
  knowDBC::Query query = _connection.createSQLQuery();
  query.setQuery(
    R"(UPDATE agentpositions SET (nextposition, nexttimestamp) = (COALESCE(next_frame.nextposition, 0), COALESCE(next_frame.nexttimestamp, 'Infinity'::numeric))
   FROM (SELECT res.id AS id, (ni).id AS nextposition, (ni).timestamp AS nexttimestamp FROM
          ( SELECT id, nextposition, nexttimestamp,
                   (SELECT (id, timestamp)::kdb_id_to_timestamp FROM agentpositions
                        WHERE name = ft.name AND frame = ft.frame AND timestamp > ft.timestamp
                        ORDER BY timestamp ASC LIMIT 1)  AS ni
                FROM agentpositions AS ft WHERE ft.timestamp >= :min_timestamp AND ft.timestamp <= :max_timestamp ) AS res) next_frame
   WHERE agentpositions.id = next_frame.id)");
  query.bindValue(":min_timestamp", _min);
  query.bindValue(":max_timestamp", _max);
  knowDBC::Result r = query.execute();
  return cres_cond<void>(r, "Failed to update agent position next information", r.error());
}

bool AgentPositionRecord::has(const kDB::Repository::QueryConnectionInfo& _connection,
                              const AgentPositionBaseValue& _value)
{
  return (AgentPositionRecord::byName(_connection, _value.name())
          and AgentPositionRecord::byFrame(_connection, _value.frame())
          and AgentPositionRecord::byTimestamp(_connection, _value.timestamp()))
           .count()
         > 0;
}
