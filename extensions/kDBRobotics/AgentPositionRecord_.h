public:
/**
 * This function will update the next information of all the agent positions between @p _min and @p
 * _max.
 */
static cres_qresult<void>
  updateNextInformation(const kDB::Repository::QueryConnectionInfo& _connection,
                        const knowCore::Timestamp& _min, const knowCore::Timestamp& _max);
static bool has(const kDB::Repository::QueryConnectionInfo& _connection,
                const AgentPositionBaseValue& _value);