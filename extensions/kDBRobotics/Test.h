#pragma once

#include <kDBGIS/Forward.h>
#include <kDBRobotics/AgentPosition.h>

namespace kDBRobotics::Test
{
  /**
   * Create positions within a \p _geometry starting at time \p _min_stamp and altitude between \ref
   * _min_altitude m and \ref _max_altitude m. The trajectory follow a lawn mower pattern with \p
   * _spacing m between strides. The platform moves at \p _speed m/s.
   */
  QList<AgentPositionRecord> createLawnMowerTrajectory(
    const kDB::Repository::QueryConnectionInfo& _connection, const QString& _name,
    const QString& _frame, const knowGIS::GeometryObject& _geometry,
    const knowCore::Timestamp& _start_stamp, qreal _min_altitude, qreal _max_altitude,
    double _speed, double _frequency, double _spacing);
} // namespace kDBRobotics::Test
