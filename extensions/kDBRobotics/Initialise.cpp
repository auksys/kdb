#include "Initialise.h"

#include <clog_qt>

#include <knowCore/UriList.h>

#include <kDB/Repository/ActiveRecord/Version.h>
#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/PersistentDatasetsUnion.h>
#include <kDB/Repository/QueryConnectionInfo.h>

#include <kDBGIS/Initialise.h>

#include "AgentPosition.h"
#include "Agents/Collection.h"
#include "Event.h"
#include "FrameTransformation.h"

#include <knowCore/Uris/askcore_agent.h>
#include <knowCore/Uris/askcore_graph.h>

using askcore_agent = knowCore::Uris::askcore_agent;
using askcore_stream = knowCore::Uris::askcore_stream;

inline void kdbrobotics_init_data() { Q_INIT_RESOURCE(kdbrobotics_data); }

cres_qresult<void> kDBRobotics::initialise(const kDB::Repository::Connection& _connection)
{
  kdbrobotics_init_data();

  cres_try(cres_ignore, kDB::Repository::Connection(_connection).enableExtension("kDBGIS"));

  AgentPositionRecord::createTable(_connection);
  FrameTransformationRecord::createTable(_connection);
  EventRecord::createTable(_connection);

  cres_try(kDB::Repository::ActiveRecord::VersionRecord vr,
           kDB::Repository::ActiveRecord::VersionRecord::get(_connection, "kDBRobotics"));

  cres_try(cres_ignore,
           _connection.graphsManager()->loadViewsFrom(":/kdbrobotics/sml/", knowCore::ValueHash()));

  Agents::Collection::registerCollection(_connection);

  cres_try(kDBRobotics::Agents::Collection dss,
           kDBRobotics::Agents::Collection::getOrCreate(
             _connection, knowCore::Uris::askcore_graph::private_agents),
           message("Failed to initialise private agents: {}"));

  clog_debug_vn(dss.isValid(), dss.uri());

  if(vr.striclyLessThan(2, 0, 0))
  {
    QString errmsg;
    cres_try(cres_ignore, _connection.executeQueryFromFile(":/kdbrobotics/sql/types.sql"));
  }

  vr.updateToCurrent();

  return cres_success();
}

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBRobotics)
