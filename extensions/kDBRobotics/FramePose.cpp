#include "FramePose.h"

#include <knowCore/Bindings/Eigen.h>

#include <knowGIS/GeoPose.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>

#include <kDBRobotics/AgentPosition.h>
#include <kDBRobotics/FrameTransformation.h>

using namespace kDBRobotics;

struct FramePose::Private : public QSharedData
{
  kDB::Repository::Connection connection;
  QHash<QString, QString> rootFramesToAgent;
};

FramePose::FramePose() : d(nullptr) {}

FramePose::FramePose(const kDB::Repository::Connection& _connection) : d(new Private)
{
  d->connection = _connection;
  knowDBC::Query query
    = d->connection.createSQLQuery("SELECT DISTINCT name, frame FROM agentpositions");
  knowDBC::Result result = query.execute();

  for(int i = 0; i < result.tuples(); ++i)
  {
    d->rootFramesToAgent[result.value<QString>(i, 1).expect_success()]
      = result.value<QString>(i, 0).expect_success();
  }
}

FramePose::FramePose(const FramePose& _rhs) : d(_rhs.d) {}

FramePose& FramePose::operator=(const FramePose& _rhs)
{
  d = _rhs.d;
  return *this;
}

FramePose::~FramePose() {}

cres_qresult<knowGIS::GeoPose> FramePose::frameGeoPose(const QString& _frame_id,
                                                       const knowCore::Timestamp& _timestamp)
{
  cres_try(knowGIS::Pose pose, framePose(_frame_id, _timestamp, Cartography::CoordinateSystem()));
  return cres_success(knowGIS::GeoPose::from(pose));
}

cres_qresult<knowGIS::Pose>
  FramePose::framePose(const QString& _frame_id, const knowCore::Timestamp& _timestamp,
                       const Cartography::CoordinateSystem& _coordinateSystem)
{
  Eigen::Vector3d translation;
  Eigen::Quaterniond rotation;

  cres_try(Cartography::CoordinateSystem cs,
           framePose(&translation, &rotation, _frame_id, _timestamp, _coordinateSystem));
  clog_assert(not _coordinateSystem.isValid() or cs == _coordinateSystem);
  return cres_success(
    knowGIS::Pose(knowGIS::Point(translation.x(), translation.y(), translation.z(), cs),
                  knowGIS::Quaternion(rotation.x(), rotation.y(), rotation.z(), rotation.w())));
}

cres_qresult<Cartography::CoordinateSystem>
  FramePose::framePose(Eigen::Vector3d* _translation, Eigen::Quaterniond* _rotation,
                       const QString& _frame_id, const knowCore::Timestamp& _timestamp,
                       const Cartography::CoordinateSystem& _coordinateSystem)
{
  namespace kDBBindings = knowCore::Bindings;

  *_translation = Eigen::Vector3d::Zero();
  *_rotation = Eigen::Quaterniond::Identity();

  QString nextFrame = _frame_id;

  Cartography::CoordinateSystem coordinateSystem = _coordinateSystem;

  while(not d->rootFramesToAgent.contains(nextFrame))
  {
    const QList<kDBRobotics::FrameTransformationRecord> transfos
      = (kDBRobotics::FrameTransformationRecord::byChildFrame(d->connection, nextFrame,
                                                              kDBRobotics::Sql::Operand::EQ)
         and kDBRobotics::FrameTransformationRecord::byTimestamp(d->connection, _timestamp,
                                                                 kDBRobotics::Sql::Operand::LTE)
         and kDBRobotics::FrameTransformationRecord::byNextTimestamp(d->connection, _timestamp,
                                                                     kDBRobotics::Sql::Operand::GT))
          .exec();

    if(transfos.size() == 0)
    {
      return cres_failure("Failed to get transformations at timestamp {} for frame {}", _timestamp,
                          nextFrame);
    }
    else
    {
      if(transfos.size() > 1)
      {
        clog_warning("Got too many transformation at {} for frame {}", _timestamp, nextFrame);
      }
      const kDBRobotics::FrameTransformationRecord transfo = transfos.first();
      const kDBRobotics::FrameTransformationRecord nextTransfo = transfo.nextFrame();

      const Eigen::Vector3d trans1 = kDBBindings::convert(transfo.translation());
      const Eigen::Vector4d rot1_vec = kDBBindings::convert(transfo.rotation());
      const Eigen::Quaterniond rot1(rot1_vec(3), rot1_vec.x(), rot1_vec.y(), rot1_vec.z());

      Eigen::Vector3d trans_interp;
      Eigen::Quaterniond rot_interp;

      if(nextTransfo.isPersistent())
      {
        const Eigen::Vector3d trans2 = kDBBindings::convert(nextTransfo.translation());
        const Eigen::Vector4d rot2_vec = kDBBindings::convert(nextTransfo.rotation());
        const Eigen::Quaterniond rot2(rot2_vec(3), rot2_vec.x(), rot2_vec.y(), rot2_vec.z());

        const double t
          = ((_timestamp - transfo.timestamp()).toEpoch<knowCore::NanoSeconds>().count()
             / (nextTransfo.timestamp() - transfo.timestamp())
                 .toEpoch<knowCore::NanoSeconds>()
                 .count())
              .toDouble();

        trans_interp = (1 - t) * trans1 + t * trans2;
        rot_interp = rot1.slerp(t, rot2);
      }
      else
      {
        trans_interp = trans1;
        rot_interp = rot1;
      }
      *_translation = trans_interp + rot_interp * *_translation;
      *_rotation = rot_interp * *_rotation;
      nextFrame = transfo.parentFrame();
    }
  }
  // BEGIN Apply agent position
  {
    const QString agentName = d->rootFramesToAgent.value(nextFrame);
    const QList<kDBRobotics::AgentPositionRecord> positions
      = (kDBRobotics::AgentPositionRecord::byName(d->connection, agentName)
         and kDBRobotics::AgentPositionRecord::byTimestamp(d->connection, _timestamp,
                                                           kDBRobotics::Sql::Operand::LTE)
         and kDBRobotics::AgentPositionRecord::byNextTimestamp(d->connection, _timestamp,
                                                               kDBRobotics::Sql::Operand::GT))
          .exec();
    if(positions.size() == 0)
    {
      return cres_failure("Failed to get position at timestamp {} for agent {}", _timestamp,
                          agentName);
    }
    else
    {
      if(positions.size() > 1)
      {
        clog_error("Got too many positions at {} for agent {}", _timestamp, agentName);
        for(const kDBRobotics::AgentPositionRecord& record : positions)
        {
          clog_error("at {} for next stamp {}", record.timestamp(), _timestamp,
                     record.nextTimestamp());
        }
      }

      kDBRobotics::AgentPositionRecord beforePosition = positions.first();
      kDBRobotics::AgentPositionRecord afterPosition = beforePosition.nextPosition();

      if(afterPosition.isPersistent())
      {
        const double t
          = ((_timestamp - beforePosition.timestamp()).toEpoch<knowCore::NanoSeconds>().count()
             / (afterPosition.timestamp() - beforePosition.timestamp())
                 .toEpoch<knowCore::NanoSeconds>()
                 .count())
              .toDouble();

        const Eigen::Vector4d rot1_vec = kDBBindings::convert(beforePosition.rotation());
        const Eigen::Vector4d rot2_vec = kDBBindings::convert(afterPosition.rotation());
        const Eigen::Quaterniond rot1(rot1_vec(3), rot1_vec.x(), rot1_vec.y(), rot1_vec.z());
        const Eigen::Quaterniond rot2(rot2_vec(3), rot2_vec.x(), rot2_vec.y(), rot2_vec.z());

        qreal longitude_interp
          = (1 - t) * beforePosition.longitude() + t * afterPosition.longitude();
        qreal latitude_interp = (1 - t) * beforePosition.latitude() + t * afterPosition.latitude();
        qreal altitude_interp = (1 - t) * beforePosition.altitude() + t * afterPosition.altitude();
        const Eigen::Quaterniond rot_interp = rot1.slerp(t, rot2);

        knowGIS::GeoPoint coord_wgs84(Cartography::Longitude{longitude_interp},
                                      Cartography::Latitude{latitude_interp});

        if(not coordinateSystem.isValid())
        {
          coordinateSystem = Cartography::CoordinateSystem::utm(coord_wgs84);
        }

        knowGIS::Point coord = coord_wgs84.transform(coordinateSystem);
        *_translation
          = Eigen::Vector3d(coord.x(), coord.y(), altitude_interp) + rot_interp * *_translation;

        *_rotation = rot_interp * *_rotation;
      }
      else
      {
        const Eigen::Vector4d rot1_vec = kDBBindings::convert(beforePosition.rotation());
        const Eigen::Quaterniond rot1(rot1_vec(3), rot1_vec.x(), rot1_vec.y(), rot1_vec.z());

        knowGIS::GeoPoint coord_wgs84(Cartography::Longitude{beforePosition.longitude()},
                                      Cartography::Latitude{beforePosition.latitude()});

        if(not coordinateSystem.isValid())
        {
          coordinateSystem = Cartography::CoordinateSystem::utm(coord_wgs84);
        }

        knowGIS::Point coord = coord_wgs84.transform(coordinateSystem);

        *_translation
          = Eigen::Vector3d(coord.x(), coord.y(), beforePosition.altitude()) + rot1 * *_translation;

        *_rotation = rot1 * *_rotation;
      }
    }
  }
  // END Apply agent position
  return cres_success(coordinateSystem);
}
