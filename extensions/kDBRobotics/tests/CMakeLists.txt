add_executable(TestAgents TestAgents.cpp )
target_link_libraries(TestAgents kDBRobotics knowValues Qt6::Test)
add_test(NAME TEST-Robotics-Agents COMMAND TestAgents)
