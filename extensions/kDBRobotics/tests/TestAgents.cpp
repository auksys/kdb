#include "TestAgents.h"

#include <clog_qt>
#include <cres_qt>

#include <knowCore/Test.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/PersistentDatasetsUnion.h>
#include <kDB/Repository/TemporaryStore.h>

#include <kDBRobotics/Agents/Agent.h>
#include <kDBRobotics/Agents/Collection.h>
#include <kDBRobotics/Agents/Stream.h>

#include <knowCore/Uris/askcore_agent.h>
#include <knowCore/Uris/askcore_graph.h>

using askcore_agent = knowCore::Uris::askcore_agent;

void TestAgents::testInitialisation()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBRobotics"));
}

void TestAgents::testCreation()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBRobotics"));

  // Create a agnss1: set of agents
  kDBRobotics::Agents::Collection agnss1;

  QCOMPARE(agnss1.isValid(), false);
  agnss1 = CRES_QVERIFY(kDBRobotics::Agents::Collection::getOrCreate(c, "agnss1"_kCu));
  QCOMPARE(agnss1.isValid(), true);

  QVERIFY(CRES_QVERIFY(c.graphsManager()->getUnion(knowCore::Uris::askcore_graph::all_focus_nodes))
            .contains("agnss1"_kCu));

  // Create a agnss2: set of agents
  kDBRobotics::Agents::Collection agnss2
    = CRES_QVERIFY(kDBRobotics::Agents::Collection::create(c, "agnss2"_kCu));

  QCOMPARE(agnss2.isValid(), true);
  QVERIFY(CRES_QVERIFY(c.graphsManager()->getUnion(knowCore::Uris::askcore_graph::all_focus_nodes))
            .contains("agnss2"_kCu));

  // Check that getting agnsss1 gives it back
  CRES_QCOMPARE(kDBRobotics::Agents::Collection::get(c, "agnss1"_kCu), agnss1);

  // Check that agnss1 cannot be created twice
  CRES_QVERIFY_FAILURE(kDBRobotics::Agents::Collection::create(c, "agnss1"_kCu));

  // Check that getting uncreated agents fails
  CRES_QVERIFY_FAILURE(agnss1.agent("agent1"_kCu));
  CRES_QVERIFY_FAILURE(agnss2.agent("agent1"_kCu));

  // Create an agent
  kDBRobotics::Agents::Agent agent1 = CRES_QVERIFY(
    agnss1.createAgent(askcore_agent::uav, "uav1", "uav1/body_frame", "agnt.uav1"_kCu));
  CRES_QVERIFY(agent1.exists());
  QCOMPARE(agent1.type(), askcore_agent::uav);
  CRES_QCOMPARE(agent1.name(), "uav1");
  QCOMPARE(agent1.uri(), "agnt.uav1");

  CRES_QCOMPARE(agnss1.agent("agnt.uav1"_kCu), agent1);
  CRES_QVERIFY_FAILURE(agnss2.agent("agnt.uav1"_kCu));
  CRES_QVERIFY_FAILURE(
    agnss2.createAgent(askcore_agent::uav, "uav1", "uav1/body_frame", "agnt.uav1"_kCu));

  kDBRobotics::Agents::Stream stream1 = CRES_QVERIFY(
    agnss1.createStream("stream_type"_kCu, "/stream", "data_type"_kCu, "agnt.stream1"_kCu));
  CRES_QCOMPARE(stream1.exists(), true);
  CRES_QCOMPARE(stream1.contentType(), "stream_type"_kCu);
  CRES_QCOMPARE(stream1.identifier(), "/stream");
  CRES_QCOMPARE(stream1.datatype(), "data_type"_kCu);
  QCOMPARE(stream1.uri(), "agnt.stream1"_kCu);

  CRES_QVERIFY_FAILURE(agnss2.agent("agnt.stream1"_kCu));

  CRES_QVERIFY(agent1.addStream(stream1));
  QList<kDBRobotics::Agents::Stream> agent1_streams = CRES_QVERIFY(agent1.streams());
  QCOMPARE(agent1_streams.size(), 1);
  QCOMPARE(agent1_streams.first(), stream1);

  agent1 = CRES_QVERIFY(agnss1.agent("agnt.uav1"_kCu));
  CRES_QCOMPARE(agent1.exists(), true);
  QCOMPARE(agent1.type(), askcore_agent::uav);
  CRES_QCOMPARE(agent1.name(), "uav1");
  QCOMPARE(agent1.uri(), "agnt.uav1");

  CRES_QCOMPARE(agnss1.agent("agnt.uav1"_kCu), agent1);
  CRES_QVERIFY_FAILURE(agnss2.agent("agnt.uav1"_kCu));
  CRES_QVERIFY_FAILURE(
    agnss2.createAgent(askcore_agent::uav, "uav1", "uav1/body_frame", "agnt.uav1"_kCu));

  stream1 = CRES_QVERIFY(agnss1.stream("agnt.stream1"_kCu));
  CRES_QCOMPARE(stream1.exists(), true);
  CRES_QCOMPARE(stream1.contentType(), "stream_type"_kCu);
  CRES_QCOMPARE(stream1.identifier(), "/stream");
  CRES_QCOMPARE(stream1.datatype(), "data_type"_kCu);
  QCOMPARE(stream1.uri(), "agnt.stream1"_kCu);

  agent1_streams = CRES_QVERIFY(agent1.streams());
  QCOMPARE(agent1_streams.size(), 1);
  QCOMPARE(agent1_streams.first(), stream1);
}

QTEST_MAIN(TestAgents)
