#include <kDB/Repository/AbstractBinaryMarshal.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/BinaryMarshalsRegistry.h>

#include <kDB/Repository/DatabaseInterface/PostgreSQL/BinaryInterface_read_p.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/BinaryInterface_write_p.h>

#include <knowGIS/GeoPose.h>

namespace kDBRobotics
{
  class GeoPoseMarshal : public kDB::Repository::AbstractBinaryMarshal
  {
  public:
    GeoPoseMarshal()
        : AbstractBinaryMarshal("kdb_geopose", knowGIS::GeoPoseMetaTypeInformation::uri(),
                                Mode::ToVariant | Mode::ToByteArray)
    {
    }
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override
    {
      _oidName = "kdb_geopose";
      knowGIS::GeoPose gp = _source.value<knowGIS::GeoPose>().expect_success();

      cres_try(quint64 kdb_geopose_oid, _connection.oid("kdb_geopose"));
      cres_try(quint64 kdb_geopoint_oid, _connection.oid("kdb_geopoint"));
      cres_try(quint64 kdb_quaternion_oid, _connection.oid("kdb_quaternion"));
      kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::struct_writer<
        kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::struct_writer<
          double, double, double>,
        kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::struct_writer<
          double, double, double, double>>

        w(kdb_geopose_oid,
          kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::struct_writer<
            double, double, double>(kdb_geopoint_oid, gp.position().longitude(),
                                    gp.position().latitude(), gp.position().altitude()),
          kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::struct_writer<
            double, double, double, double>(kdb_quaternion_oid, gp.orientation().x(),
                                            gp.orientation().y(), gp.orientation().z(),
                                            gp.orientation().w()));

      QByteArray destination;
      destination.resize(w.calculate_size());
      w.write(destination.begin());

      return cres_success(destination);
    }
    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);

      typedef std::tuple<std::tuple<double, double, double>,
                         std::tuple<double, double, double, double>>
        GeoPoseT;

      GeoPoseT t = kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::read<GeoPoseT>(
        _source.begin(), _source.size());

      std::tuple<double, double, double> tp = std::get<0>(t);
      std::tuple<double, double, double, double> tr = std::get<1>(t);
      return cres_success(knowCore::Value::fromValue(knowGIS::GeoPose(
        {Cartography::Longitude(std::get<0>(tp)), Cartography::Latitude(std::get<1>(tp)),
         Cartography::Altitude(std::get<2>(tp))},
        {std::get<0>(tr), std::get<1>(tr), std::get<2>(tr), std::get<3>(tr)})));
    }
  };

  KDB_REGISTER_SQL_MARSHAL(GeoPoseMarshal)

} // namespace kDBRobotics
