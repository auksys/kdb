#include <QExplicitlySharedDataPointer>

#include <kDB/Repository/RDF/FocusNode.h>
#include <knowCore/Value.h>

#include <kDBRobotics/Forward.h>

namespace kDBRobotics::Agents
{
  class Stream : public kDB::Repository::RDF::FocusNodeWrapper<Stream>
  {
    friend class Collection;
  public:
    Stream();
    Stream(const Stream& _rhs);
    Stream& operator=(const Stream& _rhs);
    ~Stream();
    static cres_qresult<Stream> fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node);
  public:
    cres_qresult<QString> identifier() const;
    cres_qresult<knowCore::Uri> datatype() const;
    cres_qresult<knowCore::Uri> contentType() const;
  };
} // namespace kDBRobotics::Agents