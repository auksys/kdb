#include "Stream.h"

#include <knowCore/Uris/askcore_agent.h>

using namespace kDBRobotics::Agents;

using askcore_stream = knowCore::Uris::askcore_stream;

Stream::Stream() {}

Stream::Stream(const Stream& _rhs) : kDB::Repository::RDF::FocusNodeWrapper<Stream>(_rhs) {}

Stream& Stream::operator=(const Stream& _rhs)
{
  return kDB::Repository::RDF::FocusNodeWrapper<Stream>::operator=(_rhs);
}

Stream::~Stream() {}

cres_qresult<Stream> Stream::fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node)
{
  Stream st;
  st.setFocusNode(_focus_node);
  return cres_success(st);
}

cres_qresult<QString> Stream::identifier() const
{
  return property<QString>(askcore_stream::identifier);
}

cres_qresult<knowCore::Uri> Stream::datatype() const
{
  return property<knowCore::Uri>(askcore_stream::data_type);
}

cres_qresult<knowCore::Uri> Stream::contentType() const
{
  return property<knowCore::Uri>(askcore_stream::content_type);
}
