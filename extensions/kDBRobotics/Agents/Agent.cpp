#include "Agent.h"

#include <cres_clog>

#include <knowCore/UriList.h>

#include <knowRDF/Literal.h>

#include <kDB/Repository/QueryConnectionInfo.h>

#include <kDB/Repository/RDF/FocusNode.h>
#include <kDB/Repository/RDF/FocusNodeDeclaration.h>
#include <kDB/Repository/RDF/FocusNodeDeclarationsRegistry.h>

#include <kDBRobotics/Queries/Agents.h>

#include <knowCore/Uris/askcore_agent.h>
#include <knowCore/Uris/foaf.h>

#include "Stream.h"

using namespace kDBRobotics::Agents;

using askcore_agent = knowCore::Uris::askcore_agent;
using askcore_stream = knowCore::Uris::askcore_stream;
using foaf = knowCore::Uris::foaf;

Agent::Agent() {}

Agent::Agent(const Agent& _rhs) : kDB::Repository::RDF::FocusNodeWrapper<Agent>(_rhs) {}

Agent& Agent::operator=(const Agent& _rhs)
{
  return kDB::Repository::RDF::FocusNodeWrapper<Agent>::operator=(_rhs);
}

Agent::~Agent() {}

cres_qresult<Agent> Agent::fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node)
{
  cres_try(knowCore::Uri agent_type,
           _focus_node.property<knowCore::Uri>(askcore_agent::agent_type));
  cres_try(
    kDB::Repository::RDF::FocusNodeDeclaration declaration,
    kDB::Repository::RDF::FocusNodeDeclarationsRegistry::byConstantField(
      askcore_agent::agent, askcore_agent::agent_type, knowRDF::Literal::fromValue(agent_type)));
  Agent ag;
  ag.setFocusNode(kDB::Repository::RDF::FocusNode(_focus_node.connection(), _focus_node.graph(),
                                                  _focus_node.uri(), declaration));
  return cres_success(ag);
}

cres_qresult<QString> Agent::name() const { return property<QString>(foaf::name); }

cres_qresult<void> Agent::addStream(const Stream& _stream)

{
  return focusNodeRef().addPropertyToList(askcore_stream::has_stream,
                                          knowCore::Value::fromValue(_stream.uri()));
}

cres_qresult<QList<Stream>> Agent::streams() const
{
  cres_try(QList<knowCore::Uri> uris, property<knowCore::UriList>(askcore_stream::has_stream));
  QList<Stream> r;

  cres_try(
    kDB::Repository::RDF::FocusNodeDeclaration stream_decl,
    kDB::Repository::RDF::FocusNodeDeclarationsRegistry::declaration(askcore_stream::stream));

  for(const knowCore::Uri& uri : uris)
  {
    cres_try(Stream st, Stream::fromFocusNode(kDB::Repository::RDF::FocusNode(connection(), graph(),
                                                                              uri, stream_decl)));
    r.append(st);
  }

  return cres_success(r);
}
