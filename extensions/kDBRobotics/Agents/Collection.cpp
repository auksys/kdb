#include "Collection.h"

#include <QCborMap>

#include <knowCore/UriList.h>

#include <knowCore/Uris/askcore_agent.h>
#include <knowCore/Uris/askcore_graph.h>
#include <knowCore/Uris/foaf.h>

#include <knowGIS/GeometryObject.h>
#include <knowGIS/Uris/geo.h>

#include <kDB/Repository/Connection.h>

#include <kDB/Repository/RDF/FocusNode.h>
#include <kDB/Repository/RDF/FocusNodeDeclarationsRegistry.h>

#include "Agent.h"
#include "Stream.h"

KDB_REPOSITORY_REGISTER_FOCUS_NODE_DECLARATIONS("qrc:///kdbrobotics/data/agents_shacl.ttl");

using namespace kDBRobotics::Agents;

using askcore_agent = knowCore::Uris::askcore_agent;
using askcore_graph = knowCore::Uris::askcore_graph;
using askcore_stream = knowCore::Uris::askcore_stream;
using FocusNode = kDB::Repository::RDF::FocusNode;
using FocusNodeCollection = kDB::Repository::RDF::FocusNodeCollection;

knowCore::Uri Collection::collectionType() { return askcore_agent::collection; }

knowCore::Uri Collection::allFocusNodesView() { return askcore_graph::all_agents; }

knowCore::Uri Collection::primaryType() { return askcore_agent::agent; }

cres_qresult<knowCore::UriList>
  Collection::defaultDatatypes(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>&)
{
  return cres_success<knowCore::UriList>({primaryType()});
}

knowCore::UriList Collection::containedTypes()
{
  return {knowCore::Uri(askcore_agent::agent), knowCore::Uri(askcore_stream::stream)};
}

Collection::Collection() {}

Collection::Collection(const kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>& _rhs)
    : kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>(_rhs)
{
}

Collection::Collection(const Collection& _rhs)
    : kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>(_rhs)
{
}

Collection& Collection::operator=(const Collection& _rhs)
{
  kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>::operator=(_rhs);
  return *this;
}

Collection::~Collection() {}

Collection Collection::allAgents(const kDB::Repository::Connection& _connection)
{
  return get(_connection, allFocusNodesView()).expect_success();
}

cres_qresult<Agent> Collection::agent(const knowCore::Uri& _agentUri) const
{
  return focusNode(_agentUri);
}

cres_qresult<bool> Collection::hasAgent(const knowCore::Uri& _agentUri) const
{
  return hasFocusNode(_agentUri);
}

cres_qresult<Stream> Collection::stream(const knowCore::Uri& _streamUri) const
{
  cres_try(FocusNode fn, focusNodeCollection().focusNode(_streamUri));
  cres_try(Stream vt, Stream::fromFocusNode(fn));
  return cres_success(vt);
}

cres_qresult<bool> Collection::hasStream(const knowCore::Uri& _streamUri) const
{
  return hasFocusNode(_streamUri, askcore_stream::stream);
}

cres_qresult<QList<Agent>>
  Collection::agents(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
                     const OperatorOptions& _operatorOptions) const
{
  return focusNodes(_constraints, _operatorOptions);
}

cres_qresult<Agent> Collection::createAgent(const knowCore::Uri& _typeUri, const QString& _name,
                                            const QString& _frame_name,
                                            const knowCore::Uri& _agentUri)
{
  cres_try(bool exists, allAgents(connection()).hasAgent(_agentUri));
  if(exists)
  {
    return cres_failure("Agent {} already exists.", _agentUri);
  }
  return createFocusNode(_typeUri,
                         {askcore_agent::agent_type, _typeUri, knowCore::Uris::foaf::name, _name,
                          askcore_agent::frame, _frame_name},
                         _agentUri);
}
cres_qresult<Stream> Collection::createStream(const knowCore::Uri& _contentTypeUri,
                                              const QString& _identifier,
                                              const knowCore::Uri& _dataTypeUri,
                                              const knowCore::Uri& _streamUri)
{
  cres_try(FocusNode fn, focusNodeCollection().createFocusNode(
                           askcore_stream::stream,
                           {askcore_stream::identifier, _identifier, askcore_stream::data_type,
                            _dataTypeUri, askcore_stream::content_type, _contentTypeUri},
                           _streamUri));
  return Stream::fromFocusNode(fn);
}

#include "moc_Collection.cpp"
