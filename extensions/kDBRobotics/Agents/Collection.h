#include <kDBRobotics/Forward.h>

#include <QExplicitlySharedDataPointer>

#include <knowCore/Uri.h>

#include <kDB/Repository/RDF/FocusNodeCollection.h>

class TestAgents;

template<>
struct kDB::Repository::RDF::FocusNodeCollectionTrait<kDBRobotics::Agents::Collection>
{
  using ValueType = kDBRobotics::Agents::Agent;
};

namespace kDBRobotics::Agents
{

  /**
   * Interface a @ref kDB::Repository::TripleStore that contains a set of agents.
   */
  class Collection : public kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>
  {
    friend class ::TestAgents;
  public:
    using ValueType = Agent;
  protected:
    Collection(const kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>& _rhs);
  public:
    Collection();
    Collection(const Collection& _rhs);
    Collection& operator=(const Collection& _rhs);
    ~Collection();
  public:
    static knowCore::Uri collectionType();
    static knowCore::Uri allFocusNodesView();
    static knowCore::Uri primaryType();
    static knowCore::UriList containedTypes();
    static cres_qresult<knowCore::UriList>
      defaultDatatypes(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints);
  public:
    /**
     * \return an interface to the view that covers all agents.
     */
    static Collection allAgents(const kDB::Repository::Connection& _connection);
  public:
    /**
     * \return the agent with the Uri \ref _agentUri
     */
    cres_qresult<Agent> agent(const knowCore::Uri& _agentUri) const;
    /**
     * \return the stream with the Uri \ref _agentUri
     */
    cres_qresult<Stream> stream(const knowCore::Uri& _streamUri) const;
    /**
     * @return true if the agent already exists.
     */
    cres_qresult<bool> hasAgent(const knowCore::Uri& _agentUri) const;
    /**
     * @return true if the stream already exists.
     */
    cres_qresult<bool> hasStream(const knowCore::Uri& _streamUri) const;
    /**
     * @return all the streams associated to a resource.
     */
    cres_qresult<QList<Stream>> streamsOf(const knowCore::Uri& _resource) const;
    using OperatorOptions = kDB::Repository::RDF::FocusNodeCollection::OperatorOptions;
    /**
     * @param _constraints a list of pair of list of uris representing the property uri and a
     * constraint.
     * @param _operatorOptions set the precision used by operators
     *
     * \return the list of agents that satisfies the constraints
     */
    cres_qresult<QList<Agent>>
      agents(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
             const OperatorOptions& _operatorOptions = OperatorOptions()) const;
    template<typename... _TArgs_>
    cres_qresult<QList<Agent>> agents(const knowCore::Uri& _uri,
                                      const knowCore::ConstrainedValue& _constraint,
                                      const _TArgs_&...) const;
    /**
     * Create a new \ref Agent of uri type \p _typeUri with name \p _name and add it to this agent
     * graph.
     */
    cres_qresult<Agent>
      createAgent(const knowCore::Uri& _typeUri, const QString& _name, const QString& _frame_name,
                  const knowCore::Uri& _agentUri = knowCore::Uri::createUnique({"agent"}));
    /**
     * Create a new \ref Stream of uri type \p _typeUri with name \p _name and add it to this agent
     * graph.
     */
    cres_qresult<Stream> createStream(const knowCore::Uri& _contentTypeUri,
                                      const QString& _identifier, const knowCore::Uri& _dataTypeUri,
                                      const knowCore::Uri& _streamUri
                                      = knowCore::Uri::createUnique({"stream"}));
  };
} // namespace kDBRobotics::Agents
