#include <kDB/Repository/RDF/FocusNode.h>

#include <kDBRobotics/Forward.h>

namespace kDBRobotics::Agents
{
  /**
   * @ingroup kDBRobotics
   * This class allow to handle a manage a agent
   */
  class Agent : public kDB::Repository::RDF::FocusNodeWrapper<Agent>
  {
    friend class Collection;
  public:
    /**
     * Create an invalid agent
     */
    Agent();
    Agent(const Agent& _rhs);
    Agent& operator=(const Agent& _rhs);
    ~Agent();
    static cres_qresult<Agent> fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node);
  public:
    /**
     * @return the name of the agent, can be used for display.
     */
    cres_qresult<QString> name() const;
    cres_qresult<void> addStream(const Stream& _stream);
    /**
     * @return the list of streams published by this agent
     */
    cres_qresult<QList<Stream>> streams() const;
  };

} // namespace kDBRobotics::Agents

#include <knowCore/Formatter.h>
