#include "FrameTransformation.h"

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/QueryConnectionInfo.h>

using namespace kDBRobotics;

cres_qresult<void> FrameTransformationRecord::updateNextInformation(
  const kDB::Repository::QueryConnectionInfo& _connection, const knowCore::Timestamp& _min,
  const knowCore::Timestamp& _max)
{
  knowDBC::Query query = _connection.createSQLQuery();
  query.setQuery(
    R"(UPDATE frametransformations SET (nextframe, nexttimestamp) = (COALESCE(next_frame.nextframe, 0), COALESCE(next_frame.nexttimestamp, 'Infinity'::numeric))
   FROM (SELECT res.id AS id, (ni).id AS nextframe, (ni).timestamp AS nexttimestamp FROM
          ( SELECT id, nextframe, nexttimestamp,
                   (SELECT (id, timestamp)::kdb_id_to_timestamp FROM frametransformations
                        WHERE parentframe = ft.parentframe AND childframe = ft.childframe AND timestamp > ft.timestamp
                        ORDER BY timestamp ASC LIMIT 1) AS ni
                FROM frametransformations AS ft WHERE ft.timestamp >= :min_timestamp AND ft.timestamp <= :max_timestamp ) AS res) next_frame
   WHERE frametransformations.id = next_frame.id)");
  query.bindValue(":min_timestamp", _min);
  query.bindValue(":max_timestamp", _max);
  knowDBC::Result r = query.execute();
  return cres_cond<void>(r, "Failed to update frame transformation next information", r.error());
}

bool FrameTransformationRecord::has(const kDB::Repository::QueryConnectionInfo& _connection,
                                    const FrameTransformationBaseValue& _value)
{
  return (FrameTransformationRecord::byParentFrame(_connection, _value.parentFrame())
          and FrameTransformationRecord::byChildFrame(_connection, _value.childFrame())
          and FrameTransformationRecord::byTimestamp(_connection, _value.timestamp()))
           .count()
         > 0;
}
