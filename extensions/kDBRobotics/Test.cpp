#define KDBROBOTICS_NO_QTEST

#include "Test.h"

#include <euclid/extra/curve_value>

#include <Cartography/Algorithms/LawnMowerPattern.h>
#include <Cartography/GeoPoint.h>

#include <knowGIS/GeometryObject.h>

using namespace kDBRobotics;

QList<AgentPositionRecord> kDBRobotics::Test::createLawnMowerTrajectory(
  const kDB::Repository::QueryConnectionInfo& _connection, const QString& _name,
  const QString& _frame, const knowGIS::GeometryObject& _geometry,
  const knowCore::Timestamp& _start_stamp, qreal _min_altitude, qreal _max_altitude, double _speed,
  double _frequency, double _spacing)
{
  QList<AgentPositionRecord> poses;

  Cartography::EuclidSystem::line_string trajectory = Cartography::Algorithms::lawnMowerPattern(
    _geometry.geometry().cast<Cartography::EuclidSystem::polygon>(), {_spacing});

  double trajectory_length = euclid::ops::length(trajectory);
  quint64 trajectory_t = 0;
  quint64 trajectory_dt = 1000000000ull / _frequency;
  euclid::simple::point previous_point(0, 0);
  AgentPositionRecord previous_pose = (AgentPositionRecord::byName(_connection, _name)
                                       and AgentPositionRecord::byFrame(_connection, _frame))
                                        .orderByTimestamp(Sql::Sort::DESC)
                                        .first();
  if(previous_pose.isPersistent())
  {
    clog_assert(previous_pose.timestamp() < _start_stamp);
  }
  double next_dist;

  while((next_dist = trajectory_t * _speed * 1e-9) < trajectory_length)
  {
    euclid::simple::point next_point = euclid::simple::set_z(
      euclid::extra::curve_value(trajectory, next_dist),
      _min_altitude + next_dist * (_max_altitude - _min_altitude) / trajectory_length);

    Cartography::GeoPoint geo_point
      = Cartography::GeoPoint::from(Cartography::Point(next_point, _geometry.coordinateSystem()));

    double yaw
      = std::atan2(next_point.y() - previous_point.y(), next_point.x() - previous_point.x());
    knowCore::Vector4d rotation(0, 0, sin(0.5 * yaw), cos(0.5 * yaw));
    AgentPositionRecord next_pose
      = AgentPositionRecord::create(
          _connection, _name, _frame,
          _start_stamp + knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(trajectory_t),
          geo_point.longitude(), geo_point.latitude(), geo_point.altitude(), rotation)
          .expect_success();
    poses.append(next_pose);

    if(previous_pose.isPersistent())
    {
      previous_pose.setNextPosition(next_pose);
      previous_pose.setNextTimestamp(next_pose.timestamp());
      previous_pose.record();
    }

    previous_point = next_point;
    previous_pose = next_pose;
    trajectory_t += trajectory_dt;
  }

  return poses;
}
