-- 
CREATE TYPE kdb_quaternion AS (
  x double precision,
  y double precision,
  z double precision,
  w double precision
);

CREATE OR REPLACE FUNCTION kdb_quaternion_create(x double precision, y double precision, z double precision, w double precision) RETURNS kdb_quaternion AS $$
  SELECT ROW(x, y, z, w)::kdb_quaternion
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdb_quaternion_create(quat float8[4]) RETURNS kdb_quaternion AS $$
  SELECT ROW(quat[0], quat[1], quat[2], quat[3])::kdb_quaternion
$$ LANGUAGE SQL IMMUTABLE;

CREATE TYPE kdb_geopose AS (
  position kdb_geopoint,
  rotation kdb_quaternion
);

CREATE OR REPLACE FUNCTION kdb_geopose_create(position__ kdb_geopoint, rotation kdb_quaternion) RETURNS kdb_geopose AS $$
  SELECT ROW(position__, rotation)::kdb_geopose
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdb_geopose_create(longitude double precision, latitude double precision, altitude double precision, quat float8[4]) RETURNS kdb_geopose AS $$
  SELECT ROW(kdb_geopoint_create(longitude, latitude, altitude), kdb_quaternion_create(quat))::kdb_geopose
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdb_geopose_create(longitude double precision, latitude double precision, altitude double precision, x double precision, y double precision, z double precision, w double precision) RETURNS kdb_geopose AS $$
  SELECT ROW(kdb_geopoint_create(longitude, latitude, altitude), kdb_quaternion_create(x, y, z, w))::kdb_geopose
$$ LANGUAGE SQL IMMUTABLE;


CREATE TYPE kdb_pose AS (
  position kdb_point,
  rotation kdb_quaternion
);

CREATE OR REPLACE FUNCTION kdb_pose_create(position__ kdb_point, rotation kdb_quaternion) RETURNS kdb_pose AS $$
  SELECT ROW(position__, rotation)::kdb_pose
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdb_pose_create(x double precision, y double precision, z double precision, quat float8[4]) RETURNS kdb_pose AS $$
  SELECT ROW(kdb_point_create(x, y, z, 0), kdb_quaternion_create(quat))::kdb_pose
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdb_pose_create(x double precision, y double precision, z double precision, qx double precision, qy double precision, qz double precision, qw double precision) RETURNS kdb_pose AS $$
  SELECT ROW(kdb_point_create(x, y, z, 0), kdb_quaternion_create(qx, qy, qz, qw))::kdb_pose
$$ LANGUAGE SQL IMMUTABLE;
