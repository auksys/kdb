#include <QSharedPointer>
#include <kDB/Forward.h>

namespace kDBRobotics
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection);
}
