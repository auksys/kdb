#include <Eigen/Geometry>

#include <QExplicitlySharedDataPointer>

#include <kDBRobotics/Forward.h>

namespace kDBRobotics
{
  class FramePose
  {
  public:
    FramePose();
    FramePose(const kDB::Repository::Connection& _connection);
    FramePose(const FramePose& _rhs);
    FramePose& operator=(const FramePose& _rhs);
    ~FramePose();
    cres_qresult<knowGIS::GeoPose> frameGeoPose(const QString& _frame_id,
                                                const knowCore::Timestamp& _timestamp);
    cres_qresult<knowGIS::Pose> framePose(const QString& _frame_id,
                                          const knowCore::Timestamp& _timestamp,
                                          const Cartography::CoordinateSystem& _coordinateSystem);
    cres_qresult<Cartography::CoordinateSystem>
      framePose(Eigen::Vector3d* _translation, Eigen::Quaterniond* _rotation,
                const QString& _frame_id, const knowCore::Timestamp& _timestamp,
                const Cartography::CoordinateSystem& _coordinateSystem);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace kDBRobotics
