#include <clog_qt>
#include <cres_qt>

namespace kDBRuby
{
  class VM
  {
  public:
    static cres_qresult<void> ensureStarted();
    static cres_qresult<void> stop();
    static cres_qresult<void> require(const QString& _module);
    static cres_qresult<void> executeFile(const QString&);
    static cres_qresult<void> execute(const std::function<void()>&);
  private:
    struct Private;
    static Private* d;
  };
} // namespace kDBRuby
