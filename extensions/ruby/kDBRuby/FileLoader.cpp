#include "FileLoader.h"

#include <rice/rice.hpp>

#undef int128_t
#undef uint128_t
#undef isfinite

#include <clog_qt>
#include <cres_qt>

#include <kDB/Repository/Connection.h>

#include "VM.h"

using namespace kDBRuby;

FileLoader::FileLoader() {}

FileLoader::~FileLoader() {}

bool FileLoader::canLoad(const QString& _path, const QString& _mime, const knowCore::ValueHash&)
{
  return _mime == "application/x-ruby" or _path.endsWith(".rb");
}

cres_qresult<QStringList> FileLoader::loadFile(const QString& _path,
                                               const kDB::Repository::Connection& _connection,
                                               const knowCore::ValueHash&)
{
  cres_try(cres_ignore, VM::ensureStarted());
  cres_try(cres_ignore, VM::require("kDB/Repository"));

  VM::execute(
    [_connection]() {
      Rice::define_global_function("kDB_get_connection", [_connection]() { return _connection; });
    });

  cres_try(cres_ignore, VM::executeFile(_path));

  return cres_success(QStringList());
}

#include <kDBBaseKnowledge/Manager.h>

KDB_REGISTER_BASE_KNOWLEDGE_FILE_LOADER(kDBRuby::FileLoader)
