#include <kDBBaseKnowledge/Interfaces/FileLoader.h>

namespace kDBRuby
{
  class FileLoader : public kDBBaseKnowledge::Interfaces::FileLoader
  {
  public:
    FileLoader();
    virtual ~FileLoader();
    bool canLoad(const QString& _path, const QString& _mime_type,
                 const knowCore::ValueHash& _options) override;
    cres_qresult<QStringList> loadFile(const QString& _path,
                                       const kDB::Repository::Connection& _connection,
                                       const knowCore::ValueHash& _options) override;
  };
} // namespace kDBRuby