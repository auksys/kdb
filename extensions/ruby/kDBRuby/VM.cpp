#include "VM.h"

#include <ruby.h>

#include <QMutex>

#include <QFuture>
#include <QThread>
#include <QWaitCondition>

#include <knowCore/ruby/qt.h>

using namespace kDBRuby;

namespace
{
  struct RbExecRequest
  {
    QString require;
    QString file;
    std::function<void()> f;
    QFutureInterface<cres_qresult<void>> promise;
  };
} // namespace

struct VM::Private : public QThread
{
  QMutex mutex;
  QList<RbExecRequest*> requests;
  QWaitCondition cv;
  bool started = false;
  cres_qresult<void> addRequest(RbExecRequest*);
  virtual void run();
};

cres_qresult<void> VM::Private::addRequest(RbExecRequest* _req)
{
  QMutexLocker l(&mutex);
  _req->promise.reportStarted();
  d->requests.append(_req);
  d->cv.notify_one();
  l.unlock();
  QFuture<cres_qresult<void>> future = _req->promise.future();
  future.waitForFinished();
  return future.result();
}

void VM::Private::run()
{
  QMutexLocker l(&d->mutex);
  ruby_init();
  QByteArray opts_0{"kDBRuby"};
  QByteArray opts_1{"-e 1"};
  char* opts[] = {opts_0.begin(), opts_1.begin()};
  ruby_options(2, opts);

  while(started)
  {
    if(requests.isEmpty())
    {
      cv.wait(&mutex);
    }
    else
    {
      RbExecRequest* req1 = requests.takeFirst();
      try
      {
        if(not req1->require.isEmpty())
        {
          Rice::detail::protect(rb_require, qPrintable(req1->require));
        }
        if(req1->f)
        {
          req1->f();
        }
        if(not req1->file.isEmpty())
        {
          Rice::detail::protect(rb_load, Rice::detail::To_Ruby<QString>().convert(req1->file), 0);
        }
        req1->promise.reportResult(cres_success());
      }
      catch(const Rice::Exception& e)
      {
        req1->promise.reportResult(cres_failure("Error occured while running ruby: {}", e.what()));
      }
      req1->promise.reportFinished();
    }
  }
  if(ruby_cleanup(0) != 0)
  {
    clog_error("Failed to stop ruby VM.");
  }
}

VM::Private* VM::d = new VM::Private;

cres_qresult<void> VM::ensureStarted()
{
  QMutexLocker l(&d->mutex);
  if(not d->started)
  {
    d->start();
    d->started = true;
  }
  return cres_success();
}

cres_qresult<void> VM::stop()
{
  QMutexLocker l(&d->mutex);
  d->started = false;
  d->cv.notify_one();
  return cres_success();
}

cres_qresult<void> VM::require(const QString& _module)
{
  RbExecRequest req;
  req.require = _module;
  return d->addRequest(&req);
}

cres_qresult<void> VM::executeFile(const QString& _file)
{
  RbExecRequest req;
  req.file = _file;
  return d->addRequest(&req);
}

cres_qresult<void> VM::execute(const std::function<void()>& _f)
{
  RbExecRequest req;
  req.f = _f;
  return d->addRequest(&req);
}
