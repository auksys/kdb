#include <kDB/Repository/QueryConnectionInfo.h>

namespace kDBRuby
{
  cres_qresult<void> initialise(const kDB::Repository::Connection&) { return cres_success(); }
} // namespace kDBRuby

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBRuby)
