#include <knowCore/ruby/knowCore.h>

// Cleanup some ruby defines that clashes with fmt
#undef isfinite
#undef int128_t
#undef uint128_t

#include <QDir>

#include <knowDBC/Query.h>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/DatasetsUnion.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/RDFEnvironment.h>
#include <kDB/Repository/Store.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/TripleStreamInserter.h>
#include <kDB/Repository/krQuery/Engine.h>

#include "overload.h"

namespace Rice::detail
{
  template<>
  class WrapperPointer<kDB::Repository::GraphsManager> : public Rice::detail::Wrapper
  {
  public:
    WrapperPointer(kDB::Repository::GraphsManager* data, bool isOwner) : data_(data)
    {
      clog_assert(isOwner == false);
    }

    ~WrapperPointer() {}

    void* get() override { return (void*)this->data_; }
  private:
    const kDB::Repository::GraphsManager* data_ = nullptr;
  };
} // namespace Rice::detail

namespace
{
  Rice::Data_Type<kDB::Repository::Transaction> rb_cTransaction;

  VALUE get_value(VALUE val) { return val; }
  template<typename _T_>
  VALUE get_value(const _T_& val)
  {
    return val.value();
  }

  template<typename _TC_>
  bool insert_triples(kDB::Repository::TripleStore* _store,
                      const kDB::Repository::Transaction& _transaction,
                      const knowRDF::Subject& _subject, int _index, int _argc, _TC_ _argv)
  {
    if((_argc - _index) % 2 != 0)
    {
      rb_exc_raise(rb_exc_new2(
        rb_eArgError,
        std::format(
          "Invalid number of arguments, got {} expected an even number of predicate/objects.",
          _argc)
          .c_str()));
      return false;
    }
    for(int i = 0; i < (_argc - _index) / 2; ++i)
    {
      knowCore::Uri predicate = Rice::detail::cpp_protect(
        [&] {
          return Rice::detail::From_Ruby<knowCore::Uri>().convert(get_value(_argv[2 * i + _index]));
        });
      VALUE h_object = get_value(_argv[2 * i + 1 + _index]);

      if(rb_type(h_object) == RUBY_T_ARRAY)
      {
        knowRDF::BlankNode bn;
        cres_qresult<void> rv = _store->insert(_subject, predicate, bn, _transaction);
        if(not rv.is_successful())
        {
          rb_exc_raise(rb_exc_new2(rb_eArgError, qPrintable(rv.get_error().get_message())));
          return false;
        }
        Rice::Array array(h_object);
        if(not insert_triples(_store, _transaction, bn, 0, array.size(), array))
        {
          return false;
        }
      }
      else
      {
        knowRDF::Object object;
        if(Rice::detail::From_Ruby<knowCore::Uri>().is_convertible(h_object)
           != Rice::detail::Convertible::None)
          object = Rice::detail::cpp_protect(
            [&] { return Rice::detail::From_Ruby<knowCore::Uri>().convert(h_object); });
        else if(Rice::detail::From_Ruby<knowRDF::BlankNode>().is_convertible(h_object)
                != Rice::detail::Convertible::None)
          object = Rice::detail::cpp_protect(
            [&] { return Rice::detail::From_Ruby<knowRDF::BlankNode>().convert(h_object); });
        else if(Rice::detail::From_Ruby<knowRDF::Object>().is_convertible(h_object)
                != Rice::detail::Convertible::None)
          object = Rice::detail::cpp_protect(
            [&] { return Rice::detail::From_Ruby<knowRDF::Object>().convert(h_object); });
        else
        {
          try
          {
            object = knowRDF::Literal::fromValue(
              Rice::detail::From_Ruby<knowCore::Value>().convert(h_object));
          }
          catch(...)
          {
            rb_exc_raise(
              rb_exc_new2(rb_eArgError, std::format("Failed to convert {} to knowRDF::Object.",
                                                    Rice::Object(h_object).to_s().str())
                                          .c_str()));
            return Qnil;
          }
        }
        cres_qresult<void> rv = _store->insert(_subject, predicate, object, _transaction);
        if(not rv.is_successful())
        {
          rb_exc_raise(rb_exc_new2(rb_eArgError, qPrintable(rv.get_error().get_message())));
          return false;
        }
      }
    }
    return true;
  }

  VALUE triple_store_insert(int argc, VALUE* argv, VALUE self)
  {
    kDB::Repository::TripleStore* self_ts = Rice::detail::cpp_protect(
      [&] { return Rice::detail::From_Ruby<kDB::Repository::TripleStore*>().convert(self); });

    if(argc < 3)
    {
      rb_exc_raise(rb_exc_new2(
        rb_eArgError,
        std::format("Invalid number of arguments, got {} expected at least 3.", argc).c_str()));
      return Qnil;
    }
    // Check if first argument is a transaction
    int offset = 0;
    kDB::Repository::Transaction transaction;
    bool commit_transaction = false;
    if(Rice::detail::From_Ruby<kDB::Repository::Transaction>().is_convertible(argv[0])
       != Rice::detail::Convertible::None)
    {
      transaction = Rice::detail::cpp_protect(
        [&] { return Rice::detail::From_Ruby<kDB::Repository::Transaction>().convert(argv[0]); });
      ++offset;
    }
    else
    {
      commit_transaction = true;
      transaction = kDB::Repository::Transaction(self_ts->connection());
    }

    // Check the number of arguments
    if((argc - offset) % 2 != 1)
    {
      rb_exc_raise(rb_exc_new2(
        rb_eArgError,
        std::format(
          "Invalid number of arguments, got {} expected an even number of predicate/objects.", argc)
          .c_str()));
      return Qnil;
    }

    // Construct the subject
    knowRDF::Subject subject;
    VALUE subject_value = argv[offset];
    if(Rice::detail::From_Ruby<knowCore::Uri>().is_convertible(subject_value)
       != Rice::detail::Convertible::None)
      subject = Rice::detail::cpp_protect(
        [&] { return Rice::detail::From_Ruby<knowCore::Uri>().convert(subject_value); });
    else if(Rice::detail::From_Ruby<knowRDF::BlankNode>().is_convertible(subject_value)
            != Rice::detail::Convertible::None)
      subject = Rice::detail::cpp_protect(
        [&] { return Rice::detail::From_Ruby<knowRDF::BlankNode>().convert(subject_value); });
    else if(Rice::detail::From_Ruby<knowRDF::Subject>().is_convertible(subject_value)
            != Rice::detail::Convertible::None)
      subject = Rice::detail::cpp_protect(
        [&] { return Rice::detail::From_Ruby<knowRDF::Subject>().convert(subject_value); });
    else
    {
      rb_exc_raise(
        rb_exc_new2(rb_eArgError, std::format("Failed to convert {} to knowRDF::Subject.",
                                              Rice::Object(subject_value).to_s().str())
                                    .c_str()));
      return Qnil;
    }

    if(insert_triples(self_ts, transaction, subject, 1 + offset, argc, argv))
    {
      if(commit_transaction)
      {
        transaction.commit();
      }
    }
    else
    {
      transaction.rollback();
    }
    return Qnil;
  }
} // namespace

extern "C" void Init_kdb_repository_bindings_ruby()
{
  Rice::Module rb_mkDB = Rice::define_module("KDB");
  Rice::Module rb_mkDBRepository = Rice::define_module_under(rb_mkDB, "Repository");

  namespace kR = kDB::Repository;

  Rice::define_class_under<kR::RDFDataset>(rb_mkDBRepository, "RDFDataset");
  rb_cTransaction = Rice::define_class_under<kR::Transaction>(rb_mkDBRepository, "Transaction");

  Rice::Data_Type<kR::TripleStore> rb_cTripleStore
    = Rice::define_class_under<kR::TripleStore, kR::RDFDataset>(rb_mkDBRepository, "TripleStore")
        .define_method("triples", &kR::TripleStore::triples);
  rb_define_method(rb_cTripleStore.value(), "insert", (RUBY_METHOD_FUNC)&triple_store_insert, -1);

  Rice::define_class_under<kR::DatasetsUnion, kR::RDFDataset>(rb_mkDBRepository, "DatasetsUnion")
    .define_constructor(Rice::Constructor<kR::DatasetsUnion>())
    .define_method("add", overload::overload_r_c<void, kR::DatasetsUnion, const kR::RDFDataset&>(
                            &kR::DatasetsUnion::add));

  Rice::Data_Type<kR::GraphsManager> rb_cGraphsManager
    = Rice::define_class_under<kR::GraphsManager>(rb_mkDBRepository, "GraphsManager")
        .define_method("createTripleStore", &kR::GraphsManager::createTripleStore,
                       Rice::Arg("_uri"), Rice::Arg("_transaction") = kR::Transaction())
        .define_method("getOrCreateTripleStore", &kR::GraphsManager::getOrCreateTripleStore)
        .define_method("getTripleStore", &kR::GraphsManager::getTripleStore)
        .define_method("removeTripleStore", [](kR::GraphsManager* self, const knowCore::Uri& _name)
                       { return self->removeTripleStore(_name); });

  Rice::Data_Type<kR::RDFEnvironment> rb_cRDFEnvironment
    = Rice::define_class_under<kR::RDFEnvironment>(rb_mkDBRepository, "RDFEnvironment")
        .define_constructor(Rice::Constructor<kR::RDFEnvironment, kR::RDFDataset>(),
                            Rice::Arg("_dataset") = kR::RDFDataset())
        .define_method("base", &kDB::Repository::RDFEnvironment::base)
        .define_method("defaultDataset", &kDB::Repository::RDFEnvironment::defaultDataset)
        .define_method("namedDatasets", &kDB::Repository::RDFEnvironment::namedDatasets)
        .define_method("isValid", &kDB::Repository::RDFEnvironment::isValid)
        .define_method("setBase", &kDB::Repository::RDFEnvironment::setBase)
        .define_method("addNamedDataset", &kDB::Repository::RDFEnvironment::addNamedDataset)
        .define_method("setDefaultDataset", &kDB::Repository::RDFEnvironment::setDefaultDataset);

  Rice::define_class_under<kDB::Repository::krQuery::Engine>(rb_mkDBRepository, "Engine")
    .define_method("execute",
                   overload::overload<const QString&>(&kDB::Repository::krQuery::Engine::execute));

  Rice::Data_Type<kR::Connection> rb_cConnection
    = Rice::define_class_under<kR::Connection>(rb_mkDBRepository, "Connection")
        .define_constructor(Rice::Constructor<kR::Connection>())
        .define_singleton_function(
          "createFromHostname", [](const QString& _hostname, int _port, const QString& _database)
          { return kR::Connection::create(kR::HostName(_hostname), _port, _database); },
          Rice::Arg("host"), Rice::Arg("port"), Rice::Arg("database") = "kDB"_kCs)
        .define_singleton_function(
          "createFromName",
          [](const QString& _section, const QString& _name, int _port, const QString& _database)
          {
            return kR::Connection::create(kR::SectionName(_section), kR::StoreName(_name), _port,
                                          _database);
          },
          Rice::Arg("section"), Rice::Arg("name"), Rice::Arg("port"),
          Rice::Arg("database") = "kDB"_kCs)
        .define_method("serverUri", &kR::Connection::serverUri)
        .define_method("valid?", &kR::Connection::isValid)
        .define_method("enableExtension", &kR::Connection::enableExtension)
        .define_method("connect", &kR::Connection::connect,
                       Rice::Arg("_initialise_database") = false)
        .define_method("disconnect", &kR::Connection::disconnect)
        .define_method("connected?", &kR::Connection::isConnected)
        .define_method("graphsManager", &kR::Connection::graphsManager)
        .define_method("krQueryEngine", &kDB::Repository::Connection::krQueryEngine)
        .define_method("createSQLQuery", &kR::Connection::createSQLQuery,
                       Rice::Arg("_query") = QString(),
                       Rice::Arg("_bindings") = knowCore::ValueHash(),
                       Rice::Arg("_options") = knowCore::ValueHash())
        .define_method("createSPARQLQuery", &kR::Connection::createSPARQLQuery,
                       Rice::Arg("_rdf_environment") = kR::RDFEnvironment(),
                       Rice::Arg("_query") = QString(),
                       Rice::Arg("_bindings") = knowCore::ValueHash(),
                       Rice::Arg("_options") = knowCore::ValueHash());

  rb_cTransaction.define_constructor(Rice::Constructor<kR::Transaction, kR::Connection>())
    .define_method("commit", &kR::Transaction::commit);

  Rice::Data_Type<kR::Store> rb_cStore
    = Rice::define_class_under<kR::Store>(rb_mkDBRepository, "Store")
        .define_constructor(Rice::Constructor<kR::Store, QString, int>(), Rice::Arg("_directory"),
                            Rice::Arg("_port") = 1242)
        .define_method("start", &kR::Store::start)
        .define_method("stop", &kR::Store::stop, Rice::Arg("_force") = false)
        .define_method("createConnection", &kR::Store::createConnection);

  Rice::Data_Type<kR::TemporaryStore> rb_cTemporaryStore
    = Rice::define_class_under<kR::TemporaryStore>(rb_mkDBRepository, "TemporaryStore")
        .define_constructor(Rice::Constructor<kR::TemporaryStore>())
        .define_method("start", &kR::TemporaryStore::start)
        .define_method("stop", &kR::TemporaryStore::stop, Rice::Arg("_force") = false)
        .define_method("createConnection", &kR::TemporaryStore::createConnection);

  Rice::Data_Type<kR::TripleStreamInserter> rb_cTripleStreamInserter
    = Rice::define_class_under<kR::TripleStreamInserter, knowRDF::TripleStreamListener>(
        rb_mkDBRepository, "TripleStreamInserter")
        .define_constructor(
          Rice::Constructor<kR::TripleStreamInserter, kDB::Repository::TripleStore>());
}
