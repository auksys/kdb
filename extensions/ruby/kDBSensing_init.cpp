#include <knowCore/ruby/knowCore.h>

#include <knowCore/Timestamp.h>

#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/TripleStore.h>

#include <kDBSensing/SalientRegions/Collection.h>
#include <kDBSensing/SalientRegions/SalientRegion.h>

#include "overload.h"

extern "C" void Init_kdb_sensing_bindings_ruby()
{
  Rice::Module rb_mkDBSensing = Rice::define_module("KDBSensing");
  Rice::Module rb_mkDBSensingSalientRegions
    = Rice::define_module_under(rb_mkDBSensing, "SalientRegions");

  namespace kS = kDBSensing;
  namespace kSSR = kDBSensing::SalientRegions;

  Rice::Data_Type<kSSR::SalientRegion> rb_cSalientRegion
    = Rice::define_class_under<kSSR::SalientRegion>(rb_mkDBSensingSalientRegions, "SalientRegion")
        .define_method("exists", &kSSR::SalientRegion::exists)
        .define_method("uri", &kSSR::SalientRegion::uri)
        .define_method("type", &kSSR::SalientRegion::type)
        .define_method("hasProperty", &kSSR::SalientRegion::hasProperty)
        .define_method("property",
                       overload::overload_r_c<cres_qresult<knowCore::Value>, kSSR::SalientRegion,
                                              const knowCore::Uri&>(&kSSR::SalientRegion::property))
        .define_method(
          "setProperty",
          overload::overload_r_c<cres_qresult<void>, kSSR::SalientRegion, const knowCore::Uri&,
                                 const knowCore::Value&>(&kSSR::SalientRegion::setProperty))
        .define_method("geometry", &kSSR::SalientRegion::geometry);

  typedef cres_qresult<kSSR::Collection> (*dssCreate)(
    const kDB::Repository::Connection& _connection, const knowCore::Uri& _graph);

  Rice::Data_Type<kSSR::Collection> rb_cCollection
    = Rice::define_class_under<kSSR::Collection>(rb_mkDBSensingSalientRegions, "Collection")
        .define_singleton_function("get", &kSSR::Collection::get)
        .define_singleton_function("create", dssCreate(&kSSR::Collection::create))
        .define_singleton_function("getOrCreate", &kSSR::Collection::getOrCreate)
        .define_singleton_function("get", &kSSR::Collection::getOrCreate)
        .define_method(
          "createSalientRegion",
          [](kSSR::Collection* _self, const knowGIS::GeometryObject& _geometry,
             const knowCore::Timestamp& _timestamp, const QList<knowCore::Uri>& _klasses,
             const knowCore::ValueHash& _properties, const knowCore::Uri& _salientregionUri)
          {
            return _self->createSalientRegion(_geometry, _timestamp, _klasses, _properties,
                                              _salientregionUri.isEmpty()
                                                ? knowCore::Uri::createUnique({"salientregion"})
                                                : _salientregionUri);
          },
          Rice::Arg("_typeUri"), Rice::Arg("_geometry"), Rice::Arg("_klasses"),
          Rice::Arg("_properties"), Rice::Arg("_datasetUri") = knowCore::Uri())
        .define_method("salientRegion", &kSSR::Collection::salientRegion)
        .define_method("valid?", &kSSR::Collection::isValid);
}
