#include <knowCore/ruby/knowCore.h>

#include <kDB/Repository/Connection.h>

#include <kDBRobotics/Agents/Agent.h>
#include <kDBRobotics/Agents/Collection.h>
#include <kDBRobotics/Agents/Stream.h>

extern "C" void Init_kdb_robotics_bindings_ruby()
{
  Rice::Module rb_mkDBRobotics = Rice::define_module("KDBRobotics");
  Rice::Module rb_mkDBRoboticsAgents = Rice::define_module_under(rb_mkDBRobotics, "Agents");
  namespace kR = kDBRobotics;
  namespace kRA = kR::Agents;

  typedef cres_qresult<void> (kRA::Agent::*agSetProperty)(const knowCore::Uri&,
                                                          const knowCore::Value&) const;

  Rice::Data_Type<kRA::Stream> rb_cStream
    = Rice::define_class_under<kRA::Stream>(rb_mkDBRoboticsAgents, "Stream")
        .define_method("uri", &kRA::Agent::uri)
        .define_method("type", &kRA::Agent::type);
  Rice::Data_Type<kRA::Agent> rb_cAgent
    = Rice::define_class_under<kRA::Agent>(rb_mkDBRoboticsAgents, "Agent")
        .define_method("uri", &kRA::Agent::uri)
        .define_method("type", &kRA::Agent::type)
        .define_method("addStream", &kRA::Agent::addStream)
        .define_method("streams", &kRA::Agent::streams)
        .define_method("setProperty", agSetProperty(&kRA::Agent::setProperty));
  Rice::Data_Type<kRA::Collection> rb_cAgents
    = Rice::define_class_under<kRA::Collection>(rb_mkDBRoboticsAgents, "Collection")
        .define_singleton_function("get", &kRA::Collection::get)
        .define_singleton_function("create", &kRA::Collection::create)
        .define_singleton_function("getOrCreate", &kRA::Collection::getOrCreate)
        .define_singleton_function("get", &kRA::Collection::getOrCreate)
        .define_method("createAgent", &kRA::Collection::createAgent, Rice::Arg("_typeUri"),
                       Rice::Arg("_name"), Rice::Arg("_frame_name"),
                       Rice::Arg("_agentUri") = knowCore::Uri::createUnique({"agent"}))
        .define_method("createStream", &kRA::Collection::createStream, Rice::Arg("_typeUri"),
                       Rice::Arg("_identifier"),
                       Rice::Arg("_streamUri") = knowCore::Uri::createUnique({"stream"}))
        .define_method("hasAgent", &kRA::Collection::hasAgent)
        .define_method("hasStream", &kRA::Collection::hasStream)
        .define_method("agent", &kRA::Collection::agent)
        .define_method("stream", &kRA::Collection::stream)
        .define_method("valid?", &kRA::Collection::isValid);
}
