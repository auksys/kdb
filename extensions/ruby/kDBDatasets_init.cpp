#include <knowCore/ruby/knowCore.h>

#include <knowCore/Timestamp.h>
#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/TripleStore.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>
#include <kDBDatasets/Statistics.h>

#include "overload.h"

extern "C" void Init_kdb_datasets_bindings_ruby()
{
  Rice::Module rb_mkDBDatasets = Rice::define_module("KDBDatasets");

  namespace kD = kDBDatasets;

  typedef cres_qresult<kD::Collection> (*dssCreate)(const kDB::Repository::Connection& _connection,
                                                    const knowCore::Uri& _graph);
  typedef cres_qresult<void> (kD::Dataset::*dsSetStatus)(kD::Dataset::Status) const;

  Rice::Data_Type<kD::Collection> rb_cCollection
    = Rice::define_class_under<kD::Collection>(rb_mkDBDatasets, "Collection");

  Rice::Data_Type<kD::Dataset> rb_cDataset
    = Rice::define_class_under<kD::Dataset>(rb_mkDBDatasets, "Dataset");

  Rice::Enum<kD::Dataset::Status> rb_eDatasetStatus
    = Rice::define_enum_under<kD::Dataset::Status>("Status", rb_cDataset)
        .define_value("Completed", kD::Dataset::Status::Completed)
        .define_value("InProgress", kD::Dataset::Status::InProgress)
        .define_value("InPreparation", kD::Dataset::Status::InPreparation)
        .define_value("Scheduled", kD::Dataset::Status::Scheduled)
        .define_value("Unknown", kD::Dataset::Status::Unknown);

  rb_cDataset.define_method("status=", dsSetStatus(&kD::Dataset::setStatus))
    .define_method("associate", &kD::Dataset::associate)
    .define_method("exists", &kDBDatasets::Dataset::exists)
    .define_method("uri", &kDBDatasets::Dataset::uri)
    .define_method("type", &kDBDatasets::Dataset::type)
    .define_method("startTime", &kDBDatasets::Dataset::startTime)
    .define_method("endTime", &kDBDatasets::Dataset::endTime)
    .define_method("createdFrom", &kDBDatasets::Dataset::createdFrom)
    .define_method("hasProperty", &kDBDatasets::Dataset::hasProperty)
    .define_method("property",
                   overload::overload_r_c<cres_qresult<knowCore::Value>, kDBDatasets::Dataset,
                                          const knowCore::Uri&>(&kDBDatasets::Dataset::property))
    .define_method(
      "setProperty",
      overload::overload_r_c<cres_qresult<void>, kDBDatasets::Dataset, const knowCore::Uri&,
                             const knowCore::Value&>(&kDBDatasets::Dataset::setProperty))
    .define_method("geometry", &kDBDatasets::Dataset::geometry)
    .define_method("connection", &kDBDatasets::Dataset::connection)
    .define_method("associate", &kDBDatasets::Dataset::associate)
    .define_method("dissociate", &kDBDatasets::Dataset::dissociate)
    .define_method("associatedAgents", &kDBDatasets::Dataset::associatedAgents)
    .define_method("status", &kDBDatasets::Dataset::status)
    .define_method("statusUri", &kDBDatasets::Dataset::statusUri);

  rb_cCollection.define_singleton_function("get", &kD::Collection::get)
    .define_singleton_function("create", dssCreate(&kD::Collection::create))
    .define_singleton_function("getOrCreate", &kD::Collection::getOrCreate)
    .define_singleton_function("get", &kD::Collection::getOrCreate)
    .define_method(
      "createDataset",
      [](kD::Collection* _self, const knowCore::Uri& _typeUri,
         const knowGIS::GeometryObject& _geometry, const knowCore::ValueHash& _properties,
         const knowCore::Uri& _datasetUri)
      {
        return _self->createDataset(_typeUri, _geometry, _properties,
                                    _datasetUri.isEmpty() ? knowCore::Uri::createUnique({"dataset"})
                                                          : _datasetUri);
      },
      Rice::Arg("_typeUri"), Rice::Arg("_geometry"), Rice::Arg("_properties"),
      Rice::Arg("_datasetUri") = knowCore::Uri())
    .define_method("dataset", &kD::Collection::dataset)
    .define_method("valid?", &kD::Collection::isValid);

  Rice::define_class_under<kD::ValueIterator>(rb_mkDBDatasets, "ValueIterator")
    .define_method("next?", &kD::ValueIterator::hasNext)
    .define_method("hasNext", &kD::ValueIterator::hasNext)
    .define_method("next", &kD::ValueIterator::next);

  Rice::define_class_under<kD::Statistics>(rb_mkDBDatasets, "Statistics")
    .define_method("datapointsCount", &kD::Statistics::datapointsCount);

  Rice::Module rb_mDataInterfaceRegistry
    = Rice::define_module_under(rb_mkDBDatasets, "DataInterfaceRegistry")
        .define_singleton_function("createValueIterator",
                                   &kD::DataInterfaceRegistry::createValueIterator)
        .define_singleton_function("statistics", &kD::DataInterfaceRegistry::statistics);
}
