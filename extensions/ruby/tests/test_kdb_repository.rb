require 'kdb-rspec'

require 'kDB/Repository'

RSpec.describe "RDFEnvironment" do
  it "can be created" do
    KDB::Repository::RDFEnvironment.new
  end
end

RSpec.describe "krQueryEngine" do
  it "can be used for debug query" do
    store = KDB::Repository::TemporaryStore.new
    store.start
    c = store.createConnection
    expect(c.krQueryEngine.execute("TEST: { return: 42 }")).to eq 42
  end
end
