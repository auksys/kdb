require 'kdb-rspec'

require 'knowValues'

require 'kDBDatasets'
require 'kDBSensing'

RSpec.describe "SalientRegionBuilder" do
  it "can insert data" do
    store = KDB::Repository::TemporaryStore.new
    store.start()
    c = store.createConnection()
    expect(c.valid?).to be true

    c.connect()
    expect(c.connected?).to be true

    c.enableExtension "kDBSensing"

    go0 = KnowGIS::GeometryObject.fromWKT("POLYGON((16.683875600036536 57.76189821045434,16.681086102661048 57.760765003387945,16.682416478332435 57.75976912541624,16.684680262741004 57.760621919160826,16.685205975707923 57.76117135950369,16.683875600036536 57.76189821045434))");
    go_b0 = KnowGIS::GeometryObject.fromWKT("POLYGON((16.682977084884975 57.76110412623681,16.682663266430232 57.76097034375719,16.682781283626888 57.76089379420113,16.683092419872615 57.76102399989138,16.682977084884975 57.76110412623681))")
    go_b1 = KnowGIS::GeometryObject.fromWKT("POLYGON((16.68405002286436 57.761225677221624,16.68397089769842 57.76127790215757,16.683753638768213 57.76119348373033,16.684168040061014 57.76090588726892,16.68442150881292 57.76101677672292,16.68432226707937 57.76108331023202,16.68423375418188 57.761050401199824,16.68401247193815 57.76120779195222,16.68405002286436 57.761225677221624))")
    go_b2 = KnowGIS::GeometryObject.fromWKT("POLYGON((16.683785824321795 57.76059079724401,16.683486758016635 57.76045057403095,16.683584658645678 57.76039548619127,16.683886407159854 57.7605328479209,16.683785824321795 57.76059079724401))")
    go_p0 = KnowGIS::GeometryObject.fromWKT("POLYGON((16.683191862243195 57.76084918088643,16.683001425403138 57.76077048481496,16.683081891673584 57.76068034183158,16.68335145367958 57.76080410952099,16.683191862243195 57.76084918088643))")
  
    dss = KDBDatasets::Datasets.get c, kCu("http://askco.re/graph#private_datasets")
    expect(dss.valid?).to be true

    sr_dataset_uri = kCu("dataset_0");
    ds = dss.createDataset kCu("http://askco.re/sensing#salient_region"), go0, {}, sr_dataset_uri
  
    sp_collection = KDBSensing::SalientRegions::Collection.create c, sr_dataset_uri

    sp_collection.createSalientRegion(go_b0, KnowCore::Timestamp.now(), [kCu("building")], {});
    sp_collection.createSalientRegion(go_b1, KnowCore::Timestamp.now(), [kCu("building")], {});
    sp_collection.createSalientRegion(go_b2, KnowCore::Timestamp.now(), [kCu("building")], {});
    sp_collection.createSalientRegion(go_p0, KnowCore::Timestamp.now(), [kCu("pool")], {});

    iterator = KDBDatasets::DataInterfaceRegistry.createValueIterator(c, ds)

    expect(iterator.next?).to be true
    val = iterator.next

    expect(val.valid?).to be true
    expect(val).to be_a_kind_of KnowValues::SalientRegion
    expect(val.geometry.toWKT).to eq go_b0.toWKT
    puts(pp(val.classes.sort))
    expect(val.classes.sort).to eq [kCu("building")]

    expect(iterator.next?).to be true
    val = iterator.next
    expect(val.valid?).to be true
    expect(val).to be_a_kind_of KnowValues::SalientRegion
    expect(val.geometry.toWKT).to eq go_b1.toWKT
    expect(val.classes.sort).to eq [kCu("building")]

    expect(iterator.next?).to be true
    val = iterator.next
    expect(val.valid?).to be true
    expect(val).to be_a_kind_of KnowValues::SalientRegion
    expect(val.geometry.toWKT).to eq go_b2.toWKT
    expect(val.classes.sort).to eq [kCu("building")]

    expect(iterator.next?).to be true
    val = iterator.next
    expect(val.valid?).to be true
    expect(val).to be_a_kind_of KnowValues::SalientRegion
    expect(val.geometry.toWKT).to eq go_p0.toWKT
    expect(val.classes.sort).to eq [kCu("pool")]

    expect(iterator.next?).to be false
    
  end
end
