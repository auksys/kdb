// BEGIN OVERLOAD
namespace overload
{
  namespace detail
  {
    template<typename... Args>
    struct non_const_overload
    {
      template<typename R, typename C>
      constexpr auto operator()(R (C::*ptr)(Args...)) const noexcept -> decltype(ptr)
      {
        return ptr;
      }

      template<typename R, typename C>
      static constexpr auto of(R (C::*ptr)(Args...)) noexcept -> decltype(ptr)
      {
        return ptr;
      }
    };

    template<typename... Args>
    struct const_overload
    {
      template<typename R, typename C>
      constexpr auto operator()(R (C::*ptr)(Args...) const) const noexcept -> decltype(ptr)
      {
        return ptr;
      }

      template<typename R, typename C>
      static constexpr auto of(R (C::*ptr)(Args...) const) noexcept -> decltype(ptr)
      {
        return ptr;
      }
    };

    template<typename... Args>
    struct overload : const_overload<Args...>, non_const_overload<Args...>
    {
      using const_overload<Args...>::of;
      using const_overload<Args...>::operator();
      using non_const_overload<Args...>::of;
      using non_const_overload<Args...>::operator();

      template<typename R>
      constexpr auto operator()(R (*ptr)(Args...)) const noexcept -> decltype(ptr)
      {
        return ptr;
      }

      template<typename R>
      static constexpr auto of(R (*ptr)(Args...)) noexcept -> decltype(ptr)
      {
        return ptr;
      }
    };
    template<typename R, typename C, typename... Args>
    struct non_const_overload_r_c
    {
      constexpr auto operator()(R (C::*ptr)(Args...)) const noexcept -> decltype(ptr)
      {
        return ptr;
      }

      static constexpr auto of(R (C::*ptr)(Args...)) noexcept -> decltype(ptr) { return ptr; }
    };

    template<typename R, typename C, typename... Args>
    struct const_overload_r_c
    {
      constexpr auto operator()(R (C::*ptr)(Args...) const) const noexcept -> decltype(ptr)
      {
        return ptr;
      }

      static constexpr auto of(R (C::*ptr)(Args...) const) noexcept -> decltype(ptr) { return ptr; }
    };

    template<typename R, typename C, typename... Args>
    struct overload_r_c : const_overload_r_c<R, C, Args...>, non_const_overload_r_c<R, C, Args...>
    {
      using const_overload_r_c<R, C, Args...>::of;
      using const_overload_r_c<R, C, Args...>::operator();
      using non_const_overload_r_c<R, C, Args...>::of;
      using non_const_overload_r_c<R, C, Args...>::operator();

      constexpr auto operator()(R (*ptr)(Args...)) const noexcept -> decltype(ptr) { return ptr; }

      static constexpr auto of(R (*ptr)(Args...)) noexcept -> decltype(ptr) { return ptr; }
    };

  } // namespace detail

  template<typename... Args>
  constexpr __attribute__((__unused__)) detail::overload_r_c<Args...> overload_r_c = {};
  template<typename... Args>
  constexpr __attribute__((__unused__)) detail::overload<Args...> overload = {};
  template<typename... Args>
  constexpr __attribute__((__unused__)) detail::const_overload<Args...> const_overload = {};
  template<typename... Args>
  constexpr __attribute__((__unused__)) detail::non_const_overload<Args...> non_const_overload = {};
} // namespace overload
// END OVERLOAD