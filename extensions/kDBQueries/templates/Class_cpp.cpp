// clang-format off
#include "/%= file_name %/.h"

#include <QVariant>

#include <knowCore/ValueHash.h>
#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/DatasetsUnion.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TripleStore.h>

using namespace /%
for(const QString& ns : namespaces)
{
  %/::/%= ns %//%
}
%/;

/%
for(const knowRDF::Node* query_node : query_nodes)
{
  const knowRDF::Node* name_node = query_node->getFirstChild(knowCore::Uris::foaf::name);
  const knowRDF::Node* query_query_node = query_node->getFirstChild(knowCore::Uris::askcore_db_queries::query);
  QString klass_name = toCamelCase(name_node->literal().value<QString>().expect_success(), true);
  QList<Stuff> returned_stuffs = generateStuffs(query_node, knowCore::Uris::askcore_db_queries::result);
  QList<Stuff> argumented_stuffs = generateStuffs(query_node, knowCore::Uris::askcore_db_queries::binding);
  bool single_anomymous_result  = returned_stuffs.size() == 1 and returned_stuffs.first().name.isEmpty();
  bool multiple_return          = returned_stuffs.size() > 0;
  bool single_value_return      = queryType(query_query_node->literal().value<QString>().expect_success(), argumented_stuffs) == kDB::SPARQL::Query::Type::Ask;
  
  if(multiple_return and not single_anomymous_result and not single_value_return)
  {
  %/
struct /%= klass_name %/::Result::Private : public QSharedData
{/%
    for(const Stuff& rS : returned_stuffs)
    {
    %/
  /%= rS.type %/ /%= rS.name %/;/%
    }%/
};

/%= klass_name %/::Result::Result() : d(nullptr)
{
}

/%= klass_name %/::Result::Result(const /%= klass_name %/::Result& _rhs) : d(_rhs.d)
{
}

/%= klass_name %/::Result& /%= klass_name %/::Result::operator=(const /%= klass_name %/::Result& _rhs)
{
  d = _rhs.d;
  return *this;
}

/%= klass_name %/::Result::~Result()
{
}

bool /%= klass_name %/::Result::isValid() const
{
  return d;
}

/%
    for(const Stuff& rS : returned_stuffs)
    {%/
/%= rS.type %/ /%= klass_name %/::Result::/%= toCamelCase(rS.name, false) %/() const
{
  return d->/%= rS.name %/;
}

/%
    }
  }
%/

/%= klass_name %/::Results /%= klass_name %/::execute(const kDB::Repository::QueryConnectionInfo& _connectionInfo/%= generateArguments(argumented_stuffs, true) %/)
{
  Results r;
  /%
  QStringList graphnames;
  
  for(const knowRDF::Node* n : query_node->children(knowCore::Uris::askcore_db_queries::graph))
  {
    switch(n->type())
    {
      case knowRDF::Node::Type::Uri:
        graphnames.append(n->uri());
        break;
      case knowRDF::Node::Type::Literal:
        graphnames.append(n->literal().value<QString>().expect_success());
        break;
      default:
        qFatal("unsupported");
    }
  }

  switch(graphnames.size())
  {
    case 0:
      break;
    case 1:
    {
  %/
  kDB::Repository::RDFDataset interface;
  cres_qresult<kDB::Repository::RDFDataset> interface_rv = _connectionInfo.connection().graphsManager()->getDataset(/%= getGraphUri(graphnames.first()) %/);
  if(interface_rv.is_successful())
  {
    interface = interface_rv.get_value();
  } else {
    cres_qresult<kDB::Repository::TripleStore> ts_rv = _connectionInfo.connection().graphsManager()->getOrCreateTripleStore(/%= getGraphUri(graphnames.first()) %/);
    if(ts_rv.is_successful())
    {
      interface = ts_rv.get_value();
    } else {
      r.success = false;
      r.errorMessage = ts_rv.get_error().get_message();
      return r;
    }
  }/%
      break;
    }
    default:
    {
        %/
  kDB::Repository::DatasetsUnion interface;/%
      for(const QString& gn : graphnames)
      {
  %/
  {
    kDB::Repository::RDFDataset subset;
    cres_qresult<kDB::Repository::RDFDataset> subset_rv = _connectionInfo.connection().graphsManager()->getDataset(/%= getGraphUri(gn) %/);
    if(subset_rv.is_successful())
    {
      subset = subset_rv.get_value();
    } else
    {
      cres_qresult<kDB::Repository::TripleStore> ts_rv = _connectionInfo.connection().graphsManager()->getOrCreateTripleStore(/%= getGraphUri(gn) %/);
      if(ts_rv.is_successful())
      {
        subset = ts_rv.get_value();
      } else {
        r.success = false;
        r.errorMessage = ts_rv.get_error().get_message();
        return r;
      }
    }
    interface.add(subset);
  }
        /%
      }
    }
  }
  %/
  knowDBC::Query q = _connectionInfo.createSPARQLQuery(kDB::Repository::RDFEnvironment()/% if(graphnames.size() > 0) { %/.setDefaultDataset(interface) /% } %/,
                                 "/%= escapeQuery(query_query_node->literal().value<QString>().expect_success()) %/"); /%
  
  for(const Stuff& b : argumented_stuffs)
  { %/
  q.bindValue("%/%= b.name %/", _/%= b.name %/);/%
  }
  %/
  
  knowDBC::Result result = q.execute();
  
  if(result)
  {/%
  if(single_value_return)
  {
    %/
    r.data = result.boolean();
    /%
  } else if(single_anomymous_result)
  {
    %/
    clog_assert(result.fields() == 1);
    for(int t = 0; t < result.tuples(); ++t)
    {
      r.data.append(result.value</%= returned_stuffs.first().type %/>(t, 0).expect_success());
    }
    /%
  } else if(multiple_return)
  {
    for(const Stuff& s : returned_stuffs)
    {
      %/
    const int /%= s.name %/_index = result.fieldIndex("/%= s.name %/"); /%
    }
    %/
    for(int t = 0; t < result.tuples(); ++t)
    {
      Result::Private* pd = new Result::Private;
      /%
    for(const Stuff& s : returned_stuffs)
    {
      %/
      pd->/%= s.name %/ = result.value</%= s.type %/>(t, /%= s.name %/_index).expect_success(); /%
    }
    %/
      
      
      Result pp;
      pp.d = pd;
      r.data.append(pp);
    }
    /%
  } %/
    r.success = true;
  } else {
    r.success = false;
    r.errorMessage = result.error();
  }
  
  return r;
}
/%
}
%/
