// clang-format off
#pragma once

#include <QList>
#include <QSharedDataPointer>

/%
  QStringList included_headers;
  for(const knowRDF::Node* query_node : query_nodes)
  {
    for(const Stuff& s : generateStuffs(query_node, knowCore::Uris::askcore_db_queries::result) + generateStuffs(query_node, knowCore::Uris::askcore_db_queries::binding))
    {
      if(not s.header.isEmpty() and not included_headers.contains(s.header))
      {
        included_headers.append(s.header);%/
#include </%= s.header %/>/%
      }
    }
  }

%/


namespace kDB
{
  namespace Repository
  {
    class QueryConnectionInfo;
  }
}

/%
for(const QString& ns : namespaces)
{
  %/
namespace /%= ns %/
{/%
}

for(const knowRDF::Node* query_node : query_nodes)
{
  const knowRDF::Node* name_node = query_node->getFirstChild(knowCore::Uris::foaf::name);
  const knowRDF::Node* query_query_node = query_node->getFirstChild(knowCore::Uris::askcore_db_queries::query);
  QString klass_name = toCamelCase(name_node->literal().value<QString>().expect_success(), true);
  QList<Stuff> returned_stuffs = generateStuffs(query_node, knowCore::Uris::askcore_db_queries::result);
  QList<Stuff> argumented_stuffs = generateStuffs(query_node, knowCore::Uris::askcore_db_queries::binding);
  bool single_anomymous_result  = returned_stuffs.size() == 1 and returned_stuffs.first().name.isEmpty();
  bool multiple_return          = returned_stuffs.size() > 0;
  bool single_value_return      = queryType(query_query_node->literal().value<QString>().expect_success(), argumented_stuffs) == kDB::SPARQL::Query::Type::Ask;
  %/
  class /%= klass_name %/
  {
  public:
/%
    if(single_value_return)
    {
      %/
    typedef /%= returned_stuffs.first().type %/ DataType;/%
    } else if(single_anomymous_result)
    {
      %/
    typedef QList</%= returned_stuffs.first().type %/> DataType;/%
    } else if(multiple_return)
    {
    %/
    class Result
    {
      friend struct /%= klass_name %/;
    public:
      Result();
      Result(const Result& _rhs);
      Result& operator=(const Result& _rhs);
      ~Result();
      bool isValid() const;
      /%
      for(const Stuff& rS : returned_stuffs)
      {%/
      /%= rS.type %/ /%= toCamelCase(rS.name, false) %/() const; /%
      }%/
    private:
      struct Private;
      QSharedDataPointer<Private> d;
    };
    typedef QList<Result> DataType;/%
    
    }
  %/
    struct Results
    {/%
    if(multiple_return or single_anomymous_result or single_value_return)
    {
    %/
      DataType data; /%
    } %/
      bool success;
      QString errorMessage;
    };
  public:
    static Results execute(const kDB::Repository::QueryConnectionInfo& _connectionInfo/%= generateArguments(argumented_stuffs, true) %/);
  };/%
}
for(const QString& ns : namespaces)
{
  Q_UNUSED(ns)
  %/
}/%
}

%/
