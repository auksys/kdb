#include <clog_print>
#include <clog_qt>

#include <QDir>
#include <QFileInfo>
#include <QStringList>
#include <QTextStream>

#include <knowCore/Messages.h>
#include <knowCore/Uri.h>
#include <knowCore/ValueHash.h>

#include <knowRDF/Graph.h>
#include <knowRDF/Literal.h>
#include <knowRDF/Node.h>
#include <knowRDF/TripleStream.h>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/foaf.h>
#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/xsd.h>
#include <knowCore/Uris/xsi.h>

#include <kDB/SPARQL/Query.h>

struct Stuff
{
  QString name, type, argument_type, header;
  bool optional;
};

QString getGraphUri(const QString& _graphname)
{
  if(_graphname[0] == '%')
  {
    return '_' + _graphname.right(_graphname.size() - 1);
  }
  else
  {
    return '"' + _graphname + "\"_kCu";
  }
}

kDB::SPARQL::Query::Type queryType(const QString& _query, const QList<Stuff>& _args)
{
  knowCore::ValueHash fb;
  for(const Stuff& st : _args)
  {
    fb.insert("%" + st.name, "fake"_kCs);
  }

  knowCore::Messages messages;
  QList<kDB::SPARQL::Query> q
    = kDB::SPARQL::Query::parse(_query.toUtf8(), fb, &messages, knowCore::Uri());
  switch(q.size())
  {
  case 0:
  {
    clog_print<clog_print_flag::red>("Query '{}' fails to parse with error: {}", _query,
                                     messages.toString());

    std::exit(-1);
  }
  case 1:
    return q.first().type();
  default:
    for(kDB::SPARQL::Query sq : q)
    {
      if(sq.type() != kDB::SPARQL::Query::Type::Update)
      {
        clog_warning("Only support a single non-update query");
        std::exit(-1);
      }
    }
    return kDB::SPARQL::Query::Type::Update;
  }
}

QString toCamelCase(const QString& _input, bool _first_is_upper)
{
  QString output;
  bool next_is_upper = _first_is_upper;

  for(int i = 0; i < _input.size(); ++i)
  {
    if(_input[i] == '_')
    {
      next_is_upper = true;
    }
    else if(next_is_upper)
    {
      output += _input[i].toUpper();
      next_is_upper = false;
    }
    else
    {
      output += _input[i];
    }
  }

  return output;
}

QString escapeQuery(QString _query)
{
  _query.replace('\"', "\\\"");
  _query.replace('\n', "\\n\"\n\"");
  return _query;
}

#define TTS_GEN(_URI_, _TYPE_, _ARG_TYPE_, _HEADER_)                                               \
  else if(_uri == _URI_)                                                                           \
  {                                                                                                \
    _stuff->type = #_TYPE_;                                                                        \
    _stuff->argument_type = #_ARG_TYPE_;                                                           \
    _stuff->header = _HEADER_;                                                                     \
  }

#define TTS3(_URI_, _TYPE_, _ARG_TYPE_, _HEADER_)                                                  \
  else if(_uri == knowCore::Uris::_URI_)                                                           \
  {                                                                                                \
    _stuff->type = #_TYPE_;                                                                        \
    _stuff->argument_type = #_ARG_TYPE_;                                                           \
    _stuff->header = _HEADER_;                                                                     \
  }

#define TTS_CR(_URI_, _TYPE_, _HEADER_) TTS3(_URI_, _TYPE_, const _TYPE_&, _HEADER_)

#define TTS_NCR(_URI_, _TYPE_, _HEADER_) TTS3(_URI_, _TYPE_, _TYPE_, _HEADER_)

void typeToStuff(Stuff* _stuff, const knowCore::Uri& _uri)
{
  if(false)
  {
  }
  TTS_CR(xsd::boolean, bool, "")
  TTS_CR(xsd::string, QString, "QString")
  TTS_CR(xsd::anyURI, knowCore::Uri, "knowCore/Uri.h")
  TTS_CR(xsd::dateTime, knowCore::Timestamp, "knowCore/Timestamp.h")
  TTS_CR(askcore_datatype::literal, knowRDF::Literal, "knowRDF/Literal.h")
  TTS_GEN("http://www.opengis.net/ont/geosparql#Geometry", knowGIS::GeometryObject,
          const knowGIS::GeometryObject&, "knowGIS/GeometryObject.h")
  else if(knowCore::Uris::isNumericType(_uri))
  {
    _stuff->type = "knowCore::BigNumber";
    _stuff->argument_type = "const knowCore::BigNumber&";
    _stuff->header = "knowCore/BigNumber.h";
  }
  else
  {
    qFatal("Unsupported type: %s", qPrintable(_uri));
  }
}

QList<Stuff> generateStuffs(const knowRDF::Node* _node, const knowCore::Uri& _uri)
{
  QList<Stuff> stuffs;
  for(const knowRDF::Node* n : _node->children(_uri))
  {
    Stuff s;
    s.optional = false;
    for(const knowRDF::Node* node : n->children(knowCore::Uris::rdf::a))
    {
      if(node->uri() == knowCore::Uris::askcore_db_queries::optional)
      {
        s.optional = true;
      }
    }
    const knowRDF::Node* name = n->getFirstChild(knowCore::Uris::foaf::name);
    const knowRDF::Node* type = n->getFirstChild(knowCore::Uris::xsi::type);
    if(name)
    {
      s.name = name->literal().value<QString>().expect_success();
    }
    typeToStuff(&s, type->uri());
    if(s.optional)
    {
      s.type = clog_qt::qformat("std::optional<{}>", s.type);
    }
    stuffs.append(s);
  }
  return stuffs;
}

QString generateArguments(const QList<Stuff>& _stuff, bool _start_with_comma)
{
  QString r;
  bool print_comma = _start_with_comma;
  for(const Stuff& s : _stuff)
  {
    if(print_comma)
      r += ", ";
    print_comma = true;
    r += s.argument_type + " _" + s.name;
  }
  return r;
}

int main(int _argc, char** _argv)
{
  if(_argc != 4)
  {
    clog_warning("kDBQueriesGenerator usuage: kDBQueriesGenerator [quries_definition_graph] "
                 "[namespaces_class] [output_prefix]");
    return -1;
  }

  const QString graph_filename = _argv[1];
  QStringList namespaces = QString(_argv[2]).split("::");
  const QString output_prefix = _argv[3];
  const QString file_name = QFileInfo(output_prefix).fileName();

  // namespaces
  if(namespaces.isEmpty())
  {
    clog_warning("Invalid empty namespaces_class!");
    return -1;
  }

  // Parse the test file
  knowCore::Uri graphfilenameUri(graph_filename);

  QFile testfile(graph_filename);

  if(not testfile.open(QIODevice::ReadOnly))
  {
    clog_warning("Failed to open '{}'", graph_filename);
    return -1;
  }

  knowRDF::TripleStream testfilestream;
  testfilestream.setBase(graphfilenameUri.base());
  knowRDF::Graph graph;
  testfilestream.addListener(&graph);

  cres_qresult<void> testfilestream_read = testfilestream.start(&testfile, nullptr);
  if(not testfilestream_read.is_successful())
  {
    clog_warning("Failed to parse: '{}': {}", graph_filename, testfilestream_read.get_error());
    return -1;
  }

  //
  QList<QPair<const knowRDF::Node*, const knowRDF::Node*>> query_nodes_
    = graph.getNodes(knowCore::Uris::askcore_db_queries::query);
  if(query_nodes_.size() == 0)
  {
    clog_warning("No query found!");
    return -1;
  }
  QList<const knowRDF::Node*> query_nodes;
  std::transform(
    query_nodes_.begin(), query_nodes_.end(), std::front_inserter(query_nodes),
    std::bind(&QPair<const knowRDF::Node*, const knowRDF::Node*>::first, std::placeholders::_1));

  // Create output path
  QDir().mkpath(QFileInfo(output_prefix).path());

  // Validity check
  for(const knowRDF::Node* query_node : query_nodes)
  {
    QList<Stuff> returned_stuffs
      = generateStuffs(query_node, knowCore::Uris::askcore_db_queries::result);
    if(returned_stuffs.size() > 1)
    {
      for(const Stuff& s : returned_stuffs)
      {
        if(s.name.isEmpty())
        {
          clog_warning("Anonymous returns are only allowed for a single return value!");
          return -1;
        }
      }
    }
    QList<Stuff> argumented_stuffs
      = generateStuffs(query_node, knowCore::Uris::askcore_db_queries::binding);
    for(const Stuff& s : argumented_stuffs)
    {
      if(s.name.isEmpty())
      {
        clog_warning("Anonymous arguments are not allowed!");
        return -1;
      }
    }
    const knowRDF::Node* query_query_node
      = query_node->getFirstChild(knowCore::Uris::askcore_db_queries::query);
    if(queryType(query_query_node->literal().value<QString>().expect_success(), argumented_stuffs)
       == kDB::SPARQL::Query::Type::Ask)
    {
      if(returned_stuffs.size() != 1 or returned_stuffs.first().type != "bool"
         or not returned_stuffs.first().name.isEmpty())
      {
        clog_warning("ASK query should return a single anonymous boolean value");
        return -1;
      }
    }
  }
  // Generate header
  {
    QFile header_file(output_prefix + ".h");
    if(not header_file.open(QIODevice::WriteOnly))
    {
      clog_warning("Failed to open file: '{}.h' for writting!", output_prefix);
      return -1;
    }
    QTextStream stream(&header_file);

#include "Class_h.h"
  }

  // Generate sources
  {
    QFile source_file(output_prefix + ".cpp");
    if(not source_file.open(QIODevice::WriteOnly))
    {
      clog_warning("Failed to open file: '{}.cpp' for writting!", output_prefix);
      return -1;
    }
    QTextStream stream(&source_file);

#include "Class_cpp.h"
  }

  return 0;
}
