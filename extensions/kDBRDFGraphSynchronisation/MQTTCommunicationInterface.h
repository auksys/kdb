#include <kDB/Forward.h>

#include "AbstractCommunicationInterface.h"

#include "Forward.h"

namespace kDBRDFGraphSynchronisation
{
  class MQTTCommunicationInterface : public AbstractCommunicationInterface
  {
  public:
    MQTTCommunicationInterface(const QString& _server_address, const QString& _graphname);
    virtual ~MQTTCommunicationInterface();
    void sendStatus(const Status& _status) override;
    void sendVote(const Vote& _vote) override;
    void sendRevisions(const Revisions& _revisions) override;
    void sendRevisionsRequest(const RevisionsRequest& _request) override;

    static SynchronisationManager* startSynchronisation(const QString& _name,
                                                        const QString& _server_address,
                                                        const kDB::Repository::TripleStore& _store);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBRDFGraphSynchronisation
