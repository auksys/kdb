#include "Messages.h"

namespace kDBRDFGraphSynchronisation
{
  class AbstractCommunicationInterface
  {
  public:
    virtual ~AbstractCommunicationInterface();
    virtual void sendStatus(const Status& _status) = 0;
    virtual void sendVote(const Vote& _vote) = 0;
    virtual void sendRevisions(const Revisions& _revision) = 0;
    virtual void sendRevisionsRequest(const RevisionsRequest& _request) = 0;
  };
} // namespace kDBRDFGraphSynchronisation
