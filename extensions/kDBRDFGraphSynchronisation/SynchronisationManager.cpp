#include "SynchronisationManager.h"

#include <random>

#include <QThread>
#include <QTimer>

#include <clog_chrono>

#include <Cyqlops/Crypto/RSAAlgorithm.h>

#include <clog_qt>
#include <knowCore/AsynchronousFunction.h>
#include <knowCore/CryptographicHash.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/Utils.h>

#include <kDB/Repository/VersionControl/Delta.h>
#include <kDB/Repository/VersionControl/Manager.h>
#include <kDB/Repository/VersionControl/Revision.h>
#include <kDB/Repository/VersionControl/RevisionBuilder.h>
#include <kDB/Repository/VersionControl/Signature.h>

#include "AbstractCommunicationInterface.h"

using namespace kDBRDFGraphSynchronisation;

constexpr int STATUS_PULSE_TIME = 5; // s
constexpr std::chrono::seconds STATUS_DISCONNECTED_TIME
  = std::chrono::seconds(3 * STATUS_PULSE_TIME + 1); // s
constexpr std::chrono::seconds ELECTION_GRACE_PERIOD
  = std::chrono::seconds(STATUS_PULSE_TIME * 2 + 1);                               // s
constexpr std::chrono::seconds ELECTION_LENGTH = std::chrono::seconds(10);         // s
constexpr std::chrono::seconds RELIABILITY_DURATION = std::chrono::seconds(120);   // s
constexpr std::chrono::seconds REVISIONS_PUBLISH_DELAY = std::chrono::seconds(20); // s
constexpr double RELIABILITY = 0.8;

clog_format_declare_formatter(kDB::Repository::VersionControl::Revision)
{
  return std::format_to(ctx.out(), "{}", p.hash());
}

namespace
{
  enum SynchronisationManagerVersion
  {
    v1,
    v2
  };
}

namespace
{
  template<knowCore::FixedString fname, typename _T_>
  void log_error(const cres_qresult<_T_>& _error)
  {
    if(not _error.is_successful())
    {
      clog_error("An error occured in {}: {}", fname.value, _error.get_error());
    }
  }
} // namespace

namespace kDBRDFGraphSynchronisation
{
  struct OtherNodeInfo
  {
    QUuid uuid;
    Cyqlops::Crypto::RSAAlgorithm rsaAlgorithm;
    QByteArray public_key;
    QByteArray revision_hash;
    bool is_merge_master;
    knowCore::Timestamp last_update_to_merge_master;
    knowCore::Timestamp first_seen;
    knowCore::Timestamp undisconnected_seen;
    knowCore::Timestamp last_seen;
    mutable QList<knowCore::Timestamp> last_status_timestamps;
    qreal reliability() const
    {
      knowCore::Timestamp tn = knowCore::Timestamp::now();
      knowCore::Timestamp first_reliability_stamp = tn - knowCore::Timestamp(RELIABILITY_DURATION);
      for(auto it = last_status_timestamps.begin(); it != last_status_timestamps.end();)
      {
        if(*it < first_reliability_stamp)
        {
          it = last_status_timestamps.erase(it);
        }
        else
        {
          ++it;
        }
      }
      double time_span
        = std::min<double>(RELIABILITY_DURATION.count(),
                           first_seen.intervalTo<knowCore::Seconds>(tn).count().toDouble());
      return last_status_timestamps.size() / (1 + time_span / STATUS_PULSE_TIME);
    }
  };
  struct RevisionsThread;
  struct StatusElectionThread : public QThread
  {
    StatusElectionThread();
    knowCore::AsynchronousFunction<void, const Vote&> handleVote;
    knowCore::AsynchronousFunction<cres_qresult<void>, const Status&> handleStatus;
    void setup(const QString& _name, const kDB::Repository::TripleStore& _store,
               AbstractCommunicationInterface* _communicationInterface,
               RevisionsThread* _revisionsThread, SynchronisationManagerVersion _version);
    bool isMergeMaster() const { return is_merge_master; }
  private:
    QString name;
    kDB::Repository::TripleStore store;
    AbstractCommunicationInterface* communicationInterface = nullptr;
    RevisionsThread* revisionsThread = nullptr;
  private:
    // Status Stuff
    void publishStatus();
  private:
    QTimer statusStimer;
    bool is_merge_master = false;
    QHash<QUuid, OtherNodeInfo> otherNodes;
  private:
    QList<QUuid> getCoalitionMembers() const;
    // Election Stuff
    static QByteArray hashVote(const Vote& _vote);
    void checkElection();
    void startElection(const knowCore::Timestamp& _election_time);
    void checkElectionResult();
    cres_qresult<void> doHandleStatus(const Status& _status);
    void doHandleVote(const Vote& _vote);
    void castVote(const Vote& _vote);
    knowCore::Timestamp electionDeadline;
    QTimer electionTimer;
    QUuid lastWinner;
    int currentLeg = -1;
    QList<QHash<QUuid, int>> votes;
    knowCore::Timestamp currentElection;
    knowCore::Timestamp earliestElection;
    SynchronisationManagerVersion version = SynchronisationManagerVersion::v2;
  };

  struct RevisionsThread : public QThread
  {
    RevisionsThread();
    void setup(const kDB::Repository::TripleStore& _store,
               AbstractCommunicationInterface* communicationInterface,
               SynchronisationManagerVersion _version);

    knowCore::AsynchronousFunction<void, const QUuid&, const QList<QUuid>&> setMergeMasterUuid;
    knowCore::AsynchronousFunction<void, const QUuid&> unsetMergeMasterUuid;
    knowCore::AsynchronousFunction<void> clearMergeMasterUuid;
    knowCore::AsynchronousFunction<cres_qresult<void>, const QByteArray&> fastForward;
    knowCore::AsynchronousFunction<cres_qresult<bool>, const Revisions&> handleRevisions;
    knowCore::AsynchronousFunction<void, const kDB::Repository::VersionControl::Revision&, bool>
      publishRevision;
    knowCore::AsynchronousFunction<void, const RevisionsRequest&> handleRevisionsRequest;
    knowCore::AsynchronousFunction<cres_qresult<void>> tryMerge;
    knowCore::AsynchronousFunction<void, const QUuid&, const QByteArray&> setTip;
    knowCore::AsynchronousFunction<void, const QByteArray&> requestMissingRevision;
    bool isTimerActive() const { return revisionsRequestTimer.isActive(); }
  private:
    QTimer revisionsRequestTimer, publishRevisionTimer;

    kDB::Repository::TripleStore store;
    AbstractCommunicationInterface* communicationInterface = nullptr;

    QList<QByteArray> received_revisions_hash;
    QHash<QByteArray, knowCore::Timestamp> unreceived_requested_revisions;
    QHash<QByteArray, QHash<QByteArray, Revision>> requested_revisions_children;
    QHash<QUuid, QByteArray> uuid_to_tip;
    QList<kDB::Repository::VersionControl::Revision> revisions_to_publish;

    void updateRequestMissingRevisionTimer();

    void doRequestMissingRevision(const QByteArray& _revision);
    cres_qresult<void> doTryMerge();
    void doPublishRevision(const kDB::Repository::VersionControl::Revision& _revision,
                           bool _imediate);
    cres_qresult<bool> doHandleRevisions(const Revisions& _revision);
    void doHandleRevisionsRequest(const RevisionsRequest& _revisionRequest);
    cres_qresult<void> doFastForward(const QByteArray& _fastForward);
    void publishRequestRevisions();
    void publishRevisions();
    void doSetTip(const QUuid& _uuid, const QByteArray& _hash);
    cres_qresult<bool> handleOneRevision(const Revision& _revision);

    QUuid merge_master_uuid;
    QList<QUuid> coalition_members;
    SynchronisationManagerVersion version = SynchronisationManagerVersion::v2;
  };
} // namespace kDBRDFGraphSynchronisation

StatusElectionThread::StatusElectionThread()
    : handleVote(std::bind(&StatusElectionThread::doHandleVote, this, std::placeholders::_1), this),
      handleStatus(std::bind(&StatusElectionThread::doHandleStatus, this, std::placeholders::_1),
                   this, &log_error<"handleStatus", void>)
{

  // Setup election timer
  electionTimer.moveToThread(this);
  electionTimer.setInterval(ELECTION_LENGTH);
  electionTimer.setSingleShot(true);
  QObject::connect(&electionTimer, &QTimer::timeout, [this]() { checkElectionResult(); });

  // Setup status timer
  statusStimer.moveToThread(this);
  QObject::connect(&statusStimer, &QTimer::timeout, [this]() { publishStatus(); });

  // Start timer
  QMetaObject::invokeMethod(&statusStimer, "start", Qt::QueuedConnection,
                            Q_ARG(int, STATUS_PULSE_TIME * 1000));
}

void StatusElectionThread::setup(const QString& _name, const kDB::Repository::TripleStore& _store,
                                 AbstractCommunicationInterface* _communicationInterface,
                                 RevisionsThread* _revisionsThread,
                                 SynchronisationManagerVersion _version)
{
  name = _name;
  store = _store.detach();
  communicationInterface = _communicationInterface;
  revisionsThread = _revisionsThread;
  version = _version;
}

void StatusElectionThread::publishStatus()
{
  cres_qresult<QByteArray> tH = store.versionControlManager()->tipHash();
  if(tH.is_successful())
  {
    QList<QByteArray> heads;
    cres_qresult<QList<kDB::Repository::VersionControl::Revision>> heads_revs
      = store.versionControlManager()->heads();
    if(heads_revs.is_successful())
    {
      for(const kDB::Repository::VersionControl::Revision& rev :
          knowCore::dereference(heads_revs.get_value()))
      {
        heads.append(rev.hash());
      }
      communicationInterface->sendStatus({name, store.connection().serverUuid(),
                                          store.connection().rsaAlgorithm().serialise(
                                            Cyqlops::Crypto::AbstractSerialisable::Key::Public),
                                          tH.move_value().value(), heads, is_merge_master});
    }
    else
    {
      clog_error("Failed to retrieve heads with error: {}", heads_revs.get_error());
    }
  }
  else
  {
    clog_error("Failed to retrieve tip hash with error: {}", tH.get_error());
  }
}

QList<QUuid> StatusElectionThread::getCoalitionMembers() const
{
  QList<QUuid> cm;
  for(const OtherNodeInfo& oni : otherNodes)
  {
    if(oni.reliability() > RELIABILITY)
    {
      cm.append(oni.uuid);
    }
  }
  return cm;
}

cres_qresult<void> StatusElectionThread::doHandleStatus(const Status& _status)
{
  if(_status.uuid != store.connection().serverUuid())
  {
    if(not earliestElection.isValid())
    {
      earliestElection = knowCore::Timestamp::now().add<knowCore::Seconds>(ELECTION_GRACE_PERIOD);
    }

    revisionsThread->setTip(_status.uuid, _status.revisionHash);

    if(otherNodes.contains(_status.uuid))
    {
      OtherNodeInfo& oni = otherNodes[_status.uuid];
      if(oni.public_key != _status.publicKey)
      {
        clog_warning("Node {} has changed its key to {}", _status.uuid, _status.publicKey);
        oni.public_key = _status.publicKey;
        oni.rsaAlgorithm.unserialise(_status.publicKey);
      }
      if(oni.revision_hash != _status.revisionHash)
      {
        oni.revision_hash = _status.revisionHash;
        if(is_merge_master)
        {
          revisionsThread->tryMerge();
        }
      }

      if(version == SynchronisationManagerVersion::v1 or oni.reliability() > RELIABILITY)
      {
        if(_status.mergeMaster and not oni.is_merge_master)
        {
          oni.is_merge_master = true;
          oni.last_update_to_merge_master = knowCore::Timestamp::now();
          revisionsThread->setMergeMasterUuid(oni.uuid, getCoalitionMembers());
          is_merge_master = false;
        }
        else if(_status.mergeMaster == oni.is_merge_master)
        {
          // Update the time we last got an update to merge master
          oni.last_update_to_merge_master = knowCore::Timestamp::now();
        }
        else if(not _status.mergeMaster and oni.is_merge_master
                and oni.last_update_to_merge_master
                      < knowCore::Timestamp::now().add<knowCore::Seconds>(-ELECTION_GRACE_PERIOD))
        {
          // We wrongly thought the node was a merge master, there is a delay otherwise we could
          // accidently cancel the result of an election
          oni.is_merge_master = false;
          oni.last_update_to_merge_master = knowCore::Timestamp::now();
          revisionsThread->unsetMergeMasterUuid(oni.uuid);
        }
      }
      if(oni.last_seen
         < knowCore::Timestamp::now().add<knowCore::Seconds>(-STATUS_DISCONNECTED_TIME))
      {
        // there was a disconnection since last time we saw that node
        oni.undisconnected_seen = knowCore::Timestamp::now();
        oni.last_seen = oni.undisconnected_seen;
      }
      else
      {
        // Update the last time we saw the node
        oni.last_seen = knowCore::Timestamp::now();
      }

      // Compute reliability
      oni.last_status_timestamps.append(oni.last_seen);

      // Handle heash
      cres_try(QByteArray head_hash, store.versionControlManager()->tipHash());

      if(oni.is_merge_master and _status.revisionHash != head_hash)
      {
        QByteArray new_revision = _status.revisionHash;
        revisionsThread->fastForward(new_revision);
      }

      if(is_merge_master)
      {
        // If it is the merge master, it should try to request revision that would allow it to merge
        for(const QByteArray& head : _status.heads)
        {
          QByteArray oni_revision_hash = head;
          cres_try(bool hasRev, store.versionControlManager()->hasRevision(oni_revision_hash));
          if(not hasRev)
          {
            clog_assert(not head.isEmpty());
            revisionsThread->requestMissingRevision(head);
          }
        }
      }
    }
    else
    {
      OtherNodeInfo oni;
      oni.uuid = _status.uuid;
      oni.public_key = _status.publicKey;
      oni.rsaAlgorithm.unserialise(_status.publicKey);
      oni.revision_hash = _status.revisionHash;
      oni.is_merge_master = _status.mergeMaster;
      if(_status.mergeMaster)
      {
        revisionsThread->setMergeMasterUuid(oni.uuid, getCoalitionMembers());
      }
      oni.first_seen = knowCore::Timestamp::now();
      oni.undisconnected_seen = oni.first_seen;
      oni.last_seen = oni.first_seen;
      oni.last_status_timestamps.append(oni.first_seen);
      otherNodes[_status.uuid] = oni;
    }
  }
  checkElection();
  return cres_success();
}

void StatusElectionThread::checkElection()
{
  if(earliestElection.isValid() and knowCore::Timestamp::now() < earliestElection)
  {
    return;
  }
  if(currentLeg >= 0)
    return; // Election in progress!

  // Disconnection time
  knowCore::Timestamp now = knowCore::Timestamp::now().add(-STATUS_DISCONNECTED_TIME);

  // Check if agent has priority

  int currentConnectedNode = 0;
  int currentMergeMasters = 0;

  if(is_merge_master)
  {
    currentMergeMasters = 1;
  }

  for(const OtherNodeInfo& oni : otherNodes)
  {
    if(oni.last_seen > now
       and (version == SynchronisationManagerVersion::v1 or oni.reliability() > RELIABILITY))
    {
      if(oni.is_merge_master)
      {
        currentMergeMasters++;
      }
      ++currentConnectedNode;
    }
  }
  // Start an election
  if(currentConnectedNode > 0
     and (currentMergeMasters == 0 or (is_merge_master and currentMergeMasters > 1)))
  {

    for(const OtherNodeInfo& oni : otherNodes)
    {
      if(oni.last_seen.isValid() and oni.last_seen > now
         and oni.uuid < store.connection().serverUuid())
      {
        revisionsThread->clearMergeMasterUuid();
        is_merge_master = false;
        return;
      }
    }
    startElection(knowCore::Timestamp::now());
  }
}

QByteArray StatusElectionThread::hashVote(const Vote& _vote)
{
  knowCore::CryptographicHash hash(QCryptographicHash::Sha512);
  hash.addData(_vote.voter);
  hash.addData(_vote.electionTime.toEpoch<knowCore::NanoSeconds>().count());
  hash.addData(_vote.leg);
  hash.addData(_vote.candidate);
  hash.addData(_vote.electionTime.toEpoch<knowCore::NanoSeconds>().count());
  return hash.result();
}

void StatusElectionThread::startElection(const knowCore::Timestamp& _election_time)
{
  clog_assert(currentElection != _election_time);
  clog_assert(
    (_election_time < currentElection
     and _election_time
           > currentElection.add(-ELECTION_LENGTH)) // check that _election_time is older,
                                                    // but not too old than the current one
    or _election_time > currentElection.add(ELECTION_LENGTH)); // or that it is a new election
  currentElection = _election_time;

  // No one is merge master
  revisionsThread->clearMergeMasterUuid();
  is_merge_master = false;
  for(OtherNodeInfo& oni : otherNodes)
  {
    oni.is_merge_master = false;
  }

  // clean up election
  votes = {{}};

  // Start election
  currentLeg = 0;
  Vote lastVote;
  lastVote.voter = store.connection().serverUuid();
  knowCore::CryptographicHash hash(QCryptographicHash::Sha512);
  hash.addData(lastVote.voter);
  lastVote.leg = currentLeg;
  lastVote.timestamp = knowCore::Timestamp::now();
  lastVote.electionTime = currentElection;

  knowCore::Timestamp now = knowCore::Timestamp::now().add(-STATUS_DISCONNECTED_TIME);

  /**
   * Keep voting for last time winner, if still visible, might help to stabilise election results.
   */
  if(otherNodes[lastWinner].last_seen > now)
  {
    lastVote.candidate = lastWinner;
  }
  else
  {
    knowCore::Timestamp mostAncient = knowCore::Timestamp::now().add(knowCore::Seconds(10));
    for(const OtherNodeInfo& oni : otherNodes)
    {
      if(oni.undisconnected_seen < mostAncient
         and oni.last_seen > now) // Vote for the most ancient and that has been seen recently
      {
        lastVote.candidate = oni.uuid;
        mostAncient = oni.undisconnected_seen;
      }
    }
  }
  electionDeadline = knowCore::Timestamp::now().add(ELECTION_LENGTH);
  lastVote.signature = store.connection().rsaAlgorithm().sign(
    hashVote(lastVote), Cyqlops::Crypto::Hash::Algorithm::RAW);
  ++votes[0][lastVote.candidate];
  communicationInterface->sendVote(lastVote);
  castVote(lastVote);
  electionTimer.start(ELECTION_LENGTH);
}

void StatusElectionThread::checkElectionResult()
{
  electionTimer.stop();

  if(votes.isEmpty())
  {
    electionTimer.start(ELECTION_LENGTH);
    return;
  }

  QList<QUuid> winners;
  int winners_votes = 0;
  clog_info("Received {} votes in election leg {}", votes.last().size(), votes.size());
  for(QHash<QUuid, int>::iterator it = votes.last().begin(); it != votes.last().end(); ++it)
  {
    if(it.value() == winners_votes)
    {
      winners.append(it.key());
    }
    else if(it.value() > winners_votes)
    {
      winners_votes = it.value();
      winners.clear();
      winners.append(it.key());
    }
  }
  clog_info("Has {} winners", winners.size());
  clog_assert(winners.size() > 0);
  if(winners.size() == 1)
  {
    clog_info("{} has won the election acoording to {}.", winners.first(),
              store.connection().serverUuid());
    earliestElection = knowCore::Timestamp::now().add<knowCore::Seconds>(ELECTION_GRACE_PERIOD);
    lastWinner = winners.first();
    revisionsThread->setMergeMasterUuid(winners.first(), getCoalitionMembers());
    if(winners.first() == store.connection().serverUuid())
    {
      clog_info("I {} won the election", store.connection().serverUuid());
      is_merge_master = true;
      publishStatus();
      revisionsThread->tryMerge();
    }
    else
    {
      otherNodes[winners.first()].is_merge_master = true;
      otherNodes[winners.first()].last_update_to_merge_master = knowCore::Timestamp::now();
    }
    currentLeg = -1;
  }
  else
  {
    ++currentLeg;
    votes.append(QHash<QUuid, int>{});

    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr(0, winners.size() - 1);

    Vote vote;
    vote.voter = store.connection().serverUuid();
    vote.electionTime = currentElection;

    vote.leg = currentLeg;
    vote.timestamp = knowCore::Timestamp::now();
    vote.candidate = winners[distr(eng)];
    vote.signature = store.connection().rsaAlgorithm().sign(hashVote(vote),
                                                            Cyqlops::Crypto::Hash::Algorithm::RAW);
    ++votes[currentLeg][vote.candidate];
    communicationInterface->sendVote(vote);
    castVote(vote);
    QMetaObject::invokeMethod(&electionTimer, "start", Qt::QueuedConnection,
                              Q_ARG(int, ELECTION_LENGTH.count() * 1000));
  }
}

void StatusElectionThread::castVote(const Vote& _vote)
{
  if(_vote.candidate.isNull())
  {
    clog_info("Received a blank vote.");
  }
  else
  {
    while(_vote.leg >= votes.size())
    {
      votes.append(QHash<QUuid, int>());
    }
    ++votes[_vote.leg][_vote.candidate];
  }
}

void StatusElectionThread::doHandleVote(const Vote& _vote)
{
  if(_vote.voter == store.connection().serverUuid())
  {
    return;
  }
  OtherNodeInfo& oni = otherNodes[_vote.voter];
  if(version == SynchronisationManagerVersion::v2 and oni.reliability() < RELIABILITY)
    return;
  if(_vote.electionTime == currentElection)
  {
    castVote(_vote);
  }
  else if(_vote.electionTime > currentElection.add(ELECTION_LENGTH)
          or (_vote.electionTime < currentElection
              and _vote.electionTime > currentElection.add(-ELECTION_LENGTH)))
  {
    // Received a vote for an earlier election or a much later one, drop the current election
    startElection(_vote.electionTime);
    castVote(_vote);
  }
}

RevisionsThread::RevisionsThread()
    : setMergeMasterUuid(
        [this](const QUuid& _uuid, const QList<QUuid>& _coalition_members)
        {
          merge_master_uuid = _uuid;
          coalition_members = _coalition_members;
        },
        this),
      unsetMergeMasterUuid(
        [this](const QUuid& _uuid)
        {
          if(_uuid == merge_master_uuid)
          {
            merge_master_uuid = QUuid();
            coalition_members.clear();
          }
        },
        this),
      clearMergeMasterUuid([this]() { merge_master_uuid = QUuid(); }, this),
      fastForward(std::bind(&RevisionsThread::doFastForward, this, std::placeholders::_1), this,
                  &log_error<"fastForward", void>),
      handleRevisions(std::bind(&RevisionsThread::doHandleRevisions, this, std::placeholders::_1),
                      this, &log_error<"handleRevision", bool>),
      publishRevision(std::bind(&RevisionsThread::doPublishRevision, this, std::placeholders::_1,
                                std::placeholders::_2),
                      this),
      handleRevisionsRequest(
        std::bind(&RevisionsThread::doHandleRevisionsRequest, this, std::placeholders::_1), this),
      tryMerge(std::bind(&RevisionsThread::doTryMerge, this), this, &log_error<"tryMerge", void>),
      setTip(
        std::bind(&RevisionsThread::doSetTip, this, std::placeholders::_1, std::placeholders::_2),
        this),
      requestMissingRevision(
        std::bind(&RevisionsThread::doRequestMissingRevision, this, std::placeholders::_1), this)
{

  // Setup status timer
  revisionsRequestTimer.moveToThread(this);
  QObject::connect(&revisionsRequestTimer, &QTimer::timeout,
                   [this]() { publishRequestRevisions(); });
  publishRevisionTimer.moveToThread(this);
  QObject::connect(&publishRevisionTimer, &QTimer::timeout, [this]() { publishRevisions(); });

  updateRequestMissingRevisionTimer();
}

void RevisionsThread::updateRequestMissingRevisionTimer()
{
  if(unreceived_requested_revisions.size() > 0)
  {
    if(not revisionsRequestTimer.isActive())
    {
      revisionsRequestTimer.start(STATUS_PULSE_TIME * 1000 * 2);
    }
  }
  else
  {
    revisionsRequestTimer.stop();
  }
}

void RevisionsThread::setup(const kDB::Repository::TripleStore& _store,
                            AbstractCommunicationInterface* _communicationInterface,
                            SynchronisationManagerVersion _version)
{
  store = _store;
  communicationInterface = _communicationInterface;
  version = _version;
}

void RevisionsThread::doRequestMissingRevision(const QByteArray& _revision)
{
  if(not unreceived_requested_revisions.contains(_revision)
     and not received_revisions_hash.contains(_revision))
  {
    unreceived_requested_revisions[_revision] = knowCore::Timestamp().add(knowCore::Seconds(-30));
  }
  publishRequestRevisions();
}

cres_qresult<void> RevisionsThread::doFastForward(const QByteArray& _fastForward)
{
  cres_try(bool hasFastword, store.versionControlManager()->hasRevision(_fastForward));
  if(hasFastword)
  {
    cres_try(bool canFastword, store.versionControlManager()->canFastForward(_fastForward));
    if(canFastword)
    {
      cres_try(cres_ignore, store.versionControlManager()->fastForward(_fastForward));
    }
  }
  else
  {
    clog_assert(not _fastForward.isEmpty());
    requestMissingRevision(_fastForward);
  }
  return cres_success();
}

void RevisionsThread::doSetTip(const QUuid& _uuid, const QByteArray& _hash)
{
  uuid_to_tip[_uuid] = _hash;
}

cres_qresult<bool> RevisionsThread::handleOneRevision(const Revision& _revision)
{
  if(not received_revisions_hash.contains(_revision.hash))
  {
    received_revisions_hash.append(_revision.hash);
  }
  unreceived_requested_revisions.remove(_revision.hash);

  cres_try(bool hasRev, store.versionControlManager()->hasRevision(_revision.hash));
  if(not hasRev)
  {
    bool has_all_parents = true;
    for(const Delta& delta : _revision.deltas)
    {
      cres_try(bool hasDelaParentRev, store.versionControlManager()->hasRevision(delta.parent));
      if(not hasDelaParentRev)
      {
        has_all_parents = false;
        requestMissingRevision(delta.parent);
        requested_revisions_children[delta.parent][_revision.hash] = _revision;
      }
    }
    if(has_all_parents)
    {

      kDB::Repository::VersionControl::RevisionBuilder rb
        = store.versionControlManager()->insertRevision(_revision.contentHash);
      rb.setMetaInformation(_revision.hash, _revision.historicity);
      for(const Delta& delta : _revision.deltas)
      {
        QList<kDB::Repository::VersionControl::Signature> signatures;
        for(const Signature& sig : delta.signatures)
        {
          signatures.append({sig.author, sig.timestamp, sig.signature});
        }
        rb.addDelta(delta.parent, delta.hash, delta.delta, signatures);
      }
      {
        kDB::Repository::Transaction t(store.connection());
        if(not rb.insert(t, true).is_successful())
        {
          t.rollback();
          clog_error("Failed to build revision, try again.");
          return handleOneRevision(_revision);
        }
        if(not t.commit().is_successful())
        {
          clog_error("Commit failed when inserting revision, try again.");
          return handleOneRevision(_revision);
        }
      }
      cres_try(bool canFastword, store.versionControlManager()->canFastForward(_revision.hash));
      if(canFastword)
      {
        clog_debug("{} fastforward from {} to {}", store.connection().serverUuid(),
                   store.versionControlManager()->tipHash(), _revision.hash);
        cres_try(
          cres_ignore, store.versionControlManager()->fastForward(_revision.hash),
          message("Failed to fast forward despite having inserted the commit, with error: {}"));
      }
      bool missing_revisions = false;
      QList<Revision> children = requested_revisions_children[_revision.hash].values();
      requested_revisions_children.remove(_revision.hash);
      for(const Revision& cmsg : children)
      {
        cres_try(bool v, handleOneRevision(cmsg));
        missing_revisions = not v or missing_revisions;
      }
      return cres_success(not missing_revisions);
    }
    else
    {
      return cres_success(false);
    }
  }
  return cres_success(true);
}

cres_qresult<bool> RevisionsThread::doHandleRevisions(const Revisions& _revisions)
{
  for(const Revision& revision : _revisions.revisions)
  {
    cres_try(cres_ignore, handleOneRevision(revision));
  }
  if(merge_master_uuid == store.connection().serverUuid())
  {
    tryMerge();
  }
  return cres_success(true);
}

cres_qresult<void> RevisionsThread::doTryMerge()
{
  cres_check(merge_master_uuid == store.connection().serverUuid(),
             "Call to tryMerge by non-merge master {} while merge master is {}!",
             store.connection().serverUuid(), merge_master_uuid);
  cres_try(QByteArray head_revision, store.versionControlManager()->tipHash());
  cres_try(QList<kDB::Repository::VersionControl::Revision> heads,
           store.versionControlManager()->heads());
  qsizetype original_heads_size = heads.size();

  for(QList<kDB::Repository::VersionControl::Revision>::iterator it = heads.begin();
      it != heads.end();)
  {
    bool keep = false;
    if(it->hash() == head_revision)
    {
      keep = true;
    }
    else
    {
      for(const QByteArray& hash : uuid_to_tip)
      {
        if(hash == it->hash())
        {
          keep = true;
          break;
        }
      }
    }
    if(keep)
    {
      ++it;
    }
    else
    {
      it = heads.erase(it);
    }
  }
  if(heads.size() <= 1)
  {
    if(original_heads_size > 1)
    {
      clog_info("Not merging, even though there are {} heads, but none are tips.",
                original_heads_size);
    }
    return cres_success();
  }

  if(true)
  {
    QList<QByteArray> hashes;
    cres_try(QList<kDB::Repository::VersionControl::Revision> heads,
             store.versionControlManager()->heads());
    for(const kDB::Repository::VersionControl::Revision& rev : heads)
    {
      if(rev.hash() != head_revision)
      {
        hashes.append(rev.hash());
      }
    }
    clog_debug("{} tryMerge with heads {}", store.connection().serverUuid(), hashes);
    cres_qresult<void> rv = store.versionControlManager()->merge(hashes);
    if(not rv.is_successful())
    {
      clog_error("{} Failed to merge with error: {}", store.connection().serverUuid(),
                 rv.get_error());
    }
  }
  else
  {
    clog_debug("{} tryMerge with heads {}", store.connection().serverUuid(), heads);
    while(heads.size() > 1)
    {
      bool has_merged = false;
      for(int i = 0; i < heads.size(); ++i)
      {
        if(heads[i].hash() != head_revision)
        {
          clog_chrono(merge);
          cres_try(bool cM, store.versionControlManager()->canMerge(heads[i].hash()));
          if(cM)
          {
            if(store.versionControlManager()->merge(heads[i].hash()).is_successful())
            {
              heads.removeAt(i);
              cres_try(head_revision, store.versionControlManager()->tipHash());
              break;
            }
            else
            {
              clog_error("Failed to merge!");
            }
          }
          else
          {
            clog_error("Cannot merge!");
          }
        }
      }
      if(not has_merged)
        break;
    }
  }
  if(heads.size() > 1)
  {
    tryMerge();
  }
  publishRevisions();
  return cres_success();
}

void RevisionsThread::doHandleRevisionsRequest(const RevisionsRequest& _revisionRequest)
{
  if(_revisionRequest.requester != store.connection().serverUuid())
  {
    bool answer
      = (merge_master_uuid
           == store.connection().serverUuid() // merge master always respond to revision request
         or _revisionRequest.requester
              == merge_master_uuid // other node always respond to merge master requests
         or (not coalition_members.contains(_revisionRequest.requester)
             and version == SynchronisationManagerVersion::v2)); // always respond to non member
                                                                 // of the coalition, to raise
                                                                 // chances the revision is sent
    if(answer)
    {
      bool has_one = false;
      for(const QByteArray& ha : _revisionRequest.hashes)
      {
        cres_qresult<kDB::Repository::VersionControl::Revision> rev
          = store.versionControlManager()->revision(ha);
        if(rev.is_successful())
        {
          has_one = true;
          doPublishRevision(rev.get_value(), false);
        }
        else
        {
          log_error<"handleRevisionsRequest">(rev.discard_value());
        }
      }
      if(has_one)
        publishRevisions();
    }
  }
}

void RevisionsThread::doPublishRevision(const kDB::Repository::VersionControl::Revision& _revision,
                                        bool _immediate)
{
  revisions_to_publish.append(_revision);
  if(_immediate)
  {
    publishRevisions();
  }
  else
  {
    publishRevisionTimer.start(REVISIONS_PUBLISH_DELAY);
  }
}

void RevisionsThread::publishRevisions()
{
  publishRevisionTimer.stop();
  if(revisions_to_publish.isEmpty())
    return;
  QList<Revision> revisions;
  for(const kDB::Repository::VersionControl::Revision& rev : revisions_to_publish)
  {
    QList<Delta> deltas;

    for(const kDB::Repository::VersionControl::Delta& delta : rev.deltas())
    {
      QList<Signature> signatures;
      for(const kDB::Repository::VersionControl::Signature& sign : delta.signatures())
      {
        signatures.append({sign.author(), sign.timestamp(), sign.signature()});
      }
      deltas.append({delta.parent(), delta.hash(), delta.delta(), signatures});
    }
    revisions.append({rev.hash(), rev.contentHash(), rev.historicity(), deltas});
  }
  revisions_to_publish.clear();
  clog_debug("{} is sending {} revisions", store.connection().serverUuid(), revisions.size());
  communicationInterface->sendRevisions({revisions});
}

void RevisionsThread::publishRequestRevisions()
{
  RevisionsRequest rr;
  knowCore::Timestamp limit = knowCore::Timestamp::now().add(knowCore::Seconds(-30));
  for(QHash<QByteArray, knowCore::Timestamp>::iterator it = unreceived_requested_revisions.begin();
      it != unreceived_requested_revisions.end(); ++it)
  {
    if(it.value() < limit)
    {
      rr.hashes.append(it.key());
      it.value() = knowCore::Timestamp::now();
    }
  }
  if(rr.hashes.size() > 0)
  {
    rr.requester = store.connection().serverUuid();
    communicationInterface->sendRevisionsRequest(rr);
  }
  updateRequestMissingRevisionTimer();
}

struct SynchronisationManager::Private
{
  RevisionsThread revisionsThread;
  StatusElectionThread statusElectionThread;
  kDB::Repository::TripleStore store;
  bool debug = false;
};

namespace
{
  static SynchronisationManagerVersion selected_version = SynchronisationManagerVersion::v2;
}

// TODO remove the following two functions
// They are not part of the public API and only used for the benchmarking tools
void kdb_rdf_graph_synchronisation_use_v1()
{
  selected_version = SynchronisationManagerVersion::v1;
}

void kdb_rdf_graph_synchronisation_use_v2()
{
  selected_version = SynchronisationManagerVersion::v2;
}

SynchronisationManager::SynchronisationManager(
  const QString& _name, const kDB::Repository::TripleStore& _store,
  AbstractCommunicationInterface* _communicationInterface)
    : d(new Private)
{
  d->store = _store;
  d->revisionsThread.setup(_store, _communicationInterface, selected_version);
  d->statusElectionThread.setup(_name, _store, _communicationInterface, &d->revisionsThread,
                                selected_version);

  // Send new revisions

  d->store.versionControlManager()->listenNewRevision(
    [this](const kDB::Repository::VersionControl::Revision& _revision)
    {
      if(_revision.isSignedBy(d->store.connection().serverUuid()))
      {
        d->revisionsThread.publishRevision(_revision, _revision.deltas().size() == 1);
      }
    });
}

SynchronisationManager::~SynchronisationManager()
{
  d->revisionsThread.quit();
  d->statusElectionThread.quit();
  d->revisionsThread.wait();
  d->statusElectionThread.wait();
  delete d;
}

void SynchronisationManager::handleRevisions(const Revisions& _revisions)
{
  d->revisionsThread.handleRevisions(_revisions);
}

void SynchronisationManager::handleRevisionsRequest(const RevisionsRequest& _request)
{
  d->revisionsThread.handleRevisionsRequest(_request);
}

void SynchronisationManager::handleStatus(const Status& _status)
{
  d->statusElectionThread.handleStatus(_status);
}

void SynchronisationManager::handleVote(const Vote& _vote)
{
  d->statusElectionThread.handleVote(_vote);
}

void SynchronisationManager::start()
{
  // Start threads
  d->revisionsThread.start();
  d->statusElectionThread.start();
}
