#pragma once

#include <QUuid>
#include <knowCore/Timestamp.h>

namespace kDBRDFGraphSynchronisation
{
  struct Status
  {
    QString name;
    QUuid uuid;
    QByteArray publicKey;
    QByteArray revisionHash;
    QList<QByteArray> heads;
    bool mergeMaster;
  };
  struct Vote
  {
    // Uid for the agent that casted this vote
    QUuid voter;
    // Signature for the vote
    QByteArray signature;
    // Uid for the candidate
    QUuid candidate;
    // Leg for the election from 0 -> inf
    int leg;
    /// Timestamp for the vote
    knowCore::Timestamp timestamp;
    /// Time when the election was triggered
    knowCore::Timestamp electionTime;
  };
  struct RevisionsRequest
  {
    QUuid requester;
    QList<QByteArray> hashes;
  };
  struct Signature
  {
    QUuid author;
    knowCore::Timestamp timestamp;
    QByteArray signature;
  };
  struct Delta
  {
    QByteArray parent;
    QByteArray hash;
    QByteArray delta;
    QList<Signature> signatures;
  };
  struct Revision
  {
    QByteArray hash, contentHash;
    qint64 historicity;
    QList<Delta> deltas;
  };
  struct Revisions
  {
    QList<Revision> revisions;
  };
} // namespace kDBRDFGraphSynchronisation
