#include "TestMQTTRDFSynchronisation.h"

#include <knowCore/Test.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/VersionControl/Manager.h>

#include "../MQTTCommunicationInterface.h"
#include "../SynchronisationManager.h"

void TestMQTTRDFSynchronisation::testSynchronisation()
{
  QByteArray mqtt_test_server = qgetenv("KDB_MQTT_TEST_SERVER");
  if(mqtt_test_server.isEmpty())
  {
    mqtt_test_server = "tcp://localhost:1883";
  }
  struct Agent
  {
    kDB::Repository::TemporaryStore store;
    kDB::Repository::Connection connection;
    kDB::Repository::TripleStore triplesStore;

    kDBRDFGraphSynchronisation::SynchronisationManager* syncManager;
  };

  QList<std::shared_ptr<Agent>> agents;
  for(int i = 0; i < 3; ++i)
    agents.append(std::make_shared<Agent>());

  for(std::shared_ptr<Agent> agent : agents)
  {
    CRES_QVERIFY(agent->store.start());
    agent->connection = agent->store.createConnection();
    CRES_QVERIFY(agent->connection.connect());
    agent->triplesStore
      = CRES_QVERIFY(agent->connection.graphsManager()->getOrCreateTripleStore("a"_kCu));
    agent->triplesStore.enableVersioning();
    agent->syncManager
      = kDBRDFGraphSynchronisation::MQTTCommunicationInterface::startSynchronisation(
        agent->connection.serverUri(), mqtt_test_server, agent->triplesStore);
    agent->syncManager->start();
  }

  for(std::size_t iteration = 0; iteration < 10; ++iteration)
  {
    clog_info("{}/10 iteration", iteration + 1);
    for(std::shared_ptr<Agent> agent : agents)
    {
      agent->triplesStore.insert(knowCore::Uri(QUuid::createUuid().toString()),
                                 QUuid::createUuid().toString(),
                                 knowCore::Uri(QUuid::createUuid().toString()));
    }
    QTest::qWait(5000);
  }
  clog_info("Waiting for synchronisation to finish...");
  QVERIFY(QTest::qWaitFor(
    [&agents]()
    {
      QByteArray tipHash
        = CRES_QVERIFY(agents.first()->triplesStore.versionControlManager()->tipHash());
      for(std::shared_ptr<Agent> agent : agents)
      {
        if(tipHash != agent->triplesStore.versionControlManager()->tipHash())
        {
          return false;
        }
      }
      return true;
    },
    600000));
}
QTEST_MAIN(TestMQTTRDFSynchronisation)
