#include <chrono>
#include <fstream>
#include <random>
#include <thread>

#include <clog_bench>

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDir>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/Store.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/Utils.h>
#include <kDB/Repository/VersionControl/Delta.h>
#include <kDB/Repository/VersionControl/Manager.h>
#include <kDB/Repository/VersionControl/Revision.h>
#include <kDB/Repository/VersionControl/RevisionBuilder_p.h>

#include "../AbstractCommunicationInterface.h"
#include "../SynchronisationManager.h"

using namespace std::chrono_literals;

class CommunicationInterface;

void kdb_rdf_graph_synchronisation_use_v1();
void kdb_rdf_graph_synchronisation_use_v2();

clog_format_declare_formatter(kDB::Repository::VersionControl::Revision)
{
  return std::format_to(ctx.out(), "{}", p.hash());
}

struct Parameters
{
  const std::size_t iteration_max_count = 200;
  const std::size_t iteration_with_edit_max_count = 100;
  std::chrono::seconds loop_sleep = 10s; // Control how often new revisions are created
  const unsigned seed_triples = 1042;
  const unsigned seed_communication = 4210;
  /*const*/ std::size_t coalitions = 8;
  const std::size_t agents_per_coalitions = 4;
  const std::size_t triples_added = 3;
  const std::size_t triples_removed = 1;
  const double dropout = 0.1;       // 10% message dropped
  const double disconnection = 0.7; // 70% disconnection time
  const std::chrono::seconds disconnection_cycle
    = 60s; // Clusters are disonnected for disconnection * disconnection_cycle time
  const std::chrono::time_point<std::chrono::system_clock> startTime
    = std::chrono::system_clock().now();
};

bool is_connected(const Parameters& _parameters)
{
  return (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock().now()
                                                           - _parameters.startTime)
            .count()
          % _parameters.disconnection_cycle.count())
         > _parameters.disconnection * _parameters.disconnection_cycle.count();
}

struct Agent
{
  ~Agent();
  std::size_t agent_id;
  std::size_t coalition_id;
  CommunicationInterface* comm_interface;
  kDBRDFGraphSynchronisation::SynchronisationManager* manager;
  kDB::Repository::TemporaryStore store;
  kDB::Repository::Connection connection;
  kDB::Repository::TripleStore triplesStore;
};

class CommunicationInterface : public kDBRDFGraphSynchronisation::AbstractCommunicationInterface
{
public:
  CommunicationInterface(Agent* _self, const QList<Agent*>* _agents, Parameters* _parameters,
                         std::size_t _seed_communication)
      : m_self(_self), m_agents(_agents), m_parameters(_parameters),
        m_generator_communication(_seed_communication), m_reliableCommunication(false)
  {
  }
  ~CommunicationInterface() {}

  void sendStatus(const kDBRDFGraphSynchronisation::Status& _status) override
  {
    for(Agent* a : *m_agents)
    {
      if(shouldForward(a))
      {
        a->manager->handleStatus(_status);
      }
    }
  }
  void sendVote(const kDBRDFGraphSynchronisation::Vote& _vote) override
  {
    if(not m_elections.contains(_vote.electionTime))
    {
      m_elections.append(_vote.electionTime);
    }
    for(Agent* a : *m_agents)
    {
      if(shouldForward(a))
      {
        a->manager->handleVote(_vote);
      }
    }
  }
  void sendRevisions(const kDBRDFGraphSynchronisation::Revisions& _revision) override
  {
    for(Agent* a : *m_agents)
    {
      if(shouldForward(a))
      {
        a->manager->handleRevisions(_revision);
      }
    }
  }
  void sendRevisionsRequest(const kDBRDFGraphSynchronisation::RevisionsRequest& _request) override
  {
    for(Agent* a : *m_agents)
    {
      if(shouldForward(a))
      {
        a->manager->handleRevisionsRequest(_request);
      }
    }
  }
  QList<knowCore::Timestamp> elections() { return m_elections; }
  void enableReliableCommunication() { m_reliableCommunication = true; }
private:
  double shouldForward(const Agent* _other)
  {
    return m_self != _other
           and (m_reliableCommunication or m_self->coalition_id == _other->coalition_id
                or (m_distribution(m_generator_communication) > m_parameters->dropout
                    and is_connected(*m_parameters)));
  }
  Agent* m_self;
  const QList<Agent*>* m_agents;
  const Parameters* m_parameters;
  std::default_random_engine m_generator_communication;
  std::uniform_real_distribution<double> m_distribution;
  QList<knowCore::Timestamp> m_elections;
  bool m_reliableCommunication;
};

Agent::~Agent()
{
  delete comm_interface;
  delete manager;
  store.stop();
}

int main(int _argc, char** _argv)
{
  QCoreApplication app(_argc, _argv);
  QCoreApplication::setApplicationName("BenchmarkSynchronisation");
  QCoreApplication::setApplicationVersion("1.0");
  QCommandLineParser parser;
  QCommandLineOption op_v1("v1", "use protocol v1.");
  parser.addOption(op_v1);
  QCommandLineOption op_v2("v2", "use protocol v2.");
  parser.addOption(op_v2);
  QCommandLineOption op_gor_v1("gor-v1", "use GoR v1.");
  parser.addOption(op_gor_v1);
  QCommandLineOption op_c_5("c5", "use 5 coalitions.");
  parser.addOption(op_c_5);
  parser.addPositionalArgument("output", "output file");
  parser.process(app);

  clog_remove_all_listener();
  clog_stream_to_file(parser.positionalArguments()[0].toStdString() + ".out.txt");

  Parameters parameters;

  if(parser.isSet(op_v1))
  {
    kdb_rdf_graph_synchronisation_use_v1();
  }
  else if(parser.isSet(op_v2))
  {
    kdb_rdf_graph_synchronisation_use_v2();
  }
  else
  {
    clog_fatal("Missing version use --v1 or --v2!");
  }
  if(parser.isSet(op_gor_v1))
  {
    kDB::Repository::VersionControl::internal::set_simulate_v1(true);
  }
  if(parser.isSet(op_c_5))
  {
    parameters.coalitions = 5;
    parameters.loop_sleep = 5s;
  }

  // Initialise
  std::default_random_engine generator_triples{parameters.seed_triples};

  // Create agents
  QList<Agent*> agents;
  for(std::size_t coalition_id = 0; coalition_id < parameters.coalitions; ++coalition_id)
  {
    for(std::size_t j = 0; j < parameters.agents_per_coalitions; ++j)
    {
      std::size_t agent_id = agents.size();
      QString agent_name = clog_qt::qformat("agent_{}", agent_id);

      Agent* agent = new Agent;
      agent->agent_id = agent_id;
      agent->coalition_id = coalition_id;
      agent->store.start();
      agent->connection = agent->store.createConnection();
      agent->connection.connect().expect_success();
      agent->comm_interface = new CommunicationInterface(
        agent, &agents, &parameters, parameters.seed_communication + coalition_id * j);
      agent->triplesStore
        = agent->connection.graphsManager()->createTripleStore("test_graph"_kCu).expect_success();
      agent->triplesStore.enableVersioning();
      agent->manager = new kDBRDFGraphSynchronisation::SynchronisationManager(
        agent_name, agent->triplesStore, agent->comm_interface);
      agents.append(agent);
    }
  }

  // Start synchronisation
  for(Agent* agent : agents)
  {
    agent->manager->start();
  }

  // Run the loop
  clog_bench bench;
  bench.setup_columns("is_connected", "elections", "revisions", "merged_revisions", "heads",
                      "propagation_rate", "avg_propagation_time", "number_of_propagated_revision");

  struct RevisionInfo
  {
    bool initialized = false;
    knowCore::Timestamp first_time;

    QSet<QByteArray> included_revisions;
    bool is_merge;

    int propagation_count = 0;
    QVector<knowCore::Timestamp> propagation_times;
    double propagation_time = 0.0;
  };

  QHash<QByteArray, RevisionInfo> revisions_infos;

  std::function<void(QSet<QByteArray>*, const QSet<QByteArray>&)> add_to_list
    = [](QSet<QByteArray>* _dest, const QSet<QByteArray>& _src)
  {
    for(const QByteArray& v : _src)
    {
      if(not _dest->contains(v))
      {
        _dest->insert(v);
      }
    }
  };

  std::function<RevisionInfo&(kDB::Repository::VersionControl::Manager * _manager,
                              const kDB::Repository::VersionControl::Revision&)>
    get_revision_info;

  get_revision_info = [&get_revision_info, &revisions_infos, &agents, add_to_list](
                        kDB::Repository::VersionControl::Manager* _manager,
                        const kDB::Repository::VersionControl::Revision& _rev) -> RevisionInfo&
  {
    RevisionInfo& ri = revisions_infos[_rev.hash()];
    if(not ri.initialized)
    {
      ri.initialized = true;
      ri.propagation_times.resize(agents.size());
      ri.first_time = knowCore::Timestamp::now();
      ri.included_revisions.insert(_rev.hash());
      QList<kDB::Repository::VersionControl::Delta> deltas = _rev.deltas();
      ri.is_merge = deltas.size() > 1;
      for(const kDB::Repository::VersionControl::Delta& delta : deltas)
      {
        RevisionInfo& rip
          = get_revision_info(_manager, _manager->revision(delta.parent()).expect_success());
        add_to_list(&ri.included_revisions, rip.included_revisions);
      }
    }
    return ri;
  };

  for(std::size_t iteration = 0; iteration < parameters.iteration_max_count; ++iteration)
  {
    clog_info("Starting iteration {}", iteration);
    std::chrono::time_point<std::chrono::system_clock> loop_start
      = std::chrono::system_clock().now();
    // TODO fill bench
    QList<QByteArray> heads;
    QList<knowCore::Timestamp> elections;
    QList<QByteArray> revisions_hash;
    QList<QByteArray> revisions_merged;
    QList<QByteArray> revisions_heads;
    for(Agent* agent : agents)
    {
      clog_info(
        "Checking using agent {} using db {} {} has tip hash {} (content {}) and heads {} current "
        "number of unique heads {}",
        agent->agent_id, agent->store.store()->directory().path(), agent->connection.serverUuid(),
        agent->triplesStore.versionControlManager()->tipHash(), agent->triplesStore.contentHash(),
        agent->triplesStore.versionControlManager()->heads(), revisions_heads.size());
      QList<kDB::Repository::VersionControl::Revision> revisions
        = agent->triplesStore.versionControlManager()->revisions().expect_success();
      for(const kDB::Repository::VersionControl::Revision& rev : revisions)
      {
        RevisionInfo& ri = get_revision_info(agent->triplesStore.versionControlManager(), rev);
        if(not revisions_hash.contains(rev.hash()))
        {
          revisions_hash.append(rev.hash());
          if(ri.is_merge)
          {
            revisions_merged.append(rev.hash());
          }
        }
        if(not ri.is_merge)
        {
          if(not ri.propagation_times[agent->agent_id].isValid())
          {
            if(get_revision_info(
                 agent->triplesStore.versionControlManager(),
                 agent->triplesStore.versionControlManager()->tip().expect_success())
                 .included_revisions.contains(rev.hash()))
            {
              ri.propagation_times[agent->agent_id] = knowCore::Timestamp::now();
              ++ri.propagation_count;
              if(ri.propagation_count == agents.size())
              {
                ri.propagation_time
                  = ri.first_time.intervalTo<knowCore::Seconds>(knowCore::Timestamp::now())
                      .count()
                      .toDouble();
              }
            }
          }
        }
      }
      for(const kDB::Repository::VersionControl::Revision& rev : knowCore::dereference(
            agent->triplesStore.versionControlManager()->heads().expect_success()))
      {
        if(not revisions_heads.contains(rev.hash()))
        {
          clog_debug("Unknown head: {}", rev.hash());
          revisions_heads.append(rev.hash());
        }
      }
      for(const knowCore::Timestamp& ts : agent->comm_interface->elections())
      {
        if(not elections.contains(ts))
        {
          elections.append(ts);
        }
      }
    }
    double propagation_rate = 0.0;
    double average_propagation_times = 0.0;
    int average_propagation_rate_count = 0;
    int average_propagation_times_count = 0;
    for(auto it = revisions_infos.begin(); it != revisions_infos.end(); ++it)
    {
      const RevisionInfo& rpi = it.value();
      if(not rpi.is_merge)
      {
        propagation_rate += rpi.propagation_count / double(agents.size());
        ++average_propagation_rate_count;
        if(rpi.propagation_count == agents.size())
        {
          average_propagation_times += rpi.propagation_time;
          ++average_propagation_times_count;
        }
      }
    }
    propagation_rate /= average_propagation_rate_count;
    average_propagation_times /= average_propagation_times_count;
    bench.start_iteration(iteration, is_connected(parameters), elections.size(),
                          revisions_hash.size(), revisions_merged.size(), revisions_heads.size(),
                          propagation_rate, average_propagation_times,
                          average_propagation_times_count);
    bench.end_iteration();

    if(iteration < parameters.iteration_with_edit_max_count)
    {
      // Create revisions for each agents
      for(Agent* agent : agents)
      {
        kDB::Repository::Transaction transac(agent->connection);
        QList<knowRDF::Triple> triples = agent->triplesStore.triples().expect_success();
        for(std::size_t i = 0;
            i < std::min<std::size_t>(parameters.triples_removed, triples.size()); ++i)
        {
          std::uniform_int_distribution<std::size_t> distribution(0, triples.size() - 1);
          knowRDF::Triple t = triples.at(distribution(generator_triples));
          agent->triplesStore.remove(t, transac).expect_success();
        }
        for(std::size_t i = 0; i < parameters.triples_added; ++i)
        {
          agent->triplesStore
            .insert(knowCore::Uri(QUuid::createUuid().toString()), QUuid::createUuid().toString(),
                    knowCore::Uri(QUuid::createUuid().toString()), transac)
            .expect_success();
        }
        transac.commit().expect_success();
      }
    }
    else
    {
      // for(Agent* agent : agents)
      //{
      //   agent->comm_interface->enableReliableCommunication();
      // }
    }
    std::chrono::time_point<std::chrono::system_clock> loop_end = std::chrono::system_clock().now();
    std::chrono::nanoseconds loop_duration = loop_end - loop_start;
    std::chrono::nanoseconds loop_sleep
      = std::chrono::duration_cast<std::chrono::nanoseconds>(parameters.loop_sleep);
    if(loop_duration > loop_sleep)
    {
      clog_error("Loop took too long! {}ns instead of {}ns", loop_duration.count(),
                 loop_sleep.count());
    }
    else
    {
      std::this_thread::sleep_for(loop_sleep - loop_duration);
    }
  }
  std::ofstream csv_o;
  csv_o.open(qPrintable(parser.positionalArguments()[0]), std::ios_base::out);
  bench.print_results(csv_o, true);

  qDeleteAll(agents);

  return 0;
}
