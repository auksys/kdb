#include "Messages.h"

namespace kDB
{
  namespace Repository
  {
    class TripleStore;
  }
} // namespace kDB

namespace kDBRDFGraphSynchronisation
{
  class AbstractCommunicationInterface;
  /**
   * @ingroup kDBRDFGraphSynchronisation
   *
   * The synchronisation manager is used to syncrhonise the content of a triple store with other
   * agents. It implements the synchronisation protocol described in: "RGS⊕ v2.0: RDF Graph
   * Synchronization under very Unreliable Communication".
   */
  class SynchronisationManager
  {
  public:
    /**
     * Construct a \ref SynchronisationManager
     */
    SynchronisationManager(const QString& _name, const kDB::Repository::TripleStore& _store,
                           AbstractCommunicationInterface* _communicationInterface);
    ~SynchronisationManager();
    void handleStatus(const Status& _status);
    void handleVote(const Vote& _vote);
    void handleRevisionsRequest(const RevisionsRequest& _request);
    void handleRevisions(const Revisions& _revisions);
    void start();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBRDFGraphSynchronisation
