#include "MQTTCommunicationInterface.h"

#include <QCryptographicHash>

#include <Cyqlops/MQTT/Client.h>
#include <Cyqlops/MQTT/Message.h>

#include <kDB/Repository/TripleStore.h>

#include "SynchronisationManager.h"

using namespace kDBRDFGraphSynchronisation;

struct MQTTCommunicationInterface::Private
{
  Cyqlops::MQTT::Client* client = nullptr;
  QString graphname, graphname_topic;
  SynchronisationManager* manager = nullptr;

  QString statusTopic, mergeMasterElectionTopic, revisionsTopic, revisionRequestTopic;
};

QString hash_graph_name(const QString& _graph_name)
{
  QCryptographicHash graph_name_hash(QCryptographicHash::Sha3_512);
  graph_name_hash.addData(_graph_name.toLatin1());
  return "h" + QString::fromLatin1(graph_name_hash.result().toHex());
}

MQTTCommunicationInterface::MQTTCommunicationInterface(const QString& _server_address,
                                                       const QString& _graphname)
    : d(new Private)
{
  d->client = new Cyqlops::MQTT::Client();
  d->client->setServerAddress(_server_address);
  d->graphname = _graphname;
  d->graphname_topic = hash_graph_name(d->graphname);

  d->statusTopic = "/kDB/graph_synchronisation/" + d->graphname_topic + "/status";
  d->mergeMasterElectionTopic
    = "/kDB/graph_synchronisation/" + d->graphname_topic + "/merge_master_election";
  d->revisionsTopic = "/kDB/graph_synchronisation/" + d->graphname_topic + "/revisions";
  d->revisionRequestTopic
    = "/kDB/graph_synchronisation/" + d->graphname_topic + "/revisions_request";

  d->client->subscribe(d->statusTopic);
  d->client->subscribe(d->mergeMasterElectionTopic);
  d->client->subscribe(d->revisionsTopic);
  d->client->subscribe(d->revisionRequestTopic);

  QObject::connect(
    d->client, &Cyqlops::MQTT::Client::messageReceived,
    [this](const QString& _topic, const Cyqlops::MQTT::Message& _message)
    {
      if(_topic == d->statusTopic)
      {
        QVariantMap m = _message.parsed().toMap();
        QList<QByteArray> heads;
        for(const QVariant& hash : m["heads"].toList())
        {
          heads.append(QByteArray::fromHex(hash.toByteArray()));
        }
        d->manager->handleStatus(
          {m["name"].toString(), QUuid::fromRfc4122(QByteArray::fromHex(m["uuid"].toByteArray())),
           QByteArray::fromHex(m["publicKey"].toByteArray()),
           QByteArray::fromHex(m["revisionHash"].toByteArray()), heads, m["mergeMaster"].toBool()});
      }
      else if(_topic == d->mergeMasterElectionTopic)
      {
        QVariantMap m = _message.parsed().toMap();
        d->manager->handleVote(
          {QUuid::fromRfc4122(QByteArray::fromHex(m["voter"].toByteArray())),
           QByteArray::fromHex(m["signature"].toByteArray()),
           QUuid::fromRfc4122(QByteArray::fromHex(m["candidate"].toByteArray())), m["leg"].toInt(),
           knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(m["timestamp"].value<qint64>()),
           knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(
             m["electionTime"].value<qint64>())});
      }
      else if(_topic == d->revisionsTopic)
      {
        QVariantList l = _message.parsed().toList();
        QList<Revision> revisions;
        for(const QVariant& rev_v : l)
        {
          QVariantMap m = rev_v.toMap();

          QList<kDBRDFGraphSynchronisation::Delta> deltas;

          for(const QVariant& delta : m["deltas"].toList())
          {
            QList<kDBRDFGraphSynchronisation::Signature> signatures;
            QVariantMap dm = delta.toMap();
            for(const QVariant& sign : dm["signatures"].toList())
            {
              QVariantMap sm = sign.toMap();
              signatures.append(
                {QUuid::fromRfc4122(QByteArray::fromHex(sm["author"].toByteArray())),
                 knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(
                   sm["timestamp"].value<qint64>()),
                 QByteArray::fromHex(sm["signature"].toByteArray())});
            }
            deltas.append(kDBRDFGraphSynchronisation::Delta{
              QByteArray::fromHex(dm["parent"].toByteArray()),
              QByteArray::fromHex(dm["hash"].toByteArray()),
              QByteArray::fromHex(dm["delta"].toByteArray()), signatures});
          }
          revisions.append({QByteArray::fromHex(m["hash"].toByteArray()),
                            QByteArray::fromHex(m["contentHash"].toByteArray()),
                            m["historicity"].toInt(), deltas});
        }
        d->manager->handleRevisions({revisions});
      }
      else if(_topic == d->revisionRequestTopic)
      {
        QVariantMap m = _message.parsed().toMap();
        QList<QByteArray> hashes;
        for(const QVariant& hash : m["hashes"].toList())
        {
          hashes.append(QByteArray::fromHex(hash.toByteArray()));
        }
        d->manager->handleRevisionsRequest(
          {QUuid::fromRfc4122(QByteArray::fromHex(m["requester"].toByteArray())), hashes});
      }
    });

  d->client->start();
}

MQTTCommunicationInterface::~MQTTCommunicationInterface()
{
  delete d->client;
  delete d;
}

void MQTTCommunicationInterface::sendStatus(const Status& _status)
{
  QVariantMap m;
  m["name"] = _status.name;
  m["uuid"] = _status.uuid.toRfc4122().toHex();
  m["revisionHash"] = _status.revisionHash.toHex();
  QVariantList heads;
  for(const QByteArray& hash : _status.heads)
  {
    heads.append(hash.toHex());
  }
  m["heads"] = heads;
  m["mergeMaster"] = _status.mergeMaster;
  m["publicKey"] = _status.publicKey.toHex();
  d->client->publish(d->statusTopic, m);
}

void MQTTCommunicationInterface::sendVote(const Vote& _vote)
{
  QVariantMap m;
  m["voter"] = _vote.voter.toRfc4122().toHex();
  m["signature"] = _vote.signature.toHex();
  m["candidate"] = _vote.candidate.toRfc4122().toHex();
  m["leg"] = _vote.leg;
  m["timestamp"]
    = _vote.timestamp.toEpoch<knowCore::NanoSeconds>().count().toInt64().expect_success();
  m["electionTime"]
    = _vote.electionTime.toEpoch<knowCore::NanoSeconds>().count().toInt64().expect_success();
  d->client->publish(d->mergeMasterElectionTopic, m);
}

void MQTTCommunicationInterface::sendRevisions(const Revisions& _revisions)
{
  QVariantList list;
  for(const Revision& revision : _revisions.revisions)
  {
    QVariantMap m;
    m["hash"] = revision.hash.toHex();
    m["contentHash"] = revision.contentHash.toHex();
    m["historicity"] = revision.historicity;
    QVariantList deltas;
    for(const Delta& delta : revision.deltas)
    {
      QVariantList signatures;
      for(const Signature& sign : delta.signatures)
      {
        QVariantMap ms;
        ms["author"] = sign.author.toRfc4122().toHex();
        ms["timetamp"]
          = sign.timestamp.toEpoch<knowCore::NanoSeconds>().count().toInt64().expect_success();
        ms["signature"] = sign.signature.toHex();
        signatures.append(ms);
      }
      QVariantMap dm;
      dm["parent"] = delta.parent.toHex();
      dm["hash"] = delta.hash.toHex();
      dm["delta"] = delta.delta.toHex();
      dm["signatures"] = signatures;
      deltas.push_back(dm);
    }
    m["deltas"] = deltas;
    list.append(m);
  }
  d->client->publish(d->revisionsTopic, list);
}

void MQTTCommunicationInterface::sendRevisionsRequest(const RevisionsRequest& _request)
{
  QVariantMap m;
  m["requester"] = _request.requester.toRfc4122().toHex();
  QVariantList hashes;
  for(const QByteArray& hash : _request.hashes)
  {
    hashes.append(hash.toHex());
  }
  m["hashes"] = hashes;
  d->client->publish(d->revisionRequestTopic, m);
}

SynchronisationManager* MQTTCommunicationInterface::startSynchronisation(
  const QString& _name, const QString& _server_address, const kDB::Repository::TripleStore& _store)
{
  MQTTCommunicationInterface* ci = new MQTTCommunicationInterface(_server_address, _store.uri());

  SynchronisationManager* manager = new SynchronisationManager(_name, _store, ci);
  ci->d->manager = manager;

  return manager;
}
