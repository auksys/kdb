#pragma once

#include <QFlags>
#include <QSharedDataPointer>

#include <kDBGIS/Forward.h>

#include <clog_qt>
#include <cres_qt>

#include <knowValues/Forward.h>

#include "Point.h"

namespace kDBPointClouds
{
  class Patch
  {
  public:
    enum class PGWKBFlag
    {
      Default = 0,
      IgnorePCID = 1
    };
    Q_DECLARE_FLAGS(PGWKBFlags, PGWKBFlag)
  public:
    Patch();
    Patch(const PointSpecification& _specification, const QByteArray& _data = QByteArray());
    Patch(const Patch& _rhs);
    Patch& operator=(const Patch& _rhs);
    ~Patch();
    bool operator==(const Patch& _rhs) const;
    /**
     * @return the \ref PointSpecification defining this point cloud
     */
    PointSpecification specification() const;
    /**
     * Synchronise the \ref PointSpecification with the database. Need to be done prior inserting a
     * patch in the database.
     */
    bool synchroniseSpecification(const kDB::Repository::Connection& _connection);
    /**
     * @return the \ref Cartography::CoordinateSystem associated with this point cloud
     */
    Cartography::CoordinateSystem coordinateSystem() const;
    /**
     * Reserve memory for inserting @p _pts
     */
    void reserve(std::size_t _pts);
    void append(const Point& _point);
    template<typename... Types>
    inline void append(const std::tuple<Types...>& _values);
    template<typename... Types>
    void append(Types... _values)
    {
      append(std::make_tuple(_values...));
    }
    std::size_t pointsCount() const;
    template<typename... Types>
    inline std::tuple<Types...> get(std::size_t _index) const;
    Point get(std::size_t _index) const;
    /**
     * Convert to a (pgPointCloud) WKB representation of a point cloud.
     *
     * @param _store_pcid indicate weht
     */
    QByteArray toPGWKB(PGWKBFlags _flags = PGWKBFlag::Default) const;
    static cres_qresult<Patch> fromPGWKB(const QByteArray&,
                                         const PointSpecification& _pointSpecification,
                                         PGWKBFlags _flags = PGWKBFlag::Default);
    /**
     * Get the specification from the @ref _connection . @ref _data should contains
     * a valid pcid, corresponding to a \ref PointSpecification that is stored in
     * the database.
     *
     * \ref _flags should not have \ref PGWKBFlag::IgnorePCID set.
     */
    static cres_qresult<Patch> fromPGWKB(const QByteArray& _data,
                                         const kDB::Repository::Connection& _connection,
                                         PGWKBFlags _flags = PGWKBFlag::Default);
    /**
     * @return true if the patch is valid
     */
    bool isValid() const;
    /**
     * @return the data representing the point cloud
     */
    QByteArray data() const;
    /**
     * Transform the point cloud to a different coordinate system and apply a translation
     */
    Patch transform(const Cartography::CoordinateSystem& _coordinateSystem, double _tx = 0.0,
                    double _ty = 0.0, double _tz = 0.0) const;
    cres_qresult<QByteArray> md5() const;
  public:
    /**
     * Convert this patch to a \ref knowValues::Values::PointCloud.
     */
    knowValues::Values::PointCloud toValuesPointCloud(const knowGIS::Pose& _pose,
                                                      const knowGIS::GeometryObject& _geometry,
                                                      const knowCore::Timestamp& _timestamp);
    /**
     * Convert this patch to a \ref knowValues::Values::PointCloud.
     */
    knowValues::Values::Lidar3DScan toValuesLidar3DScan(const knowGIS::Pose& _pose,
                                                        const knowCore::Timestamp& _timestamp);
    /**
     * Create a patch from \ref knowValues::Values::PointCloud.
     */
    static Patch create(const knowValues::Values::PointCloud& _rhs);
  private:
    /**
     * @return the position to the next point, and ensure that there is enough space in the storage
     * and increment the number of points
     */
    std::size_t prepareNextPointInsertion();
    std::size_t pointSize() const;
    QByteArray& data_ref();
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };

  template<typename... Types>
  void Patch::append(const std::tuple<Types...>& _values)
  {
    Point::setter<sizeof...(Types)>::doit(prepareNextPointInsertion(), data_ref(), _values);
  }
  template<typename... Types>
  inline std::tuple<Types...> Patch::get(std::size_t _index) const
  {
    std::tuple<Types...> values;
    Point::getter<sizeof...(Types)>::doit(_index * pointSize(), data(), values);
    return values;
  }
} // namespace kDBPointClouds

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(kDBPointClouds, Patch);
