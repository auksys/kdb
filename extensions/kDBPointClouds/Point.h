#ifndef _KDBPOINTCLOUDS_POINT_H_
#define _KDBPOINTCLOUDS_POINT_H_

#include <QByteArray>
#include <QSharedDataPointer>

#include <tuple>

namespace kDBPointClouds
{
  class PointSpecification;

  class Point
  {
    friend class Patch;
  public:
    Point();
    Point(const PointSpecification& _specification);
    Point(const Point& _rhs);
    Point& operator=(const Point& _rhs);
    ~Point();
  private:
    template<int Index>
    struct setter;
    template<int Index>
    struct getter;
  public:
    template<typename... Types>
    inline void set(const std::tuple<Types...>& _values);
    template<typename... Types>
    void set(Types... _values)
    {
      set(std::make_tuple(_values...));
    }
    template<typename... Types>
    inline std::tuple<Types...> get() const;
    bool operator==(const Point& _point) const;
  private:
    QByteArray& array();
    QByteArray array() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };

  template<>
  struct Point::setter<0>
  {
    template<typename... Types>
    inline static void doit(std::size_t _pos, QByteArray& _data,
                            const std::tuple<Types...>& _values)
    {
      Q_UNUSED(_pos);
      Q_UNUSED(_data);
      Q_UNUSED(_values);
    }
  };

  template<int Index_>
  struct Point::setter
  {
    template<typename... Types>
    inline static void doit(std::size_t _pos, QByteArray& _data,
                            const std::tuple<Types...>& _values)
    {
      constexpr int Index = sizeof...(Types) - Index_;
      using ValType = typename std::tuple_element<Index, std::tuple<Types...>>::type;
      ValType val = std::get<Index>(_values);
      std::size_t val_size = sizeof(ValType);
      memcpy(_data.data() + _pos, &val, val_size);
      Point::setter<Index_ - 1>::doit(_pos + val_size, _data, _values);
    }
  };

  template<>
  struct Point::getter<0>
  {
    template<typename... Types>
    inline static void doit(std::size_t _pos, const QByteArray& _data,
                            std::tuple<Types...>& _values)
    {
      Q_UNUSED(_pos);
      Q_UNUSED(_data);
      Q_UNUSED(_values);
    }
  };

  template<int Index_>
  struct Point::getter
  {
    template<typename... Types>
    inline static void doit(std::size_t _pos, const QByteArray& _data,
                            std::tuple<Types...>& _values)
    {
      constexpr int Index = sizeof...(Types) - Index_;
      using ValType = typename std::tuple_element<Index, std::tuple<Types...>>::type;
      ValType val;
      std::size_t val_size = sizeof(ValType);
      memcpy(&val, _data.data() + _pos, val_size);
      std::get<Index>(_values) = val;
      Point::getter<Index_ - 1>::doit(_pos + val_size, _data, _values);
    }
  };

  template<typename... Types>
  void Point::set(const std::tuple<Types...>& _values)
  {
    setter<sizeof...(Types)>::doit(0, array(), _values);
  }
  template<typename... Types>
  std::tuple<Types...> Point::get() const
  {
    std::tuple<Types...> values;
    getter<sizeof...(Types)>::doit(0, array(), values);
    return values;
  }
} // namespace kDBPointClouds

#endif
