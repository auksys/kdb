public:
static cres_qresult<PointCloudRecord>
  create(const kDB::Repository::QueryConnectionInfo& _connection, const QString& _datasetUri,
         const knowValues::Values::PointCloud& _pointCloud);
