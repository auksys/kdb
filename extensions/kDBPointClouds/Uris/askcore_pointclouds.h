#include <knowCore/Uris/Uris.h>

#define KDBPOINTCLOUDS_ASKCORE_POINTCLOUDS_URIS(F, ...)                                            \
  F(__VA_ARGS__, patch)                                                                            \
  F(__VA_ARGS__, intersection)                                                                     \
  F(__VA_ARGS__, union_, "union")

KNOWCORE_ONTOLOGY_URIS(kDBPointClouds::Uris, KDBPOINTCLOUDS_ASKCORE_POINTCLOUDS_URIS,
                       askcore_pointclouds, "http://askco.re/pointclouds#")
