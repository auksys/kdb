CREATE OR REPLACE FUNCTION kdb_samepcpatch(pcpatch, pcpatch) RETURNS boolean
   LANGUAGE sql IMMUTABLE
   AS 'SELECT PC_AsText($1) = PC_AsText($2)';

CREATE OR REPLACE FUNCTION kdb_pcpatch_cmp(pcpatch, pcpatch) RETURNS int
   LANGUAGE sql IMMUTABLE
   AS 'SELECT kdb_strcmp(PC_AsText($1), PC_AsText($2))';

CREATE FUNCTION kdb_pc_create_operators() RETURNS int AS $$
DECLARE
BEGIN
  BEGIN
    CREATE OPERATOR = (LEFTARG=pcpatch, RIGHTARG=pcpatch, PROCEDURE = kdb_samepcpatch);

    CREATE OPERATOR CLASS kdb_pcpatch_hash_ops DEFAULT FOR TYPE pcpatch USING btree AS
      OPERATOR 3 =(pcpatch,pcpatch),
      FUNCTION 1 kdb_pcpatch_cmp(pcpatch, pcpatch);
  EXCEPTION WHEN OTHERS THEN
  END;
  RETURN 0;
END;
$$ LANGUAGE plpgsql;

SELECT kdb_pc_create_operators();

DROP FUNCTION kdb_pc_create_operators();
