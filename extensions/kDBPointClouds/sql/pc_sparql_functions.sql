CREATE OR REPLACE FUNCTION kdbpointclouds_get_srid(a pcpatch) RETURNS integer AS $$
  SELECT srid FROM pointcloud_formats WHERE pcid = PC_PCId(a)
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdbpointclouds_intersection(pc kdb_rdf_term, boundary kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT kdb_rdf_term_create(PC_Intersection(((pc).value).pcpatch, ST_Transform(((boundary).value).geometry, kdbpointclouds_get_srid(((pc).value).pcpatch))))
$$ LANGUAGE SQL IMMUTABLE;




