#include <kDB/Forward.h>

namespace kDBPointClouds
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection);
}
