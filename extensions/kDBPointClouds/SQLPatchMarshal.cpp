#include <QString>
#include <QVariant>

#include <kDB/Repository/AbstractBinaryMarshal.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/BinaryMarshalsRegistry.h>

#include <Cartography/CoordinateSystem.h>

#include "Patch.h"
#include "PointSpecification.h"

namespace kDBPointClouds
{

  class SQLPatchMarshal : public kDB::Repository::AbstractBinaryMarshal
  {
  public:
    SQLPatchMarshal()
        : AbstractBinaryMarshal("pcpatch", kDBPointClouds::PatchMetaTypeInformation::uri(),
                                Mode::ToVariant | Mode::ToByteArray)
    {
    }
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);
      _oidName = "pcpatch";
      Patch p = _source.value<Patch>().expect_success();
      if(p.synchroniseSpecification(_connection))
      {
        return cres_success(p.toPGWKB());
      }
      else
      {
        return cres_failure("Failed to synchronise point specification");
      }
    }
    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override
    {
      cres_try(Patch patch, Patch::fromPGWKB(_source, _connection));
      return cres_success(knowCore::Value::fromValue(patch));
    }
  };

  KDB_REGISTER_SQL_MARSHAL(SQLPatchMarshal)
} // namespace kDBPointClouds
