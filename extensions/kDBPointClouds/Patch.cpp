#include "Patch.h"

#include <Cyqlops/Crypto/Hash.h>

#include <QCborMap>

#include <Cartography/CoordinateSystem.h>

#include <clog_qt>
#include <knowGIS/Point.h>
#include <knowValues/Values.h>

#include "PointSpecification.h"

using namespace kDBPointClouds;

struct Patch::Private : public QSharedData
{
  Private() : pointsCount(0) {}
  PointSpecification specification;
  Cartography::CoordinateSystem coordinates;
  QByteArray data;
  std::size_t pointsCount;
};

Patch::Patch() : d(new Private) {}

Patch::Patch(const PointSpecification& _specification, const QByteArray& _data) : d(new Private)
{
  d->specification = _specification;
  d->coordinates = _specification.coordinateSystem();
  d->data = _data;
  d->pointsCount = _data.size() / _specification.size();
  if((_data.size() % _specification.size()) != 0)
  {
    clog_error("Possible invalid point cloud size of data array is not a multiple of the point "
               "specification: array size = {} point size = {}",
               _data.size(), _specification.size());
  }
}

Patch::Patch(const Patch& _rhs) : d(_rhs.d) {}

Patch& Patch::operator=(const Patch& _rhs)
{
  d = _rhs.d;
  return *this;
}

Patch::~Patch() {}

bool Patch::operator==(const Patch& _rhs) const
{
  std::size_t s = d->pointsCount * pointSize();
  return d == _rhs.d
         or (d->specification == _rhs.d->specification and d->pointsCount == _rhs.d->pointsCount
             and memcmp(d->data.data(), _rhs.d->data.data(), s) == 0);
}

PointSpecification Patch::specification() const { return d->specification; }

bool Patch::synchroniseSpecification(const kDB::Repository::Connection& _connection)
{
  return d->specification.synchronise(_connection);
}

void Patch::reserve(std::size_t _pts)
{
  int newsize = _pts * d->specification.size();
  if(newsize > d->data.size())
  {
    QByteArray newarray;
    newarray.resize(newsize);
    memcpy(newarray.data(), d->data.data(), d->pointsCount * d->specification.size());
    d->data = newarray;
  }
}

void Patch::append(const Point& _point)
{
  std::size_t next = prepareNextPointInsertion();
  memcpy(d->data.data() + next, _point.array().data(), d->specification.size());
}

Point Patch::get(std::size_t _index) const
{
  Point pt(d->specification);
  memcpy(pt.array().data(), d->data.data() + _index * pointSize(), pointSize());
  return pt;
}

std::size_t Patch::prepareNextPointInsertion()
{
  std::size_t s = d->specification.size();
  std::size_t next = d->pointsCount * s;
  if(next + s > std::size_t(d->data.size()))
  {
    reserve(2 * d->pointsCount + 1);
  }
  ++d->pointsCount;
  return next;
}

QByteArray Patch::data() const { return d->data; }

QByteArray& Patch::data_ref() { return d->data; }

std::size_t Patch::pointsCount() const { return d->pointsCount; }

std::size_t Patch::pointSize() const { return d->specification.size(); }

cres_qresult<Patch> Patch::fromPGWKB(const QByteArray& _data,
                                     const PointSpecification& _pointSpecification,
                                     PGWKBFlags _flags)
{
  if(not _pointSpecification.isValid())
  {
    return cres_failure("Cannot create a patch with an invalid specification.");
  }
  Patch patch(_pointSpecification);

  uint8_t endianess = *((uint8_t*)_data.data());
  uint32_t pcid = *((uint32_t*)(_data.data() + 1));
  uint32_t compression = *((uint32_t*)(_data.data() + 5));
  uint32_t points = *((uint32_t*)(_data.data() + 9));
  Q_UNUSED(endianess);
  Q_UNUSED(pcid);
  Q_UNUSED(compression);
  if(QSysInfo::ByteOrder == QSysInfo::BigEndian)
  {
    clog_assert(endianess == 0);
  }
  else
  {
    clog_assert(endianess == 1);
  }
  clog_assert(compression == 0);

  if(not _flags.testFlag(PGWKBFlag::IgnorePCID))
  {
    if(pcid != (uint32_t)patch.d->specification.pcid())
    {
      return cres_failure(
        "The pcid '{}' stored in the data is different from the given point specification '{}'",
        pcid, patch.d->specification.pcid());
    }
  }
  patch.reserve(points);
  patch.d->pointsCount = points;
  const char* data_start = _data.data() + 13;
  std::size_t copy_len = patch.d->pointsCount * patch.pointSize();
  if(copy_len + 13 > (std::size_t)_data.size())
  {
    return cres_failure("Invalid WKB, not enough data");
  }
  if(copy_len + 13 < (std::size_t)_data.size())
  {
    return cres_failure("Invalid WKB, too much data");
  }
  std::copy(data_start, data_start + copy_len, patch.d->data.data());
  return cres_success(patch);
}

cres_qresult<Patch> Patch::fromPGWKB(const QByteArray& _data,
                                     const kDB::Repository::Connection& _connection,
                                     PGWKBFlags _flags)
{
  clog_assert(not _flags.testFlag(PGWKBFlag::IgnorePCID));
  uint32_t pcid = *((uint32_t*)(_data.data() + 1));
  PointSpecification ps = PointSpecification::get(_connection, pcid);
  if(ps.isValid())
  {
    return fromPGWKB(_data, ps, _flags);
  }
  else
  {
    return cres_failure("Invalid point specification with pcid '{}'", pcid);
  }
}

QByteArray Patch::toPGWKB(PGWKBFlags _flags) const
{
  QByteArray arr;
  arr.resize(1 + 4 + 4 + 4 + d->pointsCount * pointSize());

  if(QSysInfo::ByteOrder == QSysInfo::BigEndian)
  {
    *((uint8_t*)arr.data()) = 0;
  }
  else
  {
    *((uint8_t*)arr.data()) = 1;
  }

  if(_flags.testFlag(PGWKBFlag::IgnorePCID))
  {
    *((uint32_t*)(arr.data() + 1)) = std::numeric_limits<uint32_t>::max();
  }
  {
    *((uint32_t*)(arr.data() + 1)) = d->specification.pcid();
  }
  *((uint32_t*)(arr.data() + 5)) = 0;
  *((uint32_t*)(arr.data() + 9)) = d->pointsCount;
  memcpy(arr.data() + 13, d->data.data(), d->pointsCount * pointSize());

  double val;
  memcpy(&val, d->data.data(), sizeof(double));

  return arr;
}

bool Patch::isValid() const { return d->specification.isValid(); }

Patch Patch::transform(const Cartography::CoordinateSystem& _coordinateSystem, double _tx,
                       double _ty, double _tz) const
{
  PointSpecification ps = d->specification;

  QList<PointSpecification::Dimension> dimensions = ps.dimensions();
  if(dimensions.size() < 3 or dimensions[0].name != "X"
     or dimensions[0].type != PointSpecification::DataType::Float32 or dimensions[1].name != "Y"
     or dimensions[1].type != PointSpecification::DataType::Float32 or dimensions[2].name != "Z"
     or dimensions[2].type != PointSpecification::DataType::Float32)
  {
    clog_warning("Only point cloud starting with XYZ in float32 are supported for transform!");
    return Patch();
  }

  Cartography::CoordinateSystem current_cs = d->specification.coordinateSystem();
  const bool different_cs = current_cs != _coordinateSystem;
  if(different_cs)
  {
    ps = PointSpecification(d->specification, _coordinateSystem);
  }
  Patch opatch(ps);
  opatch.reserve(pointsCount());

  for(std::size_t i = 0; i < pointsCount(); ++i)
  {
    kDBPointClouds::Point pt = get(i);
    auto [pt_x, pt_y, pt_z] = pt.get<float, float, float>();

    if(different_cs)
    {
      knowGIS::Point pt(pt_x, pt_y, pt_z, current_cs);
      pt = pt.transform(_coordinateSystem);
      pt_x = pt.x();
      pt_y = pt.y();
      pt_z = pt.z();
    }

    pt.set<float, float, float>(pt_x + _tx, pt_y + _ty, pt_z + _tz);
    opatch.append(pt);
  }

  return opatch;
}

cres_qresult<QByteArray> Patch::md5() const
{
  cres_try(QByteArray ps_md5, d->specification.md5());
  return cres_success(Cyqlops::Crypto::Hash::md5(ps_md5, d->coordinates.srid(), d->data));
}

namespace
{

  template<typename _ValueT_>
  typename _ValueT_::Builder toValuesPCLF3D(const Patch* p, const knowCore::Timestamp& _timestamp)
  {
    QList<knowValues::Values::Definitions::PointCloudField*> fields;

    for(const kDBPointClouds::PointSpecification::Dimension& dim : p->specification().dimensions())
    {
      knowValues::Values::Definitions::PointCloudField::DataType dataType
        = knowValues::Values::Definitions::PointCloudField::DataType::UnsignedInteger8;
#define DI_CONV(_name_)                                                                            \
  case kDBPointClouds::PointSpecification::DataType::_name_:                                       \
    dataType = knowValues::Values::Definitions::PointCloudField::DataType::_name_;                 \
    break;
      switch(dim.type)
      {
        DI_CONV(UnsignedInteger8)
        DI_CONV(Integer8)
        DI_CONV(UnsignedInteger16)
        DI_CONV(Integer16)
        DI_CONV(UnsignedInteger32)
        DI_CONV(Integer32)
        DI_CONV(UnsignedInteger64)
        DI_CONV(Integer64)
        DI_CONV(Float32)
        DI_CONV(Float64)
      }
#undef DI_CONV
      fields.append(knowValues::Values::Definitions::PointCloudField::create()
                      .setName(dim.name)
                      .setOffset(dim.offset)
                      .setScale(dim.scale)
                      .setCount(1)
                      .setDataType(dataType));
    }

    return _ValueT_::create()
      .setTimestamp(_timestamp)
      .setFields(fields)
      .setData(p->data())
      .setPointsCount(p->pointsCount());
  }
} // namespace

knowValues::Values::PointCloud Patch::toValuesPointCloud(const knowGIS::Pose& _transformation,
                                                         const knowGIS::GeometryObject& _geometry,
                                                         const knowCore::Timestamp& _timestamp)
{
  return toValuesPCLF3D<knowValues::Values::PointCloud>(this, _timestamp)
    .setTransformation(knowGIS::GeoPose::from(_transformation))
    .setGeometry(_geometry);
}

knowValues::Values::Lidar3DScan Patch::toValuesLidar3DScan(const knowGIS::Pose& _pose,
                                                           const knowCore::Timestamp& _timestamp)
{
  return toValuesPCLF3D<knowValues::Values::Lidar3DScan>(this, _timestamp)
    .setPose(knowGIS::GeoPose::from(_pose));
}

Patch Patch::create(const knowValues::Values::PointCloud& _rhs)
{
  PointSpecification spec(-1, _rhs->pointsCoordinateSystem());
  for(const knowValues::Values::Definitions::PointCloudField* pcf : _rhs->fields())
  {
    kDBPointClouds::PointSpecification::DataType dataType
      = kDBPointClouds::PointSpecification::DataType::UnsignedInteger8;
#define DI_CONV(_name_)                                                                            \
  case knowValues::Values::Definitions::PointCloudField::DataType::_name_:                         \
    dataType = kDBPointClouds::PointSpecification::DataType::_name_;                               \
    break;
    switch(pcf->dataType())
    {
      DI_CONV(UnsignedInteger8)
      DI_CONV(Integer8)
      DI_CONV(UnsignedInteger16)
      DI_CONV(Integer16)
      DI_CONV(UnsignedInteger32)
      DI_CONV(Integer32)
      DI_CONV(UnsignedInteger64)
      DI_CONV(Integer64)
      DI_CONV(Float32)
      DI_CONV(Float64)
    }
#undef DI_CONV

    spec.addDimension(dataType, pcf->name(), QString(), pcf->scale());
  }
  return Patch(spec, _rhs->data());
}

#include "Uris/askcore_pointclouds.h"
#include <knowCore/MetaTypeImplementation.h>

#define POINTSPECIFICATION_KEY u8"pointspecification_xml"_kCs
#define DATA_KEY u8"key"_kCs

KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION(Patch)
cres_qresult<QByteArray> md5(const Patch& _value) const override { return _value.md5(); }
cres_qresult<QJsonValue> toJsonValue(const Patch& _value,
                                     const SerialisationContexts&) const override
{
  QJsonObject obj;
  obj[POINTSPECIFICATION_KEY] = _value.specification().toXML();
  obj[DATA_KEY] = QString::fromLatin1(_value.toPGWKB(Patch::PGWKBFlag::IgnorePCID).toBase64());

  return cres_success(obj);
}
cres_qresult<void> fromJsonValue(Patch* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_json_value.isObject())
  {
    QJsonObject json_object = _json_value.toObject();
    cres_try(PointSpecification pointSpecification,
             PointSpecification::fromXML(json_object.value(POINTSPECIFICATION_KEY).toString()));
    cres_try(Patch patch,
             Patch::fromPGWKB(QByteArray::fromBase64(_json_value.toString().toLatin1()),
                              pointSpecification, Patch::PGWKBFlag::IgnorePCID));
    *_value = patch;
    return cres_success();
  }
  else
  {
    return expectedError("object", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const Patch& _value,
                                     const SerialisationContexts&) const override
{
  QCborMap obj;
  obj[POINTSPECIFICATION_KEY] = _value.specification().toXML();
  obj[DATA_KEY] = _value.toPGWKB(Patch::PGWKBFlag::IgnorePCID);

  return cres_success(obj);
}
cres_qresult<void> fromCborValue(Patch* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_cbor_value.isMap())
  {
    QCborMap json_object = _cbor_value.toMap();
    cres_try(PointSpecification pointSpecification,
             PointSpecification::fromXML(json_object[POINTSPECIFICATION_KEY].toString()));
    cres_try(Patch patch, Patch::fromPGWKB(json_object[DATA_KEY].toByteArray(), pointSpecification,
                                           Patch::PGWKBFlag::IgnorePCID));
    *_value = patch;
    return cres_success();
  }
  else
  {
    return expectedError("map", _cbor_value);
  }
}
cres_qresult<QString> printable(const Patch& /*_value*/) const override
{
  return cres_success(u8"pointcloud"_kCs);
}
cres_qresult<QString> toRdfLiteral(const Patch& _value,
                                   const SerialisationContexts& _contexts) const override
{
  return toJsonString(_value, _contexts);
}
cres_qresult<void> fromRdfLiteral(Patch* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts& _contexts) const override
{
  return fromJsonString(_value, _serialised, _contexts);
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(Patch)

KNOWCORE_DEFINE_METATYPE(kDBPointClouds::Patch, kDBPointClouds::Uris::askcore_pointclouds::patch,
                         knowCore::MetaTypeTraits::None)
