#include "Initialise.h"

#include <QVariantHash>

#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uris/askcore_datatype.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/ActiveRecord/Version.h>
#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/SPARQLFunctionsManager.h>

#include <kDBGIS/Initialise.h>

#include "PatchFieldIO_p.h"
#include "PointCloud.h"
#include "Uris/askcore_pointclouds.h"

inline void kdbpointclouds_init_data() { Q_INIT_RESOURCE(kdbpointclouds_data); }

namespace kDBPointClouds
{

  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection)
  {
    cres_try(cres_ignore, kDB::Repository::Connection(_connection).enableExtension("kDBGIS"));
    kdbpointclouds_init_data();

    knowDBC::Query q = _connection.createSQLQuery();

    // Load extension
    q.setQuery("CREATE EXTENSION IF NOT EXISTS pointcloud;");

    knowDBC::Result result = q.execute();
    if(not result)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to initialize point clouds", result);
    }

    // Load pointcloud/postgis extension
    q.setQuery("CREATE EXTENSION IF NOT EXISTS pointcloud_postgis;");
    result = q.execute();
    if(not result)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to initialize point clouds", result);
    }

    cres_try(kDB::Repository::ActiveRecord::VersionRecord vr,
             kDB::Repository::ActiveRecord::VersionRecord::get(_connection, "kDBPointClouds"));

    if(vr.striclyLessThan(2, 0, 0))
    {
      // Load extra SQL files
      cres_try(cres_ignore, _connection.executeQueryFromFile(":/kdbpointclouds/sql/hash.sql"));

      // Create tables
      cres_try(cres_ignore, PointCloudRecord::createTable(_connection));

      q.setQuery("SELECT EXISTS( SELECT * FROM information_schema.tables WHERE table_schema = "
                 "'public' AND table_name = 'pointclouds');");
      result = q.execute();
      if(result and not result.value<bool>(0, 0).expect_success())
      {
        q.setQuery("CREATE TABLE pointclouds(id serial, patch PCPATCH, PRIMARY KEY (id))");
        result = q.execute();
        if(not result)
        {
          KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to create pointclouds table", result);
        }
      }
    }
    else if(vr.striclyLessThan(3, 1, 0))
    {
      knowDBC::Query q = _connection.createSQLQuery();
      // ADD and RENAME should be in different queries
      q.setQuery("ALTER TABLE \"pointclouds\" ADD COLUMN \"dataseturi\" TEXT, ADD COLUMN "
                 "\"timestamp\" bigint, ADD COLUMN \"longitude\" FLOAT8, ADD COLUMN \"latitude\" "
                 "FLOAT8, ADD COLUMN \"altitude\" FLOAT8, ADD COLUMN \"rotation\" float8[4]");
      KDB_REPOSITORY_EXECUTE_QUERY("Failed to update pointclouds table", q, false);
      q.setQuery("ALTER TABLE \"pointclouds\" RENAME COLUMN \"patch\" TO \"points\"");
      KDB_REPOSITORY_EXECUTE_QUERY("Failed to update pointclouds table", q, false);
      q.setQuery("ALTER TABLE \"pointclouds\" ADD COLUMN \"geometry\" geometry");
      KDB_REPOSITORY_EXECUTE_QUERY("Failed to update pointclouds table", q, false);
    }

    cres_try(
      cres_ignore,
      kDB::Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::registerField(
        _connection, knowCore::Uris::askcore_datatype::point_cloud, new PatchFieldIO,
        (quint64)
          kDB::Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::Operator::None,
        (quint64)
          kDB::Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::Feature::None));

    cres_try(cres_ignore, _connection.graphsManager()->loadViewsFrom(
                            ":/kdbpointclouds/sml/", knowCore::ValueHash().insert(
                                                       "%frame_base_uri", "base_uri"_kCu,
                                                       "%config_base_uri", "config_base_uri"_kCu)));

    // Initialise askcore_pointclouds functions
    if(vr.striclyLessThan(2, 0, 0))
    {
      cres_try(cres_ignore,
               _connection.executeQueryFromFile(":/kdbpointclouds/sql/pc_sparql_functions.sql"));
    }

    _connection.sparqlFunctionsManager()->registerFunction(
      Uris::askcore_pointclouds::intersection, "kdbpointclouds_intersection",
      knowCore::Uris::askcore_datatype::literal, knowCore::Uris::askcore_datatype::literal,
      knowCore::Uris::askcore_datatype::literal);

    _connection.sparqlFunctionsManager()->registerFunction(
      Uris::askcore_pointclouds::union_, "kdb_rdf_term_create(PC_Union(($0).value.pcpatch))",
      knowCore::Uris::askcore_datatype::literal, knowCore::Uris::askcore_datatype::literal);

    return vr.updateToCurrent();
  }
} // namespace kDBPointClouds

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBPointClouds)
