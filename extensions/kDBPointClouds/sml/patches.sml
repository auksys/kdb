Prefix geo: <http://www.opengis.net/ont/geosparql#> .
Prefix askcore_graph: <http://askco.re/graph#>
Prefix askcore_datatype: <http://askco.re/datatype#>
Prefix askcore_pointclouds: <http://askco.re/pointclouds#>

Create View askcore_graph:pointclouds_view As
  Construct {
    ?pc_uri a askcore_datatype:point_cloud ;
            askcore_pointclouds:points ?points ;
            geo:hasGeometry ?geometry .
  }
  With
    ?pc_uri = uri(%frame_base_uri, str(?id))
  From
    [[SELECT id, points, ST_GeomFromEWKT(ST_AsEWKT(PC_EnvelopeGeometry(points))) AS geometry FROM pointclouds]]

