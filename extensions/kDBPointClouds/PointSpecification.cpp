#include "PointSpecification.h"

#include <QStringView>
#include <QVariant>
#include <QXmlStreamReader>

#include <Cyqlops/Crypto/Hash.h>

#include <knowCore/TypeDefinitions.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/Logging.h>

#include <Cartography/CoordinateSystem.h>

using namespace kDBPointClouds;

namespace
{
  std::size_t datatypeSize(PointSpecification::DataType _type)
  {
    switch(_type)
    {
    case PointSpecification::DataType::UnsignedInteger8:
    case PointSpecification::DataType::Integer8:
      return 1;
    case PointSpecification::DataType::UnsignedInteger16:
    case PointSpecification::DataType::Integer16:
      return 2;
    case PointSpecification::DataType::UnsignedInteger32:
    case PointSpecification::DataType::Integer32:
    case PointSpecification::DataType::Float32:
      return 4;
    case PointSpecification::DataType::UnsignedInteger64:
    case PointSpecification::DataType::Integer64:
    case PointSpecification::DataType::Float64:
      return 8;
    }
    clog_error("unknown datatype in PointSpecification's datatypeSize");
    return 0;
  }
} // namespace

struct PointSpecification::Private : public QSharedData
{
  int pcid;
  Cartography::CoordinateSystem coordinateSystem;
  QList<Dimension> dimensions;
  PointSpecification::CompressionMode compressionMode;
  std::size_t size;

  void addDimension(const Dimension& _dim)
  {
    dimensions.append(_dim);
    size += datatypeSize(_dim.type);
  }
};

bool PointSpecification::Dimension::operator==(const PointSpecification::Dimension& _rhs) const
{
  return position == _rhs.position and description == _rhs.description and name == _rhs.name
         and type == _rhs.type and scale == _rhs.scale;
}

PointSpecification PointSpecification::get(const kDB::Repository::Connection& _connection,
                                           PointSpecification::StandardType _type,
                                           const Cartography::CoordinateSystem& _coordinates)
{
  PointSpecification ps(-1, _coordinates);
  switch(_type)
  {
  case StandardType::XYZf:
  {
    ps.addDimension(DataType::Float32, "X", "X coordinate", 1.0);
    ps.addDimension(DataType::Float32, "Y", "Y coordinate", 1.0);
    ps.addDimension(DataType::Float32, "Z", "Z coordinate", 1.0);
    break;
  }
  case StandardType::XYZfRGBui8:
  {
    ps.addDimension(DataType::Float32, "X", "X coordinate", 1.0);
    ps.addDimension(DataType::Float32, "Y", "Y coordinate", 1.0);
    ps.addDimension(DataType::Float32, "Z", "Z coordinate", 1.0);
    ps.addDimension(DataType::UnsignedInteger8, "R", "Red", 1.0);
    ps.addDimension(DataType::UnsignedInteger8, "G", "Green", 1.0);
    ps.addDimension(DataType::UnsignedInteger8, "B", "Blue", 1.0);
    break;
  }
  case StandardType::FirstCustomType:
    break;
  }
  ps = find(_connection, ps);
  if(ps.pcid() == -1)
  {
    knowDBC::Query q
      = _connection.createSQLQuery("WITH RECURSIVE t(n) AS ( "
                                   "  VALUES (1) "
                                   "  UNION ALL "
                                   "  SELECT n+1 FROM t WHERE n < 65536 ) "
                                   "SELECT n FROM t WHERE NOT EXISTS (SELECT pcid FROM "
                                   "pointcloud_formats WHERE pcid = t.n) ORDER by n LIMIT 1;");
    knowDBC::Result r = q.execute();
    if(r and r.tuples() == 1)
    {
      ps.d->pcid = r.value<int>(0, 0).expect_success();
      ps.save(_connection);
    }
    else
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR("getting pcid for schema ", r);
    }
  }
  return ps;
}

PointSpecification PointSpecification::get(const kDB::Repository::Connection& _connection,
                                           int _pcid)
{
  PointSpecification ps;
  ps.d->pcid = _pcid;
  ps.load(_connection);
  return ps;
}

PointSpecification PointSpecification::find(const kDB::Repository::Connection& _connection,
                                            const PointSpecification& _spec)
{
  knowDBC::Query q = _connection.createSQLQuery(
    "SELECT pcid FROM pointcloud_formats WHERE srid=?srid AND schema=?schema");
  q.bindValue("?srid", _spec.coordinateSystem().srid());
  q.bindValue("?schema", _spec.toXML());

  knowDBC::Result r = q.execute();
  if(r and r.tuples() == 1)
  {
    PointSpecification ps = _spec;
    ps.d->pcid = r.value<int>(0, 0).expect_success();
    return ps;
  }
  else
  {
    return _spec;
  }
}

PointSpecification::PointSpecification() : d(new Private)
{
  d->pcid = -1;
  d->coordinateSystem = Cartography::CoordinateSystem::wgs84;
  d->compressionMode = CompressionMode::None;
  d->size = 0;
}

PointSpecification::PointSpecification(int _pcid, const Cartography::CoordinateSystem& _cs)
    : d(new Private)
{
  clog_assert(_cs.isValid());
  d->pcid = _pcid;
  d->coordinateSystem = _cs;
  d->compressionMode = CompressionMode::None;
  d->size = 0;
}

PointSpecification::PointSpecification(const PointSpecification& _specification,
                                       const Cartography::CoordinateSystem& _coordinates)
    : d(new Private)
{
  clog_assert(_coordinates.isValid());
  d->pcid = -1;
  d->coordinateSystem = _coordinates;
  d->compressionMode = _specification.d->compressionMode;
  d->dimensions = _specification.d->dimensions;
  d->size = _specification.d->size;
}

PointSpecification::PointSpecification(const PointSpecification& _rhs) : d(_rhs.d) {}

PointSpecification& PointSpecification::operator=(const PointSpecification& _rhs)
{
  d = _rhs.d;
  return *this;
}

PointSpecification::~PointSpecification() {}

bool PointSpecification::operator==(const PointSpecification& _rhs) const
{
  return (d == _rhs.d)
         or (d->pcid == _rhs.d->pcid and d->coordinateSystem == _rhs.d->coordinateSystem
             and d->compressionMode == _rhs.d->compressionMode
             and d->dimensions == _rhs.d->dimensions);
}

void PointSpecification::addDimension(PointSpecification::DataType _type, const QString& _name,
                                      const QString& _description, double _scale)
{
  Dimension dim;
  dim.position = d->dimensions.size();
  dim.offset = d->size;
  dim.description = _description;
  dim.name = _name;
  dim.scale = _scale;
  dim.type = _type;
  d->addDimension(dim);
}

QList<PointSpecification::Dimension> PointSpecification::dimensions() const
{
  return d->dimensions;
}

PointSpecification::CompressionMode PointSpecification::compressionMode() const
{
  return d->compressionMode;
}

void PointSpecification::setCompressionMode(PointSpecification::CompressionMode _mode)
{
  d->compressionMode = _mode;
}

cres_qresult<void> PointSpecification::load(const kDB::Repository::Connection& _connection)
{
  knowDBC::Query q
    = _connection.createSQLQuery("SELECT srid, schema FROM pointcloud_formats WHERE pcid=?pcid");
  q.bindValue("?pcid", d->pcid);
  knowDBC::Result r = q.execute();
  if(r and r.tuples() == 1)
  {
    d->coordinateSystem = Cartography::CoordinateSystem(r.value<int>(0, 0).expect_success());
    cres_try(cres_ignore, loadFromXML(r.value<QString>(0, 1).expect_success()));
  }
  return cres_failure("Unknown pcid '{}' in the database", d->pcid);
}

bool PointSpecification::save(const kDB::Repository::Connection& _connection)
{
  if(d->pcid > 0)
  {
    knowDBC::Query q = _connection.createSQLQuery(
      "WITH upsert AS (UPDATE pointcloud_formats SET schema=?schema, srid=?srid WHERE pcid=?pcid "
      "RETURNING *) "
      "INSERT INTO pointcloud_formats(pcid, srid, schema) SELECT ?pcid, ?srid, ?schema "
      "WHERE NOT EXISTS (SELECT * FROM upsert)");

    q.bindValue("?pcid", d->pcid);
    q.bindValue("?srid", d->coordinateSystem.srid());
    q.bindValue("?schema", toXML());

    knowDBC::Result r = q.execute();
    if(not r)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR("when saving point specification ", r);
      return false;
    }
    return true;
  }
  else
  {
    knowDBC::Query q = _connection.createSQLQuery(
      "INSERT INTO pointcloud_formats(pcid, srid, schema) SELECT (SELECT i FROM pointcloud_formats "
      "RIGHT JOIN generate_series(:first_custom_type, 65535) as i ON (pcid=i) WHERE pcid is null "
      "LIMIT 1), :srid, :schema RETURNING pcid");

    q.bindValue(":srid", d->coordinateSystem.srid());
    q.bindValue(":schema", toXML());
    q.bindValue(":first_custom_type", (int)StandardType::FirstCustomType);

    knowDBC::Result r = q.execute();
    if(r)
    {
      if(r.tuples() != 1)
      {
        clog_error("No available index!");
        return false;
      }
      else
      {
        d->pcid = r.value<int>(0, 0).expect_success();
        return true;
      }
    }
    else
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR("when getting next free index in point specification ", r);
      return false;
    }
  }
}

bool PointSpecification::synchronise(const kDB::Repository::Connection& _connection)
{
  PointSpecification ps = find(_connection, *this);
  if(ps.pcid() == -1)
  {
    return save(_connection);
  }
  else
  {
    d->pcid = ps.pcid();
    return true;
  }
}

cres_qresult<PointSpecification> PointSpecification::fromXML(const QString& _xml)
{
  PointSpecification ps;
  cres_try(cres_ignore, ps.loadFromXML(_xml));
  return cres_success(ps);
}

cres_qresult<void> PointSpecification::loadFromXML(const QString& _xml)
{
  d->dimensions.clear();
  d->size = 0;
  Dimension current_dim;
  bool has_current_dim = false;

  QXmlStreamReader reader(_xml);
  while(not reader.atEnd())
  {
    reader.readNext();
    if(reader.isStartElement())
    {
      QStringView name = reader.name();
      if(name == QStringLiteral("dimension"))
      {
        if(has_current_dim)
        {
          d->addDimension(current_dim);
        }
        current_dim = Dimension();
        current_dim.position = d->dimensions.size();
        has_current_dim = true;
      }
      else if(name == QStringLiteral("position"))
      {
        reader.readNext();
        if(reader.isCharacters())
        {
          current_dim.position = reader.text().toULong() - 1;
        }
      }
      else if(name == QStringLiteral("description"))
      {
        reader.readNext();
        if(reader.isCharacters())
        {
          current_dim.description = reader.text().toString();
        }
      }
      else if(name == QStringLiteral("name"))
      {
        reader.readNext();
        if(reader.isCharacters())
        {
          current_dim.name = reader.text().toString();
        }
      }
      else if(name == QStringLiteral("interpretation"))
      {
        reader.readNext();
        if(reader.isCharacters())
        {
          QStringView interp = reader.text();
          if(interp == QStringLiteral("uint8_t"))
          {
            current_dim.type = DataType::UnsignedInteger8;
          }
          else if(interp == QStringLiteral("int8_t"))
          {
            current_dim.type = DataType::Integer8;
          }
          else if(interp == QStringLiteral("uint16_t"))
          {
            current_dim.type = DataType::UnsignedInteger16;
          }
          else if(interp == QStringLiteral("int16_t"))
          {
            current_dim.type = DataType::Integer16;
          }
          else if(interp == QStringLiteral("uint32_t"))
          {
            current_dim.type = DataType::UnsignedInteger32;
          }
          else if(interp == QStringLiteral("int32_t"))
          {
            current_dim.type = DataType::Integer32;
          }
          else if(interp == QStringLiteral("uint64_t"))
          {
            current_dim.type = DataType::UnsignedInteger64;
          }
          else if(interp == QStringLiteral("int64_t"))
          {
            current_dim.type = DataType::Integer64;
          }
          else if(interp == QStringLiteral("float"))
          {
            current_dim.type = DataType::Float32;
          }
          else if(interp == QStringLiteral("double"))
          {
            current_dim.type = DataType::Float64;
          }
          else
          {
            return cres_failure("Unknown interpretation value: {}", interp);
          }
        }
      }
      else if(name == QStringLiteral("scale"))
      {
        reader.readNext();
        if(reader.isCharacters())
        {
          current_dim.scale = reader.text().toDouble();
        }
      }
      else if(name == QStringLiteral("Metadata"))
      {
        QStringView metadata_name = reader.attributes().value("name");
        QStringView value;
        reader.readNext();
        if(reader.isCharacters())
        {
          value = reader.text();
        }
        if(metadata_name == QStringLiteral("compression"))
        {
          if(value == QStringLiteral("none"))
          {
            d->compressionMode = CompressionMode::None;
          }
          else if(value == QStringLiteral("dimensional"))
          {
            d->compressionMode = CompressionMode::Dimensional;
          }
          else if(value == QStringLiteral("ght"))
          {
            d->compressionMode = CompressionMode::GHT;
          }
          else
          {
            return cres_failure("Unknown compression mode: {}", value);
          }
        }
        else
        {
          clog_warning("Unhandled metadata with name {} and value {}", metadata_name, value);
        }
      }
      else if(name == QStringLiteral("size"))
      {
      }
      else if(name == QStringLiteral("metadata") or name == QStringLiteral("PointCloudSchema"))
      {
        // ignored
      }
      else
      {
        clog_warning("Unhandled tag: {} {}", reader.name(), reader.attributes().length());
      }
    }
  }
  if(has_current_dim)
  {
    d->addDimension(current_dim);
    has_current_dim = true;
  }

  std::sort(d->dimensions.begin(), d->dimensions.end(),
            [](const Dimension& _a, const Dimension& _b) { return _a.position < _b.position; });
  std::size_t current_offset = 0;
  for(Dimension& dim : d->dimensions)
  {
    dim.offset = current_offset;
    current_offset += datatypeSize(dim.type);
  }

  if(reader.hasError())
  {
    return cres_failure("Error when parsing XML: {} at {}, {}", reader.errorString(),
                        reader.lineNumber(), reader.columnNumber());
  }
  return cres_success();
}

namespace
{
  QString sizeFor(PointSpecification::DataType _type)
  {
    switch(_type)
    {
    case PointSpecification::DataType::UnsignedInteger8:
    case PointSpecification::DataType::Integer8:
      return "1";
    case PointSpecification::DataType::UnsignedInteger16:
    case PointSpecification::DataType::Integer16:
      return "2";
    case PointSpecification::DataType::UnsignedInteger32:
    case PointSpecification::DataType::Integer32:
    case PointSpecification::DataType::Float32:
      return "4";
    case PointSpecification::DataType::UnsignedInteger64:
    case PointSpecification::DataType::Integer64:
    case PointSpecification::DataType::Float64:
      return "8";
    }
    clog_error("unknown datatype in PointSpecification's sizeFor");
    return "##INVALID##";
  }
  QString interpretationFor(PointSpecification::DataType _type)
  {
    switch(_type)
    {
    case PointSpecification::DataType::UnsignedInteger8:
      return "uint8_t";
    case PointSpecification::DataType::Integer8:
      return "int8_t";
    case PointSpecification::DataType::UnsignedInteger16:
      return "uint16_t";
    case PointSpecification::DataType::Integer16:
      return "int16_t";
    case PointSpecification::DataType::UnsignedInteger32:
      return "uint32_t";
    case PointSpecification::DataType::Integer32:
      return "int32_t";
    case PointSpecification::DataType::UnsignedInteger64:
      return "uint64_t";
    case PointSpecification::DataType::Integer64:
      return "int64_t";
    case PointSpecification::DataType::Float32:
      return "float";
    case PointSpecification::DataType::Float64:
      return "double";
    }
    clog_error("unknown datatype in PointSpecification's interpretationFor");
    return "##INVALID##";
  }
  QString compressionFor(PointSpecification::CompressionMode _mode)
  {
    switch(_mode)
    {
    case PointSpecification::CompressionMode::None:
      return "none";
    case PointSpecification::CompressionMode::Dimensional:
      return "dimensional";
    case PointSpecification::CompressionMode::GHT:
      return "ght";
    }
    clog_error("unknown compression in PointSpecification's compressionFor");
    return "##INVALID##";
  }
} // namespace

QString PointSpecification::toXML() const
{
  QString result;
  QXmlStreamWriter writer(&result);

  writer.writeStartDocument();
  writer.writeStartElement("pc:PointCloudSchema");
  writer.writeAttribute("xmlns:pc", "http://pointcloud.org/schemas/PC/1.1");
  writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

  for(const Dimension& dim : d->dimensions)
  {
    writer.writeStartElement("pc:dimension");
    writer.writeTextElement("pc:position", QString::number(dim.position + 1));
    writer.writeTextElement("pc:size", sizeFor(dim.type));
    writer.writeTextElement("pc:description", dim.description);
    writer.writeTextElement("pc:name", dim.name);
    writer.writeTextElement("pc:interpretation", interpretationFor(dim.type));
    writer.writeTextElement("pc:scale", QString::number(dim.scale));
    writer.writeEndElement(); // pc:dimension
  }
  writer.writeStartElement("pc:metadata");
  writer.writeStartElement("Metadata");
  writer.writeAttribute("name", "compression");
  writer.writeCharacters(compressionFor(d->compressionMode));
  writer.writeEndElement(); // Metadata
  writer.writeEndElement(); // pc:metatdata
  writer.writeEndElement(); // pc:PointCloudSchema
  writer.writeEndDocument();

  return result;
}

Cartography::CoordinateSystem PointSpecification::coordinateSystem() const
{
  return d->coordinateSystem;
}

int PointSpecification::pcid() const { return d->pcid; }

std::size_t PointSpecification::size() const { return d->size; }

bool PointSpecification::isValid() const { return d->coordinateSystem.isValid() or d->size > 0; }

cres_qresult<QByteArray> PointSpecification::md5() const
{
  if(isValid())
  {
    QCryptographicHash hash(QCryptographicHash::Md5);
    Cyqlops::Crypto::Hash::compute(hash, d->coordinateSystem.srid());
    Cyqlops::Crypto::Hash::compute(hash, int(0));
    Cyqlops::Crypto::Hash::compute(hash, int(0), double(1.0));
    Cyqlops::Crypto::Hash::compute(hash, QByteArray(), int(0), double(1.0));
    for(const Dimension& dim : d->dimensions)
    {
      Cyqlops::Crypto::Hash::compute(hash, dim.position, dim.offset, dim.description, dim.name,
                                     int(dim.type), dim.scale);
    }
    return cres_success(hash.result());
  }
  else
  {
    return cres_failure("Invalid point specification");
  }
}
