#include <QCommandLineParser>
#include <QDir>

#include <clog_qt>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/Store.h>

#include <Cartography/CoordinateSystem.h>

#include <kDBPointClouds/Patch.h>
#include <kDBPointClouds/PointCloud.h>
#include <kDBPointClouds/PointSpecification.h>

int main(int _argc, char** _argv)
{
  QCoreApplication app(_argc, _argv);
  QCoreApplication::setApplicationName("TranslatePointCloud");
  QCoreApplication::setApplicationVersion("1.0");

  QCommandLineParser parser;
  parser.setApplicationDescription("Translation Point Cloud");
  parser.addHelpOption();
  parser.addVersionOption();
  QCommandLineOption opt_tx("tx", "<tx>", "tx");
  parser.addOption(opt_tx);
  QCommandLineOption opt_ty("ty", "<ty>", "ty");
  parser.addOption(opt_ty);
  QCommandLineOption opt_tz("tz", "<tz>", "tz");
  parser.addOption(opt_tz);
  QCommandLineOption opt_id("id", "<id>", "id");
  parser.addOption(opt_id);
  QCommandLineOption opt_hostname("hostname", "<hostname>", "hostname");
  parser.addOption(opt_hostname);

  parser.process(app);

  float tx = parser.value(opt_tx).toFloat();
  float ty = parser.value(opt_ty).toFloat();
  float tz = parser.value(opt_tz).toFloat();
  std::size_t id = parser.value(opt_id).toULongLong();

  QString hostname = parser.value(opt_hostname);

  kDB::Repository::Store store(hostname);
  store.autoSelectPort();
  cres_qresult<void> store_rv = store.startIfNeeded();
  if(not store_rv.is_successful())
  {
    clog_error("No store running or cannot be started at '{}' with error '{}'!", hostname,
               store_rv.get_error());
    return -1;
  }

  kDB::Repository::Connection connection = store.createConnection();

  QString errMsg;
  cres_qresult<void> connection_rv = connection.connect();
  if(not connection_rv.is_successful())
  {
    clog_error("Failed to connect: {}", connection_rv.get_error());
    return -1;
  }

  kDBPointClouds::PointCloudRecord pcr
    = kDBPointClouds::PointCloudRecord::byId(connection, id).first();

  kDBPointClouds::Patch patch = pcr.points();
  if(patch.isValid())
  {
    if(patch.specification()
       == kDBPointClouds::PointSpecification::get(
         connection, kDBPointClouds::PointSpecification::StandardType::XYZf,
         patch.specification().coordinateSystem()))
    {
      kDBPointClouds::Patch opatch(patch.specification());
      opatch.reserve(patch.pointsCount());
      for(std::size_t i = 0; i < patch.pointsCount(); ++i)
      {
        kDBPointClouds::Point pt = patch.get(i);
        auto [pt_x, pt_y, pt_z] = pt.get<float, float, float>();
        pt.set(pt_x + tx, pt_y + ty, pt_z + tz);
        opatch.append(pt);
      }
      pcr.setPoints(opatch);
    }
    else
    {
      clog_error("Unsupported point cloud specification!");
      return -1;
    }
  }
  else
  {
    clog_error("No valid patch with id: {}", id);
    return -1;
  }
}
