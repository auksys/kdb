#include "PatchFieldIO_p.h"

#include <knowCore/Value.h>

#include <Cartography/CoordinateSystem.h>

#include "Patch.h"
#include "PointSpecification.h"

using namespace kDBPointClouds;

PatchFieldIO::PatchFieldIO() : FieldIO("pcpatch") {}

PatchFieldIO::~PatchFieldIO() {}

bool PatchFieldIO::accept(const knowCore::Value& _variant) const
{
  return _variant.datatype() == PatchMetaTypeInformation::uri();
}

quint32 PatchFieldIO::calculateSize(const knowCore::Value& _variant) const
{
  return _variant.value<Patch>().expect_success().toPGWKB().size();
}

knowCore::Value PatchFieldIO::read(const char* _data, int _field_size,
                                   const kDB::Repository::Connection& _connection) const
{
  QByteArray arr(_data, _field_size);

  auto const [success, patch, message] = Patch::fromPGWKB(arr, _connection);
  Q_UNUSED(message);
  if(success)
  {
    return knowCore::Value::fromValue(patch.value());
  }
  else
  {
    return knowCore::Value();
  }
}

void PatchFieldIO::write(char* _data, const knowCore::Value& _variant,
                         const kDB::Repository::Connection& _connection) const
{
  Patch p = _variant.value<Patch>().expect_success();
  p.synchroniseSpecification(_connection);
  clog_assert(p.specification().pcid() >= 0);
  QByteArray arr = p.toPGWKB();
  std::copy(arr.begin(), arr.end(), _data);
}
