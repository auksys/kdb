#include "TestPointSpecification.h"

#include <knowCore/Test.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/TemporaryStore.h>

#include <Cartography/CoordinateSystem.h>

#include "../Initialise.h"
#include "../PointSpecification.h"

void TestPointSpecification::testCreation()
{
  kDBPointClouds::PointSpecification ps(1, Cartography::CoordinateSystem::wgs84);
  QCOMPARE(ps.compressionMode(), kDBPointClouds::PointSpecification::CompressionMode::None);
  ps.setCompressionMode(kDBPointClouds::PointSpecification::CompressionMode::GHT);
  QCOMPARE(ps.compressionMode(), kDBPointClouds::PointSpecification::CompressionMode::GHT);

  QCOMPARE(ps.coordinateSystem().srid(), 4326u);

  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float64, "X", "X dimension", 1.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger16, "Y",
                  "Y dimension", 0.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger32, "Z",
                  "Z dimension", 0.55);

  QCOMPARE(ps.dimensions().size(), 3);
  QCOMPARE(ps.dimensions()[0].description, QString("X dimension"));
  QCOMPARE(ps.dimensions()[0].name, QString("X"));
  QCOMPARE((unsigned long)ps.dimensions()[0].position, 0ul);
  QCOMPARE(ps.dimensions()[0].scale, 1.05);
  QCOMPARE(ps.dimensions()[0].type, kDBPointClouds::PointSpecification::DataType::Float64);
  QCOMPARE(ps.dimensions()[1].description, QString("Y dimension"));
  QCOMPARE(ps.dimensions()[1].name, QString("Y"));
  QCOMPARE((unsigned long)ps.dimensions()[1].position, 1ul);
  QCOMPARE(ps.dimensions()[1].scale, 0.05);
  QCOMPARE(ps.dimensions()[1].type,
           kDBPointClouds::PointSpecification::DataType::UnsignedInteger16);
  QCOMPARE(ps.dimensions()[2].description, QString("Z dimension"));
  QCOMPARE(ps.dimensions()[2].name, QString("Z"));
  QCOMPARE((unsigned long)ps.dimensions()[2].position, 2ul);
  QCOMPARE(ps.dimensions()[2].scale, 0.55);
  QCOMPARE(ps.dimensions()[2].type,
           kDBPointClouds::PointSpecification::DataType::UnsignedInteger32);

  QCOMPARE((unsigned long)ps.size(), 14ul);
}

void TestPointSpecification::testXML()
{
  kDBPointClouds::PointSpecification ps(1, Cartography::CoordinateSystem::wgs84);
  ps.setCompressionMode(kDBPointClouds::PointSpecification::CompressionMode::GHT);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float64, "X", "X dimension", 1.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger16, "Y",
                  "Y dimension", 0.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger32, "Z",
                  "Z dimension", 0.55);

  kDBPointClouds::PointSpecification ps2(1, Cartography::CoordinateSystem::wgs84);
  QVERIFY(ps2.loadFromXML(ps.toXML()).is_successful());

  QCOMPARE(ps2.dimensions().size(), 3);

  QCOMPARE(ps2.dimensions()[0].description, QString("X dimension"));
  QCOMPARE(ps2.dimensions()[0].name, QString("X"));
  QCOMPARE((unsigned long)ps2.dimensions()[0].position, 0ul);
  QCOMPARE(ps2.dimensions()[0].scale, 1.05);
  QCOMPARE(ps2.dimensions()[0].type, kDBPointClouds::PointSpecification::DataType::Float64);
  QCOMPARE(ps2.dimensions()[1].description, QString("Y dimension"));
  QCOMPARE(ps2.dimensions()[1].name, QString("Y"));
  QCOMPARE((unsigned long)ps2.dimensions()[1].position, 1ul);
  QCOMPARE(ps2.dimensions()[1].scale, 0.05);
  QCOMPARE(ps2.dimensions()[1].type,
           kDBPointClouds::PointSpecification::DataType::UnsignedInteger16);
  QCOMPARE(ps2.dimensions()[2].description, QString("Z dimension"));
  QCOMPARE(ps2.dimensions()[2].name, QString("Z"));
  QCOMPARE((unsigned long)ps2.dimensions()[2].position, 2ul);
  QCOMPARE(ps2.dimensions()[2].scale, 0.55);
  QCOMPARE(ps2.dimensions()[2].type,
           kDBPointClouds::PointSpecification::DataType::UnsignedInteger32);

  QCOMPARE((unsigned long)ps2.size(), 14ul);

  QCOMPARE(ps, ps2);
}

void TestPointSpecification::testSaving()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBPointClouds"));

  kDBPointClouds::PointSpecification ps;
  ps.setCompressionMode(kDBPointClouds::PointSpecification::CompressionMode::GHT);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float64, "X", "X dimension", 1.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger16, "Y",
                  "Y dimension", 0.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger32, "Z",
                  "Z dimension", 0.55);

  kDBPointClouds::PointSpecification ps3 = ps;

  ps.save(c);

  QCOMPARE(ps.pcid(), (int)kDBPointClouds::PointSpecification::StandardType::FirstCustomType);
  QCOMPARE(ps3.pcid(), -1);

  kDBPointClouds::PointSpecification ps4 = kDBPointClouds::PointSpecification::find(c, ps3);
  QCOMPARE(ps4, ps);

  ps3.save(c);
  QCOMPARE(ps3.pcid(), (int)kDBPointClouds::PointSpecification::StandardType::FirstCustomType + 1);

  kDBPointClouds::PointSpecification ps2(ps.pcid(), ps.coordinateSystem());
  ps2.load(c);
  QCOMPARE(ps, ps2);

  kDBPointClouds::PointSpecification ps6 = kDBPointClouds::PointSpecification::get(c, ps.pcid());
  QCOMPARE(ps, ps6);
}

void TestPointSpecification::testStandard()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBPointClouds"));

  kDBPointClouds::PointSpecification ps = kDBPointClouds::PointSpecification::get(
    c, kDBPointClouds::PointSpecification::StandardType::XYZf, 4326_cartographyCS);
  QVERIFY(ps.pcid() != -1);
}

QTEST_MAIN(TestPointSpecification)
