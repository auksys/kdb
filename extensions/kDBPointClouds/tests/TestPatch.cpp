#include "TestPatch.h"

#include <knowCore/Test.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryStore.h>

#include <Cartography/CoordinateSystem.h>

#include "../Initialise.h"
#include "../Patch.h"
#include "../PointSpecification.h"

#include <kDBPointClouds/PointCloud.h>

void TestPatch::testCreation()
{
  kDBPointClouds::PointSpecification ps;
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float64, "X", "X dimension", 1.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger16, "Y",
                  "Y dimension", 0.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Integer32, "Z", "Z dimension",
                  0.55);

  kDBPointClouds::Patch patch(ps);

  std::tuple<double, uint16_t, int32_t> pt1_data(2.4, 22, -10);
  std::tuple<double, uint16_t, int32_t> pt2_data(-2.4, 222, 10);
  std::tuple<double, uint16_t, int32_t> pt3_data(12.4, 2, -15);

  kDBPointClouds::Point pt2(ps);
  pt2.set(pt2_data);

  patch.append(pt1_data);
  patch.append(pt2);
  patch.append(pt3_data);

  QCOMPARE((unsigned long)patch.pointsCount(), 3ul);

  std::tuple<double, uint16_t, int32_t> pt1_data_2 = patch.get(0).get<double, uint16_t, int32_t>();
  std::tuple<double, uint16_t, int32_t> pt2_data_2 = patch.get<double, uint16_t, int32_t>(1);
  std::tuple<double, uint16_t, int32_t> pt3_data_2 = patch.get(2).get<double, uint16_t, int32_t>();

  QCOMPARE(pt1_data, pt1_data_2);
  QCOMPARE(pt2_data, pt2_data_2);
  QCOMPARE(pt3_data, pt3_data_2);
}

void TestPatch::testCreationFromData()
{
  kDBPointClouds::PointSpecification ps;
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger8, "X",
                  "X dimension", 1);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger8, "Y",
                  "Y dimension", 1);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger8, "Z",
                  "Z dimension", 1);

  QByteArray data(6, 0);
  data[0] = 1;
  data[1] = 3;
  data[2] = 2;
  data[3] = 5;
  data[4] = 4;
  data[5] = 8;

  kDBPointClouds::Patch patch(ps, data);

  QCOMPARE(patch.pointsCount(), std::size_t{2});

  QCOMPARE(patch.get(0).get<KNOWCORE_LIST(uint8_t, uint8_t, uint8_t)>(),
           std::tuple<KNOWCORE_LIST(uint8_t, uint8_t, uint8_t)>(1, 3, 2));
  QCOMPARE(patch.get(1).get<KNOWCORE_LIST(uint8_t, uint8_t, uint8_t)>(),
           std::tuple<KNOWCORE_LIST(uint8_t, uint8_t, uint8_t)>(5, 4, 8));
}

void TestPatch::testDatabase()
{
  kDBPointClouds::PointSpecification ps(1, Cartography::CoordinateSystem::wgs84);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float64, "X", "X dimension", 1.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger16, "Y",
                  "Y dimension", 0.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Integer32, "Z", "Z dimension",
                  0.55);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float32, "K", "K dimension", 2.0);

  kDBPointClouds::Patch patch(ps);

  std::tuple<double, uint16_t, int32_t, float> pt1_data(2.4, 22, -10, 3.0f);
  std::tuple<double, uint16_t, int32_t, float> pt2_data(-2.4, 222, 10, 1.0f);
  std::tuple<double, uint16_t, int32_t, float> pt3_data(12.4, 2, -15, -5.0f);

  kDBPointClouds::Point pt2(ps);
  pt2.set(pt2_data);

  patch.append(pt1_data);
  patch.append(pt2);
  patch.append(pt3_data);

  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBPointClouds"));

  ps.save(c);

  CRES_QVERIFY(kDBPointClouds::PointCloudRecord::create(c, QString(), knowCore::Timestamp(), 0.0,
                                                        0.0, 0.0, knowCore::Vector4d(),
                                                        knowGIS::GeometryObject(), patch));
  kDBPointClouds::Patch patch2 = kDBPointClouds::PointCloudRecord::byId(c, 1).first().points();

  QCOMPARE(patch.pointsCount(), std::size_t{3});
  QCOMPARE(patch2.pointsCount(), std::size_t{3});
  QCOMPARE(patch, patch2);
}

QTEST_MAIN(TestPatch)
