#include <QtTest/QtTest>

class TestPointSpecification : public QObject
{
  Q_OBJECT
private slots:
  void testCreation();
  void testXML();
  void testSaving();
  void testStandard();
};
