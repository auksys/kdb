#include <QtTest/QtTest>

class TestPatch : public QObject
{
  Q_OBJECT
private slots:
  void testCreation();
  void testCreationFromData();
  void testDatabase();
};
