#include "TestPoint.h"

#include <Cartography/CoordinateSystem.h>

#include "../Point.h"
#include "../PointSpecification.h"

void TestPoint::testCreation()
{
  kDBPointClouds::PointSpecification ps;
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float64, "X", "X dimension", 1.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::UnsignedInteger16, "Y",
                  "Y dimension", 0.05);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Integer32, "Z", "Z dimension",
                  0.55);

  kDBPointClouds::Point pt(ps);

  std::tuple<double, uint16_t, uint32_t> data(2.4, 22, -10);
  pt.set(data);
  std::tuple<double, uint16_t, uint32_t> data2 = pt.get<double, uint16_t, uint32_t>();
  QCOMPARE(data, data2);
}

QTEST_MAIN(TestPoint)
