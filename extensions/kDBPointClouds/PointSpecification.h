#pragma once

#include <QSharedDataPointer>

#include <clog_qt>
#include <cres_qt>

#include <kDBGIS/Forward.h>

namespace kDBPointClouds
{
  class PointSpecification
  {
  public:
    enum class DataType
    {
      UnsignedInteger8,
      Integer8,
      UnsignedInteger16,
      Integer16,
      UnsignedInteger32,
      Integer32,
      UnsignedInteger64,
      Integer64,
      Float32,
      Float64
    };
    enum class CompressionMode
    {
      None,
      Dimensional,
      GHT
    };
    struct Dimension
    {
      std::size_t position; //< index in the set of value (0, 1, 2, 3...)
      std::size_t offset;   //< offset of the begining of the value
      QString description;  //< description of the field
      QString name;         //< short name
      DataType type;        //< data type of the field
      double scale; //< scale applied to the value in the field (useful for integers data type)
      bool operator==(const Dimension& _rhs) const;
    };
  public:
    enum class StandardType : int
    {
      XYZf,
      XYZfRGBui8,
      FirstCustomType = 100
    };
    static PointSpecification get(const kDB::Repository::Connection& _connection,
                                  StandardType _type,
                                  const Cartography::CoordinateSystem& _coordinates);
    static PointSpecification get(const kDB::Repository::Connection& _connection, int _pcid);
    static PointSpecification find(const kDB::Repository::Connection& _connection,
                                   const PointSpecification& _spec);
  public:
    PointSpecification();
    /**
     * Create a point specification with a given connection and identifier.
     * If the specification already exists in the database, it will be loaded, in which case the
     * srid will also be loaded from the database.
     */
    PointSpecification(int _pcid, const Cartography::CoordinateSystem& _coordinates);
    /**
     * Create a point specification based on an existing one but with a different coordinate system
     */
    PointSpecification(const PointSpecification& _specification,
                       const Cartography::CoordinateSystem& _coordinates);
    PointSpecification(const PointSpecification& _rhs);
    PointSpecification& operator=(const PointSpecification& _rhs);
    ~PointSpecification();
    bool operator==(const PointSpecification& _rhs) const;
    CompressionMode compressionMode() const;
    void setCompressionMode(CompressionMode _mode);
    void addDimension(DataType _type, const QString& _name, const QString& _description,
                      double _scale);
    QList<Dimension> dimensions() const;
    /**
     * Convert to XML representation
     */
    QString toXML() const;
    /**
     * Convert from a XML representation
     */
    static cres_qresult<PointSpecification> fromXML(const QString& _xml);
    cres_qresult<void> loadFromXML(const QString& _xml);
    /**
     * Load the definition from a database. Using the pcid. It will override the srid and the
     * dimensions.
     */
    cres_qresult<void> load(const kDB::Repository::Connection& _connection);
    /**
     * Save the definition to a database.
     */
    bool save(const kDB::Repository::Connection& _connection);
    /**
     * Check if available in the database and then update srid, or save in the database.
     */
    bool synchronise(const kDB::Repository::Connection& _connection);
    Cartography::CoordinateSystem coordinateSystem() const;
    int pcid() const;
    /**
     * @return the storage size of a point using that specification
     */
    std::size_t size() const;
    /**
     * @return if it is a valid point specification
     */
    bool isValid() const;
    cres_qresult<QByteArray> md5() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace kDBPointClouds
