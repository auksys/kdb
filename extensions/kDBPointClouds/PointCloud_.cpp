#include "PointCloud.h"

using namespace kDBPointClouds;

cres_qresult<PointCloudRecord>
  PointCloudRecord::create(const kDB::Repository::QueryConnectionInfo& _connection,
                           const QString& _datasetUri,
                           const knowValues::Values::PointCloud& _pointCloud)
{
  knowGIS::GeoPose pose = _pointCloud->transformation();
  return create(_connection, _datasetUri, _pointCloud->timestamp(), pose.position().longitude(),
                pose.position().latitude(), pose.position().altitude(),
                knowCore::Vector4d(pose.orientation().x(), pose.orientation().y(),
                                   pose.orientation().z(), pose.orientation().w()),
                _pointCloud->geometry(), kDBPointClouds::Patch::create(_pointCloud));
}
