#include "Point.h"

#include <Cartography/CoordinateSystem.h>

#include "PointSpecification.h"

using namespace kDBPointClouds;

struct Point::Private : public QSharedData
{
  PointSpecification specification;

  QByteArray data;
};

Point::Point() : d(new Private) {}

Point::Point(const PointSpecification& _specification) : d(new Private)
{
  d->specification = _specification;
  d->data.resize(d->specification.size());
}

Point::Point(const Point& _rhs) : d(_rhs.d) {}

Point& Point::operator=(const Point& _rhs)
{
  d = _rhs.d;
  return *this;
}

Point::~Point() {}

QByteArray Point::array() const { return d->data; }

QByteArray& Point::array() { return d->data; }

bool Point::operator==(const kDBPointClouds::Point& _point) const
{
  return d == _point.d
         or (d->data == _point.d->data and d->specification == _point.d->specification);
}
