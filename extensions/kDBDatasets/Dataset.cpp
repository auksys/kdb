#include "Dataset.h"

#include <clog_qt>
#include <cres_clog>

#include <knowCore/Timestamp.h>
#include <knowCore/ValueList.h>

#include <knowCore/Uris/askcore_dataset.h>
#include <knowCore/Uris/askcore_graph.h>
#include <knowCore/Uris/time.h>

#include <knowGIS/GeometryObject.h>

#include <knowRDF/Literal.h>

#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/Transaction.h>

#include <kDB/Repository/RDF/FocusNode.h>
#include <kDB/Repository/RDF/FocusNodeDeclaration.h>
#include <kDB/Repository/RDF/FocusNodeDeclarationsRegistry.h>

#include <knowGIS/Uris/geo.h>

#include "Collection.h"

using askcore_dataset = knowCore::Uris::askcore_dataset;
using u_time = knowCore::Uris::time;

using namespace kDBDatasets;

Dataset::Dataset() {}

Dataset::Dataset(const Dataset& _rhs) : kDB::Repository::RDF::FocusNodeWrapper<Dataset>(_rhs) {}

Dataset& Dataset::operator=(const Dataset& _rhs)
{
  return kDB::Repository::RDF::FocusNodeWrapper<Dataset>::operator=(_rhs);
}

Dataset::~Dataset() {}

cres_qresult<Dataset> Dataset::fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node)
{
  cres_try(knowCore::Uri content_type,
           _focus_node.property<knowCore::Uri>(askcore_dataset::content_type));
  cres_try(kDB::Repository::RDF::FocusNodeDeclaration declaration,
           kDB::Repository::RDF::FocusNodeDeclarationsRegistry::byConstantField(
             knowCore::Uris::askcore_dataset::dataset,
             knowCore::Uris::askcore_dataset::content_type,
             knowRDF::Literal::fromValue(content_type)));
  Dataset ds;
  ds.setFocusNode(kDB::Repository::RDF::FocusNode(_focus_node.connection(), _focus_node.graph(),
                                                  _focus_node.uri(), declaration));
  return cres_success(ds);
}

cres_qresult<knowGIS::GeometryObject> Dataset::geometry() const
{
  return property<knowGIS::GeometryObject>(knowGIS::Uris::geo::hasGeometry);
}

cres_qresult<knowCore::Uri> Dataset::contentType() const
{
  return property<knowCore::Uri>(askcore_dataset::content_type);
}

cres_qresult<void> Dataset::associate(const knowCore::Uri& _agent)
{
  return addPropertyToList(askcore_dataset::has_dataset, knowCore::Value::fromValue(_agent));
}

cres_qresult<void> Dataset::dissociate(const knowCore::Uri& _agent)
{
  return removePropertyFromList(askcore_dataset::has_dataset, knowCore::Value::fromValue(_agent));
}

cres_qresult<QList<knowCore::Uri>> Dataset::associatedAgents() const
{
  cres_try(knowCore::ValueList values, propertyList(askcore_dataset::has_dataset));
  QList<knowCore::Uri> uris;
  for(const knowCore::Value& value : values)
  {
    cres_try(knowCore::Uri uri, value.value<knowCore::Uri>());
    uris.append(uri);
  }
  return cres_success(uris);
}

cres_qresult<knowCore::Timestamp> Dataset::startTime() const
{
  return property<knowCore::Timestamp>(u_time::hasBeginning);
}

cres_qresult<knowCore::Timestamp> Dataset::endTime() const
{
  return property<knowCore::Timestamp>(u_time::hasEnd);
}

cres_qresult<QList<Dataset>> Dataset::createdFrom() const
{
  return cres_failure("Dataset::createdFrom is currently not implemented!");
}

cres_qresult<Dataset::Status> Dataset::status() const
{
  using askcore_dataset = knowCore::Uris::askcore_dataset;
  cres_try(knowCore::Uri status_uri, statusUri());

  static QHash<knowCore::Uri, Dataset::Status> statuses
    = {{askcore_dataset::in_progress, Status::InProgress},
       {askcore_dataset::in_preparation, Status::InPreparation},
       {askcore_dataset::scheduled, Status::Scheduled},
       {askcore_dataset::completed, Status::Completed},
       {askcore_dataset::unknown, Status::Unknown}};

  if(statuses.contains(status_uri))
  {
    return cres_success(statuses.value(status_uri));
  }
  else
  {
    return cres_failure("Unknown status: '{}'", status_uri);
  }
}

cres_qresult<knowCore::Uri> Dataset::statusUri() const
{
  auto const [success, uri_val, error] = property(askcore_dataset::dataset_status);
  if(success)
  {
    return uri_val.value().value<knowCore::Uri>();
  }
  else
  {
    cres_try(bool r, hasProperty(askcore_dataset::dataset_status));
    if(r)
    {
      return cres_forward_failure(error.value());
    }
    else
    {
      return cres_success(askcore_dataset::unknown);
    }
  }
}

cres_qresult<void> Dataset::setStatus(Status _status) const
{
  using askcore_dataset = knowCore::Uris::askcore_dataset;
  switch(_status)
  {
  case Status::InProgress:
    return setStatus(askcore_dataset::in_progress);
  case Status::InPreparation:
    return setStatus(askcore_dataset::in_preparation);
  case Status::Scheduled:
    return setStatus(askcore_dataset::scheduled);
  case Status::Completed:
    return setStatus(askcore_dataset::completed);
  case Status::Unknown:
    return setStatus(askcore_dataset::unknown);
  }
  return cres_failure("Unknown status");
}

cres_qresult<void> Dataset::setStatus(const knowCore::Uri& _uri) const
{
  return setProperty(askcore_dataset::dataset_status,
                     knowRDF::Literal::fromValue<knowCore::Uri>(_uri));
}
