#include "Initialise.h"

#include <QFile>

#include <kDBRobotics/Initialise.h>

#include <kDB/Repository/krQuery/Engine.h>

#include <clog_qt>
#include <knowCore/Uri.h>
#include <knowCore/Uris/askcore_dataset.h>
#include <knowCore/Uris/askcore_graph.h>

#include <kDB/Repository/ActiveRecord/Version.h>
#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TriplesView.h>

#include "Collection.h"

#include "krQueryActions.h"

using askcore_dataset = knowCore::Uris::askcore_dataset;

inline void kdbdatasets_init_data() { Q_INIT_RESOURCE(kdbdatasets_data); }

cres_qresult<void> kDBDatasets::initialise(const kDB::Repository::Connection& _connection)
{
  kdbdatasets_init_data();

  cres_try(cres_ignore, kDB::Repository::Connection(_connection).enableExtension("kDBRobotics"));

  cres_try(kDB::Repository::ActiveRecord::VersionRecord vr,
           kDB::Repository::ActiveRecord::VersionRecord::get(_connection, "kDBDatasets"));

  cres_try(cres_ignore, _connection.graphsManager()->loadViewsFrom(
                          ":/kdbdatasets/sml/",
                          knowCore::ValueHash().insert("%frame_base_uri", "base_uri"_kCu,
                                                       "%config_base_uri", "config_base_uri"_kCu)));

  cres_try(cres_ignore, Collection::registerCollection(_connection));

  cres_try(cres_ignore,
           kDBDatasets::Collection::getOrCreate(_connection,
                                                knowCore::Uris::askcore_graph::private_datasets),
           message("Failed to initialise private collection: {}"));

  _connection.krQueryEngine()->add({"dataset"}, new krQueryActions::Dataset(), true);

  return vr.updateToCurrent();
}

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBDatasets)
