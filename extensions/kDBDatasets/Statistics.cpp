#include "Statistics.h"

#include "Dataset.h"

using namespace kDBDatasets;

struct Statistics::Private : public QSharedData
{
  Dataset dataset;
  qsizetype datapoints_count;
};

Statistics::Statistics() : d(new Private) {}

Statistics::Statistics(const Dataset& _dataset, qsizetype _datapoints_count) : Statistics()
{
  d->dataset = _dataset;
  d->datapoints_count = _datapoints_count;
}
Statistics::Statistics(const Statistics& _rhs) : d(_rhs.d) {}

Statistics& Statistics::operator=(const Statistics& _rhs)
{
  d = _rhs.d;
  return *this;
}
Statistics::~Statistics() {}

qsizetype Statistics::datapointsCount() const { return d->datapoints_count; }

bool Statistics::isValid() const { return d->dataset.exists().ok_or(false); }
