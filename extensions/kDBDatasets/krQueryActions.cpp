#include "krQueryActions.h"

#include <knowCore/BigNumber.h>
#include <knowCore/ValueHash.h>

#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TripleStore.h>

#include <kDB/Repository/krQuery/Context.h>

#include "Collection.h"
#include "Dataset.h"

using namespace kDBDatasets::krQueryActions;

struct Dataset::Private
{
};

Dataset::Dataset() : d(new Private) {}

Dataset::~Dataset() { delete d; }

cres_qresult<knowCore::Value> Dataset::execute(const kDB::Repository::krQuery::Context& _context,
                                               const QString& _key,
                                               const knowCore::ValueHash& _parameters)
{
  kDB::Repository::Connection connection = _context.queryConnectionInfo().connection();
  if(connection.isConnected())
  {
    if(_key == "dataset")
    {
      cres_try(QString action, _parameters.value<QString>("action"));
      cres_try(knowCore::Uri dataset_uri, _parameters.value<knowCore::Uri>("target"));

      if(action == "set status")
      {
        cres_try(knowCore::Uri status_uri, _parameters.value<knowCore::Uri>("status"));

        cres_try(kDBDatasets::Dataset ds,
                 kDBDatasets::Collection::allDatasets(connection).dataset(dataset_uri));

        cres_try(cres_ignore, ds.setStatus(status_uri));
        return cres_success(knowCore::Value());
      }
      else if(action == "set self has data")
      {
        cres_try(kDBDatasets::Dataset ds,
                 kDBDatasets::Collection::allDatasets(connection).dataset(dataset_uri));
        cres_try(cres_ignore, ds.associate(connection.serverUri()));
        return cres_success(knowCore::Value());
      }
      else
      {
        return cres_failure("Unknown salient region action: '{}'", action);
      }
    }
    return cres_failure("Unhandled key {}", _key);
  }
  else
  {
    return cres_failure("Not connected.");
  }
}
