#include <knowCore/Reference.h>
#include <knowCore/Value.h>

#include "Interfaces/ExtractIterator.h"
#include "Interfaces/InsertIterator.h"
#include "Interfaces/ValueIterator.h"

#include "Forward.h"

namespace kDBDatasets
{
  KNOWCORE_DEFINE_REFERENCE(InsertIterator, Interfaces::InsertIterator,
                            KNOWCORE_FORWARD_FUNCTION(next, const QByteArray&),
                            KNOWCORE_FORWARD_FUNCTIONS(finalise));
  KNOWCORE_DEFINE_REFERENCE(ExtractIterator, Interfaces::ExtractIterator,
                            KNOWCORE_FORWARD_FUNCTIONS(next),
                            KNOWCORE_FORWARD_CONST_FUNCTIONS(hasNext));
  KNOWCORE_DEFINE_REFERENCE(ValueIterator, Interfaces::ValueIterator,
                            KNOWCORE_FORWARD_FUNCTIONS(next),
                            KNOWCORE_FORWARD_CONST_FUNCTIONS(hasNext));
  class DataInterfaceRegistry
  {
    DataInterfaceRegistry() = delete;
    ~DataInterfaceRegistry() = delete;
  public:
    static cres_qresult<InsertIterator>
      createInsertIterator(const kDB::Repository::Connection& _connection, const Dataset& _dataset);
    static cres_qresult<ValueIterator>
      createValueIterator(const kDB::Repository::Connection& _connection, const Dataset& _dataset);
    static cres_qresult<ExtractIterator>
      createExtractIterator(const kDB::Repository::Connection& _connection,
                            const Dataset& _dataset);
    static cres_qresult<Statistics> statistics(const kDB::Repository::Connection& _connection,
                                               const Dataset& _dataset);
    static void registerInterface(const knowCore::Uri& _datatype,
                                  Interfaces::DataInterface* _interface);
    /**
     * Export a dataset to an IO device.
     */
    static cres_qresult<void> exportTo(const kDB::Repository::Connection& _connection,
                                       const Dataset& _dataset, QIODevice* _device);
    /**
     * Import a dataset from an IO device.
     *
     * @p _collection dataset where to insert the metadata.
     */
    static cres_qresult<void> importFrom(const kDB::Repository::Connection& _connection,
                                         const Collection& _collection, QIODevice* _device);
  private:
    struct Private;
  };
} // namespace kDBDatasets

#define __KDB_KDB_REGISTER_DATASET_DATA_INTERFACE(_NAME_, _URI_, _INTERFACE_, ...)                 \
  namespace                                                                                        \
  {                                                                                                \
    class _NAME_                                                                                   \
    {                                                                                              \
      _NAME_()                                                                                     \
      {                                                                                            \
        kDBDatasets::DataInterfaceRegistry::registerInterface(_URI_,                               \
                                                              new _INTERFACE_(__VA_ARGS__));       \
      }                                                                                            \
      static _NAME_ instance;                                                                      \
    };                                                                                             \
    _NAME_ _NAME_::instance;                                                                       \
  }

#define KDB_REGISTER_DATASET_DATA_INTERFACE(_URI_, _INTERFACE_)                                    \
  __KDB_KDB_REGISTER_DATASET_DATA_INTERFACE(__KNOWCORE_UNIQUE_STATIC_NAME(DataInterfaceRegster),   \
                                            _URI_, _INTERFACE_)
