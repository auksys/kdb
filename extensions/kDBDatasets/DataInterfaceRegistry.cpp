#include "DataInterfaceRegistry.h"

#include <QCborMap>
#include <QCborStreamReader>
#include <QCborStreamWriter>
#include <QSharedPointer>

#include <cres_qt>
#include <knowCore/Uris/askcore_graph.h>

#include <kDB/Version.h>

#include "Collection.h"
#include "Dataset.h"
#include "Interfaces/DataInterface.h"
#include "Statistics.h"

using namespace kDBDatasets;

struct DataInterfaceRegistry::Private
{
  QHash<knowCore::Uri, Interfaces::DataInterface*> interfaces;
  static Private* instance();
  static cres_qresult<Interfaces::DataInterface*> get(const Dataset& _dataset);
};

DataInterfaceRegistry::Private* DataInterfaceRegistry::Private::instance()
{
  static Private* s_instance = nullptr;
  if(Q_UNLIKELY(not s_instance))
  {
    s_instance = new Private;
  }
  return s_instance;
}

cres_qresult<Interfaces::DataInterface*>
  DataInterfaceRegistry::Private::get(const Dataset& _dataset)
{
  cres_try(knowCore::Uri u, _dataset.contentType());
  Interfaces::DataInterface* di = instance()->interfaces.value(u);
  if(di)
  {
    return cres_success(di);
  }
  else
  {
    return cres_failure("No data interface for type '{}' of dataset '{}'", _dataset.type(),
                        _dataset.uri());
  }
}

cres_qresult<InsertIterator>
  DataInterfaceRegistry::createInsertIterator(const kDB::Repository::Connection& _connection,
                                              const Dataset& _dataset)
{
  cres_try(Interfaces::DataInterface * interface, Private::get(_dataset));
  return cres_qresult<InsertIterator>(interface->createInsertIterator(_connection, _dataset));
}

cres_qresult<ValueIterator>
  DataInterfaceRegistry::createValueIterator(const kDB::Repository::Connection& _connection,
                                             const Dataset& _dataset)
{
  cres_try(Interfaces::DataInterface * interface, Private::get(_dataset));
  return cres_qresult<ValueIterator>(interface->createValueIterator(_connection, _dataset));
}

cres_qresult<ExtractIterator>
  DataInterfaceRegistry::createExtractIterator(const kDB::Repository::Connection& _connection,
                                               const Dataset& _dataset)
{
  cres_try(Interfaces::DataInterface * interface, Private::get(_dataset));
  return cres_qresult<ExtractIterator>(interface->createExtractIterator(_connection, _dataset));
}

cres_qresult<Statistics>
  DataInterfaceRegistry::statistics(const kDB::Repository::Connection& _connection,
                                    const Dataset& _dataset)
{
  cres_try(Interfaces::DataInterface * interface, Private::get(_dataset));
  return interface->statistics(_connection, _dataset);
}

cres_qresult<void> DataInterfaceRegistry::exportTo(const kDB::Repository::Connection& _connection,
                                                   const Dataset& _dataset, QIODevice* _device)
{
  cres_try(ExtractIterator iterator, createExtractIterator(_connection, _dataset));

  QCborStreamWriter writter(_device);
  // root has two keys, metadata and data
  writter.startMap(2);
  writter.append("metadata"_kCs);

  // metadata has 3 keys
  writter.startMap(3);
  writter.append("kdb_version"_kCs);
  writter.append(KDB_VERSION);
  writter.append("uri"_kCs);
  writter.append(QString(_dataset.uri()));
  writter.append("definition"_kCs);
  cres_try(QCborMap definition_cbor, _dataset.toCborMap());
  writter.append(definition_cbor.toCborValue().toCbor());
  cres_check(writter.endMap(), "Failure to finish metadata map.");

  // data is an array of unknown size
  writter.append("data"_kCs);
  writter.startArray();

  // save the data, as it comes from
  while(iterator.hasNext())
  {
    cres_try(QByteArray nextV, iterator.next());
    writter.append(nextV);
  }
  cres_check(writter.endArray(), "Failure to finish data array.");

  // Close root map
  cres_check(writter.endMap(), "Failure to finish map.");
  return cres_success();
}

#define READ_STRING(variable, msg)                                                                 \
  {                                                                                                \
    if(not reader.isString())                                                                      \
    {                                                                                              \
      return cres_failure("Exepcted a string for {}, got {}.", msg, int(reader.type()));           \
    }                                                                                              \
    QCborStreamReader::StringResult<QString> __sr__ = reader.readString();                         \
    if(__sr__.status == QCborStreamReader::Error)                                                  \
    {                                                                                              \
      return cres_failure("Failed to read string, {}, with error: {}", msg,                        \
                          reader.lastError().toString());                                          \
    }                                                                                              \
    reader.next();                                                                                 \
    variable = __sr__.data;                                                                        \
  }                                                                                                \
  (void)0
#define READ_INTEGER(variable, msg)                                                                \
  {                                                                                                \
    if(not reader.isInteger())                                                                     \
    {                                                                                              \
      return cres_failure("Exepcted an integer for {}, got {}.", msg, int(reader.type()));         \
    }                                                                                              \
    variable = reader.toInteger();                                                                 \
    reader.next();                                                                                 \
  }                                                                                                \
  (void)0

#define READ_BYTEARRAY(variable, msg)                                                              \
  {                                                                                                \
    QCborStreamReader::StringResult<QByteArray> __sr__ = reader.readByteArray();                   \
    if(__sr__.status == QCborStreamReader::Error)                                                  \
    {                                                                                              \
      return cres_failure("Failed to read string, {}, with error: {}", msg,                        \
                          reader.lastError().toString());                                          \
    }                                                                                              \
    variable = __sr__.data;                                                                        \
    reader.next();                                                                                 \
  }                                                                                                \
  (void)0

#define EXPECT_KEY(__expected__, msg)                                                              \
  {                                                                                                \
    QString __key__;                                                                               \
    READ_STRING(__key__, msg);                                                                     \
    cres_check(__key__ == __expected__, "Key {} was expected but got {}.", __expected__, __key__); \
  }                                                                                                \
  (void)0

cres_qresult<void> DataInterfaceRegistry::importFrom(const kDB::Repository::Connection& _connection,
                                                     const Collection& _collection,
                                                     QIODevice* _device)
{
  QCborStreamReader reader(_device);
  cres_check(reader.isMap(), "Expected root to be a map.");
  cres_check(reader.enterContainer(), "Failed to enter root map.");

  // Read metadata
  EXPECT_KEY("metadata", "in root map");
  cres_check(reader.isMap(), "Expected metadata to be a map.");
  cres_check(reader.enterContainer(), "Failed to enter metadata map.");
  knowCore::Uri datasetUri;
  QByteArray datasetDefinition;
  while(reader.hasNext())
  {
    QString key;
    READ_STRING(key, "in metadata key");
    if(key == "uri")
    {
      READ_STRING(datasetUri, "reading the dataset uri");
    }
    else if(key == "kdb_version")
    {
      int version;
      READ_INTEGER(version, "kdb version");
      if(version > QT_VERSION_CHECK((KDB_VERSION_MAJOR + 1), 0, 0))
      {
        return cres_failure("Reading from datasets with newer version of kDB is not supported.");
      }
    }
    else if(key == "definition")
    {
      READ_BYTEARRAY(datasetDefinition, "dataset definition");
    }
    else
    {
      clog_info("Ignoring {}.", key);
      reader.next();
    }
  }
  kDBDatasets::Dataset dataset;
  cres_qresult<Dataset> dataset_rv
    = kDBDatasets::Collection::allDatasets(_connection).dataset(datasetUri);

  if(dataset_rv.is_successful())
  {
    dataset = dataset_rv.get_value();
  }
  else
  {
    cres_try(dataset, kDBDatasets::Collection(_collection)
                        .insertDatasetFromCbor(QCborValue::fromCbor(datasetDefinition).toMap()));
  }

  cres_check(reader.leaveContainer(), "Failed to leave metadata map.");

  EXPECT_KEY("data", "in root map");
  cres_try(InsertIterator iterator, createInsertIterator(_connection, dataset));
  cres_check(reader.isArray(), "Expected metadata to be a map.");
  cres_check(reader.enterContainer(), "Failed to enter data array.");

  while(reader.hasNext())
  {
    QByteArray data;
    READ_BYTEARRAY(data, "reading data");
    cres_try(cres_ignore, iterator.next(data));
  }
  cres_try(cres_ignore, iterator.finalise());

  cres_check(reader.leaveContainer(), "Failed to leave data array.");
  cres_check(reader.leaveContainer(), "Failed to leave root map.");

  return cres_success();
}

void DataInterfaceRegistry::registerInterface(const knowCore::Uri& _datatype,
                                              Interfaces::DataInterface* _interface)
{
  Private::instance()->interfaces[_datatype] = _interface;
}
