#include <knowGIS/GeometryObject.h>

namespace kDBDatasets::Test
{
  /**
   * @ingroup kDBDatasets
   *
   * Create region A from (0,0) to (30, 20), as shown on the following figure:
   *
   * \image html dataset_tests.png
   * \image latex dataset_tests.pdf
   */
  knowGIS::GeometryObject createRegionA();
  /**
   * @ingroup kDBDatasets
   *
   * Create region B from (30,0) to (60, 20), as shown on the following figure:
   *
   * \image html dataset_tests.png
   * \image latex dataset_tests.pdf
   */
  knowGIS::GeometryObject createRegionB();
  /**
   * @ingroup kDBDatasets
   *
   * Create region C from (10,10) to (40, 30), as shown on the following figure:
   *
   * \image html dataset_tests.png
   * \image latex dataset_tests.pdf
   */
  knowGIS::GeometryObject createRegionC();
  /**
   * @ingroup kDBDatasets
   *
   * Create region D from (0,10) to (30, 30), as shown on the following figure:
   *
   * \image html dataset_tests.png
   * \image latex dataset_tests.pdf
   */
  knowGIS::GeometryObject createRegionD();
  /**
   * @ingroup kDBDatasets
   *
   * Create region E from (40,10) to (60, 40), as shown on the following figure:
   *
   * \image html dataset_tests.png
   * \image latex dataset_tests.pdf
   */
  knowGIS::GeometryObject createRegionE();
  /**
   * @ingroup kDBDatasets
   *
   * Create region F from (0,0) to (60, 40), as shown on the following figure:
   *
   * \image html dataset_tests.png
   * \image latex dataset_tests.pdf
   */
  knowGIS::GeometryObject createRegionF();
} // namespace kDBDatasets::Test
