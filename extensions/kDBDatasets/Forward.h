#pragma once

#include <kDBGIS/Forward.h>

namespace kDBDatasets
{
  class Dataset;
  class Collection;
  class Statistics;
  namespace Interfaces
  {
    class ExtractIterator;
    class InsertIterator;
    class DataInterface;
    class DataInterface;
    class ValueIterator;
  } // namespace Interfaces
} // namespace kDBDatasets
