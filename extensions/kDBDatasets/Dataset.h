#pragma once

#include <QExplicitlySharedDataPointer>

#include <knowCore/Value.h>

#include "Forward.h"
#include <kDB/Repository/RDF/FocusNode.h>

namespace kDBDatasets
{
  /**
   * @ingroup kDBDatasets
   * This class allow to handle and manage a dataset
   */
  class Dataset : public kDB::Repository::RDF::FocusNodeWrapper<Dataset>
  {
    friend class Collection;
  public:
    enum class Status
    {
      Completed,     //< The data has been acquired or computed
      InProgress,    //< The data is currently been acquired or been computed
      InPreparation, //< The acquisition or computation of the data is been prepared
      Scheduled,     //< The acquisition or computation of the data is been scheduled
      Unknown        //< The status is unknown or not specified
    };
  public:
    /**
     * Create an invalid dataset
     */
    Dataset();
    Dataset(const Dataset& _rhs);
    Dataset& operator=(const Dataset& _rhs);
    ~Dataset();
    static cres_qresult<Dataset> fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node);
  public:
    /**
     * Start time for the dataset.
     */
    cres_qresult<knowCore::Timestamp> startTime() const;
    /**
     * End time for the dataset.
     */
    cres_qresult<knowCore::Timestamp> endTime() const;
    /**
     * List of datasets that were used to create this dataset.
     */
    cres_qresult<QList<Dataset>> createdFrom() const;
    /**
     * @return the geometry of the object
     */
    cres_qresult<knowGIS::GeometryObject> geometry() const;
    /**
     * @return the content type of the dataset
     */
    cres_qresult<knowCore::Uri> contentType() const;
    /**
     * Associate this dataset to agent \ref _agent in all graph of datasets where this dataset is
     * defined.
     */
    cres_qresult<void> associate(const knowCore::Uri& _agent);
    /**
     * Dissociate this dataset to agent \ref _agent in all graph of datasets where this dataset is
     * defined.
     */
    cres_qresult<void> dissociate(const knowCore::Uri& _agent);
    /**
     * @return the list of agents associated with the dataset (i.e. agents who claim to have the
     * data).
     */
    cres_qresult<QList<knowCore::Uri>> associatedAgents() const;
    /**
     * @return the current status of the dataset
     */
    cres_qresult<Status> status() const;
    /**
     * @return the current status of the dataset uri
     */
    cres_qresult<knowCore::Uri> statusUri() const;
    /**
     * Set the status of the dataset
     */
    cres_qresult<void> setStatus(Status) const;
    /**
     * Set the status of the dataset uri
     */
    cres_qresult<void> setStatus(const knowCore::Uri& _uri) const;
  };
} // namespace kDBDatasets

#include <knowCore/Formatter.h>

clog_format_declare_enum_formatter(kDBDatasets::Dataset::Status, Completed, InProgress,
                                   InPreparation, Scheduled, Unknown);
