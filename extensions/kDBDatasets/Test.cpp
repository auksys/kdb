#include "Test.h"

using namespace kDBDatasets;

knowGIS::GeometryObject Test::createRegionA()
{
  return knowGIS::GeometryObject::fromWKT("POLYGON ((0 0, 40 0, 40 20, 0 20, 0 0))")
    .expect_success();
}

knowGIS::GeometryObject Test::createRegionB()
{
  return knowGIS::GeometryObject::fromWKT("POLYGON ((30 0, 60 0, 60 20, 30 20, 30 0))")
    .expect_success();
}

knowGIS::GeometryObject Test::createRegionC()
{
  return knowGIS::GeometryObject::fromWKT("POLYGON ((10 10, 40 10, 40 30, 10 30, 10 10))")
    .expect_success();
}

knowGIS::GeometryObject Test::createRegionD()
{
  return knowGIS::GeometryObject::fromWKT("POLYGON ((0 10, 30 10, 30 40, 0 40, 0 10))")
    .expect_success();
}

knowGIS::GeometryObject Test::createRegionE()
{
  return knowGIS::GeometryObject::fromWKT("POLYGON ((40 10, 60 10, 60 40, 40 40, 40 10))")
    .expect_success();
}

knowGIS::GeometryObject Test::createRegionF()
{
  return knowGIS::GeometryObject::fromWKT("POLYGON ((0 0, 60 0, 60 40, 0 40, 0 0))")
    .expect_success();
}
