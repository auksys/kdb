add_subdirectory(tests)

qt6_add_resources(KDBDATASETS_DATA_SRC kdbdatasets_data.qrc  )

# Create library

set(KDBDATASETS_SRCS
  Initialise.cpp
  Collection.cpp
  Dataset.cpp
  DataInterfaceRegistry.cpp
  krQueryActions.cpp
  Statistics.cpp
  Test.cpp
  Interfaces/Interfaces.cpp
  )

add_library(kDBDatasets SHARED ${KDBDATASETS_SRCS} ${KDBDATASETS_DATA_SRC})
target_link_libraries(kDBDatasets PUBLIC kDBRobotics PRIVATE knowSHACL )

# Install
install(TARGETS kDBDatasets EXPORT kDBTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )
set(PROJECT_EXPORTED_TARGETS kDBDatasets  ${PROJECT_EXPORTED_TARGETS} CACHE INTERNAL "")

install( FILES
  Collection.h
  DataInterfaceRegistry.h
  Dataset.h
  Forward.h
  Initialise.h
  Statistics.h
  Test.h
  DESTINATION ${INSTALL_INCLUDE_DIR}/kDBDatasets )

  install( FILES
  Interfaces/ExtractIterator.h
  Interfaces/InsertIterator.h
  Interfaces/DataInterface.h
  Interfaces/ValueIterator.h
  DESTINATION ${INSTALL_INCLUDE_DIR}/kDBDatasets/Interfaces )
