#include <QExplicitlySharedDataPointer>

#include "Forward.h"

namespace kDBDatasets
{
  /**
   * @ingroup kDBDatasets
   * This class holds some statistics about a dataset.
   */
  class Statistics
  {
  public:
    Statistics();
    Statistics(const Dataset& _dataset, qsizetype _datapoints_count);
    Statistics(const Statistics& _rhs);
    Statistics& operator=(const Statistics& _rhs);
    ~Statistics();
    /**
     * @return the number of data points in a dataset, i.e., the number of images, salient points...
     */
    qsizetype datapointsCount() const;
    bool isValid() const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace kDBDatasets
