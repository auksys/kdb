#include "TestDatasets.h"

#include <knowCore/ConstrainedValue.h>
#include <knowCore/QuantityValue.h>
#include <knowCore/UriList.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/PersistentDatasetsUnion.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryStore.h>

#include <kDBGIS/Test.h>
#include <knowGIS/GeometryObject.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/Dataset.h>
#include <kDBDatasets/Initialise.h>

#include <kDB/Repository/Test.h>

#include <knowCore/Uris/askcore_dataset.h>
#include <knowCore/Uris/askcore_graph.h>
#include <knowCore/Uris/askcore_unit.h>
#include <knowCore/Uris/time.h>
#include <knowGIS/Uris/geo.h>
#include <knowValues/Uris/askcore_sensing.h>

using askcore_dataset = knowCore::Uris::askcore_dataset;
using askcore_sensing = knowValues::Uris::askcore_sensing;
using askcore_unit = knowCore::Uris::askcore_unit;
using u_time = knowCore::Uris::time;

void TestDatasets::testInitialisation()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBDatasets"));
}

void TestDatasets::testCreationAccess()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBDatasets"));

  // Create a dss1: set of datasets
  kDBDatasets::Collection dss1;

  QCOMPARE(dss1.isValid(), false);
  dss1 = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c, "dss1"_kCu));
  QVERIFY(dss1.isValid());

  QVERIFY(CRES_QVERIFY(c.graphsManager()->getUnion(knowCore::Uris::askcore_graph::all_focus_nodes))
            .contains("dss1"_kCu));

  // Create a dss2: set of datasets
  kDBDatasets::Collection dss2 = CRES_QVERIFY(kDBDatasets::Collection::create(c, "dss2"_kCu));

  QCOMPARE(dss2.isValid(), true);
  QVERIFY(CRES_QVERIFY(c.graphsManager()->getUnion(knowCore::Uris::askcore_graph::all_focus_nodes))
            .contains("dss2"_kCu));

  // Check that getting dsss1 gives it back
  CRES_QCOMPARE(kDBDatasets::Collection::get(c, "dss1"_kCu), dss1);

  // Check that dss1 cannot be created twice
  CRES_QVERIFY_FAILURE(kDBDatasets::Collection::create(c, "dss1"_kCu));

  // Check that getting uncreated datasets fails
  CRES_QVERIFY_FAILURE(dss1.dataset("dataset1"_kCu));
  CRES_QVERIFY_FAILURE(dss2.dataset("dataset1"_kCu));

  // Create a dataset
  knowGIS::GeometryObject go0 = CRES_QVERIFY(
    knowGIS::GeometryObject::fromWKT("POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))"));
  //   knowGIS::GeometryObject go1 = knowGIS::GeometryObject::fromWKT("MULTIPOLYGON (((30 20, 45 40,
  //   10 40, 30 20)), ((15 5, 40 10, 10 20, 5 10, 15 5)))");

  kDBDatasets::Dataset ds1 = CRES_QVERIFY(dss1.createDataset(
    askcore_sensing::point_cloud, go0,
    {askcore_sensing::point_density,
     knowCore::QuantityNumber::create(100, askcore_unit::point_per_m2).expect_success()},
    "dataset1"_kCu));
  CRES_QCOMPARE(ds1.exists(), true);
  QCOMPARE(ds1.type(), askcore_dataset::point_cloud_dataset);
  CRES_QCOMPARE(ds1.contentType(), askcore_sensing::point_cloud);
  QCOMPARE(ds1.geometry(), go0);
  QCOMPARE(ds1.uri(), "dataset1");

  CRES_QCOMPARE(dss1.dataset("dataset1"_kCu), ds1);
  CRES_QVERIFY_FAILURE(dss2.dataset("dataset1"_kCu));

  c.disconnect();
  c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY_FAILURE(kDBDatasets::Collection::create(c, "dss1"_kCu));
  CRES_QVERIFY_FAILURE(kDBDatasets::Collection::create(c, "dss2"_kCu));

  kDBDatasets::Collection dss1_b
    = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c, "dss1"_kCu));
  kDBDatasets::Collection dss2_b = CRES_QVERIFY(kDBDatasets::Collection::get(c, "dss2"_kCu));

  QCOMPARE(dss1.uri(), dss1_b.uri());
  QCOMPARE(dss2.uri(), dss2_b.uri());

  kDBDatasets::Dataset ds1_b = CRES_QVERIFY(dss1_b.dataset("dataset1"_kCu));
  QCOMPARE(ds1, ds1_b);

  // Check in all datasets
  QList<kDBDatasets::Dataset> all_datasets;

  KNOWCORE_TEST_WAIT_FOR_ACT(all_datasets.size() == 1, {
    all_datasets = CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).all());
  });
  QCOMPARE(all_datasets.size(), 1);
  QCOMPARE(all_datasets[0].uri(), "dataset1");
  CRES_QCOMPARE(all_datasets[0].geometry(), go0);
  QCOMPARE(all_datasets[0].type(), askcore_dataset::point_cloud_dataset);
  CRES_QCOMPARE(all_datasets[0].contentType(), askcore_sensing::point_cloud);

  // Check in all focus nodes
  QList<kDB::Repository::RDF::FocusNode> all_focus_nodes;

  KNOWCORE_TEST_WAIT_FOR_ACT(all_focus_nodes.size() == 1, {
    all_focus_nodes
      = CRES_QVERIFY(kDB::Repository::RDF::FocusNodeCollection::allFocusNodes(c).all());
  });
  QCOMPARE(all_focus_nodes.size(), 1);
  QCOMPARE(all_focus_nodes[0].uri(), "dataset1");
  CRES_QCOMPARE(
    all_focus_nodes[0].property<knowGIS::GeometryObject>(knowGIS::Uris::geo::hasGeometry), go0);
  QCOMPARE(all_focus_nodes[0].declaration().type(), askcore_dataset::point_cloud_dataset);

  // Insert ds1 in to the datasets
  CRES_QVERIFY(dss2_b.insertDatasetFromCbor(CRES_QVERIFY(ds1_b.toCborMap())));

  CRES_QCOMPARE(dss2_b.dataset("dataset1"_kCu), ds1_b);

  // Check in all datasets
  KNOWCORE_TEST_WAIT_FOR_ACT(all_datasets.size() == 1, {
    all_datasets = CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).all());
  });
  QCOMPARE(all_datasets.size(), 1);
  QCOMPARE(all_datasets[0].uri(), "dataset1");
  CRES_QCOMPARE(all_datasets[0].geometry(), go0);
  QCOMPARE(all_datasets[0].type(), askcore_dataset::point_cloud_dataset);
  CRES_QCOMPARE(all_datasets[0].contentType(), askcore_sensing::point_cloud);

  // Check in all focus nodes
  KNOWCORE_TEST_WAIT_FOR_ACT(all_focus_nodes.size() == 1, {
    all_focus_nodes
      = CRES_QVERIFY(kDB::Repository::RDF::FocusNodeCollection::allFocusNodes(c).all());
  });
  QCOMPARE(all_focus_nodes.size(), 1);
  QCOMPARE(all_focus_nodes[0].uri(), "dataset1");
  CRES_QCOMPARE(
    all_focus_nodes[0].property<knowGIS::GeometryObject>(knowGIS::Uris::geo::hasGeometry), go0);
  QCOMPARE(all_focus_nodes[0].declaration().type(), askcore_dataset::point_cloud_dataset);

  // Disconnect and clear the cache
  c.disconnect();
  c = store.createConnection();
  CRES_QVERIFY(c.connect());

  kDBDatasets::Collection dss1_c = CRES_QVERIFY(kDBDatasets::Collection::get(c, "dss1"_kCu));
  kDBDatasets::Collection dss2_c = CRES_QVERIFY(kDBDatasets::Collection::get(c, "dss2"_kCu));

  kDBDatasets::Dataset ds1_c = CRES_QVERIFY(dss1_c.dataset("dataset1"_kCu));
  QVERIFY(ds1 == ds1_c);

  CRES_QCOMPARE(ds1_c.exists(), true);
  QCOMPARE(ds1_c.type(), askcore_dataset::point_cloud_dataset);
  QCOMPARE(ds1_c.geometry(), go0);
  QCOMPARE(ds1_c.uri(), "dataset1");

  CRES_QCOMPARE(dss2_c.dataset("dataset1"_kCu), ds1_c);

  // Test getting from all datasets
  ds1_c = CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).dataset("dataset1"_kCu));

  CRES_QCOMPARE(ds1_c.exists(), true);
  QVERIFY(ds1 == ds1_c);
  QCOMPARE(ds1_c.type(), askcore_dataset::point_cloud_dataset);
  QCOMPARE(ds1_c.geometry(), go0);
  QCOMPARE(ds1_c.uri(), "dataset1");

  CRES_QCOMPARE(dss2_c.dataset("dataset1"_kCu), ds1_c);

  QCOMPARE(CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).all()).size(), 1);
  QCOMPARE(CRES_QVERIFY(dss1_c.all()).size(), 1);
  QCOMPARE(CRES_QVERIFY(dss2_c.all()).size(), 1);

  // Test number of Datasets
  QCOMPARE(CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).count()), 1);
  CRES_QCOMPARE(dss1_c.count(), 1);
  CRES_QCOMPARE(dss2_c.count(), 1);
}

void TestDatasets::testConstrainedAccess()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBDatasets"));

  // Create a dss1: set of datasets
  kDBDatasets::Collection dss1;

  QCOMPARE(dss1.isValid(), false);
  CRES_QVERIFY_FAILURE(kDBDatasets::Collection::get(c, "dss1"_kCu));
  dss1 = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c, "dss1"_kCu));
  QCOMPARE(dss1.isValid(), true);

  QVERIFY(CRES_QVERIFY(c.graphsManager()->getUnion(knowCore::Uris::askcore_graph::all_focus_nodes))
            .contains("dss1"_kCu));

  // Create a dataset
  knowGIS::GeometryObject go0 = CRES_QVERIFY(
    knowGIS::GeometryObject::fromWKT("POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))"));

  kDBDatasets::Dataset ds1
    = CRES_QVERIFY(dss1.createDataset(askcore_sensing::point_cloud, go0, {}, "dataset1"_kCu));
  CRES_QCOMPARE(ds1.exists(), true);
  QCOMPARE(ds1.type(), askcore_dataset::point_cloud_dataset);
  CRES_QCOMPARE(ds1.contentType(), askcore_sensing::point_cloud);
  QCOMPARE(ds1.geometry(), go0);
  QCOMPARE(ds1.uri(), "dataset1");

  CRES_QCOMPARE(dss1.dataset("dataset1"_kCu), ds1);

  QList<kDBDatasets::Dataset> result
    = CRES_QVERIFY(dss1.datasets(QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>()));
  QCOMPARE(result.size(), 1);
  QCOMPARE(result[0], ds1);

  result = CRES_QVERIFY(
    dss1.datasets(knowCore::Uris::askcore_dataset::content_type,
                  knowCore::ConstrainedValue().apply(
                    knowCore::Value::fromValue<knowCore::Uri>(askcore_sensing::point_cloud),
                    knowCore::ConstrainedValue::Type::Equal)));

  QCOMPARE(result.size(), 1);
  QCOMPARE(result[0], ds1);

  CRES_QVERIFY_FAILURE(dss1.datasets(
    knowCore::Uris::askcore_dataset::content_type,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue("someothertype"_kCu),
                                       knowCore::ConstrainedValue::Type::Equal)));
  result = CRES_QVERIFY(
    dss1.datasets(knowCore::Uris::askcore_dataset::content_type,
                  knowCore::ConstrainedValue().apply(
                    knowCore::Value::fromValue<knowCore::Uri>(askcore_sensing::lidar_scan),
                    knowCore::ConstrainedValue::Type::Equal)));

  QCOMPARE(result.size(), 0);

  // Test geometry constraints
  knowGIS::GeometryObject go1 = CRES_QVERIFY(
    knowGIS::GeometryObject::fromWKT("POLYGON ((31 11, 41 41, 21 41, 11 21, 31 11))"));
  knowGIS::GeometryObject go2 = CRES_QVERIFY(
    knowGIS::GeometryObject::fromWKT("POLYGON ((300 100, 400 400, 200 400, 100 200, 300 100))"));
  knowGIS::GeometryObject go3 = CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(
    "POLYGON ((-100 -100, 100 -100, 100 100, -100 100, -100 -100))"));

  result = CRES_QVERIFY(dss1.datasets(
    knowGIS::Uris::geo::hasGeometry,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(go1),
                                       knowCore::ConstrainedValue::Type::GeoOverlaps)));

  QCOMPARE(result.size(), 1);
  QCOMPARE(result[0], ds1);

  result = CRES_QVERIFY(dss1.datasets(
    knowGIS::Uris::geo::hasGeometry,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(go2),
                                       knowCore::ConstrainedValue::Type::GeoOverlaps)));
  QCOMPARE(result.size(), 0);

  result = CRES_QVERIFY(
    dss1.datasets(knowGIS::Uris::geo::hasGeometry,
                  knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(go1),
                                                     knowCore::ConstrainedValue::Type::GeoWithin)));
  QCOMPARE(result.size(), 0);
  result = CRES_QVERIFY(
    dss1.datasets(knowGIS::Uris::geo::hasGeometry,
                  knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(go2),
                                                     knowCore::ConstrainedValue::Type::GeoWithin)));
  QCOMPARE(result.size(), 0);
  result = CRES_QVERIFY(
    dss1.datasets(knowGIS::Uris::geo::hasGeometry,
                  knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(go3),
                                                     knowCore::ConstrainedValue::Type::GeoWithin)));
  QCOMPARE(result.size(), 1);
  QCOMPARE(result[0], ds1);

  // Test combined constraints
  result = CRES_QVERIFY(dss1.datasets(
    knowCore::Uris::askcore_dataset::content_type,
    knowCore::ConstrainedValue().apply(
      knowCore::Value::fromValue<knowCore::Uri>(askcore_sensing::point_cloud),
      knowCore::ConstrainedValue::Type::Equal),
    knowGIS::Uris::geo::hasGeometry,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(go1),
                                       knowCore::ConstrainedValue::Type::GeoOverlaps)));

  QCOMPARE(result.size(), 1);
  QCOMPARE(result[0], ds1);

  // Test combined constraints on all_focus_nodes
  result = CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).datasets(
    knowCore::Uris::askcore_dataset::content_type,
    knowCore::ConstrainedValue().apply(
      knowCore::Value::fromValue<knowCore::Uri>(askcore_sensing::point_cloud),
      knowCore::ConstrainedValue::Type::Equal),
    knowGIS::Uris::geo::hasGeometry,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(go1),
                                       knowCore::ConstrainedValue::Type::GeoOverlaps)));

  QCOMPARE(result.size(), 1);
  QCOMPARE(result[0], ds1);

  // Test extra properties
  knowCore::Timestamp t10 = knowCore::Timestamp::fromEpoch(knowCore::NanoSeconds(10));
  knowCore::Timestamp t100 = knowCore::Timestamp::fromEpoch(knowCore::NanoSeconds(100));
  CRES_QVERIFY(ds1.setProperty(u_time::hasBeginning, t10));

  result = CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).datasets(
    u_time::hasBeginning,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(t10),
                                       knowCore::ConstrainedValue::Type::Inferior)));

  QCOMPARE(result.size(), 0);

  result = CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).datasets(
    u_time::hasBeginning,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(t100),
                                       knowCore::ConstrainedValue::Type::Inferior)));

  QCOMPARE(result.size(), 1);
  QCOMPARE(result[0], ds1);

  // BEGIN Test extra properties with units
  knowCore::Unit ptm2u = CRES_QVERIFY(knowCore::Unit::bySymbol("point/m^2"));
  knowCore::QuantityNumber qn_10 = knowCore::QuantityNumber(10ull, ptm2u);
  knowCore::QuantityNumber qn_100 = knowCore::QuantityNumber(100ull, ptm2u);

  // Test failure for extra properties that are not common
  CRES_QVERIFY_FAILURE(kDBDatasets::Collection::allDatasets(c).datasets(
    askcore_sensing::point_density,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(qn_10),
                                       knowCore::ConstrainedValue::Type::Inferior)));

  // Set the property
  CRES_QVERIFY(ds1.setProperty(askcore_sensing::point_density, qn_10));

  result = CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).datasets(
    askcore_dataset::content_type,
    knowCore::ConstrainedValue().apply(
      knowCore::Value::fromValue<knowCore::Uri>(askcore_sensing::point_cloud),
      knowCore::ConstrainedValue::Type::Equal),
    askcore_sensing::point_density,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(qn_10),
                                       knowCore::ConstrainedValue::Type::Inferior)));

  QCOMPARE(result.size(), 0);

  result = CRES_QVERIFY(kDBDatasets::Collection::allDatasets(c).datasets(
    askcore_dataset::content_type,
    knowCore::ConstrainedValue().apply(
      knowCore::Value::fromValue<knowCore::Uri>(askcore_sensing::point_cloud),
      knowCore::ConstrainedValue::Type::Equal),
    askcore_sensing::point_density,
    knowCore::ConstrainedValue().apply(knowCore::Value::fromValue(qn_100),
                                       knowCore::ConstrainedValue::Type::Inferior)));

  QCOMPARE(result.size(), 1);
  QCOMPARE(result[0], ds1);
  // END Test extra properties with units
}

void TestDatasets::testStatusOwner()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBDatasets"));

  // Create dss1/dss2: set of datasets
  kDBDatasets::Collection dss1 = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c, "dss1"_kCu));
  QCOMPARE(dss1.isValid(), true);

  kDBDatasets::Collection dss2 = CRES_QVERIFY(kDBDatasets::Collection::create(c, "dss2"_kCu));
  QCOMPARE(dss2.isValid(), true);

  knowGIS::GeometryObject go0 = CRES_QVERIFY(
    knowGIS::GeometryObject::fromWKT("POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))"));
  //   knowGIS::GeometryObject go1 = knowGIS::GeometryObject::fromWKT("MULTIPOLYGON (((30 20, 45 40,
  //   10 40, 30 20)), ((15 5, 40 10, 10 20, 5 10, 15 5)))");

  kDBDatasets::Dataset ds1 = CRES_QVERIFY(dss1.createDataset(
    askcore_sensing::point_cloud, go0,
    {askcore_sensing::point_density,
     knowCore::QuantityNumber::create(100, askcore_unit::point_per_m2).expect_success()},
    "dataset1"_kCu));
  CRES_QCOMPARE(ds1.exists(), true);
  dss2.insertDatasetFromCbor(ds1.toCborMap().expect_success());
  QCOMPARE(CRES_QVERIFY(ds1.status()), kDBDatasets::Dataset::Status::Unknown);
  CRES_QVERIFY(ds1.setStatus(kDBDatasets::Dataset::Status::Completed));
  QCOMPARE(CRES_QVERIFY(ds1.status()), kDBDatasets::Dataset::Status::Completed);
  QCOMPARE(CRES_QVERIFY(ds1.associatedAgents()), {});
  CRES_QVERIFY(ds1.associate("someagent"_kCu));
  QCOMPARE(CRES_QVERIFY(ds1.associatedAgents()), {"someagent"_kCu});

  CRES_QVERIFY_FAILURE(ds1.setStatus("notavalidstatus"_kCu));
  QCOMPARE(CRES_QVERIFY(ds1.status()), kDBDatasets::Dataset::Status::Completed);

  kDBDatasets::Dataset ds2 = CRES_QVERIFY(dss1.createDataset(
    askcore_sensing::point_cloud, go0,
    {askcore_sensing::point_density,
     knowCore::QuantityNumber::create(100, askcore_unit::point_per_m2).expect_success()},
    "dataset2"_kCu));
  CRES_QCOMPARE(ds2.exists(), true);
  dss2.insertDatasetFromCbor(ds2.toCborMap().expect_success());
  CRES_QCOMPARE(ds2.status(), kDBDatasets::Dataset::Status::Unknown);
  CRES_QVERIFY(ds2.setStatus(kDBDatasets::Dataset::Status::InPreparation));
  CRES_QCOMPARE(ds2.status(), kDBDatasets::Dataset::Status::InPreparation);
  CRES_QCOMPARE(ds2.associatedAgents(), {});
  CRES_QVERIFY(ds2.associate("someotheragent"_kCu));
  CRES_QCOMPARE(ds2.associatedAgents(), {"someotheragent"_kCu});

  CRES_QCOMPARE(ds1.status(), kDBDatasets::Dataset::Status::Completed);
  CRES_QCOMPARE(ds1.associatedAgents(), {"someagent"_kCu});

  // Disconnect and clear the cache
  c.disconnect();
  c = store.createConnection();
  CRES_QVERIFY(c.connect());

  kDBDatasets::Collection dss1_b = CRES_QVERIFY(kDBDatasets::Collection::get(c, "dss1"_kCu));
  kDBDatasets::Collection dss2_b = CRES_QVERIFY(kDBDatasets::Collection::get(c, "dss2"_kCu));

  kDBDatasets::Dataset ds1_b = CRES_QVERIFY(dss1_b.dataset("dataset1"_kCu));
  CRES_QCOMPARE(ds1_b.exists(), true);
  kDBDatasets::Dataset ds2_b = CRES_QVERIFY(dss2_b.dataset("dataset2"_kCu));
  CRES_QCOMPARE(ds2_b.exists(), true);

  QCOMPARE(CRES_QVERIFY(ds1_b.status()), kDBDatasets::Dataset::Status::Completed);
  QCOMPARE(CRES_QVERIFY(ds1_b.associatedAgents()), {"someagent"_kCu});
  QCOMPARE(CRES_QVERIFY(ds2_b.status()), kDBDatasets::Dataset::Status::Unknown);
  QCOMPARE(CRES_QVERIFY(ds2_b.associatedAgents()), {});
}

void TestDatasets::testProperties()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBDatasets"));

  // Create a dss1: set of datasets
  kDBDatasets::Collection dss1;

  QCOMPARE(dss1.isValid(), false);
  CRES_QVERIFY_FAILURE(kDBDatasets::Collection::get(c, "dss1"_kCu));
  dss1 = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c, "dss1"_kCu));
  QCOMPARE(dss1.isValid(), true);

  QVERIFY(CRES_QVERIFY(c.graphsManager()->getUnion(knowCore::Uris::askcore_graph::all_focus_nodes))
            .contains("dss1"_kCu));

  // Create a dataset
  knowGIS::GeometryObject go0 = CRES_QVERIFY(
    knowGIS::GeometryObject::fromWKT("POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))"));

  kDBDatasets::Dataset ds1 = CRES_QVERIFY(dss1.createDataset(
    askcore_sensing::point_cloud, go0,
    {askcore_sensing::point_density,
     knowCore::QuantityNumber::create(100, askcore_unit::point_per_m2).expect_success()},
    "dataset1"_kCu));

  CRES_QCOMPARE(ds1.exists(), true);

  CRES_QCOMPARE(CRES_QVERIFY(ds1.property(askcore_dataset::content_type)).value<knowCore::Uri>(),
                askcore_sensing::point_cloud);
  CRES_QVERIFY_FAILURE(ds1.setProperty(askcore_dataset::content_type, "whatever"_kCu));
  CRES_QCOMPARE(CRES_QVERIFY(ds1.property(askcore_dataset::content_type)).value<knowCore::Uri>(),
                askcore_sensing::point_cloud);

  knowCore::Timestamp t10 = knowCore::Timestamp::fromEpoch(knowCore::NanoSeconds(10));
  knowCore::Timestamp t100 = knowCore::Timestamp::fromEpoch(knowCore::NanoSeconds(100));
  CRES_QVERIFY_FAILURE(ds1.property(u_time::hasBeginning));
  QVERIFY(not CRES_QVERIFY(ds1.hasProperty(u_time::hasBeginning)));
  CRES_QVERIFY(ds1.setProperty(u_time::hasBeginning, t10));
  QVERIFY(CRES_QVERIFY(ds1.hasProperty(u_time::hasBeginning)));
  CRES_QCOMPARE(CRES_QVERIFY(ds1.property(u_time::hasBeginning)).value<knowCore::Timestamp>(), t10);
  CRES_QVERIFY_FAILURE(ds1.setProperty(u_time::hasBeginning, t100));
  CRES_QCOMPARE(CRES_QVERIFY(ds1.property(u_time::hasBeginning)).value<knowCore::Timestamp>(), t10);

  knowCore::Unit ptm2u = CRES_QVERIFY(knowCore::Unit::bySymbol("point/m^2"));
  knowCore::Unit msu = CRES_QVERIFY(knowCore::Unit::bySymbol("m/s"));
  knowCore::QuantityNumber qn_inv = knowCore::QuantityNumber(10ull, msu);
  knowCore::QuantityNumber qn_10 = knowCore::QuantityNumber(10ull, ptm2u);
  knowCore::QuantityNumber qn_100 = knowCore::QuantityNumber(100ull, ptm2u);

  CRES_QCOMPARE(
    CRES_QVERIFY(ds1.property(askcore_sensing::point_density)).value<knowCore::QuantityNumber>(),
    qn_100);
  QVERIFY(CRES_QVERIFY(ds1.hasProperty(askcore_sensing::point_density)));
  CRES_QVERIFY_FAILURE(ds1.setProperty(askcore_sensing::point_density, qn_inv));
  QVERIFY(CRES_QVERIFY(ds1.hasProperty(askcore_sensing::point_density)));
  CRES_QVERIFY_FAILURE(ds1.setProperty(askcore_sensing::point_density, qn_10));
  QVERIFY(CRES_QVERIFY(ds1.hasProperty(askcore_sensing::point_density)));
  CRES_QCOMPARE(
    CRES_QVERIFY(ds1.property(askcore_sensing::point_density)).value<knowCore::QuantityNumber>(),
    qn_100);
}

QTEST_MAIN(TestDatasets)
