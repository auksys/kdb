#include "TestDeclaration.h"

#include <kDB/Repository/RDF/FocusNodeDeclarationsRegistry.h>

#include <knowGIS/Uris/geo.h>

#include <knowCore/Test.h>
#include <knowCore/ValueList.h>

#include <knowCore/Uris/askcore_dataset.h>
#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/askcore_unit.h>
#include <knowCore/Uris/time.h>
#include <knowCore/Uris/xsd.h>
#include <knowRDF/Literal.h>
#include <knowValues/Uris/askcore_sensing.h>

using Declaration = kDB::Repository::RDF::FocusNodeDeclaration;
using Property = Declaration::Property;
using askcore_sensing = knowValues::Uris::askcore_sensing;
using askcore_datatype = knowCore::Uris::askcore_datatype;
using askcore_dataset = knowCore::Uris::askcore_dataset;
using askcore_unit = knowCore::Uris::askcore_unit;
using xsd = knowCore::Uris::xsd;
using geo = knowGIS::Uris::geo;
using kcu_time = knowCore::Uris::time;

inline void kdbdatasets_init_data() { Q_INIT_RESOURCE(kdbdatasets_data); }

void TestDeclaration::testDeclaration()
{
  kdbdatasets_init_data();
  Declaration point_cloud_declaration
    = CRES_QVERIFY(kDB::Repository::RDF::FocusNodeDeclarationsRegistry::declaration(
      askcore_dataset::point_cloud_dataset));
  QList<Property> point_cloud_declaration_fields = point_cloud_declaration.fields();
  QCOMPARE(point_cloud_declaration_fields.size(), 8);

  QList<knowCore::Uri> uniqueFields;

  for(const Property& f : point_cloud_declaration_fields)
  {
    QVERIFY(not uniqueFields.contains(f.path()));
    uniqueFields.append(f.path());

    if(f.path() == geo::hasGeometry)
    {
      QCOMPARE(f.datatype(), geo::Geometry);
      QVERIFY(not f.acceptedUnits());
    }
    else if(f.path() == askcore_dataset::content_type)
    {
      QCOMPARE(f.datatype(), xsd::anyURI);
      QVERIFY(not f.acceptedUnits());
      QCOMPARE(f.value(), knowRDF::Literal::fromValue<knowCore::Uri>(askcore_sensing::point_cloud));
    }
    else if(f.path() == askcore_dataset::dataset_type)
    {
      QCOMPARE(f.datatype(), xsd::anyURI);
      QVERIFY(not f.acceptedUnits());
      QCOMPARE(f.value(),
               knowRDF::Literal::fromValue<knowCore::Uri>(askcore_dataset::point_cloud_dataset));
    }
    else if(f.path() == askcore_dataset::dataset_include)
    {
      QCOMPARE(f.datatype(), askcore_dataset::dataset);
      QVERIFY(not f.acceptedUnits());
    }
    else if(f.path() == askcore_dataset::dataset_status)
    {
      QCOMPARE(f.datatype(), xsd::anyURI);
      QVERIFY(not f.acceptedUnits());
      QVERIFY(f.acceptedValues());
      QCOMPARE(f.acceptedValues()->size(), 5);
      QVERIFY(f.acceptedValues()->contains<knowCore::Uri>(askcore_dataset::in_progress));
      QVERIFY(f.acceptedValues()->contains<knowCore::Uri>(askcore_dataset::in_preparation));
      QVERIFY(f.acceptedValues()->contains<knowCore::Uri>(askcore_dataset::scheduled));
      QVERIFY(f.acceptedValues()->contains<knowCore::Uri>(askcore_dataset::completed));
      QVERIFY(f.acceptedValues()->contains<knowCore::Uri>(askcore_dataset::unknown));
      CRES_QCOMPARE(f.defaultValue().value<knowCore::Uri>(), askcore_dataset::unknown);
    }
    else if(f.path() == kcu_time::hasBeginning)
    {
      QCOMPARE(f.datatype(), xsd::dateTime);
      QVERIFY(not f.acceptedUnits());
    }
    else if(f.path() == kcu_time::hasEnd)
    {
      QCOMPARE(f.datatype(), xsd::dateTime);
      QVERIFY(not f.acceptedUnits());
    }
    else if(f.path() == askcore_sensing::point_density)
    {
      QCOMPARE(f.datatype(), askcore_datatype::quantityDecimal);
      QVERIFY(f.acceptedUnits());
      QVERIFY(f.acceptedUnits()->contains(askcore_unit::point_per_m2));
      QVERIFY(f.acceptedUnits()->contains(askcore_unit::point_per_millim2));
    }
    else if(f.path() == askcore_dataset::has_dataset)
    {
      QCOMPARE(f.datatype(), xsd::anyURI);
      QCOMPARE(f.direction(),
               kDB::Repository::RDF::FocusNodeDeclaration::Property::Direction::Reverse);
    }
    else
    {
      QFAIL(qPrintable(clog_qt::qformat("{} should not be there", f.path())));
    }
  }

  QCOMPARE(uniqueFields.size(), point_cloud_declaration_fields.size());
}

QTEST_MAIN(TestDeclaration)
