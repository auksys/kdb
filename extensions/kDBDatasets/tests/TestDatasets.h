
#include <QtTest/QtTest>

class TestDatasets : public QObject
{
  Q_OBJECT
private slots:
  void testInitialisation();
  void testCreationAccess();
  void testStatusOwner();
  /**
   * Test setProperty/property
   */
  void testProperties();
  void testConstrainedAccess();
};
