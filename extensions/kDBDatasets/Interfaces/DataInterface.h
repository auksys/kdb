#include <kDBDatasets/Forward.h>

namespace kDBDatasets::Interfaces
{
  class DataInterface
  {
  public:
    virtual ~DataInterface();
    virtual cres_qresult<ValueIterator*>
      createValueIterator(const kDB::Repository::Connection& _connection,
                          const Dataset& _dataset) const
      = 0;
    virtual cres_qresult<ExtractIterator*>
      createExtractIterator(const kDB::Repository::Connection& _connection,
                            const Dataset& _dataset) const
      = 0;
    virtual cres_qresult<InsertIterator*>
      createInsertIterator(const kDB::Repository::Connection& _connection,
                           const Dataset& _dataset) const
      = 0;
    virtual cres_qresult<Statistics> statistics(const kDB::Repository::Connection& _connection,
                                                const Dataset& _dataset) const
      = 0;
  };
} // namespace kDBDatasets::Interfaces
