#include "DataInterface.h"
#include "ExtractIterator.h"
#include "InsertIterator.h"
#include "ValueIterator.h"

using namespace kDBDatasets::Interfaces;

DataInterface::~DataInterface() {}

ExtractIterator::~ExtractIterator() {}

ValueIterator::~ValueIterator() {}

InsertIterator::~InsertIterator() {}
