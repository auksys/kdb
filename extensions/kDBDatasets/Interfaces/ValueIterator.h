#include <kDB/Forward.h>
#include <knowCore/Interfaces/Iterable.h>

namespace kDBDatasets::Interfaces
{
  /**
   * @ingroup kDBDatasets
   *
   * Definte an iterator over the values of a dataset.
   */
  class ValueIterator : public knowCore::Interfaces::Iterable<cres_qresult<knowCore::Value>>
  {
  public:
    virtual ~ValueIterator();
  };
} // namespace kDBDatasets::Interfaces
