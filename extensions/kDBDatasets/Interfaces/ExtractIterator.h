#include <kDB/Forward.h>
#include <knowCore/Interfaces/Iterable.h>

namespace kDBDatasets::Interfaces
{
  class ExtractIterator : public knowCore::Interfaces::Iterable<cres_qresult<QByteArray>>
  {
  public:
    virtual ~ExtractIterator();
  };
} // namespace kDBDatasets::Interfaces
