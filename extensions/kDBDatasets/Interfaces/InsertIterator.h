#include <kDB/Forward.h>
#include <knowCore/Interfaces/Iterable.h>

namespace kDBDatasets::Interfaces
{
  class InsertIterator
  {
  public:
    virtual ~InsertIterator();
    virtual cres_qresult<void> next(const QByteArray& _data) = 0;
    virtual cres_qresult<void> finalise() = 0;
  };
} // namespace kDBDatasets::Interfaces
