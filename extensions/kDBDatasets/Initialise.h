#include <QSharedPointer>

#include <kDB/Forward.h>

namespace kDBDatasets
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection);
}
