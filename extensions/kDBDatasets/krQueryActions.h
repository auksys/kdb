#include <kDB/Repository/krQuery/Interfaces/Action.h>

#include <kDB/Forward.h>

namespace kDBDatasets::krQueryActions
{
  /**
   * Define a krQL Query that reacts to "dataset" to allow adding, removing or updating salient
   * regions.
   *
   * For instance:
   * ```yaml
   * dataset:
   *   action: set status
   *   target: some_pois
   *   status: http://askco.re/dataset#completed
   * ```
   */
  class Dataset : public kDB::Repository::krQuery::Interfaces::Action
  {
  public:
    Dataset();
    virtual ~Dataset();
    cres_qresult<knowCore::Value> execute(const kDB::Repository::krQuery::Context& _context,
                                          const QString& _key,
                                          const knowCore::ValueHash& _parameters) override;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBDatasets::krQueryActions
