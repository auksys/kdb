#include "Collection.h"

#include <QCborMap>

#include <knowCore/Uris/askcore_dataset.h>
#include <knowCore/Uris/askcore_graph.h>

#include <knowRDF/Literal.h>

#include <knowGIS/GeometryObject.h>
#include <knowGIS/Uris/geo.h>

#include <kDB/Repository/Connection.h>

#include <kDB/Repository/RDF/FocusNode.h>
#include <kDB/Repository/RDF/FocusNodeDeclarationsRegistry.h>

#include "Dataset.h"

KDB_REPOSITORY_REGISTER_FOCUS_NODE_DECLARATIONS("qrc:///kdbdatasets/data/datasets_shacl.ttl");

using namespace kDBDatasets;

using askcore_dataset = knowCore::Uris::askcore_dataset;
using askcore_graph = knowCore::Uris::askcore_graph;
using geo = knowGIS::Uris::geo;
using FocusNode = kDB::Repository::RDF::FocusNode;
using FocusNodeCollection = kDB::Repository::RDF::FocusNodeCollection;

knowCore::Uri Collection::collectionType() { return askcore_dataset::collection; }

knowCore::Uri Collection::allFocusNodesView() { return askcore_graph::all_datasets; }

knowCore::Uri Collection::primaryType() { return askcore_dataset::dataset; }

knowCore::UriList Collection::containedTypes() { return {knowCore::Uri(askcore_dataset::dataset)}; }

cres_qresult<knowCore::UriList> Collection::defaultDatatypes(
  const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints)
{

  QList<knowCore::Uri> datatypes;
  for(int i = 0; i < _constraints.size(); ++i)
  {
    knowCore::Uri path = _constraints[i].first;
    if(path == askcore_dataset::content_type)
    {
      for(knowCore::ConstrainedValue::Constraint constraint : _constraints[i].second.constraints())
      {
        if(constraint.type == knowCore::ConstrainedValue::Type::Equal)
        {
          cres_try(knowCore::Uri datatype_uri, constraint.value.value<knowCore::Uri>());
          cres_try(kDB::Repository::RDF::FocusNodeDeclaration declaration,
                   kDB::Repository::RDF::FocusNodeDeclarationsRegistry::byConstantField(
                     knowCore::Uris::askcore_dataset::dataset,
                     knowCore::Uris::askcore_dataset::content_type,
                     knowRDF::Literal::fromValue(datatype_uri)));
          datatypes.append(declaration.type());
        }
        else
        {
          return cres_failure("Unsupported constraint for datatype: {}", constraint.type);
        }
      }
    }
    else if(path == askcore_dataset::dataset_type)
    {
      for(knowCore::ConstrainedValue::Constraint constraint : _constraints[i].second.constraints())
      {
        if(constraint.type == knowCore::ConstrainedValue::Type::Equal)
        {
          cres_try(knowCore::Uri datatype_uri, constraint.value.value<knowCore::Uri>());
          cres_try(kDB::Repository::RDF::FocusNodeDeclaration declaration,
                   kDB::Repository::RDF::FocusNodeDeclarationsRegistry::declaration(datatype_uri));
          datatypes.append(declaration.type());
        }
        else
        {
          return cres_failure("Unsupported constraint for datatype: {}", constraint.type);
        }
      }
    }
  }
  if(datatypes.isEmpty())
  {
    datatypes.append(knowCore::Uris::askcore_dataset::dataset);
  }
  return cres_success(datatypes);
}

Collection::Collection() {}

Collection::Collection(const kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>& _rhs)
    : kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>(_rhs)
{
}

Collection::Collection(const Collection& _rhs)
    : kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>(_rhs)
{
}

Collection& Collection::operator=(const Collection& _rhs)
{
  kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>::operator=(_rhs);
  return *this;
}

Collection::~Collection() {}

Collection Collection::allDatasets(const kDB::Repository::Connection& _connection)
{
  return get(_connection, allFocusNodesView()).expect_success();
}

cres_qresult<Dataset> Collection::dataset(const knowCore::Uri& _datasetUri) const
{
  return focusNode(_datasetUri);
}

cres_qresult<bool> Collection::hasDataset(const knowCore::Uri& _agentUri) const
{
  return hasFocusNode(_agentUri);
}

cres_qresult<QList<Dataset>>
  Collection::datasets(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
                       const OperatorOptions& _operatorOptions) const
{
  return focusNodes(_constraints, _operatorOptions);
}

cres_qresult<Dataset> Collection::createDataset(const knowCore::Uri& _frameTypeUri,
                                                const knowGIS::GeometryObject& _geometry,
                                                const knowCore::ValueHash& _properties,
                                                const knowCore::Uri& _datasetUri)
{
  cres_try(kDB::Repository::RDF::FocusNodeDeclaration declaration,
           kDB::Repository::RDF::FocusNodeDeclarationsRegistry::byConstantField(
             askcore_dataset::dataset, askcore_dataset::content_type,
             knowRDF::Literal::fromValue(_frameTypeUri)));

  return createFocusNode(declaration.type(),
                         knowCore::ValueHash(_properties)
                           .insert(askcore_dataset::content_type, _frameTypeUri)
                           .insert(geo::hasGeometry, knowCore::Value::fromValue(_geometry)),
                         _datasetUri);
}

cres_qresult<Dataset> Collection::insertDatasetFromCbor(const QCborMap& _map)
{
  cres_try(FocusNode fn, FocusNode::fromCborMap(focusNodeCollection().connection(),
                                                focusNodeCollection().uri(), _map));
  return Dataset::fromFocusNode(fn);
}

#include "moc_Collection.cpp"
