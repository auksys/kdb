#pragma once

#include <knowCore/ConstrainedValue.h>
#include <knowCore/UriList.h>

#include <kDB/Repository/RDF/FocusNodeCollection.h>

#include <kDBDatasets/Forward.h>

class TestDatasets;

template<>
struct kDB::Repository::RDF::FocusNodeCollectionTrait<kDBDatasets::Collection>
{
  using ValueType = kDBDatasets::Dataset;
};

namespace kDBDatasets
{
  /**
   * Interface a @ref kDB::Repository::TripleStore that contains a collection of datasets
   */
  class Collection : public kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>
  {
    friend class ::TestDatasets;
  public:
    using ValueType = Dataset;
  protected:
    Collection(const kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>& _rhs);
  public:
    Collection();
    Collection(const Collection& _rhs);
    Collection& operator=(const Collection& _rhs);
    ~Collection();
  public:
    static knowCore::Uri collectionType();
    static knowCore::Uri allFocusNodesView();
    static knowCore::Uri primaryType();
    static knowCore::UriList containedTypes();
    static cres_qresult<knowCore::UriList>
      defaultDatatypes(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints);
  public:
    /**
     * \return an interface to the collection that list all datasets.
     */
    static Collection allDatasets(const kDB::Repository::Connection& _connection);
  public:
    /**
     * \return the dataset with the Uri \ref _datasetUri
     */
    cres_qresult<Dataset> dataset(const knowCore::Uri& _datasetUri) const;
    cres_qresult<bool> hasDataset(const knowCore::Uri& _agentUri) const;
    using OperatorOptions = kDB::Repository::RDF::FocusNodeCollection::OperatorOptions;
    /**
     * @param _constraints a list of pair of list of uris representing the property uri and a
     * constraint.
     * @param _operatorOptions set the precision used by operators
     *
     * \return the list of datasets that satisfies the constraints
     */
    cres_qresult<QList<Dataset>>
      datasets(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
               const OperatorOptions& _operatorOptions = OperatorOptions()) const;
    template<typename... _TArgs_>
    cres_qresult<QList<Dataset>> datasets(const knowCore::Uri& _uri,
                                          const knowCore::ConstrainedValue& _constraint,
                                          const _TArgs_&... _args) const
    {
      return focusNodes(_uri, _constraint, _args...);
    }
    /**
     * Create a new \ref Dataset of uri type \p _typeUri with geometry \p _geometry and add it to
     * the \p _rdfGraph.
     */
    cres_qresult<Dataset>
      createDataset(const knowCore::Uri& _frameTypeUri, const knowGIS::GeometryObject& _geometry,
                    const knowCore::ValueHash& _properties,
                    const knowCore::Uri& _datasetUri = knowCore::Uri::createUnique({"dataset"}));
    /**
     */
    cres_qresult<Dataset> insertDatasetFromCbor(const QCborMap& _map);
  };

} // namespace kDBDatasets
