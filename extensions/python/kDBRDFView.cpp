#include <knowCorePython.h>

#include <QIODevice>

#include <knowCore/Messages.h>
#include <knowRDF/Triple.h>

#include <kDB/RDFView/ViewDefinition.h>

namespace py = pybind11;

PYBIND11_MODULE(RDFView, m)
{
  m.doc() = R"pbdoc(
    knowCore
    -----------

    .. currentmodule:: kDB.RDFView

    .. autosummary::
      :toctree: _generate
  )pbdoc";
  py::class_<kDB::RDFView::ViewDefinition>(m, "ViewDefinition")
    .def_static("parse",
                pybind11::overload_cast<QIODevice*, knowCore::Messages*, const knowCore::ValueHash&,
                                        const QString&>(&kDB::RDFView::ViewDefinition::parse),
                py::arg("device"), py::arg("messages") = nullptr,
                py::arg("bindings") = knowCore::ValueHash(), py::arg("format") = QString("SML"))
    .def_static(
      "parse",
      pybind11::overload_cast<const QString&, knowCore::Messages*, const knowCore::ValueHash&,
                              const QString&>(&kDB::RDFView::ViewDefinition::parse),
      py::arg("device"), py::arg("messages") = nullptr, py::arg("bindings") = knowCore::ValueHash(),
      py::arg("format") = QString("SML"))
    .def("isValid", &kDB::RDFView::ViewDefinition::isValid)
    .def("isValid", &kDB::RDFView::ViewDefinition::name)
    .def("isValid", &kDB::RDFView::ViewDefinition::triples);
}
