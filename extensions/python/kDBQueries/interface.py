import knowCore
import knowRDF

class interface:
  def __init__(self, connection):
    self.connection = connection

  def execute_sparql_query(self, query, bindings, term_converter = lambda uri, value, lang: (uri, value, lang), graphnames = []):
    from kDB.Repository import SPARQLQuery

    if len(graphnames) == 0:
      q = SPARQLQuery(self.connection, query)
    elif len(graphnames) == 1:
      q = SPARQLQuery(self.connection.graphsManager().getTripleStore(graphnames[0]), query)
    else:
      raise Exception("More than one graph is not currently supported")
    for k, v in bindings.items():
      if isinstance(v, dict) and len(v) == 3 and 'type' in v and 'value' in v and 'lang' in v:
        q.bindValue(k, v['type'], v['value'], v['lang'])
      else:
        q.bindValue(k, v)
    r = q.execute()
    if len(r.error()) == 0:
      result = []
      for i in range(0, r.tuples()):
        row = {}
        for j in range(0, r.fields()):
          v = r.value(i, j)
          
          if type(v) is kDB.RDF.Literal:
            v = term_converter(v.datatype(), v.value(), v.lang())
          elif type(v) is kDB.Core.Uri:
            v = v.toString()
          elif type(v) is kDB.RDF.BlankNode:
            v = "BlankNode"
          
          row[r.fieldName(j)] = v
        result.append(row)
      return (True, result)
    else:
      return (False, r.error())
