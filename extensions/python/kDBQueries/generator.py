from .converters import convert_term, convert_binding
from .queries import load_graphs, load_arguments, load_generators

class query_executor:
  def __init__(self, interface, graphname, uri, name, description, query):
    self.interface = interface
    self.graphname = graphname
    self.uri = uri
    self.name = name
    self.query = query
    self.arguments = None
    self.description = description
    self.graphnames = None

  def __call__(self, **kwargs):
    self.__load_graphnames__()
    self.__load_arguments__()
    
    if len(self.arguments) != len(kwargs):
      raise Exception("Invalid number of arguments in '{}' expected {} but got {}".format(self.name, len(self.arguments), len(kwargs)))
    
    bindings = {}
    
    for k, v in kwargs.items():
      if not k in self.arguments:
        raise Exception("Unknown argument '{}' in '{}'".format(k, self.name))
      bindings["%" + k] = convert_binding(self.arguments[k][0], v)
    
    def fix_name(n):
      if n[0] == "%":
        name = bindings[n]
        if name['type'] == 'http://www.w3.org/2001/XMLSchema#anyURI':
          return name['value']
        else:
          raise Exception("Graph names should be a URI not a '{}'").format(name['type'])
      else:
        return n
    graphnames = list(map(fix_name, self.graphnames))
    
    (success, r) = self.interface.execute_sparql_query(self.query, bindings, term_converter = convert_term, graphnames = graphnames)

    if success:
      return r
    else:
      raise Exception("Invalid query in '{}' with error '{}'".format(self.name, r))

  def help(self):
    print("{}: {}".format(self.name, self.description))
    self.__load_arguments__()
    
    if len(self.arguments) == 0:
      print("  No arguments.")
    else:
      print("  With arguments:")
      for k,v in self.arguments.items():
        print("  - '{}': {} ({})".format(k, v[1], v[0]))
    
  def __load_graphnames__(self):
    if self.graphnames == None:
      def fix_name(n):
        if type(n) is tuple:
          return n[1]
        else:
          return n
      self.graphnames = load_graphs(self.interface, self.graphname, self.uri)
      self.graphnames = list(map(fix_name, self.graphnames))
      
  def __load_arguments__(self):
    if self.arguments == None:
      self.arguments = load_arguments(self.interface, self.graphname, self.uri)

class generator:
  def __init__(self, interface, graphname):
    self.interface = interface
    
    load_generators(self, query_executor, interface, graphname)

  def help(self, executor_name = None):
    if executor_name:
      if executor_name in self.query_executors:
        self.query_executors[executor_name].help()
      else:
        print("Unknown query '{}'", executor_name)
    else:
      print("List of available executors:")
      for k,v in self.query_executors.items():
        print("{}: {}".format(k, v.description))
