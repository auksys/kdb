term_converters = {
  'http://www.w3.org/2001/XMLSchema#double': lambda uri, value, lang: float(value),
  'http://www.w3.org/2001/XMLSchema#decimal': lambda uri, value, lang: float(value)
  }

def convert_term(uri, value, lang):
  if uri in term_converters:
    return term_converters[uri](uri, value, lang)
  else:
    return (uri, value, lang)

def make_literal(uri, value, lang = None):
  return { 'type': uri, 'value': value, 'lang': lang }

def value_to_point(value):
  if isinstance(value, list): # Assume list of coordinate
    return value
  elif hasattr(value, 'longitude') and hasattr(value, 'latitude'):
    ret = [value.longitude, value.latitude]
    if hasattr(value, 'altitude'):
      ret.append(value.altitude)
    return ret
  else:
    raise ValueError("Unsupported value {}".format(value))

def value_to_wkt(uri, value):
  if isinstance(value, str):
    if value[0] == '{': # most likely a JSON string
      return make_literal(uri, kDBGIS.GeometryObject.fromJson(value).toWKT())
    else:
      # Assume WKT string
      return make_literal(uri, value)
  elif isinstance(value, list):
    import kDBGIS
    # Attempt to generate a WKT string
    coordinates = []
    for poly in value:
      poly_arr = []
      if len(poly) != 0:
        for pt in poly:
          poly_arr.append(value_to_point(pt))
        if(poly_arr[0] != poly_arr[-1]):
          poly_arr.append(poly_arr[0])
      coordinates.append(poly_arr)
    go = kDBGIS.GeometryObject.fromVariant({ 'type': 'Polygon', 'coordinates': coordinates})
    if go.isValid():
      return make_literal(uri, go.toWKT())
    else:
      raise ValueError("Unsupported value {}".format(value))
  elif hasattr(value, 'type') and hasattr(value, 'coordinates'):
    return make_literal(uri, kDBGIS.GeometryObject.fromVariant(value).toWKT())
  elif isinstance(value, kDBGIS.GeometryObject):
    return make_literal(uri, value.toWKT())
  else:
    raise ValueError("Unsupported value {}".format(value))

binding_converters = {
  'http://www.w3.org/2001/XMLSchema#double': lambda uri, value: value,
  'http://www.w3.org/2001/XMLSchema#decimal': lambda uri, value: value,
  'http://www.w3.org/2001/XMLSchema#integer': lambda uri, value: value,
  'http://www.w3.org/2001/XMLSchema#string': lambda uri, value: value,
  'http://www.w3.org/2001/XMLSchema#anyURI': lambda uri, value: make_literal('http://www.w3.org/2001/XMLSchema#anyURI', value ),
  'http://www.opengis.net/ont/geosparql#Geometry': value_to_wkt
}

def convert_binding(uri, value):
  if uri in binding_converters:
    return binding_converters[uri](uri, value)
  else:
    return make_literal(uri, value)
