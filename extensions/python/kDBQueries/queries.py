def load_graphs(interface, graphname, uri):
  (success, result) = interface.execute_sparql_query("""
PREFIX queries: <http://askco.re/db/queries#>

SELECT ?graph WHERE {
  %uri queries:graph ?graph .
}
""", { '%uri': uri }, graphnames = [graphname])
  if success:
    graphs = []
    for row in result:
      graphs.append(row["graph"])
    return graphs
  else:
    print("Error when getting graphs: {}".format(result))
    return None

def load_arguments(interface, graphname, uri):
  (success, result) = interface.execute_sparql_query("""
PREFIX queries: <http://askco.re/db/queries#>
PREFIX xsi: <http://www.w3.org/2001/XMLSchema-instance#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>

SELECT ?name ?type ?description WHERE {
  %uri queries:binding _:binding .
   _:binding foaf:name ?name ;
            xsi:type ?type .
  OPTIONAL { _:binding dc:description ?description }
}
""", { '%uri': uri }, graphnames = [graphname])
  if(success):
    arguments = {}
    for row in result:
      description = row["description"]
      if description == None:
        description = ""
      else:
        description = description[1]
      arguments[row["name"][1]] = (row["type"], description)
    return arguments
  else:
    print("Error when getting arguments information: {}".format(result))
    return None
  
  
def load_generators(self, generator_class, interface, graphname):  
  (success, result) = self.interface.execute_sparql_query("""PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX queries: <http://askco.re/db/queries#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>

SELECT ?uri ?name ?query ?description WHERE {
  ?uri foaf:name ?name ;
        queries:query ?query .
  OPTIONAL { ?uri dc:description ?description }
}""", {}, graphnames = [graphname])
  
  self.query_executors = {}
  if(success):
    for row in result:
      uri  = row["uri"]
      name = row["name"][1]
      query = row["query"][1]
      description = row["description"][1]
      
      qe = generator_class(self.interface, graphname, uri, name, description, query)
      self.query_executors[name] = qe
      setattr(self, name, qe)
  else:
    print("Error when initialising generator: {}".format(result))
