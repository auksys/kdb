#include <knowCorePython.h>

#include <knowCore/Timestamp.h>

#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/Connection.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>
#include <kDBDatasets/Statistics.h>

namespace py = pybind11;

PYBIND11_MODULE(py_kDBDatasets, m)
{
  m.doc() = R"pbdoc(
    kDBDatasets
    -----------

    .. currentmodule:: kDBDatasets

    .. autosummary::
      :toctree: _generate
  )pbdoc";
  py::class_<kDBDatasets::Dataset> dataset(m, "Dataset");
  dataset.def("exists", &kDBDatasets::Dataset::exists)
    .def("uri", &kDBDatasets::Dataset::uri)
    .def("type", &kDBDatasets::Dataset::type)
    .def("startTime", &kDBDatasets::Dataset::startTime)
    .def("endTime", &kDBDatasets::Dataset::endTime)
    .def("createdFrom", &kDBDatasets::Dataset::createdFrom)
    .def("hasProperty", &kDBDatasets::Dataset::hasProperty)
    .def("property",
         pybind11_qt::overload_r_c<cres_qresult<knowCore::Value>, kDBDatasets::Dataset,
                                   const knowCore::Uri&>(&kDBDatasets::Dataset::property))
    .def("setProperty",
         pybind11_qt::overload_r_c<cres_qresult<void>, kDBDatasets::Dataset, const knowCore::Uri&,
                                   const knowCore::Value&>(&kDBDatasets::Dataset::setProperty))
    .def("geometry", &kDBDatasets::Dataset::geometry)
    .def("connection", &kDBDatasets::Dataset::connection)
    .def("associate", &kDBDatasets::Dataset::associate)
    .def("dissociate", &kDBDatasets::Dataset::dissociate)
    .def("associatedAgents", &kDBDatasets::Dataset::associatedAgents)
    .def("status", &kDBDatasets::Dataset::status)
    .def("statusUri", &kDBDatasets::Dataset::statusUri)
    .def("setStatus",
         pybind11_qt::overload<kDBDatasets::Dataset::Status>(&kDBDatasets::Dataset::setStatus))
    .def("setStatus",
         pybind11_qt::overload<const knowCore::Uri&>(&kDBDatasets::Dataset::setStatus));
  py::enum_<kDBDatasets::Dataset::Status>(dataset, "Status")
    .value("Completed", kDBDatasets::Dataset::Status::Completed)
    .value("InProgress", kDBDatasets::Dataset::Status::InProgress)
    .value("InPreparation", kDBDatasets::Dataset::Status::InPreparation)
    .value("Scheduled", kDBDatasets::Dataset::Status::Scheduled)
    .value("Unknown", kDBDatasets::Dataset::Status::Unknown);
  py::class_<kDBDatasets::Collection> datasets(m, "Collection");
  datasets.def_static("get", &kDBDatasets::Collection::get)
    .def_static("create",
                pybind11::overload_cast<const kDB::Repository::Connection&, const knowCore::Uri&>(
                  &kDBDatasets::Collection::create))
    .def_static("getOrCreate", &kDBDatasets::Collection::getOrCreate)
    .def_static("allDatasets", &kDBDatasets::Collection::allDatasets)
    .def("isValid", &kDBDatasets::Collection::isValid)
    .def("isReadOnly", &kDBDatasets::Collection::isReadOnly)
    .def("uri", &kDBDatasets::Collection::uri)
    .def("connection", &kDBDatasets::Collection::connection)
    .def("count", &kDBDatasets::Collection::count)
    .def("dataset", &kDBDatasets::Collection::dataset)
    .def("all", &kDBDatasets::Collection::all)
    .def(
      "datasets",
      pybind11_qt::overload_r_c<cres_qresult<QList<kDBDatasets::Dataset>>, kDBDatasets::Collection,
                                const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>&,
                                const kDBDatasets::Collection::OperatorOptions&>(
        &kDBDatasets::Collection::datasets))
    .def("createDataset", &kDBDatasets::Collection::createDataset, py::arg("typeUri"),
         py::arg("geometry"), py::arg("properties"), py::arg("datasetUri"))
    .def("createDataset",
         [](kDBDatasets::Collection* _self, const knowCore::Uri& _typeUri,
            const knowGIS::GeometryObject& _geometry, const knowCore::ValueHash& _properties)
         { return _self->createDataset(_typeUri, _geometry, _properties); });
  py::class_<kDBDatasets::Collection::OperatorOptions>(datasets, "OperatorOptions")
    .def_readwrite("intersectsPrecision",
                   &kDBDatasets::Collection::OperatorOptions::intersectsPrecision);

  py::class_<kDBDatasets::ValueIterator>(m, "ValueIterator")
    .def("hasNext", &kDBDatasets::ValueIterator::hasNext)
    .def("next", &kDBDatasets::ValueIterator::next);
  py::class_<kDBDatasets::Statistics>(m, "Statistics")
    .def("datapointsCount", &kDBDatasets::Statistics::datapointsCount);

  m.def_submodule("DataInterfaceRegistry")
    .def("createValueIterator", &kDBDatasets::DataInterfaceRegistry::createValueIterator)
    .def("statistics", &kDBDatasets::DataInterfaceRegistry::statistics);
}
