#!/usr/bin/env python3

import unittest

import knowCore
import knowRDF
import kDB.Repository

from knowCore import Uri

class TestkDBRepository(unittest.TestCase):
    '''
    Unit tests for the kDB.Repository module
    '''
    def test_query(self):
        '''
        Test Query
        '''
        ts = kDB.Repository.TemporaryStore()

        ts.start()
        s = ts.store()
        c = s.createConnection()
        self.assertFalse(c.isConnected())
        c.connect()
        self.assertTrue(c.isConnected())
        q = c.createSQLQuery("SELECT 2;")
        r = q.execute()
        self.assertEqual(r.fields(), 1)
        self.assertEqual(r.tuples(), 1)
        self.assertEqual(r.value(0, 0), 2)
        q = c.createSPARQLQuery()
        q.setQuery("INSERT DATA { GRAPH <a> { <b> <c> 1 } }")
        r = q.execute()
        q.setQuery("SELECT ?z FROM <a> WHERE { <b> <c> ?z }")
        r = q.execute()
        self.assertEqual(r.fields(), 1)
        self.assertEqual(r.tuples(), 1)
        self.assertEqual(r.value(0,0).value(), 1)
        s.stop()
    def test_triples_store(self):
        '''
        Test the triples store class
        '''
        ts = kDB.Repository.TemporaryStore()

        ts.start()
        s = ts.store()
        c = s.createConnection()
        c.connect()
        self.assertTrue(c.isConnected())
        uri_mystore = knowCore.Uri("mystore")
        self.assertFalse(c.graphsManager().hasTripleStore(uri_mystore))
        store = c.graphsManager().createTripleStore(uri_mystore)
        self.assertTrue(c.graphsManager().hasTripleStore(uri_mystore))
        store.insert(Uri("a"), Uri("b"), Uri("c"))
        store.insert(Uri("a"), Uri("d"), knowRDF.Literal.fromRdfLiteral(Uri("http://www.w3.org/2001/XMLSchema#string"), "e"))
        store.insert(Uri("a"), Uri("f"), 1)
        self.assertEqual(len(store.triples()), 3)
    def test_kr_query(self):
        '''
        Test the krQL Engine
        '''
        ts = kDB.Repository.TemporaryStore()

        ts.start()
        s = ts.store()
        c = s.createConnection()
        c.connect()
        self.assertEqual(c.krQueryEngine().execute("TEST: { return: 42 }"), 42)

if __name__ == '__main__':
    unittest.main()
