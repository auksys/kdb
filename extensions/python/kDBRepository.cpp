#include <knowCorePython.h>

#include <QDir>

#include <knowRDF/Literal.h>
#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/DatasetsUnion.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/RDFDataset.h>
#include <kDB/Repository/Store.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/Transaction.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/TripleStreamInserter.h>
#include <kDB/Repository/krQuery/Engine.h>

namespace py = pybind11;

namespace
{
  template<typename _TC_>
  void insert_triples(kDB::Repository::TripleStore _store,
                      const kDB::Repository::Transaction& _transaction,
                      const knowRDF::Subject& _subject, int _index, _TC_ _args)
  {
    if((_args.size() - _index) % 2 != 0)
    {
      throw std::runtime_error("Invalid number of arguments, expected an even number in "
                               + std::string(py::repr(_args)));
    }
    for(std::size_t i = 0; i < (_args.size() - _index) / 2; ++i)
    {
      knowCore::Uri predicate = py::cast<knowCore::Uri>(_args[2 * i + _index]);
      knowRDF::Object object;
      py::handle h_object = _args[2 * i + 1 + _index];

      if(pybind11::isinstance<py::tuple>(h_object))
      {
        knowRDF::BlankNode bn;
        knowCore::pybind11::handle_result(_store.insert(_subject, predicate, bn, _transaction));
        insert_triples(_store, _transaction, bn, 0, py::cast<py::tuple>(h_object));
      }
      else
      {
        if(pybind11::isinstance<knowCore::Uri>(h_object))
          object = pybind11::cast<knowCore::Uri>(h_object);
        else if(pybind11::isinstance<knowRDF::BlankNode>(h_object))
          object = pybind11::cast<knowRDF::BlankNode>(h_object);
        else if(pybind11::isinstance<knowRDF::Object>(h_object))
          object = pybind11::cast<knowRDF::Object>(h_object);
        else
          object = knowRDF::Literal::fromValue(pybind11::cast<knowCore::Value>(h_object));
        knowCore::pybind11::handle_result(_store.insert(_subject, predicate, object, _transaction));
      }
    }
  }
  void insert_triples(kDB::Repository::TripleStore _store,
                      const kDB::Repository::Transaction& _transaction, const py::args& _args)
  {
    if(_args.size() % 2 != 1)
    {
      throw std::runtime_error("Invalid number of arguments, expected an even number");
    }
    knowRDF::Subject subject;

    if(pybind11::isinstance<knowCore::Uri>(_args[0]))
      subject = pybind11::cast<knowCore::Uri>(_args[0]);
    else if(pybind11::isinstance<knowRDF::BlankNode>(_args[0]))
      subject = pybind11::cast<knowRDF::BlankNode>(_args[0]);
    else if(pybind11::isinstance<knowRDF::Subject>(_args[0]))
      subject = pybind11::cast<knowRDF::Subject>(_args[0]);
    else
      throw std::runtime_error("Cannot cast " + std::string(py::repr(_args[0]))
                               + " to knowCore::Subject.");
    insert_triples(_store, _transaction, subject, 1, _args);
  }
} // namespace

PYBIND11_MODULE(py_kDBRepository, m)
{
  m.doc() = R"pbdoc(
    kDB.Repository
    --------------

    .. currentmodule:: kDB.Repository

    .. autosummary::
      :toctree: _generate
  )pbdoc";

  py::class_<kDB::Repository::RDFDataset> rdf_dataset(m, "RDFDataset");
  py::class_<kDB::Repository::Transaction> transaction(m, "Transaction");

  py::class_<kDB::Repository::DatasetsUnion, kDB::Repository::RDFDataset>(m, "DatasetsUnion")
    .def(py::init<>())
    .def("add", pybind11_qt::overload_r_c<void, kDB::Repository::DatasetsUnion,
                                          const kDB::Repository::RDFDataset&>(
                  &kDB::Repository::DatasetsUnion::add));

  py::class_<kDB::Repository::RDFEnvironment>(m, "RDFEnvironment")
    .def(py::init<const kDB::Repository::RDFDataset&>(),
         py::arg("dataset") = kDB::Repository::RDFDataset())
    .def("base", &kDB::Repository::RDFEnvironment::base)
    .def("defaultDataset", &kDB::Repository::RDFEnvironment::defaultDataset)
    .def("namedDatasets", &kDB::Repository::RDFEnvironment::namedDatasets)
    .def("isValid", &kDB::Repository::RDFEnvironment::isValid)
    .def("setBase", &kDB::Repository::RDFEnvironment::setBase)
    .def("addNamedDataset", &kDB::Repository::RDFEnvironment::addNamedDataset)
    .def("setDefaultDataset", &kDB::Repository::RDFEnvironment::setDefaultDataset);

  py::class_<kDB::Repository::krQuery::Engine>(m, "Engine")
    .def("execute",
         pybind11_qt::overload<const QString&>(&kDB::Repository::krQuery::Engine::execute));

  py::class_<kDB::Repository::Connection>(m, "Connection")
    .def_static(
      "createFromHostname",
      [](const QString& _hostname, int _port, const QString& _database)
      {
        return kDB::Repository::Connection::create(kDB::Repository::HostName(_hostname), _port,
                                                   _database);
      },
      py::arg("host"), py::arg("port"), py::arg("database") = "kDB"_kCs)
    .def_static(
      "createFromName",
      [](const QString& _section, const QString& _name, int _port, const QString& _database)
      {
        return kDB::Repository::Connection::create(kDB::Repository::SectionName(_section),
                                                   kDB::Repository::StoreName(_name), _port,
                                                   _database);
      },
      py::arg("section"), py::arg("name"), py::arg("port"), py::arg("database") = "kDB"_kCs)
    .def("isConnected", &kDB::Repository::Connection::isConnected)
    .def("connect", &kDB::Repository::Connection::connect, py::arg("initialise_database") = false)
    .def("disconnect", &kDB::Repository::Connection::disconnect)
    .def("serverUri", &kDB::Repository::Connection::serverUri)
    .def("serverUuid", &kDB::Repository::Connection::serverUuid)
    .def("enableExtension", &kDB::Repository::Connection::enableExtension)
    .def("isExtensionEnabled", &kDB::Repository::Connection::isExtensionEnabled)
    .def("graphsManager", &kDB::Repository::Connection::graphsManager)
    .def("krQueryEngine", &kDB::Repository::Connection::krQueryEngine,
         py::return_value_policy::reference)
    .def(
      "createSQLQuery", [](const kDB::Repository::Connection& _self, const QString& _query)
      { return _self.createSQLQuery(_query); }, py::arg("query") = QString())
    .def("createSQLQuery", &kDB::Repository::Connection::createSQLQuery,
         py::arg("query") = QString(), py::arg("bindings") = knowCore::ValueHash(),
         py::arg("options") = knowCore::ValueHash())
    .def("createSPARQLQuery", &kDB::Repository::Connection::createSPARQLQuery,
         py::arg("environment") = kDB::Repository::RDFEnvironment(), py::arg("query") = QString(),
         py::arg("bindings") = knowCore::ValueHash(), py::arg("options") = knowCore::ValueHash());
  py::class_<kDB::Repository::GraphsManager,
             std::unique_ptr<kDB::Repository::GraphsManager, py::nodelete>>(m, "GraphsManager")
    .def("createTripleStore", &kDB::Repository::GraphsManager::createTripleStore, py::arg("name"),
         py::arg("transaction") = kDB::Repository::Transaction())
    .def("getTripleStore", &kDB::Repository::GraphsManager::getTripleStore)
    .def("getOrCreateTripleStore", &kDB::Repository::GraphsManager::getOrCreateTripleStore)
    .def("removeTripleStore", &kDB::Repository::GraphsManager::removeTripleStore, py::arg("name"),
         py::arg("transaction") = kDB::Repository::Transaction())
    .def("hasTripleStore", &kDB::Repository::GraphsManager::hasTripleStore)
    .def("tripleStores", &kDB::Repository::GraphsManager::tripleStores);
  rdf_dataset.def("uri", &kDB::Repository::RDFDataset::uri)
    .def("type", &kDB::Repository::RDFDataset::type)
    .def("isValid", &kDB::Repository::RDFDataset::isValid)
    .def("toTripleStore", &kDB::Repository::RDFDataset::toTripleStore)
    .def("connection", &kDB::Repository::RDFDataset::connection)
    .def("createSPARQLQuery", &kDB::Repository::RDFDataset::createSPARQLQuery,
         py::arg("environment") = kDB::Repository::RDFEnvironment(), py::arg("query") = QString(),
         py::arg("bindings") = knowCore::ValueHash(), py::arg("options") = knowCore::ValueHash());

  py::class_<kDB::Repository::Store>(m, "Store")
    .def(py::init<>([](const QString& _storage, int _port)
                    { return new kDB::Repository::Store(_storage, _port); }),
         py::arg("storage"), py::arg("port") = 1242)
    .def("startIfNeeded", &kDB::Repository::Store::startIfNeeded)
    .def("start", &kDB::Repository::Store::start)
    .def("stop", &kDB::Repository::Store::stop, py::arg("force") = false)
    .def("erase", &kDB::Repository::Store::erase)
    .def("createConnection", &kDB::Repository::Store::createConnection)
    .def("isRunning", &kDB::Repository::Store::isRunning)
    .def("autoSelectPort", &kDB::Repository::Store::autoSelectPort);

  py::class_<kDB::Repository::TemporaryStore>(m, "TemporaryStore")
    .def(py::init())
    .def("start", &kDB::Repository::TemporaryStore::start)
    .def("stop", &kDB::Repository::TemporaryStore::stop, py::arg("force") = false)
    .def("createConnection", &kDB::Repository::TemporaryStore::createConnection)
    .def("store", &kDB::Repository::TemporaryStore::store, py::return_value_policy::reference);

  transaction
    .def(py::init<const kDB::Repository::Connection&>(),
         py::arg("connection") = kDB::Repository::Connection())
    .def("isActive", &kDB::Repository::Transaction::isActive)
    .def("commit", &kDB::Repository::Transaction::commit)
    .def("rollback", &kDB::Repository::Transaction::rollback);
  py::class_<kDB::Repository::TripleStore, kDB::Repository::RDFDataset>(m, "TripleStore")
    .def("triples", &kDB::Repository::TripleStore::triples)
    // Insert
    .def("insert",
         [](const kDB::Repository::TripleStore& _store, py::args _args)
         {
           kDB::Repository::Transaction t(_store.connection());
           try
           {
             insert_triples(_store, t, _args);
             t.commit();
           }
           catch(const std::runtime_error& ex)
           {
             t.rollback();
             throw ex;
           }
         })
    .def("insert", [](const kDB::Repository::TripleStore& _store,
                      const kDB::Repository::Transaction& _transaction, py::args _args)
         { insert_triples(_store, _transaction, _args); })
    // Remove overloads
    .def("remove",
         pybind11::overload_cast<const knowRDF::Subject&, const knowCore::Uri&,
                                 const knowCore::Uri&, const kDB::Repository::Transaction&>(
           &kDB::Repository::TripleStore::remove),
         py::arg("subject"), py::arg("predicate"), py::arg("object"),
         py::arg("transaction") = kDB::Repository::Transaction())
    .def("remove",
         pybind11::overload_cast<const knowRDF::Subject&, const knowCore::Uri&,
                                 const knowRDF::Object&, const kDB::Repository::Transaction&>(
           &kDB::Repository::TripleStore::remove),
         py::arg("subject"), py::arg("predicate"), py::arg("object"),
         py::arg("transaction") = kDB::Repository::Transaction())
    .def("remove",
         pybind11::overload_cast<const knowRDF::Subject&, const knowCore::Uri&,
                                 const knowRDF::BlankNode&, const kDB::Repository::Transaction&>(
           &kDB::Repository::TripleStore::remove),
         py::arg("subject"), py::arg("predicate"), py::arg("object"),
         py::arg("transaction") = kDB::Repository::Transaction())
    .def("remove",
         pybind11::overload_cast<const knowRDF::Triple&, const kDB::Repository::Transaction&>(
           &kDB::Repository::TripleStore::remove),
         py::arg("triple"), py::arg("transaction") = kDB::Repository::Transaction())
    .def(
      "remove",
      pybind11::overload_cast<const QList<knowRDF::Triple>&, const kDB::Repository::Transaction&>(
        &kDB::Repository::TripleStore::remove),
      py::arg("triples"), py::arg("transaction") = kDB::Repository::Transaction());
  py::class_<kDB::Repository::TripleStreamInserter, knowRDF::TripleStreamListener>(
    m, "TripleStreamInserter")
    .def(py::init<kDB::Repository::TripleStore>());
}
