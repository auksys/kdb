#include <pybind11/stl.h>

#include <knowCorePython.h>

#include <knowDBC/Query.h>

#include <kDB/Repository/Connection.h>
#include <kDBDocuments/Manager.h>

namespace py = pybind11;

PYBIND11_MODULE(kDBDocuments, m)
{
  m.doc() = R"pbdoc(
    knowCore
    -----------

    .. currentmodule:: kDBDocuments

    .. autosummary::
      :toctree: _generate
  )pbdoc";
  m.def("createDQLQuery",
        [](const kDB::Repository::Connection& _connection) {
          return knowDBC::Query(
            _connection.extensionObject<kDBDocuments::Manager>()->createDQLQuery());
        });
}
