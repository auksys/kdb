#include <knowCorePython.h>

#include <knowCore/Timestamp.h>

#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/TripleStore.h>

#include <kDBSensing/SalientRegions/Collection.h>
#include <kDBSensing/SalientRegions/SalientRegion.h>

namespace py = pybind11;

PYBIND11_MODULE(kDBSensing, m)
{
  m.doc() = R"pbdoc(
    knowCore
    -----------

    .. currentmodule:: kDBSensing

    .. autosummary::
      :toctree: _generate
  )pbdoc";

  py::module_ mSalientRegions = m.def_submodule("SalientRegions");

  namespace kS = kDBSensing;
  namespace kSSR = kDBSensing::SalientRegions;

  py::class_<kSSR::SalientRegion>(mSalientRegions, "SalientRegion")
    .def("exists", &kSSR::SalientRegion::exists)
    .def("uri", &kSSR::SalientRegion::uri)
    .def("type", &kSSR::SalientRegion::type)
    .def("hasProperty", &kSSR::SalientRegion::hasProperty)
    .def("property",
         pybind11_qt::overload_r_c<cres_qresult<knowCore::Value>, kSSR::SalientRegion,
                                   const knowCore::Uri&>(&kSSR::SalientRegion::property))
    .def("setProperty",
         pybind11_qt::overload_r_c<cres_qresult<void>, kSSR::SalientRegion, const knowCore::Uri&,
                                   const knowCore::Value&>(&kSSR::SalientRegion::setProperty))
    .def("geometry", &kSSR::SalientRegion::geometry);

  typedef cres_qresult<kSSR::Collection> (*dssCreate)(
    const kDB::Repository::Connection& _connection, const knowCore::Uri& _graph);

  py::class_<kSSR::Collection>(mSalientRegions, "Collection")
    .def_static("get", &kSSR::Collection::get)
    .def_static("create", dssCreate(&kSSR::Collection::create))
    .def_static("getOrCreate", &kSSR::Collection::getOrCreate)
    .def_static("get", &kSSR::Collection::getOrCreate)
    .def("createSalientRegion", &kSSR::Collection::createSalientRegion)
    .def("createSalientRegion",
         [](kSSR::Collection* _self, const knowGIS::GeometryObject& _geometry,
            const knowCore::Timestamp& _timestamp, const QList<knowCore::Uri>& _klasses,
            const knowCore::ValueHash& _properties, const knowCore::Uri& _salientregionUri)
         {
           return _self->createSalientRegion(_geometry, _timestamp, _klasses, _properties,
                                             _salientregionUri);
         })
    .def("salientRegion", &kSSR::Collection::salientRegion)
    .def("valid?", &kSSR::Collection::isValid);
}
