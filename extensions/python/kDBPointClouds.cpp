#include <pybind11/stl.h>

#include <knowCorePython.h>

#include <kDB/Repository/Connection.h>

#include <kDBPointClouds/Patch.h>
#include <kDBPointClouds/PointSpecification.h>

namespace py = pybind11;

PYBIND11_MODULE(kDBPointClouds, m)
{
  m.doc() = R"pbdoc(
    knowCore
    -----------

    .. currentmodule:: kDB.RDFView

    .. autosummary::
      :toctree: _generate
  )pbdoc";
  py::class_<kDBPointClouds::Patch>(m, "Patch")
    .def(py::init())
    .def(py::init<const kDBPointClouds::PointSpecification&, const QByteArray&>())
    .def("toPGWKB", &kDBPointClouds::Patch::toPGWKB)
    .def_static(
      "fromPGWKB",
      pybind11_qt::overload<const QByteArray&, const kDBPointClouds::PointSpecification&,
                            kDBPointClouds::Patch::PGWKBFlags>(&kDBPointClouds::Patch::fromPGWKB),
      py::arg("data"), py::arg("specification"),
      py::arg("flags")
      = kDBPointClouds::Patch::PGWKBFlags(kDBPointClouds::Patch::PGWKBFlag::IgnorePCID))
    .def_static(
      "fromPGWKB",
      pybind11_qt::overload<const QByteArray&, const kDB::Repository::Connection&,
                            kDBPointClouds::Patch::PGWKBFlags>(&kDBPointClouds::Patch::fromPGWKB),
      py::arg("data"), py::arg("connection"),
      py::arg("flags")
      = kDBPointClouds::Patch::PGWKBFlags(kDBPointClouds::Patch::PGWKBFlag::IgnorePCID));
  py::class_<kDBPointClouds::PointSpecification>(m, "PointSpecification");
}
