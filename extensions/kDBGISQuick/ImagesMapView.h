#pragma once

#include <QGeoRectangle>
#include <QImage>
#include <QMutex>
#include <QQuickItem>
#include <QThreadPool>

#include <kDBQuick/Forward.h>

namespace kDBGISQuick
{
  class ImagesMapView : public QQuickItem
  {
    Q_PROPERTY(kDBQuick::Connection* connection READ connection WRITE setConnection NOTIFY
                 connectionChanged);
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged);
    Q_PROPERTY(
      QGeoRectangle mapRectangle READ mapRectangle WRITE setMapRectangle NOTIFY mapRectangleChanged)
    Q_OBJECT
  public:
    ImagesMapView(QQuickItem* _parent = nullptr);

    kDBQuick::Connection* connection() const { return m_connection; }
    void setConnection(kDBQuick::Connection* _connection)
    {
      m_connection = _connection;
      emit(connectionChanged());
      loadImages();
    }
    void setMapRectangle(const QGeoRectangle& shape)
    {
      m_mapRectangle = shape;
      emit(mapRectangleChanged());
      loadImages();
    }
    QGeoRectangle mapRectangle() const { return m_mapRectangle; }
    QString source() const { return m_source; }
    void setSource(const QString& _source)
    {
      m_source = _source;
      emit(sourceChanged());
      loadImages();
    }
  private:
    void loadImages();
  signals:
    void mapRectangleChanged();
    void connectionChanged();
    void sourceChanged();
  private:
    QGeoRectangle m_mapRectangle;
    QMutex m_imageCacheMutex;
    struct ImageInfo
    {
      QImage image;
    };
    QHash<QString, ImageInfo> m_imageCache;
    kDBQuick::Connection* m_connection = nullptr;
    QThreadPool m_pool;
    QString m_source;
  };
} // namespace kDBGISQuick
