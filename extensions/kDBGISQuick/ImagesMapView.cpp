#include "ImagesMapView.h"

#include <cstring>

#include <QRegularExpression>
#include <QtConcurrent/QtConcurrent>

#include <knowCore/TypeDefinitions.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <knowGIS/Raster.h>

#include <kDB/Repository/Connection.h>

#include <kDBQuick/Connection.h>

#include <knowCore/toQImage.h>

using namespace kDBGISQuick;

ImagesMapView::ImagesMapView(QQuickItem* _parent) : QQuickItem(_parent)
{
  setFlag(QQuickItem::ItemHasContents);
  m_pool.setMaxThreadCount(1);
}

void ImagesMapView::loadImages()
{
  if(m_connection and m_connection->isConnected() and not m_source.isEmpty())
  {
    QRegularExpression sourceParser("(.*)\\((.*),(.*)\\)");
    QRegularExpressionMatch match = sourceParser.match(m_source);
    if(not match.hasMatch())
    {
      qWarning() << "Source should have form tablename(idfieldname,rasterfieldname)";
    }
    else
    {
      QString tablename = match.captured(1);
      QString fieldname_id = match.captured(2);
      QString fieldname_raster = match.captured(3);

      (void)QtConcurrent::run(
        &m_pool,
        [this, tablename, fieldname_id, fieldname_raster]()
        {
          knowDBC::Query q = m_connection->nativeConnection().createSQLQuery(
            clog_qt::qformat("SELECT {} FROM {} WHERE ST_Within(ST_Transform(ST_MakeEnvelope($1, "
                             "$2, $3, $4, $5), ST_SRID({})), ST_Envelope({}))",
                             tablename, fieldname_id, fieldname_raster));
          q.bindValue("$1", m_mapRectangle.topLeft().longitude());
          q.bindValue("$2", m_mapRectangle.topLeft().latitude());
          q.bindValue("$3", m_mapRectangle.bottomRight().longitude());
          q.bindValue("$4", m_mapRectangle.bottomRight().latitude());
          q.bindValue("$5", 4326); // WGS84

          knowDBC::Result r = q.execute();

          if(r)
          {
            QStringList ids;

            QHash<QString, ImageInfo> imageCache;
            {
              QMutexLocker l(&m_imageCacheMutex);
              imageCache = m_imageCache;
            }

            for(int i = 0; i < r.tuples(); ++i)
            {
              ids.append(r.value<QString>(0, i).expect_success());
            }
            for(const QString& key : imageCache.keys())
            {
              if(not ids.contains(key))
              {
                imageCache.remove(key);
              }
            }
            for(const QString& key : ids)
            {
              if(not imageCache.contains(key))
              {
                q.clearBindings();
                q.setQuery(clog_qt::qformat("SELECT {} FROM {} WHERE {}::text = $1", tablename,
                                            fieldname_id, fieldname_raster));
                q.bindValue("$1", key);
                r = q.execute();
                if(r)
                {
                  knowGIS::Raster raster = r.value(0, 0).value<knowGIS::Raster>().expect_success();
                  ;
                  knowCore::Image image
                    = raster.toImage().convert(knowCore::Image::Type::UnsignedInteger8);
                  ImageInfo ii;
                  ii.image = knowCore::toQImage(image);
                  imageCache[key] = ii;
                }
                else
                {
                  qWarning() << "Failed to execute query: " << r.query() << " with error "
                             << r.error();
                }
              }
            }
            {
              QMutexLocker l(&m_imageCacheMutex);
              m_imageCache = imageCache;
            }

            QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
          }
          else
          {
            qWarning() << "Failed to execute query: " << r.query() << " with error " << r.error();
          }
        });
    }
  }
}

#include "moc_ImagesMapView.cpp"
