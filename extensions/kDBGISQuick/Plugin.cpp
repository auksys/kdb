#include "Plugin.h"

#include "ImagesMapView.h"
#include "Singleton.h"

using namespace kDBGISQuick;

void Plugin::registerTypes(const char* uri)
{
  const char* uri_kDBGIS_Controls = "kDBGIS.Controls";

  if(strcmp(uri, uri_kDBGIS_Controls) == 0)
  {
    qmlRegisterType<ImagesMapView>(uri_kDBGIS_Controls, 1, 0, "ImagesMapView");
  }

  const char* uri_kDBGIS = "kDBGIS";

  if(strcmp(uri, uri_kDBGIS) == 0)
  {
    qmlRegisterSingletonType<Singleton>(uri_kDBGIS, 2, 0, "KDBGIS",
                                        [](QQmlEngine*, QJSEngine*) -> QObject*
                                        { return new Singleton; });
  }
}
