#pragma once

#include <QQmlExtensionPlugin>

namespace kDBGISQuick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "kDBGIS/5.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace kDBGISQuick
