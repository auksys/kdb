#include "Singleton.h"

#include <clog_qt>

#include <QBuffer>

#include <kDBGIS/Features/Collection.h>
#include <kDBGIS/Features/Feature.h>

Singleton::Singleton(QObject* _parent) : QObject(_parent) {}

Singleton::~Singleton() {}

QString Singleton::loadFeaturesFromOverpass(kDBQuick::Connection* _connection,
                                            const QString& _destination, const QString& _data)
{
  cres_qresult<kDBGIS::Features::Collection> col1
    = kDBGIS::Features::Collection::getOrCreate(_connection->nativeConnection(), _destination);
  if(col1.is_successful())
  {
    QByteArray data_utf8 = _data.toUtf8();
    QBuffer buffer(&data_utf8);
    kDBGIS::Features::Collection co1_v = col1.get_value();
    cres_qresult<void> r = co1_v.importFromOverpass(&buffer);
    if(r.is_successful())
    {
      return QString();
    }
    else
    {
      return r.get_error().get_message();
    }
  }
  else
  {
    return clog_qt::qformat("Cannot get or create features collection {} with error: {}",
                            _destination, col1.get_error().get_message());
  }
}
