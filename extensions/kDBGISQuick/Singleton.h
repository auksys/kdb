#pragma once

#include <QObject>

#include <kDBQuick/Connection.h>

class Singleton : public QObject
{
  Q_OBJECT
public:
  Singleton(QObject* _parent = nullptr);
  ~Singleton();
  Q_INVOKABLE QString loadFeaturesFromOverpass(kDBQuick::Connection* _connection,
                                               const QString& _destination, const QString& _data);
};