import Cyqlops.WebServer
import Cyqlops.WebServer.Fragments
import Cyqlops.WebServer.Resources

import knowCore
import knowDBC

import kDB.Repository as KDBRepository

WebSite
{
  id: root
  property KDBRepository.Connection connection: KDBRepository.Connection
  {
  }
  DynamicResource
  {
    path: "/(index\\.html)?"
    Body
    {
      Header
      {
        title: "kDB End Point"
      }
      Text
      {
          text: `<center><img src="assets/images/auksys_banner.png" alt="auksys" width="636" height="200"><br/><br/>
    <b>kDB Web Server</b> connected to <i>${webSite.store.getStoredValue("kDBHostName")}:${webSite.store.getStoredValue("kDBPort")}</i></center>
    
    Available services:

    <ul>
      <li><a href="/sparql.html">SPARQL End-Point</a></li>
      <li><a href="/krql.html">krQL End-Point</a></li>
    </ul>
    `
      }
    }
  }
  FilesResource
  {
    path: "/assets/(.*)"
    directory: "qrc:/kdbwebserver/assets/"
  }
  DynamicResource
  {
    path: "/(sparql|krql)\\.html"
    Choice
    {
      id: choice
      property var queryier: Query
      {
        queryType: getType(request.path)
        function queryExample(type)
        {
          if(type == KnowCore.Uris.askCoreQueries.SPARQL)
          {
            return "SELECT ?x ?y ?z WHERE {?x ?y ?z .}"
          } else if(type == KnowCore.Uris.askCoreQueries.krQL)
          {
            return "test:\n  return: 42"
          } else {
            return ""
          }
        }
        function getType(path)
        {
          if(path == "/krql.html")
          {
            return KnowCore.Uris.askCoreQueries.krQL
          } else if(path == "/sparql.html") {
            return KnowCore.Uris.askCoreQueries.SPARQL
          } else {
            console.log("Cannot deduce query type from path: " + path)
          }
        }
        function getQuery(request)
        {
          if(request.parameters["query"])
          {
            return request.parameters["query"][0]
          } else if(request.parameters["update"])
          {
            return request.parameters["update"][0]
          } else {
            return ""
          }
        }
        query: getQuery(request)
        onQueryChanged:
        {
          if(!root.connection.connected)
          {
            root.connection.hostname = webSite.store.getStoredValue("kDBHostName")
            root.connection.port = webSite.store.getStoredValue("kDBPort")
            root.connection.connect()
          }
          connection = root.connection.connection
          if(root.connection.connected)
          {
            execute()
            show_error.lastError = lastError
            if(format == "text/html")
            {
              html_result.headers = result.columnNames
              html_result.ltmodel.sourceModel = result
            } else {
              try {
                json_result.text = result.serialise(format);
              } catch(err) {
                show_error.lastError = err.message;
              }
            }
          } else {
            show_error.lastError = "Failed to connect to store with error: " + root.connection.lastError
          }
        }
      }
      function indexFor(format)
      {
        if(format == "text/html")
        {
          return 1
        }
        return 0
      }
      function formatFrom(accept)
      {
        if(accept)
        {
          var f = accept[0]
          return f.split(",")[0]
        } else if(request.parameters["query"]) {
          return "application/sparql-results+xml"
        } else {
          return "text/html"
        }
      }
      property string format: formatFrom(request.parameters["Accept"])
      index: indexFor(format)
      Text
      {
        id: json_result
        mime: format
      }
      Body
      {
        Header
        {
          title: "kDB End Point"
        }
        Text
        {
          property var query: request.parameters["query"]
          text: `<form action="" method=\"post\">
<label for="query">Query:</label><textarea id="query" name="query" cols="100" rows="10">${query ? query : queryier.queryExample(queryier.queryType)}</textarea><br/>
<label for="Accept">Format:</label>
  <select id="Accept" name="Accept">
    <option value="text/html">HTML</option>
    <option value="application/sparql-results+json">JSON</option>
    <option value="application/sparql-results+xml">XML</option>
  </select>
<input type="submit" />
</form>`
        }
        Conditional
        {
          id: show_error
          condition: lastError.length != 0
          property string lastError
          Text
          {
            text: "Error: " + show_error.lastError
          }
        }
        Table
        {
          id: html_result
          property ListToTableItemModel ltmodel: ListToTableItemModel
          {
          }
          model: ltmodel
          delegate: function(item) {
            return KnowCore.displayString(item)
          }
        }
      }
    }
  }
  

}
