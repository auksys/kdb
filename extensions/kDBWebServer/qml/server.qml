import Cyqlops.DB
import Cyqlops.WebServer

WebServer
{
  id: root
  property string kDBHostName
  property int kDBPort
  webSite: "qrc:/kdbwebserver/qml/site.qml"
  KeyValueStore
  {
    id: kv
  }
  onKDBHostNameChanged: kv.setStoredValue("kDBHostName", root.kDBHostName)
  onKDBPortChanged: kv.setStoredValue("kDBPort", root.kDBPort)
  maximumRequestBodySize: 81960000
}
