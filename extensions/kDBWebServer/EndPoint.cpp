#include "EndPoint.h"

#include <QQmlComponent>
#include <QQmlEngine>

#include <clog_qt>
#include <kDB/Repository/Connection.h>

using namespace kDBWebServer;

struct EndPoint::Private
{
  QObject* server = nullptr;

  QQmlEngine engine;
};

EndPoint::EndPoint(const QString& _kdb_hostname, int _kdb_port) : d(new Private)
{
  QQmlComponent c(&d->engine);
  c.loadUrl(QUrl("qrc:/kdbwebserver/qml/server.qml"), QQmlComponent::PreferSynchronous);
  if(c.isError())
  {
    clog_error("Failed to load server: {}", c.errorString());
  }
  else
  {
    d->server = c.create();
    d->server->setProperty("kDBHostName", _kdb_hostname);
    d->server->setProperty("kDBPort", _kdb_port);
  }

  setPort(4242);
}

EndPoint::~EndPoint() { delete d; }

bool EndPoint::start()
{
  if(d->server)
  {
    bool r;
    if(QMetaObject::invokeMethod(d->server, "start", Q_RETURN_ARG(bool, r)))
    {
      return r;
    }
  }
  return false;
}

void EndPoint::stop()
{
  if(d->server)
  {
    QMetaObject::invokeMethod(d->server, "stop");
  }
}

void EndPoint::setPort(int _port)
{
  if(d->server)
  {
    d->server->setProperty("port", _port);
  }
}
