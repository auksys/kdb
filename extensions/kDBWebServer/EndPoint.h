#include <kDB/Forward.h>

namespace kDBWebServer
{
  /**
   * @ingroup kDBWebServer
   * A webserver interface to the store. It allows to behave like a SPARQL end-point.
   */
  class EndPoint
  {
  public:
    EndPoint(const QString& _kdb_hostname, int _kdb_port);
    ~EndPoint();
    /**
     * Start the end point.
     */
    bool start();
    /**
     * Stop it
     */
    void stop();
    /**
     * Set the port for the webserver.
     */
    void setPort(int _port);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBWebServer
