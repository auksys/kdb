import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import knowCore
import knowBook.Private.Plugins

BaseQueryOperation
{
  name: "krQL Query"
  queryType: KnowCore.Uris.askCoreQueries.krQL
  function initialise(operation)
  {
    if(operation.definition["query"] == null)
    {
      operation.definition["query"] = `TEST:
  return: 42`
      operation.definition["bindings"] = "{}"
      operation.definition["collection"] = ""
    }
  }
  function prepareQuery(query, operation)
  {
    query.query = operation.definition["query"]
  }
  supportBindings: false
}
