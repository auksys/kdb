import QtQml
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import knowBook
import knowBook.Controls
import knowBook.Plugins
import knowBook.NotebookStyle

OperationPlugin
{
  id: _op_plugin_
  name: "RDF Dataset Export"
  function initialise(operation)
  {}
  function visible(context)
  {
    return context.state.connection && context.state.connection.connected
  }
  function execute(operation)
  {
    operation.localState.set("statusMessage", "Start saving...")
    var ts = Qt.createQmlObject('import kDB.Repository; TriplesSaver {}', _op_plugin_, "CreateTriplesSaver.qml")
    ts.graphName = operation.definition["dataset"]
    ts.connection = operation.context.state.connection
    ts.save("file:"+operation.definition["filename"])
    ts.savingFinished.connect(function() {
      operation.localState.set("statusMessage", "Saving finished.")
    })
    ts.lastErrorChanged.connect(function() {
      if(ts.lastError.length > 0)
      {
        operation.localState.set("statusMessage", "<font color='red'><b>Error:</b> failed to export with error '" + ts.lastError + "''.</font>")
      }
    })
  }
  operationComponent: RowLayout
  {
    id: _root_
    property var operation
    property var __connection: operation.context.state.connection
    ComboBox
    {
      id: cb
      model: _root_.__connection ? _root_.__connection.rdfGraphs : []
      currentIndex: operation ? model.indexOf(operation.definition["dataset"]) : 0
      onActivated: index => operation.definition["dataset"] = model[index]
      implicitContentWidthPolicy: ComboBox.WidestText
    }
    Label
    {
      text: "Filename"
    }
    TextField
    {
      text: operation.definition["filename"]
      onTextChanged: operation.definition["filename"] = text
      Layout.fillWidth: true
    }
    ComboBox
    {
      model: ["turtle (ttl)"]
    }
  }
}
