import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import kDB.Repository

import kDBDatasets

import QtPositioning
import QtLocation

import knowBook.Plugins

import knowCore
import knowValues as KV
import knowGIS as KG

import Cartography.Geometry
import Cartography.Geometry.Mapping

ViewPlugin
{
  name: "Datasets"
  viewComponent: ColumnLayout
  {
    id: root
    property Connection connection
    
    Datasets
    {
      id: dss
      connection: root.connection
      uri: cb.currentText
    }
    
    RowLayout
    {
      Layout.fillWidth: true
      Item
      {
        Layout.fillWidth: true
        height: 1
      }
      ComboBox
      {
        id: cb
        model: dss.uris
        implicitContentWidthPolicy: ComboBox.WidestText
      }
    }
    SwipeView
    {
      id: swipeView
      Layout.fillWidth: true
      Layout.fillHeight: true
      TableView
      {
        id: tableView
        clip: true
        model: DatasetsTableModel
        {
          id: tableModel
          datasets: dss.datasets
        }
        delegate: Label {
          text: display
          horizontalAlignment: row == 0 ? Text.AlignHCenter : Text.AlignLeft
          color: row == 0 ? "white" : "black"
          background: Rectangle { color: selectColor(row, column) }
          
          function selectColor(r,c)
          {
            if(r == 0)
            {
              return (c % 2) == 1 ? "#444444" : "#333333"
            } else if((r+c) % 2 == 1) {
              return "lightgray"
            } else {
              return "white"
            }
          }
          MouseArea
          {
            anchors.fill: parent
            onReleased:
            {
              if(column == 0 && row >= 1)
              {
                var ds = dss.datasets[row - 1]

                if(ds.type == "http://askco.re/sensing#salient_region")
                {
                  mapViewer.dataset = ds
                  swipeView.setCurrentIndex(2)
                } else {
                  dataViewer.dataset = ds
                  swipeView.setCurrentIndex(1)
                }

              } else {
                mouse.accepted = false
              }
            }
          }
        }
        ScrollIndicator.horizontal: ScrollIndicator { }
        ScrollIndicator.vertical: ScrollIndicator { }
      }
      Item
      {
        id: dataViewer
        property var dataset
        Text
        {
          z: -1
          anchors.centerIn: parent
          text: "Failed to load 3d Viewer, check installation..."
        }
        Button
        {
          z: 2
          text: "back"
          onClicked: swipeView.setCurrentIndex(0)
        }
        Component.onCompleted:
        {
          var pw = Qt.createQmlObject("import kDBDatasets; DataViewer { anchors.fill: parent }", dataViewer)
          pw.dataset = Qt.binding(function() { return dataViewer.dataset })
        }
      }
      Map
      {
        id: mapViewer
        property var dataset
        property var __data: dataset ? dataset.data : []
        function get_children_of_type(rr, type)
        {
          return rr ? rr.filter(result => KnowCore.getDatatype(result) == type) : []
        }

        Button
        {
          z: 2
          text: "back"
          onClicked: swipeView.setCurrentIndex(0)
        }
        // activeMapType: supportedMapTypes[supportedMapTypes.length - 1] // __map_type(supportedMapTypes, MapType.CustomMap)
        function __map_type(smt, type)
        {
          for(var mti in smt)
          {
            var mt = smt[mti]
            if(mt.style == type)
            {
              return mt
            }
          }
          return smt[0]
        }
        plugin: Plugin
        {
          id: _mapPlugin_
          name: "osm"
          PluginParameter { name: "osm.useragent"; value: "tst_editor" }
          PluginParameter { name: "osm.mapping.custom.host"; value: "http://c.tile.openstreetmap.org/" }
        }

        GeometryMapItem
        {
          id: _dataset_geometry_item_
          geometry: GeometryFactory.fromGeometryObject(mapViewer.dataset.geometry)
          color: "#33333333"
          border.color: "red"
          border.width: 1
          onGeometryChanged: mapViewer.fitViewportToMapItems([_dataset_geometry_item_.activeItem])
        }
        MapItemView
        {
          id: _dataset_items_
          model: mapViewer.get_children_of_type(mapViewer.__data, 'http://askco.re/sensing#salient_region')
          delegate: GeometryMapItem
          {
            property var salientRegion: KV.Utils.getObject(modelData)
            geometry: GeometryFactory.fromGeometryObject(salientRegion.geometry)
            pointDelegate: Image
            {
              property var __images: {
                "human": "data/kDB/icons/HumanMarker.svg",
                "depot": "data/kDB/icons/DepotMarker.svg",
                "no_sea": "data/kDB/icons/AnomalyMarker.svg"
              }
              function select_image(klasses)
              {
                for(var i = 0; i < klasses.length; ++i)
                {
                  var k = klasses[i]
                  if(__images[k] != undefined)
                  {
                    return __images[k]
                  }
                }
                console.log("No icon defined for ", klasses, " defaulting to other.")
                return  "data/kDB/icons/OtherMarker.svg"
              }
              id: image
              source: select_image(KnowCore.fromUriList(salientRegion.classes))
              width: 64
              height: 64
              property point anchorPoint: Qt.point(image.width * 0.5, image.height)
            }
          }
        }
        Component.onCompleted: zoomLevel = 16.0
      }

    }
  }
}

