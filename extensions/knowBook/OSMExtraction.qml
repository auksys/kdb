import QtQml
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import knowBook
import knowBook.Controls
import knowBook.Plugins
import knowBook.NotebookStyle

import kDBGIS

OperationPlugin
{
  name: "OSM Extraction"
  function initialise(operation)
  {}
  function visible(context)
  {
    return context.state.connection && context.state.connection.connected
  }
  function execute(operation)
  {
    operation.localState.set("statusMessage", "Sending request...")

    let request = new XMLHttpRequest();
    request.onreadystatechange = function()
    {
      if (request.readyState === XMLHttpRequest.DONE) {
        var r = KDBGIS.loadFeaturesFromOverpass(operation.context.state.connection, operation.definition["destination"], request.responseText)
        if(r.length > 0)
        {
          console.log("Full error is: " + r)
          operation.localState.set("statusMessage", "<font color='red'><b>Error:</b> failed to parse data from overpass with error: " + r + ".</font>")
        } else {
          operation.localState.set("statusMessage", "Data downloaded into '" + operation.definition["destination"] + "'")
        }
      } else {
        operation.localState.set("statusMessage", "Current status is " + request.statusText)
      }
    }
    var query = operation.localState.query
    console.log("Send request to https://overpass-api.de/api/interpreter for query: " + query)
    request.open("POST", "https://overpass-api.de/api/interpreter");
    //Send the proper header information along with the request
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var params = "data=" + encodeURIComponent(query)
    request.setRequestHeader("Content-length", params.length);
    request.setRequestHeader("Connection", "close");
    request.send(params);
  }
  operationComponent: GridLayout {
    property QtObject operation
    property var __connection: operation.context.state.connection
    enabled: __connection && __connection.connected
    columns: 3
    ButtonGroup {
      buttons: [pan_button, select_button]
    }
    Button
    {
      id: pan_button
      text: "pan"
      checked: true
      checkable: true
    }
    Button
    {
      id: select_button
      text: "select"
      checkable: true
    }
    Item
    {
      clip: true
      MapView
      {
        id: _map_view_
        anchors.fill: parent
        MouseArea
        {
          id: _select_area_
          anchors.fill: parent
          enabled: select_button.checked
          property real startX: 0
          property real startY: 0
          property real endX: 0
          property real endY: 0
          property bool active: false
          preventStealing: true
          function make_rect(x1, y1, x2, y2)
          {
            if(x2 < x1) return make_rect(x2, y1, x1, y2)
            if(y2 < y1) return make_rect(x1, y2, x2, y1)
            return Qt.rect(x1, y1, x2 - x1, y2 - y1)
          }
          property rect rect: make_rect(startX, startY, endX, endY)
          onPressed: mouse => {
            active = true
            startX = mouse.x
            startY = mouse.y
            endX = startX
            endY = startY
          }
          onPositionChanged: mouse => {
            endX = mouse.x
            endY = mouse.y
            var pt1 = _map_view_.viewTransform.toWgs84(_select_area_.rect.left, _select_area_.rect.bottom)
            var pt2 = _map_view_.viewTransform.toWgs84(_select_area_.rect.right, _select_area_.rect.top)

            operation.localState.set("query",  "nwr(" + pt1.y + "," + pt1.x + "," + pt2.y + "," + pt2.x + "); out;")

          }
        }
      }
      Rectangle
      {
        visible: _select_area_.active
        x: _select_area_.rect.left
        y: _select_area_.rect.top
        width: _select_area_.rect.width
        height: _select_area_.rect.height

        color: "#FF9800"
        opacity: 0.5
      }
      Layout.minimumHeight: 400
      Layout.fillHeight: true
      Layout.fillWidth: true
      Layout.rowSpan: 10
    }
    Label {
      text: "Destination:"
    }
    TextField
    {
      text: operation.definition["destination"]
      onTextChanged: operation.definition["destination"] = text
    }
  }
}
