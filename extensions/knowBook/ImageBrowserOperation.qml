import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import Cyqlops.Controls
import kDB.Repository

import knowCore
import knowDBC

import knowBook.Plugins

OperationPlugin
{
  name: "Image Browser"
  operationComponent: Item
  {
    id: _root_
    property var connection
    property var datasetsList: []
    property var framesList: []
    function refresh()
    {
      _sql_datasets_.execute()
    }
    onVisibleChanged:
    {
      if(visible && root.connection.isConnected)
      {
        refresh()
      }
    }
    Query
    {
      id: _sql_datasets_
      connection: _root_.connection
      query: "SELECT DISTINCT dataseturi FROM cameraframes" 
      autoExecute: true
      onExecutionFinished:
      {
        var dss = []
        var r = _sql_datasets_.result
        for(var i = 0; i < r.rowCount(); ++i)
        {
          dss.push(KnowCore.valueToVariant(r.data(r.index(i, 0), 256)))
        }
        _root_.datasetsList = dss
      }
    }
    Query
    {
      id: _sql_ids_
      connection: _root_.connection
      query: "SELECT DISTINCT id, timestamp FROM cameraframes WHERE dataseturi = '" + _dataset_selector_.currentText + "' ORDER BY timestamp"
      autoExecute: true
      onExecutionFinished:
      {
        var dss = []
        var r = _sql_ids_.result
        for(var i = 0; i < r.rowCount(); ++i)
        {
          dss.push(KnowCore.valueToVariant(r.data(r.index(i, 0), 256)))
        }
        _root_.framesList = dss
      }
    }
    Query
    {
      id: _sql_image_
      connection: _root_.connection
      query: "SELECT DISTINCT data, width, height, encoding, compression FROM cameraframes WHERE id = '" + _frame_selector_.currentText + "'"
      autoExecute: true
      onExecutionFinished:
      {
        var r = _sql_image_.result
        if(r.rowCount() == 0)
        {
          console.log("No results")
        } else if(r.rowCount() > 1)
        {
          console.log("Too many results, should have been only one")
        } else {
          var data = KnowCore.valueToVariant(r.data(r.index(0, 0), 256))
          var width = KnowCore.valueToVariant(r.data(r.index(0, 0), 256+1))
          var height = KnowCore.valueToVariant(r.data(r.index(0, 0), 256+2))
          var encoding = KnowCore.valueToVariant(r.data(r.index(0, 0), 256+3))
          var compression = KnowCore.valueToVariant(r.data(r.index(0, 0), 256+4))
          _image_view_.image = KnowCore.toImage(data, width, height, encoding, compression)
        }
      }
    }
    GridLayout
    {
      id: _grid_layout_
      anchors.fill: parent
      columns: 8
      Button
      {
        text: "Refresh"
        onClicked: _root_.refresh()
      }
      Label
      {
        text: "Dataset"
      }
      ComboBox
      {
        id: _dataset_selector_
        model: datasetsList
        implicitContentWidthPolicy: ComboBox.WidestText
      }
      Label
      {
        text: "Image"
      }
      ComboBox
      {
        id: _frame_selector_
        model: framesList
        implicitContentWidthPolicy: ComboBox.WidestText
      }
      Button
      {
        text: "<"
        onClicked: _frame_selector_.currentIndex = _frame_selector_.currentIndex - 1
        enabled: _frame_selector_.currentIndex > 0
      }
      Button
      {
        text: _auto_play_timer_.running ? "pause" : "play"
        onClicked: {
          if(_auto_play_timer_.running)
          {
            _auto_play_timer_.stop()
          } else {
            _auto_play_timer_.start()
          }
        }
        enabled: _frame_selector_.currentIndex < _frame_selector_.model.length - 1
        Timer
        {
          id: _auto_play_timer_
          repeat: true
          interval: 100
          onTriggered:
          {
            if(_frame_selector_.currentIndex < _frame_selector_.model.length - 1)
            {
              _frame_selector_.currentIndex = _frame_selector_.currentIndex + 1
            }
          }
        }
      }
      Button
      {
        text: ">"
        onClicked: _frame_selector_.currentIndex = _frame_selector_.currentIndex + 1
        enabled: _frame_selector_.currentIndex < _frame_selector_.model.length - 1
      }
      ImageView
      {
        id: _image_view_
        Layout.columnSpan: _grid_layout_.columns
        Layout.fillWidth: true
        Layout.fillHeight: true
      }
    }
  }
}
