import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts

import knowCore
import kDB.Repository

import knowBook.Plugins

OperationPlugin
{
  name: "Triple Store (Version Control)"
  function visible(context)
  {
    return false && context.state.connection && context.state.connection.connected
  }
  operationComponent: SplitView
  {
    id: _root_
    orientation: Qt.Horizontal
    property var connection

    ScrollView
    {
      id: deltas_sv
      Rectangle
      {
        color: "white"
        width: deltas.width
        height: deltas.height
        Column
        {
          id:deltas
          Repeater
          {
            id: deltas_repeater
            Column
            {
              Rectangle
              {
                color: "darkblue"
                width: Math.max(parent_text.width, delta_text.width)
                height: parent_text.height
                Text
                {
                  id: parent_text
                  text: "Parent: " + KnowCore.toHex(modelData.parent).substring(0, 10)
                  color: "white"
                  font.bold:true
                }
              }
              Repeater
              {
                model: modelData.signatures
                Rectangle
                {
                  color: "darkgrey"
                  width: Math.max(signature_text.width, delta_text.width)
                  height: parent_text.height
                  Text
                  {
                    id: signature_text
                    text: "Signed by: " + modelData["author"] + " at " + modelData["timestamp"]
                    color: "white"
                    font.bold:true
                  }
                }
              }
              Text
              {
                id: delta_text
                text: KnowCore.toUtf8(modelData.delta)
              }
            }
          }
        }
      }
    }
    ColumnLayout
    {
      RowLayout
      {
        Layout.fillWidth: true
        Item
        {
          Layout.fillWidth: true
          height: cb.height
        }
        Button
        {
          MessageDialog
          {
            id: save_to_dot_failed
            title: "Saving to DOT failed"
            text: "Saving to DOT has failed.";
            buttons: MessageDialog.Ok
          }
          FileDialog
          {
            id: saveDOTDialog
            title: "Save to DOT"
            nameFilters: [ "DOT files (*.dot)" ]
            fileMode: FileDialog.SaveFile
            onAccepted: {
              if(!triplesStore.revisionsGraph().exportToDot(selectedFile, function(node) {
                return KnowCore.toHex(node.hash).substring(0, 10) + "," + KnowCore.toHex(node.contentHash).substring(0, 10) + "," + node.historicity }))
              {
                save_to_dot_failed.open()
              }
            }
          }
          text: "Export to dot..."
          onClicked: saveDOTDialog.open()
        }
        ComboBox
        {
          id: cb
          model: _root_.connection.rdfGraphs
          implicitContentWidthPolicy: ComboBox.WidestText
        }
      }
      TripleStore
      {
        id: triplesStore
        graphName: cb.currentText
      }
      StackLayout
      {
        id: main_area
        Layout.fillWidth: true
        Layout.fillHeight: true
        function __select_index(hasVersionControl)
        {
          if(hasVersionControl)
          {
            if(graph_view.hasCyqlopsGraphviz)
            {
              return graph_view.index
            } else {
              return no_cyqlops_graphviz.index
            }
          } else {
            return no_version_control.index
          }
        }
        currentIndex: __select_index(triplesStore.hasVersionControl)
        Label
        {
          id: no_version_control
          property int index: 0
          text: "No version control for this graph"
          horizontalAlignment: Text.AlignHCenter
          verticalAlignment: Text.AlignVCenter
        }
        Label
        {
          id: no_cyqlops_graphviz
          property int index: 1
          text: "kDB was not build with Cyqlops.Graphviz!"
          color: "red"
          horizontalAlignment: Text.AlignHCenter
          verticalAlignment: Text.AlignVCenter
        }
        Item
        {
          id: graph_view
          property int index: 2
          property bool hasCyqlopsGraphviz: false
          Component.onCompleted:
          {
            try {
              var obj = Qt.createQmlObject("import knowBook.Controls; RevisionsGraph { anchors.fill: parent }", graph_view)
              obj.model = Qt.binding(function() { return triplesStore.hasVersionControl ? triplesStore.revisionsGraph() : null })
              deltas_repeater.model = Qt.binding(function() { return obj.revision ? obj.revision.deltas : null })
              hasCyqlopsGraphviz = true
            } catch(except)
            {
              console.log("Exception when trying to create a graph view: " + except)
            }
          }
        }
      }
      Label
      {
        id: errorLabel
        visible: text.length > 0
        color: "red"
        text: triplesStore.lastError
        Layout.fillWidth: true
      }
    }

    Layout.fillWidth: true
    Layout.fillHeight: true
  }
}
