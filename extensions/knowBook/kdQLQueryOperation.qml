import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import knowCore
import knowBook.Private.Plugins

BaseQueryOperation
{
  name: "kdQL Query"
  queryType: KnowCore.Uris.askCoreQueries.kdQL
  function initialise(operation)
  {
    if(operation.definition["query"] == null)
    {
      operation.definition["query"] = `retrieve:
  what: "*"
  from: :COLLECTION:`
      operation.definition["bindings"] = "{}"
      operation.definition["collection"] = ""
    }
  }
  function prepareQuery(query, operation)
  {
    var collection = operation.definition["collection"]
    query.query = operation.definition["query"].replace(":COLLECTION:", collection)
  }
  supportBindings: false
  optionsComponent: ColumnLayout {
    id: _root_
    property var operation
    property var __connection: operation.context.state.connection
    Label
    {
      text: "Collection:"
    }
    Frame
    {
      ListView
      {
        id: _document_collections_
        anchors.fill: parent
        model: _root_.__connection ? _root_.__connection.documentCollections : []
        clip: true
        currentIndex: operation ? model.indexOf(operation.definition["collection"]) : 0
        delegate: Rectangle
        {
          id: _document_collections_delegate_
          width: _document_collections_.width
          height: 4 + _document_collections_delegate_text_.height
          color: _document_collections_.currentItem == _document_collections_delegate_ ? "#00BCD4" : "transparent"
          Text
          {
            id: _document_collections_delegate_text_
            x: 2
            y: 2
            text: modelData
          }
          MouseArea
          {
            anchors.fill: parent
            onClicked:
            {
              _document_collections_.currentIndex = index
              _root_.operation.definition["collection"] = _document_collections_.model[index]
            }
          }
        } 
      }
      Layout.preferredWidth: 200
      Layout.preferredHeight: 100
    }
  }
}
