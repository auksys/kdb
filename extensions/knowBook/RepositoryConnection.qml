import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Qt.labs.settings

import Cyqlops

import knowCore
import kDB.Repository
import knowBook.Plugins
import knowBook.NotebookStyle

ConnectionPlugin
{
  name: "kDB/Repository"
  storeComponent: Store
  {}
  connectionComponent: Connection
  {}
  property Settings __settings: Settings {
    category: "kdb/repository"
    property string hostname: Cyqlops.toLocalFile(StandardPaths.writableLocation(StandardPaths.AppDataLocation)) + "/store"
    property int port: 1242
    property bool autoSelectPort: true
  }
  function initialise(operation)
  {
    if(!Object.values(operation.definition).includes("hostname"))
    {
      operation.definition["hostname"] = __settings.hostname
      operation.definition["port"] = __settings.port
      operation.definition["autoSelectPort"] = __settings.autoSelectPort
    }
    return operation
  }
  function update(operation, store, connection)
  {
    store.hostname = operation.definition["hostname"]
    store.port = operation.definition["port"]
    store.autoSelectPort = operation.definition["autoSelectPort"]
  }
  configurationComponent: GridLayout {
    id: _root_
    
    property var operation: {}
        
    columns: 2
    anchors.left: parent.left
    anchors.right: parent.right
    Label
    {
      text: "Hostname / Directory:"
    }
    TextField
    {
      id: hostname_field
      text: _root_.operation ? _root_.operation.definition["hostname"] : __settings.hostname
      onTextChanged: {
        if(_root_.operation) _root_.operation.definition["hostname"] = text
        __settings.hostname = text
      }
      Layout.fillWidth: true
    }
    Label
    {
      text: "Port:"
    }
    SpinBox
    {
      id: port_box
      editable: true
      from: 1000
      to: 60000
      textFromValue: value, locale => value
      value: _root_.operation ? _root_.operation.definition["port"] : __settings.port
      onValueChanged: {
        if(_root_.operation) _root_.operation.definition["port"] = value
        __settings.port = value
      }
    }
    Label
    {
      text: "Automatic port selection:"
    }
    CheckBox
    {
      id: auto_port
      checked: _root_.operation ? _root_.operation.definition["autoSelectPort"] : __settings.autoSelectPort
      onCheckedChanged: {
        if(_root_.operation) _root_.operation.definition["autoSelectPort"] = checked
        __settings.autoSelectPort = checked
      }
    }
  }
}