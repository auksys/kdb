#include "CameraFrame.h"

#include <knowCore/Image.h>

using namespace kDBSensing;

cres_qresult<knowCore::Image> CameraFrameBaseValue::toImage() const
{
  if(compression() == "none")
  {
    return knowCore::Image::fromRawData(data(), width(), height(), encoding());
  }
  else
  {
    return knowCore::Image::fromCompressedData(data(), compression());
  }
}
