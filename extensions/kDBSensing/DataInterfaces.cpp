#include <knowValues/Values.h>

#include <knowCore/Cast.h>
#include <knowCore/ValueHash.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <knowRDF/Triple.h>

#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/Transaction.h>
#include <kDB/Repository/TripleStore.h>

#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>
#include <kDBDatasets/Interfaces/DataInterface.h>
#include <kDBDatasets/Statistics.h>

#include <kDBRobotics/AgentPosition.h>
#include <kDBRobotics/FramePose.h>
#include <kDBRobotics/FrameTransformation.h>

#include <kDBPointClouds/PointCloud.h>
#include <kDBPointClouds/PointSpecification.h>

#include <kDBSensing/CameraFrame.h>
#include <kDBSensing/CameraInfo.h>
#include <kDBSensing/Lidar3dScan.h>
#include <kDBSensing/LidarConfig.h>
#include <kDBSensing/LidarScan.h>

#include <kDBSensing/SalientRegions/Collection.h>
#include <kDBSensing/SalientRegions/SalientRegion.h>

#include <knowValues/Uris/askcore_sensing.h>

#include "DatabaseCopy_p.h"

using namespace kDBSensing;

// TODO kDB 5.0 remove the use of Postgresql Copy in LidarDataInterface and PointCloudDataInterface,
// and do like for images, use a CBor representation
//              when doing so, add a check in DataInterfaceRegistry::importFrom to avoid loading
//              point clouds and lidar scans from older kDB versions

namespace kDBSensing
{
  struct BaseExtractIterator : public kDBDatasets::Interfaces::ExtractIterator
  {
  public:
    BaseExtractIterator(const kDB::Repository::Connection& _connection,
                        const kDBDatasets::Dataset& _dataset)
        : m_connection(_connection), m_dataset(_dataset)
    {
    }
  protected:
    virtual bool hasNextFrame() const = 0;
    virtual cres_qresult<QByteArray> nextFrame() = 0;
  public:
    bool hasNext() const override { return m_currentStep < 4; }
    cres_qresult<QByteArray> next() override
    {
      // Send data accordingly
      if(hasNextFrame())
      {
        return nextFrame();
      }
      if(not m_transformations.isEmpty())
      {
        kDBRobotics::FrameTransformationRecord record = m_transformations.takeFirst();

        knowCore::ValueHash vh = record.toValueHash().expect_success();
        cres_try(QCborValue value_data, vh.toCborValue(knowCore::SerialisationContexts()));
        QCborMap value;
        clog_assert(m_currentStep == 2);
        value["step"_kCs] = m_currentStep;
        value["data"_kCs] = value_data;

        return cres_success(value.toCborValue().toCbor());
      }
      if(not m_agent_positions.isEmpty())
      {
        kDBRobotics::AgentPositionRecord record = m_agent_positions.takeFirst();

        knowCore::ValueHash vh = record.toValueHash().expect_success();
        cres_try(QCborValue value_data, vh.toCborValue(knowCore::SerialisationContexts()));
        clog_assert(m_currentStep == 3);
        QCborMap value;
        value["step"_kCs] = m_currentStep;
        value["data"_kCs] = value_data;

        return cres_success(value.toCborValue().toCbor());
      }
      // move to the next step
      switch(m_currentStep)
      {
      case 0:
      {
        return cres_failure("Invalid iterator, should not handle step 0");
      }
      case 1:
      {
        m_currentStep = 2;
        m_transformations = ((kDBRobotics::FrameTransformationRecord::byTimestamp(
                                m_transaction, m_min_timestamp, kDBRobotics::Sql::Operand::GTE)
                              and kDBRobotics::FrameTransformationRecord::byTimestamp(
                                m_transaction, m_max_timestamp, kDBRobotics::Sql::Operand::LTE))
                             or (kDBRobotics::FrameTransformationRecord::byNextTimestamp(
                                   m_transaction, m_min_timestamp, kDBRobotics::Sql::Operand::GTE)
                                 and kDBRobotics::FrameTransformationRecord::byNextTimestamp(
                                   m_transaction, m_max_timestamp, kDBRobotics::Sql::Operand::LTE))
                             or (kDBRobotics::FrameTransformationRecord::byTimestamp(
                                   m_transaction, m_min_timestamp, kDBRobotics::Sql::Operand::LTE)
                                 and kDBRobotics::FrameTransformationRecord::byNextTimestamp(
                                   m_transaction, m_max_timestamp, kDBRobotics::Sql::Operand::GTE)))
                              .exec();
        return next();
      }
      case 2:
      {
        m_currentStep = 3;
        m_agent_positions
          = ((kDBRobotics::AgentPositionRecord::byTimestamp(m_transaction, m_min_timestamp,
                                                            kDBRobotics::Sql::Operand::GTE)
              and kDBRobotics::AgentPositionRecord::byTimestamp(m_transaction, m_max_timestamp,
                                                                kDBRobotics::Sql::Operand::LTE))
             or (kDBRobotics::AgentPositionRecord::byNextTimestamp(m_transaction, m_min_timestamp,
                                                                   kDBRobotics::Sql::Operand::GTE)
                 and kDBRobotics::AgentPositionRecord::byNextTimestamp(
                   m_transaction, m_max_timestamp, kDBRobotics::Sql::Operand::LTE))
             or (kDBRobotics::AgentPositionRecord::byTimestamp(m_transaction, m_min_timestamp,
                                                               kDBRobotics::Sql::Operand::LTE)
                 and kDBRobotics::AgentPositionRecord::byNextTimestamp(
                   m_transaction, m_max_timestamp, kDBRobotics::Sql::Operand::GTE)))
              .exec();
        return next();
      }
      case 3:
      {
        // We are DONE
        m_currentStep = 4;
        QCborMap m;
        m["step"_kCs] = m_currentStep;
        return cres_success(m.toCborValue().toCbor());
      }
      default:
        return cres_failure("Unknown step '{}'", m_currentStep);
      }
    }
  protected:
    kDB::Repository::Connection m_connection;
    kDBDatasets::Dataset m_dataset;
    kDB::Repository::Transaction m_transaction;
    knowCore::Timestamp m_min_timestamp;
    knowCore::Timestamp m_max_timestamp;
    int m_currentStep = 0;
    QList<kDBRobotics::FrameTransformationRecord> m_transformations;
    QList<kDBRobotics::AgentPositionRecord> m_agent_positions;
  };
  template<typename _T_>
  struct DefaultExtractIterator : public BaseExtractIterator
  {
    DefaultExtractIterator(const kDB::Repository::Connection& _connection,
                           const kDBDatasets::Dataset& _dataset)
        : BaseExtractIterator(_connection, _dataset)
    {
    }
    ~DefaultExtractIterator() { m_transaction.rollback(); }
    bool hasNextFrame() const override { return m_currentStep == 0 or not m_records.empty(); }
    virtual cres_qresult<void> handleExtra(const _T_& _record, QCborMap* _map)
    {
      Q_UNUSED(_record);
      Q_UNUSED(_map);
      return cres_success();
    }
    cres_qresult<QByteArray> nextFrame() override
    {
      // Send data accordingly
      if(not m_records.isEmpty())
      {
        _T_ record = m_records.takeFirst();

        knowCore::ValueHash vh = record.toValueHash().expect_success();
        cres_try(QCborValue value_data, vh.toCborValue(knowCore::SerialisationContexts()));
        QCborMap value;
        clog_assert(m_currentStep == 1);
        value["step"_kCs] = m_currentStep;
        value["data"_kCs] = value_data;
        cres_try(cres_ignore, handleExtra(record, &value));

        return cres_success(value.toCborValue().toCbor());
      }
      // move to the next step
      switch(m_currentStep)
      {
      case 0:
      {
        m_transaction = kDB::Repository::Transaction(m_connection);
        m_currentStep = 1;
        m_records = _T_::byDatasetUri(m_transaction, m_dataset.uri())
                      .orderByTimestamp(_T_::SqlSort::ASC)
                      .exec();
        if(m_records.empty())
        {
          return cres_failure("Missing data for dataset {}", m_dataset.uri());
        }
        else
        {
          m_min_timestamp = m_records.first().timestamp();
          m_max_timestamp = m_records.last().timestamp();
        }
        return next();
      }
      default:
        return cres_failure("Unknown step '{}'", m_currentStep);
      }
    }
    QList<_T_> m_records;
  };
  struct BaseInsertIterator : public kDBDatasets::Interfaces::InsertIterator
  {
    BaseInsertIterator(const kDB::Repository::Connection& _connection,
                       const kDBDatasets::Dataset& _dataset)
        : m_connection(_connection), m_dataset(_dataset)
    {
    }
    virtual cres_qresult<void> nextFrame(const QCborMap& _map) = 0;
    cres_qresult<void> next(const QByteArray& _msg_data) override
    {
      QCborValue value = QCborValue::fromCbor(_msg_data);
      QCborMap map = value.toMap();
      int step = map["step"_kCs].toInteger();
      if(step != m_currentStep)
      {
        ++m_currentStep;
        switch(m_currentStep)
        {
        case 1:
        {
          // Initialisation
          m_transaction = kDB::Repository::Transaction(m_connection);

          // Start step 1
          return next(_msg_data);
        }
        case 2:
        case 3:
          return next(_msg_data);
        case 4:
          return cres_success();
        default:
          return cres_failure("Unknown step '{}'", m_currentStep);
        }
      }
      else if(m_currentStep == 1)
      {
        return nextFrame(map);
      }
      else if(m_currentStep == 2)
      {
        cres_try(
          knowCore::ValueHash valueHash,
          knowCore::ValueHash::fromCborValue(map["data"_kCs], knowCore::DeserialisationContexts()));
        cres_try(kDBRobotics::FrameTransformationBaseValue value,
                 kDBRobotics::FrameTransformationBaseValue::fromValueHash(valueHash));
        if(not kDBRobotics::FrameTransformationRecord::has(m_transaction, value))
        {
          cres_try(cres_ignore, kDBRobotics::FrameTransformationRecord::create(
                                  m_transaction, value.parentFrame(), value.childFrame(),
                                  value.timestamp(), value.translation(), value.rotation()));
        }
        m_tf_min_timestamp = std::min(m_tf_min_timestamp, value.timestamp());
        m_tf_max_timestamp = std::max(m_tf_max_timestamp, value.timestamp());
        return cres_success();
      }
      else if(m_currentStep == 3)
      {
        cres_try(
          knowCore::ValueHash valueHash,
          knowCore::ValueHash::fromCborValue(map["data"_kCs], knowCore::DeserialisationContexts()));
        cres_try(kDBRobotics::AgentPositionBaseValue value,
                 kDBRobotics::AgentPositionBaseValue::fromValueHash(valueHash));
        if(not kDBRobotics::AgentPositionRecord::has(m_transaction, value))
        {
          cres_try(cres_ignore,
                   kDBRobotics::AgentPositionRecord::create(
                     m_transaction, value.name(), value.frame(), value.timestamp(),
                     value.longitude(), value.latitude(), value.altitude(), value.rotation()));
        }
        m_ap_min_timestamp = std::min(m_ap_min_timestamp, value.timestamp());
        m_ap_max_timestamp = std::max(m_ap_max_timestamp, value.timestamp());
        return cres_success();
      }
      else
      {
        return cres_failure("Invalid step {}", m_currentStep);
      }
    }
#define LIDAR_FINALISE_EXECUTE_QUERY(_MSG_, _QUERY_)                                               \
  {                                                                                                \
    auto result = _QUERY_.execute();                                                               \
    if(not result)                                                                                 \
    {                                                                                              \
      KDB_REPOSITORY_REPORT_QUERY_ERROR(_MSG_, result);                                            \
      m_transaction.rollback();                                                                    \
      return cres_failure(_MSG_);                                                                  \
    }                                                                                              \
  }

    cres_qresult<void> finalise() override
    {
      cres_try(cres_ignore, kDBRobotics::FrameTransformationRecord::updateNextInformation(
                              m_transaction, m_tf_min_timestamp, m_tf_max_timestamp));
      cres_try(cres_ignore, kDBRobotics::AgentPositionRecord::updateNextInformation(
                              m_transaction, m_ap_min_timestamp, m_ap_max_timestamp));

      return m_transaction.commit();
    }
    kDB::Repository::Connection m_connection;
    kDBDatasets::Dataset m_dataset;
    kDB::Repository::Transaction m_transaction;
    int m_currentStep = 0;
    knowCore::Timestamp m_tf_min_timestamp = knowCore::Timestamp::endOfTime(),
                        m_tf_max_timestamp = knowCore::Timestamp::originOfTime(),
                        m_ap_min_timestamp = knowCore::Timestamp::endOfTime(),
                        m_ap_max_timestamp = knowCore::Timestamp::originOfTime();
  };
  template<typename _TRecord_, typename _TValue_, typename _TBaseValue_ = _TValue_>
  struct DefaultInsertIterator : public BaseInsertIterator
  {
  public:
    DefaultInsertIterator(const kDB::Repository::Connection& _connection,
                          const kDBDatasets::Dataset& _dataset)
        : BaseInsertIterator(_connection, _dataset)
    {
    }
    virtual cres_qresult<_TValue_> createValue(const _TBaseValue_& _frame, const QCborMap& _data)
    {
      Q_UNUSED(_data);
      if constexpr(std::is_same_v<_TValue_, _TBaseValue_>)
      {
        return cres_success(_frame);
      }
      else
      {
        return cres_failure(
          "{} needs an overloaded implementation of DefaultInsertIterator::createValue",
          knowCore::prettyTypename<_TValue_>());
      }
    }
    cres_qresult<void> nextFrame(const QCborMap& _map) override
    {
      cres_try(
        knowCore::ValueHash valueHash,
        knowCore::ValueHash::fromCborValue(_map["data"_kCs], knowCore::DeserialisationContexts()));
      cres_try(_TBaseValue_ value, _TBaseValue_::fromValueHash(valueHash));
      cres_try(_TValue_ value_cfv, createValue(value, _map));
      cres_try(cres_ignore, _TRecord_::create(m_transaction, {value_cfv}));
      return cres_success();
    }
  };
  /**
   * Interface for Lidar
   */
  class LidarDataInterface : public kDBDatasets::Interfaces::DataInterface
  {
  private:
    struct ValueIterator : public kDBDatasets::Interfaces::ValueIterator
    {
      ValueIterator(const kDB::Repository::Connection& _connection,
                    const QList<LidarScanRecord>& _values)
          : m_framepose(_connection), m_values(_values)
      {
      }
      cres_qresult<knowCore::Value> next() override
      {
        LidarScanRecord lfr = m_values[m_next_index++];
        cres_try(knowGIS::GeoPose pose, m_framepose.frameGeoPose(lfr.frameId(), lfr.timestamp()));

        LidarConfigRecord lfr_config = lfr.config();
        clog_debug_vn(lfr.toValueHash(), lfr_config.toValueHash());

        knowValues::Values::LidarScan lidar
          = knowValues::Values::LidarScan::create()
              .setPose(pose)
              .setFrameId(lfr.frameId())
              .setTimestamp(lfr.timestamp())
              .setAngleMin(lfr_config.angleMin())
              .setAngleMax(lfr_config.angleMax())
              .setAngleIncrement(lfr_config.angleIncrement())
              .setTimeIncrement(lfr_config.timeIncrement())
              .setScanTime(lfr_config.scanTime())
              .setRangeMin(lfr_config.rangeMin())
              .setRangeMax(lfr_config.rangeMax())
              .setRanges(knowCore::vectorCast(lfr.ranges()))
              .setIntensities(knowCore::vectorCast(lfr.intensities()));

        return cres_success(knowCore::Value::fromValue(lidar));
      }
      bool hasNext() const override { return m_next_index < m_values.size(); }
      kDBRobotics::FramePose m_framepose;
      QList<LidarScanRecord> m_values;
      int m_next_index = 0;
    };
    struct ExtractIterator : public DefaultExtractIterator<kDBSensing::LidarScanRecord>
    {
      ExtractIterator(const kDB::Repository::Connection& _connection,
                      const kDBDatasets::Dataset& _dataset)
          : DefaultExtractIterator(_connection, _dataset)
      {
      }
      cres_qresult<void> handleExtra(const kDBSensing::LidarScanRecord& _record,
                                     QCborMap* _map) override
      {
        if(_record.config().isPersistent())
        {
          knowCore::ValueHash vh_info = _record.config().toValueHash().expect_success();
          cres_try(QCborValue value_data_info,
                   vh_info.toCborValue(knowCore::SerialisationContexts()));
          (*_map)["config"_kCs] = value_data_info;
        }
        return cres_success();
      }
    };
    struct InsertIterator
        : public DefaultInsertIterator<kDBSensing::LidarScanRecord, kDBSensing::LidarScanValue,
                                       kDBSensing::LidarScanBaseValue>
    {
      InsertIterator(const kDB::Repository::Connection& _connection,
                     const kDBDatasets::Dataset& _dataset)
          : DefaultInsertIterator(_connection, _dataset)
      {
      }
      cres_qresult<kDBSensing::LidarScanValue>
        createValue(const kDBSensing::LidarScanBaseValue& _frame, const QCborMap& _map) override
      {
        kDBSensing::LidarConfigRecord info_rec;
        clog_debug_vn(_map.toVariantHash());
        if(_map.contains("config"_kCs))
        {
          cres_try(knowCore::ValueHash valueHash_info,
                   knowCore::ValueHash::fromCborValue(_map["config"_kCs],
                                                      knowCore::DeserialisationContexts()));
          clog_debug_vn(valueHash_info);
          cres_try(kDBSensing::LidarConfigValue value_info,
                   kDBSensing::LidarConfigValue::fromValueHash(valueHash_info));
          QList<kDBSensing::LidarConfigRecord> configs
            = (kDBSensing::LidarConfigRecord::byAngleIncrement(m_transaction,
                                                               value_info.angleIncrement())
               and kDBSensing::LidarConfigRecord::byAngleMax(m_transaction, value_info.angleMax())
               and kDBSensing::LidarConfigRecord::byAngleMin(m_transaction, value_info.angleMin())
               and kDBSensing::LidarConfigRecord::byRangeMax(m_transaction, value_info.rangeMax())
               and kDBSensing::LidarConfigRecord::byRangeMin(m_transaction, value_info.rangeMin())
               and kDBSensing::LidarConfigRecord::byScanTime(m_transaction, value_info.scanTime())
               and kDBSensing::LidarConfigRecord::byTimeIncrement(m_transaction,
                                                                  value_info.timeIncrement()))
                .exec(1);

          if(configs.size() == 1)
          {
            info_rec = configs.first();
          }
          else
          {
            cres_try(QList<kDBSensing::LidarConfigRecord> info_recs,
                     kDBSensing::LidarConfigRecord::create(m_transaction, {value_info}));
            clog_debug_vn(info_recs.first().toValueHash());
            info_rec = info_recs.first();
          }
        }
        return cres_success(kDBSensing::LidarScanValue(m_transaction, _frame, info_rec));
      }
    };
  public:
    LidarDataInterface() {}
    ~LidarDataInterface() {}
  public:
    cres_qresult<kDBDatasets::Interfaces::ValueIterator*>
      createValueIterator(const kDB::Repository::Connection& _connection,
                          const kDBDatasets::Dataset& _dataset) const override
    {
      QList<LidarScanRecord> values
        = LidarScanRecord::byDatasetUri(_connection, _dataset.uri()).exec();
      if(values.size() == 0)
      {
        return cres_failure("Unknown dataset '{}'", _dataset.uri());
      }
      else
      {
        return cres_success(new ValueIterator(_connection, values));
      }
    }
    cres_qresult<kDBDatasets::Interfaces::ExtractIterator*>
      createExtractIterator(const kDB::Repository::Connection& _connection,
                            const kDBDatasets::Dataset& _dataset) const override
    {
      return cres_success(new ExtractIterator(_connection, _dataset));
    }
    cres_qresult<kDBDatasets::Interfaces::InsertIterator*>
      createInsertIterator(const kDB::Repository::Connection& _connection,
                           const kDBDatasets::Dataset& _dataset) const override
    {
      return cres_success(new InsertIterator(_connection, _dataset));
    }
    cres_qresult<kDBDatasets::Statistics>
      statistics(const kDB::Repository::Connection& _connection,
                 const kDBDatasets::Dataset& _dataset) const override
    {
      qsizetype count = LidarScanRecord::byDatasetUri(_connection, _dataset.uri()).count();
      if(count == 0)
        return cres_failure("Dataset {} is not local.", _dataset.uri());
      return cres_success(kDBDatasets::Statistics(_dataset, count));
    }
  };
  /**
   * Interface for PointCloud
   */
  class PointCloudDataInterface : public kDBDatasets::Interfaces::DataInterface
  {
  private:
    struct ValueIterator : public kDBDatasets::Interfaces::ValueIterator
    {
      ValueIterator(const kDB::Repository::Connection& _connection,
                    const QList<kDBPointClouds::PointCloudRecord>& _values)
          : m_framepose(_connection), m_values(_values)
      {
      }
      cres_qresult<knowCore::Value> next() override
      {
        kDBPointClouds::PointCloudRecord lfr = m_values[m_next_index++];

        kDBPointClouds::Patch patch = lfr.points();

        knowGIS::GeoPose pose
          = {{Cartography::Longitude(lfr.longitude()), Cartography::Latitude(lfr.latitude()),
              Cartography::Altitude(lfr.altitude())},
             {lfr.rotation()[0], lfr.rotation()[1], lfr.rotation()[2], lfr.rotation()[3]}};
        clog_assert(lfr.geometry().isValid());
        return cres_success(knowCore::Value::fromValue(
          patch.toValuesPointCloud(pose, lfr.geometry(), lfr.timestamp())));
      }
      bool hasNext() const override { return m_next_index < m_values.size(); }
      kDBRobotics::FramePose m_framepose;
      QList<kDBPointClouds::PointCloudRecord> m_values;
      int m_next_index = 0;
    };
  public:
    PointCloudDataInterface() {}
    ~PointCloudDataInterface() {}
  public:
    cres_qresult<kDBDatasets::Interfaces::ValueIterator*>
      createValueIterator(const kDB::Repository::Connection& _connection,
                          const kDBDatasets::Dataset& _dataset) const override
    {
      QList<kDBPointClouds::PointCloudRecord> values
        = kDBPointClouds::PointCloudRecord::byDatasetUri(_connection, _dataset.uri()).exec();
      if(values.size() == 0)
      {
        return cres_failure("Unknown dataset '{}' no values was found.", _dataset.uri());
      }
      else
      {
        return cres_success(new ValueIterator(_connection, values));
      }
    }
    cres_qresult<kDBDatasets::Interfaces::ExtractIterator*>
      createExtractIterator(const kDB::Repository::Connection& _connection,
                            const kDBDatasets::Dataset& _dataset) const override
    {
      return cres_success(
        new DefaultExtractIterator<kDBPointClouds::PointCloudRecord>(_connection, _dataset));
    }
    cres_qresult<kDBDatasets::Interfaces::InsertIterator*>
      createInsertIterator(const kDB::Repository::Connection& _connection,
                           const kDBDatasets::Dataset& _dataset) const override
    {
      return cres_success(
        new DefaultInsertIterator<kDBPointClouds::PointCloudRecord,
                                  kDBPointClouds::PointCloudValue>(_connection, _dataset));
    }
    cres_qresult<kDBDatasets::Statistics>
      statistics(const kDB::Repository::Connection& _connection,
                 const kDBDatasets::Dataset& _dataset) const override
    {
      qsizetype count
        = kDBPointClouds::PointCloudRecord::byDatasetUri(_connection, _dataset.uri()).count();
      if(count == 0)
        return cres_failure("Dataset {} is not local.", _dataset.uri());
      return cres_success(kDBDatasets::Statistics(_dataset, count));
    }
  };
  /**
   * Interface for Lidar 3D
   */
  class Lidar3DDataInterface : public kDBDatasets::Interfaces::DataInterface
  {
  private:
    struct ValueIterator : public kDBDatasets::Interfaces::ValueIterator
    {
      ValueIterator(const kDB::Repository::Connection& _connection,
                    const QList<Lidar3dScanRecord>& _values)
          : m_framepose(_connection), m_values(_values)
      {
      }
      cres_qresult<knowCore::Value> next() override
      {
        Lidar3dScanRecord lfr = m_values[m_next_index++];
        cres_try(knowGIS::GeoPose pose, m_framepose.frameGeoPose(lfr.frameId(), lfr.timestamp()));

        kDBPointClouds::Patch patch = lfr.points();

        return cres_success(
          knowCore::Value::fromValue(patch.toValuesLidar3DScan(pose, lfr.timestamp())));
      }
      bool hasNext() const override { return m_next_index < m_values.size(); }
      kDBRobotics::FramePose m_framepose;
      QList<Lidar3dScanRecord> m_values;
      int m_next_index = 0;
    };
  public:
    Lidar3DDataInterface() {}
    ~Lidar3DDataInterface() {}
  public:
    cres_qresult<kDBDatasets::Interfaces::ValueIterator*>
      createValueIterator(const kDB::Repository::Connection& _connection,
                          const kDBDatasets::Dataset& _dataset) const override
    {
      QList<Lidar3dScanRecord> values
        = Lidar3dScanRecord::byDatasetUri(_connection, _dataset.uri()).exec();
      if(values.size() == 0)
      {
        return cres_failure("Unknown dataset '{}'", _dataset.uri());
      }
      else
      {
        return cres_success(new ValueIterator(_connection, values));
      }
    }
    cres_qresult<kDBDatasets::Interfaces::ExtractIterator*>
      createExtractIterator(const kDB::Repository::Connection& _connection,
                            const kDBDatasets::Dataset& _dataset) const override
    {
      return cres_success(new DefaultExtractIterator<Lidar3dScanRecord>(_connection, _dataset));
    }
    cres_qresult<kDBDatasets::Interfaces::InsertIterator*>
      createInsertIterator(const kDB::Repository::Connection& _connection,
                           const kDBDatasets::Dataset& _dataset) const override
    {
      return cres_success(
        new DefaultInsertIterator<Lidar3dScanRecord, Lidar3dScanValue>(_connection, _dataset));
    }
    cres_qresult<kDBDatasets::Statistics>
      statistics(const kDB::Repository::Connection& _connection,
                 const kDBDatasets::Dataset& _dataset) const override
    {
      qsizetype count = Lidar3dScanRecord::byDatasetUri(_connection, _dataset.uri()).count();
      if(count == 0)
        return cres_failure("Dataset {} is not local.", _dataset.uri());
      return cres_success(kDBDatasets::Statistics(_dataset, count));
    }
  };
  /**
   * Interface for images
   */
  class ImageDataInterface : public kDBDatasets::Interfaces::DataInterface
  {
  private:
    struct ValueIterator : public kDBDatasets::Interfaces::ValueIterator
    {
      ValueIterator(const kDB::Repository::Connection& _connection,
                    const QList<kDBSensing::CameraFrameRecord>& _values)
          : m_framepose(_connection), m_values(_values)
      {
      }
      cres_qresult<knowCore::Value> next() override
      {
        kDBSensing::CameraFrameRecord lfr = m_values[m_next_index++];
        kDBSensing::CameraInfoValue civ = lfr.info();

        cres_try(knowCore::Image img, lfr.toImage());
        cres_try(knowGIS::GeoPose pose, m_framepose.frameGeoPose(lfr.frameId(), lfr.timestamp()));

        return cres_success(knowCore::Value::fromValue(
          knowValues::Values::Image(knowValues::Values::Image::create()
                                      .setPose(pose)
                                      .setFrameId(lfr.frameId())
                                      .setTimestamp(lfr.timestamp())
                                      .setImage(img)
                                      .setCameraInfo(knowValues::Values::CameraInfo::create()
                                                       .setWidth(civ.width())
                                                       .setHeight(civ.height())
                                                       .setDistortionModel(civ.distortionModel())
                                                       .setDistortion(civ.distortion())
                                                       .setProjection(civ.projection())))));
      }
      bool hasNext() const override { return m_next_index < m_values.size(); }
      kDBRobotics::FramePose m_framepose;
      QList<kDBSensing::CameraFrameRecord> m_values;
      int m_next_index = 0;
    };
    struct ExtractIterator : public DefaultExtractIterator<kDBSensing::CameraFrameRecord>
    {
      ExtractIterator(const kDB::Repository::Connection& _connection,
                      const kDBDatasets::Dataset& _dataset)
          : DefaultExtractIterator(_connection, _dataset)
      {
      }
      cres_qresult<void> handleExtra(const kDBSensing::CameraFrameRecord& _record,
                                     QCborMap* _map) override
      {
        if(_record.info().isPersistent())
        {
          knowCore::ValueHash vh_info = _record.info().toValueHash().expect_success();
          cres_try(QCborValue value_data_info,
                   vh_info.toCborValue(knowCore::SerialisationContexts()));
          (*_map)["data_info"_kCs] = value_data_info;
        }
        return cres_success();
      }
    };
    struct InsertIterator
        : public DefaultInsertIterator<kDBSensing::CameraFrameRecord, kDBSensing::CameraFrameValue,
                                       kDBSensing::CameraFrameBaseValue>
    {
      InsertIterator(const kDB::Repository::Connection& _connection,
                     const kDBDatasets::Dataset& _dataset)
          : DefaultInsertIterator(_connection, _dataset)
      {
      }
      cres_qresult<kDBSensing::CameraFrameValue>
        createValue(const kDBSensing::CameraFrameBaseValue& _frame, const QCborMap& _map) override
      {
        kDBSensing::CameraInfoRecord info_rec;
        if(_map.contains("data_info"_kCs))
        {
          cres_try(knowCore::ValueHash valueHash_info,
                   knowCore::ValueHash::fromCborValue(_map["data_info"_kCs],
                                                      knowCore::DeserialisationContexts()));
          cres_try(kDBSensing::CameraInfoValue value_info,
                   kDBSensing::CameraInfoValue::fromValueHash(valueHash_info));
          QList<kDBSensing::CameraInfoRecord> configs
            = (kDBSensing::CameraInfoRecord::byDistortion(m_transaction, value_info.distortion())
               and kDBSensing::CameraInfoRecord::byProjection(m_transaction,
                                                              value_info.projection())
               and kDBSensing::CameraInfoRecord::byDistortionModel(m_transaction,
                                                                   value_info.distortionModel())
               and kDBSensing::CameraInfoRecord::byWidth(m_transaction, value_info.width())
               and kDBSensing::CameraInfoRecord::byHeight(m_transaction, value_info.height()))
                .exec(1);

          if(configs.size() == 1)
          {
            info_rec = configs.first();
          }
          else
          {
            cres_try(info_rec, kDBSensing::CameraInfoRecord::create(
                                 m_transaction, value_info.width(), value_info.height(),
                                 value_info.distortionModel(), value_info.distortion(),
                                 value_info.projection()));
          }
        }
        return cres_success(kDBSensing::CameraFrameValue(m_transaction, _frame, info_rec));
      }
    };
  public:
    ImageDataInterface() {}
    ~ImageDataInterface() {}
  public:
    cres_qresult<kDBDatasets::Interfaces::ValueIterator*>
      createValueIterator(const kDB::Repository::Connection& _connection,
                          const kDBDatasets::Dataset& _dataset) const override
    {
      QList<kDBSensing::CameraFrameRecord> values
        = kDBSensing::CameraFrameRecord::byDatasetUri(_connection, _dataset.uri()).exec();
      if(values.size() == 0)
      {
        return cres_failure("Unknown dataset '{}' no values was found.", _dataset.uri());
      }
      else
      {
        return cres_success(new ValueIterator(_connection, values));
      }
    }
    cres_qresult<kDBDatasets::Interfaces::ExtractIterator*>
      createExtractIterator(const kDB::Repository::Connection& _connection,
                            const kDBDatasets::Dataset& _dataset) const override
    {
      return cres_success(new ExtractIterator(_connection, _dataset));
    }
    cres_qresult<kDBDatasets::Interfaces::InsertIterator*>
      createInsertIterator(const kDB::Repository::Connection& _connection,
                           const kDBDatasets::Dataset& _dataset) const override
    {
      return cres_success(new InsertIterator(_connection, _dataset));
    }
    cres_qresult<kDBDatasets::Statistics>
      statistics(const kDB::Repository::Connection& _connection,
                 const kDBDatasets::Dataset& _dataset) const override
    {
      qsizetype count = CameraFrameRecord::byDatasetUri(_connection, _dataset.uri()).count();
      if(count == 0)
        return cres_failure("Dataset {} is not local.", _dataset.uri());
      return cres_success(kDBDatasets::Statistics(_dataset, count));
    }
  };
  /**
   * Interface for Salient Point
   */
  class SalientRegionDataInterface : public kDBDatasets::Interfaces::DataInterface
  {
    struct ValueIterator : public kDBDatasets::Interfaces::ValueIterator
    {
      ValueIterator(const QList<knowCore::Value>& _values) : m_values(_values) {}
      cres_qresult<knowCore::Value> next() override
      {
        return cres_success(m_values[m_next_index++]);
      }
      bool hasNext() const override { return m_next_index < m_values.size(); }
      QList<knowCore::Value> m_values;
      int m_next_index = 0;
    };
    struct ExtractIterator : public kDBDatasets::Interfaces::ExtractIterator
    {
      ExtractIterator(const QList<knowRDF::Triple>& _triples) : m_triples(_triples) {}
      ~ExtractIterator() {}
      bool hasNext() const override { return m_currentIndex < m_triples.size(); }
      cres_qresult<QByteArray> next() override
      {
        QCborArray triples;

        for(int end = std::min<qsizetype>(m_currentIndex + 50, m_triples.size());
            m_currentIndex < end; ++m_currentIndex)
        {
          cres_try(
            QCborValue cbor_value,
            knowCore::MetaTypeInformation<knowRDF::Triple>::toCborValue(m_triples[m_currentIndex]));
          triples.append(cbor_value);
        }
        return cres_success(triples.toCborValue().toByteArray());
      }
      QList<knowRDF::Triple> m_triples;
      int m_currentIndex = 0;
    };
    struct InsertIterator : public kDBDatasets::Interfaces::InsertIterator
    {
      InsertIterator(const kDB::Repository::TripleStore& _triplesStore)
          : m_triplesStore(_triplesStore)
      {
      }
      cres_qresult<void> next(const QByteArray& _msg_data) override
      {
        QCborParserError err;
        QCborValue value = QCborValue::fromCbor(_msg_data, &err);
        if(value.isNull())
        {
          return cres_failure("Failed to parse cbor: {}", err.errorString());
        }
        else
        {
          QCborArray arr = value.toArray();
          QList<knowRDF::Triple> triples;
          for(int i = 0; i < arr.size(); ++i)
          {
            cres_try(knowRDF::Triple triple,
                     knowCore::MetaTypeInformation<knowRDF::Triple>::fromCborValue(arr[i]));
            triples.append(triple);
          }
          return m_triplesStore.insert(triples);
        }
      }

      cres_qresult<void> finalise() override { return cres_success(); }
      kDB::Repository::TripleStore m_triplesStore;
    };
  public:
    SalientRegionDataInterface() {}
    ~SalientRegionDataInterface() {}
  public:
    cres_qresult<kDBDatasets::Interfaces::ValueIterator*>
      createValueIterator(const kDB::Repository::Connection& _connection,
                          const kDBDatasets::Dataset& _dataset) const override
    {

      cres_try(kDBSensing::SalientRegions::Collection col,
               kDBSensing::SalientRegions::Collection::get(_connection, _dataset.uri()));

      if(col.isValid())
      {
        QList<knowCore::Value> values;
        cres_try(QList<kDBSensing::SalientRegions::SalientRegion> regions, col.all());
        for(const kDBSensing::SalientRegions::SalientRegion& sr : regions)
        {
          cres_try(knowValues::Values::SalientRegion srv, sr.toSalientRegion());
          values.append(knowCore::Value::fromValue(srv));
        }

        return cres_success(new ValueIterator(values));
      }
      else
      {
        return cres_failure("Got an invalid collection for {}.", _dataset.uri());
      }
    }
    cres_qresult<kDBDatasets::Interfaces::ExtractIterator*>
      createExtractIterator(const kDB::Repository::Connection& _connection,
                            const kDBDatasets::Dataset& _dataset) const override
    {
      cres_try(kDB::Repository::TripleStore ts,
               _connection.graphsManager()->getTripleStore(_dataset.uri()));

      if(ts.isValid())
      {
        cres_try(QList<knowRDF::Triple> triples, ts.triples());
        return cres_success(new ExtractIterator(triples));
      }
      else
      {
        return cres_failure("Unknown dataset '{}'", _dataset.uri());
      }
    }
    cres_qresult<kDBDatasets::Interfaces::InsertIterator*>
      createInsertIterator(const kDB::Repository::Connection& _connection,
                           const kDBDatasets::Dataset& _dataset) const override
    {
      if(_connection.graphsManager()->hasTripleStore(_dataset.uri()))
      {
        return cres_failure("There is already a triples store called {}", _dataset.uri());
      }
      cres_try(kDB::Repository::TripleStore ts,
               _connection.graphsManager()->createTripleStore(_dataset.uri()));
      return cres_success(new InsertIterator(ts));
    }
    cres_qresult<kDBDatasets::Statistics>
      statistics(const kDB::Repository::Connection& _connection,
                 const kDBDatasets::Dataset& _dataset) const override
    {
      cres_try(kDBSensing::SalientRegions::Collection col,
               kDBSensing::SalientRegions::Collection::get(_connection, _dataset.uri()));

      if(col.isValid())
      {
        cres_try(qsizetype count, col.count());
        return cres_success(kDBDatasets::Statistics(_dataset, count));
      }
      else
      {
        return cres_failure("Got an invalid collection for {}.", _dataset.uri());
      }
    }
  };
} // namespace kDBSensing

KDB_REGISTER_DATASET_DATA_INTERFACE(knowValues::Uris::askcore_sensing::lidar_scan,
                                    kDBSensing::LidarDataInterface)
KDB_REGISTER_DATASET_DATA_INTERFACE(knowValues::Uris::askcore_sensing::lidar3d_scan,
                                    kDBSensing::Lidar3DDataInterface)
KDB_REGISTER_DATASET_DATA_INTERFACE(knowValues::Uris::askcore_sensing::point_cloud,
                                    kDBSensing::PointCloudDataInterface)
KDB_REGISTER_DATASET_DATA_INTERFACE(knowValues::Uris::askcore_sensing::image_frame,
                                    kDBSensing::ImageDataInterface)
KDB_REGISTER_DATASET_DATA_INTERFACE(knowValues::Uris::askcore_sensing::salient_region,
                                    kDBSensing::SalientRegionDataInterface)
