#include <kDB/Repository/krQuery/Interfaces/Action.h>

#include <kDB/Forward.h>

namespace kDBSensing::krQueryActions
{
  /**
   * Define a krQL Query that reacts to "salient region" to allow adding, removing or updating
   * salient regions.
   *
   * For instance:
   * ```yaml
   * salient region:
   *   action: increment property
   *   dataset: some_pois
   *   uri: poi_uri
   *   property: some_property
   * ```
   */
  class SalientRegion : public kDB::Repository::krQuery::Interfaces::Action
  {
  public:
    SalientRegion();
    virtual ~SalientRegion();
    cres_qresult<knowCore::Value> execute(const kDB::Repository::krQuery::Context& _context,
                                          const QString& _key,
                                          const knowCore::ValueHash& _parameters) override;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBSensing::krQueryActions
