#include "TestLidar.h"

#include <knowCore/Cast.h>
#include <knowCore/Test.h>
#include <knowValues/Values.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryStore.h>

#include <kDBGIS/Test.h>
#include <knowGIS/GeometryObject.h>

#include <kDBRobotics/AgentPosition.h>
#include <kDBRobotics/Test.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>

#include <kDBSensing/LidarConfig.h>
#include <kDBSensing/LidarScan.h>

#include <knowCore/Uris/askcore_unit.h>
#include <knowValues/Uris/askcore_sensing.h>

using askcore_sensing = knowValues::Uris::askcore_sensing;
using askcore_unit = knowCore::Uris::askcore_unit;

void TestLidar::testConfigCreationAccess()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBSensing"));

  kDBSensing::LidarConfigRecord lidarConfig
    = CRES_QVERIFY(kDBSensing::LidarConfigRecord::create(c, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0));
  QVERIFY(lidarConfig.isPersistent());

  QList<kDBSensing::LidarConfigRecord> config_outs = kDBSensing::LidarConfigRecord::all(c).exec();
  QCOMPARE(config_outs.size(), 1);
  kDBSensing::LidarConfigRecord config_out = config_outs.first();

  QCOMPARE(config_out.id(), lidarConfig.id());
  QCOMPARE(config_out.angleMin(), 1.0);
  QCOMPARE(config_out.angleMax(), 2.0);
  QCOMPARE(config_out.angleIncrement(), 3.0);
  QCOMPARE(config_out.timeIncrement(), 4.0);
  QCOMPARE(config_out.scanTime(), 5.0);
  QCOMPARE(config_out.rangeMin(), 6.0);
  QCOMPARE(config_out.rangeMax(), 7.0);
}

void TestLidar::testFrameCreationAccess()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBSensing"));

  kDBSensing::LidarConfigRecord lidarConfig
    = CRES_QVERIFY(kDBSensing::LidarConfigRecord::create(c, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0));
  QVERIFY(lidarConfig.isPersistent());

  kDBSensing::LidarScanRecord frame = CRES_QVERIFY(
    kDBSensing::LidarScanRecord::create(c, "sensor_uri", "dataset_uri", "frame_name", 12_kCtns,
                                        lidarConfig, {1.0, 2.0, 3.0}, {4.0, 2.0, 1.0}));
  QVERIFY(frame.isPersistent());

  QList<kDBSensing::LidarScanRecord> frame_outs = kDBSensing::LidarScanRecord::all(c).exec();
  QCOMPARE(frame_outs.size(), 1);
  kDBSensing::LidarScanRecord frame_out = frame_outs.first();

  QCOMPARE(frame_out.id(), frame.id());
  QCOMPARE(frame_out.frameId(), QStringLiteral("frame_name"));
  QCOMPARE(frame_out.timestamp(), 12_kCtns);
  QCOMPARE(frame_out.config().id(), lidarConfig.id());
  QCOMPARE(frame_out.ranges().size(), 3);
  QCOMPARE(frame_out.ranges()[0], 1.0);
  QCOMPARE(frame_out.ranges()[1], 2.0);
  QCOMPARE(frame_out.ranges()[2], 3.0);
  QCOMPARE(frame_out.intensities().size(), 3);
  QCOMPARE(frame_out.intensities()[0], 4.0);
  QCOMPARE(frame_out.intensities()[1], 2.0);
  QCOMPARE(frame_out.intensities()[2], 1.0);
}

void TestLidar::testFrameFromValueCreationAccess()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBSensing"));

  kDBSensing::LidarConfigRecord lidarConfig
    = CRES_QVERIFY(kDBSensing::LidarConfigRecord::create(
                     c, {kDBSensing::LidarConfigValue(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0)}))
        .first();
  QVERIFY(lidarConfig.isPersistent());

  kDBSensing::LidarScanValue lidarvalue(c, "sensor_uri", "dataset_uri", "frame_name", 12_kCtns,
                                        lidarConfig, {1.0, 2.0, 3.0}, {4.0, 2.0, 1.0});

  kDBSensing::LidarScanRecord frame
    = CRES_QVERIFY(kDBSensing::LidarScanRecord::create(c, {lidarvalue})).first();
  QVERIFY(frame.isPersistent());

  QList<kDBSensing::LidarScanRecord> frame_outs = kDBSensing::LidarScanRecord::all(c).exec();
  QCOMPARE(frame_outs.size(), 1);
  kDBSensing::LidarScanRecord frame_out = frame_outs.first();

  QCOMPARE(frame_out.id(), frame.id());
  QCOMPARE(frame_out.frameId(), QStringLiteral("frame_name"));
  QCOMPARE(frame_out.timestamp(), 12_kCtns);
  QCOMPARE(frame_out.config().id(), lidarConfig.id());
  QCOMPARE(frame_out.ranges().size(), 3);
  QCOMPARE(frame_out.ranges()[0], 1.0);
  QCOMPARE(frame_out.ranges()[1], 2.0);
  QCOMPARE(frame_out.ranges()[2], 3.0);
  QCOMPARE(frame_out.intensities().size(), 3);
  QCOMPARE(frame_out.intensities()[0], 4.0);
  QCOMPARE(frame_out.intensities()[1], 2.0);
  QCOMPARE(frame_out.intensities()[2], 1.0);
}

void TestLidar::testDataset()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBSensing"));

  kDBSensing::LidarConfigRecord lidarConfig
    = CRES_QVERIFY(kDBSensing::LidarConfigRecord::create(c, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0));
  QVERIFY(lidarConfig.isPersistent());

  kDBSensing::LidarScanRecord frame = CRES_QVERIFY(
    kDBSensing::LidarScanRecord::create(c, "sensor_uri", "dataset1"_kCu, "frame_name", 12_kCtns,
                                        lidarConfig, {1.0, 2.0, 3.0}, {4.0, 2.0, 1.0}));
  QVERIFY(frame.isPersistent());

  kDBRobotics::AgentPositionRecord position = CRES_QVERIFY(kDBRobotics::AgentPositionRecord::create(
    c, "agent0", "frame_name", 12_kCtns, 0, 1, 2, {3, 4, 5, 6}, {}));

  kDBDatasets::Collection dss1 = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c, "dss1"_kCu));
  QCOMPARE(dss1.isValid(), true);

  knowGIS::GeometryObject go0 = CRES_QVERIFY(
    knowGIS::GeometryObject::fromWKT("POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))"));

  kDBDatasets::Dataset ds1 = CRES_QVERIFY(dss1.createDataset(
    askcore_sensing::lidar_scan, go0,
    {askcore_sensing::point_density,
     knowCore::QuantityNumber::create(100, askcore_unit::point_per_m2).expect_success()},
    "dataset1"_kCu));

  {
    kDBDatasets::ValueIterator ds1_it
      = CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::createValueIterator(c, ds1));
    QVERIFY(ds1_it.hasNext());
    knowCore::Value val = CRES_QVERIFY(ds1_it.next());
    knowValues::Values::LidarScan lf = CRES_QVERIFY(val.value<knowValues::Values::LidarScan>());
    QCOMPARE(lf->angleMin(), lidarConfig.angleMin());
    QCOMPARE(lf->angleMax(), lidarConfig.angleMax());
    QCOMPARE(lf->ranges(), knowCore::vectorCast(frame.ranges()));
    QCOMPARE(lf->intensities(), knowCore::vectorCast(frame.intensities()));
    QCOMPARE(lf->timestamp().toEpoch<knowCore::NanoSeconds>().count(), 12);
    knowGIS::GeoPose geoPose(knowGIS::GeoPoint(Cartography::Longitude(0), Cartography::Latitude(1),
                                               Cartography::Altitude(2)),
                             knowGIS::Quaternion(3, 4, 5, 6));
    QCOMPARE((double)lf->pose().position().longitude(), 0.0);
    QCOMPARE((double)lf->pose().position().latitude(), 1.0);
    QCOMPARE(lf->pose().position().altitude(), 2.0);
    QCOMPARE(lf->pose().position().coordinateSystem(), geoPose.position().coordinateSystem());
    QCOMPARE(lf->pose().orientation(), geoPose.orientation());
  }

  // Test Extract/Insert iterators
  {
    // Create a second store to load the data
    kDB::Repository::TemporaryStore store3;
    CRES_QVERIFY(store3.start());
    kDB::Repository::Connection c3 = store3.createConnection();
    CRES_QVERIFY(c3.connect());

    CRES_QVERIFY(c3.enableExtension("kDBSensing"));

    kDBDatasets::Collection dss1_3
      = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c, "dss1"_kCu));
    QCOMPARE(dss1_3.isValid(), true);
    CRES_QVERIFY(dss1_3.insertDatasetFromCbor(CRES_QVERIFY(ds1.toCborMap())));

    kDBDatasets::Dataset ds1_3 = CRES_QVERIFY(dss1.dataset(ds1.uri()));

    kDBDatasets::ExtractIterator ds1_extract_it
      = CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::createExtractIterator(c, ds1));
    kDBDatasets::InsertIterator ds1_insert_it
      = CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::createInsertIterator(c3, ds1_3));

    while(ds1_extract_it.hasNext())
    {
      QByteArray data = CRES_QVERIFY(ds1_extract_it.next());
      CRES_QVERIFY(ds1_insert_it.next(data));
    }
    CRES_QVERIFY(ds1_insert_it.finalise());

    // Test for correctness
    {
      kDBDatasets::ValueIterator ds1_it3
        = CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::createValueIterator(c3, ds1));
      QVERIFY(ds1_it3.hasNext());
      knowCore::Value val = CRES_QVERIFY(ds1_it3.next());
      knowValues::Values::LidarScan lf = CRES_QVERIFY(val.value<knowValues::Values::LidarScan>());
      QCOMPARE(lf->angleMin(), lidarConfig.angleMin());
      QCOMPARE(lf->angleMax(), lidarConfig.angleMax());
      QCOMPARE(lf->ranges(), knowCore::vectorCast(frame.ranges()));
      QCOMPARE(lf->intensities(), knowCore::vectorCast(frame.intensities()));
      QCOMPARE(lf->timestamp().toEpoch<knowCore::NanoSeconds>().count(), 12);
      knowGIS::GeoPose geoPose(knowGIS::GeoPoint(Cartography::Longitude(0),
                                                 Cartography::Latitude(1),
                                                 Cartography::Altitude(2)),
                               knowGIS::Quaternion(3, 4, 5, 6));
      QCOMPARE((double)lf->pose().position().longitude(), 0.0);
      QCOMPARE((double)lf->pose().position().latitude(), 1.0);
      QCOMPARE(lf->pose().position().altitude(), 2.0);
      QCOMPARE(lf->pose().position().coordinateSystem(), geoPose.position().coordinateSystem());
      QCOMPARE(lf->pose().orientation(), geoPose.orientation());
    }
  }
  // Test saving the file
  {
    QTemporaryFile tmpFile;
    QVERIFY(tmpFile.open());

    CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::exportTo(c, ds1, &tmpFile));

    tmpFile.seek(0);

    // Create a second store to load the data
    kDB::Repository::TemporaryStore store2;
    CRES_QVERIFY(store2.start());
    kDB::Repository::Connection c2 = store2.createConnection();
    CRES_QVERIFY(c2.connect());

    CRES_QVERIFY(c2.enableExtension("kDBSensing"));

    kDBDatasets::Collection dss1_2
      = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c2, "dss1"_kCu));
    QCOMPARE(dss1_2.isValid(), true);

    // Import the data in the second store
    CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::importFrom(c2, {dss1_2}, &tmpFile));

    // Test for correctness
    {
      kDBDatasets::ValueIterator ds1_it2
        = CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::createValueIterator(c2, ds1));
      QVERIFY(ds1_it2.hasNext());
      knowCore::Value val = CRES_QVERIFY(ds1_it2.next());
      knowValues::Values::LidarScan lf = CRES_QVERIFY(val.value<knowValues::Values::LidarScan>());
      QCOMPARE(lf->angleMin(), lidarConfig.angleMin());
      QCOMPARE(lf->angleMax(), lidarConfig.angleMax());
      QCOMPARE(lf->ranges(), knowCore::vectorCast(frame.ranges()));
      QCOMPARE(lf->intensities(), knowCore::vectorCast(frame.intensities()));
      QCOMPARE(lf->timestamp().toEpoch<knowCore::NanoSeconds>().count(), 12);
      knowGIS::GeoPose geoPose(knowGIS::GeoPoint(Cartography::Longitude(0),
                                                 Cartography::Latitude(1),
                                                 Cartography::Altitude(2)),
                               knowGIS::Quaternion(3, 4, 5, 6));
      QCOMPARE((double)lf->pose().position().longitude(), 0.0);
      QCOMPARE((double)lf->pose().position().latitude(), 1.0);
      QCOMPARE(lf->pose().position().altitude(), 2.0);
      QCOMPARE(lf->pose().position().coordinateSystem(), geoPose.position().coordinateSystem());
      QCOMPARE(lf->pose().orientation(), geoPose.orientation());
    }
  }
}

QTEST_MAIN(TestLidar)
