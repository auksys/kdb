#include "TestLidar3d.h"

#include <Cartography/CoordinateSystem.h>

#include <knowCore/Test.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/Store.h>
#include <kDB/Repository/TemporaryStore.h>

#include <kDBPointClouds/PointSpecification.h>

#include <kDBSensing/Lidar3dScan.h>

void TestLidar3d::testFrameCreationAccess()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBSensing"));

  kDBPointClouds::PointSpecification ps(1, Cartography::CoordinateSystem::wgs84);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float32, "X", "X dimension", 1.0);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float32, "Y", "Y dimension", 1.0);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float32, "Z", "Z dimension", 1.0);
  ps.addDimension(kDBPointClouds::PointSpecification::DataType::Float32, "I", "I dimension", 1.0);
  ps.save(c);

  kDBPointClouds::Patch patch(ps);
  patch.append(1.0f, 2.0f, 3.0f, 4.0f);
  patch.append(5.0f, 6.0f, 7.0f, 8.0f);

  kDBSensing::Lidar3dScanRecord frame = CRES_QVERIFY(kDBSensing::Lidar3dScanRecord::create(
    c, "some_uri", "dataset_uri", "frame_name", 12_kCtns, patch));
  QCOMPARE(frame.points(), patch);
  QVERIFY(frame.isPersistent());

  QList<kDBSensing::Lidar3dScanRecord> frame_outs = kDBSensing::Lidar3dScanRecord::all(c).exec();
  QCOMPARE(frame_outs.size(), 1);
  kDBSensing::Lidar3dScanRecord frame_out = frame_outs.first();
  QCOMPARE(frame_out.id(), frame.id());
  QCOMPARE(frame_out.frameId(), QStringLiteral("frame_name"));
  QCOMPARE(frame_out.timestamp(), 12_kCtns);
  QCOMPARE(frame_out.points().pointsCount(), std::size_t{2});
  for(int k = 0; k < 2; ++k)
  {
    QCOMPARE(frame_out.points().get(k), patch.get(k));
  }
}

QTEST_MAIN(TestLidar3d)
