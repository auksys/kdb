#include <QtTest/QtTest>

class TestLidar : public QObject
{
  Q_OBJECT
private slots:
  void testConfigCreationAccess();
  void testFrameCreationAccess();
  void testFrameFromValueCreationAccess();
  void testDataset();
};
