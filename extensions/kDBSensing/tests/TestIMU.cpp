#include "TestIMU.h"

#include <knowCore/Test.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryStore.h>

#include <kDBSensing/ImuFrame.h>

void TestIMU::testCreationAccess()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBSensing"));

  kDBSensing::ImuFrameRecord frame = CRES_QVERIFY(kDBSensing::ImuFrameRecord::create(
    c, "sensor_uri", "dataset_uri", "frame_name", 3_kCtns, knowCore::Vector3d(4.0, 5.0, 6.0),
    knowCore::Vector3d(7.0, 8.0, 9.0), knowCore::Vector4d(10.0, 11.0, 12.0, 13.0)));
  QVERIFY(frame.isPersistent());

  QList<kDBSensing::ImuFrameRecord> frame_outs = kDBSensing::ImuFrameRecord::all(c).exec();
  QCOMPARE(frame_outs.size(), 1);
  kDBSensing::ImuFrameRecord frame_out = frame_outs.first();
  QCOMPARE(frame_out.frameId(), QStringLiteral("frame_name"));
  QCOMPARE(frame_out.timestamp(), 3_kCtns);
  QCOMPARE(frame_out.angularVelocity()[0], 4.0);
  QCOMPARE(frame_out.angularVelocity()[1], 5.0);
  QCOMPARE(frame_out.angularVelocity()[2], 6.0);
  QCOMPARE(frame_out.linearAcceleration()[0], 7.0);
  QCOMPARE(frame_out.linearAcceleration()[1], 8.0);
  QCOMPARE(frame_out.linearAcceleration()[2], 9.0);
  QCOMPARE(frame_out.orientation()[0], 10.0);
  QCOMPARE(frame_out.orientation()[1], 11.0);
  QCOMPARE(frame_out.orientation()[2], 12.0);
  QCOMPARE(frame_out.orientation()[3], 13.0);
}

QTEST_MAIN(TestIMU)
