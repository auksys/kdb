#include "TestTestDatasets.h"

#include <knowValues/Values.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryStore.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>

#include <kDBRobotics/AgentPosition.h>

#include <kDBDatasets/Test.h>
#include <kDBSensing/Test.h>
#include <knowCore/Test.h>

void TestTestDatasets::testLidar()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());
  CRES_QVERIFY(c.enableExtension("kDBSensing"));

  knowCore::Timestamp startStamp = 1000_kCtns;

  kDBDatasets::Collection dss = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c, "dss"_kCu));

  // Area A
  knowGIS::GeometryObject go_rA = kDBDatasets::Test::createRegionA();
  kDBDatasets::Dataset ds_A = kDBSensing::Test::createGenericLidarDataset(
    dss, "uav0", "uav0/base", "sensor", "dataset1", "uav0/lidar", go_rA, startStamp);

  CRES_QCOMPARE(ds_A.exists(), true);

  kDBDatasets::ValueIterator value_it
    = CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::createValueIterator(c, ds_A));

  int count = 0;
  int countOutside = 0;
  while(value_it.hasNext())
  {
    knowCore::Value val = CRES_QVERIFY(value_it.next());
    CRES_QVERIFY(val.value<knowValues::Values::Value>());
    knowValues::Values::LidarScan lidar_val
      = CRES_QVERIFY(val.value<knowValues::Values::LidarScan>());
    ++count;
    QCOMPARE(lidar_val->angleMin(), float(-M_PI));
    QCOMPARE(lidar_val->angleMax(), float(M_PI));
    QCOMPARE(lidar_val->angleIncrement(), float(M_PI / 10));
    QCOMPARE(lidar_val->scanTime(), 0.02f);
    QCOMPARE(lidar_val->timeIncrement(), 0.001f);
    QCOMPARE(lidar_val->ranges().size(), 20.0f);
    if(not go_rA.contains(lidar_val->pose().position()))
      ++countOutside;
  }
  QCOMPARE(count, 86);
  QCOMPARE(countOutside, 6); // some poses are outside due to the coverage by the lawn pattern

  // Find the last pose stamp

  startStamp = (kDBRobotics::AgentPositionRecord::byName(c, "uav0")
                and kDBRobotics::AgentPositionRecord::byFrame(c, "uav0/base"))
                 .orderByTimestamp(kDBRobotics::Sql::Sort::DESC)
                 .first()
                 .timestamp();

  // Area B
  knowGIS::GeometryObject go_rB = kDBDatasets::Test::createRegionB();
  kDBDatasets::Dataset ds_B = kDBSensing::Test::createGenericLidarDataset(
    dss, "uav0", "uav0/base", "sensor", "dataset2", "uav0/lidar", go_rB, startStamp + 10000_kCtns);

  CRES_QCOMPARE(ds_B.exists(), true);

  value_it = CRES_QVERIFY(kDBDatasets::DataInterfaceRegistry::createValueIterator(c, ds_B));

  count = 0;
  countOutside = 0;
  while(value_it.hasNext())
  {
    knowCore::Value val = CRES_QVERIFY(value_it.next());
    knowValues::Values::LidarScan lidar_val
      = CRES_QVERIFY(val.value<knowValues::Values::LidarScan>());
    ++count;
    QCOMPARE(lidar_val->angleMin(), float(-M_PI));
    QCOMPARE(lidar_val->angleMax(), float(M_PI));
    QCOMPARE(lidar_val->angleIncrement(), float(M_PI / 10));
    QCOMPARE(lidar_val->scanTime(), 0.02f);
    QCOMPARE(lidar_val->timeIncrement(), 0.001f);
    QCOMPARE(lidar_val->ranges().size(), 20.0f);
    if(not go_rB.contains(lidar_val->pose().position()))
      ++countOutside;
  }
  QCOMPARE(count, 65);
  QCOMPARE(countOutside, 4); // some poses are outside due to the coverage by the lawn pattern
}

QTEST_MAIN(TestTestDatasets)
