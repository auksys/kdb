#include "TestSalientRegion.h"

#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>

#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/krQuery/Engine.h>

#include <kDBSensing/SalientRegions/Collection.h>
#include <kDBSensing/SalientRegions/SalientRegion.h>

void TestSalientRegion::testSalientRegion()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBSensing"));

  kDBSensing::SalientRegions::Collection col
    = CRES_QVERIFY(kDBSensing::SalientRegions::Collection::getOrCreate(c, "test_ts"_kCu));

  kDBSensing::SalientRegions::SalientRegion sr = CRES_QVERIFY(
    col.createSalientRegion(CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT(10 1)")),
                            knowCore::Timestamp::now(), {"kl"_kCu}, {"prop1"_kCu, 10}, "a"_kCu));

  CRES_QCOMPARE(sr.salientProperty("prop1"_kCu), knowCore::Value::fromValue(10));
  CRES_QVERIFY(sr.setSalientProperty("prop1"_kCu, 20));
  CRES_QCOMPARE(sr.salientProperty("prop1"_kCu), knowCore::Value::fromValue(20));

  // Test krQL interface

  // Check incrementing byu 1

  CRES_QVERIFY(c.krQueryEngine()->execute(R"(salient region:
  action: add to property
  dataset: test_ts
  object: a
  property: prop1
  amount: 1)"));

  CRES_QCOMPARE(sr.salientProperty("prop1"_kCu), knowCore::Value::fromValue(21));

  // Check incrementing by -2

  CRES_QVERIFY(c.krQueryEngine()->execute(R"(salient region:
  action: add to property
  dataset: test_ts
  object: a
  property: prop1
  amount: -2)"));

  CRES_QCOMPARE(sr.salientProperty("prop1"_kCu), knowCore::Value::fromValue(19));

  // Check setting to 12

  CRES_QVERIFY(c.krQueryEngine()->execute(R"(salient region:
  action: set property
  dataset: test_ts
  object: a
  property: prop1
  value: 12)"));

  CRES_QCOMPARE(sr.salientProperty("prop1"_kCu), knowCore::Value::fromValue(12));

  // Check getting 12

  CRES_QCOMPARE(c.krQueryEngine()->execute(R"(salient region:
  action: get property
  dataset: test_ts
  object: a
  property: prop1)"),
                knowCore::Value::fromValue(12));

  // Check inserting new object

  CRES_QVERIFY(c.krQueryEngine()->execute(R"(salient region:
  action: insert
  dataset: test_ts
  object: b
  geometry: POINT(16.68297538639311 57.76080352715352)
  classes: ["hello", "world"])"));

  // Check inserting new object with properties

  CRES_QVERIFY(c.krQueryEngine()->execute(R"(salient region:
  action: insert
  dataset: test_ts
  object: c
  geometry: POINT(16.68297538639311 57.76080352715352)
  classes: ["hello", "world"]
  properties: {"prop1": 42})"));

  // Check getting 42
  CRES_QCOMPARE(c.krQueryEngine()->execute(R"(salient region:
  action: get property
  dataset: test_ts
  object: c
  property: prop1)"),
                knowCore::Value::fromValue(42));

  // Check inserting new object without object

  CRES_QVERIFY(c.krQueryEngine()->execute(R"(salient region:
  action: insert
  dataset: test_ts
  geometry: POINT(16.68297538639311 57.76080352715352)
  classes: ["hello", "world"]
  properties: {"prop1": 42})"));
}

QTEST_MAIN(TestSalientRegion)
