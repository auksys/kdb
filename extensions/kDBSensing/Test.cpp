#define KDBROBOTICS_NO_QTEST

#include "Test.h"

#include <random>

#include <Cartography/CoordinateSystem.h>

#include <knowCore/Timestamp.h>
#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TripleStore.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/Dataset.h>
#include <kDBPointClouds/PointCloud.h>
#include <kDBPointClouds/PointSpecification.h>
#include <kDBRobotics/FrameTransformation.h>
#include <kDBRobotics/Test.h>
#include <kDBSensing/LidarConfig.h>

#include <kDBSensing/CameraFrame.h>
#include <kDBSensing/CameraInfo.h>

#include <knowCore/Uris/askcore_unit.h>
#include <knowValues/Uris/askcore_sensing.h>

#include "SalientRegions/Collection.h"
#include "SalientRegions/SalientRegion.h"

using namespace kDBSensing;

using askcore_unit = knowCore::Uris::askcore_unit;
using askcore_sensing = knowValues::Uris::askcore_sensing;

QList<LidarScanRecord> Test::createLidarScans(
  const LidarConfigRecord& _config, const QString& _sensorUri, const QString& _datasetUri,
  const QString& _frameId, const knowCore::Timestamp& _start_stamp,
  const knowCore::Timestamp& _end_stamp, double _frequency, double _range_min, double _range_max)
{
  QList<LidarScanRecord> records;

  knowCore::Timestamp dt
    = knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(1000000000ull / _frequency);

  std::random_device rd;
  std::default_random_engine eng(rd());
  std::uniform_int_distribution<int> distr(_range_min, _range_max);

  for(knowCore::Timestamp t = _start_stamp; t <= _end_stamp; t += dt)
  {
    knowCore::FloatList ranges;
    for(float alpha = _config.angleMin(); alpha <= _config.angleMax();
        alpha += _config.angleIncrement())
    {
      ranges.append(distr(eng));
    }
    LidarScanRecord lfr = LidarScanRecord::create(_config.connection(), _sensorUri, _datasetUri,
                                                  _frameId, t, _config, ranges)
                            .expect_success();
    records.append(lfr);
  }

  return records;
}

QList<CameraFrameRecord> Test::createImages(const kDB::Repository::QueryConnectionInfo& _connection,
                                            const QString& _sensorUri, const QString& _datasetUri,
                                            const QString& _frameId,
                                            const knowCore::Timestamp& _start_stamp,
                                            const knowCore::Timestamp& _end_stamp,
                                            double _frequency, int _width, int _height)
{
  QList<CameraFrameRecord> records;

  knowCore::Timestamp dt
    = knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(1000000000ull / _frequency);

  std::random_device rd;
  std::default_random_engine eng(rd());
  std::uniform_int_distribution<int> distr(0, 255);

  for(knowCore::Timestamp t = _start_stamp; t <= _end_stamp; t += dt)
  {
    knowCore::Image img(_width, _height, 1, knowCore::Image::Type::UnsignedInteger8);
    for(quint64 i = 0; i < img.size(); ++i)
    {
      img.dataPtr()[i] = distr(eng);
    }
    CameraFrameRecord cfr
      = CameraFrameRecord::create(_connection, _sensorUri, _datasetUri, _frameId, t, {}, _width,
                                  _height, img.scalarSize(img.type()), img.encoding(), "none",
                                  img.toArray(), {})
          .expect_success();
    records.append(cfr);
  }

  return records;
}

using namespace kDBSensing::Test;

struct DatasetBuilder::Private
{
  QString name;
  QString frame;
  qreal min_altitude, max_altitude;
  double speed, frequency, spacing;
  QString sensorFrameId;

  knowCore::Timestamp last_end_stamp;
};

DatasetBuilder::DatasetBuilder(const QString& _name, const QString& _frame) : d(new Private)
{
  d->name = _name;
  d->frame = _frame;
}

DatasetBuilder::~DatasetBuilder() { delete d; }

void DatasetBuilder::setLawnMowerTrajectory(qreal _min_altitude, qreal _max_altitude, double _speed,
                                            double _frequency, double _spacing)
{
  d->min_altitude = _min_altitude;
  d->max_altitude = _max_altitude;
  d->speed = _speed;
  d->frequency = _frequency;
  d->spacing = _spacing;
}

void DatasetBuilder::setSensorTransform(const kDB::Repository::QueryConnectionInfo& _connection,
                                        const QString& _frameId, const knowCore::Timestamp& _stamp,
                                        const knowCore::Vector3d& _translation,
                                        const knowCore::Vector4d& _rotation)
{
  d->sensorFrameId = _frameId;
  kDBRobotics::FrameTransformationRecord previous_transform
    = (kDBRobotics::FrameTransformationRecord::byParentFrame(_connection, d->frame)
       and kDBRobotics::FrameTransformationRecord::byChildFrame(_connection, _frameId))
        .orderByTimestamp(kDBRobotics::Sql::Sort::DESC)
        .first();
  knowCore::Timestamp stamp = _stamp;
  if(previous_transform.timestamp() == stamp)
  {
    stamp += knowCore::NanoSeconds(1); // most clock don't have nanosecond accuracy
  }
  kDBRobotics::FrameTransformationRecord next_transform
    = kDBRobotics::FrameTransformationRecord::create(_connection, d->frame, _frameId, stamp,
                                                     _translation, _rotation)
        .expect_success();
  if(previous_transform.isPersistent())
  {
    previous_transform.setNextTimestamp(next_transform.timestamp());
    previous_transform.setNextFrame(next_transform);
    previous_transform.record();
  }
}

void DatasetBuilder::createLidarData(const QString& _sensorUri, const QString& _datasetUri,
                                     const knowGIS::GeometryObject& _geometry,
                                     const knowCore::Timestamp& _start_stamp,
                                     const LidarConfigRecord& _config, double _frequency,
                                     double _range_min, double _range_max)
{
  QList<kDBRobotics::AgentPositionRecord> trajectory = kDBRobotics::Test::createLawnMowerTrajectory(
    _config.connection(), d->name, d->frame, _geometry, _start_stamp, d->min_altitude,
    d->max_altitude, d->speed, d->frequency, d->spacing);

  d->last_end_stamp = trajectory.back().timestamp();
  createLidarScans(_config, _sensorUri, _datasetUri, d->sensorFrameId, _start_stamp,
                   d->last_end_stamp, _frequency, _range_min, _range_max);
}

void DatasetBuilder::createPointCloudData(const kDB::Repository::Connection& _connection,
                                          const QString& _datasetUri,
                                          const knowGIS::GeometryObject& _geometry,
                                          const knowCore::Timestamp& _start_stamp,
                                          const knowCore::Timestamp& _end_stamp)
{
  kDBPointClouds::Patch patch(kDBPointClouds::PointSpecification::get(
    _connection, kDBPointClouds::PointSpecification::StandardType::XYZf,
    Cartography::CoordinateSystem::wgs84));
  kDBPointClouds::PointCloudRecord::create(_connection, _datasetUri,
                                           (_start_stamp + _end_stamp) * 0.5, 0, 0, 0,
                                           knowCore::Vector4d(), _geometry, patch);
}

void DatasetBuilder::createImageData(const kDB::Repository::QueryConnectionInfo& _connection,
                                     const QString& _sensorUri, const QString& _datasetUri,
                                     const knowGIS::GeometryObject& _geometry,
                                     const knowCore::Timestamp& _start_stamp, double _frequency,
                                     int _width, int _height)
{
  QList<kDBRobotics::AgentPositionRecord> trajectory = kDBRobotics::Test::createLawnMowerTrajectory(
    _connection, d->name, d->frame, _geometry, _start_stamp, d->min_altitude, d->max_altitude,
    d->speed, d->frequency, d->spacing);
  d->last_end_stamp = trajectory.back().timestamp();
  auto blah = createImages(_connection, _sensorUri, _datasetUri, d->sensorFrameId, _start_stamp,
                           d->last_end_stamp, _frequency, _width, _height);
}

kDBDatasets::Dataset DatasetBuilder::createLidarDataset(
  const kDBDatasets::Collection& _dsss, const QString& _sensorUri, const QString& _datasetUri,
  const knowGIS::GeometryObject& _geometry, const knowCore::Timestamp& _start_stamp,
  const LidarConfigRecord& _config, double _frequency, double _range_min, double _range_max)
{

  kDBDatasets::Dataset ds
    = kDBDatasets::Collection(_dsss)
        .createDataset(
          askcore_sensing::lidar_scan, _geometry,
          {askcore_sensing::point_density,
           knowCore::QuantityNumber::create(100, askcore_unit::point_per_m2).expect_success()},
          _datasetUri)
        .expect_success();

  createLidarData(_sensorUri, _datasetUri, _geometry, _start_stamp, _config, _frequency, _range_min,
                  _range_max);
  ds.setStatus(kDBDatasets::Dataset::Status::Completed);
  ds.associate(ds.connection().serverUri());

  return ds;
}

kDBDatasets::Dataset DatasetBuilder::createSalientRegionDataset(
  const kDBDatasets::Collection& _dsss, const QString& _datasetUri,
  const QList<knowCore::Uri>& _klasses, const knowGIS::GeometryObject& _geometry,
  const knowCore::Timestamp& _start_stamp)
{
  kDBDatasets::Dataset ds
    = kDBDatasets::Collection(_dsss)
        .createDataset(askcore_sensing::salient_region, _geometry, {}, _datasetUri)
        .expect_success();

  kDBSensing::SalientRegions::Collection col
    = kDBSensing::SalientRegions::Collection::create(ds.connection(), _datasetUri).expect_success();

  QList<kDBRobotics::AgentPositionRecord> trajectory = kDBRobotics::Test::createLawnMowerTrajectory(
    _dsss.connection(), d->name, d->frame, _geometry, _start_stamp, d->min_altitude,
    d->max_altitude, d->speed, d->frequency, d->spacing);

  d->last_end_stamp = trajectory.back().timestamp();

  clog_assert(trajectory.size() > 0);

  for(const kDBRobotics::AgentPositionRecord& agent_position : trajectory)
  {
    knowGIS::GeometryObject go(Cartography::CoordinateSystem::wgs84,
                               Cartography::EuclidSystem::point(agent_position.longitude(),
                                                                agent_position.latitude(),
                                                                agent_position.altitude()));

    knowCore::ValueHash properties;
    properties.insert("timestamp", agent_position.timestamp());

    auto const& [s, sr, e]
      = col.createSalientRegion(go, agent_position.timestamp(), _klasses, properties);
    clog_assert(s);
  }
  ds.setStatus(kDBDatasets::Dataset::Status::Completed);
  ds.associate(ds.connection().serverUri());

  return ds;
}

kDBDatasets::Dataset Test::createGenericLidarDataset(
  const kDBDatasets::Collection& _dsss, const QString& _name, const QString& _frame,
  const QString& _sensorUri, const QString& _datasetUri, const QString& _frameId,
  const knowGIS::GeometryObject& _geometry, const knowCore::Timestamp& _start_stamp)
{
  DatasetBuilder db(_name, _frame);

  db.setLawnMowerTrajectory(30, 40, 10, 5, 1);
  db.setSensorTransform(_dsss.connection(), _frameId, _start_stamp, knowCore::Vector3d(0, 0, 0),
                        knowCore::Vector4d(0, 0, 0, 1));

  return db.createLidarDataset(_dsss, _sensorUri, _datasetUri, _geometry, _start_stamp,
                               LidarConfigRecord::getOrCreate(_dsss.connection(), -M_PI, M_PI,
                                                              M_PI / 10, 0.001, 0.001 * 20, 1, 100)
                                 .expect_success(),
                               1, 1, 100);
}

kDBDatasets::Dataset Test::createGenericSalientRegionDataset(
  const kDBDatasets::Collection& _dsss, const QString& _platformName, const QString& _datasetUri,
  const QList<knowCore::Uri>& _klasses, const knowGIS::GeometryObject& _geometry,
  const knowCore::Timestamp& _start_stamp)
{
  DatasetBuilder db(_platformName, "none");

  db.setLawnMowerTrajectory(30, 40, 10, 5, 1);

  return db.createSalientRegionDataset(_dsss, _datasetUri, _klasses, _geometry, _start_stamp);
}

knowCore::Timestamp DatasetBuilder::lastEndStamp() const { return d->last_end_stamp; }
