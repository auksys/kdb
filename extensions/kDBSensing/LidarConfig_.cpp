#include "LidarConfig.h"

using namespace kDBSensing;

cres_qresult<LidarConfigRecord> LidarConfigRecord::getOrCreate(
  const kDB::Repository::QueryConnectionInfo& _connection, float _angleMin, float _angleMax,
  float _angleIncrement, float _timeIncrement, float _scanTime, float _rangeMin, float _rangeMax)
{
  LidarConfigRecord config
    = (byAngleIncrement(_connection, _angleIncrement) and byAngleMin(_connection, _angleMin)
       and byAngleMax(_connection, _angleMax) and byTimeIncrement(_connection, _timeIncrement)
       and byScanTime(_connection, _scanTime) and byRangeMin(_connection, _rangeMin)
       and byRangeMax(_connection, _rangeMax))
        .first();
  if(not config.isPersistent())
  {
    // Then create
    cres_try(config, create(_connection, _angleMin, _angleMax, _angleIncrement, _timeIncrement,
                            _scanTime, _rangeMin, _rangeMax));
  }

  return cres_success(config);
}
