#define __KDB_NO_INITIALISE_WARNING__
#include "Initialise.h"

#include <QFile>

#include <clog_qt>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/ActiveRecord/Version.h>
#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TriplesView.h>
#include <kDB/Repository/krQuery/Engine.h>

#include <kDBDatasets/Initialise.h>
#include <kDBPointClouds/Initialise.h>

#include "CameraFrame.h"
#include "CameraInfo.h"
#include "FluidPressureFrame.h"
#include "ImuFrame.h"
#include "Lidar3dScan.h"
#include "LidarConfig.h"
#include "LidarScan.h"
#include "NavSatFrame.h"
#include "TemperatureFrame.h"

#include "krQueryActions.h"

#include "SalientRegions/Collection.h"

inline void kdbsensing_init_data() { Q_INIT_RESOURCE(kdbsensing_data); }

cres_qresult<void> kDBSensing::initialise(const kDB::Repository::Connection& _connection)
{
  kdbsensing_init_data();
  cres_try(kDB::Repository::ActiveRecord::VersionRecord vr,
           kDB::Repository::ActiveRecord::VersionRecord::get(_connection, "kDBSensing"));

  cres_qresult<void> pointCloudsExt
    = kDB::Repository::Connection(_connection).enableExtension("kDBPointClouds");
  if(not pointCloudsExt.is_successful())
  {
    clog_warning("Could not load 'kDBPointClouds' with error '{}' it will not be possible to use "
                 "Lidar3dScan.");
  }
  cres_try(cres_ignore, kDB::Repository::Connection(_connection).enableExtension("kDBDatasets"));

  cres_try(cres_ignore, LidarConfigRecord::createTable(_connection));
  cres_try(cres_ignore, LidarScanRecord::createTable(_connection));
  if(pointCloudsExt.is_successful())
  {
    cres_try(cres_ignore, Lidar3dScanRecord::createTable(_connection));
  }
  cres_try(cres_ignore, CameraInfoRecord::createTable(_connection));
  cres_try(cres_ignore, CameraFrameRecord::createTable(_connection));
  cres_try(cres_ignore, ImuFrameRecord::createTable(_connection));
  cres_try(cres_ignore, NavSatFrameRecord::createTable(_connection));
  cres_try(cres_ignore, FluidPressureFrameRecord::createTable(_connection));
  cres_try(cres_ignore, TemperatureFrameRecord::createTable(_connection));

  cres_try(cres_ignore, SalientRegions::Collection::registerCollection(_connection));

  _connection.krQueryEngine()->add({"salient region"}, new krQueryActions::SalientRegion(), true);

  return vr.updateToCurrent();
}

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBSensing)
