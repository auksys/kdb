#include "krQueryActions.h"

#include <knowCore/BigNumber.h>
#include <knowCore/Timestamp.h>
#include <knowCore/UriList.h>
#include <knowCore/ValueHash.h>

#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TripleStore.h>

#include <kDB/Repository/krQuery/Context.h>

#include "SalientRegions/Collection.h"
#include "SalientRegions/SalientRegion.h"

using namespace kDBSensing::krQueryActions;

struct SalientRegion::Private
{
};

SalientRegion::SalientRegion() : d(new Private) {}

SalientRegion::~SalientRegion() { delete d; }

cres_qresult<knowCore::Value>
  SalientRegion::execute(const kDB::Repository::krQuery::Context& _context, const QString& _key,
                         const knowCore::ValueHash& _parameters)
{
  kDB::Repository::Connection connection = _context.queryConnectionInfo().connection();
  if(connection.isConnected())
  {
    if(_key == "salient region")
    {
      cres_try(QString action, _parameters.value<QString>("action"));
      cres_try(knowCore::Uri dataset_uri, _parameters.value<knowCore::Uri>("dataset"));

      knowCore::Uri object_uri;
      if(_parameters.contains("object"))
      {
        cres_try(object_uri, _parameters.value<knowCore::Uri>("object"));
      }

      if(action == "insert")
      {
        knowGIS::GeometryObject geometry;
        knowCore::Value geometry_value = _parameters.value("geometry");
        if(knowCore::ValueCast<QString> geometry_str = geometry_value)
        {
          cres_try(geometry, knowGIS::GeometryObject::fromWKT(geometry_str));
        }
        else
        {
          cres_try(geometry, geometry_value.value<knowGIS::GeometryObject>());
        }
        cres_try(knowCore::UriList classes, _parameters.value<knowCore::UriList>("classes"));
        knowCore::ValueHash properties;
        knowCore::Value properties_value = _parameters.value("properties");
        if(knowCore::ValueCast<knowCore::ValueHash> value_hash = properties_value)
        {
          properties = value_hash;
        }

        cres_try(SalientRegions::Collection collection,
                 SalientRegions::Collection::get(connection, dataset_uri));
        if(object_uri.isEmpty())
        {
          cres_try(cres_ignore, collection.createSalientRegion(geometry, knowCore::Timestamp::now(),
                                                               classes, properties));
        }
        else
        {
          cres_try(cres_ignore, collection.createSalientRegion(geometry, knowCore::Timestamp::now(),
                                                               classes, properties, object_uri));
        }

        return cres_success(knowCore::Value());
      }
      else if(action == "add to property")
      {
        cres_try(knowCore::Uri property_uri, _parameters.value<knowCore::Uri>("property"));
        cres_try(knowCore::BigNumber amount, _parameters.value<knowCore::BigNumber>("amount"));
        cres_try(SalientRegions::Collection collection,
                 SalientRegions::Collection::get(connection, dataset_uri));

        cres_try(SalientRegions::SalientRegion salient_region,
                 collection.salientRegion(object_uri));

        cres_try(knowCore::Value value, salient_region.salientProperty(property_uri));

        cres_try(knowCore::BigNumber value_bn, value.value<knowCore::BigNumber>());
        value_bn = value_bn + amount;

        cres_try(cres_ignore, salient_region.setSalientProperty(
                                property_uri, knowCore::Value::fromValue(value_bn)));

        return cres_success(knowCore::Value());
      }
      else if(action == "set property")
      {
        cres_try(knowCore::Uri property_uri, _parameters.value<knowCore::Uri>("property"));
        knowCore::Value value = _parameters.value("value");

        cres_try(kDBSensing::SalientRegions::Collection collection,
                 kDBSensing::SalientRegions::Collection::get(connection, dataset_uri));
        cres_try(SalientRegions::SalientRegion salient_region,
                 collection.salientRegion(object_uri));
        cres_try(cres_ignore, salient_region.setSalientProperty(property_uri, value));

        return cres_success(knowCore::Value());
      }
      else if(action == "get property")
      {
        cres_try(knowCore::Uri property_uri, _parameters.value<knowCore::Uri>("property"));
        cres_try(SalientRegions::Collection collection,
                 SalientRegions::Collection::get(connection, dataset_uri));

        cres_try(SalientRegions::SalientRegion salient_region,
                 collection.salientRegion(object_uri));

        return salient_region.salientProperty(property_uri);
      }
      else
      {
        return cres_failure("Unknown salient region action: '{}'", action);
      }
    }
    return cres_failure("Unhandled key {}", _key);
  }
  else
  {
    return cres_failure("Not connected.");
  }
}
