#pragma once

#include <knowCore/ValueHash.h>

#include <kDB/Forward.h>

namespace kDBSensing
{
  namespace DataBaseCopy
  {
    class From
    {
    public:
      From(const kDB::Repository::Transaction& _transaction, const QString& _query,
           const knowCore::ValueHash& _bindings = knowCore::ValueHash());
      ~From();
      bool isValid() const;
      QByteArray next();
    private:
      struct Private;
      Private* const d;
    };
    class To
    {
    public:
      To(const kDB::Repository::Transaction& _transaction, const QString& _query);
      ~To();
      bool isValid() const;
      void next(const QByteArray& _data);
      bool close();
    private:
      struct Private;
      Private* const d;
    };
  } // namespace DataBaseCopy
} // namespace kDBSensing
