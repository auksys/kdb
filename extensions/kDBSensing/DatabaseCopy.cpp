#include "DatabaseCopy_p.h"

#include <QByteArray>

#include <clog_qt>
#include <knowCore/TypeDefinitions.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/DatabaseInterface/PostgreSQL/SQLCopyData.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/SQLReadData.h>
#include <kDB/Repository/Transaction.h>

using namespace kDBSensing::DataBaseCopy;

struct From::Private
{
  kDB::Repository::Transaction transaction;
  knowDBC::Query query;
  kDB::Repository::DatabaseInterface::PostgreSQL::SQLReadData* readData = nullptr;
};

From::From(const kDB::Repository::Transaction& _transaction, const QString& _query,
           const knowCore::ValueHash& _bindings)
    : d(new Private)
{
  d->transaction = _transaction;
  d->query = d->transaction.createSQLQuery(
    clog_qt::qformat("COPY {} TO STDOUT WITH (FORMAT binary)", _query));
  d->query.setOption(knowDBC::Query::OptionsKeys::InlineArguments, true);
  d->query.bindValues(_bindings);
  knowDBC::Result r = d->query.execute();
  if(r)
  {
    d->readData = new kDB::Repository::DatabaseInterface::PostgreSQL::SQLReadData(d->transaction);
  }
  else
  {
    clog_error("Failed to start copy from query for '{}' with error '{}'.", _query, r.error());
  }
}

From::~From()
{
  delete d->readData;
  delete d;
}

bool From::isValid() const { return d->readData; }

QByteArray From::next()
{
  clog_assert(d->readData);
  if(d->readData->status() == kDB::Repository::DatabaseInterface::PostgreSQL::SQLReadData::Open)
  {
    kDB::Repository::DatabaseInterface::PostgreSQL::SQLReadData::Buffer buffer
      = d->readData->readBuffer();
    if(buffer.isNull())
    {
      return QByteArray();
    }
    else
    {
      return buffer.toByteArray();
    }
  }
  else
  {
    return QByteArray();
  }
}

struct To::Private
{
  kDB::Repository::Transaction transaction;
  knowDBC::Query query;
  kDB::Repository::DatabaseInterface::PostgreSQL::SQLCopyData* copy_data = nullptr;
};

To::To(const kDB::Repository::Transaction& _transaction, const QString& _query) : d(new Private)
{
  d->transaction = _transaction;
  d->query = d->transaction.createSQLQuery(
    clog_qt::qformat("COPY {} FROM STDIN WITH (FORMAT binary)", _query));
  knowDBC::Result r = d->query.execute();
  if(r)
  {
    d->copy_data = new kDB::Repository::DatabaseInterface::PostgreSQL::SQLCopyData(d->transaction);
  }
  else
  {
    clog_error("Failed to start copy to query for '{}' with error '{}'.", _query, r.error());
  }
}

To::~To()
{
  delete d->copy_data;
  delete d;
}

bool To::isValid() const { return d->copy_data; }

void To::next(const QByteArray& _data)
{
  clog_assert(d->copy_data);
  d->copy_data->writeBuffer(_data.data(), _data.size());
}

bool To::close()
{
  clog_assert(d->copy_data);
  bool s = d->copy_data->close();
  if(not s)
  {
    clog_error("Failed to close copy {}", d->copy_data->errorMessage());
  }
  delete d->copy_data;
  d->copy_data = nullptr;
  return s;
}
