public:
static cres_qresult<LidarConfigRecord>
  getOrCreate(const kDB::Repository::QueryConnectionInfo& _connection, float _angleMin,
              float _angleMax, float _angleIncrement, float _timeIncrement, float _scanTime,
              float _rangeMin, float _rangeMax);
