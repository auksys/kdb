Prefix ssn: <http://www.w3.org/ns/ssn/> .
Prefix lrs_sensing: <http://www.ida.liu.se/divisions/aiics/lrs/sensing#>
Prefix un: <http://www.w3.org/2007/ont/unit#>
Prefix dul: <http://www.loa-cnr.it/ontologies/DUL.owl#>
Prefix askcore_graph: <http://askco.re/graph#>

Create View askcore_graph:lidar_info As
  Construct {
    ?config_uri lrs_sensing:angle_min ?angleMin ;
                lrs_sensing:angle_max ?angleMax ;
                lrs_sensing:angle_increment ?angleIncrement ;
                lrs_sensing:time_increment ?timeIncrement ;
                lrs_sensing:scan_time ?scanTime ;
                lrs_sensing:range_min ?rangeMin ;
                lrs_sensing:range_max ?rangeMax .
  }
  With
    ?config_uri = uri(%config_base_uri, str(?id))
    key = ?id
  From
    lidarconfigs
 
