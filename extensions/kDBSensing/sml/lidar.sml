Prefix ssn: <http://www.w3.org/ns/ssn/> .
Prefix askcore_dataset: <http://askco.re/dataset#>
Prefix lrs_sensing: <http://www.ida.liu.se/divisions/aiics/lrs/sensing#>
Prefix un: <http://www.w3.org/2007/ont/unit#>
Prefix dul: <http://www.loa-cnr.it/ontologies/DUL.owl#>
Prefix askcore_graph: <http://askco.re/graph#>

Create View askcore_graph:lidar_view As
  Construct {
    ?frame_uri ssn:observationResult [
          a ssn:SensorOutput ;
          ssn:hasValue [
            lrs_sensing:ranges [ dul:hasDataValue ?ranges ; dul:isClassifiedBy un:m ]  ;
            lrs_sensing:intensities [ dul:hasDataValue ?intensities ]  ;
          ] ;
      ] ;
      ssn:observedBy ?sensorUri ;
      lrs_sensing:configuration ?config_uri ;
      askcore_dataset:belongs_to ?datasetUri .
  }
  With
    ?frame_uri = uri(%frame_base_uri, str(?id))
    ?config_uri = uri(%config_base_uri, str(?id))
    ?ranges = plainLiteral(?ranges)
    ?intensities = plainLiteral(?intensities)
    key = ?id
  From
    LidarScans
