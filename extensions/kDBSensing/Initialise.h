#include <kDB/Forward.h>

namespace kDBSensing
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection);
}
