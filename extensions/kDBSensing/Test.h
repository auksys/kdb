#include <kDBDatasets/Forward.h>
#include <kDBSensing/LidarScan.h>

namespace kDBSensing::Test
{
  QList<LidarScanRecord> createLidarScans(const LidarConfigRecord& _config,
                                          const QString& _sensorUri, const QString& _datasetUri,
                                          const QString& _frameId,
                                          const knowCore::Timestamp& _start_stamp,
                                          const knowCore::Timestamp& _end_stamp, double _frequency,
                                          double _range_min = 0.0, double _range_max = 100.0);
  QList<CameraFrameRecord> createImages(const kDB::Repository::QueryConnectionInfo& _connection,
                                        const QString& _sensorUri, const QString& _datasetUri,
                                        const QString& _frameId,
                                        const knowCore::Timestamp& _start_stamp,
                                        const knowCore::Timestamp& _end_stamp, double _frequency,
                                        int _width, int _height);

  class DatasetBuilder
  {
  public:
    DatasetBuilder(const QString& _name, const QString& _frame);
    ~DatasetBuilder();
    kDBDatasets::Dataset getDataset() const;

    void setLawnMowerTrajectory(qreal _min_altitude, qreal _max_altitude, double _speed,
                                double _frequency, double _spacing);
    void setSensorTransform(const kDB::Repository::QueryConnectionInfo& _connection,
                            const QString& _frameId, const knowCore::Timestamp& _stamp,
                            const knowCore::Vector3d& _translation,
                            const knowCore::Vector4d& _rotation);
    kDBDatasets::Dataset createLidarDataset(const kDBDatasets::Collection& _dsss,
                                            const QString& _sensorUri, const QString& _datasetUri,
                                            const knowGIS::GeometryObject& _geometry,
                                            const knowCore::Timestamp& _start_stamp,
                                            const LidarConfigRecord& _config, double _frequency,
                                            double _range_min = 0.0, double _range_max = 100.0);
    void createLidarData(const QString& _sensorUri, const QString& _datasetUri,
                         const knowGIS::GeometryObject& _geometry,
                         const knowCore::Timestamp& _start_stamp, const LidarConfigRecord& _config,
                         double _frequency, double _range_min = 0.0, double _range_max = 100.0);
    kDBDatasets::Dataset createSalientRegionDataset(const kDBDatasets::Collection& _dsss,
                                                    const QString& _datasetUri,
                                                    const QList<knowCore::Uri>& _klasses,
                                                    const knowGIS::GeometryObject& _geometry,
                                                    const knowCore::Timestamp& _start_stamp);
    void createPointCloudData(const kDB::Repository::Connection& _connection,
                              const QString& _datasetUri, const knowGIS::GeometryObject& _geometry,
                              const knowCore::Timestamp& _start_stamp,
                              const knowCore::Timestamp& _end_stamp);
    void createImageData(const kDB::Repository::QueryConnectionInfo& _connection,
                         const QString& _sensorUri, const QString& _datasetUri,
                         const knowGIS::GeometryObject& _geometry,
                         const knowCore::Timestamp& _start_stamp, double _frequency, int _width,
                         int _height);

    /**
     * @return the end stamp for the last created dataset
     */
    knowCore::Timestamp lastEndStamp() const;
  private:
    struct Private;
    Private* const d;
  };

  kDBDatasets::Dataset createGenericLidarDataset(
    const kDBDatasets::Collection& _dsss, const QString& _name, const QString& _frame,
    const QString& _sensorUri, const QString& _datasetUri, const QString& _frameId,
    const knowGIS::GeometryObject& _geometry, const knowCore::Timestamp& _start_stamp);
  /**
   * Create a dataset with salient regions
   */
  kDBDatasets::Dataset createGenericSalientRegionDataset(const kDBDatasets::Collection& _dsss,
                                                         const QString& _platformName,
                                                         const QString& _datasetUri,
                                                         const QList<knowCore::Uri>& _klasses,
                                                         const knowGIS::GeometryObject& _geometry,
                                                         const knowCore::Timestamp& _start_stamp);

} // namespace kDBSensing::Test
