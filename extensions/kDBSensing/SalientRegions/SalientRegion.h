#include <kDB/Repository/RDF/FocusNode.h>

#include <kDBRobotics/Forward.h>

#include <kDBSensing/Forward.h>

namespace kDBSensing::SalientRegions
{
  /**
   * @ingroup kDBRobotics
   * This class allow to handle a manage a salientregion
   */
  class SalientRegion : public kDB::Repository::RDF::FocusNodeWrapper<SalientRegion>
  {
    friend class Collection;
  public:
    /**
     * Create an invalid salientregion
     */
    SalientRegion();
    SalientRegion(const SalientRegion& _rhs);
    SalientRegion& operator=(const SalientRegion& _rhs);
    ~SalientRegion();
    static cres_qresult<SalientRegion>
      fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node);
  public:
    bool operator==(const SalientRegion& _rhs) const;
  public:
    /**
     * @return the geometry of the object
     */
    cres_qresult<knowGIS::GeometryObject> geometry() const;
    cres_qresult<knowCore::Timestamp> timestamp() const;
    cres_qresult<knowCore::UriList> classes() const;
    cres_qresult<knowCore::ValueHash> salientProperties() const;
    cres_qresult<knowCore::Value> salientProperty(const knowCore::Uri& _path) const;
    cres_qresult<void> setSalientProperty(const knowCore::Uri& _path,
                                          const knowCore::Value& _value);
    template<typename _T_>
    cres_qresult<void> setSalientProperty(const knowCore::Uri& _path, const _T_& _value)
    {
      return setSalientProperty(_path, knowCore::Value::fromValue(_value));
    }

    cres_qresult<knowValues::Values::SalientRegion> toSalientRegion() const;
  public:
    bool isValid() const;
  };

} // namespace kDBSensing::SalientRegions

#include <knowCore/Formatter.h>
