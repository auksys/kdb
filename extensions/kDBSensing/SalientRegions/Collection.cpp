#include "Collection.h"

#include <knowCore/Timestamp.h>
#include <knowCore/UriList.h>

#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/RDF/FocusNode.h>
#include <kDB/Repository/RDF/FocusNodeDeclarationsRegistry.h>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/askcore_graph.h>
#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/ssn.h>
#include <knowCore/Uris/time.h>
#include <knowGIS/Uris/geo.h>
#include <knowValues/Uris/askcore_sensing.h>

#include "SalientRegion.h"

KDB_REPOSITORY_REGISTER_FOCUS_NODE_DECLARATIONS("qrc:///kdbsensing/data/salient_regions_shacl.ttl");

using namespace kDBSensing::SalientRegions;

using askcore_sensing = knowValues::Uris::askcore_sensing;
using askcore_datatype = knowCore::Uris::askcore_datatype;
using askcore_graph = knowCore::Uris::askcore_graph;
using geo = knowGIS::Uris::geo;
using rdf = knowCore::Uris::rdf;
using ssn = knowCore::Uris::ssn;

using FocusNode = kDB::Repository::RDF::FocusNode;
using FocusNodeCollection = kDB::Repository::RDF::FocusNodeCollection;

knowCore::Uri Collection::collectionType() { return askcore_sensing::salient_region_collection; }

knowCore::Uri Collection::allFocusNodesView() { return askcore_graph::all_salient_regions; }

knowCore::UriList Collection::containedTypes()
{
  return {knowCore::Uri(askcore_sensing::salient_region)};
}

knowCore::Uri Collection::primaryType() { return askcore_datatype::salientRegion; }

cres_qresult<knowCore::UriList>
  Collection::defaultDatatypes(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>&)
{
  return cres_success<knowCore::UriList>({primaryType()});
}

Collection::Collection() {}

Collection::Collection(const kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>& _rhs)
    : kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>(_rhs)
{
}

Collection::Collection(const Collection& _rhs)
    : kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>(_rhs)
{
}

Collection& Collection::operator=(const Collection& _rhs)
{
  kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>::operator=(_rhs);
  return *this;
}

Collection::~Collection() {}

Collection Collection::allSalientRegions(const kDB::Repository::Connection& _connection)
{
  return get(_connection, allFocusNodesView()).expect_success();
}

cres_qresult<SalientRegion> Collection::salientRegion(const knowCore::Uri& _salientregionUri) const
{
  return focusNode(_salientregionUri);
}

cres_qresult<bool> Collection::hasSalientRegion(const knowCore::Uri& _salientregionUri) const
{
  return hasFocusNode(_salientregionUri);
}

cres_qresult<QList<SalientRegion>> Collection::salientRegions(
  const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
  const OperatorOptions& _operatorOptions) const
{
  return focusNodes(_constraints, _operatorOptions);
}

cres_qresult<SalientRegion> Collection::createSalientRegion(
  const knowGIS::GeometryObject& _geometry, const knowCore::Timestamp& _timestamp,
  const knowCore::UriList& _klasses, const knowCore::ValueHash& _properties,
  const knowCore::Uri& _salientregionUri)

{
  return createFocusNode(askcore_datatype::salientRegion,
                         {
                           geo::hasGeometry,
                           _geometry,
                           knowCore::Uris::time::hasTime,
                           _timestamp,
                           rdf::a,
                           _klasses,
                           ssn::hasProperty,
                           _properties,
                         },
                         _salientregionUri);
}

#include "moc_Collection.cpp"
