#include "SalientRegion.h"

#include <knowGIS/GeometryObject.h>

#include <knowValues/Values.h>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/askcore_focus_node.h>
#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/ssn.h>
#include <knowCore/Uris/time.h>
#include <knowGIS/Uris/geo.h>

using namespace kDBSensing::SalientRegions;

using askcore_datatype = knowCore::Uris::askcore_datatype;
using askcore_focus_node = knowCore::Uris::askcore_focus_node;
using geo = knowGIS::Uris::geo;
using rdf = knowCore::Uris::rdf;
using ssn = knowCore::Uris::ssn;

SalientRegion::SalientRegion() {}

SalientRegion::SalientRegion(const SalientRegion& _rhs)
    : kDB::Repository::RDF::FocusNodeWrapper<SalientRegion>(_rhs)
{
}

SalientRegion& SalientRegion::operator=(const SalientRegion& _rhs)
{
  return kDB::Repository::RDF::FocusNodeWrapper<SalientRegion>::operator=(_rhs);
}

SalientRegion::~SalientRegion() {}

cres_qresult<SalientRegion>
  SalientRegion::fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node)
{
  SalientRegion ag;
  ag.setFocusNode(_focus_node);
  return cres_success(ag);
}

cres_qresult<knowGIS::GeometryObject> SalientRegion::geometry() const
{
  return property<knowGIS::GeometryObject>(geo::hasGeometry);
}

cres_qresult<knowCore::Timestamp> SalientRegion::timestamp() const
{
  return property<knowCore::Timestamp>(knowCore::Uris::time::hasTime);
}

cres_qresult<knowCore::UriList> SalientRegion::classes() const
{
  cres_try(knowCore::UriList unfiltered, property<knowCore::UriList>(rdf::a));
  QList<knowCore::Uri> uris;
  for(const knowCore::Uri& u : unfiltered)
  {
    if(u != askcore_datatype::salientRegion and u != askcore_focus_node::focus_node)
    {
      uris.append(u);
    }
  }
  return cres_success(uris);
}

cres_qresult<knowCore::ValueHash> SalientRegion::salientProperties() const
{
  return property<knowCore::ValueHash>(ssn::hasProperty);
}

cres_qresult<knowCore::Value> SalientRegion::salientProperty(const knowCore::Uri& _path) const
{
  return propertyMapValue(ssn::hasProperty, _path);
}
cres_qresult<void> SalientRegion::setSalientProperty(const knowCore::Uri& _path,
                                                     const knowCore::Value& _value)
{
  return setPropertyInMap(ssn::hasProperty, _path, _value);
}

cres_qresult<knowValues::Values::SalientRegion> SalientRegion::toSalientRegion() const
{

  cres_try(knowGIS::GeometryObject go, geometry());
  cres_try(knowCore::Timestamp timestamp_v, timestamp());
  cres_try(QList<knowCore::Uri> classes_v, classes());
  cres_try(knowCore::ValueHash properties_v, salientProperties());

  return cres_success(knowValues::Values::SalientRegion::create()
                        .setUri(uri())
                        .setGeometry(go)
                        .setClasses(classes_v)
                        .setTimestamp(timestamp_v)
                        .setProperties(properties_v));
}
