#pragma once

#include <kDBDatasets/Forward.h>
#include <knowValues/Forward.h>

namespace kDBSensing
{
  namespace SalientRegions
  {
    class SalientRegion;
    class Collection;
  } // namespace SalientRegions
} // namespace kDBSensing