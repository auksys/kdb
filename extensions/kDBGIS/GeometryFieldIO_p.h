#include <kDB/Repository/DatabaseInterface/PostgreSQL/RDFValueBinaryMarshal.h>

namespace kDBGIS
{
  class GeometryFieldIO
      : public kDB::Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::FieldIO
  {
  public:
    GeometryFieldIO();
    virtual ~GeometryFieldIO();
    bool accept(const knowCore::Value&) const override;
    quint32 calculateSize(const knowCore::Value&) const override;
    knowCore::Value read(const char* _data, int _field_size,
                         const kDB::Repository::Connection& _connection) const override;
    void write(char* _data, const knowCore::Value&,
               const kDB::Repository::Connection& _connection) const override;
  };
} // namespace kDBGIS
