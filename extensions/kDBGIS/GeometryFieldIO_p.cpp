#include "GeometryFieldIO_p.h"

#include <QVariant>

#include <knowCore/Value.h>
#include <knowGIS/GeometryObject.h>

using namespace kDBGIS;

GeometryFieldIO::GeometryFieldIO() : FieldIO("geometry") {}

GeometryFieldIO::~GeometryFieldIO() {}

bool GeometryFieldIO::accept(const knowCore::Value& _variant) const
{
  return _variant.canConvert<knowGIS::GeometryObject>();
}

quint32 GeometryFieldIO::calculateSize(const knowCore::Value& _variant) const
{
  return _variant.value<knowGIS::GeometryObject>().expect_success().toEWKB().size();
}

knowCore::Value GeometryFieldIO::read(const char* _data, int _field_size,
                                      const kDB::Repository::Connection& /*_connection*/) const
{
  QByteArray arr(_data, _field_size);
  auto const [success, value, message] = knowGIS::GeometryObject::fromEWKB(arr);
  if(success)
  {
    return knowCore::Value::fromValue(value.value());
  }
  else
  {
    clog_warning("Failed to parse data from WKB: {}", message.value());
    return knowCore::Value();
  }
}

void GeometryFieldIO::write(char* _data, const knowCore::Value& _variant,
                            const kDB::Repository::Connection& /*_connection*/) const
{
  QByteArray arr = _variant.value<knowGIS::GeometryObject>().expect_success().toEWKB();
  std::copy(arr.begin(), arr.end(), _data);
}
