CREATE OR REPLACE FUNCTION kdbgis_is_geometry(a kdb_rdf_term) RETURNS boolean AS $$
  SELECT NOT ((a).value IS NULL) AND ((a).value).type = 'geometry' AND NOT (((a).value).geometry IS NULL)
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdbgis_get_srid(a kdb_rdf_term) RETURNS integer AS $$
  SELECT CASE WHEN NOT kdbgis_is_geometry(a) THEN null -- Only apply on geometry value
              ELSE ST_SRID(((a).value).geometry) END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdbgis_geo_sfWithin(a kdb_rdf_term, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT CASE WHEN NOT kdbgis_is_geometry(a) OR NOT kdbgis_is_geometry(b) THEN null -- Only apply on geometry value
              ELSE ST_Within(((a).value).geometry, ST_Transform(((b).value).geometry, ST_SRID(((a).value).geometry))) END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdbgis_geo_sfOverlaps(a kdb_rdf_term, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT CASE WHEN NOT kdbgis_is_geometry(a) OR NOT kdbgis_is_geometry(b) THEN null -- Only apply on geometry value
              ELSE ST_Overlaps(((a).value).geometry, ST_Transform(((b).value).geometry, ST_SRID(((a).value).geometry))) END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdbgis_geo_sfIntersects(a kdb_rdf_term, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT CASE WHEN NOT kdbgis_is_geometry(a) OR NOT kdbgis_is_geometry(b) THEN null -- Only apply on geometry value
              ELSE ST_Intersects(((a).value).geometry, ST_Transform(((b).value).geometry, ST_SRID(((a).value).geometry))) END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION __kdbgis_geo_sfIntersects__internal(a geometry, b geometry, epsilon float8) RETURNS boolean AS $$
  SELECT ST_Intersects(a, b) AND ST_Area(ST_Intersection(a, b))/ST_Area(a) > epsilon
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdbgis_geo_sfIntersects(a kdb_rdf_term, b kdb_rdf_term, epsilon kdb_rdf_term) RETURNS boolean AS $$
  SELECT CASE WHEN NOT kdbgis_is_geometry(a) OR NOT kdbgis_is_geometry(b) THEN null -- Only apply on geometry value
              ELSE __kdbgis_geo_sfIntersects__internal(((a).value).geometry, ST_Transform(((b).value).geometry, ST_SRID(((a).value).geometry)), ((epsilon).value).numeric) END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION kdbgis_geo_union(a kdb_rdf_term, b kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN kdbgis_is_geometry(a) AND b IS NULL THEN a
              WHEN a IS NULL AND kdbgis_is_geometry(b) THEN b
              WHEN NOT kdbgis_is_geometry(a) OR NOT kdbgis_is_geometry(b) THEN null -- Only apply on geometry value
              ELSE kdb_rdf_term_create(ST_Union(((a).value).geometry, ((b).value).geometry)) END
$$ LANGUAGE SQL IMMUTABLE;

DROP AGGREGATE IF EXISTS kdbgis_geo_union(kdb_rdf_term);
CREATE AGGREGATE kdbgis_geo_union(kdb_rdf_term) (
  sfunc = kdbgis_geo_union,
  combinefunc = kdbgis_geo_union,
  parallel = safe,
  stype = kdb_rdf_term
  );

CREATE OR REPLACE FUNCTION kdbgis_geo_difference(a kdb_rdf_term, b kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN NOT kdbgis_is_geometry(a) OR NOT kdbgis_is_geometry(b) THEN null -- Only apply on geometry value
              ELSE kdb_rdf_term_create(ST_Difference(((a).value).geometry, ((b).value).geometry)) END
$$ LANGUAGE SQL IMMUTABLE;
