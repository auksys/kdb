CREATE TYPE kdb_geopoint AS (
  longitude double precision,
  latitude double precision,
  altitude double precision
);

CREATE OR REPLACE FUNCTION kdb_geopoint_create(longitude double precision, latitude double precision, altitude double precision) RETURNS kdb_geopoint AS $$
  SELECT ROW(longitude, latitude, altitude)::kdb_geopoint
$$ LANGUAGE SQL IMMUTABLE;

CREATE TYPE kdb_point AS (
  x double precision,
  y double precision,
  z double precision,
  srid integer
);

CREATE OR REPLACE FUNCTION kdb_point_create(x double precision, y double precision, z double precision,
  srid integer) RETURNS kdb_point AS $$
  SELECT ROW(x, y, z, srid)::kdb_point
$$ LANGUAGE SQL IMMUTABLE;

