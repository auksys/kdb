#pragma once

#include <Cartography/Forward.h>
#include <kDB/Forward.h>
#include <knowGIS/Forward.h>

namespace kDBGIS
{
  namespace Features
  {
    class Collection;
    class Feature;
  } // namespace Features
} // namespace kDBGIS
