#include <QtTest/QtTest>

#include <knowGIS/GeometryObject.h>

namespace kDBGIS
{
  char* toString(const knowGIS::GeometryObject& go)
  {
    return go.isValid() ? QTest::toString(go.toWKT()) : QTest::toString("[invalid]");
  }
} // namespace kDBGIS
