#ifndef _KDBGIS_TYPES_DEFINITIONS_H_
#define _KDBGIS_TYPES_DEFINITIONS_H_

#include <QVariant>
#include <knowCore/TypeDefinitions.h>

#include <knowGIS/AltitudeReference.h>
#include <knowGIS/GeometryObject.h>
#include <knowGIS/Raster.h>

#include "Forward.h"

#endif
