#include <QString>
#include <QVariant>

#include <kDB/Repository/AbstractBinaryMarshal.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/BinaryMarshalsRegistry.h>

#include <kDB/Repository/DatabaseInterface/PostgreSQL/BinaryInterface_read_p.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/BinaryInterface_write_p.h>

#include <knowGIS/GeoPoint.h>
#include <knowGIS/GeometryObject.h>
#include <knowGIS/Point.h>
#include <knowGIS/Raster.h>

namespace kDBGIS
{

  class SQLRasterMarshal : public kDB::Repository::AbstractBinaryMarshal
  {
  public:
    SQLRasterMarshal()
        : AbstractBinaryMarshal("raster", datatype<knowGIS::Raster>(),
                                Mode::ToVariant | Mode::ToByteArray)
    {
    }
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);
      _oidName = "raster";
      return cres_success(_source.value<knowGIS::Raster>().expect_success().toWKBRaster());
    }
    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);
      knowGIS::Raster raster = knowGIS::Raster::fromWKBRaster(_source);
      if(raster.isValid())
      {
        return cres_success(knowCore::Value::fromValue(raster));
      }
      else
      {
        return cres_failure("Invalid raster");
      }
    }
  };

  KDB_REGISTER_SQL_MARSHAL(SQLRasterMarshal)

  class GeometryObjectMarshal : public kDB::Repository::AbstractBinaryMarshal
  {
  public:
    GeometryObjectMarshal()
        : AbstractBinaryMarshal("geometry", datatype<knowGIS::GeometryObject>(),
                                Mode::ToVariant | Mode::ToByteArray)
    {
    }
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);
      _oidName = "geometry";
      QByteArray data = _source.value<knowGIS::GeometryObject>().expect_success().toEWKB();
      if(data.isEmpty())
      {
        // Empty Point in wkb 01 04 00 00 00 00 00 00 00
        data.fill(0, 9);
        data[0] = 1;
        data[1] = 4;
      }
      return cres_success(data);
    }
    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);
      if(_source.isEmpty())
        return cres_success(knowCore::Value::fromValue(knowGIS::GeometryObject()));
      auto const [success, object, message] = knowGIS::GeometryObject::fromEWKB(_source);
      if(success)
      {
        return cres_success(knowCore::Value::fromValue(object.value()));
      }
      else
      {
        return cres_failure("Failed to parse WKB: {}", message.value());
      }
    }
  };

  KDB_REGISTER_SQL_MARSHAL(GeometryObjectMarshal)

  class PointMarshal : public kDB::Repository::AbstractBinaryMarshal
  {
  public:
    PointMarshal()
        : AbstractBinaryMarshal("kdb_point", datatype<knowGIS::Point>(),
                                Mode::ToVariant | Mode::ToByteArray)
    {
    }
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);

      _oidName = "kdb_point";
      knowGIS::Point gp = _source.value<knowGIS::Point>().expect_success();
      QByteArray destination;
      cres_try(qint64 oid, _connection.oid("kdb_point"));
      kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::struct_writer<double, double,
                                                                                     double, int>
        w(oid, gp.x(), gp.y(), gp.z(), gp.coordinateSystem().srid());
      clog_assert(w.calculate_size() == (1 + 4 * 2 * 3) * 4);
      destination.resize(w.calculate_size());
      w.write(destination.begin());

      return cres_success(destination);
    }
    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);

      std::tuple<double, double, double, int> t
        = kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::read<
          std::tuple<double, double, double, int>>(_source.begin(), _source.size());

      return cres_success(
        knowCore::Value::fromValue(knowGIS::Point(std::get<0>(t), std::get<1>(t), std::get<2>(t),
                                                  Cartography::CoordinateSystem(std::get<3>(t)))));
    }
  };

  KDB_REGISTER_SQL_MARSHAL(PointMarshal)

  /**
   * Marshall between knowGIS::GeoPoint and kdb_geopoint.
   */
  class GeoPointMarshal : public kDB::Repository::AbstractBinaryMarshal
  {
  public:
    GeoPointMarshal()
        : AbstractBinaryMarshal("kdb_geopoint", datatype<knowGIS::GeoPoint>(),
                                Mode::ToVariant | Mode::ToByteArray)
    {
    }
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);

      _oidName = "kdb_geopoint";
      knowGIS::GeoPoint gp = _source.value<knowGIS::GeoPoint>().expect_success();

      QByteArray destination;

      cres_try(qint64 oid, _connection.oid("kdb_geopoint"));
      kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::struct_writer<double, double,
                                                                                     double>
        w(oid, gp.longitude(), gp.latitude(), gp.altitude());
      clog_assert(w.calculate_size() == (1 + 4 * 2 * 3) * 4);
      destination.resize(w.calculate_size());
      w.write(destination.begin());

      return cres_success(destination);
    }
    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);

      std::tuple<double, double, double> t
        = kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface::read<
          std::tuple<double, double, double>>(_source.begin(), _source.size());
      return cres_success(knowCore::Value::fromValue(knowGIS::GeoPoint(
        Cartography::Longitude(std::get<0>(t)), Cartography::Latitude(std::get<1>(t)),
        Cartography::Altitude(std::get<2>(t)))));
    }
  };

  KDB_REGISTER_SQL_MARSHAL(GeoPointMarshal)
} // namespace kDBGIS
