#include "DEM.h"

#include <clog_qt>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Logging.h>
#include <kDB/Repository/QueryConnectionInfo.h>

#include <knowGIS/GeoPoint.h>
#include <knowGIS/Point.h>

using namespace kDBGIS;

double DEMRecord::altitude(const kDB::Repository::QueryConnectionInfo& _connection,
                           const knowGIS::Point& _point,
                           knowGIS::AltitudeReference::Type _altitudeReference)
{
  knowDBC::Query q = _connection.createSQLQuery(
    "SELECT Q.altitude AS altitude, Q.altitudeReference AS altitudeReference,"
    "       ST_X(Q.point_wgs84) AS longitude,"
    "       ST_Y(Q.point_wgs84) AS latitude,"
    "       (ST_X(Q.raster_res_wgs84) + ST_Y(Q.raster_res_wgs84)) AS raster_resolution"
    "    FROM ( SELECT ST_Value(rast, ST_Transform(foo.pt_geom, ST_SRID(rast))) AS altitude,"
    "                  ST_SRID(rast) AS srid,"
    "                  foo.pt_geom AS pt_geom, altitudeReference, foo.point_wgs84 AS point_wgs84,"
    "                  ST_Transform(ST_SetSRID(ST_MakePoint(ST_PixelWidth(rast), "
    "ST_PixelHeight(rast)), ST_SRID(rast)), 4326) AS raster_res_wgs84"
    "              FROM dems CROSS JOIN (SELECT ST_SetSRID(ST_MakePoint(?x, ?y), ?srid) AS pt_geom,"
    "                                           ST_Transform(ST_SetSRID(ST_MakePoint(?x, ?y), "
    "?srid), 4326) AS point_wgs84) As foo) AS Q WHERE Q.altitude IS NOT NULL"
    "    ORDER BY raster_resolution DESC"
    "    LIMIT 1;");
  q.bindValue("?x", _point.x());
  q.bindValue("?y", _point.y());
  q.bindValue("?srid", _point.coordinateSystem().srid());

  knowDBC::Result r = q.execute();

  if(r)
  {
    if(r.tuples() > 0)
    {
      qreal altitude = r.value<double>(0, 0).expect_success();
      const QString ref = r.value<QString>(0, 1).expect_success();
      const qreal longitude = r.value<qreal>(0, 2).expect_success();
      const qreal latitude = r.value<qreal>(0, 3).expect_success();

      knowGIS::GeoPoint pt(Cartography::Longitude{longitude}, Cartography::Latitude{latitude});

      altitude -= knowGIS::AltitudeReference::get(_altitudeReference, pt);

      if(ref == "WGS84")
      {
        altitude
          += knowGIS::AltitudeReference::get(knowGIS::AltitudeReference::Type::WGS84_Ellipsoid, pt);
      }
      else if(ref == "EGM84")
      {
        altitude
          += knowGIS::AltitudeReference::get(knowGIS::AltitudeReference::Type::EGM84_Geoid, pt);
      }
      else if(ref == "EGM96")
      {
        altitude
          += knowGIS::AltitudeReference::get(knowGIS::AltitudeReference::Type::EGM96_Geoid, pt);
      }
      else if(ref == "EGM2008")
      {
        altitude
          += knowGIS::AltitudeReference::get(knowGIS::AltitudeReference::Type::EGM2008_Geoid, pt);
      }
      return altitude;
    }
    else
    {
      clog_warning("No results when querying altitude at longitude = {} lattitude = {}", _point.x(),
                   _point.y());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to get altitude", r);
  }
  return std::numeric_limits<double>::quiet_NaN();
}
