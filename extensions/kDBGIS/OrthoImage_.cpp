#include "OrthoImage.h"

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/QueryConnectionInfo.h>

using namespace kDBGIS;

QList<knowGIS::Raster> OrthoImageRecord::at(const kDB::Repository::QueryConnectionInfo& _connection,
                                            double _x, double _y, unsigned int _srid)
{
  knowDBC::Query q = _connection.createSQLQuery(
    "SELECT rast FROM orthoimages WHERE ST_Within(ST_Transform(ST_SetSRID(ST_MakePoint(?x, ?y), "
    "?srid), ST_SRID(rast)), ST_Envelope(rast))");
  q.bindValue("?x", _x);
  q.bindValue("?y", _y);
  q.bindValue("?srid", _srid);

  knowDBC::Result r = q.execute();
  QList<knowGIS::Raster> rasters;
  if(r)
  {
    for(int t = 0; t < r.tuples(); ++t)
    {
      rasters.append(r.value(t, 0).value<knowGIS::Raster>().expect_success());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to get orthoimage", r);
  }
  return rasters;
}
