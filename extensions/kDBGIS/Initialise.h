#include <QSharedPointer>

#include <kDB/Forward.h>

namespace kDBGIS
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection);
}
