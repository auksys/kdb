public:
/**
 * In WGS 84 _x is longitude and _y is latitude
 */
static double altitude(const kDB::Repository::QueryConnectionInfo& _connection,
                       const knowGIS::Point& _point,
                       knowGIS::AltitudeReference::Type _altitudeReference);
