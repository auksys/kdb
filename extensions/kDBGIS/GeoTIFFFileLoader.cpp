#include "GeoTIFFFileLoader.h"

#include <QFile>
#include <QStringList>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/RDFEnvironment.h>

#include "DEM.h"
#include "OrthoImage.h"

using namespace kDBGIS;

GeoTIFFFileLoader::GeoTIFFFileLoader() {}

GeoTIFFFileLoader::~GeoTIFFFileLoader() {}

bool GeoTIFFFileLoader::canLoad(const QString& _path, const QString& _mime,
                                const knowCore::ValueHash&)
{
  return _mime == "image/tiff" or _path.endsWith(".tif") or _path.endsWith(".tiff");
}

cres_qresult<QStringList>
  GeoTIFFFileLoader::loadFile(const QString& _path, const kDB::Repository::Connection& _connection,
                              const knowCore::ValueHash& _options)
{
  knowGIS::Raster rast = knowGIS::Raster::load(_path);
  if(not rast.isValid())
  {
    return cres_failure("Invalid GeoTIFF file {}", _path);
  }
  bool is_dem = false;
  if(rast.bandsCount() == 1 and _options.contains("is_dem"))
  {
    if(knowCore::ValueCast<bool> is_dem_opt = _options.value("is_dem"))
    {
      is_dem = is_dem_opt;
    }
  }
  if(is_dem)
  {
    cres_try(cres_ignore,
             kDBGIS::DEMRecord::create(_connection, rast, QString::fromStdString("EGM2008")));
  }
  else
  {
    cres_try(cres_ignore, kDBGIS::OrthoImageRecord::create(_connection, rast));
  }
  return cres_success(QStringList());
}

#include <kDBBaseKnowledge/Manager.h>

KDB_REGISTER_BASE_KNOWLEDGE_FILE_LOADER(GeoTIFFFileLoader)
