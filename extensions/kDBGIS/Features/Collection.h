#pragma once

#include <knowCore/ConstrainedValue.h>
#include <knowCore/UriList.h>

#include <kDB/Repository/RDF/FocusNodeCollection.h>

#include <kDBGIS/Forward.h>

class TestFeatures;

template<>
struct kDB::Repository::RDF::FocusNodeCollectionTrait<kDBGIS::Features::Collection>
{
  using ValueType = kDBGIS::Features::Feature;
};

namespace kDBGIS::Features
{
  /**
   * Interface a @ref kDB::Repository::TripleStore that contains a collection of salientregions
   */
  class Collection : public kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>
  {
    friend class ::TestFeatures;
  public:
    using ValueType = Feature;
  protected:
    Collection(const kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>& _rhs);
  public:
    Collection();
    Collection(const Collection& _rhs);
    Collection& operator=(const Collection& _rhs);
    ~Collection();
  public:
    static knowCore::Uri collectionType();
    static knowCore::Uri allFocusNodesView();
    static knowCore::Uri primaryType();
    static knowCore::UriList containedTypes();
    static cres_qresult<knowCore::UriList>
      defaultDatatypes(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints);
  public:
    bool operator==(const Collection& _dss) const;
  public:
    /**
     * \return an interface to the collection that list all salient regions.
     */
    static Collection allFeatures(const kDB::Repository::Connection& _connection);
  public:
    /**
     * \return the salientRegion with the Uri \ref _salientRegionUri
     */
    cres_qresult<Feature> salientRegion(const knowCore::Uri& _salientregionUri) const;
    cres_qresult<bool> hasFeature(const knowCore::Uri& _salientregionUri) const;
    using OperatorOptions = kDB::Repository::RDF::FocusNodeCollection::OperatorOptions;
    /**
     * @param _constraints a list of pair of list of uris representing the property uri and a
     * constraint.
     * @param _operatorOptions set the precision used by operators
     *
     * \return the list of features that satisfies the constraints
     */
    cres_qresult<QList<Feature>>
      features(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
               const OperatorOptions& _operatorOptions = OperatorOptions()) const;
    template<typename... _TArgs_>
    cres_qresult<QList<Feature>> features(const knowCore::Uri& _uri,
                                          const knowCore::ConstrainedValue& _constraint,
                                          const _TArgs_&...) const;
    /**
     */
    cres_qresult<Feature> insertFeatureFromCbor(const QCborMap& _map);
    /**
     * Import the content of the iodevice from overpass raw API output.
     */
    cres_qresult<void> importFromOverpass(QIODevice* _device);
  public:
    /**
     * Create a new \ref Feature of uri type \p _typeUri with geometry \p _geometry and add it
     * to the \p _rdfGraph.
     */
    cres_qresult<Feature> createFeature(const knowGIS::GeometryObject& _geometry,
                                        const knowCore::ValueHash& _properties,
                                        const knowCore::Uri& _featureUri
                                        = knowCore::Uri::createUnique({"salientregion"}));
  };

} // namespace kDBGIS::Features
