#include "Feature.h"

#include <knowGIS/GeometryObject.h>

#include <knowValues/Values.h>

#include <knowCore/Uris/rdf.h>
#include <knowGIS/Uris/askcore_sgf.h>
#include <knowGIS/Uris/geo.h>

using namespace kDBGIS::Features;

using askcore_sgf = knowGIS::Uris::askcore_sgf;
using geo = knowGIS::Uris::geo;
using rdf = knowCore::Uris::rdf;

Feature::Feature() {}

Feature::Feature(const Feature& _rhs) : kDB::Repository::RDF::FocusNodeWrapper<Feature>(_rhs) {}

Feature& Feature::operator=(const Feature& _rhs)
{
  return kDB::Repository::RDF::FocusNodeWrapper<Feature>::operator=(_rhs);
}

Feature::~Feature() {}

cres_qresult<Feature> Feature::fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node)
{
  Feature ag;
  ag.setFocusNode(_focus_node);
  return cres_success(ag);
}

cres_qresult<knowGIS::GeometryObject> Feature::geometry() const
{
  return property<knowGIS::GeometryObject>(geo::hasGeometry);
}

cres_qresult<knowCore::UriList> Feature::classes() const
{
  return property<knowCore::UriList>(rdf::a);
}

cres_qresult<knowCore::ValueHash> Feature::featureProperties() const
{
  return property<knowCore::ValueHash>(askcore_sgf::has_property);
}

cres_qresult<knowCore::Value> Feature::featureProperty(const knowCore::Uri& _path) const
{
  return propertyMapValue(askcore_sgf::has_property, _path);
}
cres_qresult<void> Feature::setFeatureProperty(const knowCore::Uri& _path,
                                               const knowCore::Value& _value)
{
  return setPropertyInMap(askcore_sgf::has_property, _path, _value);
}
