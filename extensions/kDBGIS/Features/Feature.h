#include <kDB/Repository/RDF/FocusNode.h>

#include <kDBGIS/Forward.h>

namespace kDBGIS::Features
{
  /**
   * @ingroup kDBGIS
   * This class allow to handle a manage a feature
   */
  class Feature : public kDB::Repository::RDF::FocusNodeWrapper<Feature>
  {
    friend class Collection;
  public:
    /**
     * Create an invalid feature
     */
    Feature();
    Feature(const Feature& _rhs);
    Feature& operator=(const Feature& _rhs);
    ~Feature();
    static cres_qresult<Feature> fromFocusNode(const kDB::Repository::RDF::FocusNode& _focus_node);
  public:
    bool operator==(const Feature& _rhs) const;
  public:
    /**
     * @return the geometry of the object
     */
    cres_qresult<knowGIS::GeometryObject> geometry() const;
    cres_qresult<knowCore::UriList> classes() const;
    cres_qresult<knowCore::ValueHash> featureProperties() const;
    cres_qresult<knowCore::Value> featureProperty(const knowCore::Uri& _path) const;
    cres_qresult<void> setFeatureProperty(const knowCore::Uri& _path,
                                          const knowCore::Value& _value);
    template<typename _T_>
    cres_qresult<void> setFeatureProperty(const knowCore::Uri& _path, const _T_& _value)
    {
      return setSalientProperty(_path, knowCore::Value::fromValue(_value));
    }
  public:
    bool isValid() const;
  };

} // namespace kDBGIS::Features

#include <knowCore/Formatter.h>
