#include "Collection.h"

#include <euclid/format>

#include <QXmlStreamReader>

#include <knowCore/QuantityValue.h>
#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/UriList.h>

#include <knowGIS/GeometryObject.h>

#include <Cartography/CoordinateSystem.h>

#include <kDB/Repository/RDF/FocusNode.h>
#include <kDB/Repository/RDF/FocusNodeDeclarationsRegistry.h>

#include <knowCore/Uris/askcore_graph.h>
#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/unit.h>
#include <knowGIS/Uris/askcore_sgf.h>
#include <knowGIS/Uris/geo.h>

#include "Feature.h"

KDB_REPOSITORY_REGISTER_FOCUS_NODE_DECLARATIONS("qrc:///kdbgis/data/features_shacl.ttl");

using namespace kDBGIS::Features;

using askcore_sgf = knowGIS::Uris::askcore_sgf;
using askcore_graph = knowCore::Uris::askcore_graph;
using geo = knowGIS::Uris::geo;
using rdf = knowCore::Uris::rdf;
using unit = knowCore::Uris::unit;

using FocusNode = kDB::Repository::RDF::FocusNode;
using FocusNodeCollection = kDB::Repository::RDF::FocusNodeCollection;

knowCore::Uri Collection::collectionType() { return askcore_sgf::collection; }

knowCore::Uri Collection::allFocusNodesView() { return askcore_graph::all_simple_features; }

knowCore::UriList Collection::containedTypes() { return {knowCore::Uri(askcore_sgf::feature)}; }

knowCore::Uri Collection::primaryType() { return askcore_sgf::feature; }

cres_qresult<knowCore::UriList>
  Collection::defaultDatatypes(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>&)
{
  return cres_success<knowCore::UriList>({primaryType()});
}

Collection::Collection() {}

Collection::Collection(const kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>& _rhs)
    : kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>(_rhs)
{
}

Collection::Collection(const Collection& _rhs)
    : kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>(_rhs)
{
}

Collection& Collection::operator=(const Collection& _rhs)
{
  kDB::Repository::RDF::FocusNodeCollectionWrapper<Collection>::operator=(_rhs);
  return *this;
}

Collection::~Collection() {}

Collection Collection::allFeatures(const kDB::Repository::Connection& _connection)
{
  return get(_connection, allFocusNodesView()).expect_success();
}

cres_qresult<Feature> Collection::salientRegion(const knowCore::Uri& _salientregionUri) const
{
  return focusNode(_salientregionUri);
}

cres_qresult<bool> Collection::hasFeature(const knowCore::Uri& _salientregionUri) const
{
  return hasFocusNode(_salientregionUri);
}

cres_qresult<QList<Feature>>
  Collection::features(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
                       const OperatorOptions& _operatorOptions) const
{
  return focusNodes(_constraints, _operatorOptions);
}

cres_qresult<Feature> Collection::createFeature(const knowGIS::GeometryObject& _geometry,
                                                const knowCore::ValueHash& _properties,
                                                const knowCore::Uri& _featureUri)

{
  return createFocusNode(askcore_sgf::feature,
                         {
                           geo::hasGeometry,
                           _geometry,
                           askcore_sgf::has_property,
                           _properties,
                         },
                         _featureUri);
}

namespace
{
  struct Node
  {
    double lat, lon;
    knowCore::ValueHash properties;
  };
  struct Way
  {
    QList<qsizetype> geometry;
    knowCore::ValueHash properties;
  };
} // namespace

#define CHECK_READER_ERROR()                                                                       \
  if(reader.hasError())                                                                            \
  {                                                                                                \
    return cres_failure("Parsing of overpass data failed with error: '{}'", reader.errorString()); \
  }                                                                                                \
  (void)0

namespace
{
  cres_qresult<void> setProperty(knowCore::ValueHash* _properties, const QString& k,
                                 const QString& v)
  {
    if(k == "ele_local" or k == "height" or k.contains("socket") or k == "capacity")
    {
      cres_qresult<knowCore::QuantityNumber> qn = knowCore::QuantityNumber::fromRdfLiteral(v);
      if(qn.is_successful())
      {
        knowCore::QuantityNumber qnq = qn.get_value();
        if((k == "ele_local" or k == "height") and qnq.unit() == knowCore::Unit::empty())
        {
          qnq
            = knowCore::QuantityNumber(qnq.value(), knowCore::Unit::bySymbol("m").expect_success());
        }
        _properties->insert("http://askco.re/gsf/osm/k#" + k, qnq);
      }
      else
      {
        clog_warning("Failed to parse {} as unit number {}", v, qn.get_error());
        _properties->insert("http://askco.re/gsf/osm/k#" + k, v);
      }
    }
    else if(k.contains("postcode"))
    {
      _properties->insert("http://askco.re/gsf/osm/k#" + k, QString(v).replace(" ", "").toInt());
    }
    else if(k == "start_date")
    {
      _properties->insert("http://askco.re/gsf/osm/k#" + k,
                          knowCore::Timestamp::from(QDateTime::fromString(v)));
    }
    else if(k.contains("addr") or k.contains("name") or k.contains("title") or k == "ref"
            or k == "source" or k == "source2" or k == "operator" or k == "opening_hours"
            or k == "note" or k == "fixme" or k == "FIXME" or k.contains("wikipedia")
            or k.contains("brand") or k == "seamark:topmark:shape" or k == "phone"
            or k.contains("information") or k.contains("description") or k == "artist_name"
            or k == "owner" or k == "charge" or k == "designation" or k == "species"
            or k == "feature" or k.contains("contact"))
    {
      _properties->insert("http://askco.re/gsf/osm/k#" + k, v);
    }
    else if(k == "reference" or k == "seamark:light:reference")
    {
      _properties->insert(
        "http://askco.re/gsf/osm/k#" + k,
        knowCore::Uri("http://askco.re/gsf/osm/v#" + QString(v).replace(" ", "_")));
    }
    else
    {
      _properties->insert("http://askco.re/gsf/osm/k#" + k,
                          knowCore::Uri("http://askco.re/gsf/osm/v#" + v));
    }
    return cres_success();
  }
} // namespace

cres_qresult<void> Collection::importFromOverpass(QIODevice* _device)
{
  if(not _device->isOpen())
  {
    if(not _device->open(QIODevice::ReadOnly))
    {
      return cresError("Device is not open for reading and failed to open the device.");
    }
  }
  QXmlStreamReader reader(_device);
  reader.readNextStartElement();
  CHECK_READER_ERROR();
  if(reader.name() != "osm"_kCs)
  {
    return cres_failure("Expected <osm> got <{}>.", reader.name());
  }
  QHash<qsizetype, Node> id2node;
  QHash<qsizetype, Way> id2way;
  while(reader.readNextStartElement())
  {
    CHECK_READER_ERROR();
    if(reader.name() == "meta"_kCs or reader.name() == "note"_kCs
       or reader.name() == "relation"_kCs)
    {
      reader.skipCurrentElement();
    }
    else if(reader.name() == "node"_kCs)
    {
      qsizetype id = reader.attributes().value("id").toULongLong();
      Node n;
      n.lat = reader.attributes().value("lat").toDouble();
      n.lon = reader.attributes().value("lon").toDouble();

      while(reader.readNextStartElement())
      {
        if(reader.name() == "tag"_kCs)
        {
          QString k = reader.attributes().value("k").toString();
          QString v = reader.attributes().value("v").toString();
          cres_try(cres_ignore, setProperty(&n.properties, k, v));
          reader.skipCurrentElement();
        }
        else
        {
          return cres_failure("Unhandled tag <{}> in <node>", reader.name());
        }
      }
      id2node[id] = n;
    }
    else if(reader.name() == "way"_kCs)
    {
      qsizetype id = reader.attributes().value("id").toULongLong();
      Way w;
      while(reader.readNextStartElement())
      {
        if(reader.name() == "tag"_kCs)
        {
          QString k = reader.attributes().value("k").toString();
          QString v = reader.attributes().value("v").toString();
          cres_try(cres_ignore, setProperty(&w.properties, k, v));
          reader.skipCurrentElement();
        }
        else if(reader.name() == "nd"_kCs)
        {
          w.geometry.append(reader.attributes().value("ref").toULongLong());
          reader.skipCurrentElement();
        }
        else
        {
          return cres_failure("Unhandled tag <{}> in <node>", reader.name());
        }
      }
      id2way[id] = w;
    }
    else
    {
      return cres_failure("Unhandled tag <{}> in <osm>", reader.name());
    }
  }
  CHECK_READER_ERROR();
  for(auto it = id2node.begin(); it != id2node.end(); ++it)
  {
    if(it->properties.size() > 0)
    {
      Cartography::EuclidSystem::point pt(it->lat, it->lon);
      knowCore::Uri u(clog_qt::qformat("http://askco.re/gsf/osm/{}", it.key()));
      cres_try(bool hFu, hasFeature(u));
      if(not hFu)
      {
        cres_try(cres_ignore,
                 createFeature(knowGIS::GeometryObject(Cartography::CoordinateSystem::wgs84, pt),
                               it->properties, u));
      }
    }
  }
  for(auto it = id2way.begin(); it != id2way.end(); ++it)
  {
    if(it->geometry.size() >= 2)
    {
      Cartography::EuclidSystem::geometry geom;
      if(it->geometry.first() == it->geometry.last())
      {
        Cartography::EuclidSystem::polygon_builder pb;
        std::size_t count = 0;
        for(size_t ref : it->geometry)
        {
          if(id2node.contains(ref))
          {
            Node n = id2node.value(ref);
            pb.add_point(n.lat, n.lon);
            ++count;
          }
        }
        if(count > 2)
        {
          geom = pb.create();
        }
      }
      else
      {
        Cartography::EuclidSystem::line_string_builder ls;
        std::size_t count = 0;
        for(size_t ref : it->geometry)
        {
          if(id2node.contains(ref))
          {
            Node n = id2node.value(ref);
            ls.add_point(n.lat, n.lon);
            ++count;
          }
        }
        if(count > 1)
        {
          geom = ls.create();
        }
      }
      if(not geom.is_null())
      {
        knowCore::Uri u(clog_qt::qformat("http://askco.re/gsf/osm/{}", it.key()));
        cres_try(bool hFu, hasFeature(u));
        if(not hFu)
        {
          cres_try(cres_ignore, createFeature(knowGIS::GeometryObject(
                                                Cartography::CoordinateSystem::wgs84, geom),
                                              it->properties, u));
        }
      }
    }
  }
  return cres_success();
}

#include "moc_Collection.cpp"
