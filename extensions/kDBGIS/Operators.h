namespace kDBGIS
{
  template<typename _T_>
  struct overlaps;
  template<typename _T_>
  struct intersects;
  template<typename _T_>
  struct within;
} // namespace kDBGIS
