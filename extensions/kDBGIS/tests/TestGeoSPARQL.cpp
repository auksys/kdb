#include "TestGeoSPARQL.h"

#include <knowCore/Messages.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/Test.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/TripleStreamInserter.h>
#include <kDB/Repository/tests/CompareSPARQLResults.h>

#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uris/askcore_db.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Test.h>
#include <knowRDF/Triple.h>
#include <knowRDF/TripleStream.h>

#include <knowDBC/Query.h>

#include <knowGIS/GeometryObject.h>
#include <knowGIS/Uris/geo.h>

#include <kDBGIS/Initialise.h>

void TestGeoSPARQL::testInsert()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBGIS"));

  kDB::Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());

  knowRDF::Triple triple1("http://test/path/to/subject"_kCu, knowGIS::Uris::geo::hasGeometry,
                          CRES_QVERIFY(knowRDF::Literal::fromValue(
                            knowGIS::Uris::geo::Geometry,
                            CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT (30 10)")))));

  CRES_QVERIFY(interface.insert(triple1));

  QList<knowRDF::Triple> triples = CRES_QVERIFY(interface.triples());

  QCOMPARE(triples.size(), 1);
  QCOMPARE(triples[0], triple1);

//   const char* large_wkt =
// #include "TestGeoSPSRAQL_LargeWKT.h"
//     ;
//   knowGIS::GeometryObject large_go
//     = CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(QString::fromLocal8Bit(large_wkt)));

//   knowRDF::Triple triple2("http://test/path/to/subject"_kCu, knowGIS::Uris::geo::hasGeometry,
//                           knowRDF::Literal::fromValue(large_go));
//   CRES_QVERIFY(interface.insert(triple2));

  const char* large_ls_wkt =
#include "TestGeoSPARQL_LargeLineString.h"
    ;
  knowGIS::GeometryObject large_ls_go
    = CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(QString::fromLocal8Bit(large_ls_wkt)));

  knowRDF::Triple triple3("http://test/path/to/subject"_kCu, knowGIS::Uris::geo::hasGeometry,
                          knowRDF::Literal::fromValue(large_ls_go));
  CRES_QVERIFY(interface.insert(triple3));
}

void TestGeoSPARQL::testRDFInsert()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBGIS"));

  kDB::Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());

  kDB::Repository::TripleStreamInserter tsi(interface);

  knowRDF::TripleStream stream;
  stream.addListener(&tsi);
  QFile file(":/test_wkt.ttl");
  file.open(QIODevice::ReadOnly);

  CRES_QVERIFY(stream.start(&file, nullptr, knowCore::FileFormat::Turtle));

  QList<knowRDF::Triple> triples = CRES_QVERIFY(interface.triples());

  knowRDF::Triple triple1(knowCore::Uri("http://test/path/to/subject"),
                          knowGIS::Uris::geo::hasGeometry,
                          CRES_QVERIFY(knowRDF::Literal::fromValue(
                            knowGIS::Uris::geo::wktLiteral,
                            CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT (30 10)")))));

  QCOMPARE(triples.size(), 1);
  QCOMPARE(triples[0], triple1);
}

void TestGeoSPARQL::testQuery()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBGIS"));

  kDB::Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());

  knowRDF::Triple triple1(knowCore::Uri("http://test/path/to/subject1"),
                          knowGIS::Uris::geo::hasGeometry,
                          CRES_QVERIFY(knowRDF::Literal::fromValue(
                            knowGIS::Uris::geo::wktLiteral,
                            CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT (30 10)")))));

  interface.insert(triple1);
  knowRDF::Triple triple2(knowCore::Uri("http://test/path/to/subject2"),
                          knowGIS::Uris::geo::hasGeometry,
                          CRES_QVERIFY(knowRDF::Literal::fromValue(
                            knowGIS::Uris::geo::wktLiteral,
                            CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT (300 100)")))));

  interface.insert(triple2);

  knowDBC::Query q = c.createSPARQLQuery();
  q.loadFromFile(":/geo_within.sparql");

  knowDBC::Result r = q.execute();

  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.value(0, 0).value<QString>(), (QString)triple1.subject().uri());
  CRES_QCOMPARE(r.value(0, 1).value<knowRDF::Literal>(), triple1.object().literal());

  // Check with srx results
  QFile f_srx(":/geo_within_results.srx");
  knowDBC::Result r_srx = CRES_QVERIFY(knowDBC::Result::read(&f_srx, knowCore::FileFormat::SRX));

  QCOMPARE(r.tuples(), r_srx.tuples());
  QCOMPARE(r.fields(), r_srx.fields());
  QVERIFY(compare_tuples(r, r_srx));

  QBuffer tmp_srx;
  QCOMPARE(r.type(), knowDBC::Result::Type::VariableBinding);
  CRES_QVERIFY(r.write(&tmp_srx, knowCore::FileFormat::SRX));
  tmp_srx.close();
  knowDBC::Result r_srx_2
    = CRES_QVERIFY(knowDBC::Result::read(&tmp_srx, knowCore::FileFormat::SRX));

  QCOMPARE(r.tuples(), r_srx_2.tuples());
  QCOMPARE(r.fields(), r_srx_2.fields());
  QVERIFY(compare_tuples(r, r_srx_2));

  // Check with JSON results
  QFile f_json(":/geo_within_results.json");
  knowDBC::Result r_json = CRES_QVERIFY(knowDBC::Result::read(&f_json, knowCore::FileFormat::JSON));

  QCOMPARE(r.tuples(), r_json.tuples());
  QCOMPARE(r.fields(), r_json.fields());
  QVERIFY(compare_tuples(r, r_json));

  QBuffer tmp_json;
  CRES_QVERIFY(r.write(&tmp_json, knowCore::FileFormat::JSON));
  tmp_json.close();
  knowDBC::Result r_json_2
    = CRES_QVERIFY(knowDBC::Result::read(&tmp_json, knowCore::FileFormat::JSON));

  QCOMPARE(r.tuples(), r_json_2.tuples());
  QCOMPARE(r.fields(), r_json_2.fields());
  QVERIFY(compare_tuples(r, r_json_2));
}

void TestGeoSPARQL::testWktLiteral()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBGIS"));

  kDB::Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());

  knowGIS::GeometryObject point_go
    = CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POINT (30 10)"));

  knowRDF::Triple triple1(
    knowCore::Uri("http://test/path/to/subject1"), knowGIS::Uris::geo::hasGeometry,
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowGIS::Uris::geo::wktLiteral, point_go)));

  interface.insert(triple1);

  knowDBC::Query q = interface.createSPARQLQuery();
  q.setQuery("SELECT ?x ?y ?z WHERE { ?x ?y ?z . }");
  knowDBC::Result r = q.execute();
  VERIFY_QUERY_RESULT(r);

  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 3);
  CRES_QCOMPARE(r.value<knowCore::Uri>(0, 0), triple1.subject().uri());
  CRES_QCOMPARE(r.value<knowCore::Uri>(0, 1), triple1.predicate());
  QCOMPARE(r.value(0, 2), triple1.object().literal());

  // Test union
  q.setQuery("SELECT (<http://www.opengis.net/def/function/geosparql/union>(?z, ?z) AS ?r) WHERE { "
             "?x ?y ?z . }");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);

  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.value(0, 0), knowCore::Value::fromValue(point_go));

  // Test union aggregate
  q.setQuery("SELECT (<http://www.opengis.net/def/function/geosparql/union>(?z) AS ?r) WHERE { ?x "
             "?y ?z . }");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);

  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.value(0, 0), triple1.object().literal());
}

void TestGeoSPARQL::testSQLFunctions()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBGIS"));

  knowDBC::Query q = c.createSQLQuery(
    "SELECT kdbgis_geo_sfOverlaps(kdb_rdf_term_create(:a), kdb_rdf_term_create(:b))");
  q.bindValue(":a", CRES_QVERIFY(
                      knowGIS::GeometryObject::fromWKT("POLYGON ((0 0, 10 0, 10 10, 0 10, 0 0))")));
  q.bindValue(":b", CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(
                      "POLYGON ((-5 -5, 5 -5, 5 5, -5 5, -5 -5))")));
  knowDBC::Result r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 1);
  CRES_QCOMPARE(r.value<bool>(0, 0), true);

  q.setQuery("SELECT kdbgis_geo_sfWithin(kdb_rdf_term_create(:a), kdb_rdf_term_create(:b))", true);
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 1);
  CRES_QCOMPARE(r.value<bool>(0, 0), false);

  q.setQuery("SELECT kdbgis_geo_union(kdb_rdf_term_create(:a), kdb_rdf_term_create(:b))", true);
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 1);
  CRES_QCOMPARE(r.value<knowGIS::GeometryObject>(0, 0),
                CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(
                  "POLYGON ((0 10, 10 10, 10 0, 5 0, 5 -5, -5 -5, -5 5, 0 5, 0 10))")));
}

void TestGeoSPARQL::testSPARQLFunctions()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBGIS"));

  knowDBC::Query q = c.createSPARQLQuery();
  q.setQuery(
    "SELECT (<http://www.opengis.net/def/function/geosparql/sfOverlaps>(%a, %b) AS ?r) {}");
  q.bindValue("%a", CRES_QVERIFY(
                      knowGIS::GeometryObject::fromWKT("POLYGON ((0 0, 10 0, 10 10, 0 10, 0 0))")));
  q.bindValue("%b", CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(
                      "POLYGON ((-5 -5, 5 -5, 5 5, -5 5, -5 -5))")));
  knowDBC::Result r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 1);
  CRES_QCOMPARE(r.value<bool>(0, 0), true);

  q.setQuery("SELECT (<http://www.opengis.net/def/function/geosparql/union>(%a, %b) AS ?r) {}",
             true);
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 1);
  CRES_QCOMPARE(r.value<knowGIS::GeometryObject>(0, 0),
                CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(
                  "POLYGON ((0 10, 10 10, 10 0, 5 0, 5 -5, -5 -5, -5 5, 0 5, 0 10))")));

  q.bindValue(
    "%a", CRES_QVERIFY(knowRDF::Literal::fromValue(knowGIS::Uris::geo::wktLiteral,
                                                   CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(
                                                     "POLYGON ((0 0, 10 0, 10 10, 0 10, 0 0))")))));
  q.bindValue("%b",
              CRES_QVERIFY(knowRDF::Literal::fromValue(
                knowGIS::Uris::geo::wktLiteral, CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(
                                                  "POLYGON ((-5 -5, 5 -5, 5 5, -5 5, -5 -5))")))));
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.tuples(), 1);
  QCOMPARE(r.fields(), 1);
  CRES_QCOMPARE(r.value<knowGIS::GeometryObject>(0, 0),
                CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(
                  "POLYGON ((0 10, 10 10, 10 0, 5 0, 5 -5, -5 -5, -5 5, 0 5, 0 10))")));
}

QTEST_MAIN(TestGeoSPARQL)
