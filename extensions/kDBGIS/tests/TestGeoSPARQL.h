#include <QtTest/QtTest>

class TestGeoSPARQL : public QObject
{
  Q_OBJECT
private slots:
  void testInsert();
  void testRDFInsert();
  void testQuery();
  void testWktLiteral();
  void testSQLFunctions();
  void testSPARQLFunctions();
};
