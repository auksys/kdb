#include <QtTest/QtTest>

class TestFeatures : public QObject
{
  Q_OBJECT
private slots:
  void testFeatureCreation();
  void testParsingOverpass();
};
