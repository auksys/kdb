#include "TestFeatures.h"

#include <knowCore/QuantityValue.h>
#include <knowCore/Test.h>
#include <knowCore/Uri.h>

#include <knowGIS/GeometryObject.h>

#include <knowRDF/Literal.h>

#include <kDB/Repository/TemporaryStore.h>

#include <kDBGIS/Features/Collection.h>
#include <kDBGIS/Features/Feature.h>

#include <knowCore/Uris/unit.h>
#include <knowCore/Uris/xsd.h>

void TestFeatures::testFeatureCreation()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBGIS"));
  // Create a collection of features

  kDBGIS::Features::Collection col1
    = CRES_QVERIFY(kDBGIS::Features::Collection::create(c, "test_features"_kCu));
  QCOMPARE(col1.isValid(), true);

  // Add feature
  const char* large_wkt =
#include "TestGeoSPSRAQL_LargeWKT.h"
    ;

  knowCore::ValueHash properties;
  properties.insert("hello", "world"_kCs);

  knowGIS::GeometryObject large_go
    = CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(QString::fromLocal8Bit(large_wkt)));

  CRES_QVERIFY(col1.createFeature(large_go, properties));

  // Add feature
  const char* large_ls_wkt =
#include "TestGeoSPARQL_LargeLineString.h"
    ;

  knowGIS::GeometryObject large_ls_go
    = CRES_QVERIFY(knowGIS::GeometryObject::fromWKT(QString::fromLocal8Bit(large_ls_wkt)));

  CRES_QVERIFY(col1.createFeature(large_ls_go, properties));
}

void TestFeatures::testParsingOverpass()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  CRES_QVERIFY(c.enableExtension("kDBGIS"));

  // Create a collection of features

  kDBGIS::Features::Collection col1
    = CRES_QVERIFY(kDBGIS::Features::Collection::create(c, "test_features"_kCu));
  QCOMPARE(col1.isValid(), true);

  QFile f(":/overpass_test_data");
  QVERIFY(f.open(QIODevice::ReadOnly));

  CRES_QVERIFY(col1.importFromOverpass(&f));

  QList<kDBGIS::Features::Feature> all = CRES_QVERIFY(col1.all());
  QCOMPARE(all.size(), 113);
  int checked = 0;
  for(kDBGIS::Features::Feature f : all)
  {
    if(f.uri() == "http://askco.re/gsf/osm/11459931656")
    {
      ++checked;
      CRES_QCOMPARE(
        f.geometry(),
        knowGIS::GeometryObject::fromWKT("POINT (51.477887 -0.000109)").expect_success());
      knowCore::ValueHash vh = CRES_QVERIFY(f.featureProperties());
      QCOMPARE(vh.size(), 1);
      knowCore::Value val = vh.value("http://askco.re/gsf/osm/k#natural");
      QCOMPARE(CRES_QVERIFY(val.value<knowRDF::Literal>()).datatype(), knowCore::Uris::xsd::anyURI);
      CRES_QCOMPARE(vh.value<knowCore::Uri>("http://askco.re/gsf/osm/k#natural"),
                    "http://askco.re/gsf/osm/v#tree"_kCu);
    }
    else if(f.uri() == "http://askco.re/gsf/osm/64754142")
    {
      ++checked;
      CRES_QCOMPARE(
        f.geometry(),
        knowGIS::GeometryObject::fromWKT("POINT (51.4777406 -0.000476)").expect_success());
      knowCore::ValueHash vh = CRES_QVERIFY(f.featureProperties());
      QCOMPARE(vh.size(), 1);
      CRES_QCOMPARE(vh.value<knowCore::QuantityNumber>("http://askco.re/gsf/osm/k#ele_local"),
                    CRES_QVERIFY(knowCore::QuantityNumber::create(50.73, knowCore::Uris::unit::M)));
    }
    else if(f.uri() == "http://askco.re/gsf/osm/968253937")
    {
      ++checked;
      CRES_QCOMPARE(
        f.geometry(),
        knowGIS::GeometryObject::fromWKT(
          "POLYGON ((51.4773616 -0.0000001, 51.4774524 0.0000166, 51.4775612 0.0000419, 51.4776157 "
          "0.0000568, 51.477669 0.0000746, 51.4776697 0.0000626, 51.4776704 0.0000507, 51.4776176 "
          "0.0000363, 51.4775628 0.0000229, 51.4774539 -0.000002, 51.4773621 -0.0000186, "
          "51.4772862 -0.0000283, 51.4772867 -0.0000187, 51.4772871 -0.0000091, 51.4773616 "
          "-0.0000001, 51.4773616 -0.0000001))")
          .expect_success());
      knowCore::ValueHash vh = CRES_QVERIFY(f.featureProperties());
      QCOMPARE(vh.size(), 5);
      knowCore::Value val = vh.value("http://askco.re/gsf/osm/k#lit");
      QCOMPARE(CRES_QVERIFY(val.value<knowRDF::Literal>()).datatype(), knowCore::Uris::xsd::anyURI);
      CRES_QCOMPARE(vh.value<knowCore::Uri>("http://askco.re/gsf/osm/k#lit"),
                    "http://askco.re/gsf/osm/v#no"_kCu);
      CRES_QCOMPARE(vh.value<knowCore::Uri>("http://askco.re/gsf/osm/k#area:highway"),
                    "http://askco.re/gsf/osm/v#footway"_kCu);
      CRES_QCOMPARE(vh.value<knowCore::Uri>("http://askco.re/gsf/osm/k#bicycle"),
                    "http://askco.re/gsf/osm/v#dismount"_kCu);
      CRES_QCOMPARE(vh.value<knowCore::Uri>("http://askco.re/gsf/osm/k#surface"),
                    "http://askco.re/gsf/osm/v#asphalt"_kCu);
      CRES_QCOMPARE(vh.value<knowCore::Uri>("http://askco.re/gsf/osm/k#foot"),
                    "http://askco.re/gsf/osm/v#yes"_kCu);
    }
    else if(f.uri() == "http://askco.re/gsf/osm/268533482")
    {
      ++checked;
      CRES_QCOMPARE(f.geometry(), knowGIS::GeometryObject::fromWKT(
                                    "LINESTRING (51.4771351 -0.0004566, 51.4771156 -0.0005147, "
                                    "51.4770976 -0.0005149, 51.4770926 -0.0005154)")
                                    .expect_success());
      knowCore::ValueHash vh = CRES_QVERIFY(f.featureProperties());
      QCOMPARE(vh.size(), 1);
      CRES_QCOMPARE(vh.value<knowCore::Uri>("http://askco.re/gsf/osm/k#highway"),
                    "http://askco.re/gsf/osm/v#footway"_kCu);
    }
  }
  QCOMPARE(checked, 4);
}

QTEST_MAIN(TestFeatures)
