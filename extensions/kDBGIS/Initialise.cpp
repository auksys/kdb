#include "Initialise.h"

#include <clog_qt>
#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/xsd.h>

#include <knowGIS/Uris/geo.h>
#include <knowGIS/Uris/geof.h>

#include <kDB/Repository/ActiveRecord/Version.h>
#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/SPARQLFunctionsManager.h>

#include "GeometryFieldIO_p.h"

#include "DEM.h"
#include "OrthoImage.h"

inline void kdbgis_init_data() { Q_INIT_RESOURCE(kdbgis_data); }

namespace kDBGIS
{
  bool initialise()
  {
    static bool initialised = false;
    if(not initialised)
    {
      kdbgis_init_data();
    }
    initialised = true;
    return true;
  }

  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection)
  {
    if(not initialise())
      return cres_failure("Could not load kDBGIS data.");

    kDB::Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::registerField(
      _connection, knowGIS::Uris::geo::Geometry, new GeometryFieldIO,
      (quint64)
        kDB::Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::Operator::Equal,
      (quint64)kDB::Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::Feature::
        GistIndexable);

    cres_try(kDB::Repository::ActiveRecord::VersionRecord vr,
             kDB::Repository::ActiveRecord::VersionRecord::get(_connection, "kDBGIS"));

    if(vr.striclyLessThan(2, 0, 0))
    {
      cres_try(cres_ignore, _connection.executeQueryFromFile(":/kdbgis/sql/types.sql"));
    }
    if(vr.striclyLessThan(3, 1, 1))
    {
      cres_try(cres_ignore,
               _connection.executeQueryFromFile(":/kdbgis/sql/geo_sparql_functions.sql"));
    }
    if(vr.striclyLessThan(4, 1, 1))
    {
      cres_try(cres_ignore, DEMRecord::createTable(_connection));
      cres_try(cres_ignore, OrthoImageRecord::createTable(_connection));
    }

    _connection.sparqlFunctionsManager()->registerFunction(
      knowGIS::Uris::geof::sfWithin, "kdbgis_geo_sfWithin", knowCore::Uris::xsd::boolean,
      knowCore::Uris::askcore_datatype::literal, knowCore::Uris::askcore_datatype::literal);
    _connection.sparqlFunctionsManager()->registerFunction(
      knowGIS::Uris::geof::sfOverlaps, "kdbgis_geo_sfOverlaps", knowCore::Uris::xsd::boolean,
      knowCore::Uris::askcore_datatype::literal, knowCore::Uris::askcore_datatype::literal);
    _connection.sparqlFunctionsManager()->registerFunction(
      knowGIS::Uris::geof::union_, "kdbgis_geo_union", knowCore::Uris::askcore_datatype::literal,
      knowCore::Uris::askcore_datatype::literal, knowCore::Uris::askcore_datatype::literal);
    _connection.sparqlFunctionsManager()->registerFunction(
      knowGIS::Uris::geof::union_, "kdbgis_geo_union", knowCore::Uris::askcore_datatype::literal,
      knowCore::Uris::askcore_datatype::literal);
    _connection.sparqlFunctionsManager()->registerFunction(
      knowGIS::Uris::geof::difference, "kdbgis_geo_difference",
      knowCore::Uris::askcore_datatype::literal, knowCore::Uris::askcore_datatype::literal,
      knowCore::Uris::askcore_datatype::literal);
    _connection.sparqlFunctionsManager()->registerFunction(
      knowGIS::Uris::geof::sfIntersects, "kdbgis_geo_sfIntersects", knowCore::Uris::xsd::boolean,
      knowCore::Uris::askcore_datatype::literal, knowCore::Uris::askcore_datatype::literal);
    _connection.sparqlFunctionsManager()->registerFunction(
      knowGIS::Uris::geof::sfIntersects, "kdbgis_geo_sfIntersects", knowCore::Uris::xsd::boolean,
      knowCore::Uris::askcore_datatype::literal, knowCore::Uris::askcore_datatype::literal,
      knowCore::Uris::xsd::float64);
    _connection.sparqlFunctionsManager()->registerFunction(
      knowGIS::Uris::geof::getSRID, "kdbgis_get_srid", knowCore::Uris::xsd::integer,
      knowCore::Uris::askcore_datatype::literal);

    vr.updateToCurrent();

    return cres_success();
  }

} // namespace kDBGIS

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBGIS)
