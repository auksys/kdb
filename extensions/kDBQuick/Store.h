#ifndef _KDBQUICK_STORE_H_
#define _KDBQUICK_STORE_H_

#include <knowDBC/Quick/Store.h>

#include <knowCore/Forward.h>

namespace kDBQuick
{
  class Connection;
  class Store : public knowDBC::Quick::Store
  {
    Q_OBJECT
    Q_PROPERTY(QString hostname READ hostname WRITE setHostname NOTIFY hostnameChanged)
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)
    Q_PROPERTY(bool autoSelectPort READ isAutoSelectPort WRITE setAutoSelectPort NOTIFY
                 autoSelectPortChanged)
  public:
    Store(QObject* _parent = nullptr);
    ~Store();
    QString hostname() const;
    void setHostname(const QString& _value);
    int port() const;
    void setPort(int _port);
    bool isAutoSelectPort() const;
    /**
     * Try to select the port automatically if a port is already set in the directory of the store.
     * Otherwise, attempt to use the one set in port.
     */
    void setAutoSelectPort(bool _v);
    Q_INVOKABLE bool start() override;
    Q_INVOKABLE bool stop() override;
    Q_INVOKABLE bool erase() override;
    bool isStoreRunning() const override;
  private slots:
    void connectIfNeeded();
    void updateStore() const;
  signals:
    void hostnameChanged();
    void portChanged();
    void autoSelectPortChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBQuick

#endif
