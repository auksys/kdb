#include "TripleStoreRevision.h"

#include <QUuid>
#include <knowCore/Timestamp.h>

#include <kDB/Repository/VersionControl/Delta.h>
#include <kDB/Repository/VersionControl/Revision.h>

#include "TripleStoreDelta.h"

using namespace kDBQuick;

struct TripleStoreRevision::Private
{
  const TripleStore* store;
  kDB::Repository::VersionControl::Revision revision;
  mutable QList<QObject*> deltas;
};

TripleStoreRevision::TripleStoreRevision(const TripleStore* _store,
                                         const kDB::Repository::VersionControl::Revision& _revision,
                                         QObject* _parent)
    : QObject(_parent), d(new Private)
{
  d->store = _store;
  d->revision = _revision;
}

TripleStoreRevision::~TripleStoreRevision() { delete d; }

QByteArray TripleStoreRevision::hash() const { return d->revision.hash(); }

QList<QObject*> TripleStoreRevision::deltas() const
{
  if(d->deltas.isEmpty())
  {
    for(const kDB::Repository::VersionControl::Delta& delta : d->revision.deltas())
    {
      d->deltas.append(new TripleStoreDelta(this, delta, const_cast<TripleStoreRevision*>(this)));
    }
  }
  return d->deltas;
}

QByteArray TripleStoreRevision::contentHash() const { return d->revision.contentHash(); }

int TripleStoreRevision::historicity() const { return d->revision.historicity(); }

#include "moc_TripleStoreRevision.cpp"
