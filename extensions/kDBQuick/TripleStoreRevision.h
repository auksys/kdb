#pragma once

#include <QObject>

namespace kDB::Repository::VersionControl
{
  class Revision;
}

namespace kDBQuick
{
  class TripleStore;
  class TripleStoreRevision : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QByteArray hash READ hash CONSTANT)
    Q_PROPERTY(QByteArray contentHash READ contentHash CONSTANT)
    Q_PROPERTY(int historicity READ historicity CONSTANT)
    Q_PROPERTY(QList<QObject*> deltas READ deltas CONSTANT)
  public:
    TripleStoreRevision(const TripleStore* _store,
                        const kDB::Repository::VersionControl::Revision& _revision,
                        QObject* _parent);
    ~TripleStoreRevision();
  public:
    QByteArray hash() const;
    QByteArray contentHash() const;
    int historicity() const;
    QList<QObject*> deltas() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBQuick
