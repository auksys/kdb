import QtQuick
import kDB.Repository

QtObject
{
  id: root
  
  property url queries
  property string queryName
  property string lastError
  
  onLastErrorChanged: console.log("Error while executing '" + root.queryName + "': " + lastError)
  
  property alias connection: data_query.connection
  property var bindings: {}
  property alias result: data_query.result
  property alias autoExecute: data_query.autoExecute
  
  function execute()
  {
    data_query.execute()
  }
  
  property var __query_query: SPARQLQuery
  {
    id: query_query
    connection: data_query.connection
    query: "PREFIX askcore_db_queries: <http://askco.re/db/queries#> PREFIX foaf: <http://xmlns.com/foaf/0.1/> SELECT ?query FROM %queries WHERE { ?query_uri askcore_db_queries:query ?query ; foaf:name %queryName . }"
    bindings: { '%queries': root.queries, '%queryName': root.queryName }
    autoExecute: true
    onLastErrorChanged: root.lastError = lastError
    onExecutionFinished: if(query_query.result.rowCount() > 0)
    {
      data_query.query = query_query.result.data(query_query.result.index(0,0), query_query.result.roleIndex('query'))["value"]
    }
  }
  property var __data_query: SPARQLQuery
  {
    id: data_query
    function percentagify(bin)
    {
      var pbin = {}
      for(const key in bin)
      {
        pbin['%' + key] = bin[key]
      }
      return pbin
    }
    bindings: percentagify(root.bindings)
    onLastErrorChanged: root.lastError = lastError
  }
}
