#pragma once

#include <QObject>

namespace kDB::Repository::VersionControl
{
  class Delta;
}

namespace kDBQuick
{
  class TripleStoreRevision;
  class TripleStoreDelta : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QByteArray hash READ hash CONSTANT)
    Q_PROPERTY(QByteArray delta READ delta CONSTANT)
    Q_PROPERTY(QByteArray parent READ parent CONSTANT)
    Q_PROPERTY(QVariantList signatures READ signatures CONSTANT)
  public:
    TripleStoreDelta(const TripleStoreRevision* _store,
                     const kDB::Repository::VersionControl::Delta& _delta, QObject* _parent);
    ~TripleStoreDelta();
  public:
    QByteArray hash() const;
    QByteArray delta() const;
    QByteArray parent() const;
    QVariantList signatures() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBQuick
