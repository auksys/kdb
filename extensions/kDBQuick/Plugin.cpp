#include "Plugin.h"

#include <QtQml>

#include <knowCore/Forward.h>

#include "Connection.h"
#include "Store.h"
#include "TripleStore.h"
#include "TriplesLoader.h"
#include "TriplesSaver.h"

#include <kDB/Version.h>

using namespace kDBQuick;

void Plugin::registerTypes(const char* /*uri*/)
{
  const char* uri_kDB_Repository = "kDB.Repository";
  qmlRegisterType<kDBQuick::Connection>(uri_kDB_Repository, KDB_VERSION_MAJOR, 0, "Connection");
  qmlRegisterType<kDBQuick::Store>(uri_kDB_Repository, KDB_VERSION_MAJOR, 0, "Store");
  qmlRegisterType<kDBQuick::TriplesLoader>(uri_kDB_Repository, KDB_VERSION_MAJOR, 0,
                                           "TriplesLoader");
  qmlRegisterType<kDBQuick::TriplesSaver>(uri_kDB_Repository, KDB_VERSION_MAJOR, 0, "TriplesSaver");
  qmlRegisterType<kDBQuick::TripleStore>(uri_kDB_Repository, KDB_VERSION_MAJOR, 0, "TripleStore");
}

#include "moc_Plugin.cpp"
