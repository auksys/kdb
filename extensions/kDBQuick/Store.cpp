#include "Store.h"

#include <QDir>

#include <cres_qt>
#include <kDB/Repository/Store.h>

#include "Connection.h"

using namespace kDBQuick;

struct Store::Private
{
  kDB::Repository::Store* store = nullptr;
  QString lastError;
  Connection* connection = nullptr;

  bool autoSelectPort = false;
};

Store::Store(QObject* _parent) : knowDBC::Quick::Store(new Connection, _parent), d(new Private)
{
  d->connection = static_cast<Connection*>(connection());
  QObject::connect(d->connection, SIGNAL(connectionStatusChanged()), this,
                   SIGNAL(storeRunningChanged()));
  QObject::connect(this, SIGNAL(hostnameChanged()), this, SIGNAL(storeRunningChanged()));
  QObject::connect(this, SIGNAL(portChanged()), this, SIGNAL(storeRunningChanged()));
  QObject::connect(d->connection, SIGNAL(hostnameChanged()), this, SIGNAL(hostnameChanged()));
  QObject::connect(d->connection, SIGNAL(hostnameChanged()), this, SLOT(connectIfNeeded()));
  QObject::connect(d->connection, SIGNAL(portChanged()), this, SIGNAL(portChanged()));
  QObject::connect(d->connection, SIGNAL(portChanged()), this, SLOT(connectIfNeeded()));
  QObject::connect(d->connection, SIGNAL(autoConnectChanged()), this, SLOT(connectIfNeeded()));
}

Store::~Store()
{
  delete d->store;
  delete d;
}

int Store::port() const { return d->connection->port(); }

void Store::setPort(int _port) { d->connection->setPort(_port); }

QString Store::hostname() const { return d->connection->hostname(); }

void Store::setHostname(const QString& _value) { d->connection->setHostname(_value); }

bool kDBQuick::Store::isAutoSelectPort() const { return d->autoSelectPort; }

void kDBQuick::Store::setAutoSelectPort(bool _v)
{
  d->autoSelectPort = _v;
  if(d->autoSelectPort)
  {
    if(d->store)
    {
      d->store->autoSelectPort();
      d->connection->setPort(d->store->port());
    }
  }
  connectIfNeeded();
  emit(autoSelectPortChanged());
}

bool Store::isStoreRunning() const
{
  updateStore();
  return d->store->isRunning();
}

void Store::connectIfNeeded()
{
  if(d->connection->isAutoConnect())
  {
    if(not d->store or (d->store and not d->store->isRunning()))
    {
      start();
    }
    d->connection->connect();
  }
}

void Store::updateStore() const
{
  if(d->store and d->store->directory() == hostname() and d->store->port() == port())
  {
    return;
  }
  if(d->store)
  {
    delete d->store;
    d->store = nullptr;
  }
  d->store = new kDB::Repository::Store(hostname(), port());
  if(d->autoSelectPort)
  {
    d->store->autoSelectPort();
    d->connection->setPort(d->store->port());
  }
}

bool Store::start()
{
  if(d->store and d->store->isRunning())
  {
    d->lastError = "Store is already running";
    return false;
  }
  updateStore();
  if(d->store->isRunning())
  {
    d->lastError = "Store is already running";
  }
  if(not updateLastError(d->store->start()))
  {
    delete d->store;
    d->store = nullptr;
    return false;
  }
  emit(storeRunningChanged());
  connectIfNeeded();
  return true;
}

bool Store::stop()
{
  updateStore();
  d->connection->disconnect();
  updateLastError(d->store->stop());
  if(d->store->isRunning())
  {
    updateLastError(d->store->stop(true));
  }
  if(d->store->isRunning())
  {
    return false;
  }
  delete d->store;
  d->store = nullptr;
  emit(storeRunningChanged());
  return true;
}

bool Store::erase()
{
  updateStore();
  bool s = false;
  if(not d->store->isRunning())
  {
    s = updateLastError(d->store->erase());
  }
  delete d->store;
  d->store = nullptr;
  return s;
}

#include "moc_Store.cpp"
