#include "Connection.h"

#include <QVariant>
#include <QtConcurrent/QtConcurrent>

#include <knowCore/Cast.h>
#include <knowCore/Uri.h>

#include <knowCore/Quick/Error.h>

#include <knowDBC/Connection.h>
#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/RDFView/ViewDefinition.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/Extensions.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TriplesView.h>

#include "Store.h"

using namespace kDBQuick;

struct Connection::Private
{
  kDB::Repository::Connection connection;

  QString hostname, lastError, name, section;
  int port = 1242;
};

Connection::Connection(QObject* _parent) : knowDBC::Quick::Connection(_parent), d(new Private) {}

Connection::~Connection() { delete d; }

void Connection::setHostname(const QString& _value)
{
  if(d->hostname != _value)
  {
    if(d->connection.isValid())
      d->connection.disconnect();
    d->hostname = _value;
    emit(hostnameChanged());
    connectIfNeeded();
  }
}

QString Connection::hostname() const { return d->hostname; }

QString Connection::storeName() const { return d->name; }

void Connection::setStoreName(const QString& _value)
{
  d->name = _value;
  emit(storeNameChanged());
}

QString Connection::sectionName() const { return d->section; }

void Connection::setSectionName(const QString& _value)
{
  d->section = _value;
  emit(sectionNameChanged());
}

void Connection::setPort(int _port)
{
  if(d->port != _port)
  {
    if(d->connection.isValid())
      d->connection.disconnect();
    d->port = _port;
    emit(portChanged());
    connectIfNeeded();
  }
}

int Connection::port() const { return d->port; }

bool Connection::isConnected() const
{
  return d->connection.isValid() and d->connection.isConnected();
}

kDB::Repository::Connection Connection::nativeConnection() const { return d->connection; }

knowDBC::Connection Connection::connection() const { return d->connection; }

void Connection::connectIfNeeded()
{
  if(isAutoConnect() and not qobject_cast<Store*>(parent()))
  {
    disconnect();
    connect();
  }
}

bool Connection::connect()
{
  if(d->connection.isValid())
    return false;
  if(d->name.isEmpty())
  {
    d->connection
      = kDB::Repository::Connection::create(kDB::Repository::HostName(hostname()), port());
  }
  else
  {
    d->connection = kDB::Repository::Connection::create(
      kDB::Repository::SectionName(sectionName()), kDB::Repository::StoreName(storeName()), port());
  }
  if(updateLastError(d->connection.connect()))
  {
    emit(connectionStatusChanged());
    return true;
  }
  else
  {
    d->connection = kDB::Repository::Connection();
    emit(connectionStatusChanged());
    return false;
  }
}

bool Connection::disconnect()
{
  if(d->connection.isValid())
  {
    d->connection.disconnect();
    d->connection = kDB::Repository::Connection();
    emit(connectionStatusChanged());
    return true;
  }
  else
  {
    return false;
  }
}

QStringList Connection::rdfGraphs() const
{
  return knowCore::listCast<QString>(
    d->connection.isValid() ? d->connection.graphsManager()->graphs() : QList<knowCore::Uri>());
}

QStringList Connection::sqlTables() const
{
  if(not d->connection.isValid())
    return QStringList();

  knowDBC::Query q
    = d->connection.createSQLQuery("SELECT table_name"
                                   " FROM information_schema.tables"
                                   " WHERE table_schema='public' ORDER BY table_name;");
  knowDBC::Result r = q.execute();
  QStringList l;
  for(int i = 0; i < r.tuples(); ++i)
  {
    l << r.value(i, 0).value<QString>().expect_success();
    ;
  }
  return l;
}

bool Connection::insertView(const QUrl& _filename)
{
  QFile file(_filename.toLocalFile());
  kDB::RDFView::ViewDefinition d1 = kDB::RDFView::ViewDefinition::parse(&file);

  if(d1.isValid())
  {
    return updateLastError(d->connection.graphsManager()->createView(d1).discard_value());
  }
  return false;
}

bool Connection::loadExtension(const QString& _name)
{
  if(d->connection.isValid())
  {
    return updateLastError(d->connection.enableExtension(_name));
  }
  else
  {
    knowCore::Quick::throwError(this, "Cannot call 'loadExtension' on an invalid connection.");
    return false;
  }
}

bool Connection::extensionEnabled(const QString& _name)
{
  return d->connection.isExtensionEnabled(_name);
}

#include "moc_Connection.cpp"
