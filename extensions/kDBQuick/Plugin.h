#pragma once

#include <QQmlExtensionPlugin>

namespace kDBQuick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "kDB/5.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace kDBQuick
