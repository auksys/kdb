#include "TripleStore.h"

#include "config.h"

#ifdef HAVE_CYQLOPS_GRAPHVIZ
#include <Cyqlops/Graphviz/Model/Graph.h>
#include <Cyqlops/Graphviz/Model/Node.h>
#endif

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/VersionControl/Delta.h>
#include <kDB/Repository/VersionControl/Manager.h>
#include <kDB/Repository/VersionControl/Revision.h>
#include <knowCore/Uri.h>

#include "Connection.h"
#include "TripleStoreRevision.h"

using namespace kDBQuick;

struct TripleStore::Private
{
  Connection* connection = nullptr;
  kDB::Repository::TripleStore store;
  QString graphName, lastError;
};

TripleStore::TripleStore(QObject* _object) : knowCore::Quick::Object(_object), d(new Private) {}

TripleStore::~TripleStore() { delete d; }

QObject* TripleStore::revisionsGraph()
{
#ifdef HAVE_CYQLOPS_GRAPHVIZ
  if(hasVersionControl())
  {
    Cyqlops::Graphviz::Model::Graph* graph = new Cyqlops::Graphviz::Model::Graph;
    cres_qresult<QList<kDB::Repository::VersionControl::Revision>> revisions
      = d->store.versionControlManager()->revisions();
    if(not updateLastError(revisions.discard_value()))
      return nullptr;
    QHash<QByteArray, Cyqlops::Graphviz::Model::Node*> nodes;
    for(const kDB::Repository::VersionControl::Revision& rev : revisions.get_value())
    {
      Cyqlops::Graphviz::Model::Node* n = new Cyqlops::Graphviz::Model::Node;
      n->setData(QVariant::fromValue(new TripleStoreRevision(this, rev, n)));
      nodes[rev.hash()] = n;
      graph->addNode(n);
    }

    for(const kDB::Repository::VersionControl::Revision& rev : revisions.get_value())
    {
      for(const kDB::Repository::VersionControl::Delta& delta : rev.deltas())
      {
        graph->createEdge(nodes[delta.parent()], nodes[rev.hash()]);
      }
    }

    return graph;
  }
  else
  {
    return nullptr;
  }
#else
  return nullptr;
#endif
}

Connection* TripleStore::connection() const { return d->connection; }

void TripleStore::setConnection(Connection* _connection)
{
  d->connection = _connection;
  emit(connectionChanged());
  updateStore();
}

QString TripleStore::graphName() { return d->graphName; }

void TripleStore::setGraphName(const QString& _graph)
{
  d->graphName = _graph;
  emit(graphNameChanged());
  updateStore();
}

bool TripleStore::isValid() const { return d->store.isValid(); }

bool TripleStore::hasVersionControl() const
{
  return isValid()
         and d->store.options().testFlag(kDB::Repository::TripleStore::Option::Versioning);
}

QByteArray TripleStore::tipHash()
{
  if(d->store.versionControlManager())
  {
    cres_qresult<QByteArray> tH = d->store.versionControlManager()->tipHash();
    if(updateLastError(tH.discard_value()))
    {
      return tH.get_value();
    }
    else
    {
      return QByteArray();
    }
  }
  else
  {
    return QByteArray();
  }
}

void TripleStore::updateStore()
{
  if(d->connection and d->connection->nativeConnection().isValid())
  {
    if(d->graphName.isEmpty())
    {
      cres_qresult<kDB::Repository::TripleStore> store
        = d->connection->nativeConnection().defaultTripleStore();
      if(updateLastError(store.discard_value()))
      {
        d->store = store.get_value();
      }
    }
    else
    {
      cres_qresult<kDB::Repository::TripleStore> rv
        = d->connection->nativeConnection().graphsManager()->getTripleStore(d->graphName);
      if(updateLastError(rv.discard_value()))
      {
        d->store = rv.get_value();
      }
    }
  }
  else
  {
    d->store = kDB::Repository::TripleStore();
  }
  emit(validChanged());
  emit(versionControlChanged());
  emit(tipRevisionChanged());
}

#include "moc_TripleStore.cpp"
