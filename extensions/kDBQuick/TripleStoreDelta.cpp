#include "TripleStoreDelta.h"

#include <knowCore/Timestamp.h>

#include <kDB/Repository/VersionControl/Delta.h>
#include <kDB/Repository/VersionControl/Signature.h>

using namespace kDBQuick;

struct TripleStoreDelta::Private
{
  const TripleStoreRevision* revision;
  kDB::Repository::VersionControl::Delta delta;
};

TripleStoreDelta::TripleStoreDelta(const TripleStoreRevision* _revision,
                                   const kDB::Repository::VersionControl::Delta& _delta,
                                   QObject* _parent)
    : QObject(_parent), d(new Private)
{
  d->revision = _revision;
  d->delta = _delta;
}

TripleStoreDelta::~TripleStoreDelta() { delete d; }

QByteArray TripleStoreDelta::hash() const { return d->delta.hash(); }

QByteArray TripleStoreDelta::delta() const { return d->delta.delta(); }

QByteArray TripleStoreDelta::parent() const { return d->delta.parent(); }

QVariantList TripleStoreDelta::signatures() const
{
  QVariantList signs;
  for(const kDB::Repository::VersionControl::Signature& sign : d->delta.signatures())
  {
    QVariantMap m;
    m["author"] = sign.author().toString();
    m["timestamp"] = sign.timestamp().toQDateTime().expect_success();
    signs.append(m);
  }
  return signs;
}

#include "moc_TripleStoreDelta.cpp"
