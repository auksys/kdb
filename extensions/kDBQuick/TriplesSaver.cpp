#include "TriplesSaver.h"

#include <QFile>
#include <QUrl>

#include <QtConcurrent/QtConcurrent>

#include <knowCore/Messages.h>
#include <knowCore/Uri.h>

#include <knowRDF/Triple.h>
#include <knowRDF/TriplesSerialiser.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TripleStore.h>

#include "Connection.h"

using namespace kDBQuick;

struct TriplesSaver::Private
{
  Connection* connection = nullptr;
  QString graphName;
};

TriplesSaver::TriplesSaver(QObject* _parent) : knowCore::Quick::Object(_parent), d(new Private) {}

TriplesSaver::~TriplesSaver() { delete d; }

Connection* TriplesSaver::connection() const { return d->connection; }

void TriplesSaver::setConnection(Connection* _connection)
{
  if(d->connection != _connection)
  {
    d->connection = _connection;
    emit(connectionChanged());
  }
}

QString TriplesSaver::graphName() { return d->graphName; }

void TriplesSaver::setGraphName(const QString& _graphName)
{
  if(d->graphName != _graphName)
  {
    d->graphName = _graphName;
    emit(graphNameChanged());
  }
}

void TriplesSaver::save(const QUrl& _filename)
{
  if(d->connection)
  {
    (void)QtConcurrent::run(
      [_filename, this]()
      {
        setLastError(QString());
        kDB::Repository::TripleStore store;
        if(d->graphName.isEmpty())
        {
          cres_qresult<kDB::Repository::TripleStore> store_rv
            = d->connection->nativeConnection().defaultTripleStore();
          if(updateLastError(store_rv.discard_value()))
          {
            store = store_rv.get_value();
          }
        }
        else
        {
          cres_qresult<kDB::Repository::TripleStore> ts_rv
            = d->connection->nativeConnection().graphsManager()->getTripleStore(d->graphName);
          if(updateLastError(ts_rv.discard_value()))
          {
            store = ts_rv.get_value();
          }
          else
          {
            return;
          }
        }
        QFile file(_filename.toLocalFile());
        if(file.open(QIODevice::WriteOnly))
        {
          knowRDF::TriplesSerialiser serializer;
          serializer.start(&file);
          cres_qresult<QList<knowRDF::Triple>> triples = store.triples();
          updateLastError(triples.discard_value());
          for(const knowRDF::Triple& triple : triples.get_value())
          {
            serializer.triple(triple);
          }
          serializer.finish();

          emit(savingFinished(_filename));
        }
        else
        {
          setLastError(
            clog_qt::qformat("Failed to open {} for writting.", _filename.toLocalFile()));
          emit(lastErrorChanged());
        }
      });
  }
  else
  {
    setLastError("No connection");
  }
}

#include "moc_TriplesSaver.cpp"
