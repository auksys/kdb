#pragma once

#include <knowCore/Forward.h>
#include <knowCore/Quick/Object.h>

namespace kDBQuick
{
  class Connection;
  class TriplesSaver : public knowCore::Quick::Object
  {
    Q_OBJECT
    Q_PROPERTY(kDBQuick::Connection* connection READ connection WRITE setConnection NOTIFY
                 connectionChanged);
    Q_PROPERTY(QString graphName READ graphName WRITE setGraphName NOTIFY graphNameChanged);
  public:
    TriplesSaver(QObject* _parent = nullptr);
    ~TriplesSaver();
  public:
    Connection* connection() const;
    void setConnection(Connection* _connection);
    QString graphName();
    void setGraphName(const QString& _graph);
  public:
    Q_INVOKABLE void save(const QUrl& _filename);
  signals:
    void connectionChanged();
    void graphNameChanged();
    void savingFinished(const QUrl& _filename);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBQuick
