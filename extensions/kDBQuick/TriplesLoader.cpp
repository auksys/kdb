#include "TriplesLoader.h"

#include <QFile>
#include <QUrl>

#include <QtConcurrent/QtConcurrent>

#include <knowCore/Messages.h>
#include <knowCore/Uri.h>

// #include <knowRDF/Literal.h>
// #include <knowRDF/Triple.h>
#include <knowRDF/TripleStream.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/ThreadedTripleStreamInserter.h>
#include <kDB/Repository/TripleStore.h>

#include "Connection.h"

using namespace kDBQuick;

struct TriplesLoader::Private
{
  Connection* connection = nullptr;
  QString graphName, lastError;
};

TriplesLoader::TriplesLoader(QObject* _parent) : knowCore::Quick::Object(_parent), d(new Private) {}

TriplesLoader::~TriplesLoader() { delete d; }

Connection* TriplesLoader::connection() const { return d->connection; }

void TriplesLoader::setConnection(Connection* _connection)
{
  if(d->connection != _connection)
  {
    d->connection = _connection;
    emit(connectionChanged());
  }
}

QString TriplesLoader::graphName() { return d->graphName; }

void TriplesLoader::setGraphName(const QString& _graphName)
{
  if(d->graphName != _graphName)
  {
    d->graphName = _graphName;
    emit(graphNameChanged());
  }
}

void TriplesLoader::load(const QUrl& _filename)
{
  (void)QtConcurrent::run(
    [_filename, this]()
    {
      kDB::Repository::TripleStore store;
      if(d->graphName.isEmpty())
      {
        cres_qresult<kDB::Repository::TripleStore> dts
          = d->connection->nativeConnection().defaultTripleStore();
        if(updateLastError(dts.discard_value()))
        {
          store = dts.get_value();
        }
      }
      else
      {
        cres_qresult<kDB::Repository::TripleStore> store_rv
          = d->connection->nativeConnection().graphsManager()->getTripleStore(d->graphName);
        if(updateLastError(store_rv.discard_value()))
        {
          store = store_rv.get_value();
        }
        else
        {
          return;
        }
      }

      kDB::Repository::ThreadedTripleStreamInserter* tsi
        = new kDB::Repository::ThreadedTripleStreamInserter(store, 1000000);
      QFile file(_filename.toLocalFile());
      file.open(QIODevice::ReadOnly);
      knowRDF::TripleStream stream;
      stream.addListener(tsi);
      updateLastError(stream.start(&file, nullptr));
      tsi->waitForFinished();
      delete tsi;
      emit(loadingFinished(_filename));
    });
}

#include "moc_TriplesLoader.cpp"
