#ifndef _KDBQUICK_CONNECTION_H_
#define _KDBQUICK_CONNECTION_H_

#include <QObject>
#include <QSharedPointer>

#include <kDB/Forward.h>

#include <knowDBC/Quick/Connection.h>

namespace kDBQuick
{
  class Connection : public knowDBC::Quick::Connection
  {
    Q_OBJECT
    Q_PROPERTY(QString hostname READ hostname WRITE setHostname NOTIFY hostnameChanged)
    Q_PROPERTY(QString storeName READ storeName WRITE setStoreName NOTIFY storeNameChanged)
    Q_PROPERTY(QString sectionName READ sectionName WRITE setSectionName NOTIFY hostnameChanged)
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)
    Q_PROPERTY(QStringList sqlTables READ sqlTables NOTIFY connectionStatusChanged)
    Q_PROPERTY(QStringList rdfGraphs READ rdfGraphs NOTIFY connectionStatusChanged)
  public:
    Connection(QObject* _parent = nullptr);
    ~Connection();
  public:
    QString hostname() const;
    void setHostname(const QString& _value);
    QString storeName() const;
    void setStoreName(const QString& _value);
    QString sectionName() const;
    void setSectionName(const QString& _value);
    int port() const;
    void setPort(int _port);
    bool isConnected() const override;
    QStringList rdfGraphs() const;
    QStringList sqlTables() const;
    knowDBC::Connection connection() const override;
  public:
    kDB::Repository::Connection nativeConnection() const;
  public:
    bool connect() override;
    bool disconnect() override;
    Q_INVOKABLE bool insertView(const QUrl& _filename);
    Q_INVOKABLE bool loadExtension(const QString& _name);
    Q_INVOKABLE bool extensionEnabled(const QString& _name);
  private:
    void connectIfNeeded();
  signals:
    void hostnameChanged();
    void portChanged();
    void storeNameChanged();
    void sectionNameChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBQuick

#endif
