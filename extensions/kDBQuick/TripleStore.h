#pragma once

#include <knowCore/Quick/Object.h>

#include <cres_qt>

namespace kDBQuick
{
  class Connection;
  class TripleStore : public knowCore::Quick::Object
  {
    Q_OBJECT
    Q_PROPERTY(
      kDBQuick::Connection* connection READ connection WRITE setConnection NOTIFY connectionChanged)
    Q_PROPERTY(QString graphName READ graphName WRITE setGraphName NOTIFY graphNameChanged)
    Q_PROPERTY(bool isValid READ isValid NOTIFY validChanged)
    Q_PROPERTY(bool hasVersionControl READ hasVersionControl NOTIFY versionControlChanged)
    Q_PROPERTY(QByteArray tipHash READ tipHash NOTIFY tipRevisionChanged)
  public:
    TripleStore(QObject* _object = nullptr);
    ~TripleStore();
    /**
     * Build a graph of revisions (required the Cyqlops.Graphviz library, it will return a null
     * graph if not available).
     */
    Q_INVOKABLE QObject* revisionsGraph();
  public:
    Connection* connection() const;
    void setConnection(Connection* _connection);
    QString graphName();
    void setGraphName(const QString& _graph);
    bool isValid() const;
    bool hasVersionControl() const;
    QByteArray tipHash();
  signals:
    void connectionChanged();
    void graphNameChanged();
    void validChanged();
    void versionControlChanged();
    void tipRevisionChanged();
  private:
    void updateStore();
    struct Private;
    Private* const d;
  };
} // namespace kDBQuick
