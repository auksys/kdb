#ifndef _KDBQUICK_TRIPLESLOADER_H_
#define _KDBQUICK_TRIPLESLOADER_H_

#include <knowCore/Quick/Object.h>

#include <knowCore/Forward.h>

namespace kDBQuick
{
  class Connection;
  class TriplesLoader : public knowCore::Quick::Object
  {
    Q_OBJECT
    Q_PROPERTY(kDBQuick::Connection* connection READ connection WRITE setConnection NOTIFY
                 connectionChanged);
    Q_PROPERTY(QString graphName READ graphName WRITE setGraphName NOTIFY graphNameChanged);
    Q_PROPERTY(QString lastError READ lastError NOTIFY lastErrorChanged);
  public:
    TriplesLoader(QObject* _parent = nullptr);
    ~TriplesLoader();
  public:
    Connection* connection() const;
    void setConnection(Connection* _connection);
    QString graphName();
    void setGraphName(const QString& _graph);
  public:
    Q_INVOKABLE void load(const QUrl& _filename);
  signals:
    void connectionChanged();
    void graphNameChanged();
    void loadingFinished(const QUrl& _filename);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBQuick

#endif
