#include <kDB/Forward.h>

namespace kDBMapping
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection);
}
