#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

#include <octomap/ColorOcTree.h>
#include <octomap/octomap.h>

#pragma GCC diagnostic pop

#include <QVariant>
#include <knowCore/SharedRef.h>
#include <knowCore/TypeDefinitions.h>

#include <knowGIS/GeometryObject.h>

namespace kDBMapping
{
  typedef knowCore::SharedRef<octomap::OcTree> OcTree;
  typedef knowCore::SharedRef<octomap::ColorOcTree> ColorOcTree;
} // namespace kDBMapping

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(kDBMapping, OcTree);
KNOWCORE_DECLARE_FULL_METATYPE(kDBMapping, ColorOcTree);
