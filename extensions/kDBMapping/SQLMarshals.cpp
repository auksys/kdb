#include <sstream>

#include <QString>
#include <QVariant>

#include <kDB/Repository/AbstractBinaryMarshal.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/BinaryMarshalsRegistry.h>

#include "TypesDefinitions.h"

namespace kDBMapping
{
  namespace details
  {
    struct OcTreeTrait
    {
      static constexpr const char* oid = "kdbmapping_octree";
      static knowCore::Uri uri() { return kDBMapping::OcTreeMetaTypeInformation::uri(); }
      typedef kDBMapping::OcTree Type;
      typedef octomap::OcTree OMType;
    };
    struct ColorOcTreeTrait
    {
      static constexpr const char* oid = "kdbmapping_coloroctree";
      static knowCore::Uri uri() { return kDBMapping::ColorOcTreeMetaTypeInformation::uri(); }
      typedef kDBMapping::ColorOcTree Type;
      typedef octomap::ColorOcTree OMType;
    };
  } // namespace details
  template<typename _T_>
  class OcTreeMarshalImpl : public kDB::Repository::AbstractBinaryMarshal
  {
    typedef typename _T_::Type Type;
    typedef typename _T_::OMType OMType;
  public:
    OcTreeMarshalImpl()
        : AbstractBinaryMarshal(_T_::oid, _T_::uri(), Mode::ToVariant | Mode::ToByteArray)
    {
    }
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);
      _oidName = _T_::oid;

      Type ot = _source.value<Type>().expect_success();
      if(ot)
      {
        std::ostringstream oss;
        bool r = ot->writeBinary(oss);
        return cres_cond(r, QByteArray(oss.str().c_str(), oss.str().length()),
                         "Failed to write OcTree to binary representation.");
      }
      else
      {
        return cres_failure("Invalid octomap.");
      }
    }
    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override
    {
      Q_UNUSED(_connection);
      Type ot(new OMType(0.1), true);

      std::string str(_source.data(), _source.length());

      std::istringstream iss(str);
      bool r = ot->readBinary(iss);

      return cres_cond(r, knowCore::Value::fromValue(ot),
                       "Failed to parse OcTree from binary representation");
    }
  };

  typedef OcTreeMarshalImpl<details::OcTreeTrait> OcTreeMarshal;
  typedef OcTreeMarshalImpl<details::ColorOcTreeTrait> ColorOcTreeMarshal;

  KDB_REGISTER_SQL_MARSHAL(OcTreeMarshal)
  KDB_REGISTER_SQL_MARSHAL(ColorOcTreeMarshal)

} // namespace kDBMapping
