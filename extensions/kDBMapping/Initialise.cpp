#include "Initialise.h"

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/ActiveRecord/Version.h>
#include <kDB/Repository/Connection.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/QueryConnectionInfo.h>

#include <kDBGIS/Initialise.h>

#include "ColorOctomap.h"
#include "Octomap.h"

namespace kDBMapping
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection)
  {
    cres_try(cres_ignore, kDB::Repository::Connection(_connection).enableExtension("kDBGIS"));
    knowDBC::Query q = _connection.createSQLQuery();

    cres_try(kDB::Repository::ActiveRecord::VersionRecord vr,
             kDB::Repository::ActiveRecord::VersionRecord::get(_connection, "kDBMapping"));

    QStringList types;
    types << "kdbmapping_octree" << "kdbmapping_coloroctree";

    for(const QString& type : types)
    {
      q.setQuery("SELECT COUNT(*) FROM pg_type WHERE typname=?typname");
      q.bindValue("?typname", type);
      knowDBC::Result sr = q.execute();
      if(not sr)
      {
        KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to check for octree type", sr);
      }
      if(sr.value<int>(0, 0).expect_success() < 1)
      {
        q.setQuery("CREATE DOMAIN " + type + " AS bytea;");
        KDB_REPOSITORY_EXECUTE_QUERY(clog_qt::qformat("failed to create {} domain", type), q,
                                     false);
      }
    }

    OctomapRecord::createTable(_connection);
    ColorOctomapRecord::createTable(_connection);

    return vr.updateToCurrent();
  }

} // namespace kDBMapping

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBMapping)
