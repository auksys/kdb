#include "TestOctomap.h"

#include <knowCore/Test.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryStore.h>

#include <kDBMapping/Initialise.h>
#include <kDBMapping/Octomap.h>

void TestOctomap::testCreationAndRetrieve()
{
  kDB::Repository::TemporaryStore ts;
  ts.start();
  kDB::Repository::Connection c = ts.createConnection();
  c.connect();

  CRES_QVERIFY(c.enableExtension("kDBMapping"));

  kDBMapping::OcTree oct(new octomap::OcTree(1.0), true);

  kDBMapping::OctomapRecord::create(c, oct, knowGIS::GeometryObject());

  kDBMapping::OctomapRecord octomap = kDBMapping::OctomapRecord::all(c).first();

  QVERIFY(octomap.data());
}

QTEST_MAIN(TestOctomap)
