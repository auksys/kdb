#include "TypesDefinitions.h"

#include <QByteArray>

#include <knowCore/MetaTypeImplementation.h>
#include <knowCore/Uris/askcore_datatype.h>

#include <sstream>

using namespace kDBMapping;

#define MAKE_CONVERT(_NAME_)                                                                       \
  KNOWCORE_DEFINE_CONVERT(QByteArray, _from, kDBMapping::_NAME_, _to, Safe)                        \
  {                                                                                                \
    if(not *_to)                                                                                   \
      *_to = kDBMapping::_NAME_(new octomap::_NAME_(0.1), true);                                   \
    std::string str(_from->data(), _from->length());                                               \
    std::istringstream iss(str);                                                                   \
    bool r = (*_to)->readBinary(iss);                                                              \
    if(r)                                                                                          \
    {                                                                                              \
      return cres_success();                                                                       \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      return cres_failure("fail to parse binary data into an octomap");                            \
    }                                                                                              \
  }                                                                                                \
  KNOWCORE_DEFINE_CONVERT(kDBMapping::_NAME_, _from, QByteArray, _to, Safe)                        \
  {                                                                                                \
    std::ostringstream oss;                                                                        \
    const_cast<kDBMapping::_NAME_::ElementType*>(_from->data())->writeBinary(oss);                 \
    _to->setRawData(oss.str().c_str(), oss.str().length());                                        \
    return cres_success();                                                                         \
  }

MAKE_CONVERT(OcTree)
MAKE_CONVERT(ColorOcTree)

template<typename _T_>
  requires(std::is_same_v<_T_, OcTree> or std::is_same_v<_T_, ColorOcTree>)
KNOWCORE_DEFINE_METATYPE_START_IMPLEMENTATION_TEMPLATE(_T_)
QByteArray toByteArray(const _T_& _value) const
{
  QByteArray array;
  knowCore::convert<_T_, QByteArray>(&_value, &array).expect_success();
  return array;
}
cres_qresult<_T_> fromByteArray(const QByteArray& _value) const
{
  _T_ to;
  cres_try(cres_ignore, knowCore::convert(&_value, &to));
  return cres_success(to);
}
QString toString(const _T_& _value) const
{
  return QString::fromLatin1(toByteArray(_value).toBase64());
}
cres_qresult<_T_> fromString(const QString& _value) const
{
  cres_try(_T_ value, fromByteArray(QByteArray::fromBase64(_value.toLatin1())));
  return cres_success(value);
}
cres_qresult<QByteArray> md5(const _T_& _value) const override
{
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(toByteArray(_value));
  return cres_success(hash.result());
}
cres_qresult<QJsonValue> toJsonValue(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(toString(_value));
}
cres_qresult<void> fromJsonValue(_T_* _value, const QJsonValue& _json_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_json_value.isString())
  {
    cres_try(_T_ value, fromString(_json_value.toString()));
    *_value = value;
    return cres_success();
  }
  else
  {
    return expectedError("string", _json_value);
  }
}
cres_qresult<QCborValue> toCborValue(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(toByteArray(_value));
}
cres_qresult<void> fromCborValue(_T_* _value, const QCborValue& _cbor_value,
                                 const knowCore::DeserialisationContexts&) const override
{
  if(_cbor_value.isByteArray())
  {
    cres_try(*_value, fromByteArray(_cbor_value.toByteArray()));
    return cres_success();
  }
  else
  {
    return expectedError("bytearray", _cbor_value);
  }
}
cres_qresult<QString> printable(const _T_& /*_value*/) const override
{
  return cres_success(QString("octree"));
}
cres_qresult<QString> toRdfLiteral(const _T_& _value, const SerialisationContexts&) const override
{
  return cres_success(toString(_value));
}
cres_qresult<void> fromRdfLiteral(_T_* _value, const QString& _serialised,
                                  const knowCore::DeserialisationContexts&) const override
{
  cres_try(*_value, fromString(_serialised));
  return cres_success();
}
KNOWCORE_DEFINE_METATYPE_FINISH_IMPLEMENTATION(_T_)

KNOWCORE_DEFINE_METATYPE(kDBMapping::OcTree, knowCore::Uris::askcore_datatype::octree,
                         knowCore::MetaTypeTraits::FromByteArray
                           | knowCore::MetaTypeTraits::ToByteArray)
KNOWCORE_DEFINE_METATYPE(kDBMapping::ColorOcTree, knowCore::Uris::askcore_datatype::colored_octree,
                         knowCore::MetaTypeTraits::FromByteArray
                           | knowCore::MetaTypeTraits::ToByteArray)
