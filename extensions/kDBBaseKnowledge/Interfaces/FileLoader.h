#include <kDB/Forward.h>

namespace kDBBaseKnowledge::Interfaces
{
  /**
   * @ingroup kDBBaseKnowledge
   *
   * Interface for implementing a FileLoader that will insert base knowledge in a database.
   */
  class FileLoader
  {
  public:
    virtual ~FileLoader();
    /**
     * Check if this loader can be used on a specific file.
     * @p _options contains an options file that is store alongside a base knowledge YAML file.
     */
    virtual bool canLoad(const QString& _path, const QString& _mime_type,
                         const knowCore::ValueHash& _options)
      = 0;
    /**
     * Load the file from \ref _path into the database for the given connection \ref _connection.
     *
     * @return a list of files on which this file depend.
     */
    virtual cres_qresult<QStringList> loadFile(const QString& _path,
                                               const kDB::Repository::Connection& _connection,
                                               const knowCore::ValueHash& _options)
      = 0;
  };
} // namespace kDBBaseKnowledge::Interfaces
