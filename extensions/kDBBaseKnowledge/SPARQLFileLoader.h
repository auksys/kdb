#include "Interfaces/FileLoader.h"

namespace kDBBaseKnowledge
{
  class SPARQLFileLoader : public Interfaces::FileLoader
  {
  public:
    SPARQLFileLoader();
    virtual ~SPARQLFileLoader();
    bool canLoad(const QString& _path, const QString& _mime_type,
                 const knowCore::ValueHash& _options) override;
    cres_qresult<QStringList> loadFile(const QString& _path,
                                       const kDB::Repository::Connection& _connection,
                                       const knowCore::ValueHash& _options) override;
  };
} // namespace kDBBaseKnowledge
