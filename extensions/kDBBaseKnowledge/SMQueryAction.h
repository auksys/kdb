#include <QtGlobal>

#include <kDB/SMQuery/Interfaces/Action.h>

#include <kDB/Forward.h>

namespace kDBBaseKnowledge
{
  /**
   * Define a SMQuery that reacts to "register base knowledge" to allow adding source of base
   * knowledge.
   *
   * For instance: "register base knowledge sour"
   */
  class Q_DECL_DEPRECATED_X("SMQuery is deprecated in favor of krQL.") SMQueryAction
      : public kDB::SMQuery::Interfaces::Action
  {
  public:
    SMQueryAction();
    SMQueryAction(const kDB::Repository::Connection& _connection);
    virtual ~SMQueryAction();
    cres_qresult<void> execute(const QString& _key,
                               const knowCore::ValueHash& _parameters) override;
    void setConnection(const kDB::Repository::Connection& _connection);
    void addToEngine(kDB::SMQuery::Engine* _engine);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBBaseKnowledge
