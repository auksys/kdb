#include "SMQueryAction.h"

#include <kDB/Repository/Connection.h>
#include <kDB/SMQuery/Engine.h>

#include "Manager.h"

using namespace kDBBaseKnowledge;

struct SMQueryAction::Private
{
  kDB::Repository::Connection connection;
};

SMQueryAction::SMQueryAction() : d(new Private) {}

SMQueryAction::SMQueryAction(const kDB::Repository::Connection& _connection)
    : d(new Private{_connection})
{
}

SMQueryAction::~SMQueryAction() {}

cres_qresult<void> SMQueryAction::execute(const QString& _key,
                                          const knowCore::ValueHash& _parameters)
{
  if(d->connection.isConnected())
  {
    if(_key == "REGISTER BASE KNOWLEDGE")
    {
      for(const QString& key : _parameters.keys())
      {
        if(key != "directory" and key != "file" and key != "directories" and key != "files")
        {
          return cres_failure("Unknown key '{}', only 'directory' or 'file' are supported", key);
        }
      }
      if(_parameters.contains("directory"))
      {
        cres_try(directory, _parameters.value<QString>("directory"));
        cres_try(cres_ignore, d->connection.extensionObject<Manager>()->addDirectory(directory));
      }
      if(_parameters.contains("directories"))
      {
        cres_try(directories, _parameters.value<QString>("directories"));
        for(const QString& directory : directories.split(":"))
        {
          cres_try(cres_ignore, d->connection.extensionObject<Manager>()->addDirectory(directory));
        }
      }
      if(_parameters.contains("file"))
      {
        cres_try(file, _parameters.value<QString>("file"));
        cres_try(cres_ignore, d->connection.extensionObject<Manager>()->addFile(file));
      }
      if(_parameters.contains("files"))
      {
        cres_try(files, _parameters.value<QString>("files"));
        for(const QString& file : files.split(":"))
        {
          cres_try(cres_ignore, d->connection.extensionObject<Manager>()->addFile(file));
        }
      }
      return cres_success();
    }
    return cres_failure("Unhandled key {}", _key);
  }
  else
  {
    return cres_failure("Not connected.");
  }
}

void SMQueryAction::setConnection(const kDB::Repository::Connection& _connection)
{
  d->connection = _connection;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
void SMQueryAction::addToEngine(kDB::SMQuery::Engine* _engine)
{
  _engine->add({"register base knowledge"}, this, false);
}
#pragma GCC diagnostic pop
