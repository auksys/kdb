#include "krQueryAction.h"

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/krQuery/Context.h>

#include "Manager.h"

using namespace kDBBaseKnowledge;

struct krQueryAction::Private
{
};

krQueryAction::krQueryAction() : d(new Private) {}

krQueryAction::~krQueryAction() { delete d; }

cres_qresult<knowCore::Value>
  krQueryAction::execute(const kDB::Repository::krQuery::Context& _context, const QString& _key,
                         const knowCore::ValueHash& _parameters)
{
  kDB::Repository::Connection connection = _context.queryConnectionInfo().connection();
  if(connection.isConnected())
  {
    if(_key == "register base knowledge")
    {
      for(const QString& key : _parameters.keys())
      {
        if(key != "directory" and key != "file" and key != "directories" and key != "files")
        {
          return cres_failure("Unknown key '{}', only 'directory' or 'file' are supported", key);
        }
      }
      if(_parameters.contains("directory"))
      {
        cres_try(QString directory, _parameters.value<QString>("directory"));
        cres_try(cres_ignore, connection.extensionObject<Manager>()->addDirectory(directory));
      }
      if(_parameters.contains("directories"))
      {
        cres_try(QString directories, _parameters.value<QString>("directories"));
        for(const QString& directory : directories.split(":"))
        {
          cres_try(cres_ignore, connection.extensionObject<Manager>()->addDirectory(directory));
        }
      }
      if(_parameters.contains("file"))
      {
        cres_try(QString file, _parameters.value<QString>("file"));
        cres_try(cres_ignore, connection.extensionObject<Manager>()->addFile(file));
      }
      if(_parameters.contains("files"))
      {
        cres_try(QString files, _parameters.value<QString>("files"));
        for(const QString& file : files.split(":"))
        {
          cres_try(cres_ignore, connection.extensionObject<Manager>()->addFile(file));
        }
      }
      return cres_success(knowCore::Value());
    }
    return cres_failure("Unhandled key {}", _key);
  }
  else
  {
    return cres_failure("Not connected.");
  }
}
