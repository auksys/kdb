#include <kDB/Repository/krQuery/Interfaces/Action.h>

#include <kDB/Forward.h>

namespace kDBBaseKnowledge
{
  /**
   * Define a krQL Query that reacts to "register base knowledge" to allow adding source of base
   * knowledge.
   *
   * For instance: "register base knowledge sour"
   */
  class krQueryAction : public kDB::Repository::krQuery::Interfaces::Action
  {
  public:
    krQueryAction();
    virtual ~krQueryAction();
    cres_qresult<knowCore::Value> execute(const kDB::Repository::krQuery::Context& _context,
                                          const QString& _key,
                                          const knowCore::ValueHash& _parameters) override;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBBaseKnowledge
