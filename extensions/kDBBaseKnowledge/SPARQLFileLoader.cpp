#include "SPARQLFileLoader.h"

#include <QFile>
#include <QStringList>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/RDFEnvironment.h>

#include <kDB/SPARQL/Algebra/NodeVisitor.h>
#include <kDB/SPARQL/Lexer_p.h>
#include <kDB/SPARQL/Parser_p.h>

using namespace kDBBaseKnowledge;

SPARQLFileLoader::SPARQLFileLoader() {}

SPARQLFileLoader::~SPARQLFileLoader() {}

bool SPARQLFileLoader::canLoad(const QString& _path, const QString& _mime,
                               const knowCore::ValueHash&)
{
  return _mime == "application/sparql-query" or _path.endsWith(".rq") or _path.endsWith(".sparql");
}

namespace
{
  struct DependsFinder : public kDB::SPARQL::Algebra::NodeVisitor<void>
  {
    QStringList depends;
    void visit(kDB::SPARQL::Algebra::LoadCSP _node) override
    {
      depends.append(QUrl(_node->uri()).toLocalFile());
    }
  };
} // namespace

cres_qresult<QStringList> SPARQLFileLoader::loadFile(const QString& _path,
                                                     const kDB::Repository::Connection& _connection,
                                                     const knowCore::ValueHash&)
{
  QFile file(_path);
  if(not file.open(QIODevice::ReadOnly))
  {
    return cres_failure("Failed to open {}", _path);
  }
  QByteArray query_text = file.readAll();
  knowDBC::Query q = _connection.createSPARQLQuery(
    kDB::Repository::RDFEnvironment().setBase("file:///" + _path), query_text);
  knowDBC::Result r = q.execute();
  if(r)
  {
    // Find all the depends
    DependsFinder depends;
    for(kDB::SPARQL::Algebra::NodeCSP node :
        kDB::SPARQL::Parser(new kDB::SPARQL::Lexer(query_text, knowCore::ValueHash()),
                            knowCore::ValueHash(), "file:///" + _path)
          .parse())
    {
      depends.start(node);
    }
    return cres_success(depends.depends);
  }
  else
  {
    return cres_failure("Failed to execute query from {} with error {}.", _path, r.error());
  }
}

#include "Manager.h"

KDB_REGISTER_BASE_KNOWLEDGE_FILE_LOADER(SPARQLFileLoader)
