#include <QObject>

#include "Forward.h"
#include <knowCore/Global.h>

namespace kDBBaseKnowledge
{
  class Manager : public QObject
  {
  public:
    Manager(const knowCore::WeakReference<kDB::Repository::Connection>& _connection);
    ~Manager();
    /**
     * Add a directory to load base knowledge from.
     */
    cres_qresult<void> addDirectory(const QString& _directory);
    /**
     * Add a file to load.
     */
    cres_qresult<void> addFile(const QString& _file);
    /**
     * Add a directory to load base knowledge from, for all connections. (only apply to connection
     * created after call to this function)
     */
    static void addDirectoryToAll(const QString& _directory);
    /**
     * Add a file to load, for all connections. (only apply to connection created after call to this
     * function)
     */
    static void addFileToAll(const QString& _file);
    static void registerFileLoader(Interfaces::FileLoader* _fileLoader);
    static void ignoreMimeType(const QString& _mime);
    template<typename... _TArgs_>
    static void ignoreMimeTypes(const QString& _mime, const _TArgs_&... _args);
  private:
    static void ignoreMimeTypes();
    struct Private;
    Private* const d;
  };

  template<typename... _TArgs_>
  inline void Manager::ignoreMimeTypes(const QString& _mime, const _TArgs_&... _args)
  {
    ignoreMimeType(_mime);
    ignoreMimeTypes(_args...);
  }
  inline void Manager::ignoreMimeTypes() {}

} // namespace kDBBaseKnowledge

#define __KDB_REGISTER_BASE_KNOWLEDGE_FILE_LOADER(_NAME_, _KLASS_)                                 \
  namespace                                                                                        \
  {                                                                                                \
    class _NAME_                                                                                   \
    {                                                                                              \
      _NAME_() { kDBBaseKnowledge::Manager::registerFileLoader(new _KLASS_); }                     \
      static _NAME_ instance;                                                                      \
    };                                                                                             \
    _NAME_ _NAME_::instance;                                                                       \
  }

#define KDB_REGISTER_BASE_KNOWLEDGE_FILE_LOADER(_KLASS_)                                           \
  __KDB_REGISTER_BASE_KNOWLEDGE_FILE_LOADER(__KNOWCORE_UNIQUE_STATIC_NAME(FileLoaderRegister),     \
                                            _KLASS_)

#define __KDB_REGISTER_BASE_KNOWLEDGE_IGNORE_MIME_TYPE(_NAME_, ...)                                \
  namespace                                                                                        \
  {                                                                                                \
    class _NAME_                                                                                   \
    {                                                                                              \
      _NAME_() { kDBBaseKnowledge::Manager::ignoreMimeTypes(__VA_ARGS__); }                        \
      static _NAME_ instance;                                                                      \
    };                                                                                             \
    _NAME_ _NAME_::instance;                                                                       \
  }

#define KDB_REGISTER_BASE_KNOWLEDGE_IGNORE_MIME_TYPE(...)                                          \
  __KDB_REGISTER_BASE_KNOWLEDGE_IGNORE_MIME_TYPE(                                                  \
    __KNOWCORE_UNIQUE_STATIC_NAME(IgnoreMimetypeRegster), __VA_ARGS__)
