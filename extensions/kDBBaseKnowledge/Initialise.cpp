#include "Manager.h"

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/ActiveRecord/Version.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/krQuery/Engine.h>

#include "Directory.h"
#include "File.h"
#include "krQueryAction.h"

namespace kDBBaseKnowledge
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection)
  {
    cres_try(cres_ignore, FileRecord::createTable(_connection));
    cres_try(cres_ignore, DirectoryRecord::createTable(_connection));
    kDB::Repository::Connection(_connection).createExtensionObject<Manager>();
    cres_try(kDB::Repository::ActiveRecord::VersionRecord vr,
             kDB::Repository::ActiveRecord::VersionRecord::get(_connection, "kDBBaseKnowledge"));
    if(vr.striclyLessThan(4, 2, 1))
    {
      knowDBC::Query q = _connection.createSQLQuery(
        "ALTER TABLE \"files\" ADD COLUMN IF NOT EXISTS \"ignore\" bool DEFAULT true");
      KDB_REPOSITORY_EXECUTE_QUERY("Failed to update files table", q, false);
    }
    _connection.krQueryEngine()->add({"register base knowledge"}, new krQueryAction(), true);
    return vr.updateToCurrent();
  }
} // namespace kDBBaseKnowledge

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBBaseKnowledge)
