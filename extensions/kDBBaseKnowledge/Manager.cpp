#include "Manager.h"

#include <QDir>
#include <QFileSystemWatcher>
#include <QMimeDatabase>
#include <QThread>

#include <Cyqlops/Crypto/Hash.h>
#include <Cyqlops/Yaml/Node.h>

#include <kDB/Repository/QueryConnectionInfo.h>

#include "Directory.h"
#include "File.h"
#include "Interfaces/FileLoader.h"

using namespace kDBBaseKnowledge;

struct Manager::Private
{
  ~Private()
  {
    thread.exit();
    thread.wait();
  }
  knowCore::WeakReference<kDB::Repository::Connection> connection;
  QStringList directories, files;
  QFileSystemWatcher watcher;
  QThread thread;
  cres_qresult<void> updateFile(FileRecord _fr, bool _update_depedents, bool _force_update);
  cres_qresult<void> updateDirectory(DirectoryRecord _dr);
  cres_qresult<void> addFile(const QString& _file);
  struct Static
  {
    QStringList directories_all, files_all, ignoredMimes;
    QList<Interfaces::FileLoader*> loaders;
    QMimeDatabase mime_database;
  };
  static Static s_static;
};

Manager::Private::Static Manager::Private::s_static;

cres_qresult<void> Manager::Private::updateDirectory(DirectoryRecord _dr)
{
  QDir directory(_dr.path());
  for(const QString& file : directory.entryList(QDir::Files))
  {
    cres_try(cres_ignore, addFile(directory.absoluteFilePath(file)));
  }
  return cres_success();
}

cres_qresult<void> Manager::Private::addFile(const QString& _file)
{
  if(files.contains(_file))
  {
    FileRecord fr = FileRecord::byPath(connection, _file).first();
    if(fr.isPersistent())
    {
      if(fr.ignore())
      {
        fr.setIgnore(false);
        cres_try(cres_ignore, fr.record());
      }
      return cres_success();
    }
    else
    {
      return cres_failure("{} is cached but absent from database.", _file);
    }
  }
  files.append(_file);
  watcher.addPath(_file);

  FileRecord fr = FileRecord::byPath(connection, _file).first();
  if(fr.isPersistent())
  {
    if(fr.ignore())
    {
      fr.setIgnore(false);
      cres_try(cres_ignore, fr.record());
    }
  }
  else
  {
    cres_try(fr, FileRecord::create(connection, _file, knowCore::Timestamp(), QByteArray(), false));
  }
  return updateFile(fr, false, false);
}

cres_qresult<void> Manager::Private::updateFile(FileRecord _fr, bool _update_depedents,
                                                bool _force_update)
{
  QFileInfo f(_fr.path());
  knowCore::Timestamp ts = knowCore::Timestamp::from(f.lastModified());
  if(ts > _fr.lastModified() or _force_update)
  {
    clog_info("Updating file: {} last modified {}", _fr.path(), _fr.lastModified());
    QFile ff(f.absoluteFilePath());
    if(ff.open(QIODevice::ReadOnly))
    {
      QByteArray md5 = Cyqlops::Crypto::Hash::md5(&ff);
      if(md5 != _fr.md5() or _force_update)
      {
        bool loaded = false;
        QString mtype = s_static.mime_database.mimeTypeForFile(_fr.path()).name();
        if(not _fr.ignore())
        {
          knowCore::ValueHash options;
          QFileInfo fi_options(_fr.path() + ".options");
          if(fi_options.exists())
          {
            QFile file_options(_fr.path() + ".options");
            if(not file_options.open(QIODevice::ReadOnly))
            {
              return cres_failure("Failed to open options file '{}' for reading.",
                                  _fr.path() + ".options");
            }
            Cyqlops::Yaml::YamlParseError parseError;
            Cyqlops::Yaml::Node node = Cyqlops::Yaml::Node::fromYaml(
              QString::fromUtf8(file_options.readAll()), &parseError);
            if(node.isUndefined())
            {
              return cres_failure("Failed to parse options file '{}' with error '{}'.",
                                  _fr.path() + ".options", parseError.errorMessage);
            }
            cres_try(options, knowCore::ValueHash::fromVariantMap(node.toVariant().toMap()));
          }
          for(Interfaces::FileLoader* fl : s_static.loaders)
          {
            if(fl->canLoad(_fr.path(), mtype, options))
            {
              cres_try(QStringList depends, fl->loadFile(_fr.path(), connection, options));
              _fr.setLastModified(ts);
              _fr.setMd5(md5);
              cres_try(cres_ignore, _fr.record());
              loaded = true;
              for(const QString& dep : depends)
              {
                FileRecord dep_fr = FileRecord::byPath(connection, dep).first();
                if(not dep_fr.isPersistent())
                {
                  cres_try(dep_fr, FileRecord::create(connection, dep, knowCore::Timestamp(),
                                                      QByteArray(), true));
                  watcher.addPath(dep);
                  cres_try(cres_ignore, updateFile(dep_fr, false, false));
                }
                QStringList dependent_fr = dep_fr.dependends();
                if(not dependent_fr.contains(_fr.path()))
                {
                  dependent_fr.append(_fr.path());
                  dep_fr.setDependends(dependent_fr);
                  cres_try(cres_ignore, dep_fr.record());
                }
              }
              break;
            }
          }
          if(not loaded)
          {
            if(not s_static.ignoredMimes.contains(mtype))
            {
              return cres_failure("No base knowledge loader for {}", _fr.path());
            }
          }
        }
        // Look at the dependents
        if(_update_depedents)
        {
          for(const QString& dep : _fr.dependends())
          {
            FileRecord dep_fr = FileRecord::byPath(connection, dep).first();
            if(dep_fr.isPersistent())
            {
              updateFile(dep_fr, true, true);
            }
            else
            {
              clog_warning("Unknown depedent {} for {}", dep, _fr.path());
            }
          }
        }
      }
    }
    return cres_success();
  }
  else
  {
    return cres_success();
  }
}

Manager::Manager(const knowCore::WeakReference<kDB::Repository::Connection>& _connection)
    : d(new Private)
{
  d->connection = _connection;

  // Load back the content of Directory/File once the connection is established and all extensions
  // are loaded
  kDB::Repository::Connection(d->connection)
    .executeConnection(
      [this](const kDB::Repository::Connection&)
      {
        clog_info("Loading/updating base knowledge...");
        // Load files
        for(const DirectoryRecord& dr : DirectoryRecord::all(d->connection).exec())
        {
          d->directories.append(dr.path());
          d->watcher.addPath(dr.path());
          cres_qresult<void> directoryUpdate = d->updateDirectory(dr);
          if(not directoryUpdate.is_successful())
          {
            clog_error("Could not update {} yet, error: {}", dr.path(),
                       directoryUpdate.get_error());
          }
        }
        for(const FileRecord& fr : FileRecord::all(d->connection).exec())
        {
          d->directories.append(fr.path());
          d->watcher.addPath(fr.path());
          cres_qresult<void> fileUpdate = d->updateFile(fr, true, false);
          if(not fileUpdate.is_successful())
          {
            clog_error("Could not update {} yet, error: {}", fr.path(), fileUpdate.get_error());
          }
        }
      });
  d->watcher.moveToThread(&d->thread);
  QObject::connect(&d->watcher, &QFileSystemWatcher::directoryChanged,
                   [this](const QString& _path)
                   {
                     DirectoryRecord dr = DirectoryRecord::byPath(d->connection, _path).first();
                     if(dr.isPersistent())
                     {
                       cres_qresult<void> r = d->updateDirectory(dr);
                       if(not r.is_successful())
                       {
                         clog_error("Failure to update {} with error {}.", _path, r.get_error());
                       }
                     }
                     else
                     {
                       clog_warning("{} was changed on disk, but is not listed in database.",
                                    _path);
                     }
                   });
  QObject::connect(&d->watcher, &QFileSystemWatcher::fileChanged,
                   [this](const QString& _path)
                   {
                     FileRecord f = FileRecord::byPath(d->connection, _path).first();
                     if(f.isPersistent())
                     {
                       cres_qresult<void> r = d->updateFile(f, true, false);
                       if(not r.is_successful())
                       {
                         clog_error("Failure to update {} with error {}.", _path, r.get_error());
                       }
                     }
                     else
                     {
                       clog_warning("{} was changed on disk, but is not listed in database.",
                                    _path);
                     }
                   });
  d->thread.start();
}

Manager::~Manager() { delete d; }

cres_qresult<void> Manager::addDirectory(const QString& _directory)
{
  if(d->directories.contains(_directory))
  {
    return cres_success();
  }
  d->directories.append(_directory);
  d->watcher.addPath(_directory);

  QDir directory(_directory);
  for(const QString& file : directory.entryList(QDir::Files))
  {
    cres_try(cres_ignore, addFile(directory.absoluteFilePath(file)));
  }
  return DirectoryRecord::create(d->connection, _directory).discard_value();
}

cres_qresult<void> Manager::addFile(const QString& _file) { return d->addFile(_file); }

void Manager::addDirectoryToAll(const QString& _directory)
{
  Private::s_static.directories_all.append(_directory);
}

void Manager::addFileToAll(const QString& _file) { Private::s_static.files_all.append(_file); }

void Manager::registerFileLoader(Interfaces::FileLoader* _fileLoader)
{
  Private::s_static.loaders.append(_fileLoader);
}

void Manager::ignoreMimeType(const QString& _mime) { Private::s_static.ignoredMimes.append(_mime); }

KDB_REGISTER_BASE_KNOWLEDGE_IGNORE_MIME_TYPE("text/turtle");
