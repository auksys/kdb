#include "TestBKManager.h"

#include <QTemporaryDir>

#include <knowCore/Test.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>

#include "../Manager.h"

namespace kDBBaseKnowledge
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection);
}

namespace
{
  void writeToFile(const QString& _fn, const QString& _data)
  {
    QFile f(_fn);
    QVERIFY(f.open(QIODevice::WriteOnly));
    f.write(_data.toUtf8());
  }
} // namespace

void TestBKManager::testManager()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c1 = store.createConnection();
  CRES_QVERIFY(c1.connect());

  CRES_QVERIFY(c1.enableExtension("kDBBaseKnowledge"));

  kDBBaseKnowledge::Manager* m = c1.extensionObject<kDBBaseKnowledge::Manager>();

  QTemporaryDir tmpDir;
  qDebug() << tmpDir.isValid() << tmpDir.path();

  writeToFile(tmpDir.filePath("td.rq"), R"V0G0N(
DROP GRAPH <p://test_graph>;
LOAD <td.ttl> INTO GRAPH <p://test_graph> 
)V0G0N");

  writeToFile(tmpDir.filePath("td.ttl"), R"V0G0N(
<p://a> <p://b> <p://c> .
)V0G0N");

  CRES_QVERIFY(m->addDirectory(tmpDir.path()));

  kDB::Repository::TripleStore interface = CRES_QVERIFY(
    c1.graphsManager()->getTripleStore("p://test_graph"_kCu));
  QList<knowRDF::Triple> triples = CRES_QVERIFY(interface.triples());
  QCOMPARE(triples.size(), 1);
  QCOMPARE(triples[0].subject(), "p://a"_kCu);
  QCOMPARE(triples[0].predicate(), "p://b"_kCu);
  QCOMPARE(triples[0].object(), "p://c"_kCu);

  writeToFile(tmpDir.filePath("td.ttl"), R"V0G0N(
<p://d> <p://e> <p://f> .
)V0G0N");

  KNOWCORE_TEST_WAIT_FOR_ACT(
    (
      [c1]()
      {
        cres_qresult<kDB::Repository::TripleStore> interface = c1.graphsManager()->getTripleStore(
          "p://test_graph"_kCu);
        if(interface.is_successful())
        {
          QList<knowRDF::Triple> triples = CRES_QVERIFY(interface.get_value().triples());
          return triples.size() == 1 and triples[0].subject().uri() == "p://d"_kCu;
        }
        else
        {
          return false;
        }
      })(),
    QCoreApplication::processEvents(););
  interface = CRES_QVERIFY(c1.graphsManager()->getTripleStore("p://test_graph"_kCu));
  triples = CRES_QVERIFY(interface.triples());
  QCOMPARE(triples.size(), 1);
  QCOMPARE(triples[0].subject(), "p://d"_kCu);
  QCOMPARE(triples[0].predicate(), "p://e"_kCu);
  QCOMPARE(triples[0].object(), "p://f"_kCu);
}

QTEST_MAIN(TestBKManager)
