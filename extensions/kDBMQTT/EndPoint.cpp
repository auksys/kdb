#include "EndPoint.h"

#include <QDir>

#include <Cyqlops/MQTT/Client.h>
#include <Cyqlops/MQTT/Message.h>
#include <Cyqlops/MQTT/ServiceServer.h>

#include <kDB/Repository/Connection.h>

using namespace kDBMQTT;

struct EndPoint::Private
{
  QString prefix;
  Cyqlops::MQTT::Client mqtt_client;
  kDB::Repository::Connection connection;
};

EndPoint::EndPoint(const kDB::Repository::Connection& _connection, const QString& _prefix,
                   const QString& _mqtt_address)
    : d(new Private())
{
  d->prefix = _prefix;
  d->mqtt_client.setServerAddress(_mqtt_address);
  d->connection = _connection;
}

EndPoint::~EndPoint() { delete d; }

cres_qresult<void> EndPoint::start()
{
  if(not d->connection.isConnected())
  {
    cres_try(cres_ignore, d->connection.connect());
  }
  if(not d->mqtt_client.start())
  {
    return cres_failure("Failed to connect to MQTT broker.");
  }
  return cres_success();
}

void EndPoint::stop() { d->mqtt_client.stop(); }
