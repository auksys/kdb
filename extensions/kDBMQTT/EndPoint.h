#include <clog_qt>
#include <cres_qt>

#include <kDB/Forward.h>

namespace kDBMQTT
{
  /**
   * This class allow to setup a MQTT Client to interract with a kDB Store.
   */
  class EndPoint
  {
  public:
    EndPoint(const kDB::Repository::Connection& _connection, const QString& _prefix,
             const QString& _mqtt_address = "mqtt://localhost:1883");
    ~EndPoint();
    cres_qresult<void> start();
    void stop();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBMQTT
