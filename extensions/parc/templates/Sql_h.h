// clang-format off
/%  QString guard = _database->namespaces().join("_").toUpper() + "_SQL_H_";
%/ #ifndef _/%= guard %/
#define _/%= guard %/

#include <QVariant>

namespace /%= _database->namespaces().join("::") %/::Sql
{
  enum class Sort
  {
    ASC,
    DESC
  };
  enum class Operand
  {
    EQ, GT, LT, LTE, GTE, NEQ, LIKE
  };
  namespace details
  {
    template<typename _T_, class Enable = void>
    struct value_conversion_helper;
    
    template<typename _T_>
    requires (not std::is_enum_v<_T_>)
    struct value_conversion_helper<_T_>
    {
      static knowCore::Value toValue(const _T_& _t)
      {
        return knowCore::Value::fromValue(_t);
      }
      static _T_ fromValue(const knowCore::Value& _variant)
      {
        if(_variant.isEmpty())
        {
          return _T_();
        } else {
          return _variant.value<_T_>(knowCore::TypeCheckingMode::Force).expect_success();
        }
      }
    };
    template<typename _T_>
    requires (std::is_enum_v<_T_>)
    struct value_conversion_helper<_T_>
    {
      static knowCore::Value toValue(const _T_& _t)
      {
        return knowCore::Value::fromValue(int(_t));
      }
      static _T_ fromValue(const knowCore::Value& _variant)
      {
        if(_variant.isEmpty())
        {
          return _T_();
        } else {
          return _T_(_variant.value<int>().expect_success());
        }
      }
    };
  }
  
  template<typename _T_>
  knowCore::Value toVariant(const _T_& _t)
  {
    return details::value_conversion_helper<_T_>::toValue(_t);
  }
  template<typename _T_>
  _T_ fromVariant(const knowCore::Value& _variant)
  {
    return details::value_conversion_helper<_T_>::fromValue(_variant);
  }
  
}

#endif
