// clang-format off
/%  QString guard = _database->namespaces().join("_").toUpper() + "_" + _klass->name().toUpper() + "_H_";
%/#ifndef _/%= guard %/
#define _/%= guard %/
#include <QList>
#include <tuple>

#include <kDB/Forward.h>
#include <knowCore/Global.h>
#include <knowCore/ValueList.h>

#include "Sql.h"
#include "TypesDefinitions.h"

class QString;

/% // Generate forward definition
  for(const definitions::Forward* fw : _database->forwards())
  {
    // Generate namespaces
    for(const QString& ns : fw->namespaces())
    {%/
      namespace /%= ns %/ {
  /%}%/
    class /%= fw->name() %/;
  /% // Unwind the namespaces
    for(const QString& ns : fw->namespaces())
    {
      Q_UNUSED(ns)%/
    }
  /%}
  } // Generate the namespace
  if(not _database->namespaces().isEmpty())
  {
  %/
namespace /%= _database->namespaces().join("::") %/
{/%
  }
  // Generate the forward definition for all classes in the database
  for(const definitions::Class* klass : _database->classes())
  {
    if(klass != _klass)
    {
    %/
  class /%= klass->name() %/Value;
  class /%= klass->name() %/Record;
  class /%= klass->name() %/SelectQuery;
/%
    }
  }
  for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
  {
    %/
  class /%= sklass->name() %/Value;
  class /%= sklass->name() %/Record;
  class /%= sklass->name() %/SelectQuery;
    /%
  }
  %/
  class /%= _klass->name() %/Value;
  class /%= _klass->name() %/Record;

  /% /* Generate the query class */ %/
  class /%= _klass->name() %/SelectQuery
  {
    friend class /%= _klass->name() %/Record;
    struct Private;
    Private* const d;
  private:
    /%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/);
    /%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/, const QString& _fieldName, const knowCore::Value& _value, Sql::Operand _op);
    /%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/, const QString& _expression, const QVariantList& _values);
    /%= _klass->name() %/SelectQuery(Private* _d);
  public:
    /%= _klass->name() %/SelectQuery();
    /%= _klass->name() %/SelectQuery(const /%= _klass->name() %/SelectQuery& _rhs);
    /%= _klass->name() %/SelectQuery& operator=(const /%= _klass->name() %/SelectQuery& _rhs);
    ~/%= _klass->name() %/SelectQuery();
  public:
    std::size_t count(int _count = 0) const;
    QList</%= _klass->name() %/Record> exec(int _count = 0) const;
    QList</%= _klass->name() %/Record> exec(int _skip, int _count) const;
    /%= _klass->name() %/Record first() const;
  public:
    /%= _klass->name() %/SelectQuery operator||(const /%= _klass->name() %/SelectQuery& _rhs) const;
    /%= _klass->name() %/SelectQuery operator&&(const /%= _klass->name() %/SelectQuery& _rhs) const;
  public:
/% // Generate order functions
  for(const definitions::Field* field : _klass->fields())
  { %/
    /%= _klass->name() %/SelectQuery orderBy/%= capitalizeFirstLetter(field->name()) %/(Sql::Sort _order) const;/%
  }
  %/
    /%= _klass->name() %/SelectQuery randomOrder() const;
  };
  //BEGIN fields enum
  enum class /%= _klass->name() %/Fields {/%
  // Generate field enum
  for(const definitions::Field* field : _klass->fields())
  { %/
    /%= field->name() %/,/%
  }%/
  };
  //END fields enum
  //BEGIN base value class
  class /%= baseValueClassName(_klass) %/
  {
    friend class /%= _klass->name() %/Record;
    /%
    for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
    {
      %/
      friend class /%= sklass->name() %/;
      friend class /%= sklass->name() %/SelectQuery;
      /%
    }
    %/
  protected:
    struct Private;
    Private* const d;
    KNOWCORE_D_DECL();
  protected:
    /%= baseValueClassName(_klass) %/(Private* _d);
  public:/%
    if(hasDefaultBaseValueConstructor(_klass))
    {%/
    /%= baseValueClassName(_klass) %/();/%
    }%/
    /%= baseValueClassName(_klass) %/(/%= baseValueConstructionArguments(_klass, true, true) %/);
    /%= baseValueClassName(_klass) %/(const /%= baseValueClassName(_klass) %/& _rhs);
    /%= baseValueClassName(_klass) %/& operator=(const /%= baseValueClassName(_klass) %/& _rhs);
    ~/%= baseValueClassName(_klass) %/();
  public:
    cres_qresult<knowCore::ValueHash> toValueHash() const;
    static cres_qresult</%= baseValueClassName(_klass) %/> fromValueHash(const knowCore::ValueHash& _valueHash);
  public:
    bool operator==(const /%= baseValueClassName(_klass) %/& _rhs) const;
    bool operator!=(const /%= baseValueClassName(_klass) %/& _rhs) const;
  public:
/%
  // Add field function: getter, setter and query
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {
    %/
    /%= cppReturnType(field) %/ /%= field->name() %/() const;/%
      if(not field->options().testFlag(definitions::Field::Constant) and not field->options().testFlag(definitions::Field::Key))
      {%/
    void set/%= capitalizeFirstLetter(field->name()) %/(/%= cppArgType(field) %/ _value);/%
      }
    }
  }
  %/
  private:
    /%
    if(_klass->doesImplement(definitions::INIT))
    {
      %/
    void init();/%
    }
    if(_klass->doesImplement(definitions::CLEANUP))
    {
      %/
    void cleanup();/%
    }
    if(_klass->doesImplement(definitions::EXTENDED))
    {
      %/
#include "/%= baseValueClassName(_klass) %/_.h"/%
    }
    %/
  };
  //END base value class
  /%
  if(hasExtendedValueClass(_klass))
  {
  %/
  //BEGIN extended value class
  class /%= _klass->name() %/Value : public /%= _klass->name() %/BaseValue
  {
    friend class /%= _klass->name() %/Record;
    /%
    for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
    {
      %/
      friend class /%= sklass->name() %/;
      friend class /%= sklass->name() %/SelectQuery;
      /%
    }
    %/
  protected:
    KNOWCORE_D_DECL();
  protected:
    /%= _klass->name() %/Value(Private* _d);
  private:
    void init(const kDB::Repository::QueryConnectionInfo& _connection, /%= extendedValueConstructionArguments(_klass, true, false) %/);
  public:
    /%= _klass->name() %/Value();
    /%= _klass->name() %/Value(const kDB::Repository::QueryConnectionInfo& _connection, /%= extendedValueConstructionArguments(_klass, true, false) %/);
    /%= _klass->name() %/Value(const kDB::Repository::QueryConnectionInfo& _connection, const /%= _klass->name() %/BaseValue& _base_value, /%= onlyExtendedValueConstructionArguments(_klass, true, false) %/);
    /%= _klass->name() %/Value(const /%= _klass->name() %/Value& _rhs);
    /%= _klass->name() %/Value& operator=(const /%= _klass->name() %/Value& _rhs);
    ~/%= _klass->name() %/Value();
  public:
    bool operator==(const /%= _klass->name() %/Value& _rhs) const;
    bool operator!=(const /%= _klass->name() %/Value& _rhs) const;
  public:
/%
  // Add field function: getter, setter and query
  for(const definitions::Field* field : _klass->fields())
  {
    if(isExtendedValueField(_klass, field))
    {
    %/
    /%= cppReturnType(field) %/ /%= field->name() %/() const;/%
      if(not field->options().testFlag(definitions::Field::Constant) and not field->options().testFlag(definitions::Field::Key))
      {%/
    void set/%= capitalizeFirstLetter(field->name()) %/(/%= cppArgType(field) %/ _value);/%
      }
    }
  }
  %/
  private:
    /%
    if(_klass->doesImplement(definitions::INIT))
    {
      %/
    void init();/%
    }
    if(_klass->doesImplement(definitions::CLEANUP))
    {
      %/
    void cleanup();/%
    }
    if(_klass->doesImplement(definitions::EXTENDED))
    {
      %/
#include "/%= _klass->name() %/Value_.h"/%
    }
    %/
  public:
    kDB::Repository::QueryConnectionInfo connection() const;
  };
  //END extended value class
  /%
  }
  %/
  //BEGIN record class
  class /%= _klass->name() %/Record : public /%= _klass->name() %/Value
  {
    /%
    for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
    {
      %/
      friend class /%= sklass->name() %/;
      friend class /%= sklass->name() %/SelectQuery;
      /%
    }
    %/
  public:
    using SelectQuery = /%= _klass->name() %/SelectQuery;
    using SqlSort = /%= _database->namespaces().join("::") %/::Sql::Sort;
    using SqlOperand = /%= _database->namespaces().join("::") %/::Sql::Operand;
  public:
    /%
    if(not _klass->classOfContainerDefinition())
    {
      %/
      /**
       * This function will ensure that the table for those objects is created in the database.
       * If the table already exists, this function does nothing.
       */
      static cres_qresult<void> createTable(const kDB::Repository::QueryConnectionInfo& _connection);
      /%
    }
    %/
    static cres_qresult</%= _klass->name() %/Record> create(/%= databaseOrContainer(_klass) %/, /%= createArguments(_klass, true) %/);
    static cres_qresult<QList</%= _klass->name() %/Record>> create(/%= databaseOrContainer(_klass) %/, const QList</%= _klass->name() %/Value>& _values);
    enum LockMode
    {
      AccessShare, RowShare, RowExclusive, ShareUpdateExclusive, Share, ShareRowExclusive, Exclusive, AccessExclusive
    };
    static cres_qresult<void> lock(const kDB::Repository::Transaction& _transaction, LockMode _mode = LockMode::AccessExclusive);
  public:
    /%= _klass->name() %/Record();
    /%= _klass->name() %/Record(const /%= _klass->name() %/Record& _rhs);
    /%= _klass->name() %/Record& operator=(const /%= _klass->name() %/Record& _rhs);
    /%= _klass->name() %/Record(/%= databaseOrContainer(_klass) %/ /%= constructorKeyArguments(_klass) %/);
    ~/%= _klass->name() %/Record();
  public:
    bool operator==(const /%= _klass->name() %/Record& _rhs) const;
    /**
     * Similar to @ref operator== but only check if the values are identical, ignoring automatic keys and connection
     */
    bool sameValuesAs(const /%= _klass->name() %/Record& _rhs) const;
    bool operator!=(const /%= _klass->name() %/Record& _rhs) const;
  public:
    cres_qresult<void> discard();
    cres_qresult<void> refresh();
    /**
     * Record into the database
     */
    cres_qresult<void> record();
    /**
     * Record into the database, specified with \ref _connection
     */
    cres_qresult<void> record(const kDB::Repository::QueryConnectionInfo& _connection);
    /**
     * Erase from the database
     */
    cres_qresult<void> erase();
    /**
     * Erase from the database, specified with \ref _connection
     */
    cres_qresult<void> erase(const kDB::Repository::QueryConnectionInfo& _connection);
    /**
     * @return true if the object is stored in a database
     */
    bool isPersistent() const;
  public:
/%
  // Add function to access children
  if(not _klass->children().empty())
  {
    for(const definitions::Class* child : _klass->children())
    {
      if(hasSingleChild(child, _klass))
      {%/
    /%= child->name() %/ /%= childfunctionname(child->name()) %/() const;/%
      } else {
    %/
    /%= child->name() %/SelectQuery /%= childrenfunctionname(child->name()) %/() const;/%
      }
    }
  }
  
  // Add field function: getter for auto, and query for everything
  for(const definitions::Field* field : _klass->fields())
  {
    if(isRecordField(_klass, field))
    {%/
    /%= cppReturnType(field) %/ /%= field->name() %/() const;/%
    }
    %/
    static SelectQuery by/%= capitalizeFirstLetter(field->name()) %/(/%= databaseOrContainer(_klass) %/, /%= cppArgType(field) %/, Sql::Operand _op = Sql::Operand::EQ);/%
  }
  %/
  
    /**
     * Use an arbitrary expression for filtering, it must evaluate to a boolean
     */
    static SelectQuery by(/%= databaseOrContainer(_klass) %/, const QString& _expression, const QVariantList& _values); // QVariantList has to be used, because knowCore::Value cannot hold a /%= _klass->name() %/Fields
    
    template<typename _T1_, typename ... _T_>
    static SelectQuery by(/%= databaseOrContainer(_klass) %/, const QString& _expression, _T1_ _v, _T_... _values)
    {
      return by(/%= databaseOrContainerParameterName(_klass) %/, _expression, QVariantList(), _v, _values...);
    }
  private:
    template<typename _T1_, typename ... _T_>
    static SelectQuery by(/%= databaseOrContainer(_klass) %/, const QString& _expression, const QVariantList& _arguments, _T1_ _v,  _T_... _values)
    {
      QVariantList args = _arguments; args.append(QVariant::fromValue(_v));
      return by(/%= databaseOrContainerParameterName(_klass) %/, _expression, args, _values...);
    }
    
    // Add all queries
  public:
    static SelectQuery all(/%= databaseOrContainer(_klass) %/);

  public:
/%  for(const definitions::Mapping* mapping : _klass->mappings())
  {
    const definitions::Class* klass = mapping->other(_klass);%/
    QList</%= klass->name() %/> /%= functiongetallname(klass->name()) %/() const;
    cres_qresult<void> add/%= klass->name() %/(const /%= klass->name() %/& _other);
    cres_qresult<void> remove/%= klass->name() %/(const /%= klass->name() %/& _other);
/%}%/
  private:
    /%
    if(_klass->doesImplement(definitions::INIT))
    {
      %/
      void init();/%
    }
    if(_klass->doesImplement(definitions::CLEANUP))
    {
      %/
      void cleanup();/%
    }
    if(_klass->doesImplement(definitions::EXTENDED))
    {
      %/
#include "/%= _klass->name() %/Record_.h"/%
    }
    %/
  public:
    kDB::Repository::QueryConnectionInfo connection() const;
  private:
    KNOWCORE_D_DECL();
  };
  /%
  //END Generate the record class
  if(hasExtendedValueClass(_klass))
  {%/
  //BEGIN Extended value constructor with default
  inline /%= _klass->name() %/Value::/%= _klass->name() %/Value(const kDB::Repository::QueryConnectionInfo& _connection, /%= extendedValueConstructionArguments(_klass, true, true) %/) : /%= _klass->name() %/Value()
  {
    init(_connection, /%= extendedValueConstructionArguments(_klass, false, false) %/);
  }
  /%
  //END Extended value constructor with default
  }
if(not _database->namespaces().isEmpty())
{
%/
}/%
}
%/

#include <QMetaType>
/%
  if(hasExtendedValueClass(_klass))
  {%/
Q_DECLARE_METATYPE(/%= _database->namespaces().join("::") %/::/%= _klass->name() %/BaseValue)/%
  }
%/
Q_DECLARE_METATYPE(/%= _database->namespaces().join("::") %/::/%= _klass->name() %/Value)
Q_DECLARE_METATYPE(/%= _database->namespaces().join("::") %/::/%= _klass->name() %/Record)
Q_DECLARE_METATYPE(/%= _database->namespaces().join("::") %/::/%= _klass->name() %/Fields)

#include <knowCore/Formatter.h>

clog_format_declare_formatter(/%= _database->namespaces().join("::") %/::/%= _klass->name() %/Record)
{
  return format_to(ctx.out(), "'/%= _database->namespaces().join("::") %/::/%= _klass->name() %/Record'");
}

#endif
