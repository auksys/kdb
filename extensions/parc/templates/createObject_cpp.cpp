// clang-format off
/%
clog_assert(queryGenerator()->createReturnValues());
%/
  
  knowDBC::Query query = connection.createSQLQuery();
  query.setQuery(QString("/%= queryGenerator()->generateCreateQuery(klass, false) %/")/%= tableQueryArg(klass, "_container") %/);
/%
for(const parc::definitions::Field* field : klass->fields())
{
  if(not field->options().testFlag(definitions::Field::Auto))
  {
  %/
  query.bindValue(":/%= field->name() %/", /%= argumentToBinding(field, "_" + field->name()) %/);/%
  }
}
%/
  knowDBC::Result result = query.execute();
  if(result)
  {
    /%= klass->name() %/Record obj;
    obj.D()->connection = connection;/%
  if(_klass->classOfContainerDefinition())
  {
    %/
    obj.D()->container = _container;
  /%
  }
  int auto_index = 0;
  for(const definitions::Field* field : klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Auto))
    {
      %/
    obj.D()->/%= field->name() %/ = result.value<int>(0, /%= auto_index++ %/).expect_success();/%
    } else {
      %/
    obj.D()->/%= field->name() %/ = /%= valueToMember(field->sqlType(), "_" + field->name()) %/;/%
    }
  }

  for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
  {
    // Need to create a table
    const definitions::Field* keyfield = klass->keys().first();
    %/
    /%= cppReturnType(keyfield) %/ object_key = obj./%= keyfield->name() %/();
    query.setQuery(QString("/%= queryGenerator()->createTable(sklass) %/").arg(/%= valueToMember(keyfield->sqlType(), "object_key") %/));
    result = query.execute();
    if(not result)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to create table for sub definition", result);
    }
    /%
  }
  %/
  
    return cres_success(obj);
  } else {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to create object", result);
  }
