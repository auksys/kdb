// clang-format off
#include "/%= _klass->name() %/.h"

#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryTransaction.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/SQLCopyData.h>
#include <kDB/Repository/Transaction.h>
#include <QStringList>

/%
  // TODO fine grain to only include needed classes
  foreach(const definitions::Class* klass, _database->classes())
  {
%/
    #include "/%= klass->name() %/.h"
/%
  }
for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
{
  %/
    #include "/%= sklass->name() %/.h"

  /%
}
%/

using namespace /%= _database->namespaces().join("::") %/;
namespace Repository = kDB::Repository;

//BEGIN SelectQuery class

struct /%= _klass->name() %/SelectQuery::Private
{
  Private* clone() const;
  struct WhereDefinition
  {
    virtual ~WhereDefinition() {}
    virtual QString generateWhere(int& _counter) = 0;
    virtual void setBinding(knowDBC::Query& _query) = 0;
    virtual WhereDefinition* clone() = 0;
  };
  struct All : public WhereDefinition
  {
    virtual QString generateWhere(int& _counter);
    virtual void setBinding(knowDBC::Query& _query);
    virtual WhereDefinition* clone();
  };
  struct FieldComp : public WhereDefinition
  {
    QString fieldName;
    QString bindName;
    knowCore::Value value;
    Sql::Operand op;
    virtual QString generateWhere(int& _counter);
    virtual void setBinding(knowDBC::Query& _query);
    virtual WhereDefinition* clone();
  };
  struct FieldExpression : public WhereDefinition
  {
    knowCore::ValueHash bindings;
    QString expression;
    QVariantList values;
    virtual QString generateWhere(int& _counter);
    virtual void setBinding(knowDBC::Query& _query);
    virtual FieldExpression* clone();
  };
  struct Operator : public WhereDefinition
  {
    virtual ~Operator();
    WhereDefinition* def_left;
    WhereDefinition* def_right;
    virtual void setBinding(knowDBC::Query& _query);
  };
  struct AndOperator : public Operator
  {
    virtual QString generateWhere(int& _counter);
    virtual WhereDefinition* clone();
  };
  struct OrOperator : public Operator
  {
    virtual QString generateWhere(int& _counter);
    virtual WhereDefinition* clone();
  };

  kDB::Repository::QueryConnectionInfo connection;/%
  if(_klass->classOfContainerDefinition())
  {
    %/
  /%= _klass->classOfContainerDefinition()->name() %/Record container;/%
  }
  %/
  
  WhereDefinition* where;
  QStringList orderBy;
};

/%= _klass->name() %/SelectQuery::Private* /%= _klass->name() %/SelectQuery::Private::clone() const
{
  Private* nd = new Private(*this);
  if(where)
  {
    nd->where = where->clone();
  }
  return nd;
}


QString /%= _klass->name() %/SelectQuery::Private::All::generateWhere(int& )
{
  return "TRUE";
}

void /%= _klass->name() %/SelectQuery::Private::All::setBinding(knowDBC::Query& /*_query*/)
{
}

/%= _klass->name() %/SelectQuery::Private::WhereDefinition* /%= _klass->name() %/SelectQuery::Private::All::clone()
{
  return new All;
}


QString /%= _klass->name() %/SelectQuery::Private::FieldComp::generateWhere(int& _counter)
{
  bindName = QString(":bv%1").arg(++_counter);
  switch(op)
  {
    case Sql::Operand::EQ:
      return QString("/%= queryGenerator()->generateWhereEq() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::GT:
      return QString("/%= queryGenerator()->generateWhereGT() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LT:
      return QString("/%= queryGenerator()->generateWhereLT() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LTE:
      return QString("/%= queryGenerator()->generateWhereLTE() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::GTE:
      return QString("/%= queryGenerator()->generateWhereGTE() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::NEQ:
      return QString("/%= queryGenerator()->generateWhereNEq() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LIKE:
      return QString("/%= queryGenerator()->generateWhereLike() %/").arg(fieldName).arg(bindName);
      break;
  }
  qFatal("Unknown query");
}

void /%= _klass->name() %/SelectQuery::Private::FieldComp::setBinding(knowDBC::Query& _query)
{
  _query.bindValue(bindName, value);
}

/%= _klass->name() %/SelectQuery::Private::WhereDefinition* /%= _klass->name() %/SelectQuery::Private::FieldComp::clone()
{
  FieldComp* c = new FieldComp;
  *c = *this;
  return c;
}

QString /%= _klass->name() %/SelectQuery::Private::FieldExpression::generateWhere(int& _counter)
{
  bindings.clear();
  
  QStringList variable_names;
  
  for(const QVariant& var : values)
  {
    if(var.userType() == qMetaTypeId</%= _klass->name() %/Fields>())
    {
      switch(var.value</%= _klass->name() %/Fields>())
      {/%
        for(const definitions::Field* field : _klass->fields())
        {%/
        case /%= _klass->name() %/Fields::/%= field->name() %/:
          variable_names.append("/%= queryGenerator()->fieldName(field) %/");
          break;
          /%
        }%/
      }
    } else  {
      QString bindName = clog_qt::qformat(":bv{}", ++_counter);
      bindings.insert(bindName, knowCore::Value::fromVariant(var).expect_success());
      variable_names.append(bindName);
    }
  }
  QString expr = expression;
  for(const QString& vn : variable_names)
  {
    expr = expr.arg(vn);
  }
  
  return expr;
}

void /%= _klass->name() %/SelectQuery::Private::FieldExpression::setBinding(knowDBC::Query& _query)
{
  _query.bindValues(bindings);
}

/%= _klass->name() %/SelectQuery::Private::FieldExpression* /%= _klass->name() %/SelectQuery::Private::FieldExpression::clone()
{
  return new FieldExpression(*this);
}

/%= _klass->name() %/SelectQuery::Private::Operator::~Operator()
{
  delete def_left;
  delete def_right;
}

void /%= _klass->name() %/SelectQuery::Private::Operator::setBinding(knowDBC::Query& _query)
{
  def_left->setBinding(_query);
  def_right->setBinding(_query);
}

QString /%= _klass->name() %/SelectQuery::Private::AndOperator::generateWhere(int& _counter)
{
  return "(" + def_left->generateWhere(_counter) + " AND " + def_right->generateWhere(_counter) + ")";     
}

/%= _klass->name() %/SelectQuery::Private::WhereDefinition* /%= _klass->name() %/SelectQuery::Private::AndOperator::clone()
{
  AndOperator* op = new AndOperator();
  op->def_left  = def_left->clone();
  op->def_right = def_right->clone();
  return op;
}

QString /%= _klass->name() %/SelectQuery::Private::OrOperator::generateWhere(int& _counter)
{
  return "(" + def_left->generateWhere(_counter) + " OR " + def_right->generateWhere(_counter) + ")";     
}

/%= _klass->name() %/SelectQuery::Private::WhereDefinition* /%= _klass->name() %/SelectQuery::Private::OrOperator::clone()
{
  OrOperator* op = new OrOperator();
  op->def_left  = def_left->clone();
  op->def_right = def_right->clone();
  return op;
}

/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/) : d(new Private)
{
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
    d->connection = _container.connection();
    d->container = _container;
    /%
  } else {
    %/
    d->connection = _connection;
    /%
  }
  %/
  d->where = new Private::All;
}
    
/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/, const QString& _fieldName,
                                                                   const knowCore::Value& _value, Sql::Operand _op) : d(new Private)
{
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
    d->connection = _container.connection();
    d->container = _container;
    /%
  } else {
    %/
    d->connection = _connection;
    /%
  }
  %/
  Private::FieldComp* fc = new Private::FieldComp;
  fc->fieldName = _fieldName;
  fc->value     = _value;
  fc->op        = _op;
  d->where      = fc;
}

/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/, const QString& _expression, const QVariantList& _values) : d(new Private)
{
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
    d->connection = _container.connection();
    d->container = _container;
    /%
  } else {
    %/
    d->connection = _connection;
    /%
  }
  %/
  Private::FieldExpression* fc = new Private::FieldExpression;
  fc->expression    = _expression;
  fc->values        = _values;
  d->where          = fc;
}

/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery() : d(new Private)
{
  d->connection = kDB::Repository::QueryConnectionInfo();
  d->where      = 0;
}

/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(/%= _klass->name() %/SelectQuery::Private* _d) : d(_d)
{
}

/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(const /%= _klass->name() %/SelectQuery& _rhs) : d(_rhs.d->clone())
{
}

/%= _klass->name() %/SelectQuery& /%= _klass->name() %/SelectQuery::operator=(const /%= _klass->name() %/SelectQuery& _rhs)
{
  *d = *_rhs.d;
  if(d->where)
  {
    d->where = d->where->clone();
  }
  return *this;
}

/%= _klass->name() %/SelectQuery::~/%= _klass->name() %/SelectQuery()
{
  delete d->where;
  delete d;
}

std::size_t /%= _klass->name() %/SelectQuery::count(int _count) const
{
  QString limit;
  if(_count != 0)
  {
    limit = QString("/%= queryGenerator()->generateLimit() %/").arg(_count);
  } 
  knowDBC::Query query = d->connection.createSQLQuery();
  int count = 0;
  query.setQuery(QString("/%= queryGenerator()->generateSelectCount(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/.arg(d->where->generateWhere(count)).arg(limit));
  d->where->setBinding(query);
  
  knowDBC::Result result = query.execute();
  
  if(result)
  {
    return result.value<int>(0, 0).expect_success();
  } else {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to count objects", result);
    return -1;
  }
}

QList</%= _klass->name() %/Record> /%= _klass->name() %/SelectQuery::exec(int _count) const
{
  return exec(0, _count);
}

QList</%= _klass->name() %/Record> /%= _klass->name() %/SelectQuery::exec(int _skip, int _count) const
{
  QString limit;
  if(_count == 0)
  {
    if(_skip != 0) limit = QString("/%= queryGenerator()->generateOffset() %/").arg(_skip);
  } else if(_skip == 0)
  {
    if(_count != 0) limit = QString("/%= queryGenerator()->generateLimit() %/").arg(_count);
  } else
  {
    limit = QString("/%= queryGenerator()->generateOffsetLimit() %/").arg(_skip).arg(_count);
  }
  knowDBC::Query query = d->connection.createSQLQuery();
  if(d->orderBy.empty())
  {
    int count = 0;
    query.setQuery(QString("/%= queryGenerator()->generateSelect(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/.arg(d->where->generateWhere(count)).arg(limit));
  } else {
    int count = 0;
    query.setQuery(QString("/%= queryGenerator()->generateSelectOrderBy(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/.arg(d->where->generateWhere(count)).arg(d->orderBy.join(",")).arg(limit));
  }
  d->where->setBinding(query);
  /%
  makeResultList(stream, _klass);
  %/
}

/%= _klass->name() %/Record /%= _klass->name() %/SelectQuery::first() const
{
  QList</%= _klass->name() %/Record> list = exec(1);
  if(list.isEmpty()) return /%= _klass->name() %/Record();
  return list.first();
}

/%= _klass->name() %/SelectQuery /%= _klass->name() %/SelectQuery::operator||(const /%= _klass->name() %/SelectQuery& _rhs) const
{
  if(not d->where)
  {
    return _rhs;
  }
  clog_assert(d->connection == _rhs.d->connection);
  Private* nd = new Private;
  nd->connection = d->connection;/%
  if(_klass->classOfContainerDefinition())
  {
    %/
  nd->container = d->container;
  clog_assert(d->container == _rhs.d->container);/%
  }
  %/

  if(not d->where) nd->where = _rhs.d->where->clone();
  else if(not _rhs.d->where) nd->where = d->where->clone();
  else {
    Private::OrOperator* aop = new Private::OrOperator;
    aop->def_left = d->where->clone();
    aop->def_right = _rhs.d->where->clone();
    nd->where = aop;
  }
  nd->orderBy = d->orderBy + _rhs.d->orderBy;
  return /%= _klass->name() %/SelectQuery(nd);
}

/%= _klass->name() %/SelectQuery /%= _klass->name() %/SelectQuery::operator&&(const /%= _klass->name() %/SelectQuery& _rhs) const
{
  if(not d->where)
  {
    return _rhs;
  }
  clog_assert(d->connection == _rhs.d->connection);
  Private* nd = new Private;
  nd->connection = d->connection;/%
  if(_klass->classOfContainerDefinition())
  {
    %/
  nd->container = d->container;
  clog_assert(d->container == _rhs.d->container);/%
  }
  %/
  if(not d->where) nd->where = _rhs.d->where->clone();
  else if(not _rhs.d->where) nd->where = d->where->clone();
  else {
    Private::AndOperator* aop = new Private::AndOperator;
    aop->def_left = d->where->clone();
    aop->def_right = _rhs.d->where->clone();
    nd->where = aop;
  }
  nd->orderBy = d->orderBy + _rhs.d->orderBy;
  return /%= _klass->name() %/SelectQuery(nd);
}
/%
  foreach(const definitions::Field* field, _klass->fields())
  {%/
    /%= _klass->name() %/SelectQuery /%= _klass->name() %/SelectQuery::orderBy/%= capitalizeFirstLetter(field->name()) %/(Sql::Sort _order) const
    {
      Private* nd = d->clone();
      switch(_order)
      {
      case Sql::Sort::ASC:
        nd->orderBy.append("/%= queryGenerator()->generateOrderAsc(field) %/");
        break;
      case Sql::Sort::DESC:
        nd->orderBy.append("/%= queryGenerator()->generateOrderDsc(field) %/");
        break;
      }
      return /%= _klass->name() %/SelectQuery(nd);
    }
/%}%/
  /%= _klass->name() %/SelectQuery /%= _klass->name() %/SelectQuery::randomOrder() const
  {
    Private* nd = d->clone();
    nd->orderBy.append("/%= queryGenerator()->generateRandomOrder() %/");
    return /%= _klass->name() %/SelectQuery(nd);
  }
//END SelectQuery class
//BEGIN Base Value class
struct /%= baseValueClassName(_klass) %/::Private
{
  virtual ~Private() {}
  bool modified = false;
  /%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {
    %/
  /%= cppMemberType(field) %/ /%= field->name() %/ = {};/%
      if(not field->options().testFlag(definitions::Field::Key)
          and not field->options().testFlag(definitions::Field::Constant))
      {
      %/
  bool /%= field->name() %/_modified = false;/%
      }
    }
  }
  %/
};

//BEGIN Constructors

KNOWCORE_D_FUNC_DEF(/%= baseValueClassName(_klass) %/)

/%= baseValueClassName(_klass) %/::/%= baseValueClassName(_klass) %/(Private* _d) : d(_d)
{/%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
}
/%
if(hasDefaultBaseValueConstructor(_klass))
{%/
/%= baseValueClassName(_klass) %/::/%= baseValueClassName(_klass) %/()
  : /%= baseValueClassName(_klass) %/(new Private)
{
}/%
}
%/

/%= baseValueClassName(_klass) %/::/%= baseValueClassName(_klass) %/(const /%= baseValueClassName(_klass) %/& _rhs)
  : /%= baseValueClassName(_klass) %/(new Private(*_rhs.d))
{
}

/%= baseValueClassName(_klass) %/& /%= baseValueClassName(_klass) %/::operator=(const /%= baseValueClassName(_klass) %/& _rhs)
{
  *d = *_rhs.d;/%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
  return *this;
}

/%= baseValueClassName(_klass) %/::/%= baseValueClassName(_klass) %/(/%= baseValueConstructionArguments(_klass, true, false) %/) : /%= baseValueClassName(_klass) %/(new Private)
{/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {%/
  d->/%= field->name() %/ = _/%= field->name() %/;/%
    }
  }%/
}

/%= baseValueClassName(_klass) %/::~/%= baseValueClassName(_klass) %/()
{/%
  if(_klass->doesImplement(definitions::CLEANUP))
  {
    %/
  cleanup();/%
  }
  %/
  delete d;
}

//END Constructors

//BEGIN to/FromValueHash

cres_qresult<knowCore::ValueHash> /%= baseValueClassName(_klass) %/::toValueHash() const
{
  knowCore::ValueHash result_hash;/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {
    %/
  result_hash.insert("/%= field->name() %/", d->/%= field->name() %/);/%
    }
  }
  %/
  return cres_success(result_hash);
}

cres_qresult</%= baseValueClassName(_klass) %/> /%= baseValueClassName(_klass) %/::fromValueHash(const knowCore::ValueHash& _valueHash)
{
  /%= baseValueClassName(_klass) %/ value;/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {
    %/
  {
    cres_try(value.d->/%= field->name() %/, _valueHash.value("/%= field->name() %/").value</%= cppMemberType(field) %/>());
  }/%
    }
  }
  %/
  return cres_success(value);
}

//END to/FromValueHash

//BEGIN comparison operators

bool /%= baseValueClassName(_klass) %/::operator==(const /%= baseValueClassName(_klass) %/& _rhs) const
{
  return true/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {
       %/ and d->/%= field->name() %/ == _rhs.d->/%= field->name()%//%
    }
  }
  %/;
}

bool /%= baseValueClassName(_klass) %/::operator!=(const /%= baseValueClassName(_klass) %/& _rhs) const
{
  return not(*this == _rhs);
}

//END comparison operators

//BEGIN field getter and setter
/%
for(const definitions::Field* field : _klass->fields())
{
  if(isBaseValueField(_klass, field))
  {
  %/
/%= cppReturnType(field) %/ /%= baseValueClassName(_klass) %/::/%= field->name() %/() const
{
  return /%= memberToValue(field, QString("d->{}")) %/;
}
  /%
    if(not field->options().testFlag(definitions::Field::Constant) and not field->options().testFlag(definitions::Field::Key))
    {%/
void /%= baseValueClassName(_klass) %/::set/%= capitalizeFirstLetter(field->name()) %/(/%= cppArgType(field) %/ _value)
{
  d->modified = true;
  d->/%= field->name() %/_modified = true;
  d->/%= field->name() %/ = /%= valueToMember(field->sqlType(), "_value") %/;
}
    /%
    }
  }
}
%/

//END field getter and setter

//END Base Value class
/%
if(hasExtendedValueClass(_klass))
{
%/
//BEGIN Extended Value class
struct /%= _klass->name() %/Value::Private : public /%= baseValueClassName(_klass) %/::Private
{
  kDB::Repository::QueryConnectionInfo connection;/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isExtendedValueField(_klass, field))
    {
    %/
  /%= cppMemberType(field) %/ /%= field->name() %/ = {};/%
      if(not field->options().testFlag(definitions::Field::Key)
          and not field->options().testFlag(definitions::Field::Constant))
      {
      %/
  bool /%= field->name() %/_modified = false;/%
      }
    }
  }
  %/
};

KNOWCORE_D_FUNC_DEF(/%= _klass->name() %/Value)

//BEGIN Constructors

/%= _klass->name() %/Value::/%= _klass->name() %/Value(Private* _d) : /%= baseValueClassName(_klass) %/(_d)
{/%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
}

/%= _klass->name() %/Value::/%= _klass->name() %/Value()
  : /%= _klass->name() %/Value(new Private)
{
}

/%= _klass->name() %/Value::/%= _klass->name() %/Value(const /%= _klass->name() %/Value& _rhs)
  : /%= _klass->name() %/Value(new Private(*_rhs.D()))
{
}

/%= _klass->name() %/Value::/%= _klass->name() %/Value(const kDB::Repository::QueryConnectionInfo& _connection, const /%= _klass->name() %/BaseValue& _base_value, /%= onlyExtendedValueConstructionArguments(_klass, true, false) %/) : /%= _klass->name() %/Value(new Private)
{
  init(_connection/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {%/, _base_value./%= field->name() %/()/%
    } else if(isExtendedValueField(_klass, field))
    {%/, _/%= field->name() %//%
    }
  }%/);
}


/%= _klass->name() %/Value& /%= _klass->name() %/Value::operator=(const /%= _klass->name() %/Value& _rhs)
{
  *d = *_rhs.d;/%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
  return *this;
}

void /%= _klass->name() %/Value::init(const kDB::Repository::QueryConnectionInfo& _connection, /%= extendedValueConstructionArguments(_klass, true, false) %/)
{
  D()->connection = _connection;
  /%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {%/
  d->/%= field->name() %/ = _/%= field->name() %/;/%
    } else if(isExtendedValueField(_klass, field))
    {%/
  D()->/%= field->name() %/ = /%= valueToMember(field->sqlType(), "_" + field->name()) %/;/%
    }
  }%/
}

/%= _klass->name() %/Value::~/%= _klass->name() %/Value()
{/%
  if(_klass->doesImplement(definitions::CLEANUP))
  {
    %/
  cleanup();/%
  }
  %/
}

//END Constructors

//BEGIN comparison operators

bool /%= _klass->name() %/Value::operator==(const /%= _klass->name() %/Value& _rhs) const
{
  return this->/%= baseValueClassName(_klass) %/::operator==(_rhs)/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(isExtendedValueField(_klass, field))
    {
       %/ and D()->/%= field->name() %/ == _rhs.D()->/%= field->name()%//%
    }
  }
  %/;
}

bool /%= _klass->name() %/Value::operator!=(const /%= _klass->name() %/Value& _rhs) const
{
  return not(*this == _rhs);
}

//END comparison operators

//BEGIN field getter and setter
/%
for(const definitions::Field* field : _klass->fields())
{
  if(isExtendedValueField(_klass, field))
  {
  %/
/%= cppReturnType(field) %/ /%= _klass->name() %/Value::/%= field->name() %/() const
{
  return /%= memberToValue(field, QString("D()->{}")) %/;
}
  /%
    if(not field->options().testFlag(definitions::Field::Constant) and not field->options().testFlag(definitions::Field::Key))
    {%/
void /%= _klass->name() %/Value::set/%= capitalizeFirstLetter(field->name()) %/(/%= cppArgType(field) %/ _value)
{
  d->modified = true;
  D()->/%= field->name() %/_modified = true;
  D()->/%= field->name() %/ = /%= valueToMember(field->sqlType(), "_value") %/;
}
    /%
    }
  }
}
%/

//END field getter and setter

//END Extended Value class
/%
}
%/
//BEGIN Record class
struct /%= _klass->name() %/Record::Private : public /%= _klass->name() %/Value::Private
{/%
  if(not hasExtendedValueClass(_klass))
  {
  %/
  Repository::QueryConnectionInfo connection;/%
  }
  if(_klass->classOfContainerDefinition())
  {
    %/
  /%= _klass->classOfContainerDefinition()->name() %/Record container;/%
  }
  for(const definitions::Field* field : _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Auto))
    {
    %/
  /%= cppMemberType(field) %/ /%= field->name() %/ = {};/%
    }
  }
  %/
};

KNOWCORE_D_FUNC_DEF(/%= _klass->name() %/Record)

//BEGIN Constructors

/%= _klass->name() %/Record::/%= _klass->name() %/Record() : /%= _klass->name() %/Value(new Private)
{
  D()->connection = kDB::Repository::QueryConnectionInfo();/%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
}

/%= _klass->name() %/Record::/%= _klass->name() %/Record(const /%= _klass->name() %/Record& _rhs) : /%= _klass->name() %/Value(new Private(*_rhs.D()))
{/%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
}

/%= _klass->name() %/Record& /%= _klass->name() %/Record::operator=(const /%= _klass->name() %/Record& _rhs)
{
  *D() = *_rhs.D();/%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
  return *this;
}

/%= _klass->name() %/Record::/%= _klass->name() %/Record(/%= databaseOrContainer(_klass) %/ /%= constructorKeyArguments(_klass) %/) : /%= _klass->name() %/Value(new Private())
{/%
  if(_klass->classOfContainerDefinition())
  {
    %/
  D()->connection = _container.connection();
  D()->container = _container;/%
  } else {
    %/
  D()->connection = _connection;/%
  }
  for(const definitions::Field* field : _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      %/
  D()->/%= field->name() %/ = /%= sqlToVariant(field->sqlType(), "_" + field->name()) %/;/%
    }
  }
  %/
  refresh();/%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
}

/%= _klass->name() %/Record::~/%= _klass->name() %/Record()
{
  clog_assert(not d->modified);/%
  if(_klass->doesImplement(definitions::CLEANUP))
  {
    %/
  cleanup();/%
  }
  %/
}

//END Constructors

//BEGIN Create table

/%
if(not _klass->classOfContainerDefinition())
{
  %/
cres_qresult<void> /%= _klass->name() %/Record::createTable(const kDB::Repository::QueryConnectionInfo& _connection)
{
  knowDBC::Query query = _connection.createSQLQuery("/%= queryGenerator()->checkTableExistence(_klass) %/");
  knowDBC::Result result = query.execute();
  if(result)
  {
    if(knowCore::ValueCast<bool> r = result.value(0,0))
    {
      if(not r.value())
      {
        query.setQuery("/%= queryGenerator()->createTable(_klass) %/");
        result = query.execute();
        if(not result)
        {
          KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to create table", result);
        }
        /%
        for(const definitions::Field* field : _klass->fields())
        {
          if(field->options().testFlag(definitions::Field::Indexed))
          {
            QList<const definitions::Field*> fields;
            fields.append(field);
            %/
            query.setQuery("/%= queryGenerator()->createIndex(_klass, fields) %/");
            result = query.execute();
            if(not result)
            {
              KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to create index", result);
            }
            /%
          }
        }
        for(const definitions::Index* index : _klass->indexes())
        {
          %/
          query.setQuery("/%= queryGenerator()->createIndex(_klass, index->fields()) %/");
          result = query.execute();
          if(not result)
          {
            KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to create index", result);
          }
          /%
        }%/
      }
      return cres_success();
    } else {
      KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to check for table existance, expected a boolean", result);
    }
  } else {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to check for table existance", result);
  }
}

/%
}
%/

//END Create table

//BEGIN Create record

cres_qresult</%= _klass->name() %/Record> /%= _klass->name() %/Record::create(/%= databaseOrContainer(_klass) %/, /%= createArguments(_klass, false) %/)
{
  /%
  const definitions::Class* klass = _klass;
  if(_klass->classOfContainerDefinition())
  {
  %/
  kDB::Repository::QueryConnectionInfo connection = _container.connection();/%
  } else {
  %/
  kDB::Repository::QueryConnectionInfo connection = _connection;/%
  }
#include "createObject_cpp.h"
  %/
}

cres_qresult<QList</%= _klass->name() %/Record>> /%= _klass->name() %/Record::create(/%= databaseOrContainer(_klass) %/, const QList</%= _klass->name() %/Value>& _values)
{/%
  if(_klass->classOfContainerDefinition())
  {
  %/
  kDB::Repository::QueryConnectionInfo connection = _container.connection();
  /%
  } else {
  %/
  kDB::Repository::QueryConnectionInfo connection = _connection;
  /%
  }%/
  kDB::Repository::TemporaryTransaction transaction(connection);
  knowDBC::Query q = transaction.transaction().createSQLQuery("/%= queryGenerator()->generateCopyQuery(_klass, false) %/");
  KDB_REPOSITORY_EXECUTE_QUERY(clog_qt::qformat("Failed to initiate copy of {} /%= _klass->name() %/", _values.size()), q);
  kDB::Repository::DatabaseInterface::PostgreSQL::SQLCopyData scd(transaction.transaction());
  scd.writeHeader();
  scd.write<quint32>(0, false); // No oid
  scd.write<quint32>(0, false); // No extra
  
  for(const /%= _klass->name() %/Value& v : _values)
  {/%
    qsizetype fields_count = 0;
    for(const parc::definitions::Field* field : _klass->fields())
    {
      if(not field->options().testFlag(definitions::Field::Auto)
        and not field->options().testFlag(definitions::Field::Dependent))
      {
        ++fields_count;
      }
    }
  %/
    scd.write<quint16>(/%= fields_count %/, false);/%
    for(const parc::definitions::Field* field : _klass->fields())
    {
      if(not field->options().testFlag(definitions::Field::Auto)
        and not field->options().testFlag(definitions::Field::Dependent))
      {
    %/
    scd.write<knowCore::Value>(/%= memberToBinding(field, "v.D()->" + field->name()) %/, true);/%
      }
    }%/
  }
  scd.close();
 
  q.setQuery("SELECT lastval();");
  knowDBC::Result sqlResult = q.execute();
  if(sqlResult)
  {
    cres_try(cres_ignore, transaction.commitIfOwned());
    /%
    for(const definitions::Field* field : klass->fields())
    {
      if(field->options().testFlag(definitions::Field::Auto))
      {%/
    int firstInsertId = sqlResult.value<int>(0, 0).expect_success() - _values.size() + 1;/%
        break;
      }
    }
    %/

    QList</%= _klass->name() %/Record> r;

    for(const /%= _klass->name() %/Value& v : _values)
    {
      /%= klass->name() %/Record obj;
      obj.D()->connection = connection;/%
      for(const definitions::Field* field : klass->fields())
      {
        if(field->options().testFlag(definitions::Field::Auto))
        {
        %/
      obj.D()->/%= field->name() %/ = firstInsertId;
      ++firstInsertId;/%
        } else {
        %/
      obj.D()->/%= field->name() %/ = /%= valueToMember(field->sqlType(), "v." + field->name() + "()") %/;/%
        }
      }
      %/
      r.append(obj);
    }
    return cres_success(r);
  } else {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("getting lastInsertId", sqlResult);
  }    
}

//END Create record

//BEGIN lock

cres_qresult<void> /%= _klass->name() %/Record::lock(const kDB::Repository::Transaction& _transaction, LockMode _mode)
{
  clog_assert(_transaction.isActive());
  QString modestring;
  
  switch(_mode)
  {
    case LockMode::AccessShare:
      modestring = "ACCESS SHARE";
      break;
    case LockMode::RowShare:
      modestring = "ROW SHARE";
      break;
    case LockMode::RowExclusive:
      modestring = "ROW EXCLUSIVE";
      break;
    case LockMode::ShareUpdateExclusive:
      modestring = "SHARE UPDATE EXCLUSIVE";
      break;
    case LockMode::Share:
      modestring = "SHARE";
      break;
    case LockMode::ShareRowExclusive:
      modestring = "SHARE ROW EXCLUSIVE";
      break;
    case LockMode::Exclusive:
      modestring = "EXCLUSIVE";
      break;
    case LockMode::AccessExclusive:
      modestring = "ACCESS EXCLUSIVE";
      break;
  }
  
  QString query = clog_qt::qformat("LOCK TABLE /%= queryGenerator()->tableName(_klass) %/ IN {} MODE", modestring);
  
  knowDBC::Query q = _transaction.createSQLQuery(query);
  KDB_REPOSITORY_EXECUTE_QUERY(clog_qt::qformat("Failed to lock table /%= queryGenerator()->tableName(_klass) %/ in mode {}", modestring), q, false);
  return cres_success();
}

//END lock

//BEGIN get connection
kDB::Repository::QueryConnectionInfo /%= _klass->name() %/Record::connection() const
{
  return D()->connection;
}
//END get connection

//BEGIN comparison operators

bool /%= _klass->name() %/Record::operator==(const /%= _klass->name() %/Record& _rhs) const
{
  return D()->connection == _rhs.D()->connection/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Auto))
    {
       %/ and D()->/%= field->name() %/ == _rhs.D()->/%= field->name()%//%
    }
  }
  %/ and sameValuesAs(_rhs);
}

bool /%= _klass->name() %/Record::sameValuesAs(const /%= _klass->name() %/Record& _rhs) const
{
  return this->/%= _klass->name() %/Value::operator==(_rhs);
}


bool /%= _klass->name() %/Record::operator!=(const /%= _klass->name() %/Record& _rhs) const
{
  return not(*this == _rhs);
}

//END comparison operators

//BEGIN refresh
cres_qresult<void> /%= _klass->name() %/Record::refresh()
{
  d->modified = false;
  knowDBC::Query query = D()->connection.createSQLQuery();
  query.setQuery(QString("/%= queryGenerator()->generateRefreshQuery(_klass) %/")/%= tableQueryArg(_klass, "D()->container") %/);
/%  setKeysOnQuery(stream, "query", _klass); %/
  knowDBC::Result result = query.execute();
  if(result)
  {
    if(result.tuples() > 0)
    {/%
{
    int k = 0;
    for(const definitions::Field* field : _klass->fields())
    {
      %/
      D()->/%= field->name() %/ = /%= queryResultToMember(field, clog_qt::qformat("result.value(0, {})", k)) %/;/%
      if(not field->options().testFlag(definitions::Field::Key)
        and not field->options().testFlag(definitions::Field::Constant))
      {
        %/
      D()->/%= field->name() %/_modified = false;/%
      }
      ++k;
    }
  } %/
      return cres_success();
    } else {
      return cres_failure("No such record in database.");
    }
  } else {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to refresh", result);
  }
}
//END refresh

//BEGIN record
cres_qresult<void> /%= _klass->name() %/Record::record()
{
  return record(D()->connection);
}

cres_qresult<void> /%= _klass->name() %/Record::record(const kDB::Repository::QueryConnectionInfo& _connection)
{
  if(not d->modified) return cres_failure("Nothing to record.");
  d->modified = false;
  knowDBC::Query query = _connection.createSQLQuery();
  QString fields;
/%
  QString KLASS_NAME = _klass->name().toUpper();
  for(const definitions::Field* field : _klass->fields())
  {
    if(not field->options().testFlag(definitions::Field::Constant) and not field->options().testFlag(definitions::Field::Key))
    {
      %/
  if(D()->/%= field->name() %/_modified)
  {
    if(not fields.isEmpty()) fields += ",";
    fields += "/%= queryGenerator()->fieldName(field) %/=:/%= field->name() %/";
  }/%
    }
  }
%/
  query.setQuery(QString("/%= queryGenerator()->generateRecordQuery(_klass) %/")/%= tableQueryArg(_klass, "D()->container") %/.arg(fields));
/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      %/
  query.bindValue(":/%= field->name() %/", D()->/%= field->name() %/);/%
    } else if(not field->options().testFlag(definitions::Field::Constant))
    {
      %/
  if(D()->/%= field->name() %/_modified)
  {
    query.bindValue(":/%= field->name() %/", knowCore::Value::fromValue(D()->/%= field->name() %/));
    D()->/%= field->name() %/_modified = false;
  }/%
    }
  }
%/
  KDB_REPOSITORY_EXECUTE_QUERY("recording", query, );
  return cres_success();
}
//END record

//BEGIN erase
cres_qresult<void> /%= _klass->name() %/Record::erase()
{
  return erase(D()->connection);
}

cres_qresult<void> /%= _klass->name() %/Record::erase(const kDB::Repository::QueryConnectionInfo& _connection)
{
  knowDBC::Query query = _connection.createSQLQuery();

  query.setQuery(QString("/%= queryGenerator()->generateDeleteQuery(_klass) %/")/%= tableQueryArg(_klass, "D()->container") %/);

  // Set the keys
/%
  for(const definitions::Field* field : _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      %/
  query.bindValue(":/%= field->name() %/", D()->/%= field->name() %/);/%
    }
  }
  %/
  KDB_REPOSITORY_EXECUTE_QUERY("erase", query, );/%

  for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
  {
    const definitions::Field* keyfield = klass->keys().first();
    
    %/
    // Need to drop the table
  query.setQuery(QString("/%= queryGenerator()->dropTable(sklass) %/").arg(D()->/%= keyfield->name() %/ ));
  KDB_REPOSITORY_EXECUTE_QUERY("drop sub table", query, );/%
  }
%/
  return cres_success();

}
//END erase

//BEGIN persistent
bool /%= _klass->name() %/Record::isPersistent() const
{
  if(D()->connection.isValid())
  {
    knowDBC::Query query = D()->connection.createSQLQuery();
    query.setQuery(QString("/%= queryGenerator()->generateExistQuery(_klass) %/")/%= tableQueryArg(_klass, "D()->container") %/);
/% setKeysOnQuery(stream, "query", _klass); %/
    knowDBC::Result result = query.execute();
    if(result and result.tuples() > 0) return true;
  }
  return false;
}
//END persistent

//BEGIN Children access

/%
// Add function to access children
if(not _klass->children().empty())
{
  for(const definitions::Class* child : _klass->children())
  {
    if(hasSingleChild(child, _klass))
    {
      const definitions::Field* field = child->parentFields(_klass).first();
      %/
      /%= child->name() %/ /%= _klass->name() %/::/%= childfunctionname(child->name()) %/() const
      {
        QList</%= child->name() %/> child = /%= child->name() %/::by/%= capitalizeFirstLetter(field->name()) %/(d->connection, *this).exec();
        clog_assert(child.size() <= 1);
        if(child.isEmpty()) return /%= child->name() %/();
        return child.first();
      }
      /%
    } else {
      %/
      /%= child->name() %/SelectQuery /%= _klass->name() %/::/%= childrenfunctionname(child->name()) %/() const
      {
        return /%
      QList<const definitions::Field*> parentfields = child->parentFields(_klass);
      bool isfirst = true;
      for(const definitions::Field* parentfield : child->parentFields(_klass))
      {
        if(isfirst)
        {
          isfirst = false;
        } else {
            %/ || /%
        } %/
        /%= child->name() %/::by/%= capitalizeFirstLetter(parentfield->name()) %/(d->connection, *this)/%
      }
      %/;
      }/%
    }
  }
}

%/

//END Children access

//BEGIN field getter, setter and query
/%
for(const definitions::Field* field : _klass->fields())
{
  if(isRecordField(_klass, field))
  {
    %/
/%= cppReturnType(field) %/ /%= _klass->name() %/Record::/%= field->name() %/() const
{
  return /%= memberToValue(field, QString("D()->{}")) %/;
}/%
  }
  %/
/%= _klass->name() %/Record::SelectQuery /%= _klass->name() %/Record::by/%= capitalizeFirstLetter(field->name()) %/(/%= databaseOrContainer(_klass) %/, /%= cppArgType(field) %/ _value, Sql::Operand _op)
{
  return SelectQuery(/%= databaseOrContainerParameterName(_klass) %/, "/%= queryGenerator()->fieldName(field) %/", /%= argumentToBinding(field, "_value") %/, _op);
}
  /%
}
%/

// Add the by function function
/%= _klass->name() %/Record::SelectQuery /%= _klass->name() %/Record::by(/%= databaseOrContainer(_klass) %/, const QString& _expression, const QVariantList& _values)
{
  return SelectQuery(/%= databaseOrContainerParameterName(_klass) %/, _expression, _values);
}

//END field getter, setter and query

//BEGIN All Query

/%= _klass->name() %/Record::SelectQuery /%= _klass->name() %/Record::all(/%= databaseOrContainer(_klass) %/)
{
  return SelectQuery(/%= databaseOrContainerParameterName(_klass) %/);
}

//END All Query

//BEGIN Mapping

/%
foreach(const definitions::Mapping* mapping, _klass->mappings())
{
  const definitions::Class* klass = mapping->other(_klass);
  %/
//     header_stream %/    QList</%= klass->name() %/> /%= functiongetallname(klass->name()) %/() const;
//                      "    void add/%= klass->name() %/(const /%= klass->name() %/& _other);
//                      "    void remove/%= klass->name() %/(const /%= klass->name() %/& _other);
  QList</%= klass->name() %/> /%= _klass->name() %/::/%= functiongetallname(klass->name()) %/() const
  {
    knowDBC::Query query = d->connection.createSQLQuery();
    query.setQuery("/%= queryGenerator()->generateGetMappingsQuery(mapping, _klass) %/");
    query.bindValue(":id_self", /%= _klass->keys().first()->name() %/());
    knowDBC::Result sqlResult = query.execute();
    QList</%= klass->name() %/> result;
    if(not sqlResult)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to get mapping", sqlResult);
      return result;
    }
    for(int i = 0; i < sqlResult.tuples(); ++i)
    {
      result.append(/%= klass->name() %/(d->connection, sqlResult.value<int>(i, 0).expect_success()));
    }
    return result;
  }
  cres_qresult<void> /%= _klass->name() %/::remove/%= klass->name() %/(const /%= klass->name() %/& _other)
  {
    knowDBC::Query query = d->connection.createSQLQuery();
    query.setQuery("/%= queryGenerator()->generateRemoveMappingQuery(mapping, _klass) %/");
    query.bindValue(":id_self", /%= _klass->keys().first()->name() %/());
    query.bindValue(":id_other", _other./%= mapping->other(_klass)->keys().first()->name() %/());
    KDB_REPOSITORY_EXECUTE_QUERY("removing", query);
    return cres_success();
  }
  cres_qresult<void> /%= _klass->name() %/::add/%= klass->name() %/(const /%= klass->name() %/& _other)
  {
    knowDBC::Query query = d->connection.createSQLQuery();
    query.setQuery("/%= queryGenerator()->generateAddMappingQuery(mapping, _klass) %/");
    query.bindValue(":id_self", /%= _klass->keys().first()->name() %/());
    query.bindValue(":id_other", _other./%= mapping->other(_klass)->keys().first()->name() %/());
    KDB_REPOSITORY_EXECUTE_QUERY("removing", query);
    return cres_success();
  }
  /%
}
%/
//END Mapping

//END Record class

