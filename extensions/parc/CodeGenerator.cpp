#include "CodeGenerator.h"

#include <clog_qt>

#include <QFile>
#include <QString>
#include <QTextStream>

#include <parc/Generators.h>
#include <parc/definitions/Class.h>
#include <parc/definitions/Database.h>
#include <parc/definitions/Field.h>
#include <parc/definitions/Forward.h>
#include <parc/definitions/Index.h>
#include <parc/definitions/Mapping.h>
#include <parc/definitions/Typedef.h>

namespace definitions = parc::definitions;

struct CodeGeneratorRegister
{
  CodeGeneratorRegister() { parc::Generators::add<CodeGenerator>("kdb"); }
};

CodeGeneratorRegister cgr;

CodeGenerator::CodeGenerator(parc::AbstractQueryGenerator* _aqg)
    : parc::generators::QtBaseCodeGenerator(_aqg)
{
}

CodeGenerator::~CodeGenerator() {}

bool CodeGenerator::canGenerate(const parc::definitions::Database* _database, QString* _reason)
{
  for(const definitions::Class* klass : _database->classes())
  {
    if(klass->doesImplement(parc::definitions::NOTIFICATIONS))
    {
      if(_reason)
        *_reason = "Notifications are not supported in kdb generator";
      return false;
    }
    if(klass->doesImplement(parc::definitions::UPDATENOTIFICATION))
    {
      if(_reason)
        *_reason = "Update notifications are not supported in kdb generator";
      return false;
    }
    if(klass->isTree())
    {
      if(_reason)
        *_reason = "Unsupported tree in kdb generator";
      return false;
    }
  }
  return true;
}

void CodeGenerator::generateDatabase(const parc::definitions::Database* _database,
                                     const QString& _destination)
{
  // Generate sql header
  {
    QFile header_file(_destination + "/Sql.h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);

#include "Sql_h.h"
  }
}

void CodeGenerator::generateClass(const parc::definitions::Database* _database,
                                  const parc::definitions::Class* _klass,
                                  const QString& _destination)
{
  // Generate header
  {
    QFile header_file(_destination + "/" + _klass->name() + ".h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);
#include "Class_h.h"
  }

  {
    QFile body_file(_destination + "/" + _klass->name() + ".cpp");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);

#include "Class_cpp.h"
  }
}

void CodeGenerator::generateJournal(const parc::definitions::Database* _database,
                                    const QString& _destination)
{
  Q_UNUSED(_database);
  Q_UNUSED(_destination);
  qFatal("No support for journal in kDB.");
}

void CodeGenerator::generateView(const parc::definitions::Database* _database,
                                 const parc::definitions::View* _view, const QString& _destination)
{
  Q_UNUSED(_database);
  Q_UNUSED(_view);
  Q_UNUSED(_destination);
  qFatal("No support for views in kDB.");
}

QString CodeGenerator::cppType(const parc::SqlType* _sqlType) const
{
  if(_sqlType->type() == parc::SqlType::REFERENCE)
  {
    return static_cast<const definitions::Class*>(_sqlType)->name() + "Record";
  }
  else
  {
    return QtBaseCodeGenerator::cppType(_sqlType);
  }
}

QString CodeGenerator::cppMemberType(const parc::SqlType* _sqlType) const
{
  if(_sqlType->type() == parc::SqlType::REFERENCE)
  {
    return "int";
  }
  else
  {
    return QtBaseCodeGenerator::cppMemberType(_sqlType);
  }
}

QString CodeGenerator::cppArgType(const parc::SqlType* _sqlType) const
{
  if(_sqlType->type() == parc::SqlType::REFERENCE)
  {
    return "const " + static_cast<const definitions::Class*>(_sqlType)->name() + "Record&";
  }
  else
  {
    return QtBaseCodeGenerator::cppArgType(_sqlType);
  }
}

QString CodeGenerator::cppReturnType(const parc::SqlType* _sqlType) const
{
  if(_sqlType->type() == parc::SqlType::REFERENCE)
  {
    return static_cast<const definitions::Class*>(_sqlType)->name() + "Record";
  }
  else
  {
    return QtBaseCodeGenerator::cppReturnType(_sqlType);
  }
}

QString CodeGenerator::defaultValue(const parc::definitions::Field* _field) const
{
  if(_field->defaultValue().isEmpty() and _field->sqlType()
     and _field->sqlType()->type() == parc::SqlType::REFERENCE)
  {
    return static_cast<const definitions::Class*>(_field->sqlType())->name() + "Record()";
  }
  else
  {
    return QtBaseCodeGenerator::defaultValue(_field);
  }
}

bool CodeGenerator::hasDefaultBaseValueConstructor(const parc::definitions::Class* _klass) const
{
  if(_klass->isTree())
  {
    return true;
  }
  for(const definitions::Field* field : _klass->fields())
  {
    if(isBaseValueField(_klass, field))
    {
      if(field->options().testFlag(definitions::Field::Required)
         or field->options().testFlag(definitions::Field::Key))
      {
        return true;
      }
    }
  }
  return false;
}

QString CodeGenerator::databaseOrContainer(const definitions::Class* _klass) const
{
  if(_klass->classOfContainerDefinition())
  {
    return "const " + _klass->classOfContainerDefinition()->name() + "Record& _container";
  }
  else
  {
    return "const kDB::Repository::QueryConnectionInfo& _connection";
  }
}

QString CodeGenerator::databaseOrContainerParameterName(const definitions::Class* _klass) const
{
  if(_klass->classOfContainerDefinition())
  {
    return "_container";
  }
  else
  {
    return "_connection";
  }
}

QString CodeGenerator::databaseOrContainerMemberName(const definitions::Class* _klass) const
{
  if(_klass->classOfContainerDefinition())
  {
    return "container";
  }
  else
  {
    return "connection";
  }
}

QString CodeGenerator::queryResultToMember(const parc::SqlType* _sqlType,
                                           const QString& _value) const
{
  using SqlType = parc::SqlType;
  switch(_sqlType->type())
  {
  case SqlType::BOOLEAN:
    return "Sql::fromVariant<bool>(" + _value + ")";
  case SqlType::INTEGER:
    return "Sql::fromVariant<int>(" + _value + ")";
  case SqlType::FLOAT32:
    return "Sql::fromVariant<float>(" + _value + ")";
  case SqlType::FLOAT64:
    return "Sql::fromVariant<double>(" + _value + ")";
  case SqlType::STRING:
    return "Sql::fromVariant<QString>(" + _value + ")";
  case SqlType::DATETIME:
    return "Sql::fromVariant<knowCore::Timestamp>(" + _value + ")";
  case SqlType::REFERENCE:
    return "Sql::fromVariant<int>(" + _value + ")";
  case SqlType::BLOB:
    return "Sql::fromVariant<QByteArray>(" + _value + ")";
  case SqlType::ENUM:
  case SqlType::FLAG:
  case SqlType::CUSTOM:
  case SqlType::SQLQUERY:
    return "Sql::fromVariant<" + _sqlType->cppType() + ">(" + _value + ")";
  }
  qFatal("variantToSql unimplemented");
}

QString CodeGenerator::queryResultToConstructor(const parc::SqlType* _sqlType,
                                                const QString& _value) const
{
  using SqlType = parc::SqlType;
  switch(_sqlType->type())
  {
  case SqlType::BOOLEAN:
    return "Sql::fromVariant<bool>(" + _value + ")";
  case SqlType::INTEGER:
    return "Sql::fromVariant<int>(" + _value + ")";
  case SqlType::FLOAT32:
    return "Sql::fromVariant<float>(" + _value + ")";
  case SqlType::FLOAT64:
    return "Sql::fromVariant<double>(" + _value + ")";
  case SqlType::STRING:
    return "Sql::fromVariant<QString>(" + _value + ")";
  case SqlType::DATETIME:
    return "Sql::fromVariant<knowCore::Timestamp>(" + _value + ")";
  case SqlType::BLOB:
    return "Sql::fromVariant<QByteArray>(" + _value + ")";
  case SqlType::REFERENCE:
  {
    const definitions::Class* klass = static_cast<const definitions::Class*>(_sqlType);
    return klass->name() + "(d->database, Sql::fromVariant<int>(" + _value + "))";
  }
  case SqlType::ENUM:
  case SqlType::FLAG:
  case SqlType::CUSTOM:
  case SqlType::SQLQUERY:
    return "Sql::fromVariant<" + _sqlType->cppType() + ">(" + _value + ")";
  }
  qFatal("variantToSql unimplemented");
}

void CodeGenerator::makeResultList(QTextStream& body_stream, const definitions::Class* _klass) const
{
  body_stream << "  QList< " << _klass->name()
              << "Record> results;\n"
                 "  knowDBC::Result sqlresult = query.execute();\n"
                 "  if(sqlresult)\n"
                 "  {\n"
                 "    for(int i = 0; i < sqlresult.tuples(); ++i)\n"
                 "    {\n"
                 "      results.push_back("
              << _klass->name() << "Record( d->" << databaseOrContainerMemberName(_klass);
  ;
  {
    int idx = 0;
    foreach(const definitions::Field* field, _klass->fields())
    {
      if(field->options().testFlag(definitions::Field::Key))
      {
        body_stream << ", "
                    << queryResultToConstructor(field,
                                                clog_qt::qformat("sqlresult.value(i, {})", idx));
        ++idx;
      }
    }
  }
  body_stream
    << " ) );\n"
       "    }\n"
       "  } else {\n"
       "    KDB_REPOSITORY_REPORT_QUERY_ERROR(\"failed to retrieve objects\", sqlresult);\n"
       "  }\n"
       "  return results;\n";
}

void CodeGenerator::setKeysOnQuery(QTextStream& _stream, const QString& _query,
                                   const definitions::Class* _klass) const
{
  foreach(const definitions::Field* field, _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      _stream << _query << ".bindValue(\":" << field->name() << "\", "
              << memberToBinding(field, "D()->" + field->name()) << ");\n";
    }
  }
}

QString CodeGenerator::memberToValue(const definitions::Field* _field,
                                     const QString& _member_prefix_template) const
{
  if(_field->sqlType()->type() == parc::SqlType::REFERENCE)
  {
    const definitions::Class* klass = static_cast<const definitions::Class*>(_field->sqlType());
    return clog_qt::qformat("{}Record({}, {})", klass->name(),
                            clog_qt::qformat(_member_prefix_template, "connection"),
                            clog_qt::qformat(_member_prefix_template, _field->name()));
  }
  else
  {
    return clog_qt::qformat(_member_prefix_template, _field->name());
  }
}

QString CodeGenerator::baseValueClassName(const parc::definitions::Class* _klass) const
{
  return clog_qt::qformat(hasExtendedValueClass(_klass) ? QStringLiteral("{}BaseValue")
                                                        : QStringLiteral("{}Value"),
                          _klass->name());
}

bool CodeGenerator::hasExtendedValueClass(const parc::definitions::Class* _klass) const
{
  for(const parc::definitions::Field* field : _klass->fields())
  {
    if(field->sqlType()->type() == parc::SqlType::REFERENCE)
    {
      return true;
    }
  }
  return false;
}

bool CodeGenerator::isBaseValueField(const parc::definitions::Class* _klass,
                                     const parc::definitions::Field* _field) const
{
  return not(
    _field->options().testFlag(definitions::Field::Auto)
    or (_field->options().testFlag(definitions::Field::Dependent) and hasExtendedValueClass(_klass))
    or _field->sqlType()->type() == parc::SqlType::REFERENCE);
}

bool CodeGenerator::isExtendedValueField(const parc::definitions::Class* _klass,
                                         const parc::definitions::Field* _field) const
{
  Q_UNUSED(_klass);
  return (_field->options().testFlag(definitions::Field::Dependent)
          or _field->sqlType()->type() == parc::SqlType::REFERENCE)
         and not _field->options().testFlag(definitions::Field::Auto);
}

bool CodeGenerator::isRecordField(const parc::definitions::Class* _klass,
                                  const parc::definitions::Field* _field) const
{
  Q_UNUSED(_klass);
  return _field->options().testFlag(definitions::Field::Auto);
}

QString CodeGenerator::constructionArguments(
  const parc::definitions::Class* _klass, bool _with_types, bool _default_values,
  const std::function<bool(const parc::definitions::Field*)>& _filter) const
{
  QString args;
  if(_klass->isTree())
  {
    if(_with_types)
    {
      args += "const " + _klass->name() + "& _parent";
    }
    else
    {
      args += "_parent";
    }
  }
  for(const definitions::Field* field : _klass->fields())
  {
    if(_filter(field))
    {
      if(not args.isEmpty())
        args += ", ";
      if(_with_types)
      {
        args += cppArgType(field) + " ";
      }
      args += "_" + field->name();
      if(_with_types and _default_values
         and not field->options().testFlag(definitions::Field::Required)
         and not field->options().testFlag(definitions::Field::Key))
      {
        args += " = " + defaultValue(field);
      }
    }
  }
  return args;
}

QString CodeGenerator::baseValueConstructionArguments(const parc::definitions::Class* _klass,
                                                      bool _with_types, bool _default_values) const
{
  return constructionArguments(_klass, _with_types, _default_values,
                               [this, _klass](const parc::definitions::Field* _field)
                               { return isBaseValueField(_klass, _field); });
}

QString CodeGenerator::extendedValueConstructionArguments(const parc::definitions::Class* _klass,
                                                          bool _with_types,
                                                          bool _default_values) const
{
  return constructionArguments(
    _klass, _with_types, _default_values, [this, _klass](const parc::definitions::Field* _field)
    { return isBaseValueField(_klass, _field) or isExtendedValueField(_klass, _field); });
}

QString CodeGenerator::onlyExtendedValueConstructionArguments(
  const parc::definitions::Class* _klass, bool _with_types, bool _default_values) const
{
  return constructionArguments(_klass, _with_types, _default_values,
                               [this, _klass](const parc::definitions::Field* _field)
                               { return isExtendedValueField(_klass, _field); });
}
