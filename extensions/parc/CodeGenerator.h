#include <functional>

#include <parc/generators/QtBaseCodeGenerator.h>

class QTextStream;

class CodeGenerator : public parc::generators::QtBaseCodeGenerator
{
public:
  CodeGenerator(parc::AbstractQueryGenerator* _aqg);
  virtual ~CodeGenerator();
  bool canGenerate(const parc::definitions::Database* _database, QString* _reason) override;
protected:
  void generateDatabase(const parc::definitions::Database* _database,
                        const QString& _destination) override;
  void generateClass(const parc::definitions::Database* _database,
                     const parc::definitions::Class* _klass, const QString& _destination) override;
  void generateJournal(const parc::definitions::Database* _database,
                       const QString& _destination) override;
  void generateView(const parc::definitions::Database* _database,
                    const parc::definitions::View* _view, const QString& _destination) override;
  QString cppType(const parc::SqlType* _sqlType) const override;
  QString cppMemberType(const parc::SqlType* _sqlType) const override;
  QString cppArgType(const parc::SqlType* _sqlType) const override;
  QString cppReturnType(const parc::SqlType* _sqlType) const override;
  using parc::generators::QtBaseCodeGenerator::cppArgType;
  using parc::generators::QtBaseCodeGenerator::cppMemberType;
  using parc::generators::QtBaseCodeGenerator::cppReturnType;
  using parc::generators::QtBaseCodeGenerator::cppType;
  QString defaultValue(const parc::definitions::Field* _field) const override;
private:
  /**
   * @return true if it should generate a default constructor for the base Value class.
   *         false if all the members of the value class have a default argument (ie none
   *         is a key or is required).
   */
  bool hasDefaultBaseValueConstructor(const parc::definitions::Class* _klass) const;
  QString databaseOrContainer(const parc::definitions::Class* _klass) const;
  QString databaseOrContainerParameterName(const parc::definitions::Class* _klass) const;
  QString databaseOrContainerMemberName(const parc::definitions::Class* _klass) const;
  virtual QString queryResultToMember(const parc::SqlType* _sqlType, const QString& _value) const;
  using parc::generators::QtBaseCodeGenerator::queryResultToMember;
  virtual QString queryResultToConstructor(const parc::SqlType* _sqlType,
                                           const QString& _value) const;
  using parc::generators::QtBaseCodeGenerator::queryResultToConstructor;
  void makeResultList(QTextStream& body_stream, const parc::definitions::Class* _klass) const;
  void setKeysOnQuery(QTextStream& _stream, const QString& _query,
                      const parc::definitions::Class* _klass) const;
  QString memberToValue(const parc::definitions::Field* _field,
                        const QString& _member_prefix_template) const;
  /**
   * @return the name of the base value class, depending on whether there is an extended value class
   */
  QString baseValueClassName(const parc::definitions::Class* _klass) const;
  /**
   * @return true if it has a field which is a reference, in which case the Value class needs to be
   * split into two parts, one without the reference (BaseValue) and one with the reference (Value)
   */
  bool hasExtendedValueClass(const parc::definitions::Class* _klass) const;

  bool isBaseValueField(const parc::definitions::Class* _klass,
                        const parc::definitions::Field* _field) const;
  bool isExtendedValueField(const parc::definitions::Class* _klass,
                            const parc::definitions::Field* _field) const;
  bool isRecordField(const parc::definitions::Class* _klass,
                     const parc::definitions::Field* _field) const;

  QString constructionArguments(
    const parc::definitions::Class* _klass, bool _with_types, bool _default_values,
    const std::function<bool(const parc::definitions::Field*)>& _filter) const;
  QString baseValueConstructionArguments(const parc::definitions::Class* _klass, bool _with_types,
                                         bool _default_values) const;
  QString extendedValueConstructionArguments(const parc::definitions::Class* _klass,
                                             bool _with_types, bool _default_values) const;
  QString onlyExtendedValueConstructionArguments(const parc::definitions::Class* _klass,
                                                 bool _with_types, bool _default_values) const;
};
