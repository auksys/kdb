#pragma once

#include <QObject>

#include <kDBDatasets/Forward.h>

namespace kDBDatasetsQuick
{
  class Dataset : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QString uri READ uri CONSTANT)
    Q_PROPERTY(QString type READ type CONSTANT)
    Q_PROPERTY(knowGIS::GeometryObject geometry READ geometry CONSTANT)
    Q_PROPERTY(QVariant data READ data CONSTANT)
  public:
    Dataset(const kDBDatasets::Dataset& _ds, QObject* _parent = nullptr);
    ~Dataset();
    QString uri() const;
    QString type() const;
    knowGIS::GeometryObject geometry() const;
    kDBDatasets::Dataset dataset() const;
    QVariant data() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBDatasetsQuick
