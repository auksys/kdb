#include <QAbstractTableModel>

namespace kDBDatasetsQuick
{
  class Datasets;
  class DatasetsTableModel : public QAbstractTableModel
  {
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> datasets READ datasets WRITE setDatasets NOTIFY datasetsChanged)
    Q_PROPERTY(QStringList columnNames READ columnNames CONSTANT)
  public:
    DatasetsTableModel(QObject* _parent = nullptr);
    ~DatasetsTableModel();
    QStringList columnNames() const;
    int rowCount(const QModelIndex& = QModelIndex()) const override;
    int columnCount(const QModelIndex& = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    void setDatasets(const QList<QObject*>& datasets);
    QList<QObject*> datasets() const;
  signals:
    void datasetsChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBDatasetsQuick
