#pragma once

#include <knowCore/Quick/Object.h>

#include <kDBQuick/Forward.h>

namespace kDBDatasetsQuick
{
  class Collection : public knowCore::Quick::Object
  {
    Q_OBJECT
    Q_PROPERTY(kDBQuick::Connection* connection READ connection WRITE setConnection NOTIFY
                 connectionChanged);
    Q_PROPERTY(QString uri READ uri WRITE setUri NOTIFY uriChanged)
    Q_PROPERTY(QStringList uris READ uris NOTIFY urisChanged)
    Q_PROPERTY(QList<QObject*> datasets READ datasets NOTIFY datasetsChanged)
  public:
    Collection(QObject* _parent = nullptr);
    ~Collection();
    QString uri() const;
    void setUri(const QString& _uri);
    kDBQuick::Connection* connection() const;
    void setConnection(kDBQuick::Connection* _connection);
    /**
     * @return all the uris of all the collections.
     */
    QStringList uris();
    QList<QObject*> datasets();
  signals:
    void uriChanged();
    void urisChanged();
    void datasetsChanged();
    void connectionChanged();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBDatasetsQuick
