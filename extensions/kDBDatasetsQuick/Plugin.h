#pragma once

#include <QQmlExtensionPlugin>

namespace kDBDatasetsQuick
{
  class Plugin : public QQmlExtensionPlugin
  {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "kDBDatasets/5.0")
  public:
    void registerTypes(const char* uri) override;
  };
} // namespace kDBDatasetsQuick
