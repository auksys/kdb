#include "Collection.h"

#include <knowCore/Cast.h>

#include <knowCore/Uris/askcore_dataset.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/Dataset.h>

#include <kDBQuick/Connection.h>

#include "Dataset.h"

using namespace kDBDatasetsQuick;

using askcore_dataset = knowCore::Uris::askcore_dataset;

struct Collection::Private
{
  QHash<QString, Dataset*> datasetsCache;
  QString uri;
  kDBDatasets::Collection dss;
  kDBQuick::Connection* connection = nullptr;
  void updateDss();
  QMetaObject::Connection updateDssConnection;
};

void Collection::Private::updateDss()
{
  if(connection and connection->nativeConnection().isValid()
     and connection->nativeConnection().isExtensionEnabled("kDBDatasets"))
  {
    dss = kDBDatasets::Collection::get(connection->nativeConnection(), uri).expect_success();
  }
}

Collection::Collection(QObject* _parent) : knowCore::Quick::Object(_parent), d(new Private) {}

Collection::~Collection()
{
  disconnect(d->updateDssConnection);
  delete d;
}

kDBQuick::Connection* Collection::connection() const { return d->connection; }

void Collection::setConnection(kDBQuick::Connection* _connection)
{
  if(d->connection)
  {
    disconnect(d->connection, SIGNAL(connectionStatusChanged()), this, SLOT(urisChanged()));
    disconnect(d->connection, SIGNAL(connectionStatusChanged()), this, SLOT(datasetsChanged()));
    disconnect(d->updateDssConnection);
  }
  d->connection = _connection;
  d->datasetsCache.clear();
  d->updateDss();
  emit(connectionChanged());
  emit(urisChanged());
  emit(datasetsChanged());

  if(d->connection)
  {
    connect(d->connection, SIGNAL(connectionStatusChanged()), SIGNAL(urisChanged()));
    connect(d->connection, SIGNAL(connectionStatusChanged()), SIGNAL(datasetsChanged()));
    d->updateDssConnection = connect(d->connection, &kDBQuick::Connection::connectionStatusChanged,
                                     std::bind(&Private::updateDss, d));
  }
}

QString Collection::uri() const { return d->uri; }

void Collection::setUri(const QString& _uri)
{
  if(_uri != d->uri)
  {
    d->uri = _uri;
    d->updateDss();
    emit(uriChanged());
    emit(datasetsChanged());
  }
}

QStringList Collection::uris()
{
  if(d->connection and d->connection->nativeConnection().isValid())
  {
    cres_qresult<QList<knowCore::Uri>> l
      = kDB::Repository::RDF::FocusNodeCollection::allCollectionUrisWith(
        d->connection->nativeConnection(), askcore_dataset::dataset);
    if(updateLastError(l.discard_value()))
    {
      return knowCore::listCast<QString>(l.get_value());
    }
    else
    {
      clog_error(
        "Failure to get all datasets: {} likely that kDBDatasets extension was not loaded.",
        l.get_error());
      return QStringList();
    }
  }
  else
  {
    return QStringList();
  }
}

QList<QObject*> Collection::datasets()
{
  QList<QObject*> answer;
  if(d->dss.isValid())
  {
    cres_qresult<QList<kDBDatasets::Dataset>> dss_all = d->dss.all();
    if(updateLastError(dss_all.discard_value()))
    {
      for(const kDBDatasets::Dataset& ds : dss_all.get_value())
      {
        Dataset* qml_ds = d->datasetsCache.value(ds.uri());
        if(not qml_ds)
        {
          qml_ds = new Dataset(ds, const_cast<Collection*>(this));
          d->datasetsCache[ds.uri()] = qml_ds;
        }
        answer.append(qml_ds);
      }
    }
    else
    {
      clog_error("Failed to query for list of datasets: {}", dss_all.get_error());
    }
  }
  else
  {
    clog_error("Collection {} is invalid.", d->uri);
  }
  return answer;
}

#include "moc_Collection.cpp"
