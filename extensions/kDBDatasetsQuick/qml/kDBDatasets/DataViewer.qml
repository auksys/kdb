import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Scene3D
import QtQuick3D
import Qt3D.Core
import Qt3D.Render
import Qt3D.Input
import Qt3D.Extras
import Qt3D.Logic
import QtQuick as QQ2
import knowVis

import knowCore
import knowValues as KV

Item
{
  id: root
  property var dataset
  property var __data: dataset ? dataset.data : []
  
  onDatasetChanged: console.log(root.dataset, root.__data, root.get_children_of_type(root.__data, 'http://askco.re/sensing#point_cloud'))
  
  function get_children_of_type(rr, type)
  {
    console.log(rr)
    return rr ? rr.filter(result => KnowCore.getDatatype(result) == type) : []
  }
  Context
  {
    id: _context_
    origin.longitude: 16.68290742925276
    origin.latitude: 57.75995505114895
    origin.altitude: 69.69978369831762
  }
  Scene3D
  {
    id: scene3d
    anchors.fill: parent
    focus: true
    aspects: ["input", "logic"]
    cameraAspectRatioMode: Scene3D.AutomaticAspectRatio
    Entity {
      id: sceneRoot

      Camera {
        id: camera
        fieldOfView: 45
        nearPlane : 0.1
        farPlane : 1000.0
        projectionType: CameraLens.PerspectiveProjection
        position: Qt.vector3d( 0.0, 0.0, 5.0 )
        upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
        viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
      }

      MouseDevice
      {
        id: mouseDevice_
      }
      XYOrbitCameraController {
        camera: camera
        mouseDevice: mouseDevice_
      }

      components: [
        RenderSettings {
          activeFrameGraph: ForwardRenderer {
            camera: camera
            clearColor: "black"
          }
        },
        InputSettings { }
      ]

      Axes
      {
        id: origin
        scale: 5
      }
      NodeInstantiator
      {
        model: root.get_children_of_type(root.__data, 'http://askco.re/sensing#point_cloud')
        PointCloud
        {
          id: pc
          context: _context_
          pointCloud: KV.Utils.getPointCloud(modelData)
          material {
            color: "white"
            secondaryColor: "#333333"
            colorMode: PointCloudMaterial.ZGradient
            gradientMin: pc.geometry.minExtent.z
            gradientMax: pc.geometry.maxExtent.z
            pointSize: 4
          }
        }
      }
    }
    Layout.fillWidth: true
    Layout.fillHeight: true
  }
}
