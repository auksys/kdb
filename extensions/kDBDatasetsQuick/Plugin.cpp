#include "Plugin.h"

#include <QtQml>

#include "Collection.h"
#include "Dataset.h"
#include "DatasetsTableModel.h"

using namespace kDBDatasetsQuick;

void Plugin::registerTypes(const char* /*uri*/)
{
  const char* uri_kDBDatasets = "kDBDatasets";
  qmlRegisterUncreatableType<Dataset>(uri_kDBDatasets, 1, 0, "Dataset",
                                      "Access them through Datasets");
  qmlRegisterType<Collection>(uri_kDBDatasets, 1, 0, "Datasets");
  qmlRegisterType<Collection>(uri_kDBDatasets, 1, 0, "Collection");
  qmlRegisterType<DatasetsTableModel>(uri_kDBDatasets, 1, 0, "DatasetsTableModel");
}
