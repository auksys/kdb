#include "DatasetsTableModel.h"

#include <clog_qt>
#include <cres_qt>
#include <knowCore/Cast.h>
#include <knowCore/Uri.h>

#include <knowGIS/GeometryObject.h>

#include <kDB/Repository/Connection.h>

#include <kDBDatasets/Dataset.h>

#include "Dataset.h"

using namespace kDBDatasetsQuick;

struct DatasetsTableModel::Private
{
  QList<Dataset*> datasets;
};

DatasetsTableModel::DatasetsTableModel(QObject* _parent)
    : QAbstractTableModel(_parent), d(new Private)
{
}

DatasetsTableModel::~DatasetsTableModel() { delete d; }

QStringList DatasetsTableModel::columnNames() const
{
  return {"uri", "type", "status", "area", "local", "agents"};
}

int DatasetsTableModel::rowCount(const QModelIndex&) const { return d->datasets.size() + 1; }
int DatasetsTableModel::columnCount(const QModelIndex&) const { return columnNames().size(); }

QVariant DatasetsTableModel::data(const QModelIndex& index, int role) const
{
  if(role == Qt::DisplayRole)
  {
    if(index.row() == 0)
    {
      return columnNames()[index.column()];
    }
    else
    {
      kDBDatasets::Dataset ds = d->datasets[index.row() - 1]->dataset();
      switch(index.column())
      {
      case 0:
        return QString(ds.uri());
      case 1:
        return QString(ds.type());
      case 2:
        return clog_qt::to_qstring(ds.status());
      case 3:
        return clog_qt::to_qstring(ds.geometry());
      case 4:
      {
        auto const& [success, agents, error] = ds.associatedAgents();
        if(success)
        {
          return agents.value().contains(ds.connection().serverUri()) ? "yes" : "no";
        }
        else
        {
          return error.value().get_message();
        }
      }
      case 5:
      {
        auto const& [success, agents, error] = ds.associatedAgents();
        if(success)
        {
          return knowCore::listCast<QString>(agents.value()).join(", ");
        }
        else
        {
          return error.value().get_message();
        }
      }
      }
      return QVariant();
    }
  }
  return QVariant();
}

QHash<int, QByteArray> DatasetsTableModel::roleNames() const
{
  return {{Qt::DisplayRole, "display"}};
}

void DatasetsTableModel::setDatasets(const QList<QObject*>& datasets)
{
  beginResetModel();
  d->datasets.clear();
  for(QObject* obj : datasets)
  {
    Dataset* ds = qobject_cast<Dataset*>(obj);
    if(ds)
    {
      d->datasets.append(ds);
    }
    else
    {
      qDebug() << "Invalid dataset, got" << obj;
    }
  }
  endResetModel();
  emit(datasetsChanged());
}

QList<QObject*> DatasetsTableModel::datasets() const
{
  return knowCore::listCast<QObject*>(d->datasets);
}
