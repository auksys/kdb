#include "Dataset.h"

#include <QtQml>

#include <kDB/Repository/Connection.h>

#include <knowGIS/GeometryObject.h>

#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>

using namespace kDBDatasetsQuick;

struct Dataset::Private
{
  kDBDatasets::Dataset ds;
};

Dataset::Dataset(const kDBDatasets::Dataset& _ds, QObject* _parent)
    : QObject(_parent), d(new Private)
{
  d->ds = _ds;
}

Dataset::~Dataset() {}

kDBDatasets::Dataset Dataset::dataset() const { return d->ds; }

QString Dataset::uri() const { return d->ds.uri(); }

QString Dataset::type() const { return d->ds.type(); }

knowGIS::GeometryObject Dataset::geometry() const { return d->ds.geometry().expect_success(); }

QVariant Dataset::data() const
{
  QVariantList list;
  if(d->ds.exists().ok_or(false))
  {
    auto [success, it_, errorMessage]
      = kDBDatasets::DataInterfaceRegistry::createValueIterator(d->ds.connection(), d->ds);
    kDBDatasets::ValueIterator it = it_.value();
    if(success)
    {
      while(it.hasNext())
      {
        auto [success, val, errorMessage] = it.next();
        if(success)
        {
          list.append(QVariant::fromValue(val.value()));
        }
        else
        {
          qmlEngine(this)->throwError(clog_qt::qformat("Failed to get data: {}", errorMessage.value()));
        }
      }
    }
    else
    {
      qmlEngine(this)->throwError(clog_qt::qformat("Failed to get data: {}", errorMessage.value()));
    }
  }

  return list;
}

#include "moc_Dataset.cpp"
