#include "Manager.h"

#include <knowDataTransfert/Interfaces/ReceiveIterator.h>
#include <knowDataTransfert/Interfaces/SendIterator.h>
#include <knowDataTransfert/Manager.h>

#include <kDB/Repository/Connection.h>

#include <kDBDatasets/DataInterfaceRegistry.h>
#include <kDBDatasets/Dataset.h>

using namespace kDBDataSynchronisation;

struct Manager::Private
{
  knowCore::Uri name;
  knowDataTransfert::Manager* kdt_manager = nullptr;
};

Manager::Manager(const knowCore::Uri& _name,
                 knowDataTransfert::AbstractCommunicationInterfaceFactory* _communicationInterface)
    : d(new Private)
{
  d->name = _name;
  d->kdt_manager = new knowDataTransfert::Manager(_name, _communicationInterface);
}

Manager::~Manager() { delete d; }

namespace
{
  class SendIterator : public knowDataTransfert::Interfaces::SendIterator
  {
  public:
    SendIterator(const kDBDatasets::ExtractIterator& _iterator) : m_iterator(_iterator) {}
    ~SendIterator() {}
    bool hasNext() const override { return m_iterator.hasNext(); }
    cres_qresult<QCborValue> next() override { return m_iterator.next(); }
  private:
    kDBDatasets::ExtractIterator m_iterator;
  };
} // namespace

cres_qresult<void> Manager::setupSendJob(const kDBDatasets::Dataset& _dataset,
                                         const QString& _communicationNamespace,
                                         const QStringList& _receivers)
{
  cres_try(
    kDBDatasets::ExtractIterator iterator,
    kDBDatasets::DataInterfaceRegistry::createExtractIterator(_dataset.connection(), _dataset));

  return d->kdt_manager->setupSendJob(new SendIterator(iterator), _communicationNamespace,
                                      _receivers);
}

namespace
{
  class ReceiveIterator : public knowDataTransfert::Interfaces::ReceiveIterator
  {
  public:
    ReceiveIterator(const knowCore::Uri& _name, const kDBDatasets::Dataset& _dataset,
                    const kDBDatasets::InsertIterator& _iterator)
        : m_name(_name), m_dataset(_dataset), m_iterator(_iterator)
    {
    }
    ~ReceiveIterator() {}
    cres_qresult<void> finalise() override
    {
      cres_try(cres_ignore, m_iterator.finalise());
      return m_dataset.associate(m_dataset.connection().serverUri());
    }
    cres_qresult<void> next(const QCborValue& _data) override
    {
      return m_iterator.next(_data.toByteArray());
    }
  private:
    knowCore::Uri m_name;
    kDBDatasets::Dataset m_dataset;
    kDBDatasets::InsertIterator m_iterator;
  };
} // namespace

cres_qresult<void> Manager::setupReceiveJob(const kDBDatasets::Dataset& _dataset,
                                            const QString& _communicationNamespace,
                                            const QString& _sender)
{
  cres_try(
    kDBDatasets::InsertIterator iterator,
    kDBDatasets::DataInterfaceRegistry::createInsertIterator(_dataset.connection(), _dataset));

  return d->kdt_manager->setupReceiveJob(new ReceiveIterator(d->name, _dataset, iterator),
                                         _communicationNamespace, _sender);
}

void Manager::stopTransfert() { d->kdt_manager->stopTransfert(); }

void Manager::waitForFinished() { d->kdt_manager->waitForFinished(); }
