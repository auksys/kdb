#include "TestLidarSynchronisation.h"

#include <knowCore/Uri.h>

#include <knowDataTransfert/Test.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryStore.h>

#include <knowGIS/GeometryObject.h>

#include <kDBRobotics/AgentPosition.h>
#include <kDBRobotics/FrameTransformation.h>
#include <kDBRobotics/Initialise.h>

#include <kDBDatasets/Collection.h>
#include <kDBDatasets/Dataset.h>

#include <kDBSensing/LidarConfig.h>
#include <kDBSensing/LidarScan.h>

#include <kDBDataSynchronisation/Manager.h>

#include <kDB/Repository/Test.h>

#include <knowCore/Uris/askcore_unit.h>
#include <knowValues/Uris/askcore_sensing.h>

using askcore_sensing = knowValues::Uris::askcore_sensing;
using askcore_unit = knowCore::Uris::askcore_unit;

void TestLidarSynchronisation::testLidarSynchronisation()
{

  kDB::Repository::TemporaryStore store_src, store_dst;
  CRES_QVERIFY(store_src.start());
  CRES_QVERIFY(store_dst.start());
  kDB::Repository::Connection c_src = store_src.createConnection();
  CRES_QVERIFY(c_src.connect());
  kDB::Repository::Connection c_dst = store_dst.createConnection();
  CRES_QVERIFY(c_dst.connect());

  CRES_QVERIFY(c_src.enableExtension("kDBSensing"));
  CRES_QVERIFY(c_dst.enableExtension("kDBSensing"));

  kDBSensing::LidarConfigRecord lidarConfig
    = CRES_QVERIFY(kDBSensing::LidarConfigRecord::create(c_src, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0));
  QVERIFY(lidarConfig.isPersistent());

  QList<kDBSensing::LidarScanRecord> frames;
  frames.append(CRES_QVERIFY(
    kDBSensing::LidarScanRecord::create(c_src, "sensor_uri", "dataset_uri", "frame_name", 12_kCtns,
                                        lidarConfig, {1.0, 2.0, 3.0}, {4.0, 2.0, 1.0})));
  frames.append(CRES_QVERIFY(
    kDBSensing::LidarScanRecord::create(c_src, "sensor_uri", "dataset_uri", "frame_name", 13_kCtns,
                                        lidarConfig, {3.0, 2.0, 1.0}, {1.0, 2.0, 4.0})));
  QList<kDBRobotics::FrameTransformationRecord> transformations;
  transformations.append(CRES_QVERIFY(kDBRobotics::FrameTransformationRecord::create(
    c_src, "robot_frame", "frame_name", 13_kCtns, {0.0, 1.0, 2.0}, {3.0, 4.0, 5.0, 6.0})));
  QList<kDBRobotics::AgentPositionRecord> positions;
  positions.append(CRES_QVERIFY(kDBRobotics::AgentPositionRecord::create(
    c_src, "name", "robot_frame", 13_kCtns, 1, 2, 3, {4, 5, 6, 7})));

  // Register dataset
  kDBDatasets::Collection dss_src
    = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c_src, "graph_of_dss"_kCu));
  QCOMPARE(dss_src.isValid(), true);
  kDBDatasets::Dataset ds_src = CRES_QVERIFY(dss_src.createDataset(
    knowValues::Uris::askcore_sensing::lidar_scan,
    CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))")),
    {askcore_sensing::point_density,
     knowCore::QuantityNumber::create(100, askcore_unit::point_per_m2).expect_success()},
    "dataset_uri"_kCu));
  CRES_QCOMPARE(ds_src.exists(), true);

  kDBDatasets::Collection dss_dst
    = CRES_QVERIFY(kDBDatasets::Collection::getOrCreate(c_dst, "graph_of_dss"_kCu));
  kDBDatasets::Dataset ds_dst = CRES_QVERIFY(dss_dst.createDataset(
    knowValues::Uris::askcore_sensing::lidar_scan,
    CRES_QVERIFY(knowGIS::GeometryObject::fromWKT("POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))")),
    {askcore_sensing::point_density,
     knowCore::QuantityNumber::create(100, askcore_unit::point_per_m2).expect_success()},
    "dataset_uri"_kCu));
  CRES_QCOMPARE(ds_dst.exists(), true);

  // Transfert lidar dataset
  knowDataTransfert::Test::CommunicationCenter commCenter;
  kDBDataSynchronisation::Manager manager_src(
    "src"_kCu, new knowDataTransfert::Test::CommunicationInterfaceFactory(&commCenter));
  kDBDataSynchronisation::Manager manager_dst(
    "dst"_kCu, new knowDataTransfert::Test::CommunicationInterfaceFactory(&commCenter));

  manager_src.setupSendJob(ds_src, "/test", {"dst"});
  manager_dst.setupReceiveJob(ds_dst, "/test", "src");

  KNOWCORE_TEST_WAIT_FOR(kDBSensing::LidarScanRecord::byDatasetUri(c_dst, "dataset_uri").count()
                         == std::size_t(frames.size()));
  QList<kDBSensing::LidarScanRecord> frames_recv
    = kDBSensing::LidarScanRecord::byDatasetUri(c_dst, "dataset_uri")
        .orderByTimestamp(kDBSensing::Sql::Sort::ASC)
        .exec();
  QVERIFY(frames_recv.size() == frames.size());
  for(int i = 0; i < frames_recv.size(); ++i)
  {
    QVERIFY(frames_recv[i].sameValuesAs(frames[i]));
  }

  KNOWCORE_TEST_WAIT_FOR(kDBRobotics::FrameTransformationRecord::all(c_dst).count()
                         == std::size_t(transformations.size()));
  QList<kDBRobotics::FrameTransformationRecord> transformations_recv
    = kDBRobotics::FrameTransformationRecord::all(c_dst)
        .orderByTimestamp(kDBRobotics::Sql::Sort::ASC)
        .exec();
  QVERIFY(transformations_recv.size() == transformations.size());

  manager_src.waitForFinished();
  manager_dst.waitForFinished();

  for(int i = 0; i < transformations_recv.size(); ++i)
  {
    QCOMPARE(transformations_recv[i].parentFrame(), transformations[i].parentFrame());
    QCOMPARE(transformations_recv[i].childFrame(), transformations[i].childFrame());
    QCOMPARE(transformations_recv[i].timestamp(), transformations[i].timestamp());
    QCOMPARE(transformations_recv[i].translation(), transformations[i].translation());
    QCOMPARE(transformations_recv[i].rotation(), transformations[i].rotation());
  }

  KNOWCORE_TEST_WAIT_FOR(kDBRobotics::AgentPositionRecord::all(c_dst).count()
                         == std::size_t(transformations.size()));
  QList<kDBRobotics::AgentPositionRecord> positions_recv
    = kDBRobotics::AgentPositionRecord::all(c_dst)
        .orderByTimestamp(kDBRobotics::Sql::Sort::ASC)
        .exec();
  QVERIFY(positions_recv.size() == positions.size());
  for(int i = 0; i < positions.size(); ++i)
  {
    QCOMPARE(positions_recv[i].name(), positions[i].name());
    QCOMPARE(positions_recv[i].frame(), positions[i].frame());
    QCOMPARE(positions_recv[i].timestamp(), positions[i].timestamp());
    QCOMPARE(positions_recv[i].latitude(), positions[i].latitude());
    QCOMPARE(positions_recv[i].longitude(), positions[i].longitude());
    QCOMPARE(positions_recv[i].altitude(), positions[i].altitude());
    QCOMPARE(positions_recv[i].rotation(), positions[i].rotation());
  }
}

QTEST_MAIN(TestLidarSynchronisation)
