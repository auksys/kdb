#include <kDBSensing/Forward.h>
#include <knowDataTransfert/Forward.h>

namespace kDBDataSynchronisation
{
  class Manager
  {
  public:
    Manager(const knowCore::Uri& _name,
            knowDataTransfert::AbstractCommunicationInterfaceFactory* _communicationInterface);
    ~Manager();
    cres_qresult<void> setupSendJob(const kDBDatasets::Dataset& _dataset,
                                    const QString& _communicationNamespace,
                                    const QStringList& _receivers);
    cres_qresult<void> setupReceiveJob(const kDBDatasets::Dataset& _dataset,
                                       const QString& _communicationNamespace,
                                       const QString& _sender);
    void stopTransfert();
    void waitForFinished();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBDataSynchronisation
