#include "kdQLFileLoader.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QStringList>

#include <Cyqlops/Yaml/Node.h>

#include <knowCore/ValueList.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>

#include "Manager.h"

using namespace kDBDocuments;

kdQLFileLoader::kdQLFileLoader() {}

kdQLFileLoader::~kdQLFileLoader() {}

bool kdQLFileLoader::canLoad(const QString& _path, const QString&, const knowCore::ValueHash&)
{
  return _path.endsWith(".kdql");
}

namespace
{
  void list_includes(QStringList* _output, const Cyqlops::Yaml::Node& _node,
                     const QString& _include_directory)
  {
    using ynType = Cyqlops::Yaml::Node::Type;
    switch(_node.type())
    {
    case ynType::Null:
    case ynType::Undefined:
    case ynType::Integer:
    case ynType::Double:
    case ynType::Boolean:
      return;
    case ynType::Sequence:
      for(std::size_t i = 0; i < _node.childrenCount(); ++i)
      {
        list_includes(_output, _node[i], _include_directory);
      }
      return;
    case ynType::Map:
      for(const QString& k : _node.keys())
      {
        list_includes(_output, _node[k], _include_directory);
      }
      return;
    case ynType::String:
    {
      if(_node.tag() == "tag:askco.re/kdql,2023:include")
      {
        QString fn = _include_directory + "/" + _node.toString();
        if(QFileInfo(fn).isFile())
        {
          _output->append(fn);
        }
      }
      return;
    }
    }
  }
} // namespace

cres_qresult<QStringList> kdQLFileLoader::loadFile(const QString& _path,
                                                   const kDB::Repository::Connection& _connection,
                                                   const knowCore::ValueHash&)
{
  QFile file(_path);
  if(not file.open(QIODevice::ReadOnly))
  {
    return cres_failure("Failed to open {}", _path);
  }
  QByteArray query_text = file.readAll();
  Manager* m = _connection.extensionObject<Manager>();
  knowDBC::Query q = m->createDQLQuery(query_text);
  QString include_directory = QFileInfo(_path).absoluteDir().absolutePath();
  q.setOption("include_directories", knowCore::ValueList(QList<knowCore::Value>{
                                       knowCore::Value::fromValue(include_directory)}));
  knowDBC::Result r = q.execute();
  if(r)
  {
    QStringList l;
    list_includes(&l, Cyqlops::Yaml::Node::fromYaml(query_text),
                  include_directory); // If query was successfullm it is a valid Yaml document
    clog_debug("loadFile: depends on {}", l);
    return cres_success(l);
  }
  else
  {
    return cres_failure("Failed to execute query from {} with error {}.", _path, r.error());
  }
}

#include <kDBBaseKnowledge/Manager.h>

KDB_REGISTER_BASE_KNOWLEDGE_FILE_LOADER(kdQLFileLoader)
