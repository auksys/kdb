#include "Manager.h"

#include <knowDBC/Query.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>

#include "QueryExecutor.h"

#include <knowCore/Uris/askcore_db.h>

using namespace kDBDocuments;

struct Manager::Private
{
  knowCore::WeakReference<kDB::Repository::Connection> connection;
};

Manager::Manager(const knowCore::WeakReference<kDB::Repository::Connection>& _connection)
    : d(new Private)
{
  d->connection = _connection;
}

Manager::~Manager() {}

knowDBC::Query Manager::createDQLQuery(const QString& _query, const knowCore::ValueHash& _bindings,
                                       const knowCore::ValueHash& _options) const
{
  return createDQLQuery(d->connection, _query, _bindings, _options);
}

knowDBC::Query Manager::createDQLQuery(const kDB::Repository::QueryConnectionInfo& _connection,
                                       const QString& _query, const knowCore::ValueHash& _bindings,
                                       const knowCore::ValueHash& _options) const
{
  knowDBC::Query q(new QueryExecutor(_connection));
  q.setQuery(_query);
  q.bindValues(_bindings);
  q.setOptions(_options);
  return q;
}

bool Manager::canCreateQuery(const knowCore::Uri& _type, const knowCore::ValueHash&) const
{
  return _type == knowCore::Uris::askcore_db_query_language::kdQL;
}

cres_qresult<knowDBC::Query> Manager::createQuery(const knowCore::Uri& _type,
                                                  const knowCore::ValueHash&) const
{
  if(_type == knowCore::Uris::askcore_db_query_language::kdQL)
  {
    return cres_success(createDQLQuery());
  }
  else
  {
    return cres_failure("Cannot create a query of type {}", _type);
  }
}
