#include "TestDocumentsQuery.h"

#include <QTemporaryDir>

#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/Test.h>
#include <kDB/Repository/TripleStore.h>

#include "../Manager.h"

namespace kDBDocuments
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection);
}

void TestDocumentsQuery::testSimpleQueries()
{
  kDB::Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDB::Repository::Connection c1 = store.createConnection();
  CRES_QVERIFY(c1.connect());

  CRES_QVERIFY(c1.enableExtension("kDBDocuments"));
  kDBDocuments::Manager* m = c1.extensionObject<kDBDocuments::Manager>();

  knowDBC::Query q = m->createDQLQuery();

  q.setQuery(R"(
create:
  into: tests
  documents:
    - name: "a"
      age: 10
      location: "world"
      alive: false
    - name: "b"
      age: 20
      location: "universe"
    - name: "b"
      age: 30
      location: "world"
)");

  VERIFY_QUERY_EXECUTION(q);

  q.setQuery(R"(
retrieve:
  what: "*"
  from: tests
  matches:
    name: "a"
)");
  knowDBC::Result r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.fieldNames(), QStringList() << "*");
  QCOMPARE(r.tuples(), 1);

  knowCore::ValueHash r_0 = CRES_QVERIFY(r.value(0, 0).value<knowCore::ValueHash>());
  CRES_QCOMPARE(r_0.value("name").value<QString>(), "a");
  CRES_QCOMPARE(r_0.value("age").value<int>(), 10);
  CRES_QCOMPARE(r_0.value("location").value<QString>(), "world");
  CRES_QCOMPARE(r_0.value("alive").value<bool>(), false);

  q.setQuery(R"(
retrieve:
  what: "age"
  from: tests
  matches:
    name: "b"
)");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.fieldNames(), QStringList() << "age");
  QCOMPARE(r.tuples(), 2);
  CRES_QCOMPARE(r.value<int>(0, "age"), 20);
  CRES_QCOMPARE(r.value<int>(1, 0), 30);

  q.setQuery(R"(
retrieve:
  what: [name]
  from: tests
  matches:
    name: "a"
    location: "universe"
  )");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.fieldNames(), QStringList() << "name");
  QCOMPARE(r.tuples(), 0);

  q.setQuery(R"(
retrieve:
  what: [name, age]
  from: tests
  matches:
    name: "b"
    location: "world"
)");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.fieldNames(), QStringList() << "name" << "age");
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), "b");
  CRES_QCOMPARE(r.value<int>(0, 1), 30);
  CRES_QCOMPARE(r.value<QString>(0, "name"), "b");
  CRES_QCOMPARE(r.value<int>(0, "age"), 30);

  // set
  q.setQuery(R"(
update:
  from: tests
  matches:
    name: "b"
  values:
    age: 25
    alive: true
    dead: false
)");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);

  q.setQuery(R"(
retrieve:
  what: ["age", "alive", "dead"]
  from: tests
  matches:
    name: "b"
)");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.fieldNames(), QStringList() << "age" << "alive" << "dead");
  QCOMPARE(r.tuples(), 2);
  CRES_QCOMPARE(r.value<int>(0, "age"), 25);
  CRES_QCOMPARE(r.value<bool>(0, "alive"), true);
  CRES_QCOMPARE(r.value<bool>(0, "dead"), false);
  CRES_QCOMPARE(r.value<int>(1, 0), 25);
  CRES_QCOMPARE(r.value<bool>(1, 1), true);
  CRES_QCOMPARE(r.value<bool>(1, 2), false);

  q.setQuery(R"(
retrieve:
  what: ["age", "alive", "dead"]
  from: tests
  matches:
    name: !binding name
)");
  q.bindValue("name", "a");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.fieldNames(), QStringList() << "age" << "alive" << "dead");
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<int>(0, "age"), 10);
  CRES_QCOMPARE(r.value<bool>(0, "alive"), false);
  QVERIFY(r.value(0, "dead").isEmpty());

  q.bindValue("name", "b");
  r = q.execute();
  VERIFY_QUERY_RESULT(r);
  QCOMPARE(r.fieldNames(), QStringList() << "age" << "alive" << "dead");
  QCOMPARE(r.tuples(), 2);
  CRES_QCOMPARE(r.value<int>(0, "age"), 25);
  CRES_QCOMPARE(r.value<bool>(0, "alive"), true);
  CRES_QCOMPARE(r.value<bool>(0, "dead"), false);
  CRES_QCOMPARE(r.value<int>(1, 0), 25);
  CRES_QCOMPARE(r.value<bool>(1, 1), true);
  CRES_QCOMPARE(r.value<bool>(1, 2), false);
  c1.disconnect();
}

QTEST_MAIN(TestDocumentsQuery)
