#include "QueryExecutor.h"

#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>

#include <Cyqlops/Yaml/Node.h>

#include <knowCore/Uris/askcore_db.h>

#include <knowDBC/Result.h>

#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TemporaryTransaction.h>
#include <kDB/Repository/Transaction.h>

#include "Collection.h"
#include "Document.h"

using namespace kDBDocuments;

struct QueryExecutor::Private
{
  kDB::Repository::QueryConnectionInfo connectionInfo;

  struct Options
  {
    QStringList include_directories;
  };

  knowCore::ValueHash bindings;

  cres_qresult<QVariant> create_document(const Cyqlops::Yaml::Node& _node, const Options& _options);
  knowDBC::Result execute_one(const Cyqlops::Yaml::Node& _node, const QString& _query,
                              const Options& _options,
                              const kDB::Repository::QueryConnectionInfo& _connectionInfo);

  cres_qresult<QList<DocumentRecord>> execute_matches(const Cyqlops::Yaml::Node& _matches_node,
                                                      const CollectionRecord& _cr_source);

  QJsonValue update_rec(const QJsonValue& _value, const Cyqlops::Yaml::Node& _update);
};

cres_qresult<QList<DocumentRecord>>
  QueryExecutor::Private::execute_matches(const Cyqlops::Yaml::Node& _matches_node,
                                          const CollectionRecord& _cr_source)
{
  if(_matches_node.isUndefined() or (_matches_node.isMap() and _matches_node.childrenCount() == 0))
  {
    return cres_success(DocumentRecord::all(_cr_source).exec());
  }
  else if(_matches_node.isMap())
  {
    DocumentSelectQuery sq;
    for(const QString& key : _matches_node.keys())
    {
      Cyqlops::Yaml::Node cn = _matches_node[key];
      QVariant var = cn.toVariant();
      clog_debug_vn(key, var, cn.tag());
      if(cn.tag() == "tag:askco.re/kdql,2023:binding" or cn.tag() == "!binding")
      {
        QString bk = var.toString();
        if(bindings.contains(bk))
        {
          var = bindings.value(var.toString()).toVariant();
        }
        else
        {
          return cres_failure("Missing binding '{}'", bk);
        }
      }
      sq = sq
           and DocumentRecord::by(_cr_source, "(%1 #>> %2) = %3", DocumentFields::content,
                                  QStringList() << key, var);
    }
    return cres_success(sq.exec());
  }
  else
  {
    return cres_failure("Expected matches to be a map.");
  }
}

QJsonValue QueryExecutor::Private::update_rec(const QJsonValue& _value,
                                              const Cyqlops::Yaml::Node& _update)
{
  if(_update.isMap())
  {
    QJsonObject obj = _value.toObject();
    for(const QString& key : _update.keys())
    {
      obj[key] = update_rec(obj[key], _update[key]);
    }
    return obj;
  }
  else
  {
    return QJsonValue::fromVariant(_update.toVariant());
  }
}

cres_qresult<QVariant> QueryExecutor::Private::create_document(const Cyqlops::Yaml::Node& _node,
                                                               const Options& _options)
{
  using ynType = Cyqlops::Yaml::Node::Type;
  switch(_node.type())
  {
  case ynType::Null:
  case ynType::Undefined:
    return cres_success(QVariant());
  case ynType::Sequence:
  {
    QVariantList l;
    for(std::size_t i = 0; i < _node.childrenCount(); ++i)
    {
      cres_try(QVariant v, create_document(_node[i], _options));
      l.append(v);
    }
    return cres_success(l);
  }
  case ynType::Map:
  {
    QVariantMap m;
    for(const QString& k : _node.keys())
    {
      cres_try(QVariant v, create_document(_node[k], _options));
      m[k] = v;
    }
    return cres_success(m);
  }
  case ynType::String:
  {
    if(_node.tag() == "tag:askco.re/kdql,2023:include")
    {
      for(const QString& id : _options.include_directories)
      {
        QString fn = id + "/" + _node.toString();
        if(QFileInfo(fn).isFile())
        {
          QFile f(fn);
          if(f.open(QIODevice::ReadOnly))
          {
            return cres_success(QString::fromUtf8(f.readAll()));
          }
        }
      }
      return cres_failure("Failed to find file '{}' for inclusion, in '{}'.", _node.toString(),
                          _options.include_directories);
    }
    else
    {
      return cres_success(_node.toString());
    }
  }
  case ynType::Integer:
    return cres_success(_node.toInteger());
  case ynType::Double:
    return cres_success(_node.toDouble());
  case ynType::Boolean:
    return cres_success(_node.toBoolean());
  }
  return cres_failure("Unhandled YAML type.");
}

knowDBC::Result
  QueryExecutor::Private::execute_one(const Cyqlops::Yaml::Node& _node, const QString& _query,
                                      const Options& _options,
                                      const kDB::Repository::QueryConnectionInfo& _connectionInfo)
{
  clog_assert(_node.isMap() and _node.childrenCount() == 1);
  QString key = _node.keys().first();
  Cyqlops::Yaml::Node query_node = _node[key];
  /*************************
   *  Execute create queries
   */
  if(key == "create")
  {
    Cyqlops::Yaml::Node into_node = query_node["into"];
    if(into_node.isUndefined())
    {
      return knowDBC::Result::create(_query, "Missing 'into' node in create statement.");
    }
    QString destination = into_node.toString();

    CollectionRecord cr_destination;
    QList<CollectionRecord> cr_query
      = CollectionRecord::byName(_connectionInfo, destination).exec();
    if(cr_query.isEmpty())
    {
      auto const& [success, cr_destination_, errorMessage]
        = CollectionRecord::create(_connectionInfo, destination);
      if(success)
      {
        cr_destination = cr_destination_.value();
      }
      else
      {
        return knowDBC::Result::create(
          _query, clog_qt::qformat("Failed to create collection '{}' with error: {}.", destination,
                                   errorMessage.value()));
      }
    }
    else
    {
      cr_destination = cr_query.first();
    }
    Cyqlops::Yaml::Node values_node = query_node["documents"];
    for(std::size_t i = 0; i < values_node.childrenCount(); ++i)
    {
      cres_qresult<QVariant> document = create_document(values_node[i], _options);
      if(document.is_successful())
      {
        DocumentRecord::create(cr_destination, document.get_value().toJsonValue());
      }
      else
      {
        return knowDBC::Result::create(
          _query, clog_qt::qformat("Failed to generate document from YAML: {}", document.get_error()));
      }
    }
    return knowDBC::Result::create(_query, QStringList(), QList<knowCore::ValueList>());
  }
  /***********************
   *  Execute drop queries
   */
  else if(key == "drop")
  {
    for(std::size_t i = 0; i < query_node.childrenCount(); ++i)
    {
      for(CollectionRecord cr :
          CollectionRecord::byName(_connectionInfo, query_node[i].toString()).exec())
      {
        cres_qresult<void> r = cr.erase();
        if(not r.is_successful())
        {
          return knowDBC::Result::create(
            _query, clog_qt::qformat("Failed to drop collection '{}' with error: {}.",
                                     query_node[i].toString(), r.get_error()));
        }
      }
    }
    return knowDBC::Result::create(_query, QStringList(), QList<knowCore::ValueList>());
  }
  /***************************
   *  Execute retrieve queries
   */
  else if(key == "retrieve")
  {
    Cyqlops::Yaml::Node from_node = query_node["from"];
    if(from_node.isUndefined())
    {
      return knowDBC::Result::create(_query, "Missing 'from' node in retrieve statement.");
    }
    Cyqlops::Yaml::Node what_node = query_node["what"];
    if(what_node.isUndefined())
    {
      return knowDBC::Result::create(_query, "Missing 'from' what in retrieve statement.");
    }

    QString source = from_node.toString();

    CollectionRecord cr_source;
    QList<CollectionRecord> cr_query = CollectionRecord::byName(_connectionInfo, source).exec();
    if(cr_query.isEmpty())
    {
      return knowDBC::Result::create(_query, clog_qt::qformat("Unknown collection '{}'.", source));
    }
    else
    {
      cr_source = cr_query.first();
    }

    cres_qresult<QList<DocumentRecord>> documents_rv
      = execute_matches(query_node["matches"], cr_source);

    if(not documents_rv.is_successful())
    {
      return knowDBC::Result::create(_query, documents_rv.get_error().get_message());
    }

    QList<DocumentRecord> documents = documents_rv.get_value();

    QStringList columnNames;
    QList<knowCore::ValueList> results;
    if(what_node.isString() and what_node.toString() == "*")
    {
      columnNames << "*";
      for(const DocumentRecord& dr : documents)
      {
        QList<knowCore::Value> l;
        cres_qresult<knowCore::Value> value
          = knowCore::Value::fromVariant(dr.content().toVariant());
        if(value.is_successful())
        {
          l.append(value.get_value());
        }
        else
        {
          return knowDBC::Result::create(
            _query, clog_qt::qformat("Failed to create result with error: {}", value.get_error()));
        }
        results.append(l);
      }
    }
    else if(what_node.isString() or what_node.isSequence())
    {
      if(what_node.isString())
      {
        columnNames.append(what_node.toString());
      }
      else
      {
        for(std::size_t i = 0; i < what_node.childrenCount(); ++i)
        {
          columnNames.append(what_node[i].toString());
        }
      }
      for(const DocumentRecord& dr : documents)
      {
        cres_qresult<knowCore::Value> value
          = knowCore::Value::fromVariant(dr.content().toVariant());
        if(value.is_successful())
        {
          cres_qresult<knowCore::ValueHash> value_hash = value.get_value().value<knowCore::ValueHash>();
          if(value_hash.is_successful())
          {
            QList<knowCore::Value> row;
            for(const QString& column : columnNames)
            {
              row.append(value_hash.get_value().value(column));
            }
            results.append(row);
          }
          else
          {
            return knowDBC::Result::create(
              _query, clog_qt::qformat("Failed to create result with error: {}", value.get_error()));
          }
        }
        else
        {
          return knowDBC::Result::create(
            _query, clog_qt::qformat("Failed to create result with error: {}", value.get_error()));
        }
      }
    }
    else
    {
      return knowDBC::Result::create(_query, "Expect 'what' to be a sequence.");
    }

    return knowDBC::Result::create(_query, columnNames, results);
  }
  /***************************
   *  Execute set queries
   */
  else if(key == "update")
  {
    Cyqlops::Yaml::Node from_node = query_node["from"];
    if(from_node.isUndefined())
    {
      return knowDBC::Result::create(_query, "Missing 'from' node in retrieve statement.");
    }

    QString source = from_node.toString();

    CollectionRecord cr_source;
    QList<CollectionRecord> cr_query = CollectionRecord::byName(_connectionInfo, source).exec();
    if(cr_query.isEmpty())
    {
      return knowDBC::Result::create(_query, clog_qt::qformat("Unknown collection '{}'.", source));
    }
    else
    {
      cr_source = cr_query.first();
    }
    cres_qresult<QList<DocumentRecord>> documents_rv
      = execute_matches(query_node["matches"], cr_source);

    if(not documents_rv.is_successful())
    {
      return knowDBC::Result::create(_query, documents_rv.get_error().get_message());
    }
    for(DocumentRecord dr : documents_rv.get_value())
    {
      Cyqlops::Yaml::Node values_node = query_node["values"];
      dr.setContent(update_rec(dr.content(), values_node));
      dr.record();
    }
    return knowDBC::Result::create(_query, {}, {});
  }
  else
  {
    return knowDBC::Result::create(_query, clog_qt::qformat("Unsupported type of query: {}", key));
  }
}

QueryExecutor::QueryExecutor(const kDB::Repository::QueryConnectionInfo& _connectionInfo)
    : d(new Private{_connectionInfo, {}})
{
}

QueryExecutor::~QueryExecutor() {}

knowDBC::Result QueryExecutor::execute(const QString& _query, const knowCore::ValueHash& _options,
                                       const knowCore::ValueHash& _bindings)
{
  Cyqlops::Yaml::YamlParseError ype;
  Cyqlops::Yaml::Node node = Cyqlops::Yaml::Node::fromYaml(_query, &ype);

  d->bindings = _bindings;

  Private::Options options;
  if(_options.contains("include_directories"))
  {
    cres_qresult<knowCore::ValueList> vl
      = _options.value<knowCore::ValueList>("include_directories");
    if(vl.is_successful())
    {
      for(const knowCore::Value& val : vl.get_value())
      {
        cres_qresult<QString> id = val.value<QString>();
        if(id.is_successful())
        {
          options.include_directories.append(id.get_value());
        }
        else
        {
          return knowDBC::Result::create(
            _query,
            clog_qt::qformat("'include_directories' was not a list of string: {}", id.get_error()));
        }
      }
    }
    else
    {
      return knowDBC::Result::create(
        _query, clog_qt::qformat("'include_directories' was not a list of string: {}", vl.get_error()));
    }
  }

  if(node.isUndefined())
  {
    return knowDBC::Result::create(_query, clog_qt::qformat("Parse error: {}.", ype.errorMessage));
  }
  else if(node.isMap() and node.childrenCount() == 1)
  {
    return d->execute_one(node, _query, options, d->connectionInfo);
  }
  else if(node.isSequence())
  {
    kDB::Repository::TemporaryTransaction tt(d->connectionInfo);

    knowDBC::Result r;
    for(std::size_t i = 0; i < node.childrenCount(); ++i)
    {
      Cyqlops::Yaml::Node sub_node = node[i];
      if(sub_node.isMap() and sub_node.childrenCount() == 1)
      {
        r = d->execute_one(sub_node, _query, options, tt.transaction());
        if(not r)
        {
          tt.rollback();
          return r;
        }
      }
      else
      {
        tt.rollback();
        return knowDBC::Result::create(_query, "Query nodes should be a map with a single key.");
      }
    }
    cres_qresult<void> rv = tt.commitIfOwned();
    if(rv.is_successful())
    {
      return r;
    }
    else
    {
      return knowDBC::Result::create(
        _query, clog_qt::qformat("Failed to apply transaction: {}.", rv.get_error()));
    }
  }
  else
  {
    return knowDBC::Result::create(_query,
                                   "Root node should be a map with a single key or a sequence.");
  }
}

knowCore::Uri QueryExecutor::queryLanguage() const
{
  return knowCore::Uris::askcore_db_query_language::kdQL;
}
