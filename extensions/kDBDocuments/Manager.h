#include <kDB/Forward.h>

#include <QObject>
#include <knowCore/ValueHash.h>

#include <kDB/Repository/Interfaces/QueryFactory.h>

namespace kDBDocuments
{
  class Manager : public QObject, private kDB::Repository::Interfaces::QueryFactory
  {
    friend class kDB::Repository::Connection;
  public:
    Manager(const knowCore::WeakReference<kDB::Repository::Connection>& _connection);
    virtual ~Manager();
    /**
     * Create a DQL query.
     */
    knowDBC::Query createDQLQuery(const QString& _query = QString(),
                                  const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                  const knowCore::ValueHash& _options
                                  = knowCore::ValueHash()) const;
    knowDBC::Query createDQLQuery(const kDB::Repository::QueryConnectionInfo& _connection,
                                  const QString& _query = QString(),
                                  const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                  const knowCore::ValueHash& _options
                                  = knowCore::ValueHash()) const;
  private:
    // QueryFactory interface
    bool canCreateQuery(const knowCore::Uri& _type,
                        const knowCore::ValueHash& _environment) const override;
    cres_qresult<knowDBC::Query>
      createQuery(const knowCore::Uri& _type,
                  const knowCore::ValueHash& _environment) const override;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBDocuments
