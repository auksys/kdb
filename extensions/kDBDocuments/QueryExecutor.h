#include <kDB/Forward.h>

#include <knowDBC/Interfaces/QueryExecutor.h>

namespace kDBDocuments
{
  class QueryExecutor : public knowDBC::Interfaces::QueryExecutor
  {
  public:
    QueryExecutor(const kDB::Repository::QueryConnectionInfo& _connectionInfo);
    ~QueryExecutor();
    knowDBC::Result execute(const QString& _query, const knowCore::ValueHash& _options,
                            const knowCore::ValueHash& _bindings) override;
    knowCore::Uri queryLanguage() const override;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDBDocuments
