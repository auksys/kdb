#include "Manager.h"

#include <kDB/Repository/QueryConnectionInfo.h>

#include "Collection.h"
#include "Document.h"

namespace kDBDocuments
{
  cres_qresult<void> initialise(const kDB::Repository::Connection& _connection)
  {
    cres_try(cres_ignore, CollectionRecord::createTable(_connection));
    kDB::Repository::Connection(_connection).createExtensionObject<Manager>();
    return cres_success();
  }
} // namespace kDBDocuments

#include <kDB/Repository/Extensions.h>

KDB_REPOSITORY_EXTENSION_EXPORT(kDBDocuments)
