#!/usr/bin/env bash

FILES=*.xml
DSTDIR=$HOME/.local/share/katepart5/syntax
mkdir -p $DSTDIR
for f in $FILES; do
  ln -s `readlink -e $f` $DSTDIR/$f;
done
