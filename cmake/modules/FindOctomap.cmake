# Look for Octomap Library
#
# Set
#  OCTOMAP_FOUND = TRUE
#  OCTOMAP_INCLUDE_DIR = /usr/local/include
#  OCTOMAP_LIBRARIES = /usr/local/lib/libGeographic.so

find_path(OCTOMAP_INCLUDE_DIR octomap/octomap.h)

find_library (OCTOMAP_LIBRARY octomap)
find_library (OCTOMATH_LIBRARY octomath)

set(OCTOMAP_LIBRARIES
  ${OCTOMAP_LIBRARY}
  ${OCTOMATH_LIBRARY}
)

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (OCTOMAP DEFAULT_MSG OCTOMAP_LIBRARIES OCTOMAP_INCLUDE_DIR)
mark_as_advanced (OCTOMAP_INCLUDE_DIR OCTOMAP_LIBRARIES)
