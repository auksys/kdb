find_package(Ruby)

if(RUBY_FOUND)

# Look for rice prefix

execute_process(COMMAND ${RUBY_EXECUTABLE} -e "print(Gem::Specification.find_by_name('rice').gem_dir)" OUTPUT_VARIABLE RICE_PREFIX)

message(STATUS "ruby version: /${RUBY_VERSION}/")
message(STATUS "rice prefix: /${RICE_PREFIX}/")

find_path(RICE_INCLUDE_DIR rice/rice.hpp
          HINTS ${RICE_PREFIX}/include )

message(STATUS "rice include: /${RICE_INCLUDE_DIR}/")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Rice  DEFAULT_MSG  RICE_INCLUDE_DIR)

add_library(rice INTERFACE IMPORTED)
set_target_properties(rice PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${RICE_INCLUDE_DIR};${RUBY_INCLUDE_DIRS}"
  INTERFACE_LINK_LIBRARIES "${RUBY_LIBRARY}"
)

endif()
