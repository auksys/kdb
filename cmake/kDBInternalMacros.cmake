function(copy_headers_to_dir TARGET_NAME)

  cmake_parse_arguments(copy_headers_to_dir "" "DESTINATION" "HEADERS" ${ARGN})

  foreach(p ${copy_headers_to_dir_HEADERS})
    list(APPEND HEADERS_BINARY ${copy_headers_to_dir_DESTINATION}/${p})
  endforeach()

  add_custom_target(${TARGET_NAME} DEPENDS ${HEADERS_BINARY})

  foreach(p ${copy_headers_to_dir_HEADERS})
    get_filename_component(p_directory ${copy_headers_to_dir_DESTINATION}/${p} DIRECTORY)
    add_custom_command(OUTPUT ${p_directory}
                      COMMAND cmake -E make_directory ${p_directory})
    if(${UNIX})
      add_custom_command(OUTPUT ${copy_headers_to_dir_DESTINATION}/${p}
                        DEPENDS ${p_directory}
                        COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_CURRENT_SOURCE_DIR}/${p} ${copy_headers_to_dir_DESTINATION}/${p})
    else()
      add_custom_command(OUTPUT ${copy_headers_to_dir_DESTINATION}/${p}
                        DEPENDS ${p_directory}
                        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${p} ${copy_headers_to_dir_DESTINATION}/${p})
    endif()
  endforeach()
endfunction()
