#ifndef _KDB_REPOSITORY_CONNECTION_H_
#define _KDB_REPOSITORY_CONNECTION_H_

#include <QSharedPointer>
#include <QString>

#include <kDB/Forward.h>
#include <knowCore/ValueHash.h>
#include <knowCore/WeakReference.h>

#include "NamedTypes.h"
#include "RDFEnvironment.h"

class QUuid;

namespace Cyqlops::Crypto
{
  class RSAAlgorithm;
}

namespace kDB
{
  namespace Repository
  {
    class Connection
    {
      class knowDBCConnectionInterface;
      template<typename _T_>
      friend class knowCore::WeakReference;
      friend class ConnectionHandle;
      friend class GraphsManager;
      friend class QueryConnectionInfo;
      friend class SPARQLQuery;
      friend class SPARQLFunctionsManager;
      friend class Transaction;
      friend class TripleStore;
      friend class TriplesView;
      friend class DatabaseInterface::PostgreSQL::BinaryMarshalsRegistry;
      friend class DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal;
      friend class DatabaseInterface::PostgreSQL::RDFTermBinaryMarshal;
      friend class DatabaseInterface::PostgreSQL::SQLCopyData;
      friend class DatabaseInterface::PostgreSQL::SQLInterface;
      friend class DatabaseInterface::PostgreSQL::SQLQueryExecutor;
      friend class DatabaseInterface::PostgreSQL::SQLResult;
      friend class SPARQLExecution::QueryExecutorVisitor;
      friend class SPARQLExecution::SPARQLAlgebraToPostgresSQL;
      friend struct SPARQLExecution::SPARQLAlgebraToPostgresSQLVisitor;
      friend class knowDBCConnectionInterface;
      Connection(const QString& _host, int _port, const QString& _database = "kDB");
    public:
      static Connection create(const HostName& _host, int _port, const QString& _database = "kDB");
      static Connection create(const SectionName& _section, const StoreName& _store, int _port,
                               const QString& _database = "kDB");
      static Connection create(const StoreName& _store, int _port,
                               const QString& _database = "kDB");
      Connection();
      template<typename _T_>
        requires std::same_as<_T_, Connection>
      Connection(const knowCore::WeakReference<_T_>& _d);
      Connection(const Connection& _rhs);
      Connection& operator=(const Connection& _rhs);
      ~Connection();
      bool operator==(const Connection& _rhs) const { return d == _rhs.d; }
      bool operator!=(const Connection& _rhs) const { return d != _rhs.d; }
      /**
       * @return true if this is a valid connection (ie created with \ref create for instance
       * Connection().isValid() return false)
       */
      bool isValid() const;
      /**
       * Disconnect this connection from the server (this propagage to all the copy of this object!)
       */
      void disconnect();
      /**
       * Attempt to connect to the server
       * \return false if connection fails, or if it is already connected
       */
      cres_qresult<void> connect(bool _initialise_database = false);
      /**
       * \return true if connected to the server
       */
      bool isConnected() const;
      GraphsManager* graphsManager() const;
      NotificationsManager* notificationsManager();
      SPARQLFunctionsManager* sparqlFunctionsManager() const;
      cres_qresult<void> executeQueryFromFile(const QString& _fileName) const;
      cres_qresult<void> executeQuery(const QString& _query) const;
      cres_qresult<TripleStore> defaultTripleStore() const;
      cres_qresult<TripleStore> infoTripleStore() const;
      QUuid serverUuid() const;
      /**
       * @return a URI uniquely identifying this server (based on the UUID)
       */
      knowCore::Uri serverUri() const;
      /**
       * This function gives access to the RSA key used by the server for signing.
       */
      Cyqlops::Crypto::RSAAlgorithm rsaAlgorithm() const;
      /**
       * \return the postgresql Oid type associated with the \p _type
       */
      cres_qresult<quint64> oid(const QString& _type) const;
      /**
       * Enable an extension
       * \return true if extension is successfully enabled
       */
      cres_qresult<void> enableExtension(const QString& _extension);
      /**
       * Enable an extension
       * \return true if an extension is enabled
       */
      bool isExtensionEnabled(const QString& _extension) const;
      /**
       * \return the UUID to the connection
       */
      QUuid connectionUuid() const;
      /**
       * Called a function when disconnected (warning, cannot use connection as disconnection can
       * occurs during deletion, and should not hold a reference to connection to avoid leaks).
       */
      QUuid executeDisconnection(const std::function<void()>& _function) const;
      void removeExecuteDisconnection(const QUuid& _uuid) const;
      /**
       * Called a function after connection is completed.
       */
      QUuid executeConnection(const std::function<void(const Connection&)>& _function) const;
      void removeExecuteConnection(const QUuid& _uuid) const;
      operator knowDBC::Connection();
      /**
       * Create an SQL query for this connection
       */
      knowDBC::Query createSQLQuery(const QString& _query = QString(),
                                    const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                    const knowCore::ValueHash& _options
                                    = knowCore::ValueHash()) const;
      /**
       * Create an SPARQL query for this connection
       */
      knowDBC::Query createSPARQLQuery(const RDFEnvironment& _environment = RDFEnvironment(),
                                       const QString& _query = QString(),
                                       const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                       const knowCore::ValueHash& _options
                                       = knowCore::ValueHash()) const;
      /**
       * @return the extension object associated with this connection
       */
      template<typename _T_>
      _T_* extensionObject() const;
      /**
       * Register an extension object with this connection.
       * Connection take ownership of the object. The object is deleted upon disconnection and
       * should hold a knowCore::WeakReference<Connection> to avoid circular reference.
       *
       * This function is intended to be use by an initaĺisation function of an extension.
       */
      template<typename _T_>
      void createExtensionObject();
      /**
       * @return the query engine associated with this connection.
       */
      krQuery::Engine* krQueryEngine() const;
    private:
      void increaseSelfPCount();
      void decreaseSelfPCount();
      QObject* extensionObject(const std::type_index& _index) const;
      void createExtensionObject(QObject* _object, const std::type_index& _index);
      void addQueryFactory(const Interfaces::QueryFactory* _query_factory);
      struct Private;
      QSharedPointer<Private> d;
    };
    template<typename _T_>
      requires std::same_as<_T_, Connection>
    Connection::Connection(const knowCore::WeakReference<_T_>& _d) : d(_d.d)
    {
      increaseSelfPCount();
    }
    template<typename _T_>
    _T_* Connection::extensionObject() const
    {
      return static_cast<_T_*>(extensionObject(typeid(_T_)));
    }
    template<typename _T_>
    void Connection::createExtensionObject()
    {
      _T_* t = new _T_(*this);
      createExtensionObject(t, typeid(_T_));
      if constexpr(std::is_base_of_v<Interfaces::QueryFactory, _T_>)
      {
        addQueryFactory(t);
      }
    }

  } // namespace Repository
} // namespace kDB

#endif
