#pragma once

#include "RDFDataset.h"

#include <QSharedData>

#include "Connection.h"

namespace kDB
{
  namespace Repository
  {
    struct RDFDataset::Private : public QSharedData
    {
      Private(RDFDataset::Type _type, const Connection& _connection = Connection())
          : type(_type), connection(_connection)
      {
      }
      virtual ~Private();
      const RDFDataset::Type type;
      Connection connection;
      virtual bool isValid() const;
      virtual knowCore::Uri uri() const;
    };
  } // namespace Repository
} // namespace kDB
