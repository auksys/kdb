#include <QMutex>

#include <knowCore/Messages.h>

#include "DeltaParser/Parser_p.h"

namespace kDB::Repository::VersionControl
{
  struct QueryCache
  {
    static DeltaParser::Parser::Result get(const QByteArray& _query)
    {

      QMutexLocker l(&m);
      auto it = queries.find(_query);
      if(it != queries.end())
      {
        return it.value();
      }
      l.unlock();

      knowCore::Messages messages;
      DeltaParser::Parser::Result r = DeltaParser::Parser::parse(_query, &messages);
      if(messages.hasErrors())
      {
        clog_error("Error in default merge strategy while parsing query {}: {}",
                   QString::fromUtf8(_query), messages.toString());
        return DeltaParser::Parser::Result{false, {}, {}};
      }

      l.relock();
      queries[_query] = r;
      return r;
    }
    static void add(const QByteArray& _query, const QList<knowRDF::Triple>& inserted,
                    const QList<knowRDF::Triple>& deleted)
    {
      QMutexLocker l(&m);
      queries[_query] = {true, inserted, deleted};
    }
  private:
    static QHash<QByteArray, DeltaParser::Parser::Result> queries;
    static QMutex m;
  };

} // namespace kDB::Repository::VersionControl
