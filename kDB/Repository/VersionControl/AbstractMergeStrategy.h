#ifndef _KDB_REPOSITORY_VERSION_CONTROL_ABSTRACT_MERGE_STRATEGY_H_
#define _KDB_REPOSITORY_VERSION_CONTROL_ABSTRACT_MERGE_STRATEGY_H_

#include <QList>

class QUuid;

#include <kDB/Forward.h>

namespace Cyqlops::Crypto
{
  class RSAAlgorithm;
}

namespace kDB::Repository::VersionControl
{
  /**
   * @ingroup kDB_Repository_VersionControl
   *
   * Base class for merge strategies.
   */
  class AbstractMergeStrategy
  {
  protected:
    struct Private;
    AbstractMergeStrategy(Private* _d);
  public:
    ~AbstractMergeStrategy();
    void addRevisions(const QList<Revision>& _revisions);
    bool merge();
    /**
     * @return true if there are changes between the left and merged revisions.
     *
     * Value is undefined before call to \ref merge.
     */
    bool hasChanges(qsizetype _index) const;
    QByteArray diff(qsizetype _index) const;
    QByteArray parentHash(qsizetype _index) const;
    bool allHaveChanges() const;
  protected:
    Private* const d;
  };
} // namespace kDB::Repository::VersionControl

#endif
