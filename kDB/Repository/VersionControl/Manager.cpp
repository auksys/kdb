#include "Manager_p.h"

#include <QVariant>

#include <Cyqlops/Crypto/Random.h>

#include <clog_qt>
#include <knowCore/TypeDefinitions.h>

#include <knowDBC/Result.h>
#include <knowRDF/Triple.h>

#include "../Connection.h"
#include "../Logging.h"
#include "../NotificationsManager.h"
#include "../QueryConnectionInfo.h"
#include "../TemporaryTransaction.h"
#include "../Transaction.h"
#include "../TripleStore_p.h"
#include "../Utils.h"

#include "BuildAccumulatedDiff_p.h"
#include "DefaultMergeStrategy.h"
#include "DeltaBuilder.h"
#include "RevisionBuilder_p.h"
#include "Revision_p.h"
#include "Signature.h"
#include "Utils_p.h"

using namespace kDB::Repository::VersionControl;

Manager::Manager(const kDB::Repository::Connection& _connection,
                 QSharedPointer<TripleStore::Definition> _definition)
    : d(new Private)
{
  d->connection = _connection;
  d->definition = _definition;
}

Manager::~Manager()
{
  {
    QMutexLocker l(&d->mutex);
    for(const QMetaObject::Connection& c : d->connections)
    {
      QObject::disconnect(c);
    }
  }
  delete d;
}

void Manager::setSigningEnabled(bool _signing) { d->signingEnabled = _signing; }

bool Manager::signingEnabled() const { return d->signingEnabled; }

cres_qresult<QList<Revision>>
  Manager::revisions(const kDB::Repository::Transaction& _transaction) const
{
  return DatabaseInterface::PostgreSQL::SQLInterface::revisions({_transaction, d->connection},
                                                                d->definition->table);
}

cres_qresult<Revision> Manager::tip(const kDB::Repository::Transaction& _transaction) const
{
  cres_try(QByteArray tHash, tipHash(_transaction));
  return DatabaseInterface::PostgreSQL::SQLInterface::revision({_transaction, d->connection},
                                                               d->definition->table, tHash);
}

cres_qresult<QByteArray> Manager::tipHash(const kDB::Repository::Transaction& _transaction) const
{
  return Utils::MetaVersion::getHead({_transaction, d->connection}, d->definition);
}

cres_qresult<QList<Revision>> Manager::heads(const kDB::Repository::Transaction& _transaction) const
{
  return DatabaseInterface::PostgreSQL::SQLInterface::heads({_transaction, d->connection},
                                                            d->definition->table);
}

RevisionBuilder Manager::insertRevision(const QByteArray& _content_hash)
{
  RevisionBuilder rb;
  rb.d->connection = d->connection;
  rb.d->triplesStoreTableName = d->definition->table;
  rb.d->content_hash = _content_hash;
  // rb.d->signature = _signature;
  // rb.d->author = _author;
  // rb.d->timestamp = _timestamp;
  return rb;
}

cres_qresult<bool> Manager::hasRevision(const QByteArray& _hash,
                                        const kDB::Repository::Transaction& _transaction) const
{
  return DatabaseInterface::PostgreSQL::SQLInterface::hasRevision({_transaction, d->connection},
                                                                  d->definition->table, _hash);
}

cres_qresult<Revision> Manager::revision(const QByteArray& _hash,
                                         const kDB::Repository::Transaction& _transaction) const
{
  return DatabaseInterface::PostgreSQL::SQLInterface::revision({_transaction, d->connection},
                                                               d->definition->table, _hash);
}

cres_qresult<bool> Manager::canFastForward(const QByteArray& _hash,
                                           const kDB::Repository::Transaction& _transaction) const
{
  QueryConnectionInfo qci(_transaction, d->connection);
  cres_try(QByteArray head_hash, tipHash(_transaction));
  if(head_hash == _hash)
    return cres_success(true);
  return cres_success(DatabaseInterface::PostgreSQL::SQLInterface::forwardRevisionPath(
                        qci, d->definition->table, head_hash, _hash)
                        .size()
                      >= 2);
}

cres_qresult<bool>
  Manager::containsChangesFrom(const QByteArray& _hash,
                               const kDB::Repository::Transaction& _transaction) const
{
  QueryConnectionInfo qci(_transaction, d->connection);
  cres_try(QByteArray head_hash, tipHash(_transaction));
  if(head_hash == _hash)
    return cres_success(true);
  return cres_success(DatabaseInterface::PostgreSQL::SQLInterface::forwardRevisionPath(
                        qci, d->definition->table, _hash, head_hash)
                        .size()
                      >= 2);
}

cres_qresult<QList<Revision>> Manager::revisionsPath(const QByteArray& _source,
                                                     const QByteArray& _destination) const
{
  QList<qint32> path = DatabaseInterface::PostgreSQL::SQLInterface::forwardRevisionPath(
    d->connection, d->definition->table, _source, _destination);
  QList<Revision> revs;

  for(qint32 r : path)
  {
    cres_try(Revision rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                             d->connection, d->definition->table, r));
    revs.append(rev);
  }

  return cres_success(revs);
}

cres_qresult<void> Manager::applyDelta(const kDB::Repository::Transaction& _transaction,
                                       const Delta& _delta, TripleStore* _store, bool _reverse)
{
  if(_delta.delta().isEmpty())
  {
    return cres_success();
  }
  else
  {
    DeltaParser::Parser::Result r = QueryCache::get(_delta.delta());
    if(not r.valid)
    {
      return cres_failure("Parse error");
    }

    QList<knowRDF::Triple> deleted = r.deleted;
    QList<knowRDF::Triple> inserted = r.inserted;
    if(_reverse)
    {
      std::swap(deleted, inserted);
    }
    cres_try(cres_ignore, _store->remove(deleted, _transaction));
    cres_try(cres_ignore, _store->insert(inserted, _transaction));
    return cres_success();
  }
}

cres_qresult<void> Manager::fastForward(const QByteArray& _hash,
                                        const kDB::Repository::Transaction& _transaction)
{
  TemporaryTransaction tt(_transaction, d->connection);
  DatabaseInterface::PostgreSQL::SQLInterface::lockTripleStore(tt.transaction(),
                                                               d->definition->table, true);

  cres_try(QByteArray head_hash, tipHash(tt.transaction()));
  if(head_hash == _hash)
  {
    return tt.commitIfOwned();
  }
  QList<qint32> path = DatabaseInterface::PostgreSQL::SQLInterface::forwardRevisionPath(
    tt.transaction(), d->definition->table, head_hash, _hash);
  if(path.size() < 2)
  {
    cres_try(cres_ignore, tt.rollback(), message("Failed to get forward revision path."));
    return cres_failure("Failed to get forward revision path.");
  }
  // Create a TripleStore instance which does not record changes, to avoid creating new revisions
  TripleStore store(d->connection, d->definition);
  static_cast<TripleStore::Private*>(store.d.data())->disable_recording_version = true;

  // Apply deltas
  for(int i = 1; i < path.size(); ++i)
  {
    cres_try(Delta delta,
             DatabaseInterface::PostgreSQL::SQLInterface::delta(
               tt.transaction(), d->definition->table, path[i - 1], path[i]),
             on_failure(tt.rollback()));
    cres_try(cres_ignore, applyDelta(tt.transaction(), delta, &store, false),
             on_failure(tt.rollback()));
  }

  // Update headdelta,
  Utils::MetaVersion::setHead(tt.transaction(), d->definition, path.last(), _hash);

  return tt.commitIfOwned();
}

cres_qresult<void> Manager::checkout(const QByteArray& _hash,
                                     const kDB::Repository::Transaction& _transaction)
{
  cres_try(bool ff, canFastForward(_hash));
  if(ff)
  {
    return fastForward(_hash, _transaction);
  }

  TemporaryTransaction tt(_transaction, d->connection);
  DatabaseInterface::PostgreSQL::SQLInterface::lockTripleStore(tt.transaction(),
                                                               d->definition->table, true);

  // Look for the path to checkout
  cres_try(QByteArray head_hash, tipHash());
  QPair<QList<qint32>, QList<qint32>> path
    = DatabaseInterface::PostgreSQL::SQLInterface::revisionPath(d->connection, d->definition->table,
                                                                head_hash, _hash);

  if(path.first.size() == 0 or path.second.size() == 0)
  {
    cres_try(cres_ignore, tt.rollback(), message("Failed to get path between revisions."));
    return cres_failure("Failed to get path between revisions.");
  }
  // Create a TripleStore instance which does not record changes, to avoid creating new revisions
  TripleStore store(d->connection, d->definition);
  static_cast<TripleStore::Private*>(store.d.data())->disable_recording_version = true;

  // Apply deltas in reverse
  for(int i = path.first.size() - 1; i > 0; --i)
  {
    cres_try(Delta delta,
             DatabaseInterface::PostgreSQL::SQLInterface::delta(
               tt.transaction(), d->definition->table, path.first[i - 1], path.first[i]),
             on_failure(tt.rollback()));
    cres_try(cres_ignore, applyDelta(tt.transaction(), delta, &store, true),
             on_failure(tt.rollback()));
  }

  // Apply deltas forward
  for(int i = 1; i < path.second.size(); ++i)
  {
    cres_try(Delta delta,
             DatabaseInterface::PostgreSQL::SQLInterface::delta(
               tt.transaction(), d->definition->table, path.second[i - 1], path.second[i]),
             on_failure(tt.rollback()));
    cres_try(cres_ignore, applyDelta(tt.transaction(), delta, &store, false),
             on_failure(tt.rollback()));
  }

  // Update head
  Utils::MetaVersion::setHead(tt.transaction(), d->definition, path.second.last(), _hash);

  return tt.commitIfOwned();
}

cres_qresult<bool> Manager::canMerge(const QByteArray& _hash,
                                     const kDB::Repository::Transaction& _transaction) const
{
  QueryConnectionInfo qci(_transaction, d->connection);
  cres_try(QByteArray head_hash, tipHash(_transaction));
  if(head_hash == _hash)
    return cres_success(true);
  QPair<QList<qint32>, QList<qint32>> path
    = DatabaseInterface::PostgreSQL::SQLInterface::revisionPath(qci, d->definition->table,
                                                                head_hash, _hash);
  return cres_success(path.first.size() >= 1 and path.second.size() >= 1);
}

cres_qresult<void> Manager::merge(const QByteArray& _hash,
                                  const kDB::Repository::Transaction& _transaction)
{
  return merge(QList<QByteArray>{_hash}, _transaction);
}

cres_qresult<void> Manager::merge(const QList<QByteArray>& _hashes,
                                  const kDB::Repository::Transaction& _transaction)
{
  TemporaryTransaction tt(_transaction, d->connection);
  DatabaseInterface::PostgreSQL::SQLInterface::lockTripleStore(tt.transaction(),
                                                               d->definition->table, true);
  cres_try(QByteArray head_hash, tipHash(_transaction), on_failure(tt.rollbackIfOwned()));

  if(_hashes.size() == 1 and head_hash == _hashes.first())
  {
    tt.rollback();
    return cres_success();
  }

  QList<QByteArray> hashes = _hashes;
  hashes.append(head_hash);

  cres_try(QList<QList<qint32>> pathes,
           DatabaseInterface::PostgreSQL::SQLInterface::revisionPath(tt.transaction(),
                                                                     d->definition->table, hashes),
           on_failure(tt.rollbackIfOwned();));

  if(pathes.size() == 2) // Aka merge between current and an other branch
  {
    if(pathes[0].size() == 1)
    {
      clog_warning("Cannot merge, head ({}) is a child of target ({}).", kDBppRevHash(head_hash),
                   kDBppRevHash(_hashes.first()));
      cres_try(cres_ignore, tt.rollbackIfOwned());
      return cres_success();
    }
    if(pathes[1].size() == 1)
    {
      clog_warning(
        "Nothing to merge, target ({}) is already a child of head ({}), fast forwarding...",
        kDBppRevHash(head_hash), kDBppRevHash(_hashes.first()));
      cres_try(cres_ignore, fastForward(_hashes.first(), tt.transaction()));
      cres_try(cres_ignore, tt.commitIfOwned());
      return cres_success();
    }
  }
  clog_assert(pathes.size() == hashes.size());

  DefaultMergeStrategy dms;
  for(const QList<qint32>& p : pathes)
  {
    QList<Revision> revisions;
    for(int i = 0; i < p.size(); ++i)
    {
      cres_try(Revision r, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                             tt.transaction(), d->definition->table, p[i]));
      revisions.append(r);
    }
    dms.addRevisions(revisions);
  }

  if(dms.merge())
  {
    if(dms.allHaveChanges())
    {
      knowCore::Timestamp timestamp = knowCore::Timestamp::now();
      QByteArray tmp_hash = Cyqlops::Crypto::Random::generate(20);
      RevisionBuilder rb = insertRevision(tmp_hash); // Use a temporary hash
      for(qsizetype idx = 0; idx < hashes.size(); ++idx)
      {
        rb.addDelta(dms.parentHash(idx), dms.diff(idx), true);
      }
      cres_try(Revision r, rb.insert(tt.transaction(), false));

      cres_try(cres_ignore, fastForward(r.hash(), tt.transaction()));
      QByteArray content_hash = DatabaseInterface::PostgreSQL::SQLInterface::computeContentHash(
        tt.transaction(), d->definition->table);

      cres_try(Revision target_merge_rev,
               DatabaseInterface::PostgreSQL::SQLInterface::revision(
                 tt.transaction(), d->definition->table, content_hash, r.historicity()));

      QByteArray real_hash;
      if(target_merge_rev.isValid())
      {
        // Revision already exists
        cres_try(cres_ignore,
                 DatabaseInterface::PostgreSQL::SQLInterface::removeRevision(tt.transaction(), r),
                 on_failure(tt.rollback();));
        r = target_merge_rev;
        real_hash = r.hash();

        knowCore::Timestamp tnow = knowCore::Timestamp::now();
        QUuid author = d->connection.serverUuid();
        for(qsizetype i = 0; i < hashes.size(); ++i)
        {
          clog_assert(dms.parentHash(i) != real_hash);
          QByteArray parentHash = dms.parentHash(i);
          cres_try(Revision parent_rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                                          tt.transaction(), d->definition->table, parentHash));
          // Check if need to insert deltas
          cres_try(bool hasDelta, DatabaseInterface::PostgreSQL::SQLInterface::hasDelta(
                                    tt.transaction(), d->definition->table, parentHash, real_hash));
          if(not hasDelta)
          {
            QByteArray diff = dms.diff(i);
            cres_try(cres_ignore,
                     DatabaseInterface::PostgreSQL::SQLInterface::recordDelta(
                       tt.transaction(), d->definition->table, parent_rev.d->revisionId,
                       r.d->revisionId, Utils::computeDeltaHash(diff), diff));
            clog_assert(DatabaseInterface::PostgreSQL::SQLInterface::hasDelta(
                          tt.transaction(), d->definition->table, parentHash, real_hash)
                          .expect_success());
          }
          cres_try(Delta delta, DatabaseInterface::PostgreSQL::SQLInterface::delta(
                                  tt.transaction(), d->definition->table, parent_rev, r));
          cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::recordDeltaSignature(
                                  tt.transaction(), d->definition->table, parent_rev.d->revisionId,
                                  r.d->revisionId, author.toByteArray(), tnow,
                                  Utils::computeSignature(d->connection.rsaAlgorithm(), author,
                                                          tnow, delta.hash())));
        }
      }
      else
      {
        real_hash = Utils::computeRevisionHash(content_hash, r.historicity());
        cres_try(
          cres_ignore,
          DatabaseInterface::PostgreSQL::SQLInterface::setRevisionHash(
            tt.transaction(), d->definition->table, r.d->revisionId, real_hash, content_hash),
          on_failure(tt.rollback()));
        DatabaseInterface::PostgreSQL::SQLInterface::notifyNewRevision(
          tt.transaction(), d->definition->table, real_hash);
        clog_assert(DatabaseInterface::PostgreSQL::SQLInterface::hasRevision(
                      tt.transaction(), d->definition->table, real_hash)
                      .expect_success());
      }

      // Update head
      Utils::MetaVersion::setHead(tt.transaction(), d->definition, r.d->revisionId, real_hash);
      return tt.commitIfOwned();
    }
    else
    {
      // One of the revision already contains all the changes, and become the target of the merge
      Revision target_rev;
      for(qsizetype i = 0; i < hashes.size(); ++i)
      {
        if(not dms.hasChanges(i))
        {
          cres_try(Revision parent_rev,
                   DatabaseInterface::PostgreSQL::SQLInterface::revision(
                     tt.transaction(), d->definition->table, dms.parentHash(i)));
          // If two targets have no changes, choose the revision with lowest historicity as the main
          // merge
          if(not target_rev.isValid() or parent_rev.historicity() < target_rev.historicity())
          {
            target_rev = parent_rev;
          }
        }
      }

      for(qsizetype i = 0; i < hashes.size(); ++i)
      {
        cres_try(Revision parent_rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                                        tt.transaction(), d->definition->table, dms.parentHash(i)));
        if(parent_rev.d->revisionId != target_rev.d->revisionId)
        {
          cres_try(bool hasDelta, DatabaseInterface::PostgreSQL::SQLInterface::hasDelta(
                                    tt.transaction(), d->definition->table,
                                    parent_rev.d->revisionId, target_rev.d->revisionId));
          if(not hasDelta)
          {
            QByteArray diff = dms.diff(i);
            cres_try(cres_ignore,
                     DatabaseInterface::PostgreSQL::SQLInterface::recordDelta(
                       tt.transaction(), d->definition->table, parent_rev.d->revisionId,
                       target_rev.d->revisionId, Utils::computeDeltaHash(diff), diff));
          }
          cres_try(Delta delta, DatabaseInterface::PostgreSQL::SQLInterface::delta(
                                  tt.transaction(), d->definition->table, parent_rev, target_rev));
          knowCore::Timestamp tnow = knowCore::Timestamp::now();
          QUuid author = d->connection.serverUuid();
          cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::recordDeltaSignature(
                                  tt.transaction(), d->definition->table, parent_rev.d->revisionId,
                                  target_rev.d->revisionId, author.toByteArray(), tnow,
                                  Utils::computeSignature(d->connection.rsaAlgorithm(), author,
                                                          tnow, delta.hash())));
        }
      }

      // Insert the delta
      cres_try(cres_ignore, fastForward(target_rev.hash(), tt.transaction()));

      return tt.commitIfOwned();
    }
  }
  else
  {
    cres_try(cres_ignore, tt.rollback(), message("Merge algorithm failed."));
    return cres_failure("Merge algorithm failed.");
  }
}

cres_qresult<bool> Manager::canRebase(const QByteArray& _hash,
                                      const kDB::Repository::Transaction& _transaction) const
{
  QueryConnectionInfo qci(_transaction, d->connection);
  cres_try(QByteArray head_hash, tipHash());
  if(head_hash == _hash)
    return cres_success(true);
  QPair<QList<qint32>, QList<qint32>> path
    = DatabaseInterface::PostgreSQL::SQLInterface::revisionPath(qci, d->definition->table,
                                                                head_hash, _hash);
  if(path.first.size() == 0 or path.second.size() == 0)
    return cres_success(false);

  // Check that the revisions are private
  for(int i = 1; i < path.second.size(); ++i)
  {
    cres_try(Revision rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                             qci, d->definition->table, path.second[i]));
    if(not rev.tags().testFlag(Revision::Tag::Private))
    {
      return cres_success(false);
    }
  }
  return cres_success(true);
}

cres_qresult<bool> Manager::canRebaseTo(const QByteArray& _hash,
                                        const kDB::Repository::Transaction& _transaction) const
{
  QueryConnectionInfo qci(_transaction, d->connection);
  cres_try(QByteArray head_hash, tipHash());
  if(head_hash == _hash)
    return cres_success(true);
  QPair<QList<qint32>, QList<qint32>> path
    = DatabaseInterface::PostgreSQL::SQLInterface::revisionPath(qci, d->definition->table,
                                                                head_hash, _hash);
  if(path.first.size() == 0 or path.second.size() == 0)
    return cres_success(false);

  // Check that the revisions are private
  for(int i = 1; i < path.first.size(); ++i)
  {
    cres_try(Revision rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                             qci, d->definition->table, path.first[i]));
    if(not rev.tags().testFlag(Revision::Tag::Private))
    {
      return cres_success(false);
    }
  }
  return cres_success(true);
}

cres_qresult<Revision> Manager::rebase(const QByteArray& _hash, bool _squash,
                                       const kDB::Repository::Transaction& _transaction)
{
  TemporaryTransaction tt(_transaction, d->connection);
  DatabaseInterface::PostgreSQL::SQLInterface::lockTripleStore(tt.transaction(),
                                                               d->definition->table, true);

  // Look for the path to checkout
  cres_try(QByteArray head_hash, tipHash(tt.transaction()));
  QPair<QList<qint32>, QList<qint32>> path
    = DatabaseInterface::PostgreSQL::SQLInterface::revisionPath(
      tt.transaction(), d->definition->table, head_hash, _hash);

  if(path.first.size() == 0 or path.second.size() == 0)
  {
    cres_try(cres_ignore, tt.rollback(), message("Failed to get path between revisions."));
    return cres_failure("Failed to get path between revisions.");
  }
  if(path.second.size() == 1)
  {
    clog_warning("Cannot rebase, head ({}) is a child of target ({}).", kDBppRevHash(head_hash),
                 kDBppRevHash(_hash));
    cres_try(cres_ignore, tt.rollbackIfOwned());
    return tip();
  }
  if(path.first.size() == 1)
  {
    clog_warning(
      "Nothing to rebase, target ({}) is already a child of head ({}), fast forwarding...",
      kDBppRevHash(head_hash), kDBppRevHash(_hash));
    cres_try(cres_ignore, fastForward(_hash, tt.transaction()));
    cres_try(cres_ignore, tt.commitIfOwned());
    return revision(_hash, _transaction);
  }

  QList<QList<Revision>> revisions;
  for(int i = 1; i < path.second.size(); ++i)
  {
    cres_try(Revision rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                             tt.transaction(), d->definition->table, path.second[i]));
    if(not rev.tags().testFlag(Revision::Tag::Private))
    {
      cres_try(cres_ignore, tt.rollback());
      return cres_log_failure("Cannot rebase, there is a non private revision ({}) in the path",
                              kDBppRevHash(rev.hash()));
    }
    if(rev.deltas().size() != 1)
    {
      cres_try(cres_ignore, tt.rollback());
      return cres_failure("Cannot rebase, there is a merge revision ({}) in the path",
                          kDBppRevHash(rev.hash()));
    }
    if(not _squash or not rev.tags().testFlag(Revision::Tag::Editable) or revisions.isEmpty()
       or not revisions.last().last().tags().testFlag(Revision::Tag::Editable))
    {
      revisions.append({rev});
    }
    else
    {
      revisions.last().append(rev);
    }
  }
  clog_assert(revisions.size() > 0);

  cres_try(Revision tipRev, revision(head_hash, tt.transaction()));
  cres_try(Revision prevRev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                               tt.transaction(), d->definition->table, path.second.first()));

  // Unapply the revisions
  TripleStore store(d->connection, d->definition);
  static_cast<TripleStore::Private*>(store.d.data())->disable_recording_version = true;
  for(int i = revisions.size() - 1; i >= 0; --i)
  {
    for(int j = revisions[i].size() - 1; j >= 0; --j)
    {
      Revision rev = revisions[i][j];
      clog_assert(rev.deltas().size() == 1);
      Delta delta = rev.deltas().first();
      if(not applyDelta(tt.transaction(), delta, &store, true).is_successful())
      {
        cres_try(cres_ignore, tt.rollback());
        return cres_log_failure("failed remove revision");
      }
    }
  }

  // Generate the revisions
  for(int i = 0; i < revisions.size(); ++i)
  {
    QList<Revision> revisonList = revisions[i];

    RevisionBuilder rb;
    rb.d->tags = revisonList.last().tags();
    rb.d->content_hash = Cyqlops::Crypto::Random::generate(20);
    rb.d->connection = d->connection;
    rb.d->triplesStoreTableName = d->definition->table;

    if(revisonList.size() == 1)
    {
      cres_try(Delta delta,
               DatabaseInterface::PostgreSQL::SQLInterface::delta(
                 tt.transaction(), d->definition->table, prevRev, revisonList.first()));
      rb.addDelta(tipRev.hash(), delta.delta(), true);
    }
    else
    {
      BuildAccumulatedDiff bad;

      Revision badPrevRev = prevRev;

      for(const Revision& rev : revisonList)
      {
        cres_try(Delta delta, DatabaseInterface::PostgreSQL::SQLInterface::delta(
                                tt.transaction(), d->definition->table, badPrevRev, rev));
        bad.execute(delta.delta());
        badPrevRev = rev;
      }
      DeltaBuilder deltaBuilder;

      for(TriplesToStatus::const_iterator sTPit = bad.diff.begin(); sTPit != bad.diff.end();
          ++sTPit)
      {
        const TripleDeltaStatus source_status = sTPit.value();
        const knowRDF::Triple& triple = sTPit.key();

        switch(source_status)
        {
        case TripleDeltaStatus::Skip:
        case TripleDeltaStatus::Unknown:
          // Well, skip
          break;
        case TripleDeltaStatus::Added:
          deltaBuilder.reportInsertion(triple);
          break;
        case TripleDeltaStatus::Removed:
          // This is actually a conflict, we take the conservative approach and keep the triple in
          // the result graph
          deltaBuilder.reportRemoval(triple);
          break;
        }
      }
      rb.addDelta(tipRev.hash(), deltaBuilder.delta(), true);
    }
    prevRev = revisonList.last();
    cres_try(tipRev, rb.insert(tt.transaction(), false));

    cres_try(cres_ignore,
             applyDelta(tt.transaction(), tipRev.deltas(tt.transaction()).first(), &store, false),
             on_failure(tt.rollback()));
    tipRev.d->content_hash = DatabaseInterface::PostgreSQL::SQLInterface::computeContentHash(
      tt.transaction(), d->definition->table);
    tipRev.d->hash = Utils::computeRevisionHash(tipRev.d->content_hash, tipRev.historicity());
    cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::setRevisionHash(
                            tt.transaction(), d->definition->table, tipRev.d->revisionId,
                            tipRev.d->hash, tipRev.d->content_hash));
    DatabaseInterface::PostgreSQL::SQLInterface::notifyNewRevision(
      tt.transaction(), d->definition->table, tipRev.d->hash);

    if(not tipRev.isValid())
    {
      tt.rollback();
      return cres_log_failure("Failed to create revision while rebasing");
    }
  }

  // Remove revisions

  for(const QList<Revision>& revisonList : revisions)
  {
    for(const Revision& rev : revisonList)
    {
      DatabaseInterface::PostgreSQL::SQLInterface::removeRevision(tt.transaction(), rev);
    }
  }

  clog_assert(canFastForward(tipRev.hash(), tt.transaction()).expect_success());
  cres_try(cres_ignore, fastForward(tipRev.hash(), tt.transaction()));
  cres_try(cres_ignore, tt.commitIfOwned());
  return cres_success(tipRev);
}

cres_qresult<void> kDB::Repository::VersionControl::Manager::publish(
  const QByteArray& _hash, const kDB::Repository::Transaction& _transaction)
{
  cres_try(Revision r, revision(_hash, _transaction));
  return publish(r, _transaction);
}

cres_qresult<void> kDB::Repository::VersionControl::Manager::publish(
  const kDB::Repository::VersionControl::Revision& _revision,
  const kDB::Repository::Transaction& _transaction)
{
  TemporaryTransaction tt(_transaction, d->connection);

  clog_assert(_revision.isValid());

  QList<kDB::Repository::VersionControl::Revision> revisions;
  QList<kDB::Repository::VersionControl::Revision> revisions_to_tag;

  revisions.append(_revision);

  while(not revisions.isEmpty())
  {
    Revision r = revisions.takeFirst();
    clog_assert(r.isValid());
    for(const Delta& d : r.deltas(_transaction))
    {
      cres_try(Revision pr, revision(d.parent(), tt.transaction()));
      if(pr.tags().testFlag(Revision::Tag::Private) or pr.tags().testFlag(Revision::Tag::Editable))
      {
        revisions.append(pr);
        revisions_to_tag.prepend(pr);
      }
    }
  }
  // tag first the parents
  for(const Revision& r : revisions_to_tag)
  {
    Revision::Tags nt
      = r.tags().setFlag(Revision::Tag::Private, false).setFlag(Revision::Tag::Editable, false);
    cres_try(cres_ignore,
             DatabaseInterface::PostgreSQL::SQLInterface::setRevisionTags(
               tt.transaction(), d->definition->table, r, nt),
             on_failure(tt.rollback()));
  }

  return tt.commitIfOwned();
}

QMetaObject::Connection
  Manager::listenNewRevision(const std::function<void(const Revision&)>& _receiver)
{
  QMutexLocker l(&d->mutex);
  QMetaObject::Connection c = d->connection.notificationsManager()->listen(
    qPrintable(d->definition->table + "_new_revision"),
    [_receiver, this](const QByteArray& _payload)
    {
      QMutexLocker l(&d->mutex);
      cres_qresult<Revision> rev = DatabaseInterface::PostgreSQL::SQLInterface::revision(
        d->connection, d->definition->table, QByteArray::fromBase64(_payload));
      if(rev.is_successful())
      {
        _receiver(rev.get_value());
      }
    });
  d->connections.append(c);
  return c;
}

QMetaObject::Connection
  Manager::listenRevisionTagsChanged(const std::function<void(const Revision&)>& _receiver)
{
  QMutexLocker l(&d->mutex);
  QMetaObject::Connection c = d->connection.notificationsManager()->listen(
    qPrintable(d->definition->table + "_revision_tags_changed"),
    [_receiver, this](const QByteArray& _payload)
    {
      QMutexLocker l(&d->mutex);
      cres_qresult<Revision> rev = DatabaseInterface::PostgreSQL::SQLInterface::revision(
        d->connection, d->definition->table, QByteArray::fromBase64(_payload));
      rev.expect_success();
      _receiver(rev.get_value());
    });
  d->connections.append(c);
  return c;
}

void Manager::dump()
{
  clog_info("Revisions for '{}'", d->definition->name);
  QList<Revision> revs = revisions().expect_success();
  for(const Revision& rev : revs)
  {
    clog_info("Revision ({}):", kDBppRevHash(rev.hash()));
    for(const Delta& delta : rev.deltas())
    {
      clog_info(" - Delta with parent ({}) hash ({}): {}", kDBppRevHash(delta.parent()),
                delta.hash().toHex(), delta.delta());
      for(const Signature& sign : delta.signatures())
      {
        clog_info("   Signed by {} at {}", sign.author(), sign.timestamp());
      }
    }
  }
}

void Manager::setDefaultRevisionTags(Revision::Tags _tags) { d->defaultRevisionTags = _tags; }

void Manager::setDefaultRevisionTag(Revision::Tag _tag, bool _on)
{
  d->defaultRevisionTags.setFlag(_tag, _on);
}

Revision::Tags Manager::defaultRevisionTags() const { return d->defaultRevisionTags; }
