#include "Signature.h"

#include <QUuid>
#include <knowCore/Timestamp.h>

using namespace kDB::Repository::VersionControl;

struct Signature::Private : public QSharedData
{
  QUuid author;
  knowCore::Timestamp timestamp;
  QByteArray signature;
};

Signature::Signature() : d(new Private) {}

Signature::Signature(const QUuid& _author, const knowCore::Timestamp& _timestamp,
                     const QByteArray& _signature)
    : d(new Private{QSharedData(), _author, _timestamp, _signature})
{
}

Signature::Signature(const Signature& _rhs) : d(_rhs.d) {}

Signature& Signature::operator=(const Signature& _rhs)
{
  d = _rhs.d;
  return *this;
}

Signature::~Signature() {}

QUuid Signature::author() const { return d->author; }

knowCore::Timestamp Signature::timestamp() const { return d->timestamp; }

QByteArray Signature::signature() const { return d->signature; }
