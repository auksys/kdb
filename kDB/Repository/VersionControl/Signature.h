#include <QExplicitlySharedDataPointer>

#include <knowCore/Forward.h>

class QUuid;

namespace kDB::Repository::VersionControl
{
  /**
   * Represent a signature.
   */
  class Signature
  {
  public:
    Signature();
    Signature(const QUuid& _author, const knowCore::Timestamp& _timestamp,
              const QByteArray& _signature);
    Signature(const Signature& _rhs);
    Signature& operator=(const Signature& _rhs);
    ~Signature();
    QUuid author() const;
    knowCore::Timestamp timestamp() const;
    QByteArray signature() const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace kDB::Repository::VersionControl
