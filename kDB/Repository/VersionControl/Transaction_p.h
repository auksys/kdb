#pragma once

#include <QSharedPointer>
#include <kDB/Forward.h>

namespace kDB::Repository::VersionControl
{
  class Transaction
  {
    friend class Repository::TripleStore;
    friend class Repository::DatabaseInterface::PostgreSQL::SQLInterface;
  public:
    Transaction();
    Transaction(const TripleStore& _store, const Repository::Transaction& _transaction);
    Transaction(const Transaction& _rhs);
    Transaction& operator=(const Transaction& _rhs);
    ~Transaction();
    cres_qresult<void> commit();
    cres_qresult<void> rollback();
  private:
    struct Private;
    QSharedPointer<Private> d;
  };
} // namespace kDB::Repository::VersionControl
