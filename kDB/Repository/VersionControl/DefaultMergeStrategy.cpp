#include "DefaultMergeStrategy.h"

#include <QMutex>

#include <clog_qt>
#include <knowCore/Uri.h>

#include <knowRDF/Literal.h>
#include <knowRDF/Triple.h>

#include "Delta.h"

#include "AbstractMergeStrategy_p.h"
#include "BuildAccumulatedDiff_p.h"
#include "DeltaBuilder.h"
using namespace kDB::Repository::VersionControl;

struct DefaultMergeStrategy::Private : public AbstractMergeStrategy::Private
{
  bool merge() override;

  /**
   * Compute the merge of other branches into the branch referenced by \p _index
   */
  QByteArray delta(qsizetype _index, const QList<TriplesToStatus>& _statutes)
  {
    TriplesToStatus operations;
    const TriplesToStatus& target = _statutes[_index];

    for(qsizetype other_s_i = 0; other_s_i < _statutes.size(); ++other_s_i)
    {
      if(other_s_i != _index)
      {
        const TriplesToStatus& other = _statutes[other_s_i];
        for(TriplesToStatus::const_iterator sTPit = other.begin(); sTPit != other.end(); ++sTPit)
        {
          const TripleDeltaStatus target_status
            = target.value(sTPit.key(), TripleDeltaStatus::Unknown);
          TripleDeltaStatus new_operation = TripleDeltaStatus::Unknown;
          if(target_status == sTPit.value())
          {
            new_operation = TripleDeltaStatus::Skip;
          }
          else
          {
            switch(sTPit.value())
            {
            case TripleDeltaStatus::Unknown:
              clog_fatal("Should not happen.");
              break;
            case TripleDeltaStatus::Skip:
            case TripleDeltaStatus::Added:
            case TripleDeltaStatus::Removed:
              new_operation = sTPit.value();
              break;
            }
          }
          TripleDeltaStatus current_operation
            = operations.value(sTPit.key(), TripleDeltaStatus::Unknown);
          if(current_operation != new_operation)
          {
            switch(current_operation)
            {
            case TripleDeltaStatus::Unknown:
            case TripleDeltaStatus::Skip:
              operations[sTPit.key()] = new_operation;
              break;
            case TripleDeltaStatus::Added:
              if(new_operation == TripleDeltaStatus::Removed)
              {
                clog_fatal("Should not happen.");
              }
              break;
            case TripleDeltaStatus::Removed:
              if(new_operation == TripleDeltaStatus::Added)
              {
                clog_fatal("Should not happen.");
              }
              break;
            }
          }

          if(sTPit.value() != TripleDeltaStatus::Unknown)
          {
            new_operation = sTPit.value();
          }
          else
          {
            new_operation = TripleDeltaStatus::Skip;
          }
        }
      }
    }
    DeltaBuilder deltaBuilder;
    for(TriplesToStatus::const_iterator sTPit = operations.begin(); sTPit != operations.end();
        ++sTPit)
    {
      switch(sTPit.value())
      {
      case TripleDeltaStatus::Skip:
      case TripleDeltaStatus::Unknown:
        break;
      case TripleDeltaStatus::Added:
        deltaBuilder.reportInsertion(sTPit.key());
        break;
      case TripleDeltaStatus::Removed:
        deltaBuilder.reportRemoval(sTPit.key());
        break;
      }
    }
    if(deltaBuilder.isEmpty())
    {
      return QByteArray();
    }
    else
    {
      return deltaBuilder.delta();
    }
  }
};

bool DefaultMergeStrategy::Private::merge()
{
  QList<TriplesToStatus> triplesToStatuses;

  for(BranchInfo& bi : branchInfos)
  {
    BuildAccumulatedDiff accumulator;
    QByteArray previousRevisionHash = bi.revisions.front().hash();
    for(qsizetype i = 1; i < bi.revisions.size(); ++i)
    {
      Delta d = bi.revisions[i].deltaFrom(previousRevisionHash);
      clog_assert(d.isValid());
      if(not accumulator.execute(d.delta()))
        return false;
      previousRevisionHash = bi.revisions[i].hash();
    }
    triplesToStatuses.append(accumulator.diff);
  }
  allHaveChanges = true;
  for(qsizetype idx = 0; idx < branchInfos.size(); ++idx)
  {
    BranchInfo& bi = branchInfos[idx];
    bi.diff = delta(idx, triplesToStatuses);
    bi.hasChanges = not bi.diff.isEmpty();
    allHaveChanges = allHaveChanges and bi.hasChanges;
  }

  return true;
}

DefaultMergeStrategy::DefaultMergeStrategy() : AbstractMergeStrategy(new Private) {}

DefaultMergeStrategy::~DefaultMergeStrategy() {}
