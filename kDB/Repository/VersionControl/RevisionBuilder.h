#include "../TripleStore.h"

#include "Revision.h"

namespace kDB::Repository::VersionControl
{
  namespace internal
  {
    void set_simulate_v1(bool _v);
  }
  /**
   * This class is intended to help with inserting revisions in the database.
   */
  class RevisionBuilder
  {
    friend class Manager;
    friend void internal::set_simulate_v1(bool _v);
  public:
    RevisionBuilder();
    RevisionBuilder(const RevisionBuilder& _rhs);
    RevisionBuilder& operator=(const RevisionBuilder& _rhs);
    ~RevisionBuilder();
    RevisionBuilder& setMetaInformation(const QByteArray& _hash, qint64 _historicity);
    RevisionBuilder& setTags(Revision::Tags _tag);
    /**
     * Add a delta to the \p _parent revision with the given \p _hash, \p _delta and \p signatures.
     */
    RevisionBuilder& addDelta(const QByteArray& _parent, const QByteArray& _hash,
                              const QByteArray& _delta, const QList<Signature>& _signatures);
    /**
     * Add a delta to the \p _parent revision with the given  \p _delta. The delta hash will be
     * computed from \p _delta. \p _sign indicates whether this server should sign the revision or
     * not.
     */
    RevisionBuilder& addDelta(const QByteArray& _parent, const QByteArray& _delta, bool _sign);
    cres_qresult<Revision> insert(Repository::Transaction _transaction, bool _notify_revision);
    void discard();
  private:
    struct Private;
    QSharedPointer<Private> d;
  };
} // namespace kDB::Repository::VersionControl
