#include "AbstractMergeStrategy.h"

namespace kDB::Repository::VersionControl
{
  class DefaultMergeStrategy : public AbstractMergeStrategy
  {
  public:
    DefaultMergeStrategy();
    ~DefaultMergeStrategy();
  private:
    struct Private;
  };
} // namespace kDB::Repository::VersionControl
