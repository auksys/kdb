#include "Revision_p.h"

#include <QCryptographicHash>

#include "../DatabaseInterface/PostgreSQL/SQLInterface_p.h"
#include "../QueryConnectionInfo.h"

#include "Delta.h"

using namespace kDB::Repository::VersionControl;

Revision::Revision() : d(new Private) {}

Revision::Revision(const Revision& _rhs) : d(_rhs.d) {}

Revision& Revision::operator=(const Revision& _rhs)
{
  d = _rhs.d;
  return *this;
}

Revision::~Revision() {}

bool Revision::isValid() const { return not d->hash.isEmpty(); }

QList<Delta> Revision::deltas(const kDB::Repository::Transaction& _transaction) const
{
  // deltas cannot be cached, more can be added
  return DatabaseInterface::PostgreSQL::SQLInterface::deltas({_transaction, d->connection},
                                                             d->store_table_name, d->revisionId);
}

Delta Revision::deltaFrom(const QByteArray& _parent) const
{
  for(const Delta& delta : deltas())
  {
    if(delta.parent() == _parent)
    {
      return delta;
    }
  }
  return Delta();
}

cres_qresult<QList<Revision>>
  Revision::children(const kDB::Repository::Transaction& _transaction) const
{
  return DatabaseInterface::PostgreSQL::SQLInterface::revisionChildren(
    {_transaction, d->connection}, d->store_table_name, d->revisionId);
}

QByteArray Revision::hash() const { return d->hash; }

QByteArray Revision::contentHash() const { return d->content_hash; }

int Revision::historicity() const { return d->historicity; }

Revision::Tags Revision::tags() const { return d->tags; }

bool Revision::isSignedBy(const QUuid& _author) const
{
  for(const Delta& delta : deltas())
  {
    clog_assert(delta.isSigned());
    if(delta.isSignedBy(_author))
    {
      return true;
    }
  }
  return false;
}

#define COMP(_FIELD_) d->_FIELD_ == _rhs.d->_FIELD_

bool Revision::operator==(const Revision& _rhs) const
{
  return COMP(hash) and COMP(historicity) and COMP(store_table_name) and deltas() == _rhs.deltas();
}

QByteArray Revision::initialHash()
{
  return QCryptographicHash(QCryptographicHash::QCryptographicHash::Sha512).result();
}
