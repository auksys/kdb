#include "AbstractMergeStrategy.h"

#include "Revision.h"

#include <QUuid>
#include <knowCore/Timestamp.h>

#include <Cyqlops/Crypto/RSAAlgorithm.h>

namespace kDB::Repository::VersionControl
{
  struct AbstractMergeStrategy::Private
  {
    struct BranchInfo
    {
      QList<Revision> revisions;
      QByteArray diff;
      bool hasChanges;
    };
    QList<BranchInfo> branchInfos;
    bool allHaveChanges;

    virtual ~Private() {}
    virtual bool merge() = 0;
  };
} // namespace kDB::Repository::VersionControl
