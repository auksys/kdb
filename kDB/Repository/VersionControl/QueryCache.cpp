#include "QueryCache_p.h"

using namespace kDB::Repository::VersionControl;

QHash<QByteArray, DeltaParser::Parser::Result> QueryCache::queries;
QMutex QueryCache::m;
