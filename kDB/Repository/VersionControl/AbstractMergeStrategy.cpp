#include "AbstractMergeStrategy_p.h"

#include "Utils_p.h"

using namespace kDB::Repository::VersionControl;

AbstractMergeStrategy::AbstractMergeStrategy(AbstractMergeStrategy::Private* _d) : d(_d) {}

AbstractMergeStrategy::~AbstractMergeStrategy() { delete d; }

bool AbstractMergeStrategy::merge() { return d->merge(); }

void AbstractMergeStrategy::addRevisions(const QList<Revision>& _revisions)
{
  Private::BranchInfo bi;
  bi.revisions = _revisions;
  d->branchInfos.append(bi);
}

bool AbstractMergeStrategy::AbstractMergeStrategy::hasChanges(qsizetype _index) const
{
  return d->branchInfos[_index].hasChanges;
}

QByteArray AbstractMergeStrategy::diff(qsizetype _index) const
{
  return d->branchInfos[_index].diff;
}

QByteArray AbstractMergeStrategy::parentHash(qsizetype _index) const
{
  return d->branchInfos[_index].revisions.back().hash();
}

bool AbstractMergeStrategy::allHaveChanges() const { return d->allHaveChanges; }
