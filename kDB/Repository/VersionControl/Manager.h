#include <functional>

#include <kDB/Forward.h>
#include <kDB/Repository/TripleStore.h>

#include "Revision.h"

namespace kDB::Repository::VersionControl
{
  class Manager
  {
    friend class Repository::TripleStore;
    Manager(const kDB::Repository::Connection& _connection,
            QSharedPointer<TripleStore::Definition> _definition);
  public:
    ~Manager();
    /**
     * Define is signing is enabled or not.
     */
    void setSigningEnabled(bool _signing);
    /**
     * @return if signing is enabled or not.
     */
    bool signingEnabled() const;
    cres_qresult<QList<Revision>> revisions(const kDB::Repository::Transaction& _transaction
                                            = kDB::Repository::Transaction()) const;
    cres_qresult<Revision> tip(const kDB::Repository::Transaction& _transaction
                               = kDB::Repository::Transaction()) const;
    cres_qresult<QByteArray> tipHash(const kDB::Repository::Transaction& _transaction
                                     = kDB::Repository::Transaction()) const;
    cres_qresult<QList<Revision>> heads(const kDB::Repository::Transaction& _transaction
                                        = kDB::Repository::Transaction()) const;
    /**
     * Start inserting a revision with the given content hash.
     */
    RevisionBuilder insertRevision(const QByteArray& _content_hash);
    cres_qresult<QList<Revision>> revisionsPath(const QByteArray& _source,
                                                const QByteArray& _destination) const;
    cres_qresult<bool> hasRevision(const QByteArray& _hash,
                                   const kDB::Repository::Transaction& _transaction
                                   = kDB::Repository::Transaction()) const;
    cres_qresult<Revision> revision(const QByteArray& _hash,
                                    const kDB::Repository::Transaction& _transaction
                                    = kDB::Repository::Transaction()) const;
    /**
     * @return true if there is a direct path between \ref tipHash and \ref _hash.
     */
    cres_qresult<bool> canFastForward(const QByteArray& _hash,
                                      const kDB::Repository::Transaction& _transaction
                                      = kDB::Repository::Transaction()) const;
    cres_qresult<void> fastForward(const QByteArray& _hash,
                                   const kDB::Repository::Transaction& _transaction
                                   = kDB::Repository::Transaction());
    /**
     * @return true if the changes from the given revision \p _hash are included in the current tip.
     * This function is the opposite of \ref canFastForward.
     */
    cres_qresult<bool> containsChangesFrom(const QByteArray& _hash,
                                           const kDB::Repository::Transaction& _transaction
                                           = kDB::Repository::Transaction()) const;
    /**
     * Checkout a specific \ref _hash
     */
    cres_qresult<void> checkout(const QByteArray& _hash,
                                const kDB::Repository::Transaction& _transaction
                                = kDB::Repository::Transaction());
    cres_qresult<bool> canMerge(const QByteArray& _hash,
                                const kDB::Repository::Transaction& _transaction
                                = kDB::Repository::Transaction()) const;
    cres_qresult<void> merge(const QByteArray& _hash,
                             const kDB::Repository::Transaction& _transaction
                             = kDB::Repository::Transaction());
    /**
     * Merge many revisions into a single revision.
     */
    cres_qresult<void> merge(const QList<QByteArray>& _hashes,
                             const kDB::Repository::Transaction& _transaction
                             = kDB::Repository::Transaction());
    /**
     * Publish the revision specified by hash and all its Private/Editable ancestors
     */
    cres_qresult<void> publish(const QByteArray& _hash,
                               const kDB::Repository::Transaction& _transaction
                               = kDB::Repository::Transaction());
    /**
     * Publish @p _revision and all its Private/Editable ancestors
     */
    cres_qresult<void> publish(const Revision& _revision,
                               const kDB::Repository::Transaction& _transaction
                               = kDB::Repository::Transaction());
    /**
     * Call the \p _receiver function every time a new revision is added to the manager.
     */
    QMetaObject::Connection
      listenNewRevision(const std::function<void(const Revision&)>& _receiver);
    /**
     * Call the \p _receiver function every time the tags are changed on a revision.
     */
    QMetaObject::Connection
      listenRevisionTagsChanged(const std::function<void(const Revision&)>& _receiver);
    /**
     * @return true if it is possible to rebase \ref _hash onto the current revision.
     *         This check if there is a connection between the current revison and \ref _hash
     *         and if the revisions that needs to be moved are marked with \ref
     * Revision::Tag::Private
     */
    cres_qresult<bool> canRebase(const QByteArray& _hash,
                                 const kDB::Repository::Transaction& _transaction
                                 = kDB::Repository::Transaction()) const;
    /**
     * @return true if it is possible to rebase the current revision on revision \ref _hash .
     *         This check if there is a connection between the current revison and \ref _hash
     *         and if the revisions that needs to be moved are marked with \ref
     * Revision::Tag::Private
     */
    cres_qresult<bool> canRebaseTo(const QByteArray& _hash,
                                   const kDB::Repository::Transaction& _transaction
                                   = kDB::Repository::Transaction()) const;
    /**
     * Attempt to rebase \ref _hash on top of the current revision.
     *
     * @param _squash indicates if we should try to squash (ie combine) revisions that are rebased
     * (provided that the revisions are marked with \ref Revision::Tag::Editable)
     */
    cres_qresult<Revision> rebase(const QByteArray& _hash, bool _squash,
                                  const kDB::Repository::Transaction& _transaction
                                  = kDB::Repository::Transaction());
    void dump();
    /**
     * Set the flags used by default when creating a new revision.
     */
    void setDefaultRevisionTags(Revision::Tags _tags);
    /**
     * Set (if \ref _on is true) or unset (if \ref _on is false) the flags used by default when
     * creating a new revision.
     */
    void setDefaultRevisionTag(Revision::Tag _tag, bool _on = true);
    /**
     * \return the flags used by default when creating a new revision.
     */
    Revision::Tags defaultRevisionTags() const;
  private:
    cres_qresult<void> applyDelta(const kDB::Repository::Transaction& _transaction,
                                  const Delta& _delta, TripleStore* _store, bool _reverse);
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::VersionControl
