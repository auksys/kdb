#include "DeltaBuilder.h"

#include <knowRDF/Triple.h>
#include <knowRDF/TripleStore.h>

namespace kDB::Repository::VersionControl
{

  struct DeltaBuilder::Private
  {
    bool recording_enabled = true; ///< used to disable recording, for instance if you want to use a
                                   ///< load or clear command

    knowRDF::TripleStore insertion, deletion;
  };
} // namespace kDB::Repository::VersionControl
