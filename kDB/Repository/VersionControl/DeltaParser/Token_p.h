/*
 *  Copyright (c) 2008,2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#ifndef _SML_TOKEN_H_
#define _SML_TOKEN_H_

#include <QString>
#include <sstream>

#include <knowCore/Curie.h>

namespace kDB::Repository::VersionControl::DeltaParser
{
  struct Token
  {
    /**
     * List of possible token type
     */
    enum Type
    {
      // Not really token
      UNFINISHED_STRING = -4,
      END_OF_FILE = -3,
      END_OF_LINE = -2,
      UNKNOWN = -1,
      // Special characters
      SEMI = 0,        ///< ;
      COLON,           ///< :
      COMA,            ///< ,
      DOT,             ///< .
      STARTBRACE,      ///< {
      ENDBRACE,        ///< }
      STARTBRACKET,    ///< (
      ENDBRACKET,      ///< )
      STARTBOXBRACKET, ///< [
      ENDBOXBRACKET,   ///< ]
      EQUAL,           ///< =
      QUESTION,        /// < ?
      UNDERSCORECOLON, ///< _:
      CIRCUMFLEXCIRCUMFLEX,
      // Constants
      FLOAT_CONSTANT,
      INTEGER_CONSTANT,
      STRING_CONSTANT,
      URI_CONSTANT,
      BINDING,
      IDENTIFIER,
      LANG_TAG,
      CURIE_CONSTANT,
      // Keywords
      DELETE,
      DATA,
      INSERT,
      // Needed for reusing BaseParser
      A,
      TRUE,
      FALSE,
      LOAD_FILE
    };
    /// type of the token
    Type type;
    /// line of the token
    int line;
    /// Column of the token
    int column;
    /// String or identifier name
    QString string;
    // Curie
    knowCore::Curie curie;
    Token();
    Token(const knowCore::Curie& _curie, int _line, int _column);
    /**
     * Creates a token of the given type
     */
    Token(Type _type, int _line, int _column);
    /**
     * Creates an identifier or a strstring constant or a number constant
     */
    Token(Type _type, const QString& _string, int _line, int _column);
    bool isExpressionTerminal();
    bool isConstant() const;
    bool isPrimary() const;
    static QString typeToString(Type);
    QString toString() const;
  };
} // namespace kDB::Repository::VersionControl::DeltaParser

#include <knowCore/Formatter.h>

clog_format_declare_formatter(kDB::Repository::VersionControl::DeltaParser::Token::Type)
{
  return format_to(ctx.out(), "{}", p);
}

#endif
