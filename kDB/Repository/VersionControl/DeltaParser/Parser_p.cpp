#include "Parser_p.h"

#include <QBuffer>

#include <knowCore/TypeDefinitions.h>

#include "Lexer_p.h"
#include <knowRDF/Turtle/BaseParser_p.h>

using namespace kDB::Repository::VersionControl::DeltaParser;

struct Parser::PrivateBase
{
  QList<knowRDF::Triple> triples;

  void appendTriple(const knowRDF::Triple& _triple) { triples.append(_triple); }
};

struct Parser::Private : public knowRDF::Turtle::BaseParser<PrivateBase, Lexer, Token, true>
{
  void parseTriples();
};

void Parser::Private::parseTriples()
{
  getNextToken();
  while(currentToken.type != Token::ENDBRACE and currentToken.type != Token::END_OF_FILE)
  {
    isOfType(currentToken, Token::URI_CONSTANT);
    knowCore::Uri subject_uri = currentToken.string;
    getNextToken();
    isOfType(currentToken, Token::URI_CONSTANT);
    knowCore::Uri predicate_uri = currentToken.string;
    getNextToken();

    parseObject(subject_uri, predicate_uri);
    isOfType(currentToken, Token::DOT);
    getNextToken();
  }
  isOfType(currentToken, Token::ENDBRACE);
  getNextToken();
  isOfType(currentToken, Token::SEMI);
  getNextToken();
}

Parser::Parser(Lexer* _lexer) : d(new Private) { d->lexer = _lexer; }

Parser::~Parser() { delete d; }

const knowCore::Messages& Parser::messages() const { return d->messages; }

Parser::Result Parser::parse()
{
  d->lexer->setCurieLexingEnabled(false);
  d->getNextToken();

  Parser::Result r;

  if(d->currentToken.type == Token::DELETE)
  {
    if(d->isOfType(d->getNextToken(), Token::DATA)
       and d->isOfType(d->getNextToken(), Token::STARTBRACE))
    {
      d->parseTriples();
      r.deleted = d->triples;
      d->triples.clear();
    }
  }
  if(d->currentToken.type == Token::INSERT)
  {
    if(d->isOfType(d->getNextToken(), Token::DATA)
       and d->isOfType(d->getNextToken(), Token::STARTBRACE))
    {
      d->parseTriples();
      r.inserted = d->triples;
      d->triples.clear();
    }
  }
  d->isOfType(d->currentToken, Token::END_OF_FILE);
  return r;
}

Parser::Result Parser::parse(const QByteArray& _query, knowCore::Messages* _messages)
{
  Lexer l(QString::fromUtf8(_query));
  Parser p(&l);
  Result r = p.parse();
  r.valid = not p.messages().hasErrors();
  *_messages = p.messages();
  return r;
}
