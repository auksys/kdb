#include <QVariantHash>

#include <knowRDF/Triple.h>

namespace kDB::Repository::VersionControl::DeltaParser
{
  class Lexer;
  class Parser
  {
  public:
    struct Result
    {
      bool valid;
      QList<knowRDF::Triple> inserted, deleted;
    };
  public:
    Parser(Lexer* _lexer);
    ~Parser();
    Result parse();
    const knowCore::Messages& messages() const;
    static Result parse(const QByteArray& _query, knowCore::Messages* _messages);
  private:
    struct PrivateBase;
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::VersionControl::DeltaParser
