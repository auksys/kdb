/*
 *  Copyright (c) 2008,2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 */

class QIODevice;

#include "Token_p.h"
#include <knowCore/LexerTextStream.h>

namespace kDB::Repository::VersionControl::DeltaParser
{
  class Lexer
  {
  public:
    Lexer(const QString& _string);
    ~Lexer();
  public:
    void setCurieLexingEnabled(bool _v);
    bool isCurieLexingEnabled() const;
    Token nextToken();
  protected:
    /**
     * Get an identifier (or keyword) in the current flow of character.
     */
    QString getIdentifier(knowCore::LexerTextStream::Element lastChar);
    Token getDigit(knowCore::LexerTextStream::Element lastChar);
    Token getString(int terminator, Token::Type _type, bool _tripleEnding);
    bool isTriple(const QString& _char);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::VersionControl::DeltaParser
