#include "Revision.h"

#include <kDB/Repository/Connection.h>

#include <QUuid>
#include <knowCore/Timestamp.h>

namespace kDB::Repository::VersionControl
{
  struct Revision::Private
  {
    Connection connection;
    QByteArray hash, content_hash;
    int historicity;
    int revisionId;
    QString store_table_name;
    Tags tags;
    int distance_to_root;
  };
} // namespace kDB::Repository::VersionControl
