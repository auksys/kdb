#include "Delta.h"

#include <QList>

#include "Signature.h"

namespace kDB::Repository::VersionControl
{
  struct Delta::Private : public QSharedData
  {
    QByteArray parent, child, hash, delta;
    QList<Signature> signatures;
  };
} // namespace kDB::Repository::VersionControl
