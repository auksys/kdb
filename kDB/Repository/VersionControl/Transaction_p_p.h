#include "Transaction_p.h"

#include "../Transaction_p.h"
#include "../TripleStore.h"
#include "Revision.h"

namespace kDB::Repository::VersionControl
{
  struct Transaction::Private
  {
    Private(const TripleStore& _store, const Repository::Transaction& _transaction);
    ~Private();
    TripleStore store;
    QWeakPointer<Repository::Transaction::Private> transaction;
    bool commited = true;
    Revision::Tags tags;
  };
} // namespace kDB::Repository::VersionControl
