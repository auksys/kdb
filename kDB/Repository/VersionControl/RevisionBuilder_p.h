#include "RevisionBuilder.h"

#include <QUuid>
#include <knowCore/Timestamp.h>

#include "Delta.h"

#include <clog_qt>
#include <kDB/Repository/Connection.h>
#include <kDB/Repository/TripleStore.h>

#include <Cyqlops/Crypto/RSAAlgorithm.h>

namespace kDB::Repository::VersionControl
{
  struct RevisionBuilder::Private
  {
    ~Private()
    {
      if(not inserted)
      {
        clog_warning("Revision built but not inserted or discarded");
      }
    }
    bool inserted = false;

    kDB::Repository::Connection connection;
    QString triplesStoreTableName;
    qint64 historicity = 0;
    QByteArray content_hash, revision_hash;
    QList<Delta> deltas;

    Revision::Tags tags;

    static bool simulate_v1;
  };
  inline void internal::set_simulate_v1(bool _v) { RevisionBuilder::Private::simulate_v1 = _v; }
} // namespace kDB::Repository::VersionControl
