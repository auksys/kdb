#include <kDB/Forward.h>

namespace kDB::Repository::VersionControl
{
  class DeltaBuilder
  {
  public:
    DeltaBuilder();
    ~DeltaBuilder();
    bool isEmpty() const;
    QByteArray delta() const;
    void reportInsertion(const knowRDF::Triple& _triple);
    void reportInsertion(const QList<knowRDF::Triple>& _triple);
    void reportRemoval(const knowRDF::Triple& _triple);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::VersionControl
