#include <QUuid>

#include <Cyqlops/Crypto/Hash.h>
#include <Cyqlops/Crypto/RSAAlgorithm.h>

#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>

#include <kDB/Repository/DatabaseInterface/PostgreSQL/SQLInterface_p.h>
#include <kDB/Repository/TripleStore_p.h>

namespace kDB::Repository::VersionControl::Utils
{
  inline QByteArray computeDeltaHash(const QByteArray& _diff)
  {
    return Cyqlops::Crypto::Hash::md5(_diff);
  }
  inline QByteArray computeRevisionHash(const QByteArray& _content_hash, int _historicity)
  {
    return Cyqlops::Crypto::Hash::md5(_content_hash, _historicity);
  }

  inline QByteArray computeSignature(const Cyqlops::Crypto::RSAAlgorithm& _signature_algortihm,
                                     const QUuid& _author, const knowCore::Timestamp& _time,
                                     const QByteArray& _hash)
  {
    QByteArray hash = Cyqlops::Crypto::Hash::md5(
      _author.toByteArray(), _time.toEpoch<knowCore::NanoSeconds>().count(), _hash);
    return _signature_algortihm.sign(hash, Cyqlops::Crypto::Hash::Algorithm::RAW);
  }

  struct MetaVersion
  {
    static inline void setHead(const kDB::Repository::Transaction& _transaction,
                               QSharedPointer<TripleStore::Definition> _definition, int _revisionId,
                               const QByteArray& _hash)
    {
      knowCore::ValueHash new_head_map;
      new_head_map.insert("id", _revisionId);
      new_head_map.insert("hash", QString::fromLatin1(_hash.toHex()));
      _definition->setMeta(QStringList() << "versioning" << "head",
                           knowCore::Value::fromValue(new_head_map), _transaction);
    }
    static inline cres_qresult<QByteArray>
      getHead(const QueryConnectionInfo& _connection_info,
              QSharedPointer<TripleStore::Definition> _definition)
    {
      cres_try(knowCore::Value hash_var, DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
                                           _connection_info, _definition->name,
                                           QStringList() << "versioning" << "head" << "hash"));
      cres_try(QString hash_str, hash_var.value<QString>());
      return cres_success(QByteArray::fromHex(hash_str.toLatin1()));
    }
  };
} // namespace kDB::Repository::VersionControl::Utils
