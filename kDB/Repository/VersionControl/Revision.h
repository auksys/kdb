#pragma once

#include <QFlags>
#include <QSharedPointer>

class QByteArray;

template<typename T>
class QList;

#include <kDB/Repository/Transaction.h>

namespace kDB::Repository::VersionControl
{
  class Revision
  {
    friend class Manager;
    friend class RevisionBuilder;
    friend class Transaction;
    friend class Repository::DatabaseInterface::PostgreSQL::SQLInterface;
    friend class Repository::TripleStore;
  public:
    enum class Tag
    {
      Private
      = 0x1, ///< indicates that the revision can be edited or moved and should not be propagated
      Editable = 0x2 ///< indicates that the revision can be edited
    };
    Q_DECLARE_FLAGS(Tags, Tag)
  public:
    Revision();
    Revision(const Revision& _rhs);
    Revision& operator=(const Revision& _rhs);
    ~Revision();
    bool isValid() const;
    QByteArray hash() const;
    /**
     * @return the hash of the revisions, which is the combination of the sorted hash.
     */
    QByteArray contentHash() const;
    int historicity() const;
    QList<Delta> deltas(const kDB::Repository::Transaction& _transaction
                        = kDB::Repository::Transaction()) const;
    Delta deltaFrom(const QByteArray& _parent) const;
    /**
     * @return the list of children
     */
    cres_qresult<QList<Revision>> children(const kDB::Repository::Transaction& _transaction
                                           = kDB::Repository::Transaction()) const;
    /**
     * @return tags associated with the revision
     */
    Tags tags() const;
    /**
     * @return true if one of the delta is signed by @p _author
     */
    bool isSignedBy(const QUuid& _author) const;
    bool operator==(const Revision& _revision) const;
    /**
     * @return the hash used for the first revision
     */
    static QByteArray initialHash();
  private:
    struct Private;
    QSharedPointer<Private> d;
  };
} // namespace kDB::Repository::VersionControl
