#include "RevisionBuilder_p.h"

#include "../QueryConnectionInfo.h"
#include "../Transaction.h"

#include "Connection.h"
#include "Delta_p.h"
#include "Revision_p.h"
#include "Utils_p.h"

using namespace kDB::Repository::VersionControl;

bool RevisionBuilder::Private::simulate_v1 = false;

RevisionBuilder::RevisionBuilder() : d(new Private) {}

RevisionBuilder::RevisionBuilder(const RevisionBuilder& _rhs) : d(_rhs.d) {}

RevisionBuilder& RevisionBuilder::operator=(const RevisionBuilder& _rhs)
{
  d = _rhs.d;
  return *this;
}

RevisionBuilder::~RevisionBuilder() {}

RevisionBuilder& RevisionBuilder::setMetaInformation(const QByteArray& _hash, qint64 _historicity)
{
  d->historicity = _historicity;
  d->revision_hash = _hash;
  return *this;
}

RevisionBuilder& RevisionBuilder::setTags(Revision::Tags _tags)
{
  d->tags = _tags;
  return *this;
}

RevisionBuilder& RevisionBuilder::addDelta(const QByteArray& _parent, const QByteArray& _hash,
                                           const QByteArray& _delta,
                                           const QList<Signature>& _signatures)
{
  Delta delta;
  delta.d->parent = _parent;
  delta.d->hash = _hash;
  // delta.d->child  = d->hash;
  delta.d->delta = _delta;
  delta.d->signatures = _signatures;
  d->deltas.append(delta);
  return *this;
}

RevisionBuilder& RevisionBuilder::addDelta(const QByteArray& _parent, const QByteArray& _delta,
                                           bool _sign)
{
  Delta delta;
  delta.d->parent = _parent;
  delta.d->hash = Utils::computeDeltaHash(_delta);
  delta.d->delta = _delta;
  if(_sign)
  {
    delta.sign(d->connection.serverUuid(), d->connection.rsaAlgorithm());
  }
  d->deltas.append(delta);
  return *this;
}

cres_qresult<Revision> RevisionBuilder::insert(Repository::Transaction _transaction,
                                               bool _notify_revision)
{
  clog_assert(not d->inserted);
  clog_assert(not d->content_hash.isEmpty());
  d->inserted = true;
  Revision r;
  r.d->content_hash = d->content_hash;
  r.d->store_table_name = d->triplesStoreTableName;

  // Compute historicity and minimum distance to root
  int min_distance_to_root = std::numeric_limits<int>::max();

  if(RevisionBuilder::Private::simulate_v1)
  {
    int computed_historicity = 0;

    for(const Delta& delta : d->deltas)
    {
      cres_try(Revision parent_rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                                      _transaction, d->triplesStoreTableName, delta.parent()));
      min_distance_to_root = std::min(parent_rev.d->distance_to_root, min_distance_to_root);
      computed_historicity = std::max(computed_historicity, parent_rev.historicity());
    }
    ++computed_historicity;
    if(d->historicity == 0)
    {
      d->historicity = computed_historicity;
    }
  }
  else
  {
    int computed_historicity = 0;

    for(const Delta& delta : d->deltas)
    {
      cres_try(Revision parent_rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                                      _transaction, d->triplesStoreTableName, delta.parent()));
      if(parent_rev.d->distance_to_root < min_distance_to_root)
      {
        min_distance_to_root = parent_rev.d->distance_to_root;
        computed_historicity = parent_rev.historicity();
      }
      else if(parent_rev.d->distance_to_root == min_distance_to_root)
      {
        computed_historicity = std::min(computed_historicity, parent_rev.historicity());
      }
    }
    if(d->deltas.size() == 1)
    {
      ++computed_historicity;
    }
    if(d->historicity == 0)
    {
      d->historicity = computed_historicity;
    }
  }
  r.d->historicity = d->historicity;
  r.d->hash = Utils::computeRevisionHash(d->content_hash, d->historicity);
  clog_assert(r.d->hash == d->revision_hash or d->revision_hash.isEmpty());

  // Insert revision

  // Check if the revision already exists

  cres_try(bool has_revision, DatabaseInterface::PostgreSQL::SQLInterface::hasRevision(
                                _transaction, d->triplesStoreTableName, r.d->hash));
  if(has_revision)
  {
    cres_try(Revision real_rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                                  _transaction, d->triplesStoreTableName, r.d->hash));
    r.d->revisionId = real_rev.d->revisionId;
  }
  else
  {
    cres_try(r.d->revisionId,
             DatabaseInterface::PostgreSQL::SQLInterface::createRevision(
               _transaction, d->triplesStoreTableName, r.d->hash, r.d->content_hash, d->historicity,
               d->tags, min_distance_to_root + 1));
  }

  // Insert deltas
  for(Delta& delta : d->deltas)
  {
    delta.d->child = r.d->hash;
    clog_assert(delta.isValid());

    // Retrieve parent_rev
    cres_try(Revision parent_rev, DatabaseInterface::PostgreSQL::SQLInterface::revision(
                                    _transaction, d->triplesStoreTableName, delta.parent()));
    clog_assert(parent_rev.isValid());
    clog_assert(parent_rev.hash() == delta.parent());

    // Check if the main revision already exists
    bool should_insert = true;
    if(has_revision)
    {
      cres_try(bool has_delta, DatabaseInterface::PostgreSQL::SQLInterface::hasDelta(
                                 _transaction, d->triplesStoreTableName, parent_rev.d->revisionId,
                                 r.d->revisionId));
      should_insert = not has_delta;
    }
    if(should_insert)
    {
      cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::recordDelta(
                              _transaction, d->triplesStoreTableName, parent_rev.d->revisionId,
                              r.d->revisionId, delta.hash(), delta.delta()));
    }
    // Add signatures
    clog_assert(delta.isSigned());
    for(const Signature& sign : delta.signatures())
    {
      cres_try(cres_ignore,
               DatabaseInterface::PostgreSQL::SQLInterface::recordDeltaSignature(
                 _transaction, d->triplesStoreTableName, parent_rev.d->revisionId, r.d->revisionId,
                 sign.author().toByteArray(), sign.timestamp(), sign.signature()));
    }
  }
  if(_notify_revision)
  {
    DatabaseInterface::PostgreSQL::SQLInterface::notifyNewRevision(
      _transaction, d->triplesStoreTableName, r.d->hash);
  }
  return cres_success(r);
}

void RevisionBuilder::discard() { d->inserted = true; }
