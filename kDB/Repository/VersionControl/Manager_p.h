#include "Manager.h"

#include <QMutex>

#include <kDB/Repository/Connection.h>

namespace kDB::Repository::VersionControl
{
  struct Manager::Private
  {
    QMutex mutex;
    QList<QMetaObject::Connection> connections;
    kDB::Repository::Connection connection;
    QSharedPointer<TripleStore::Definition> definition;
    Revision::Tags defaultRevisionTags;
    bool signingEnabled = true;
  };
} // namespace kDB::Repository::VersionControl
