#include <knowCore/Messages.h>
#include <knowCore/ValueHash.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>

#include <kDB/SPARQL/Algebra/NodeVisitor.h>
#include <kDB/SPARQL/Algebra/Utils.h>
#include <kDB/SPARQL/Query.h>

#include "QueryCache_p.h"

namespace kDB::Repository::VersionControl
{
  enum class TripleDeltaStatus
  {
    Skip,
    Removed,
    Added,
    Unknown
  };

  using TriplesToStatus = QHash<knowRDF::Triple, TripleDeltaStatus>;

  struct BuildAccumulatedDiff
  {
    TriplesToStatus diff;

    TripleDeltaStatus& valueRef(const knowRDF::Triple& _triple, TripleDeltaStatus _status)
    {
      TriplesToStatus::iterator it = diff.find(_triple);
      if(it == diff.end())
      {
        TripleDeltaStatus& s = diff[_triple];
        s = _status;
        return s;
      }
      else
      {
        return it.value();
      }
    }
    bool execute(const QByteArray& _query)
    {
      if(_query.isEmpty())
        return true;
      DeltaParser::Parser::Result r = QueryCache::get(_query);
      if(r.deleted.isEmpty() and r.inserted.isEmpty())
      {
        clog_error("Error in default merge strategy query {} is invalid!",
                   QString::fromUtf8(_query));
        return false;
      }
      for(const knowRDF::Triple& t : r.inserted)
      {
        TripleDeltaStatus& status = valueRef(t, TripleDeltaStatus::Skip);
        switch(status)
        {
        case TripleDeltaStatus::Skip:
          status = TripleDeltaStatus::Added;
          break;
        case TripleDeltaStatus::Removed:
          status = TripleDeltaStatus::Skip;
          break;
        case TripleDeltaStatus::Added:
          clog_warning("Triple added twice: {}", t);
          break;
        case TripleDeltaStatus::Unknown:
          clog_fatal("Internal error!");
        }
      }
      for(const knowRDF::Triple& t : r.deleted)
      {
        TripleDeltaStatus& status = valueRef(t, TripleDeltaStatus::Skip);
        switch(status)
        {
        case TripleDeltaStatus::Skip:
          status = TripleDeltaStatus::Removed;
          break;
        case TripleDeltaStatus::Removed:
          clog_warning("Triple removed twice: {}", t);
          break;
        case TripleDeltaStatus::Added:
          status = TripleDeltaStatus::Skip;
          break;
        case TripleDeltaStatus::Unknown:
          clog_fatal("Internal error!");
        }
      }
      return true;
    }
  };

} // namespace kDB::Repository::VersionControl

#include <knowCore/Formatter.h>

clog_format_declare_enum_formatter(kDB::Repository::VersionControl::TripleDeltaStatus, Skip,
                                   Removed, Added, Unknown);
