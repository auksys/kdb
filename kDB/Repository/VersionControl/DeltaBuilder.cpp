#include "DeltaBuilder_p.h"

#include <QBuffer>

#include <clog_qt>
#include <knowCore/UriManager.h>
#include <knowCore/UrisRegistry.h>
#include <knowRDF/Serialiser.h>

#include "QueryCache_p.h"

using namespace kDB::Repository::VersionControl;

DeltaBuilder::DeltaBuilder() : d(new Private) {}

DeltaBuilder::~DeltaBuilder() { delete d; }

namespace
{
  QByteArray to_data(const QList<knowRDF::Triple>& _triples)
  {
    QBuffer buffer;
    buffer.open(QIODevice::WriteOnly);
    knowRDF::Serialiser serializer(&buffer, knowCore::UriManager());
    serializer.setSaveBlankNodeAsUri(true);
    serializer.serialise(_triples);
    buffer.close();
    return buffer.buffer();
  }
} // namespace

bool DeltaBuilder::isEmpty() const { return d->insertion.isEmpty() and d->deletion.isEmpty(); }

QByteArray DeltaBuilder::delta() const
{
  clog_assert(not isEmpty());
  if(d->insertion.isEmpty() and d->deletion.isEmpty())
    return QByteArray();
  QList<QByteArray> prefixes;
  QByteArray prolog;
  QByteArray body;

  QList<knowRDF::Triple> inserted, deleted;

  if(not d->deletion.isEmpty())
  {
    deleted = d->deletion.triples();
    body += "DELETE DATA {";
    body += to_data(deleted);
    body += "};";
  }
  if(not d->insertion.isEmpty())
  {
    inserted = d->insertion.triples();
    body += "INSERT DATA {";
    body += to_data(inserted);
    body += "};";
  }
  QByteArray q = prolog + "\n" + body;
  QueryCache::add(q, inserted, deleted);
  return q;
}

void DeltaBuilder::reportInsertion(const QList<knowRDF::Triple>& _triple)
{
  for(const knowRDF::Triple& t : _triple)
  {
    reportInsertion(t);
  }
}

void DeltaBuilder::reportInsertion(const knowRDF::Triple& _triple)
{
  if(d->deletion.hasTriple(_triple))
  {
    d->deletion.removeTriple(_triple);
  }
  else
  {
    d->insertion.triple(_triple);
  }
}

void DeltaBuilder::reportRemoval(const knowRDF::Triple& _triple)
{
  if(d->insertion.hasTriple(_triple))
  {
    d->insertion.removeTriple(_triple);
  }
  else
  {
    d->deletion.triple(_triple);
  }
}
