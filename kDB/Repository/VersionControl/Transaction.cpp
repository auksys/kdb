#include "Transaction_p_p.h"

#include <QVariant>
#include <knowCore/Timestamp.h>

#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>
#include <knowRDF/Triple.h>

#include "../Logging.h"
#include "../QueryConnectionInfo.h"
#include "../TripleStore_p.h"

#include "Delta.h"
#include "DeltaBuilder.h"
#include "Manager.h"
#include "RevisionBuilder.h"
#include "Revision_p.h"
#include "Utils_p.h"

using namespace kDB::Repository::VersionControl;

Transaction::Private::Private(const TripleStore& _store,
                              const Repository::Transaction& _transaction)
    : store(_store), transaction(_transaction.d)
{
  Repository::Transaction t(transaction);

  clog_assert(_transaction.isActive());
  DatabaseInterface::PostgreSQL::SQLInterface::lockTripleStore(t, store.tablename(), true);

  DatabaseInterface::PostgreSQL::SQLInterface::startListeningChanges(t, store);

  commited = false;
}

Transaction::Private::~Private() { clog_assert(commited); }

Transaction::Transaction() : d(nullptr) {}

Transaction::Transaction(const TripleStore& _store, const Repository::Transaction& _transaction)
    : d(new Private(_store, _transaction))
{
}

Transaction::Transaction(const Transaction& _rhs) : d(_rhs.d) {}

Transaction& Transaction::operator=(const Transaction& _rhs)
{
  d = _rhs.d;
  return *this;
}

Transaction::~Transaction() {}

cres_qresult<void> Transaction::commit()
{
  Repository::Transaction t(d->transaction);
  QList<std::tuple<QString, knowRDF::Triple>> changes
    = DatabaseInterface::PostgreSQL::SQLInterface::retrieveChanges(t, d->store);
  DatabaseInterface::PostgreSQL::SQLInterface::stopListeningChanges(t, d->store);

  // Generate delta
  DeltaBuilder builder;
  for(const std::tuple<QString, knowRDF::Triple>& change : changes)
  {
    QString change_type = std::get<0>(change);
    knowRDF::Triple triple = std::get<1>(change);

    if(change_type == "INSERT")
    {
      builder.reportInsertion(triple);
    }
    else if(change_type == "REMOVE")
    {
      builder.reportRemoval(triple);
    }
    else
    {
      qFatal("Unsupported change type: %s", qPrintable(change_type));
    }
  }

  if(not builder.isEmpty()) // Don't create a revision if the difference is empty
  {
    RevisionBuilder revisionBuilder = d->store.versionControlManager()->insertRevision(
      DatabaseInterface::PostgreSQL::SQLInterface::computeContentHash(t, d->store.tablename()));
    revisionBuilder.setTags(d->tags);
    cres_try(QByteArray tH, d->store.versionControlManager()->tipHash());
    revisionBuilder.addDelta(tH, builder.delta(), true);
    cres_try(Revision rev, revisionBuilder.insert(t, true), on_failure(rollback();),
             message("Failed to create a revision for delta: {} and tip {} with error {}",
                     builder.delta(), tH.toHex()));
    Utils::MetaVersion::setHead(
      t, static_cast<const TripleStore::Private*>(d->store.d.data())->definition, rev.d->revisionId,
      rev.hash());
  }
  d->commited = true;
  d->store = Repository::TripleStore();
  d = nullptr;
  return cres_success();
}

cres_qresult<void> Transaction::rollback()
{
  Repository::Transaction t(d->transaction);
  cres_try(cres_ignore,
           DatabaseInterface::PostgreSQL::SQLInterface::stopListeningChanges(t, d->store));
  d->commited = true;
  d->store = Repository::TripleStore();
  d = nullptr;
  return cres_success();
}
