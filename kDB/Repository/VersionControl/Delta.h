#ifndef _KDB_REPOSITORY_VERSIONCONTROL_DELTA_H_
#define _KDB_REPOSITORY_VERSIONCONTROL_DELTA_H_

#include <QSharedDataPointer>

#include <kDB/Forward.h>

class QUuid;

namespace Cyqlops::Crypto
{
  class RSAAlgorithm;
}

namespace kDB::Repository::VersionControl
{
  class Delta
  {
    friend class kDB::Repository::DatabaseInterface::PostgreSQL::SQLInterface;
    friend class DeltaBuilder;
    friend class RevisionBuilder;
  public:
    Delta();
    Delta(const Delta& _delta);
    Delta& operator=(const Delta& _delta);
    ~Delta();
  public:
    QByteArray parent() const;
    QByteArray child() const;
    QByteArray hash() const;
    QByteArray delta() const;
    bool isValid() const;
    bool operator==(const Delta& _rhs) const;
    Delta reverse() const;
    /**
     * @return true if signed by at least one agent.
     */
    bool isSigned() const;
    /**
     * List of agents that have signed this delta.
     */
    QList<Signature> signatures() const;
    /**
     * @return true if one of the delta is signed by @p _author
     */
    bool isSignedBy(const QUuid& _author) const;
    /**
     * Sign this delta.
     */
    void sign(const QUuid& _author, const Cyqlops::Crypto::RSAAlgorithm& _signature_algortihm);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace kDB::Repository::VersionControl

#endif
