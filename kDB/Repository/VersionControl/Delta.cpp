#include "Delta_p.h"

#include <knowCore/ValueHash.h>

#include <kDB/SPARQL/Algebra/Visitors/Inverter.h>
#include <kDB/SPARQL/Algebra/Visitors/Serialiser.h>
#include <kDB/SPARQL/Query.h>

#include "Utils_p.h"

using namespace kDB::Repository::VersionControl;

Delta::Delta() : d(new Private) {}

Delta::Delta(const Delta& _delta) : d(_delta.d) {}

Delta& Delta::operator=(const Delta& _delta)
{
  d = _delta.d;
  return *this;
}

Delta::~Delta() {}

QByteArray Delta::parent() const { return d->parent; }

QByteArray Delta::child() const { return d->child; }

QByteArray Delta::delta() const { return d->delta; }

QByteArray Delta::hash() const { return d->hash; }

QList<Signature> Delta::signatures() const { return d->signatures; }

bool Delta::isSigned() const { return not d->signatures.isEmpty(); }

bool Delta::isSignedBy(const QUuid& _author) const
{
  for(const Signature& sign : d->signatures)
  {
    if(sign.author() == _author)
    {
      return true;
    }
  }
  return false;
}

void Delta::sign(const QUuid& _author, const Cyqlops::Crypto::RSAAlgorithm& _signature_algortihm)
{
  knowCore::Timestamp time = knowCore::Timestamp::now();

  d->signatures.append(Signature(
    _author, time, Utils::computeSignature(_signature_algortihm, _author, time, d->hash)));
}

bool Delta::isValid() const
{
  return not(d->parent.isEmpty() or d->child.isEmpty() or d->hash.isEmpty());
}

Delta Delta::reverse() const
{
  Delta reversed;
  reversed.d->child = d->parent;
  reversed.d->parent = d->child;
  QStringList list_of_queries;
  for(kDB::SPARQL::Query q :
      kDB::SPARQL::Query::parse(d->delta, knowCore::ValueHash(), nullptr, knowCore::Uri()))
  {
    kDB::SPARQL::Algebra::NodeCSP node
      = kDB::SPARQL::Algebra::Visitors::Inverter::invert(q.getNode());
    list_of_queries.append(
      kDB::SPARQL::Algebra::Visitors::Serialiser::serialise(node, knowCore::ValueHash()).queryText);
  }
  reversed.d->delta = list_of_queries.join(";").toUtf8() + ";";
  reversed.d->hash = Utils::computeDeltaHash(reversed.d->delta);
  return reversed;
}

#define COMP(_FIELD_) d->_FIELD_ == _rhs.d->_FIELD_

bool Delta::operator==(const Delta& _rhs) const
{
  return COMP(hash) and COMP(parent) and COMP(child) and COMP(delta);
}
