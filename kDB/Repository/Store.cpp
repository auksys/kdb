#include "Store.h"

#include <libpq-fe.h>

#include <clog_qt>

#include <QBuffer>
#include <QDir>
#include <QProcess>
#include <QStandardPaths>

#include <clog_qt>
#include <knowCore/WhoAmI.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "Connection.h"
#include "Logging.h"

using namespace kDB::Repository;

namespace
{
#define TEST_FIND_PATH_TO_PG(_PATH_)                                                               \
  if(QDir(_PATH_).exists())                                                                        \
  {                                                                                                \
    return _PATH_;                                                                                 \
  }
  QString find_path_to_pg()
  {
    QByteArray env_path = qgetenv("KDB_POSTGRES_BIN");
    if(not env_path.isEmpty())
    {
      TEST_FIND_PATH_TO_PG(env_path);
    }

    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/16/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/15/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/14/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/13/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/12/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/11/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/10/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/9.6/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/9.5/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/9.4/bin/")
    TEST_FIND_PATH_TO_PG("/usr/lib/postgresql/9.3/bin/")
    clog_error("Could not find postgresql >= 9.3");
    return QString();
  }
} // namespace

QString path_to_pg = find_path_to_pg();

struct Store::Private
{
  QDir dir;
  int port;
  bool controling;

  cres_qresult<void> setConfiguration(const QString& _key, const QString& _value,
                                      const Connection& _connection);
};

QDir Store::standardDir(const SectionName& _section, const StoreName& _name)
{
  if(_section->isEmpty())
  {
    return clog_qt::qformat("{}/auKsys/5/kdb/stores/{}",
                            QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation),
                            _name->isEmpty() ? "default" : _name.get_value());
  }
  else
  {
    return clog_qt::qformat("{}/auKsys/5/kdb/stores/{}/{}",
                            QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation),
                            _section, _name->isEmpty() ? "default" : _name.get_value());
  }
}

QDir Store::standardDir(const StoreName& _name)
{
  return standardDir(SectionName(QString()), _name);
}

cres_qresult<void>
  kDB::Repository::Store::Private::setConfiguration(const QString& _key, const QString& _value,
                                                    const kDB::Repository::Connection& _connection)
{
  knowDBC::Query q = _connection.createSQLQuery("ALTER SYSTEM SET " + _key + " TO " + _value);
  KDB_REPOSITORY_EXECUTE_QUERY("Failed to setConfiguration", q, false);
  return cres_success();
}

Store::Store(const QDir& _storage, int _port) : d(new Private)
{
  d->dir = _storage;
  d->port = _port;
  d->controling = false;
}

Store::Store(const SectionName& _section, const StoreName& _store, int _port)
    : Store(standardDir(_section, _store), _port)
{
}

Store::Store(const StoreName& _store, int _port)
    : Store(standardDir(SectionName("default"), _store), _port)
{
}

Store::~Store()
{
  if(d->controling)
  {
    stop();
    stop(true);
  }
  delete d;
}

int Store::port() const { return d->port; }

QDir Store::directory() const { return d->dir; }

cres_qresult<void> Store::startIfNeeded()
{
  if(isRunning())
    return cres_success();
  else
    return start();
}

cres_qresult<void> Store::start()
{
  if(not d->dir.exists())
  {
    d->dir.mkpath(".");
  }
  if(not d->dir.exists("PG_VERSION"))
  {
    QStringList args;
    args << "-D" << d->dir.absolutePath() << "--auth-local=trust";
    QProcess process;
    process.start(path_to_pg + "initdb", args);
    process.waitForFinished();
    if(process.exitCode() != 0)
    {
      clog_error("Failed to start {}/initdb (with arguments '\"{}\"') in '{}' got exit code '{}'",
                 path_to_pg, d->dir.path(), args.join("\" \""), process.exitCode());
      clog_error("Standard output:\n{}\n", QString::fromLocal8Bit(process.readAllStandardOutput()));
      clog_error("Standard error:\n{}\n", QString::fromLocal8Bit(process.readAllStandardError()));
      return cres_failure("Failed to start initdb process, check logs.");
    }
    // Set the permissions
    QFile file(d->dir.absoluteFilePath("pg_hba.conf"));
    file.open(QIODevice::ReadOnly);
    QByteArray ba = file.readAll();
    file.close();
    file.open(QIODevice::WriteOnly);
    QBuffer babuf(&ba);
    babuf.open(QIODevice::ReadOnly);
    while(not babuf.atEnd())
    {
      QByteArray line = babuf.readLine();
      if(line[0] != '#')
      {
        file.write("#");
      }
      file.write(line);
    }
    file.write("\n#Only allow current user to connect with a Unix socket\n");
    file.write(qPrintable(
      clog_qt::qformat("local   all             {}                                     trust\n",
                       knowCore::whoAmI())));
    file.write(qPrintable(
      QString("local   all             kdb                                    trust\n")));
    file.close();

    //     QFile file2(d->dir.absoluteFilePath("postgresql.conf"));
    //     file2.open(QIODevice::Append);
    //     file2.write("synchronous_commit = off\n");
    //     file2.write("random_page_cost = 1\n");
  }
  QStringList args;

  QString options
    = clog_qt::qformat("-p {} -c unix_socket_directories={}", d->port, d->dir.absolutePath());
  args << "-D" << d->dir.absolutePath() << "-o" << options << "-w" << "start";
  QProcess process;
  process.start(path_to_pg + "pg_ctl", args);
  process.waitForFinished();
  if(process.exitCode() == 0)
  {
    d->controling = true;
    // Make sure the database is initialised
    Connection c = createConnection();
    auto const& [s, err] = c.connect(true);
    if(s)
    {
      d->setConfiguration("synchronous_commit", "off", c);
      d->setConfiguration("random_page_cost", "1", c);
      return restart(false);
    }
    else
    {
      clog_error("Failed to initialise store with error: {}", err.value());
      return cres_forward_failure(err);
    }
  }
  else
  {
    d->controling = false;
    clog_error("Failed to start {}/pg_ctl (with arguments '\"{}\"') in '{}' got exit code '{}'",
               path_to_pg, d->dir.path(), args.join("\" \""), process.exitCode());
    clog_error("Standard output:\n{}\n", QString::fromLocal8Bit(process.readAllStandardOutput()));
    clog_error("Standard error:\n{}\n", QString::fromLocal8Bit(process.readAllStandardError()));
    return cres_failure("Failed to run pg_control process, check logs.");
  }
}

cres_qresult<void> Store::stop(bool _force)
{
  QStringList args;
  if(_force)
  {
    args << "-m" << "immediate";
  }
  args << "-D" << d->dir.absolutePath() << "stop";
  bool v = QProcess::execute(path_to_pg + "pg_ctl", args) == 0;
  if(v)
  {
    d->controling = false;
    return cres_success();
  }
  else
  {
    return cres_failure("Failed to stop store.");
  }
}

cres_qresult<void> Store::restart(bool _force)
{
  QStringList args;
  if(_force)
  {
    args << "-m" << "immediate";
  }
  args << "-D" << d->dir.absolutePath() << "restart";
  bool v = QProcess::execute(path_to_pg + "pg_ctl", args) == 0;
  if(v)
  {
    return cres_success();
  }
  else
  {
    return cres_failure("Failed to restart store");
  }
}

cres_qresult<void> Store::erase()
{
  if(isRunning())
  {
    cres_try(cres_ignore, stop(true));
  }
  return cres_cond<void>(d->dir.removeRecursively(), "Failed to remove directory {}",
                         d->dir.absolutePath());
}

Connection Store::createConnection() const
{
  return Connection::create(HostName(d->dir.absolutePath()), d->port);
}

bool Store::isRunning() const
{
  QString conninfo
    = clog_qt::qformat("host={} port={} dbname=template1 user=kdb", d->dir.absolutePath(), d->port);
  return PQping(qPrintable(conninfo)) == PQPING_OK;
}

void Store::autoSelectPort()
{
  QStringList l = d->dir.entryList({".s.PGSQL.*.lock"}, QDir::Hidden | QDir::Files);
  if(l.size() == 1)
  {
    QRegularExpression r(".s.PGSQL.(\\d*).lock");
    QRegularExpressionMatch match = r.match(l[0]);
    if(match.hasMatch())
    {
      d->port = match.captured(1).toInt();
    }
    else
    {
      clog_error("Failed to extract port from filename '{}'", l[0]);
    }
  }
}

cres_qresult<void> Store::setConfiguration(const QString& _key, const QString& _value,
                                           bool _restart_server)
{
  return setConfiguration({{_key, _value}}, _restart_server);
}

cres_qresult<void> Store::setConfiguration(const QList<QPair<QString, QString>>& _values,
                                           bool _restart_server)
{
  bool was_running = isRunning();
  if(not was_running)
  {
    cres_try(cres_ignore, start());
  }
  Connection c = createConnection();
  auto const& [s_conn, m_conn] = c.connect();
  if(not s_conn)
  {
    if(not was_running)
      stop();
    return cres_failure("Failed to connect to database: {}", m_conn.value());
  }
  for(const QPair<QString, QString>& v : _values)
  {
    cres_try(cres_ignore, d->setConfiguration(v.first, v.second, c),
             on_failure(if(not was_running) stop();),
             message("Failed to set configuration {} {} with error {}", v.first, v.second));
  }
  if(was_running)
  {
    if(_restart_server)
    {
      cres_try(cres_ignore, stop());
      cres_try(cres_ignore, start());
    }
  }
  else
  {
    cres_try(cres_ignore, stop());
  }

  return cres_success();
}

bool Store::isControlling() const { return d->controling; }

void Store::detach() { d->controling = false; }
