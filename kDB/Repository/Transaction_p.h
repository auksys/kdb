#include "ConnectionHandle_p.h"
#include "Transaction.h"

#include "VersionControl/Transaction_p.h"

namespace kDB
{
  namespace Repository
  {
    struct Transaction::Private
    {
      Private(const Connection& _cp) : handle(_cp), connection(_cp) {}
      ~Private();
      ConnectionHandle handle;
      Connection connection;
      QHash<QString, VersionControl::Transaction> triplesStoreTransactions;
      QList<std::function<void()>> executeOnSuccessfulCommit;
      cres_qresult<void> commit();
      cres_qresult<void> rollback();
    };
  } // namespace Repository
} // namespace kDB
