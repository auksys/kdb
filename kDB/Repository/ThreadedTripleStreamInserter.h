#include <QSharedPointer>
#include <knowRDF/TripleStreamListener.h>

namespace kDB
{
  namespace Repository
  {
    class TripleStore;
    class ThreadedTripleStreamInserter : public knowRDF::TripleStreamListener
    {
    public:
      ThreadedTripleStreamInserter(const TripleStore& _triplesStore,
                                   int _max_queue_size = std::numeric_limits<int>::max());
      virtual ~ThreadedTripleStreamInserter();
      virtual void triple(const knowRDF::Triple& _triple);
      void waitForFinished();
    private:
      struct Private;
      Private* const d;
    };
  } // namespace Repository
} // namespace kDB
