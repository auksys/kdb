#include "TemporaryStore.h"

#include <clog_qt>
#include <cres_clog>

#include <QRandomGenerator>
#include <QTemporaryDir>
#include <QTime>

#include "Connection.h"
#include "Store.h"

using namespace kDB::Repository;

struct TemporaryStore::Private
{
  Store* store = nullptr;
  QTemporaryDir dir;
};

TemporaryStore::TemporaryStore() : d(new Private) {}

TemporaryStore::~TemporaryStore()
{
  stop(true);
  delete d->store;
  delete d;
}

Connection TemporaryStore::createConnection() const
{
  return d->store ? d->store->createConnection() : Connection();
}

cres_qresult<void> TemporaryStore::start()
{
  QDir dir_tmp(d->dir.path());
  int port;

  QRandomGenerator rng(QTime::currentTime().msec());

  for(std::size_t i = 0; i < 100; ++i)
  {
    dir_tmp.removeRecursively();
    port = rng.bounded(5000, 6000);
    d->store = new Store(dir_tmp, port);
    if(d->store->start().is_successful())
    {
      break;
    }
    else
    {
      delete d->store;
      d->store = nullptr;
    }
  }
  if(d->store)
  {
    clog_info("Created a temporary store at {}:{}", dir_tmp.dirName(), port);
    return cres_success();
  }
  else
  {
    return cres_log_failure("Failed to create a temporary store in {}", dir_tmp.dirName());
  }
}

Store* TemporaryStore::store() const { return d->store; }

cres_qresult<void> TemporaryStore::stop(bool _force)
{
  if(d->store)
  {
    cres_try(cres_ignore, d->store->stop(_force));
    delete d->store;
    d->store = nullptr;
  }
  return cres_success();
}

void TemporaryStore::setAutoRemove(bool _v) { d->dir.setAutoRemove(_v); }
