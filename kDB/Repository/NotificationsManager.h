#include <QThread>

#include <functional>

namespace kDB
{
  namespace Repository
  {
    class Connection;
    class NotificationsManager : public QThread
    {
      friend class Connection;
      NotificationsManager();
      ~NotificationsManager();
      void start(void* _connection);
      void stop();
    public:
      QMetaObject::Connection listen(const char* _channel, const QObject* receiver,
                                     const char* member,
                                     Qt::ConnectionType _type = Qt::AutoConnection);
      QMetaObject::Connection listen(const char* _channel,
                                     const std::function<void(const QByteArray&)>& _receiver);
      bool unlisten(const char* _channel, const QObject* receiver, const char* member);
      bool unlisten(const QMetaObject::Connection& _connection);
    protected:
      void run() override;
    private:
      struct Private;
      Private* d;
    };
  } // namespace Repository
} // namespace kDB
