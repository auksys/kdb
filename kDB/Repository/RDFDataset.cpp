#include "RDFDataset_p.h"

#include <knowCore/Uri.h>
#include <knowDBC/Query.h>

#include "DatasetsUnion.h"
#include "PersistentDatasetsUnion.h"
#include "TripleStore.h"
#include "TriplesView.h"

using namespace kDB::Repository;

RDFDataset::Private::~Private() {}

bool RDFDataset::Private::isValid() const { return false; }

knowCore::Uri RDFDataset::Private::uri() const { return knowCore::Uri(); }

RDFDataset::RDFDataset() : d(new Private(Type::Invalid)) {}

RDFDataset::RDFDataset(RDFDataset::Private* _private) : d(_private) {}

RDFDataset::RDFDataset(const RDFDataset& _rhs) : d(_rhs.d) {}

RDFDataset& RDFDataset::operator=(const RDFDataset& _rhs)
{
  d = _rhs.d;
  return *this;
}

RDFDataset::~RDFDataset() {}

RDFDataset::Type RDFDataset::type() const { return d->type; }

bool RDFDataset::isValid() const { return d->isValid(); }

DatasetsUnion RDFDataset::toDatasetsUnion() const
{
  DatasetsUnion tgu;
  switch(d->type)
  {
  case Type::DatasetsUnion:
    tgu.d = d;
    break;
  case Type::PersistentDatasetsUnion:
  {
    PersistentDatasetsUnion pdu;
    pdu.d = d;
    tgu = pdu.toDatasetsUnion();
    break;
  }
  case Type::Invalid:
  case Type::Empty:
  case Type::TripleStore:
  case Type::TriplesView:
    break;
  }
  return tgu;
}

PersistentDatasetsUnion RDFDataset::toPersistentDatasetsUnion() const
{
  PersistentDatasetsUnion pdu;
  if(d->type == Type::PersistentDatasetsUnion)
  {
    pdu.d = d;
  }
  return pdu;
}

TripleStore RDFDataset::toTripleStore() const
{
  TripleStore tgu;
  if(d->type == Type::TripleStore)
  {
    tgu.d = d;
  }
  return tgu;
}

TriplesView RDFDataset::toTriplesView() const
{
  TriplesView tgu;
  if(d->type == Type::TriplesView)
  {
    tgu.d = d;
  }
  return tgu;
}

Connection RDFDataset::connection() const { return d->connection; }

knowCore::Uri RDFDataset::uri() const { return d->uri(); }

knowDBC::Query RDFDataset::createSPARQLQuery(const RDFEnvironment& _environment,
                                             const QString& _query,
                                             const knowCore::ValueHash& _bindings,
                                             const knowCore::ValueHash& _options) const
{
  return d->connection.createSPARQLQuery(RDFEnvironment(_environment).setDefaultDataset(*this),
                                         _query, _bindings, _options);
}
