#include "Connection_p.h"

#include "DatabaseInterface/PostgreSQL/postgresql_p.h"

#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QLibrary>

#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Unit.h>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Object.h>
#include <knowRDF/Skolemisation.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Connection.h>
#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/askcore_graph.h>
#include <knowCore/Uris/dul.h>
#include <knowCore/Uris/xsd.h>

#include <kDB/SPARQL/Logging.h>

#include "ConnectionHandle_p.h"
#include "GraphsManager.h"
#include "Logging.h"
#include "NotificationsManager.h"
#include "SPARQLFunctionsManager.h"
#include "Store.h"
#include "TripleStore.h"

#include "SPARQLExecution/QueryExecutor.h"

#include <kDB/Repository/ActiveRecord/Version.h>

#include "DatabaseInterface/PostgreSQL/BinaryInterface_write_p.h"
#include "DatabaseInterface/PostgreSQL/BinaryMarshalsRegistry.h"
#include "DatabaseInterface/PostgreSQL/RDFTermBinaryMarshal.h"
#include "DatabaseInterface/PostgreSQL/RDFValueBinaryMarshal.h"
#include "DatabaseInterface/PostgreSQL/SQLInterface_p.h"
#include "DatabaseInterface/PostgreSQL/SQLQueryExecutor.h"

#include "RDF/FocusNodeCollection.h"

using namespace kDB::Repository;

using askcore_graph = knowCore::Uris::askcore_graph;

inline void kdb_repository_init_repository_data() { Q_INIT_RESOURCE(repository_data); }

void Connection::Private::addOid(Oid _oid, const QString& _name) const
{
  oids2name[_oid] = _name;
  name2oid[_name] = _oid;
}

Connection::Private::Private()
{
  addOid(NAMEOID, "name");
  addOid(TEXTOID, "text");
  addOid(INT2OID, "int2");
  addOid(INT4OID, "int4");
  addOid(INT8OID, "int8");
}

Connection::Private::~Private()
{
  clog_assert(self_p_count == 0);
  clog_assert(connections.isEmpty());
  delete graphsManager;
  delete sparqlFunctionsManager;
  delete notificationsManager;
  delete rdf_term_marshal;
  delete rdf_value_marshal;
  delete krQueryEngine;
}

void Connection::Private::disconnect()
{
  notificationsManager->stop();
  for(const std::function<void()>& f : disconnection_listener.values())
  {
    f();
  }
  QMutexLocker l(&connections_lock);
  qDeleteAll(extensionObjects.values());
  extensionObjects.clear();
  if(available_connections.size() != connections.size())
  {
    clog_warning("Some connections are still active");
  }
  for(PGconn* conn : connections)
  {
    PQfinish(conn);
  }
  available_connections.clear();
  connections.clear();
  query_factories.clear();
}

cres_qresult<QString> Connection::Private::oidToName(Oid _oid) const
{
  QMutexLocker l(&metainformation_lock);
  if(not oids2name.contains(_oid))
  {
    l.unlock();
    knowDBC::Query q
      = Connection(self_p).createSQLQuery("SELECT typname FROM pg_type WHERE oid=:oid;");
    q.bindValue(":oid", _oid);
    knowDBC::Result r = q.execute();
    if(r and r.tuples() == 1)
    {
      cres_try(QString n, r.value(0, 0).value<QString>());
      l.relock();
      addOid(_oid, n);
      return cres_success(n);
    }
    else
    {
      return cres_failure("Unknown Oid {}", _oid);
    }
  }
  return cres_success(oids2name.value(_oid));
}

cres_qresult<Oid> Connection::Private::nameToOid(const QString& _name) const
{
  QMutexLocker l(&metainformation_lock);
  clog_assert(not _name.isEmpty());
  if(not name2oid.contains(_name))
  {
    l.unlock();
    knowDBC::Query q
      = Connection(self_p).createSQLQuery("SELECT oid FROM pg_type WHERE typname=?tn;");
    QString typname = _name;
    if(typname.endsWith("[]"))
    {
      while(typname.endsWith("[]"))
      {
        typname = typname.left(typname.size() - 2);
      }
      typname = "_" + typname;
    }
    q.bindValue("?tn", typname);
    knowDBC::Result r = q.execute();
    if(r and r.tuples() == 1)
    {
      cres_try(Oid oid, r.value(0, 0).value<uint>());
      l.relock();
      addOid(oid, _name);
      return cres_success(oid);
    }
    else
    {
      if(r)
      {
        return cres_failure("Unknown type {} ({})", _name, typname);
      }
      else
      {
        return cres_log_failure("in name to oid: {}", r.error());
      }
    }
  }
  return cres_success(name2oid.value(_name));
}

cres_qresult<void> Connection::Private::load_sql_files(PGconn* _conn, const QStringList& _files)
{
  for(const QString& filename : _files)
  {
    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
      QByteArray initdb = file.readAll();
      cres_try(cres_ignore, execute_sql_query(_conn, initdb));
    }
    else
    {
      return cres_failure("Failed to open file {}", filename);
    }
  }
  return cres_success();
}

cres_qresult<void> Connection::Private::execute_sql_query(PGconn* _conn, const QByteArray& _query)
{
  PGresult* result = PQexec(_conn, _query.constData());
  if(PQresultStatus(result) != PGRES_COMMAND_OK)
  {
    QString errMsg = PQerrorMessage(_conn);
    PQclear(result);
    return cres_failure(errMsg);
  }
  PQclear(result);
  return cres_success();
}

void Connection::Private::update_kdb_rdf_value_type_enum_values()
{
  knowDBC::Query q
    = Connection(self_p).createSQLQuery("SELECT typename FROM kdb_rdf_value_definitions;");
  knowDBC::Result r = q.execute();
  if(r)
  {
    kdb_rdf_value_type_enum_values.clear();
    kdb_rdf_value_type_enum_values.append("null");
    for(int i = 0; i < r.tuples(); ++i)
    {
      kdb_rdf_value_type_enum_values.append(r.value(i, 0).value<QString>().expect_success());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("in updating type enum", r);
  }
}

cres_qresult<knowCore::Value> Connection::Private::toValue(const QString& _oid,
                                                           const QByteArray& _source)
{
  QMutexLocker l(&metainformation_lock);
  if(oid2marshals.contains(_oid))
  {
    AbstractBinaryMarshal* marshal = oid2marshals[_oid];
    l.unlock();
    return marshal->toValue(_source, self_p);
  }
  else
  {
    l.unlock();
    return DatabaseInterface::PostgreSQL::BinaryMarshalsRegistry::toValue(_oid, _source, self_p);
  }
}

cres_qresult<QByteArray> Connection::Private::toByteArray(const knowCore::Value& _source,
                                                          QString& _oidName)
{
  QMutexLocker l(&metainformation_lock);
  if(metaTypeId2marshals.contains(_source.datatype()))
  {
    AbstractBinaryMarshal* marshal = metaTypeId2marshals[_source.datatype()];
    l.unlock();
    return marshal->toByteArray(_source, _oidName, self_p);
  }
  else
  {
    l.unlock();
    return DatabaseInterface::PostgreSQL::BinaryMarshalsRegistry::toByteArray(_source, _oidName,
                                                                              self_p);
  }
}

namespace
{
  template<class TargetType, class FloatType>
  inline void assignSpecialPsqlFloatValue(FloatType val, TargetType* target)
  {
    if(std::isnan(val))
    {
      *target = TargetType("'NaN'");
    }
    else
    {
      if(std::isinf(val))
      {
        if(val > 0)
        {
          *target = TargetType("'Infinity'");
        }
        else
        {
          *target = TargetType("'-Infinity'");
        }
      }
    }
  }
} // namespace

void Connection::Private::write(const knowCore::Value& val, QByteArray* r, Oid* oid,
                                bool* is_binary, bool* is_valid)
{
  *is_valid = true;

  if(val.isEmpty())
  {
    *r = QByteArray();
    *is_binary = true;
    *is_valid = false;
  }
  else
  {
    QString oidName;
    auto const [success, arr, message] = Connection(self_p).d->toByteArray(val, oidName);
    if(success)
    {
      *r = arr.value();
      *oid = Connection(self_p).d->nameToOid(oidName).expect_success();
      if(oidName == "raster")
      {
        *r = r->toHex();
        *is_binary = false;
      }
      else
      {
        *is_binary = true;
      }
    }
    else
    {
      clog_error("Conversion to binary failed or unknown type for binding with datatype {}: {}",
                 val.datatype(), message.value());
      *is_valid = false;
    }
  }
}

knowRDF::BlankNode Connection::Private::blankNode(const QString& _url)
{
  QPair<QString, QString> uuid_label = knowRDF::parseBlankNodeUri(_url);
  if(blanknodes.contains(uuid_label.first))
  {
    return blanknodes.value(uuid_label.first);
  }
  else
  {
    knowRDF::BlankNode bn = knowRDF::BlankNode(QUuid(uuid_label.first), uuid_label.second);
    blanknodes[uuid_label.first] = bn;
    return bn;
  }
}

QString Connection::Private::uniqueTableName(const QString& _prefix, const QString& _key)
{
  QMutexLocker l(&metainformation_lock);
  QString tn = prefix2key2tablename.value(_prefix).value(_key);
  l.unlock();
  if(tn.isEmpty())
  {
    knowDBC::Query q = Connection(self_p).createSQLQuery(
      "SELECT id FROM unique_tables WHERE prefix=:prefix AND key=:key");
    q.bindValue(":prefix", _prefix);
    q.bindValue(":key", _key);
    knowDBC::Result r = q.execute();
    if(r)
    {
      if(r.tuples() == 1)
      {
        tn = clog_qt::qformat("{}{}", _prefix, r.value(0, 0).value<int>().expect_success());
      }
      else
      {
        q.setQuery("INSERT INTO unique_tables(prefix, key) VALUES(:prefix, :key)", true);
        r = q.execute();
        if(r)
        {
          tn = uniqueTableName(_prefix, _key);
          l.relock();
          prefix2key2tablename[_prefix][_key] = tn;
          l.unlock();
        }
        else
        {
          KDB_REPOSITORY_REPORT_QUERY_ERROR("Failed to access 'unique_tables'", r);
          clog_fatal("internal error");
        }
      }
    }
    else
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR("Failed to access 'unique_tables'", r);
      clog_fatal("internal error");
    }
  }
  return tn;
}

cres_qresult<void> Connection::Private::loadExtension(const QString& _name)
{
  clog_info("Loading extension: {}", _name);
  QMutexLocker l(&metainformation_lock);
  loadedExtensions.append(_name);
  l.unlock();

  typedef cres_qresult<void> (*init_func)(const Connection&);

  QLibrary lib(_name);
  if(not lib.load())
  {
    return cres_log_failure("Failed to load extension '{}' with error '{}'", _name,
                            lib.errorString());
  }
  init_func i_func = reinterpret_cast<init_func>(lib.resolve(qPrintable(_name + "_initialise")));
  if(i_func)
  {
    cres_qresult<void> i_r = i_func(self_p);
    if(i_r.is_successful())
    {
      return cres_success();
    }
    else
    {
      l.relock();
      loadedExtensions.removeAll(_name);
      return cres_log_failure("Extension '{}' failed to load with error {}!", _name,
                              i_r.get_error());
    }
  }
  else
  {
    l.relock();
    loadedExtensions.removeAll(_name);
    return cres_log_failure("Extension '{}' is missing initialise function!", _name);
  }
}

cres_qresult<void> Connection::Private::loadExtensions()
{
  knowDBC::Query q = Connection(self_p).createSQLQuery("SELECT name FROM extensions");
  knowDBC::Result r = q.execute();
  if(r)
  {
    for(int i = 0; i < r.tuples(); ++i)
    {
      QString extension = r.value(i, 0).value<QString>().expect_success();
      QMutexLocker l(&metainformation_lock);
      if(not enabledExtensions.contains(extension))
      {
        enabledExtensions.append(extension);
      }
    }
    for(int i = 0; i < r.tuples(); ++i)
    {
      QString extension = r.value(i, 0).value<QString>().expect_success();
      clog_info("Loading extension: '{}'", extension);
      QMutexLocker l(&metainformation_lock);
      if(not loadedExtensions.contains(extension))
      {
        l.unlock();
        cres_try(cres_ignore, loadExtension(extension));
      }
    }
    return cres_success();
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("Failed to list extensions", r);
  }
}

void Connection::increaseSelfPCount()
{
  if(d)
  {
    ++d->self_p_count;
  }
}

void Connection::decreaseSelfPCount()
{
  if(d)
  {
    --d->self_p_count;
    if(d->self_p_count == 0)
    {
      d->disconnect();
    }
  }
}

Connection::Connection(const QString& _host, int _port, const QString& _database) : d(new Private)
{
  increaseSelfPCount();
  d->host = _host;
  d->port = _port;
  d->database = _database.toLower();
}

Connection::Connection() {}

Connection::Connection(const Connection& _rhs) : d(_rhs.d) { increaseSelfPCount(); }

Connection& Connection::operator=(const Connection& _rhs)
{
  decreaseSelfPCount();
  d = _rhs.d;
  increaseSelfPCount();
  return *this;
}

Connection Connection::create(const HostName& _host, int _port, const QString& _database)
{
  QDir host_dir = _host.get_value();
  Connection c(host_dir.exists() ? host_dir.absolutePath() : _host, _port, _database);
  c.d->self_p = c;
  c.d->graphsManager = new GraphsManager(c);
  c.d->notificationsManager = new NotificationsManager();
  c.d->krQueryEngine = new krQuery::Engine(c);
  return c;
}

Connection Connection::create(const SectionName& _section, const StoreName& _store, int _port,
                              const QString& _database)
{
  return create(HostName(Store::standardDir(_section, _store).absolutePath()), _port, _database);
}

Connection Connection::create(const StoreName& _store, int _port, const QString& _database)
{
  return create(HostName(Store::standardDir(_store).absolutePath()), _port, _database);
}

Connection::~Connection() { decreaseSelfPCount(); }

bool Connection::isValid() const { return bool(d); }

void Connection::disconnect() { d->disconnect(); }

cres_qresult<void> Connection::connect(bool _initialise_database)
{
  {
    QMutexLocker l(&d->connections_lock);
    if(not d->connections.isEmpty())
      return cres_failure("Already connected");
  }

  d->uuid = QUuid::createUuid();

  bool initialising_database = false;

  QString conninfo
    = clog_qt::qformat("host={} port={} dbname={} user=kdb", d->host, d->port, d->database);

  PGconn* conn = PQconnectdb(qPrintable(conninfo));
  clog_info("Connecting to '{}:{}'", d->host, d->port);
  bool new_instance = false;
  if(PQstatus(conn) != CONNECTION_OK)
  {
    if(not _initialise_database)
    {
      PQfinish(conn);
      return cres_log_failure("Failed to connect to database: {}", conninfo);
    }
    new_instance = true;
    QString errMsg = PQerrorMessage(conn);
    if(errMsg.contains(QString("FATAL:  role \"kdb\" does not exist")))
    {
      PQfinish(conn);
      QString conninfo2 = clog_qt::qformat("host={} port={} dbname=template1", d->host, d->port);
      conn = PQconnectdb(qPrintable(conninfo2));
      if(PQstatus(conn) != CONNECTION_OK)
      {
        QString errMSg = PQerrorMessage(conn);
        PQfinish(conn);
        return cres_log_failure(qPrintable(errMsg));
      }
      PGresult* result = PQexec(conn, qPrintable(QString("CREATE ROLE kdb SUPERUSER LOGIN;")));
      if(PQresultStatus(result) != PGRES_COMMAND_OK)
      {
        QString errMSg = PQerrorMessage(conn);
        PQclear(result);
        PQfinish(conn);
        return cres_log_failure(qPrintable(errMsg));
      }
      PQclear(result);
      PQfinish(conn);
      conn = PQconnectdb(qPrintable(conninfo));
      errMsg = PQerrorMessage(conn);
    }
    if(errMsg.contains(clog_qt::qformat("FATAL:  database \"{}\" does not exist", d->database)))
    {
      PQfinish(conn);
      QString conninfo2 = clog_qt::qformat("host={} port={} dbname=template1", d->host, d->port);
      conn = PQconnectdb(qPrintable(conninfo2));
      if(PQstatus(conn) != CONNECTION_OK)
      {
        QString errMSg = PQerrorMessage(conn);
        PQfinish(conn);
        return cres_log_failure(qPrintable(errMsg));
      }
      PGresult* result
        = PQexec(conn, qPrintable(clog_qt::qformat("CREATE DATABASE {};", d->database)));
      if(PQresultStatus(result) != PGRES_COMMAND_OK)
      {
        QString errMSg = PQerrorMessage(conn);
        PQfinish(conn);
        return cres_log_failure(qPrintable(errMsg));
      }
      PQfinish(conn);
      ConnectionHandle handle(d->self_p, true);
      kdb_repository_init_repository_data();
#define CLEANUP_ON_FAILURE                                                                         \
  on_failure(handle.release(); disconnect(); conn = PQconnectdb(qPrintable(conninfo2));            \
             PQexec(conn, qPrintable(clog_qt::qformat("DROP DATABASE {};", d->database)));         \
             PQfinish(conn);)
      cres_try(cres_ignore,
               d->load_sql_files(handle.connection(),
                                 {":/kdb/repository/DatabaseInterface/PostgreSQL/sql/initdb.sql"}),
               CLEANUP_ON_FAILURE);
      cres_try(d->rdf_value_marshal,
               DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::create(d->self_p),
               CLEANUP_ON_FAILURE);
      d->rdf_term_marshal
        = new DatabaseInterface::PostgreSQL::RDFTermBinaryMarshal(d->rdf_value_marshal, d->self_p);

      cres_try(cres_ignore,
               d->load_sql_files(
                 handle.connection(),
                 {":/kdb/repository/DatabaseInterface/PostgreSQL/sql/functions.sql",
                  ":/kdb/repository/DatabaseInterface/PostgreSQL/sql/rdf_value.sql",
                  ":/kdb/repository/DatabaseInterface/PostgreSQL/sql/rdf_term.sql",
                  ":/kdb/repository/DatabaseInterface/PostgreSQL/sql/views.sql",
                  ":/kdb/repository/DatabaseInterface/PostgreSQL/sql/sparql_functions.sql",
                  ":/kdb/repository/DatabaseInterface/PostgreSQL/sql/version_control.sql",
                  ":/kdb/repository/DatabaseInterface/PostgreSQL/sql/version_control_44.sql",
                  ":/kdb/repository/DatabaseInterface/PostgreSQL/sql/initdb_32.sql",
                  ":/kdb/repository/DatabaseInterface/PostgreSQL/sql/initdb_40.sql"}),
               CLEANUP_ON_FAILURE);
      cres_try(cres_ignore, d->rdf_value_marshal->finishInitialisation(), CLEANUP_ON_FAILURE);

      PQclear(result);
      initialising_database = true;
    }
    else
    {
      return cres_log_failure("Failed to connect to database: {}", conninfo);
    }
    ActiveRecord::VersionRecord::createTable(*this);
  }
  else
  {
    QMutexLocker l(&d->connections_lock);
    d->connections.append(conn);
    d->available_connections.append(conn);
  }

  cres_try(ActiveRecord::VersionRecord vr, ActiveRecord::VersionRecord::get(*this, "kDB"),
           on_failure(disconnect()));
  clog_assert(vr.isPersistent());
  // update to SQL in the future
  if(vr.striclyLessThan(3, 2, 0))
  {
    if(not new_instance)
    {
      ConnectionHandle handle(*this);
      cres_try(cres_ignore, d->load_sql_files(
                              handle.connection(),
                              {":/kdb/repository/DatabaseInterface/PostgreSQL/sql/initdb_32.sql"}));
    }
  }
  if(vr.striclyLessThan(4, 0, 0))
  {
    if(not new_instance)
    {
      ConnectionHandle handle(*this);
      cres_try(cres_ignore, d->load_sql_files(
                              handle.connection(),
                              {":/kdb/repository/DatabaseInterface/PostgreSQL/sql/initdb_40.sql"}));
      for(TripleStore ts : d->graphsManager->tripleStores())
      {
        if(ts.options().testFlag(TripleStore::Option::Versioning))
        {
          clog_warning("Reset of version control for {}", ts.uri());
          ts.disableVersioning().expect_success();
          ts.enableVersioning().expect_success();
        }
        QString errMsg;
        cres_try(cres_ignore, d->execute_sql_query(
                                handle.connection(),
                                clog_qt::qformat(
                                  "ALTER TABLE {} ALTER COLUMN TYPE bytea USING decode(md5, 'hex')",
                                  ts.tablename())
                                  .toLatin1()));
      }
    }
  }
  if(vr.striclyLessThan(5, 0, 0))
  {
    if(not new_instance)
    {
      cres_failure("Cannot automatically upgrade a store from version {}.{}.{} to kDB >= 5",
                   vr.major(), vr.minor(), vr.revision());
    }
  }

  // Load units
  cres_try(cres_ignore,
           DatabaseInterface::PostgreSQL::SQLInterface::setUnitConversionFactors(d->self_p),
           on_failure(disconnect()), message("Could not set conversion factors with error: {}"));

  // Start notifications manager
  d->notificationsManager->start(PQconnectdb(qPrintable(conninfo)));
  cres_try(cres_ignore, d->graphsManager->reload(), on_failure(disconnect()));

  // Setup the RDF value/term marshal
  if(not d->rdf_value_marshal)
  {
    cres_try(d->rdf_value_marshal,
             DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::create(d->self_p),
             on_failure(disconnect()));
  }
  Private* v_d = d.data();
  d->notificationsManager->listen("kdb_rdf_value_type_enum_values_updated", [v_d](const QByteArray&)
                                  { v_d->update_kdb_rdf_value_type_enum_values(); });
  d->update_kdb_rdf_value_type_enum_values(); // just to make sure that we didn't miss a
                                              // notification

  d->oid2marshals[d->rdf_value_marshal->oid()] = d->rdf_value_marshal;
  d->metaTypeId2marshals[d->rdf_value_marshal->datatype()] = d->rdf_value_marshal;
  if(not d->rdf_term_marshal)
  {
    d->rdf_term_marshal
      = new DatabaseInterface::PostgreSQL::RDFTermBinaryMarshal(d->rdf_value_marshal, d->self_p);
  }
  d->oid2marshals[d->rdf_term_marshal->oid()] = d->rdf_term_marshal;
  d->metaTypeId2marshals[knowRDF::ObjectMetaTypeInformation::uri()]
    = d->rdf_term_marshal; // for knowRDF::Object
  d->metaTypeId2marshals[d->rdf_term_marshal->datatype()]
    = d->rdf_term_marshal; // for knowRDF::Literal
  d->sparqlFunctionsManager = new SPARQLFunctionsManager(d->self_p);

  // Monitor changes in triple stores and views and sync them into the graphs manager
  d->notificationsManager->listen("triples_stores_changes", [v_d](const QByteArray&)
                                  { v_d->graphsManager->reload().expect_success(); });
  d->notificationsManager->listen("views_changes", [v_d](const QByteArray&)
                                  { v_d->graphsManager->reload().expect_success(); });
  d->notificationsManager->listen("unions_changes", [v_d](const QByteArray&)
                                  { v_d->graphsManager->reload().expect_success(); });

  d->notificationsManager->listen("kdb_extensions_changes",
                                  [v_d](const QByteArray&) { v_d->loadExtensions(); });
  d->loadExtensions();

  if(initialising_database)
  {
    cres_try(cres_ignore, d->graphsManager->createTripleStore(askcore_graph::default_));
    cres_try(cres_ignore, d->graphsManager->createTripleStore(askcore_graph::info));

    // Add the uuid
    cres_try(TripleStore infoTS, infoTripleStore(), on_failure(disconnect()));
    d->m_server_uuid = QUuid::createUuid();
    clog_assert(not d->m_server_uuid.isNull());
    infoTS.insert(
      knowCore::Uris::askcore_db_info::self, knowCore::Uris::askcore_db_info::hasUUID,
      knowRDF::Literal::fromValue<QString>(knowCore::Uris::xsd::string, d->m_server_uuid.toString())
        .expect_success());
    clog_info("Create server with uuid: {}", d->m_server_uuid);

    // Insert signature
    d->m_rsa_algorithm.generateKey();
    knowRDF::BlankNode signatureBN;
    QList<knowRDF::Triple> signature_triples;
    signature_triples.append(knowRDF::Triple(knowCore::Uris::askcore_db_info::self,
                                             knowCore::Uris::askcore_db_info::hasSignature,
                                             signatureBN));
    signature_triples.append(
      knowRDF::Triple(signatureBN, knowCore::Uris::dul::hasDataValue,
                      knowRDF::Literal::fromValue(knowCore::Uris::askcore_datatype::binarydata,
                                                  d->m_rsa_algorithm.serialise())
                        .expect_success()));
    signature_triples.append(knowRDF::Triple(signatureBN, knowCore::Uris::dul::isClassifiedBy,
                                             knowCore::Uris::askcore_db_info::RSA));
    infoTS.insert(signature_triples);
  }
  else
  {
    // Get the uuid
    cres_try(TripleStore infoTS, infoTripleStore(), on_failure(disconnect()));
    if(infoTS.isValid())
    {
      knowDBC::Query uuidSparqlQuery = infoTS.createSPARQLQuery(
        {}, "PREFIX: <http://askco.re/db/info#> SELECT ?uuid WHERE { :self :hasUUID ?uuid }");
      knowDBC::Result uuidSparqlResult = uuidSparqlQuery.execute();
      if(uuidSparqlResult)
      {
        if(uuidSparqlResult.tuples() >= 1)
        {
          cres_try(QString uuid_data, uuidSparqlResult.value<QString>(0, 0),
                   on_failure(disconnect()));
          d->m_server_uuid = QUuid(uuid_data);
          clog_assert(not d->m_server_uuid.isNull());
        }
        else
        {
          disconnect();
          return cres_log_failure("No UUID for the server in database!");
        }
      }
      else
      {
        disconnect();
        return cres_log_failure("in SPARQLQuery in getting kDB's UUID with error {} in query {}",
                                uuidSparqlResult.error(), uuidSparqlResult.query());
      }
      // Select signature
      knowDBC::Query rsaSparqlQuery = infoTS.createSPARQLQuery(
        {}, "PREFIX: <http://askco.re/db/info#> PREFIX dul: "
            "<http://www.loa-cnr.it/ontologies/DUL.owl#> SELECT ?signature WHERE { :self "
            ":hasSignature  [ dul:hasDataValue ?signature ; dul:isClassifiedBy :RSA ] }");
      knowDBC::Result rsaSparqlResult = rsaSparqlQuery.execute();
      if(rsaSparqlResult)
      {
        if(rsaSparqlResult.tuples() == 1)
        {
          d->m_rsa_algorithm.unserialise(rsaSparqlResult.value<QByteArray>(0, 0).expect_success());
        }
        else
        {
          disconnect();
          return cres_log_failure("Missing server signature in database!");
        }
      }
      else
      {
        disconnect();
        return cres_log_failure(
          "in SPARQLQuery in getting kDB's RSA signature with error {} in query {}",
          uuidSparqlResult.error(), uuidSparqlResult.query());
      }
    }
    else
    {
      disconnect();
      return cres_log_failure("Missing info triple store!");
    }
  }
  cres_try(cres_ignore, RDF::FocusNodeCollection::initialise(*this), on_failure(disconnect()));
  vr.updateToCurrent();
  for(const std::function<void(const Connection&)>& f : d->connection_listener.values())
  {
    f(*this);
  }
  return cres_success();
}

bool Connection::isConnected() const
{
  QMutexLocker l(&d->connections_lock);
  return d->connections.size() > 0;
}

GraphsManager* Connection::graphsManager() const { return d->graphsManager; }

NotificationsManager* Connection::notificationsManager() { return d->notificationsManager; }

SPARQLFunctionsManager* Connection::sparqlFunctionsManager() const
{
  return d->sparqlFunctionsManager;
}

cres_qresult<void> Connection::executeQueryFromFile(const QString& _fileName) const
{
  ConnectionHandle h(d->self_p);
  return d->load_sql_files(h.connection(), {_fileName});
}

cres_qresult<void> Connection::executeQuery(const QString& _query) const
{
  ConnectionHandle h(d->self_p);
  return d->execute_sql_query(h.connection(), _query.toLatin1());
}

cres_qresult<TripleStore> Connection::defaultTripleStore() const
{
  return d->graphsManager->getTripleStore(askcore_graph::default_);
}

cres_qresult<TripleStore> Connection::infoTripleStore() const
{
  return d->graphsManager->getTripleStore(askcore_graph::info);
}

QUuid Connection::serverUuid() const { return d->m_server_uuid; }

knowCore::Uri Connection::serverUri() const
{
  return knowCore::Uri(knowCore::Uris::askcore_db_server::base,
                       d->m_server_uuid.toString(QUuid::WithoutBraces));
}

Cyqlops::Crypto::RSAAlgorithm Connection::rsaAlgorithm() const { return d->m_rsa_algorithm; }

cres_qresult<quint64> Connection::oid(const QString& _type) const { return d->nameToOid(_type); }

cres_qresult<void> Connection::enableExtension(const QString& _extension)
{
  QMutexLocker l(&d->metainformation_lock);
  if(d->loadedExtensions.contains(_extension))
    return cres_success();
  l.unlock();
  cres_try(cres_ignore, d->loadExtension(_extension));
  if(d->enabledExtensions.contains(_extension))
    return cres_success();
  knowDBC::Query q = createSQLQuery("INSERT INTO extensions(name) VALUES (:name)");
  q.bindValue(":name", _extension);
  knowDBC::Result r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("error inserting extension", r);
  }
  d->enabledExtensions.append(_extension);
  return cres_success();
}

bool Connection::isExtensionEnabled(const QString& _extension) const
{
  QMutexLocker l(&d->metainformation_lock);
  return d->loadedExtensions.contains(_extension);
}

QUuid Connection::connectionUuid() const { return d->uuid; }

QUuid Connection::executeDisconnection(const std::function<void()>& _function) const
{
  QUuid uuid_function = QUuid::createUuid();
  d->disconnection_listener[uuid_function] = _function;
  return uuid_function;
}

void Connection::removeExecuteDisconnection(const QUuid& _uuid) const
{
  d->disconnection_listener.remove(_uuid);
}

QUuid Connection::executeConnection(const std::function<void(const Connection&)>& _function) const
{
  QUuid uuid_function = QUuid::createUuid();
  d->connection_listener[uuid_function] = _function;
  return uuid_function;
}
void Connection::removeExecuteConnection(const QUuid& _uuid) const
{
  d->connection_listener.remove(_uuid);
}

knowDBC::Query Connection::createSQLQuery(const QString& _query,
                                          const knowCore::ValueHash& _bindings,
                                          const knowCore::ValueHash& _options) const
{
  knowDBC::Query q(new DatabaseInterface::PostgreSQL::SQLQueryExecutor(*this));
  q.setQuery(_query);
  q.bindValues(_bindings);
  q.setOptions(_options);
  return q;
}

knowDBC::Query Connection::createSPARQLQuery(const RDFEnvironment& _environment,
                                             const QString& _query,
                                             const knowCore::ValueHash& _bindings,
                                             const knowCore::ValueHash& _options) const
{
  knowDBC::Query q(
    new SPARQLExecution::QueryExecutor(RDFEnvironment(_environment).setConnection(*this)));
  q.setQuery(_query);
  q.bindValues(_bindings);
  q.setOptions(_options);
  return q;
}

QObject* Connection::extensionObject(const std::type_index& _index) const
{
  return d->extensionObjects[_index];
}

void Connection::createExtensionObject(QObject* _object, const std::type_index& _index)
{
  if(d->extensionObjects.contains(_index))
  {
    clog_warning("{} is already set as an extension object, new instance is discarded.",
                 _index.name());
    delete _object;
  }
  else
  {
    d->extensionObjects[_index] = _object;
  }
}

krQuery::Engine* Connection::krQueryEngine() const { return d->krQueryEngine; }

void Connection::addQueryFactory(const Interfaces::QueryFactory* _query_factory)
{
  d->query_factories.append(_query_factory);
}

struct Connection::knowDBCConnectionInterface : public knowDBC::Interfaces::Connection
{
  knowDBCConnectionInterface(const kDB::Repository::Connection& _connection)
      : connection(_connection)
  {
  }
  virtual ~knowDBCConnectionInterface() {}
  bool supportQuery(const knowCore::Uri& _type) const override
  {
    if(_type == knowCore::Uris::askcore_db_query_language::SQL
       or _type == knowCore::Uris::askcore_db_query_language::SPARQL
       or _type == knowCore::Uris::askcore_db_query_language::krQL)
    {
      return true;
    }
    for(const Interfaces::QueryFactory* qf : connection.d->query_factories)
    {
      if(qf->canCreateQuery(_type, knowCore::ValueHash()))
      {
        return true;
      }
    }
    return false;
  }
  cres_qresult<knowDBC::Query> createQuery(const knowCore::Uri& _type,
                                           const knowCore::ValueHash& _environment) const
  {
    if(_type == knowCore::Uris::askcore_db_query_language::SQL)
    {
      return cres_success(connection.createSQLQuery());
    }
    else if(_type == knowCore::Uris::askcore_db_query_language::SPARQL)
    {
      RDFEnvironment env;
      if(_environment.contains("default_dataset"))
      {
        cres_try(knowCore::Uri dataset_uri, _environment.value<knowCore::Uri>("default_dataset"));
        cres_try(kDB::Repository::RDFDataset dataset,
                 connection.graphsManager()->getDataset(dataset_uri));
        env.setDefaultDataset(dataset);
      }
      return cres_success(connection.createSPARQLQuery(env));
    }
    else if(_type == knowCore::Uris::askcore_db_query_language::krQL)
    {
      return cres_success(connection.krQueryEngine()->createQuery());
    }
    else
    {
      for(const Interfaces::QueryFactory* qf : connection.d->query_factories)
      {
        if(qf->canCreateQuery(_type, _environment))
        {
          return qf->createQuery(_type, _environment);
        }
      }
      return cres_log_failure("Unsupported type of query: {}.", _type);
    }
  }
  bool isConnected() const { return connection.isConnected(); }
  kDB::Repository::Connection connection;
};

Connection::operator knowDBC::Connection()
{
  return d ? new knowDBCConnectionInterface{*this} : knowDBC::Connection();
}
