#include "TripleStore_p.h"

#include <QBuffer>

#include <clog_qt>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>

#include <knowCore/Uris/askcore_db.h>
#include <knowRDF/BlankNode.h>
#include <knowRDF/Object.h>
#include <knowRDF/Serialiser.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "Connection_p.h"
#include "Logging.h"
#include "NotificationsManager.h"
#include "QueryConnectionInfo.h"

#include "VersionControl/DeltaBuilder.h"
#include "VersionControl/Manager_p.h"
#include "VersionControl/RevisionBuilder.h"
#include "VersionControl/Revision_p.h"
#include "VersionControl/Transaction_p_p.h"
#include "VersionControl/Utils_p.h"

#include "DatabaseInterface/PostgreSQL/BinaryMarshalsRegistry.h"
#include "DatabaseInterface/PostgreSQL/RDFTermBinaryMarshal.h"
#include "DatabaseInterface/PostgreSQL/SQLCopyData.h"

using namespace kDB::Repository;

TripleStore::Private::~Private() { delete manager; }

TripleStore::TSTemporaryTransaction
  TripleStore::Private::acquireTransaction(TripleStore* _self, const Transaction& _transaction)
{
  TSTemporaryTransaction tt;
  if(_transaction.isActive())
  {
    tt.sql_transaction = _transaction;
  }
  else
  {
    tt.sql_transaction = Transaction(_self->connection());
    tt.own_sql_transaction = true;
  }
  DatabaseInterface::PostgreSQL::SQLInterface::lockTripleStore(tt.sql_transaction,
                                                               _self->tablename(), false);

  if(definition->options.testFlag(Option::Versioning) and not disable_recording_version
     and not tt.sql_transaction.d->triplesStoreTransactions.contains(definition->name))
  {
    VersionControl::Transaction transaction_ts(*_self, tt.sql_transaction);
    tt.sql_transaction.d->triplesStoreTransactions[definition->name] = transaction_ts;
    transaction_ts.d->tags = _self->versionControlManager()->defaultRevisionTags();
  }
  return tt;
}

cres_qresult<void> TripleStore::Definition::setMeta(const QStringList& _path,
                                                    const knowCore::Value& _value,
                                                    const Transaction& _transaction)
{
  clog_assert(not _path.isEmpty());
  clog_assert(_transaction.isActive());
  QMutexLocker l(&mutex);
  cres_try(cres_ignore,
           DatabaseInterface::PostgreSQL::SQLInterface::setMeta(_transaction, name, _path, _value));
  _transaction.executeOnSuccessfulCommit(
    [this, _path, _value]()
    {
      QMutexLocker l(&mutex);
      meta = setMeta(meta.hash(), _path, _value);
    });
  return cres_success();
}

QHash<QString, knowCore::Value>
  TripleStore::Definition::setMeta(QHash<QString, knowCore::Value> _map, const QStringList& _path,
                                   const knowCore::Value& _value)
{
  if(_path.size() == 1)
  {
    _map[_path.front()] = _value;
    return _map;
  }
  else
  {
    QStringList subpath = _path;
    subpath.removeFirst();
    QHash<QString, knowCore::Value> current_hash;
    knowCore::Value front_value = _map[_path.front()];
    if(knowCore::ValueIs<knowCore::ValueHash> front_value_hash = front_value)
    {
      current_hash = front_value_hash->hash();
    }
    _map[_path.front()] = knowCore::ValueHash(setMeta(current_hash, subpath, _value));
    return _map;
  }
}

cres_qresult<knowCore::Value> TripleStore::Definition::getMeta(const QStringList& _path,
                                                               const Transaction& _transaction)
{
  if(_transaction.isActive())
  {
    return DatabaseInterface::PostgreSQL::SQLInterface::getMeta(_transaction, name, _path);
  }
  else
  {
    QMutexLocker l(&mutex);
    clog_assert(not _path.isEmpty());
    knowCore::ValueHash g = meta;
    for(int i = 0; i < _path.length() - 1; ++i)
    {
      knowCore::Value v = g.value(_path[i]);
      if(v.canConvert<knowCore::ValueHash>())
      {
        g = v.value<knowCore::ValueHash>().expect_success().hash();
      }
      else
      {
        return cres_success(knowCore::Value());
      }
    }
    return cres_success(g.value(_path.last()));
  }
}

TripleStore::TripleStore(const Connection& _connection, QSharedPointer<Definition> _definition)
    : RDFDataset(new Private(_connection))
{
  D()->definition = _definition;
  clog_assert(not D()->definition or not D()->definition->table.isEmpty());
}

TripleStore::TripleStore() : RDFDataset(new RDFDataset::Private(Type::Invalid)) {}

TripleStore::TripleStore(const TripleStore& _rhs) : RDFDataset(0) { d = _rhs.d; }

TripleStore& TripleStore::operator=(const TripleStore& _rhs)
{
  d = _rhs.d;
  return *this;
}

TripleStore::~TripleStore() {}

TripleStore TripleStore::detach() const { return TripleStore(d->connection, D()->definition); }

QString TripleStore::tablename() const { return D()->definition->table; }

TripleStore::Options TripleStore::options() const { return D()->definition->options; }

cres_qresult<void> TripleStore::insert(const knowRDF::Subject& _subject,
                                       const knowCore::Uri& _predicate, const knowCore::Uri& _value,
                                       const Transaction& _transaction)
{
  return insert(knowRDF::Triple(_subject, _predicate, _value), _transaction);
}

cres_qresult<void> TripleStore::insert(const knowRDF::Subject& _subject,
                                       const knowCore::Uri& _predicate,
                                       const knowRDF::Object& _object,
                                       const Transaction& _transaction)
{
  return insert(knowRDF::Triple(_subject, _predicate, _object), _transaction);
}

cres_qresult<void> TripleStore::insert(const knowRDF::Subject& _subject,
                                       const knowCore::Uri& _predicate, const knowCore::Uri& _type,
                                       const knowCore::Value& _value, const QString& _lang,
                                       const Transaction& _transaction)
{
  return insert(knowRDF::Triple(_subject, _predicate,
                                knowRDF::Object::fromValue(_type, _value, _lang).expect_success()),
                _transaction);
}

cres_qresult<void> TripleStore::insert(const knowRDF::Subject& _subject,
                                       const knowCore::Uri& _predicate,
                                       const knowRDF::BlankNode& _value,
                                       const Transaction& _transaction)
{
  return insert(knowRDF::Triple(_subject, _predicate, _value), _transaction);
}

cres_qresult<void> TripleStore::insert(const knowRDF::Triple& _triple,
                                       const Transaction& _transaction)
{
  TSTemporaryTransaction tt = D()->acquireTransaction(this, _transaction);

  knowCore::Uri subject_id
    = DatabaseInterface::PostgreSQL::SQLInterface::subjectString(_triple.subject());

  knowDBC::Query q = tt.sql_transaction.createSQLQuery();
  q.setQuery(
    "INSERT INTO " + D()->definition->table
    + "  (subject, predicate, object, md5) VALUES (?s, ?p, ?o, ?m) ON CONFLICT DO NOTHING");
  q.bindValue("?s", knowCore::Value::fromValue(subject_id));
  q.bindValue("?p", knowCore::Value::fromValue(_triple.predicate()));
  q.bindValue("?o", knowCore::Value::fromValue(_triple.object()));
  cres_try(QByteArray triple_md5, _triple.md5());
  q.bindValue("?m", triple_md5);
  knowDBC::Result r = q.execute();
  if(not r)
  {
    cres_try(cres_ignore, tt.rollback());
    return cres_failure("Failed to insert triple: {}", r.error());
  }
  cres_try(cres_ignore, tt.commitIfOwn());
  return cres_success();
}

cres_qresult<bool> TripleStore::hasTriple(const knowRDF::Triple& _triple,
                                          const Transaction& _transaction)
{
  knowDBC::Query q = QueryConnectionInfo(_transaction, D()->connection).createSQLQuery();
  q.setQuery("SELECT COUNT(*) FROM " + D()->definition->table
             + " WHERE subject=:subject AND predicate = :predicate AND object = :object");
  q.bindValue(":subject",
              knowCore::Value::fromValue(
                DatabaseInterface::PostgreSQL::SQLInterface::subjectString(_triple.subject())));
  q.bindValue(":predicate", knowCore::Value::fromValue(_triple.predicate()));
  q.bindValue(":object", knowCore::Value::fromValue(_triple.object()));
  knowDBC::Result r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to check for triple", r);
    return cres_failure("Failed to check for triple {} with error {}", _triple, r.error());
  }
  return cres_success(r.value(0, 0).value<int>().expect_success() == 1);
}

cres_qresult<void> TripleStore::insert(const QList<knowRDF::Triple>& _triple,
                                       const Transaction& _transaction)
{
  TSTemporaryTransaction tt = D()->acquireTransaction(this, _transaction);

  cres_try(cres_ignore,
           DatabaseInterface::PostgreSQL::SQLInterface::insertOrRemoveTriples(
             true, tt.sql_transaction, _triple, D()->definition->table),
           on_failure(tt.rollback()));
  cres_try(cres_ignore, tt.commitIfOwn(), on_failure(tt.rollback()));
  return cres_success();
}

cres_qresult<void> TripleStore::remove(const knowRDF::Subject& _subject,
                                       const knowCore::Uri& _predicate, const knowCore::Uri& _value,
                                       const Transaction& _transaction)
{
  return remove(knowRDF::Triple(_subject, _predicate, _value), _transaction);
}

cres_qresult<void> TripleStore::remove(const knowRDF::Subject& _subject,
                                       const knowCore::Uri& _predicate,
                                       const knowRDF::Object& _object,
                                       const Transaction& _transaction)
{
  return remove(knowRDF::Triple(_subject, _predicate, _object), _transaction);
}

cres_qresult<void> TripleStore::remove(const knowRDF::Subject& _subject,
                                       const knowCore::Uri& _predicate, const knowCore::Uri& _type,
                                       const knowCore::Value& _value, const QString& _lang,
                                       const Transaction& _transaction)
{
  return remove(knowRDF::Triple(_subject, _predicate,
                                knowRDF::Object::fromValue(_type, _value, _lang).expect_success()),
                _transaction);
}

cres_qresult<void> TripleStore::remove(const knowRDF::Subject& _subject,
                                       const knowCore::Uri& _predicate,
                                       const knowRDF::BlankNode& _value,
                                       const Transaction& _transaction)
{
  return remove(knowRDF::Triple(_subject, _predicate, _value), _transaction);
}

cres_qresult<void> TripleStore::remove(const knowRDF::Triple& _triple,
                                       const Transaction& _transaction)
{
  TSTemporaryTransaction tt = D()->acquireTransaction(this, _transaction);

  QString subject_id
    = DatabaseInterface::PostgreSQL::SQLInterface::subjectString(_triple.subject());

  knowDBC::Query q = tt.sql_transaction.createSQLQuery();
  q.setQuery("DELETE FROM " + D()->definition->table
             + " WHERE subject=?s and predicate=?p and object=?o");
  q.bindValue("?s", subject_id);
  q.bindValue("?p", (QString)_triple.predicate());
  q.bindValue("?o", knowCore::Value::fromValue(_triple.object()));
  knowDBC::Result r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to remove triple", r);
    tt.rollback();
    return cres_failure("Failed to remove triple {} with error {}", _triple, r.error());
  }
  tt.commitIfOwn();
  return cres_success();
}

cres_qresult<void> TripleStore::remove(const QList<knowRDF::Triple>& _triple,
                                       const Transaction& _transaction)
{
  if(_triple.isEmpty())
    return cres_success();

  TSTemporaryTransaction tt = D()->acquireTransaction(this, _transaction);

  cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::insertOrRemoveTriples(
                          false, tt.sql_transaction, _triple, D()->definition->table));
  cres_try(cres_ignore, tt.commitIfOwn());
  return cres_success();
}

cres_qresult<QList<knowRDF::Triple>> TripleStore::triples() const
{
  knowDBC::Query q = D()->connection.createSQLQuery("SELECT subject, predicate, object FROM "
                                                    + D()->definition->table);
  knowDBC::Result r = q.execute();

  if(r)
  {
    QList<knowRDF::Triple> triples;

    for(int i = 0; i < r.tuples(); ++i)
    {
      triples.append(DatabaseInterface::PostgreSQL::SQLInterface::getTriple(D()->connection, r, i,
                                                                            0, 1, 2, false));
    }

    return cres_success(triples);
  }
  else
  {
    return cres_failure(r.error());
  }
}

std::size_t TripleStore::triplesCount() const
{
  knowDBC::Query q
    = D()->connection.createSQLQuery("SELECT COUNT(*) FROM " + D()->definition->table);
  knowDBC::Result r = q.execute();
  return r.value<int>(0, 0).expect_success();
}

knowRDF::Triple TripleStore::triple(std::size_t _r) const
{
  knowDBC::Query q = D()->connection.createSQLQuery("SELECT subject, predicate, object FROM "
                                                    + D()->definition->table
                                                    + " ORDER BY id LIMIT 1 OFFSET :offset");
  q.bindValue<quint64>(":offset", _r);
  knowDBC::Result r = q.execute();

  if(r.tuples() > 0)
  {
    return DatabaseInterface::PostgreSQL::SQLInterface::getTriple(D()->connection, r, 0, 0, 1, 2,
                                                                  false);
  }

  return knowRDF::Triple();
}

QString TripleStore::generateInsertDataQuery(bool _blankNodeAsUri) const
{
  QBuffer buffer;
  buffer.open(QIODevice::WriteOnly);
  knowRDF::Serialiser serialiser(&buffer, knowCore::UriManager());
  serialiser.setSaveBlankNodeAsUri(_blankNodeAsUri);
  serialiser.serialise(triples().expect_success());
  buffer.close();
  return QString::fromUtf8("INSERT DATA {\n" + buffer.buffer() + "}\n");
}

cres_qresult<void> TripleStore::clear(const Transaction& _transaction)
{
  TSTemporaryTransaction tt = D()->acquireTransaction(this, _transaction);
  knowDBC::Query q = tt.sql_transaction.createSQLQuery("DELETE FROM " + D()->definition->table);
  knowDBC::Result r = q.execute();
  if(not r)
  {
    tt.rollback();
    KDB_REPOSITORY_REPORT_QUERY_ERROR("Error when deleting all triples", r);
  }
  cres_try(cres_ignore, tt.commitIfOwn());
  return cres_success();
}

cres_qresult<void> TripleStore::disableVersioning()
{
  if(D()->definition->options.testFlag(Option::Versioning))
  {
    delete D()->manager;
    D()->manager = nullptr;

    cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::disableVersioning(
                            D()->connection, *this));
    D()->definition->options &= ~(int)Option::Versioning;
    return cres_success();
  }
  else
  {
    return cres_failure("Versioning is not enabled!");
  }
}

cres_qresult<void> TripleStore::enableVersioning()
{
  if(D()->definition->options.testFlag(Option::Versioning))
  {
    return cres_failure("Versioning is already enabled");
  }
  Transaction transaction(D()->connection);
  cres_try(cres_ignore,
           DatabaseInterface::PostgreSQL::SQLInterface::enableVersioning(transaction, *this),
           on_failure(transaction.rollback()));
  D()->definition->options |= Option::Versioning;
  cres_try(QList<knowRDF::Triple> ts, triples());
  if(not ts.isEmpty())
  {
    VersionControl::Manager* manager = versionControlManager();
    VersionControl::DeltaBuilder deltaBuilder;
    deltaBuilder.reportInsertion(ts);
    VersionControl::RevisionBuilder revisionBuilder
      = manager->insertRevision(DatabaseInterface::PostgreSQL::SQLInterface::computeContentHash(
        transaction, D()->definition->table));
    cres_try(QByteArray head_hash,
             VersionControl::Utils::MetaVersion::getHead(transaction, D()->definition));
    revisionBuilder.addDelta(head_hash, deltaBuilder.delta(), true);
    cres_try(VersionControl::Revision rev, revisionBuilder.insert(transaction, true));

    knowCore::ValueHash new_head_map;
    new_head_map.insert("id", rev.d->revisionId);
    new_head_map.insert("hash", QString::fromLatin1(rev.hash().toHex()));
    VersionControl::Utils::MetaVersion::setHead(transaction, D()->definition, rev.d->revisionId,
                                                rev.hash());
  }
  cres_try(cres_ignore, transaction.commit());
  return cres_success();
}

VersionControl::Manager* TripleStore::versionControlManager()
{
  if(D()->manager == nullptr)
  {
    D()->manager = new VersionControl::Manager(D()->connection, D()->definition);
  }
  return D()->manager;
}

const VersionControl::Manager* TripleStore::versionControlManager() const { return D()->manager; }

QByteArray TripleStore::contentHash(const Transaction& _transaction) const
{
  return DatabaseInterface::PostgreSQL::SQLInterface::computeContentHash(
    {_transaction, D()->connection}, D()->definition->table);
}

cres_qresult<void> TripleStore::disableNotifications()
{
  if(D()->definition->options.testFlag(Option::Notifications))
  {
    cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::disableNotifications(
                            D()->connection, *this));
    D()->definition->options &= ~(int)Option::Notifications;
    return cres_success();
  }
  else
  {
    return cres_failure("Notifications are not enabled!");
  }
}

cres_qresult<void> TripleStore::enableNotifications()
{
  if(D()->definition->options.testFlag(Option::Notifications))
  {
    return cres_failure("Notifications are already enabled");
  }
  cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::enableNotifications(
                          D()->connection, *this));
  D()->definition->options |= Option::Notifications;
  return cres_success();
}

bool TripleStore::unlisten(QMetaObject::Connection _connection)
{
  return D()->connection.notificationsManager()->unlisten(_connection);
}

QMetaObject::Connection TripleStore::listen(const QObject* receiver, const char* member,
                                            Qt::ConnectionType _type)
{
  if(not D()->definition->options.testFlag(Option::Notifications))
  {
    clog_warning("Notifications are not enabled on '{}', no changes will be reported",
                 D()->definition->name);
  }
  return D()->connection.notificationsManager()->listen(
    DatabaseInterface::PostgreSQL::SQLInterface::notificationsChannel(*this).toLatin1().data(),
    receiver, member, _type);
}

QMetaObject::Connection TripleStore::listen(const std::function<void(const QByteArray&)>& _receiver)
{
  if(not D()->definition->options.testFlag(Option::Notifications))
  {
    clog_warning("Notifications are not enabled on '{}', no changes will be reported",
                 D()->definition->name);
  }
  return D()->connection.notificationsManager()->listen(
    DatabaseInterface::PostgreSQL::SQLInterface::notificationsChannel(*this).toLatin1().data(),
    _receiver);
}

QMetaObject::Connection TripleStore::listenOptionsChanged(const QObject* _receiver,
                                                          const char* _member,
                                                          Qt::ConnectionType _type)
{
  return QObject::connect(&D()->definition->notifications, SIGNAL(optionsChanged()), _receiver,
                          _member, _type);
}

QMetaObject::Connection TripleStore::listenOptionsChanged(const std::function<void()>& _receiver)
{
  return QObject::connect(&D()->definition->notifications,
                          &TripleStoreNotifications::optionsChanged, _receiver);
}

QMetaObject::Connection TripleStore::listenMetaChanged(const QObject* _receiver,
                                                       const char* _member,
                                                       Qt::ConnectionType _type)
{
  return QObject::connect(&D()->definition->notifications, SIGNAL(metaChanged()), _receiver,
                          _member, _type);
}

QMetaObject::Connection TripleStore::listenMetaChanged(const std::function<void()>& _receiver)
{
  return QObject::connect(&D()->definition->notifications, &TripleStoreNotifications::metaChanged,
                          _receiver);
}
