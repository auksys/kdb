#include "Transaction_p.h"

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <clog_qt>
#include <cres_clog>

#include "Connection.h"

#include "DatabaseInterface/PostgreSQL/Logging.h"
#include "DatabaseInterface/PostgreSQL/SQLQueryExecutor.h"
#include "SPARQLExecution/QueryExecutor.h"

using namespace kDB::Repository;

Transaction::Private::~Private()
{
  clog_assert(not handle.connection());
  if(handle.connection())
  {
    clog_error("Transaction was not closed, auto-aborting, this might be an error.");
    rollback();
  }
}

cres_qresult<void> Transaction::Private::commit()
{
  for(VersionControl::Transaction t : triplesStoreTransactions)
  {
    auto const [success, error] = t.commit();
    if(not success)
    {
      cres_try(cres_ignore, rollback(),
               message("Failed to commit version control transaction and rollback: {} {}", error.value()));
      return cres_log_failure("Failed to commit version control transaction: {}", error.value());
    }
  }
  PGresult* result = PQexec(handle.connection(), "COMMIT");
  bool success = PQresultStatus(result) == PGRES_COMMAND_OK;
  handle.release();
  connection = Connection();
  if(success)
  {
    for(const std::function<void()>& f : executeOnSuccessfulCommit)
    {
      f();
    }
    return cres_success();
  }
  else
  {
    QString err = clog_qt::qformat("Failed to commit transaction: {} status is {} !",
                                   PQresultErrorMessage(result), PQresultStatus(result));
    PQclear(result);
    rollback();
    return cres_failure(err);
  }
}

cres_qresult<void> Transaction::Private::rollback()
{
  QString accumulatedError;
  bool failed = false;
  for(VersionControl::Transaction t : triplesStoreTransactions)
  {
    auto const& [s, m] = t.rollback();
    if(not s)
    {
      failed = true;
      accumulatedError += m.value().get_message() + ";";
    }
  }
  triplesStoreTransactions.clear();
  PGresult* result = PQexec(handle.connection(), "ROLLBACK");
  PQclear(result);
  handle.release();
  connection = Connection();
  if(failed)
  {
    return cres_failure(accumulatedError);
  }
  else
  {
    return cres_success();
  }
}

Transaction::Transaction() : d(nullptr) {}

Transaction::Transaction(const Connection& _connection) : d(new Private(_connection))
{
  PGresult* result = PQexec(d->handle.connection(), "BEGIN");
  bool success = PQresultStatus(result) == PGRES_COMMAND_OK;
  if(not success)
  {
    clog_error("Error while executing BEGIN of transaction: '{}'", PQresultErrorMessage(result));
  }
  clog_assert(success);
  PQclear(result);
}

Transaction::Transaction(const knowCore::WeakReference<Connection>& _connection)
    : Transaction(Connection(_connection))
{
}

Transaction::Transaction(const Transaction& _rhs) : d(_rhs.d) {}

Transaction& Transaction::operator=(const Transaction& _rhs)
{
  d = _rhs.d;
  return *this;
}

Transaction::~Transaction() {}

bool Transaction::operator==(const Transaction& _rhs) const { return d == _rhs.d; }

bool Transaction::operator!=(const Transaction& _rhs) const
{
  return d->handle.connection() != _rhs.d->handle.connection();
}

bool Transaction::isActive() const { return d != nullptr and d->handle.connection(); }

cres_qresult<void> Transaction::commit() const { return d->commit(); }

cres_qresult<void> Transaction::rollback() const { return d->rollback(); }

void Transaction::executeOnSuccessfulCommit(const std::function<void()>& _function) const
{
  const_cast<Private*>(d.data())->executeOnSuccessfulCommit.append(_function);
}

knowDBC::Query Transaction::createSQLQuery(const QString& _query,
                                           const knowCore::ValueHash& _bindings,
                                           const knowCore::ValueHash& _options) const
{
  knowDBC::Query q(new DatabaseInterface::PostgreSQL::SQLQueryExecutor(*this));
  q.setQuery(_query);
  q.bindValues(_bindings);
  q.setOptions(_options);
  return q;
}

knowDBC::Query Transaction::createSPARQLQuery(const RDFEnvironment& _environment,
                                              const QString& _query,
                                              const knowCore::ValueHash& _bindings,
                                              const knowCore::ValueHash& _options) const
{
  knowDBC::Query q(
    new SPARQLExecution::QueryExecutor(RDFEnvironment(_environment).setConnection(*this)));
  q.setQuery(_query);
  q.bindValues(_bindings);
  q.setOptions(_options);
  return q;
}
