#ifndef _KDB_REPOSITORY_TRANSACTION_H_
#define _KDB_REPOSITORY_TRANSACTION_H_

#include <functional>
#include <knowCore/ValueHash.h>

#include "RDFEnvironment.h"

namespace kDB
{
  namespace Repository
  {
    /**
     * @ingroup kDB_Repository
     *
     * This class represents a transaction over the database.
     */
    class Transaction
    {
      friend class QueryConnectionInfo;
      friend class TripleStore;
      friend class VersionControl::Transaction;
      friend class DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal;
      friend class DatabaseInterface::PostgreSQL::SQLCopyData;
      friend class DatabaseInterface::PostgreSQL::SQLReadData;
      friend class DatabaseInterface::PostgreSQL::SQLInterface;
    public:
      Transaction();
      explicit Transaction(const Connection& _connection);
      explicit Transaction(const knowCore::WeakReference<Connection>& _connection);
      Transaction(const Transaction& _rhs);
      Transaction& operator=(const Transaction& _rhs);
      ~Transaction();
      bool operator==(const Transaction& _rhs) const;
      bool operator!=(const Transaction& _rhs) const;
      bool isActive() const;
      cres_qresult<void> commit() const;
      cres_qresult<void> rollback() const;
      void executeOnSuccessfulCommit(const std::function<void()>& _function) const;
      /**
       * Create a SQL query for this transaction
       */
      knowDBC::Query createSQLQuery(const QString& _query = QString(),
                                    const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                    const knowCore::ValueHash& _options
                                    = knowCore::ValueHash()) const;
      /**
       * Create a SPARQL query for this transaction
       */
      knowDBC::Query createSPARQLQuery(const RDFEnvironment& _environment = RDFEnvironment(),
                                       const QString& _query = QString(),
                                       const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                       const knowCore::ValueHash& _options
                                       = knowCore::ValueHash()) const;
    private:
      struct Private;
      QSharedPointer<Private> d;
      Transaction(const QWeakPointer<Private>& _d) : d(_d) {}
    };
  } // namespace Repository
} // namespace kDB

#endif
