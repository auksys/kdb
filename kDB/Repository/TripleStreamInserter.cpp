#include "TripleStreamInserter.h"

#include "Connection.h"
#include "TripleStore.h"

using namespace kDB::Repository;

struct TripleStreamInserter::Private
{
  TripleStore interface;
};

TripleStreamInserter::TripleStreamInserter(const TripleStore& _triplesStore) : d(new Private())
{
  d->interface = _triplesStore;
}

TripleStreamInserter::~TripleStreamInserter() { delete d; }

void TripleStreamInserter::triple(const knowRDF::Triple& _triple) { d->interface.insert(_triple); }
