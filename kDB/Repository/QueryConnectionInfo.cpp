#include "QueryConnectionInfo_p.h"

#include <knowDBC/Query.h>

#include "Connection.h"
#include "Transaction_p.h"

#include "DatabaseInterface/PostgreSQL/SQLQueryExecutor.h"
#include "SPARQLExecution/QueryExecutor.h"

using namespace kDB::Repository;

QSharedPointer<ConnectionHandle> QueryConnectionInfo::Private::connectionHandle()
{
  if(transaction.isActive())
  {
    return QSharedPointer<ConnectionHandle>(&transaction.d->handle,
                                            [](const ConnectionHandle*) { /* don't delete */ });
  }
  else
  {
    return QSharedPointer<ConnectionHandle>(new ConnectionHandle(connection));
  }
}

QueryConnectionInfo::QueryConnectionInfo(const Connection& _connection) : d(new Private)
{
  setConnection(_connection);
}

QueryConnectionInfo::QueryConnectionInfo(const knowCore::WeakReference<Connection>& _connection)
    : QueryConnectionInfo(Connection(_connection))
{
}

QueryConnectionInfo::QueryConnectionInfo(const Transaction& _transaction) : d(new Private)
{
  clog_assert(_transaction.isActive());
  setTransaction(_transaction);
}

QueryConnectionInfo::QueryConnectionInfo::QueryConnectionInfo(const Transaction& _transaction,
                                                              const Connection& _connection)
    : d(new Private)
{
  if(_transaction.isActive())
  {
    setTransaction(_transaction);
  }
  else
  {
    setConnection(_connection);
  }
}

QueryConnectionInfo::QueryConnectionInfo(const QueryConnectionInfo& _rhs) : d(_rhs.d) {}

QueryConnectionInfo& QueryConnectionInfo::operator=(const QueryConnectionInfo& _rhs)
{
  d = _rhs.d;
  return *this;
}

bool QueryConnectionInfo::operator==(const QueryConnectionInfo& _rhs) const
{
  return d == _rhs.d
         or (d->connection == _rhs.d->connection and d->transaction == _rhs.d->transaction);
}

QueryConnectionInfo::~QueryConnectionInfo() {}

Connection QueryConnectionInfo::connection() const { return d->connection; }

Transaction QueryConnectionInfo::transaction() const { return d->transaction; }

void QueryConnectionInfo::setConnection(const Connection& _connection)
{
  d->connection = _connection;
}

void QueryConnectionInfo::setTransaction(const Transaction& _transaction)
{
  d->transaction = _transaction;
  d->connection = _transaction.d->connection;
}

void QueryConnectionInfo::startTransaction() { d->transaction = Transaction(d->connection); }

void QueryConnectionInfo::removeTransaction() { d->transaction = Transaction(); }

bool QueryConnectionInfo::isValid() const
{
  return (d->connection.isValid() and d->connection.isConnected()) or d->transaction.isActive();
}

knowDBC::Query QueryConnectionInfo::createSQLQuery(const QString& _query,
                                                   const knowCore::ValueHash& _bindings,
                                                   const knowCore::ValueHash& _options) const
{
  knowDBC::Query q(new DatabaseInterface::PostgreSQL::SQLQueryExecutor(*this));
  q.setQuery(_query);
  q.bindValues(_bindings);
  q.setOptions(_options);
  return q;
}

knowDBC::Query QueryConnectionInfo::createSPARQLQuery(const RDFEnvironment& _environment,
                                                      const QString& _query,
                                                      const knowCore::ValueHash& _bindings,
                                                      const knowCore::ValueHash& _options) const
{
  knowDBC::Query q(
    new SPARQLExecution::QueryExecutor(RDFEnvironment(_environment).setConnection(*this)));
  q.setQuery(_query);
  q.bindValues(_bindings);
  q.setOptions(_options);
  return q;
}
