#include "PersistentDatasetsUnion.h"

#include <QMutex>

#include <knowCore/UriList.h>

#include "RDFDataset_p.h"

namespace kDB::Repository
{
  struct PersistentDatasetsUnion::Definition
  {
    QMutex mutex;
    knowCore::Uri name;
    knowCore::UriList datasets;
  };
  struct PersistentDatasetsUnion::Private : public RDFDataset::Private
  {
    Private(const Connection& _connection)
        : RDFDataset::Private(RDFDataset::Type::PersistentDatasetsUnion, _connection)
    {
    }
    virtual ~Private();
    QSharedPointer<Definition> definition;
    bool isValid() const override;
    knowCore::Uri uri() const override;
  };
} // namespace kDB::Repository
