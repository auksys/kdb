#include "RDFDataset.h"

namespace kDB
{
  namespace Repository
  {
    class EmptyRDFDataset : public RDFDataset
    {
    public:
      EmptyRDFDataset(const Connection& _connection);
      ~EmptyRDFDataset();
    };
  } // namespace Repository
} // namespace kDB
