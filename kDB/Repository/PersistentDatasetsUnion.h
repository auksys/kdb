#include <knowCore/Global.h>

#include "RDFDataset.h"
#include "Transaction.h"

namespace kDB::Repository
{
  class PersistentDatasetsUnion : public RDFDataset
  {
    friend class GraphsManager;
    struct Definition;
  private:
    PersistentDatasetsUnion(const kDB::Repository::Connection& _connection,
                            QSharedPointer<Definition> _definition);
  public:
    PersistentDatasetsUnion();
    PersistentDatasetsUnion(const PersistentDatasetsUnion& _rhs);
    PersistentDatasetsUnion& operator=(const PersistentDatasetsUnion& _rhs);
    ~PersistentDatasetsUnion();
  public:
    /**
     * @return true if the union contains the \ref RDFDataset \p _uri
     */
    bool contains(const knowCore::Uri& _uri) const;
    QList<RDFDataset> datasets() const;
    void add(const RDFDataset& _graph, const Transaction& _transaction = Transaction());
    void remove(const RDFDataset& _graph, const Transaction& _transaction = Transaction());
    DatasetsUnion toDatasetsUnion() const;
    QString tablename() const;
  private:
    KNOWCORE_D_DECL();
  };
} // namespace kDB::Repository
