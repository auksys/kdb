#include "PersistentDatasetsUnion_p.h"

#include <clog_qt>

#include "DatabaseInterface/PostgreSQL/SQLInterface_p.h"
#include "DatasetsUnion.h"
#include "GraphsManager.h"
#include "QueryConnectionInfo.h"
#include "TemporaryTransaction.h"

using namespace kDB::Repository;

KNOWCORE_D_FUNC_DEF(PersistentDatasetsUnion)

PersistentDatasetsUnion::Private::~Private() {}

bool PersistentDatasetsUnion::Private::isValid() const
{
  QMutexLocker l(&definition->mutex);
  return not definition->datasets.isEmpty();
}

knowCore::Uri PersistentDatasetsUnion::Private::uri() const { return definition->name; }

PersistentDatasetsUnion::PersistentDatasetsUnion(
  const Connection& _connection, QSharedPointer<PersistentDatasetsUnion::Definition> _definition)
    : RDFDataset(new Private(_connection))
{
  clog_assert(not _definition->name.isEmpty());
  D()->definition = _definition;
}

PersistentDatasetsUnion::PersistentDatasetsUnion(const PersistentDatasetsUnion& _rhs)
    : RDFDataset(_rhs.d.data())
{
}

PersistentDatasetsUnion::PersistentDatasetsUnion() {}

PersistentDatasetsUnion& PersistentDatasetsUnion::operator=(const PersistentDatasetsUnion& _rhs)
{
  d = _rhs.d;
  return *this;
}

PersistentDatasetsUnion::~PersistentDatasetsUnion() {}

void PersistentDatasetsUnion::add(const RDFDataset& _graph, const Transaction& _transaction)
{
  QMutexLocker l(&D()->definition->mutex);
  knowCore::UriList datasets = D()->definition->datasets;
  l.unlock();
  TemporaryTransaction transaction(_transaction, d->connection);
  DatabaseInterface::PostgreSQL::SQLInterface::addToUnion(transaction.transaction(),
                                                          D()->definition->name, _graph.uri());
  datasets.append(_graph.uri());
  DatabaseInterface::PostgreSQL::SQLInterface::updateUnionView(
    transaction.transaction(), false, tablename(),
    d->connection.graphsManager()->datasets(datasets));
  transaction.commitIfOwned();
  l.relock();
  if(not D()->definition->datasets.contains(_graph.uri()))
  {
    D()->definition->datasets.append(_graph.uri());
  }
}

void PersistentDatasetsUnion::remove(const RDFDataset& _graph, const Transaction& _transaction)
{
  QMutexLocker l(&D()->definition->mutex);
  knowCore::UriList datasets = D()->definition->datasets;
  l.unlock();
  TemporaryTransaction transaction(_transaction, d->connection);
  DatabaseInterface::PostgreSQL::SQLInterface::removeFromUnion(transaction.transaction(), D()->definition->name,
                                                               _graph.uri());
  datasets.removeAll(_graph.uri());
  DatabaseInterface::PostgreSQL::SQLInterface::updateUnionView(
    transaction.transaction(), false, tablename(), d->connection.graphsManager()->datasets(datasets));
  transaction.commitIfOwned();
  l.relock();
  if(not D()->definition->datasets.contains(_graph.uri()))
  {
    D()->definition->datasets.removeAll(_graph.uri());
  }
}

bool PersistentDatasetsUnion::contains(const knowCore::Uri& _uri) const
{
  QMutexLocker l(&D()->definition->mutex);
  return D()->definition->datasets.contains(_uri);
}

QList<RDFDataset> PersistentDatasetsUnion::datasets() const
{
  QMutexLocker l(&D()->definition->mutex);
  return d->connection.graphsManager()->datasets(D()->definition->datasets);
}

DatasetsUnion PersistentDatasetsUnion::toDatasetsUnion() const
{
  DatasetsUnion du;
  du.add(datasets());
  return du;
}

QString PersistentDatasetsUnion::tablename() const
{
  return DatabaseInterface::PostgreSQL::SQLInterface::persistentDatasetsUnionTableName(
    d->connection, D()->definition->name);
}
