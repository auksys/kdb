#include "NotificationsManager.h"

#include <libpq-fe.h>

#include <QHash>
#include <QMutex>

#include <clog_qt>
#include <knowCore/IoEvent.h>

#include "NotificationHandler_p.h"

using namespace kDB::Repository;

struct NotificationsManager::Private
{
  knowCore::IoEvent ioevent;
  PGconn* connection = nullptr;
  QHash<QByteArray, NotificationHandler*> handlers;
  bool running = false;
  QMutex mutex;
  NotificationsManager* self;
  NotificationHandler* get_handler(const QByteArray& _key);
};

kDB::Repository::NotificationHandler*
  kDB::Repository::NotificationsManager::Private::get_handler(const QByteArray& _key)
{
  kDB::Repository::NotificationHandler* handler = handlers.value(_key, nullptr);
  if(not handler)
  {
    QMutexLocker l(&mutex);
    QByteArray q = "LISTEN " + _key;
    PGresult* res = PQexec(connection, q.data());
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
      clog_error("Failed to start listening on {}: {}", QString::fromLatin1(_key),
                 PQerrorMessage(connection));
      PQclear(res);
      return nullptr;
    }
    PQclear(res);
    handler = new NotificationHandler();
    handler->moveToThread(self);
    handlers[_key] = handler;
  }
  return handler;
}

NotificationsManager::NotificationsManager() : d(new Private) { d->self = this; }

NotificationsManager::~NotificationsManager() { delete d; }

QMetaObject::Connection NotificationsManager::listen(const char* _channel, const QObject* receiver,
                                                     const char* member, Qt::ConnectionType _type)
{
  QByteArray key = _channel;
  NotificationHandler* handler = d->get_handler(key);
  if(not handler)
    return QMetaObject::Connection();
  return QObject::connect(handler, SIGNAL(triggered()), receiver, member, _type);
}

QMetaObject::Connection
  NotificationsManager::listen(const char* _channel,
                               const std::function<void(const QByteArray&)>& _receiver)
{
  QByteArray key = _channel;
  NotificationHandler* handler = d->get_handler(key);
  if(not handler)
    return QMetaObject::Connection();
  return QObject::connect(handler, &NotificationHandler::triggered, _receiver);
}

bool NotificationsManager::unlisten(const char* _channel, const QObject* receiver,
                                    const char* member)
{
  QMutexLocker l(&d->mutex);
  QByteArray key = _channel;
  NotificationHandler* handler = d->handlers.value(key, nullptr);
  if(handler)
  {
    return QObject::disconnect(handler, SIGNAL(triggered()), receiver, member);
  }
  return false;
}

bool NotificationsManager::unlisten(const QMetaObject::Connection& _connection)
{
  QMutexLocker l(&d->mutex);
  return QObject::disconnect(_connection);
}

void NotificationsManager::stop()
{
  if(not d->running)
    return;
  d->running = false;
  {
    QMutexLocker l(&d->mutex);
    d->ioevent.set();
  }
  wait();
  {
    QMutexLocker l(&d->mutex);
    PQfinish(d->connection);
  }
  for(NotificationHandler* nh : d->handlers.values())
  {
    delete nh;
  }
  d->handlers.clear();
}

void NotificationsManager::start(void* _connection)
{
  d->connection = reinterpret_cast<PGconn*>(_connection);
  d->running = true;
  QThread::start();
}

void NotificationsManager::run()
{

  while(d->running)
  {
    int sock;
    fd_set input_mask;

    {
      QMutexLocker l(&d->mutex);
      sock = PQsocket(d->connection);
      if(PQstatus(d->connection) != CONNECTION_OK)
      {
        clog_warning(
          "Disconnected from the database, notifications might not work properly anymore!");
        return;
      }
      FD_ZERO(&input_mask);
      FD_SET(sock, &input_mask);
    }

    FD_SET(d->ioevent.fd(), &input_mask);

    if(select(std::max(sock, d->ioevent.fd()) + 1, &input_mask, NULL, NULL, NULL) < 0)
    {
      clog_error("select() failed: {}", strerror(errno));
      return;
    }
    d->ioevent.reset();

    QMutexLocker l(&d->mutex);
    PQconsumeInput(d->connection);
    PGnotify* notify;
    while((notify = PQnotifies(d->connection)) != NULL)
    {
      QByteArray key = notify->relname;
      QByteArray payload = notify->extra;
      PQfreemem(notify);
      NotificationHandler* handler = d->handlers.value(key, nullptr);
      if(handler)
      {
        emit(handler->triggered(payload));
      }
    }
  }
}
