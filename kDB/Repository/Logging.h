#include <clog_qt>
#include <cres_clog>

#define KDB_REPOSITORY_REPORT_QUERY_ERROR_FORMATED_MSG(_MSG_, _RESULT_, ...)                       \
  "in SQLQuery: {} with error {} in query {}", clog_qt::qformat(_MSG_ __VA_OPT__(, ) __VA_ARGS__), \
    _RESULT_.error(), _RESULT_.query()

#define KDB_REPOSITORY_REPORT_QUERY_ERROR(_MSG_, _RESULT_, ...)                                    \
  clog_error(                                                                                      \
    KDB_REPOSITORY_REPORT_QUERY_ERROR_FORMATED_MSG(_MSG_, _RESULT_ __VA_OPT__(, ) __VA_ARGS__))

#define KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN(_MSG_, _RESULT_, ...)                             \
  return cres_log_failure(                                                                         \
    KDB_REPOSITORY_REPORT_QUERY_ERROR_FORMATED_MSG(_MSG_, _RESULT_ __VA_OPT__(, ) __VA_ARGS__));

#define KDB_REPOSITORY_EXECUTE_QUERY(_MSG_, _QUERY_, ...)                                          \
  {                                                                                                \
    auto result = _QUERY_.execute();                                                               \
    if(not result)                                                                                 \
    {                                                                                              \
      KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN(_MSG_, result __VA_OPT__(, ) __VA_ARGS__);          \
    }                                                                                              \
  }
