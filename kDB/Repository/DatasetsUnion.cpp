#include "DatasetsUnion.h"

#include <clog_qt>
#include <knowCore/Uri.h>

#include "RDFDataset_p.h"

using namespace kDB::Repository;

struct DatasetsUnion::Private : public RDFDataset::Private
{
  Private() : RDFDataset::Private(RDFDataset::Type::DatasetsUnion) {}
  QList<RDFDataset> graphs;
  QHash<QString, RDFDataset> named_graphs;
  bool isValid() const override
  {
    for(const RDFDataset& atg : graphs)
    {
      if(not atg.isValid())
        return false;
    }
    return graphs.size() > 0;
  }
};

KNOWCORE_D_FUNC_DEF(DatasetsUnion)

DatasetsUnion::DatasetsUnion() : RDFDataset(new Private) {}

DatasetsUnion::DatasetsUnion(const DatasetsUnion& _rhs) : RDFDataset(0) { d = _rhs.d; }

DatasetsUnion& DatasetsUnion::operator=(const DatasetsUnion& _rhs)
{
  d = _rhs.d;
  return *this;
}

DatasetsUnion::~DatasetsUnion() {}

void DatasetsUnion::add(const RDFDataset& _graph)
{
  switch(_graph.type())
  {
  case RDFDataset::Type::DatasetsUnion:
  {
    QExplicitlySharedDataPointer<Private> d = _graph.toDatasetsUnion().D();
    D()->graphs.append(d->graphs);
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
    D()->named_graphs = D()->named_graphs.unite(d->named_graphs);
#else
    D()->named_graphs.insert(d->named_graphs);
#endif
    break;
  }
  default:
  {
    D()->graphs.append(_graph);
    break;
  }
  }
  if(not D()->connection.isValid() and D()->graphs.size() > 0)
  {
    D()->connection = D()->graphs.first().connection();
  }
}

void DatasetsUnion::add(const QList<RDFDataset>& _graphs)
{
  for(RDFDataset rdfd : _graphs)
  {
    add(rdfd);
  }
}

void DatasetsUnion::add(const knowCore::Uri& _uri, const RDFDataset& _graph)
{
  clog_assert(_graph.type() == RDFDataset::Type::TripleStore
              or _graph.type() == RDFDataset::Type::TriplesView);

  D()->named_graphs[_uri] = _graph;
}

DatasetsUnion DatasetsUnion::operator||(const RDFDataset& _graph) const
{
  DatasetsUnion tgu;
  tgu.D()->graphs = D()->graphs;
  tgu.add(_graph);
  return tgu;
}

QList<RDFDataset> DatasetsUnion::datasets() const { return D()->graphs; }
