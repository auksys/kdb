#include <arpa/inet.h>

#include <knowCore/Array.h>
#include <knowCore/BigNumber.h>
#include <knowCore/Vector.h>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include "postgresql_p.h"

#include "BinaryInterface_traits_p.h"

namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface
{
  template<typename _T_>
  struct write_details
  {
    static inline std::size_t do_calculate_size(const _T_&) { return sizeof(_T_); }
  };

  template<typename _T_>
  inline std::size_t calculate_size(const _T_& _v)
  {
    return write_details<_T_>::do_calculate_size(_v);
  }

  template<typename _T_>
  inline void write(const _T_& _value, char* _data)
  {
    write_details<_T_>::do_write(_value, _data);
  }

  template<typename _T_>
  inline void write(const _T_& _value, QByteArray* _array)
  {
    _array->resize(calculate_size<_T_>(_value));
    write(_value, _array->data());
  }
  template<typename _T_>
  inline QByteArray write(_T_ _value) Q_REQUIRED_RESULT;
  template<typename _T_>
  inline QByteArray write(_T_ _value)
  {
    QByteArray r;
    write<_T_>(_value, &r);
    return r;
  }

  template<>
  inline void write<qint64>(const qint64& _value, char* _data)
  {
    qint32* h32 = (qint32*)_data;
    qint32* l32 = (qint32*)(_data + 4);

    *h32 = htonl(_value >> 32);
    *l32 = htonl(_value & 0xFFFFFFFF);
  }
  template<>
  inline void write<quint64>(const quint64& _value, char* _data)
  {
    write<qint64>(_value, _data);
  }
  template<>
  inline void write<qint32>(const qint32& _value, char* _data)
  {
    *reinterpret_cast<qint32*>(_data) = htonl(_value);
  }
  template<>
  inline void write<quint32>(const quint32& _value, char* _data)
  {
    write<qint32>(_value, _data);
  }
  template<>
  inline void write<qint16>(const qint16& _value, char* _data)
  {
    *reinterpret_cast<qint16*>(_data) = htons(_value);
  }
  template<>
  inline void write<quint16>(const quint16& _value, char* _data)
  {
    write<qint16>(_value, _data);
  }
  template<>
  inline void write<bool>(const bool& _value, char* _data)
  {
    *_data = _value ? 1 : 0;
  }

  template<>
  inline void write<float>(const float& _fl, char* data)
  {
    union
    {
      float f;
      qint32 i;
    } swap;
    swap.f = _fl;
    write<qint32>(swap.i, data);
  }
  template<>
  inline void write<double>(const double& _fl, char* data)
  {
    union
    {
      double f;
      qint64 i;
    } swap;
    swap.f = _fl;
    write<qint64>(swap.i, data);
  }
  template<>
  inline std::size_t calculate_size<knowCore::Timestamp>(const knowCore::Timestamp& _v)
  {
    Q_UNUSED(_v);
    return calculate_size<qint64>(0);
  }
  template<>
  inline void write<knowCore::Timestamp>(const knowCore::Timestamp& _dt, char* _data)
  {
    Q_UNUSED(_dt);
    Q_UNUSED(_data);
    qFatal("Unimplemented");
  }
  template<>
  inline std::size_t calculate_size<QString>(const QString& _v)
  {
    return _v.toUtf8().size();
  }
  template<>
  inline void write<QString>(const QString& _dt, char* data)
  {
    QByteArray r = _dt.toUtf8();
    memcpy(data, r.data(), r.size());
  }

  template<>
  inline std::size_t calculate_size<QByteArray>(const QByteArray& _v)
  {
    return _v.size();
  }
  template<>
  inline void write<QByteArray>(const QByteArray& _dt, char* data)
  {
    std::copy(_dt.begin(), _dt.end(), data);
  }

  template<>
  inline void write<knowCore::BigNumber>(const knowCore::BigNumber& _number, char* data)
  {
    write<quint16>(_number.digits().size(), data);
    data += 2;
    write<quint16>(_number.weight(), data);
    data += 2;
    quint16 sign = 0xC000;
    switch(_number.sign())
    {
    case knowCore::BigNumber::Sign::Negative:
      sign = 0x4000;
      break;
    case knowCore::BigNumber::Sign::Positive:
      sign = 0x0000;
      break;
    case knowCore::BigNumber::Sign::NaN:
      sign = 0xC000;
      break;
    case knowCore::BigNumber::Sign::PositiveInfinite:
      sign = 0xD000;
      break;
    case knowCore::BigNumber::Sign::NegativeInfinite:
      sign = 0xF000;
      break;
    }
    write<quint16>(sign, data);
    data += 2;
    write<quint16>(_number.dscale(), data);
    data += 2;

    for(quint16 d : _number.digits())
    {
      write<quint16>(d, data);
      data += 2;
    }
  }
  template<>
  inline std::size_t calculate_size<knowCore::BigNumber>(const knowCore::BigNumber& _number)
  {
    return (4 + _number.digits().size()) * sizeof(quint16);
  }
  namespace details
  {
    inline char* write_enum(const char* enum_name, char* data)
    {
      const std::size_t s = strlen(enum_name);
      write<quint32>(s, data);
      data += sizeof(quint32);
      std::copy(enum_name, enum_name + s, data);
      data += s;
      return data;
    }
    inline char* write_empty(Oid _oid, char* data)
    {
      write<Oid>(_oid, data);
      data += sizeof(Oid);
      write<quint32>(0xFFFFFFFF, data);
      data += sizeof(quint32);
      return data;
    }
    template<typename _T_>
    inline char* write_value(Oid _oid, const _T_& _value, char* data)
    {
      std::size_t s = calculate_size<_T_>(_value);
      write<Oid>(_oid, data);
      data += sizeof(Oid);
      write<quint32>(s, data);
      data += sizeof(quint32);
      write<_T_>(_value, data);
      data += s;
      return data;
    }
  } // namespace details
  template<typename _T_>
  struct write_details<QList<_T_>>
  {
    static inline std::size_t do_calculate_size(const QList<_T_>& _val)
    {
      std::size_t s = 4 * 5;
      for(const _T_& t : _val)
      {
        s += 4 + calculate_size<_T_>(t);
      }
      return s;
    }
    static inline void do_write(const QList<_T_>& _value, char* data)
    {
      write<quint32>(1, data);
      data += 4; // ndim

      write<quint32>(0, data);
      data += 4; // has_null
      write<quint32>(traits<_T_>::oid(), data);
      data += 4;

      write<quint32>(_value.size(), data);
      data += 4;
      write<quint32>(0, data);
      data += 4; // lbound

      for(int i = 0; i < _value.size(); ++i)
      {
        const _T_& val = _value[i];
        std::size_t s = calculate_size<_T_>(val);
        write<quint32>(s, data);
        data += 4;
        write<_T_>(val, data);
        data += s;
      }
    }
  };
  template<typename _T_, std::size_t _dimension>
  struct write_details<knowCore::Vector<_T_, _dimension>>
  {
    static inline std::size_t do_calculate_size(const knowCore::Vector<_T_, _dimension>& _val)
    {
      return 4 * 5 + _dimension * (4 + calculate_size<_T_>(_val[0]));
    }
    static inline void do_write(const knowCore::Vector<_T_, _dimension>& _value, char* data)
    {
      write<QList<_T_>>(_value, data);
    }
  };

  template<typename _T_, std::size_t _dimension>
  struct write_details<QList<knowCore::Vector<_T_, _dimension>>>
  {
    static inline std::size_t
      do_calculate_size(const QList<knowCore::Vector<_T_, _dimension>>& _val)
    {
      std::size_t s = 4 * 7;
      for(const knowCore::Vector<_T_, _dimension>& t : _val)
      {
        for(std::size_t j = 0; j < _dimension; ++j)
        {
          s += 4 + calculate_size<_T_>(t[j]);
        }
      }
      return s;
    }
    static inline void do_write(const QList<knowCore::Vector<_T_, _dimension>>& _value, char* data)
    {
      write<quint32>(2, data);
      data += 4; // ndim

      write<quint32>(0, data);
      data += 4; // has_null
      write<quint32>(traits<_T_>::oid(), data);
      data += 4;

      write<quint32>(_value.size(), data);
      data += 4;
      write<quint32>(0, data);
      data += 4; // lbound
      write<quint32>(_dimension, data);
      data += 4;
      write<quint32>(0, data);
      data += 4; // lbound

      for(int i = 0; i < _value.size(); ++i)
      {
        const knowCore::Vector<_T_, _dimension>& v = _value[i];
        for(std::size_t j = 0; j < _dimension; ++j)
        {
          std::size_t s = calculate_size<_T_>(v[j]);
          write<quint32>(s, data);
          data += 4;
          write<_T_>(v[j], data);
          data += s;
        }
      }
    }
  };
  template<typename _T_>
  struct write_details<knowCore::Array<_T_>>
  {
    static inline std::size_t do_calculate_size(const knowCore::Array<_T_>& _val)
    {
      std::size_t s = 4 * (3 + 2 * _val.dimensions().size());
      for(const _T_& t : _val.data())
      {
        s += 4 + calculate_size<_T_>(t);
      }
      return s;
    }
    static inline void do_write(const knowCore::Array<_T_>& _value, char* data)
    {
      write<quint32>(_value.dimensions().size(), data);
      data += 4; // ndim

      write<quint32>(0, data);
      data += 4; // has_null
      write<quint32>(traits<_T_>::oid(), data);
      data += 4;

      for(int d : _value.dimensions())
      {
        write<quint32>(d, data);
        data += 4;
        write<quint32>(0, data);
        data += 4; // lbound
      }

      for(const _T_& val : _value.data())
      {
        std::size_t s = calculate_size<_T_>(val);
        write<quint32>(s, data);
        data += 4;
        write<_T_>(val, data);
        data += s;
      }
    }
  };

  // Json
  template<>
  inline void write<QJsonDocument>(const QJsonDocument& _document, QByteArray* _array)
  {
    write(_document.toJson(QJsonDocument::Compact), _array);
  }
  template<>
  inline void write<QJsonArray>(const QJsonArray& _value, QByteArray* _array)
  {
    QJsonDocument doc;
    doc.setArray(_value);
    write(doc, _array);
  }
  template<>
  inline void write<QJsonObject>(const QJsonObject& _value, QByteArray* _array)
  {
    QJsonDocument doc;
    doc.setObject(_value);
    write(doc, _array);
  }
  template<>
  inline void write<QJsonValue>(const QJsonValue& _value, QByteArray* _array)
  {
    switch(_value.type())
    {
    case QJsonValue::Null:
    case QJsonValue::Undefined:
      *_array = QByteArray();
      break;
    case QJsonValue::Bool:
    case QJsonValue::Double:
    case QJsonValue::String:
      clog_fatal("Invalid type of QJsonValue: {}", _value);
    case QJsonValue::Array:
      write(_value.toArray(), _array);
      break;
    case QJsonValue::Object:
      write(_value.toObject(), _array);
      break;
    }
  }

  namespace details
  {
    template<int _N_, typename... T>
    struct struct_writer
    {
      static int do_calculate_size(const std::tuple<T...>& data)
      {
        return struct_writer<_N_ - 1, T...>::do_calculate_size(data)
               + calculate_size(std::get<sizeof...(T) - _N_>(data)) + 4 /* oid */ + 4 /* size */;
      }
      static void do_write(const std::tuple<T...>& data, char* ptr)
      {
        auto v = std::get<sizeof...(T) - _N_>(data);
        std::size_t s = calculate_size(v);
        write(traits<decltype(v)>::oid(), ptr);
        ptr += 4;
        write(s);
        ptr += 4;
        write(v, ptr);
        ptr += s;
        struct_writer<_N_ - 1, T...>::do_write(data, ptr);
      }
    };
    template<typename... T>
    struct struct_writer<0, T...>
    {
      static int do_calculate_size(std::tuple<T...> data)
      {
        Q_UNUSED(data);
        return 4; // For the fields
      }
      static void do_write(const std::tuple<T...>& data, char* ptr)
      {
        Q_UNUSED(data);
        Q_UNUSED(ptr);
      }
    };
  } // namespace details

  template<typename... T>
  struct struct_writer
  {
    struct_writer(Oid oid, T... _t) : m_data(_t...), m_oid(oid) {}
    int calculate_size() const
    {
      return details::struct_writer<sizeof...(T), T...>::do_calculate_size(m_data);
    }
    void write(char* ptr) const
    {
      BinaryInterface::write<quint32>(sizeof...(T), ptr);
      ptr += 4;
      details::struct_writer<0, T...>::do_write(m_data, ptr);
    }
    Oid oid() const { return m_oid; }
  private:
    std::tuple<T...> m_data;
    Oid m_oid;
  };

  template<typename... T>
  struct write_details<struct_writer<T...>>
  {
    static inline std::size_t do_calculate_size(const struct_writer<T...>& _t)
    {
      return _t.calculate_size();
    }
    static inline void write(const struct_writer<T...>& _value, char* _data)
    {
      _value.write(_data);
    }
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface
