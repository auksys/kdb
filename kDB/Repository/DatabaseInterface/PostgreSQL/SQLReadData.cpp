#include "SQLReadData.h"

#include "BinaryInterface_read_p.h"
#include "BinaryInterface_write_p.h"
#include "QueryConnectionInfo_p.h"
#include "Transaction_p.h"

using namespace kDB::Repository::DatabaseInterface::PostgreSQL;

struct SQLReadData::Buffer::Private
{
  ~Private() { PQfreemem(data); }
  std::size_t size = 0;
  char* data = nullptr;
};

SQLReadData::Buffer::Buffer() : d(new Private) {}

SQLReadData::Buffer::Buffer(const Buffer& _rhs) : d(_rhs.d) {}

SQLReadData::Buffer& SQLReadData::Buffer::operator=(const Buffer& _rhs)
{
  d = _rhs.d;
  return *this;
}

SQLReadData::Buffer::~Buffer() {}

std::size_t SQLReadData::Buffer::size() const { return d->size; }

const char* SQLReadData::Buffer::data() const { return d->data; }

QByteArray SQLReadData::Buffer::toByteArray() const { return QByteArray(d->data, d->size); }

bool SQLReadData::Buffer::isNull() const
{
  const unsigned char* c = reinterpret_cast<const unsigned char*>(d->data);
  return d->size == 0 or (d->size == 2 and c[0] == 0xFF and c[1] == 0xFF);
}

struct SQLReadData::Private
{
  Transaction transaction;
  Status status;
  QString errorMessage;
  Buffer currentBuffer;
  std::size_t offset = 0;
};

SQLReadData::SQLReadData(const Transaction& _transaction) : d(new Private)
{
  d->transaction = _transaction;
  d->status = Status::Open;
}

SQLReadData::~SQLReadData() { delete d; }

QString SQLReadData::errorMessage() const { return d->errorMessage; }

SQLReadData::Status SQLReadData::status() const { return d->status; }

SQLReadData::Buffer SQLReadData::readBuffer()
{
  Buffer b;

  int ret = PQgetCopyData(d->transaction.d->handle.connection(), &b.d->data, 0);

  switch(ret)
  {
  case -2:
    d->status = Status::Failed;
    d->errorMessage = PQerrorMessage(d->transaction.d->handle.connection());
    break;
  case -1:
    d->status = Status::Close;
    break;
  default:
    b.d->size = ret;
  }

  return b;
}

bool SQLReadData::readHeader()
{
  const char* header = "PGCOPY\n\377\r\n\0";
  const std::size_t header_len = strlen(header) + 1;

  const char* data = readData(header_len);
  return strcmp(header, data) == 0;
}

const char* SQLReadData::readData(std::size_t _len)
{
  if(d->offset >= d->currentBuffer.size())
  {
    d->currentBuffer = readBuffer();
    d->offset = 0;
  }
  if(d->offset + _len > d->currentBuffer.size())
    return nullptr;
  const char* r = d->currentBuffer.data() + d->offset;
  d->offset += _len;
  return r;
}

template<typename _T_>
_T_ SQLReadData::read_impl(bool _read_size)
{
  std::size_t s = 0;
  if(_read_size)
  {
    const char* c = readData(sizeof(quint32));
    s = BinaryInterface::read<quint32>(c, sizeof(quint32));
  }
  else
  {
    s = BinaryInterface::calculate_size(_T_());
  }

  const char* c = readData(s);
  return BinaryInterface::read<_T_>(c, s);
}

#define MAKE_READ(_TYPE_)                                                                          \
  template<>                                                                                       \
  _TYPE_ SQLReadData::read<_TYPE_>(bool _read_size)                                                \
  {                                                                                                \
    return read_impl<_TYPE_>(_read_size);                                                          \
  }

namespace kDB
{
  namespace Repository
  {
    MAKE_READ(quint16)
    MAKE_READ(qint32)
    MAKE_READ(quint32)
    MAKE_READ(QString)
  } // namespace Repository
} // namespace kDB

bool SQLReadData::readNull()
{
  const unsigned char* c = reinterpret_cast<const unsigned char*>(readData(2));
  return c[0] == 0xff and c[1] == 0xff;
}
