#include <QSharedPointer>

#include <kDB/Forward.h>

namespace kDB::Repository::DatabaseInterface::PostgreSQL
{
  class SQLReadData
  {
  public:
    class Buffer
    {
      friend class SQLReadData;
    public:
      Buffer();
      Buffer(const Buffer& _rhs);
      Buffer& operator=(const Buffer& _rhs);
      ~Buffer();
      const char* data() const;
      std::size_t size() const;
      QByteArray toByteArray() const;
      bool isNull() const;
    private:
      struct Private;
      QSharedPointer<Private> d;
    };
  public:
    enum Status
    {
      Open,
      Close,
      Failed
    };
  public:
    SQLReadData(const Transaction& _transaction);
    ~SQLReadData();
    Buffer readBuffer();
    Status status() const;
    QString errorMessage() const;
    bool readHeader();
    template<typename _T_>
    _T_ read(bool _read_size);
    bool readNull();
  private:
    const char* readData(std::size_t _len);
    template<typename _T_>
    _T_ read_impl(bool _read_size);
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL
