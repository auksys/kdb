#include <kDB/Repository/Logging.h>

#include <libpq-fe.h>

clog_format_declare_enum_formatter(ConnStatusType, CONNECTION_OK, CONNECTION_BAD,
                                   CONNECTION_STARTED, CONNECTION_MADE,
                                   CONNECTION_AWAITING_RESPONSE, CONNECTION_AUTH_OK,
                                   CONNECTION_SETENV, CONNECTION_SSL_STARTUP, CONNECTION_NEEDED,
                                   CONNECTION_CHECK_WRITABLE, CONNECTION_CONSUME,
                                   CONNECTION_GSS_STARTUP, CONNECTION_CHECK_TARGET,
                                   CONNECTION_CHECK_STANDBY);

clog_format_declare_enum_formatter(ExecStatusType, PGRES_EMPTY_QUERY, PGRES_COMMAND_OK,
                                   PGRES_TUPLES_OK, PGRES_COPY_OUT, PGRES_COPY_IN,
                                   PGRES_BAD_RESPONSE, PGRES_NONFATAL_ERROR, PGRES_FATAL_ERROR,
                                   PGRES_COPY_BOTH, PGRES_SINGLE_TUPLE, PGRES_PIPELINE_SYNC,
                                   PGRES_PIPELINE_ABORTED);
