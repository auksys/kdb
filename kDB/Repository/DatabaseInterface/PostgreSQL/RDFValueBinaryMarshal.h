#include <kDB/Repository/AbstractBinaryMarshal.h>

#include <QSharedPointer>

namespace kDB::Repository::DatabaseInterface::PostgreSQL
{
  /**
   * @ingroup kDB_Repository
   *
   * This class handles the storage of a value of a RDF Literal in the database.
   *
   * Values are stored using multiple types, each different types is stored in a different field.
   * There should be only field in use at a given time.
   */
  class RDFValueBinaryMarshal : public AbstractBinaryMarshal
  {
    friend class Repository::Connection;
    friend class Repository::GraphsManager;
  public:
    /**
     * Interface for storing a value
     */
    class FieldIO
    {
    public:
      FieldIO(const QString& _typename);
      virtual ~FieldIO();
    public:
      virtual knowCore::Value read(const char* _data, int _field_size,
                                   const kDB::Repository::Connection& _connection) const
        = 0;
      virtual bool accept(const knowCore::Value& _value) const = 0;
      virtual quint32 calculateSize(const knowCore::Value& _value) const = 0;
      virtual void write(char* _data, const knowCore::Value& _value,
                         const kDB::Repository::Connection& _connection) const
        = 0;
      QString typeName() const;
    private:
      struct Private;
      Private* const d;
    };
    enum class Operator : quint64
    {
      None = 0,
      Addition = 1,
      Multiplication = 1 << 2,
      Substraction = 1 << 3,
      Division = 1 << 4,
      Minus = 1 << 5,
      AllArithmetic = Addition | Multiplication | Substraction | Division | Minus,
      Inferior = 1 << 6,
      InferiorEqual = 1 << 7,
      Superior = 1 << 8,
      SuperiorEqual = 1 << 9,
      Equal = 1 << 10,
      NotEqual = 1 << 11,
      AllComparison = Inferior | InferiorEqual | Superior | SuperiorEqual | Equal | NotEqual
    };
    enum class Feature : quint64
    {
      None = 0,
      BtreeIndexable = 1,
      GistIndexable = 1 << 2
    };
  private:
    RDFValueBinaryMarshal(
      const Connection& _connection); // Do not call directly, use create instead.
  public:
    static cres_qresult<RDFValueBinaryMarshal*> create(const Connection& _connection);
    virtual ~RDFValueBinaryMarshal();

    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override;
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override;

    static QStringList fields(const Connection& _connection);
    static cres_qresult<void> registerField(const Connection& _connection,
                                            const knowCore::Uri& _defaultUri,
                                            const FieldIO* _fieldIO, quint64 _operators,
                                            quint64 _features);
    /**
     * This function generate additional create term function, provided that @ref _typename can be
     * casted to an exisiting value
     */
    static cres_qresult<void> registerTerm(const Connection& _connection,
                                           const knowCore::Uri& _defaultUri,
                                           const QString& _typename,
                                           const QString& _cast_typename = QString());
  private:
    cres_qresult<void> setupIndexes(const TripleStore& _store, const Transaction& _transaction);
    cres_qresult<void> finishInitialisation();
  private:
    inline quint32 calculateSize(const knowCore::Value& _number) const;
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL
