#include "SQLResult_p.h"

#include "postgresql_p.h"

#include <QVariant>
#include <knowCore/Timestamp.h>

#include <libpq-fe.h>

#include <clog_qt>
#include <knowCore/CSVWriter.h>
#include <knowCore/TypeDefinitions.h>

#include "BinaryInterface_read_p.h"
#include "Connection_p.h"

using namespace kDB::Repository::DatabaseInterface::PostgreSQL;

struct SQLResult::Private : public QSharedData
{
  ~Private() { PQclear(result); }
  PGresult* result;
  QString query, error;
  Connection connection;
  Type type;
  mutable QStringList fieldNames;
};

SQLResult::SQLResult(void* _result, const QString& _query, const QString& _error,
                     Connection _connection)
    : d(new Private)
{
  d->result = reinterpret_cast<PGresult*>(_result);
  d->connection = _connection;
  d->query = _query;
  d->error = _error;

  switch(PQresultStatus(d->result))
  {
  case PGRES_COMMAND_OK:
    d->type = Type::VariableBinding;
    break;
  case PGRES_TUPLES_OK:
  case PGRES_COPY_IN:
  case PGRES_COPY_OUT:
    d->type = Type::Boolean;
    break;
  default:
    d->type = Type::Failed;
    break;
  }
}

SQLResult::~SQLResult() { delete d; }

knowDBC::Interfaces::Result::Type SQLResult::type() const { return d->type; }

QStringList SQLResult::fieldNames() const
{
  if(d->fieldNames.isEmpty())
  {
    for(int i = 0; i < PQnfields(d->result); ++i)
    {
      d->fieldNames.append(PQfname(d->result, i));
    }
  }
  return d->fieldNames;
}

int SQLResult::fields() const { return PQnfields(d->result); }

int SQLResult::tuples() const { return PQntuples(d->result); }

#ifndef FLOAT8ARRAYOID
#define FLOAT8ARRAYOID 1022
#endif

knowCore::Value SQLResult::value(int _tuple, int _field) const
{
  if(PQgetisnull(d->result, _tuple, _field))
    return knowCore::Value();
  using namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface;
  Oid type = PQftype(d->result, _field);
  char* value = PQgetvalue(d->result, _tuple, _field);
  int value_size = PQgetlength(d->result, _tuple, _field);

  QString typeName = d->connection.d->oidToName(type).expect_success();
  auto const [success, variant, message]
    = d->connection.d->toValue(typeName, QByteArray(value, value_size));
  if(success)
  {
    return variant.value();
  }
  clog_error("Unknown type: {}({}) for column {} with value '{}' with error '{}'", int(type),
             _field, value, typeName, message.value());
  std::size_t len = PQgetlength(d->result, _tuple, _field);
  return knowCore::Value::fromValue(QByteArray(value, len));
}

QString SQLResult::error() const { return d->error; }

QString SQLResult::query() const { return d->query; }
