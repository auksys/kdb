#include <QList>
#include <QPair>
#include <QSharedPointer>
#include <QString>

#include <kDB/Repository/TripleStore.h>

class QVariant;

namespace kDB::Repository::DatabaseInterface::PostgreSQL
{
  class SQLInterface
  {
    SQLInterface(const SQLInterface& _rhs);
    SQLInterface& operator=(const SQLInterface& _rhs);
    SQLInterface();
    ~SQLInterface();
  public:
    enum LockMode
    {
      AccessShare,
      RowShare,
      RowExclusive,
      ShareUpdateExclusive,
      Share,
      ShareRowExclusive,
      Exclusive,
      AccessExclusive
    };
    static cres_qresult<void> lock(const Transaction& _transaction, const QString& _tablename,
                                   LockMode _lockMode);
    static cres_qresult<void> lockTripleStore(const Transaction& _transaction,
                                              const QString& _tablename, bool _lock_versioning);

    static QString subjectString(const knowRDF::Subject& _subject);

    static cres_qresult<void> dropView(const QueryConnectionInfo& _connection_info,
                                       const QString& _name);

    static QString viewTableName(const Connection& _connection, const QString& _name);

    static QString persistentDatasetsUnionTableName(const Connection& _connection,
                                                    const QString& _name);
    static cres_qresult<void> updateUnionView(const QueryConnectionInfo& _connection_info,
                                              bool _temporary, const QString& _name,
                                              const QList<RDFDataset>& _documents);
    static cres_qresult<void> addToUnion(const QueryConnectionInfo& _connection_info,
                                         const knowCore::Uri& _union_name,
                                         const knowCore::Uri& _dataset_name);
    static cres_qresult<void> removeFromUnion(const QueryConnectionInfo& _connection_info,
                                              const knowCore::Uri& _union_name,
                                              const knowCore::Uri& _dataset_name);
    static cres_qresult<void> clearUnion(const QueryConnectionInfo& _connection_info,
                                         const knowCore::Uri& _union_name);
    static QHash<QString, QStringList> getUnions(const QueryConnectionInfo& _connection_info);

    static cres_qresult<void> setDefinition(const QueryConnectionInfo& _connection_info,
                                            const RDFView::ViewDefinition& _definition);
    static cres_qresult<void> removeViewDefinition(const QueryConnectionInfo& _connection_info,
                                                   const knowCore::Uri& _name);
    static QList<RDFView::ViewDefinition>
      getDefinitions(const QueryConnectionInfo& _connection_info);

    static cres_qresult<QString> createTripleStore(const Transaction& _transaction,
                                                   const knowCore::Uri& _name);
    static cres_qresult<void> removeTripleStore(const Transaction& _transaction,
                                                const knowCore::Uri& _name);
    static cres_qresult<void> createIndex(const Transaction& _transaction,
                                          const TripleStore& _store, const QString& _field,
                                          const QString& _index_method);

    static cres_qresult<void> enableNotifications(const QueryConnectionInfo& _connection_info,
                                                  const TripleStore& _store);
    static cres_qresult<void> disableNotifications(const QueryConnectionInfo& _connection_info,
                                                   const TripleStore& _store);
    static QString notificationsChannel(const TripleStore& _store);

    static cres_qresult<void> enableVersioning(const Transaction& _transaction,
                                               const TripleStore& _store);
    static cres_qresult<void> disableVersioning(const QueryConnectionInfo& _connection_info,
                                                const TripleStore& _store);

    static cres_qresult<void> startListeningChanges(const Transaction& _transaction,
                                                    const TripleStore& _store);
    static cres_qresult<void> stopListeningChanges(const Transaction& _transaction,
                                                   const TripleStore& _store);
    static QList<std::tuple<QString, knowRDF::Triple>>
      retrieveChanges(const Transaction& _transaction, const TripleStore& _store);

    /**
     * Compute the hash of a triple store.
     */
    static QByteArray computeContentHash(const QueryConnectionInfo& _connection_info,
                                         const QString& _triples_store_table_name);
    /**
     * Create a new revision
     * @param revisionHash is the combination of the content hash with the historicity
     * @param historicity is defined as the number of non-merge delta on shortest path to root
     */
    static cres_qresult<int> createRevision(const Transaction& _transaction,
                                            const QString& _triples_store_table_name,
                                            const QByteArray& _revision_hash,
                                            const QByteArray& _content_hash, int _historicity,
                                            int _tags, int _distance_to_root);
    /**
     * Remove revision (and associated deltas) from the database, no check is made if
     * this make some revisions unaccessible
     */
    static cres_qresult<void> removeRevision(const Transaction& _transaction,
                                             const VersionControl::Revision& _revision);
    static cres_qresult<void> recordDelta(const Transaction& _transaction,
                                          const QString& _triples_store_table_name, int _parent,
                                          int _child, const QByteArray& _hash,
                                          const QByteArray& _delta);
    static cres_qresult<void> recordDeltaSignature(const Transaction& _transaction,
                                                   const QString& _triples_store_table_name,
                                                   int _parent, int _child,
                                                   const QByteArray& _author,
                                                   const knowCore::Timestamp& _timestamp,
                                                   const QByteArray& _signature);
    static cres_qresult<void> setRevisionHash(const Transaction& _transaction,
                                              const QString& _triples_store_table_name,
                                              int _revision, const QByteArray& _hash,
                                              const QByteArray& _content_hash);
    static cres_qresult<void> setRevisionTags(const Transaction& _transaction,
                                              const QString& _triples_store_table_name,
                                              const VersionControl::Revision& _revision, int _tags);
    /**
     * Send a notification through postgresql that a new revision is available
     */
    static cres_qresult<void> notifyNewRevision(const Transaction& _transaction,
                                                const QString& _triples_store_table_name,
                                                const QByteArray& _hash);
    /**
     * Send a notification through postgresql that the tags have been changed in a revision
     */
    static cres_qresult<void> notifyRevisionTagsChanged(const Transaction& _transaction,
                                                        const QString& _triples_store_table_name,
                                                        const QByteArray& _hash);

    static QList<VersionControl::Delta> deltas(const QueryConnectionInfo& _connection_info,
                                               const QString& _triples_store_table_name,
                                               int _revisionId);
    static cres_qresult<VersionControl::Delta> delta(const QueryConnectionInfo& _connection_info,
                                                     const QString& _triples_store_table_name,
                                                     int _parent, int _child);
    static cres_qresult<VersionControl::Delta> delta(const QueryConnectionInfo& _connection_info,
                                                     const QString& _triples_store_table_name,
                                                     const VersionControl::Revision& _parent,
                                                     const VersionControl::Revision& _child);
    static cres_qresult<bool> hasDelta(const QueryConnectionInfo& _connection_info,
                                       const QString& _triples_store_table_name,
                                       const QByteArray& _parent, const QByteArray& _child);
    static cres_qresult<bool> hasDelta(const QueryConnectionInfo& _connection_info,
                                       const QString& _triples_store_table_name, int _parent,
                                       int _child);

    /**
     * @return the list of signatures for a given delta
     */
    static cres_qresult<QList<VersionControl::Signature>>
      deltaSignatures(const QueryConnectionInfo& _connection_info,
                      const QString& _triples_store_table_name, int _parent, int _child);

    static cres_qresult<void> setUnitConversionFactors(const QueryConnectionInfo& _connection_info);
  private:
    static cres_qresult<QList<VersionControl::Revision>>
      queryToRevisions(const QueryConnectionInfo& _connection_info,
                       const QString& _triples_store_table_name, knowDBC::Query& _query);
  public:
    static cres_qresult<QList<VersionControl::Revision>>
      revisions(const QueryConnectionInfo& _connection_info,
                const QString& _triples_store_table_name);
    /**
     * @return the list of children for a given revision.
     */
    static cres_qresult<QList<VersionControl::Revision>>
      revisionChildren(const QueryConnectionInfo& _connection_info,
                       const QString& _triples_store_table_name, int _revisionId);
    static cres_qresult<QList<VersionControl::Revision>>
      heads(const QueryConnectionInfo& _connection_info, const QString& _triples_store_table_name);
    /**
     * @return the revision id for the given \ref _hash
     */
    static cres_qresult<int> revisionId(const QueryConnectionInfo& _connection_info,
                                        const QString& _triples_store_table_name,
                                        const QByteArray& _hash);
    static cres_qresult<kDB::Repository::VersionControl::Revision>
      revision(const QueryConnectionInfo& _connection_info,
               const QString& _triples_store_table_name, const QByteArray& _hash);
    static cres_qresult<kDB::Repository::VersionControl::Revision>
      revision(const QueryConnectionInfo& _connection_info,
               const QString& _triples_store_table_name, int _revisionId);
    static cres_qresult<kDB::Repository::VersionControl::Revision>
      revision(const QueryConnectionInfo& _connection_info,
               const QString& _triples_store_table_name, const QByteArray& _content_hash,
               int _minimum_historicity);
    static cres_qresult<bool> hasRevision(const QueryConnectionInfo& _connection_info,
                                          const QString& _triples_store_table_name,
                                          const QByteArray& _hash);

    static QList<qint32> forwardRevisionPath(const QueryConnectionInfo& _connection_info,
                                             const QString& _triples_store_table_name,
                                             const QByteArray& _source_hash,
                                             const QByteArray& _destination_hash);
    static QList<qint32> forwardRevisionPath(const QueryConnectionInfo& _connection_info,
                                             const QString& _triples_store_table_name,
                                             int _source_id, int _destination_id);
    static QPair<QList<qint32>, QList<qint32>>
      revisionPath(const QueryConnectionInfo& _connection_info,
                   const QString& _triples_store_table_name, const QByteArray& _source_hash,
                   const QByteArray& _destination_hash);
    static cres_qresult<QList<QList<qint32>>>
      revisionPath(const QueryConnectionInfo& _connection_info,
                   const QString& _triples_store_table_name, const QList<QByteArray>& _revisions);

    static cres_qresult<knowCore::Value> parseMeta(const QJsonValue& _value);
    static cres_qresult<knowCore::Value> getMeta(const QueryConnectionInfo& _connection_info,
                                                 const QString& _store, const QStringList& _path);
    static cres_qresult<void> setMeta(const QueryConnectionInfo& _connection_info,
                                      const QString& _store, const QStringList& _path,
                                      const knowCore::Value& _value);

    static cres_qresult<void> dropMeta(const QueryConnectionInfo& _connection_info,
                                       const QString& _store, const QStringList& _path);

    static QList<knowCore::Uri> getGraphUris(const Transaction& _transaction,
                                             const QString& _tablename, const QString& _field);
    static cres_qresult<void> removeTriples(const Transaction& _transaction, TripleStore* _store,
                                            const knowCore::Uri& _store_uri);
    static cres_qresult<void> insertTriples(const Transaction& _transaction, TripleStore* _store,
                                            const knowCore::Uri& _store_uri);

    static knowRDF::Triple getTriple(const Connection& _connection, const knowDBC::Result& _result,
                                     int _tuple, int _subject, int _predicate, int _object,
                                     bool _blank_as_uri);

    /**
     * Create a temporary table with a list of triples (used for mass insertion/deletion).
     *
     * @param _table_to_model the name of the table whose column to model
     */
    static cres_qresult<void> insertOrRemoveTriples(bool _insert,
                                                    const Transaction& _connection_info,
                                                    const QList<knowRDF::Triple>& _triples,
                                                    const QString& _table_to_model);
  private:
    bool insertValue(const QueryConnectionInfo& _connection_info, const QString& _tablename,
                     const QVariant& _variant);
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL
