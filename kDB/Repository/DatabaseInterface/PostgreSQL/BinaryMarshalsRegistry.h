#include <kDB/Repository/Connection.h>

#include <kDB/Forward.h>

namespace kDB::Repository::DatabaseInterface::PostgreSQL
{
  class BinaryMarshalsRegistry
  {
    friend class Connection::Private;
    BinaryMarshalsRegistry() = delete;
    ~BinaryMarshalsRegistry() = delete;
  public:
    static void add(AbstractBinaryMarshal* _marshal);
  private:
    static cres_qresult<knowCore::Value> toValue(const QString& _oid, const QByteArray& _source,
                                                 const kDB::Repository::Connection& _connection);
    static cres_qresult<QByteArray> toByteArray(const knowCore::Value& _source, QString& _oidName,
                                                const kDB::Repository::Connection& _connection);
  private:
    struct Private;
    Private* d;
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL

#define KDB_REGISTER_SQL_MARSHAL(_KLASS_)                                                          \
  struct _KLASS_##Registrer                                                                        \
  {                                                                                                \
    _KLASS_##Registrer()                                                                           \
    {                                                                                              \
      kDB::Repository::DatabaseInterface::PostgreSQL::BinaryMarshalsRegistry::add(new _KLASS_());  \
    }                                                                                              \
  };                                                                                               \
  _KLASS_##Registrer _KLASS_##Registrer_instance;
