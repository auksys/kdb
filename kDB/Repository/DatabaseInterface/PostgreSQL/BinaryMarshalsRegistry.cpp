#include "BinaryMarshalsRegistry.h"

#include "postgresql_p.h"

#include <QFile>
#include <QHash>
#include <QList>
#include <QString>
#include <QVariant>
#include <knowCore/Timestamp.h>

#include <clog_qt>
#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uri.h>
#include <knowCore/Uris/xsd.h>

#include <knowRDF/Literal.h>

#include "AbstractBinaryMarshal.h"
#include "BinaryInterface_read_p.h"
#include "BinaryInterface_write_p.h"

using namespace kDB::Repository::DatabaseInterface::PostgreSQL;

using xsd = knowCore::Uris::xsd;

namespace kDB
{
  namespace Repository
  {
    namespace Details
    {
      template<typename _T_>
      knowCore::Value value2variant(const _T_& _t)
      {
        return knowCore::Value::fromValue(_t);
      }
      template<typename _T_>
      class BinaryMarshal : public AbstractBinaryMarshal
      {
      public:
        BinaryMarshal(const QString& _oid, Modes _mode = Mode::ToVariant | Mode::ToByteArray,
                      std::optional<knowCore::Uri> _datatype = std::optional<knowCore::Uri>())
            : AbstractBinaryMarshal(_oid, _datatype ? *_datatype : datatype<_T_>(), _mode)
        {
        }
        cres_qresult<knowCore::Value>
          toValue(const QByteArray& _source,
                  const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          return cres_success(
            value2variant(BinaryInterface::read<_T_>(_source.data(), _source.size())));
        }
        cres_qresult<QByteArray>
          toByteArray(const knowCore::Value& _source, QString& _oidName,
                      const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          _oidName = oid();
          QByteArray destination;
          BinaryInterface::write<_T_>(_source.value<_T_>().expect_success(), &destination);
          return cres_success(destination);
        }
      };
      template<typename _T_>
      class WriteBinaryMarshal : public AbstractBinaryMarshal
      {
      public:
        WriteBinaryMarshal(const QString& _oid)
            : AbstractBinaryMarshal(_oid, datatype<_T_>(), Mode::ToByteArray)
        {
        }
        cres_qresult<QByteArray>
          toByteArray(const knowCore::Value& _source, QString& _oidName,
                      const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          _oidName = oid();
          QByteArray destination;
          BinaryInterface::write<_T_>(_source.value<_T_>().expect_success(), &destination);
          return cres_success(destination);
        }
      };
      template<typename _T_>
      class ReadBinaryMarshal : public AbstractBinaryMarshal
      {
      public:
        ReadBinaryMarshal(const QString& _oid)
            : AbstractBinaryMarshal(_oid, knowCore::Uri(), Mode::ToVariant)
        {
        }
        cres_qresult<knowCore::Value>
          toValue(const QByteArray& _source,
                  const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          return cres_success(
            value2variant(BinaryInterface::read<_T_>(_source.data(), _source.size())));
        }
      };
      template<typename _T_>
      class JsonBinaryMarshal : public AbstractBinaryMarshal
      {
      public:
        JsonBinaryMarshal(const QString& _oid)
            : AbstractBinaryMarshal(_oid, datatype<_T_>(), Mode::ToVariant)
        {
        }
        cres_qresult<knowCore::Value>
          toValue(const QByteArray& _source,
                  const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          return cres_success(
            value2variant(BinaryInterface::read<_T_>(_source.data(), _source.size())));
        }
      };
      template<typename _T_>
      bool is_json_null(const _T_&)
      {
        return false;
      }
      template<>
      bool is_json_null(const QJsonValue& _value)
      {
        return _value.isNull();
      }
      template<typename _T_>
      class JsonbBinaryMarshal : public AbstractBinaryMarshal
      {
      public:
        JsonbBinaryMarshal(const QString& _oid)
            : AbstractBinaryMarshal(_oid, datatype<_T_>(), Mode::ToVariant | Mode::ToByteArray)
        {
        }
        cres_qresult<knowCore::Value>
          toValue(const QByteArray& _source,
                  const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          if(_source.isEmpty())
            return cres_success(knowCore::Value::fromValue(QJsonValue()));
          int skip = sizeof(qint8);
          qint8 v = *(reinterpret_cast<const qint8*>(_source.data()));
          if(v != 1)
          {
            return cres_failure("Unsupported Jsonb version: {}", v);
          }
          return cres_success(knowCore::Value::fromValue(
            BinaryInterface::read<_T_>(_source.data() + skip, _source.size() - skip)));
        }
        cres_qresult<QByteArray>
          toByteArray(const knowCore::Value& _source, QString& _oidName,
                      const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          _oidName = oid();
          QByteArray d;
          _T_ v = _source.value<_T_>().expect_success();
          if(is_json_null(v))
          {
            return cres_success<QByteArray>(QByteArray("\x01{}"));
          }
          BinaryInterface::write<_T_>(v, &d);
          QByteArray destination;
          destination.resize(d.size() + sizeof(qint8));
          *(reinterpret_cast<qint8*>(destination.data())) = 1;
          std::copy(d.data(), d.data() + d.size(), destination.data() + sizeof(qint8));
          return cres_success(destination);
        }
      };
      class ByteaBinaryMarshal : public AbstractBinaryMarshal
      {
      public:
        ByteaBinaryMarshal()
            : AbstractBinaryMarshal("bytea", datatype<QByteArray>(),
                                    Mode::ToVariant | Mode::ToByteArray)
        {
        }
        cres_qresult<knowCore::Value>
          toValue(const QByteArray& _source,
                  const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          return cres_success(knowCore::Value::fromValue(_source));
        }
        cres_qresult<QByteArray>
          toByteArray(const knowCore::Value& _source, QString& _oidName,
                      const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          _oidName = oid();
          return cres_success(_source.value<QByteArray>().expect_success());
        }
      };
      class UriMarshal : public AbstractBinaryMarshal
      {
      public:
        UriMarshal()
            : AbstractBinaryMarshal("kdb_uri", datatype<knowCore::Uri>(),
                                    Mode::ToVariant | Mode::ToByteArray)
        {
        }
        cres_qresult<knowCore::Value>
          toValue(const QByteArray& _source,
                  const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          return cres_success(knowCore::Value::fromValue<knowCore::Uri>(
            BinaryInterface::read<QString>(_source.data(), _source.size())));
        }
        cres_qresult<QByteArray>
          toByteArray(const knowCore::Value& _source, QString& _oidName,
                      const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          _oidName = "kdb_uri";
          QByteArray destination;
          BinaryInterface::write<QString>(_source.value<knowCore::Uri>().expect_success(),
                                          &destination);
          return cres_success(destination);
        }
      };
      class TimestampMarshal : public AbstractBinaryMarshal
      {
      public:
        TimestampMarshal()
            : AbstractBinaryMarshal("kdb_timestamp", datatype<knowCore::Timestamp>(),
                                    Mode::ToVariant | Mode::ToByteArray)
        {
        }
        cres_qresult<knowCore::Value>
          toValue(const QByteArray& _source,
                  const kDB::Repository::Connection& _connection) const override
        {
          knowCore::BigNumber number
            = BinaryInterface::read<knowCore::BigNumber>(_source.data(), _source.size());
          knowCore::Timestamp dt = knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(number);
          Q_UNUSED(_connection);
          return cres_success(knowCore::Value::fromValue(dt));
        }
        cres_qresult<QByteArray>
          toByteArray(const knowCore::Value& _source, QString& _oidName,
                      const kDB::Repository::Connection& _connection) const override
        {
          Q_UNUSED(_connection);
          _oidName = "kdb_timestamp";
          cres_try(knowCore::Timestamp dt, _source.value<knowCore::Timestamp>());
          knowCore::BigNumber bn = dt.toEpoch<knowCore::NanoSeconds>().count();
          QByteArray destination;
          BinaryInterface::write<knowCore::BigNumber>(bn, &destination);
          return cres_success(destination);
        }
      };
    } // namespace Details
  } // namespace Repository
} // namespace kDB

struct BinaryMarshalsRegistry::Private
{
  ~Private();
  QList<AbstractBinaryMarshal*> marshals;
  QHash<QString, AbstractBinaryMarshal*> oid2marshals;
  QHash<knowCore::Uri, AbstractBinaryMarshal*> metaTypeId2marshals;

  static BinaryMarshalsRegistry::Private* s_instance;
  static BinaryMarshalsRegistry::Private* instance();
};

BinaryMarshalsRegistry::Private* BinaryMarshalsRegistry::Private::s_instance = 0;

BinaryMarshalsRegistry::Private::~Private() { qDeleteAll(marshals); }

#define VECTOR_MARSHAL_ADD(__SIZE__)                                                               \
  BinaryMarshalsRegistry::add(                                                                     \
    new Details::WriteBinaryMarshal<knowCore::Vector##__SIZE__##d>("_float8"));                    \
  BinaryMarshalsRegistry::add(                                                                     \
    new Details::WriteBinaryMarshal<knowCore::Vector##__SIZE__##dList>("_float8"));                \
  BinaryMarshalsRegistry::add(                                                                     \
    new Details::WriteBinaryMarshal<knowCore::Vector##__SIZE__##f>("_float4"));                    \
  BinaryMarshalsRegistry::add(                                                                     \
    new Details::WriteBinaryMarshal<knowCore::Vector##__SIZE__##fList>("_float4"));

#define ARRAY_MARSHAL_ADD(__OID__, __NAME__)                                                       \
  BinaryMarshalsRegistry::add(new Details::BinaryMarshal<knowCore::__NAME__##Array>(__OID__));

BinaryMarshalsRegistry::Private* BinaryMarshalsRegistry::Private::instance()
{
  if(s_instance == 0)
  {
    using Mode = AbstractBinaryMarshal::Mode;
    s_instance = new Private;
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<bool>("bool"));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<qint16>("int2"));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<qint32>("int4"));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<qint64>("int8"));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<quint16>("int2", Mode::ToByteArray));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<quint32>("int4", Mode::ToByteArray));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<quint64>("int8", Mode::ToByteArray));
    BinaryMarshalsRegistry::add(new Details::WriteBinaryMarshal<knowCore::Int16List>("_int2"));
    BinaryMarshalsRegistry::add(new Details::WriteBinaryMarshal<knowCore::Int32List>("_int4"));
    BinaryMarshalsRegistry::add(new Details::WriteBinaryMarshal<knowCore::Int64List>("_int8"));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<float>("float4"));
    BinaryMarshalsRegistry::add(new Details::WriteBinaryMarshal<knowCore::FloatList>("_float4"));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<double>("float8"));
    BinaryMarshalsRegistry::add(new Details::WriteBinaryMarshal<knowCore::DoubleList>("_float8"));
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<QString>("text"));
    BinaryMarshalsRegistry::add(new Details::WriteBinaryMarshal<QStringList>("_text"));
    BinaryMarshalsRegistry::add(new Details::ReadBinaryMarshal<knowCore::Timestamp>("timestamp"));
    BinaryMarshalsRegistry::add(new Details::TimestampMarshal());
    BinaryMarshalsRegistry::add(new Details::BinaryMarshal<knowCore::BigNumber>("numeric"));
    BinaryMarshalsRegistry::add(
      new Details::BinaryMarshal<knowCore::BigNumber>("numeric", Mode::ToByteArray, xsd::integer));
    BinaryMarshalsRegistry::add(new Details::ReadBinaryMarshal<QString>("name"));
    BinaryMarshalsRegistry::add(new Details::ReadBinaryMarshal<Oid>("oid"));
    BinaryMarshalsRegistry::add(new Details::ByteaBinaryMarshal());
    BinaryMarshalsRegistry::add(new Details::JsonBinaryMarshal<QJsonValue>("json"));
    BinaryMarshalsRegistry::add(new Details::JsonbBinaryMarshal<QJsonValue>("jsonb"));
    VECTOR_MARSHAL_ADD(2)
    VECTOR_MARSHAL_ADD(3)
    VECTOR_MARSHAL_ADD(4)
    VECTOR_MARSHAL_ADD(5)
    VECTOR_MARSHAL_ADD(6)
    VECTOR_MARSHAL_ADD(7)
    VECTOR_MARSHAL_ADD(8)
    VECTOR_MARSHAL_ADD(9)
    ARRAY_MARSHAL_ADD("_bytea", Bytes)
    ARRAY_MARSHAL_ADD("_text", String)
    ARRAY_MARSHAL_ADD("_int2", Int16)
    ARRAY_MARSHAL_ADD("_int4", Int32)
    ARRAY_MARSHAL_ADD("_int8", Int64)
    ARRAY_MARSHAL_ADD("_float4", Float)
    ARRAY_MARSHAL_ADD("_float8", Double)
    BinaryMarshalsRegistry::add(new Details::UriMarshal());
  }
  return s_instance;
}

void BinaryMarshalsRegistry::add(AbstractBinaryMarshal* _marshal)
{
  Private* p = Private::instance();
  p->marshals.append(_marshal);
  if(_marshal->modes().testFlag(AbstractBinaryMarshal::Mode::ToByteArray))
  {
    p->metaTypeId2marshals[_marshal->datatype()] = _marshal;
    for(const knowCore::Uri& u : knowCore::MetaTypes::aliases(_marshal->datatype()))
    {
      p->metaTypeId2marshals[u] = _marshal;
    }
  }
  if(_marshal->modes().testFlag(AbstractBinaryMarshal::Mode::ToVariant))
  {
    p->oid2marshals[_marshal->oid()] = _marshal;
  }
}

cres_qresult<QByteArray>
  BinaryMarshalsRegistry::toByteArray(const knowCore::Value& _source, QString& _oidName,
                                      const kDB::Repository::Connection& _connection)
{
  if(Private::instance()->metaTypeId2marshals.contains(_source.datatype()))
  {
    return Private::instance()->metaTypeId2marshals[_source.datatype()]->toByteArray(
      _source, _oidName, _connection);
  }
  else
  {
    return cres_failure("Unsupported type: '{}'", _source.datatype());
  }
}

cres_qresult<knowCore::Value>
  BinaryMarshalsRegistry::toValue(const QString& _oid, const QByteArray& _source,
                                  const kDB::Repository::Connection& _connection)
{
  if(Private::instance()->oid2marshals.contains(_oid))
  {
    return Private::instance()->oid2marshals[_oid]->toValue(_source, _connection);
  }
  else
  {
    return cres_failure("Unsupported type: '{}'", _oid);
  }
}
