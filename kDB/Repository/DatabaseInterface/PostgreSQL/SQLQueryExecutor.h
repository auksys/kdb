#include <QFlags>
#include <QString>

#include <knowCore/Value.h>
#include <knowDBC/Interfaces/QueryExecutor.h>

#include "QueryConnectionInfo.h"

namespace kDB::Repository::DatabaseInterface::PostgreSQL
{
  class SQLQueryExecutor : public knowDBC::Interfaces::QueryExecutor
  {
  public:
    SQLQueryExecutor(const QueryConnectionInfo& _connectionInfo);
    ~SQLQueryExecutor();
  public:
    knowDBC::Result execute(const QString& _query, const knowCore::ValueHash& _options,
                            const knowCore::ValueHash& _bindings) final;
    knowCore::Uri queryLanguage() const final;
  private:
    struct Private;
    Private* const d;
  };

} // namespace kDB::Repository::DatabaseInterface::PostgreSQL
