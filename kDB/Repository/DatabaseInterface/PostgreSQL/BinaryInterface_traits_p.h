namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface
{
  template<typename _T_>
  struct traits;

  template<>
  struct traits<qint16>
  {
    static constexpr Oid inline oid() { return INT2OID; }
  };

  template<>
  struct traits<quint16>
  {
    static constexpr Oid inline oid() { return INT2OID; }
  };

  template<>
  struct traits<qint32>
  {
    static constexpr Oid inline oid() { return INT4OID; }
  };

  template<>
  struct traits<quint32>
  {
    static constexpr Oid inline oid() { return INT4OID; }
  };

  template<>
  struct traits<qint64>
  {
    static constexpr Oid inline oid() { return INT8OID; }
  };

  template<>
  struct traits<quint64>
  {
    static constexpr Oid inline oid() { return INT8OID; }
  };

  template<>
  struct traits<float>
  {
    static constexpr Oid inline oid() { return FLOAT4OID; }
  };

  template<>
  struct traits<double>
  {
    static constexpr Oid inline oid() { return FLOAT8OID; }
  };
  template<>
  struct traits<QString>
  {
    static constexpr Oid inline oid() { return TEXTOID; }
  };
  template<>
  struct traits<QByteArray>
  {
    static constexpr Oid inline oid() { return BYTEAOID; }
  };
  template<>
  struct traits<knowCore::BigNumber>
  {
    static constexpr Oid inline oid() { return NUMERICOID; }
  };

} // namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface
