#include "AbstractBinaryMarshal.h"

#include <QSharedPointer>

namespace kDB::Repository::DatabaseInterface::PostgreSQL
{
  class RDFValueBinaryMarshal;
  /**
   * @ingroup kDB_Repository
   *
   * This class handles the storage of RDF Literal in the database. It stores the dataype, the value
   * (using \ref RDFValueBinaryMarshal) and the lang.
   */
  class RDFTermBinaryMarshal : public AbstractBinaryMarshal
  {
  public:
    /**
     * When storing a RDF Value in the database, \ref RDFValueBinaryMarshal tries to store it as an
     * efficient field first, if it cannot it is stored as a string literal using the
     * to/fromRdfLiteral of \ref knowCore::Value. This class allows to define alternative
     * Serialisation, for instance, \ref knowCore::Timestamp are stored as a number (\ref
     * knowCore::BigNumber).
     */
    class AbstractSerialiser
    {
    public:
      virtual ~AbstractSerialiser();
      virtual cres_qresult<knowCore::Value> serialise(const knowCore::Value& _value) const = 0;
      virtual cres_qresult<knowCore::Value> unserialise(const knowCore::Value& _value) const = 0;
    };
    static bool registerSerialiser(const knowCore::Uri& _valueUri, AbstractSerialiser* _serialiser);
  public:
    RDFTermBinaryMarshal(RDFValueBinaryMarshal* _marshal, const Connection& _connection);
    virtual ~RDFTermBinaryMarshal();
    cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source,
              const kDB::Repository::Connection& _connection) const override;
    cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const override;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL
