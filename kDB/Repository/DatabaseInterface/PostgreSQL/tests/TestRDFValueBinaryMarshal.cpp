#include "TestRDFValueBinaryMarshal.h"

#include <clog_qt>
#include <knowCore/Uri.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/DatabaseInterface/PostgreSQL/RDFValueBinaryMarshal.h>
#include <kDB/Repository/NotificationsManager.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>

#include <Cyqlops/Crypto/RSAAlgorithm.h>

#include <kDB/Repository/Test.h>

namespace Repository = kDB::Repository;

struct DummyFieldIO : Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::FieldIO
{
  DummyFieldIO() : FieldIO("kdb_dummy") {}
  virtual ~DummyFieldIO() {}
  bool accept(const knowCore::Value&) const override { return false; }
  quint32 calculateSize(const knowCore::Value&) const override { return 0; }
  knowCore::Value read(const char*, int, const kDB::Repository::Connection&) const override
  {
    return knowCore::Value();
  }
  void write(char*, const knowCore::Value&, const kDB::Repository::Connection&) const override {}
};

void TestRDFValueBinaryMarshal::testRegister()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c1 = store.createConnection();
  Repository::Connection c2 = store.createConnection();
  CRES_QVERIFY(c1.connect());
  CRES_QVERIFY(c2.connect());
  QAtomicInt counter1 = 0;
  QAtomicInt counter2 = 0;
  c1.notificationsManager()->listen("kdb_rdf_value_type_enum_values_updated",
                                    [&counter1](const QByteArray&) { ++counter1; });
  c2.notificationsManager()->listen("kdb_rdf_value_type_enum_values_updated",
                                    [&counter2](const QByteArray&) { ++counter2; });

  QCOMPARE(Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c1).size(), 6);
  QCOMPARE(Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c1),
           Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c2));

  CRES_QVERIFY_FAILURE(
    Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::registerField(
      c1, "dummy"_kCu, new DummyFieldIO, 0, 0));
  QCOMPARE(Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c1).size(), 6);
  QCOMPARE(Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c1),
           Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c2));

  CRES_QVERIFY(c1.executeQuery("CREATE TYPE kdb_dummy AS ENUM ('null');"));

  CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::registerField(
    c1, "dummy"_kCu, new DummyFieldIO, 0, 0));
  //   QVERIFY(c1->executeQuery("NOTIFY kdb_rdf_value_type_enum_values_updated"));
  KNOWCORE_TEST_WAIT_FOR(counter1 == 1);
  QCOMPARE((int)counter1, 1);
  KNOWCORE_TEST_WAIT_FOR(counter2 == 1);
  QCOMPARE((int)counter2, 1);
  KNOWCORE_TEST_WAIT_FOR(
    Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c1).size() == 7);
  QCOMPARE(Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c1).size(), 7);
  KNOWCORE_TEST_WAIT_FOR(
    Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c2).size() == 7);
  QCOMPARE(Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c1),
           Repository::DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal::fields(c2));
}

QTEST_MAIN(TestRDFValueBinaryMarshal)
#include "TestRDFValueBinaryMarshal.moc"
