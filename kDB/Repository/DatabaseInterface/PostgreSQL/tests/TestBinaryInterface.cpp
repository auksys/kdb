#include "TestBinaryInterface.h"

#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>

#include "../postgresql_p.h"

#include "../BinaryInterface_read_p.h"
#include "../BinaryInterface_write_p.h"

namespace BI = kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface;

void TestBinaryInterface::test()
{
  QCOMPARE((int)BI::read<qint16>(BI::write<qint16>(0x1234).data(), 2), 0x1234);
  QCOMPARE((int)BI::read<qint32>(BI::write<qint32>(0x12345678).data(), 4), 0x12345678);
  QCOMPARE(BI::read<qint64>(BI::write<qint64>(0x12345678).data(), 8), 0x12345678ll);
  QCOMPARE(BI::read<qint64>(BI::write<qint64>(0x1234567890123ll).data(), 8), 0x1234567890123ll);
  QCOMPARE(BI::read<float>(BI::write<float>(0.5f).data(), 4), 0.5f);
  QCOMPARE(BI::read<double>(BI::write<double>(-0.5f).data(), 8), -0.5f);

  QList<float> list_of_floats;
  list_of_floats.push_back(4.0);
  list_of_floats.push_back(2.0);
  list_of_floats.push_back(6.0);
  QCOMPARE((unsigned long)BI::calculate_size(list_of_floats), 44ul);

  QByteArray list_of_floats_buffer = BI::write<QList<float>>(list_of_floats);
  QCOMPARE(list_of_floats_buffer.size(), 20 + list_of_floats.size() * (4 + 4));
  CRES_QCOMPARE(
    BI::read<knowCore::FloatArray>(list_of_floats_buffer.data(), list_of_floats_buffer.size())
      .toList(),
    list_of_floats);
}

QTEST_MAIN(TestBinaryInterface)
