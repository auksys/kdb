#include "TestRdfValue.h"

#include <knowCore/BigNumber.h>
#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/TemporaryStore.h>

namespace Repository = kDB::Repository;

void TestRdfValue::testSQL()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query q
    = c.createSQLQuery("SELECT kdb_rdf_value_create(1242), kdb_rdf_value_create('azerty'), "
                       "kdb_rdf_value_create(-0.00002), kdb_rdf_value_create(:number)");
  q.bindValue(":number", -0.00002);

  knowDBC::Result r = q.execute();
  QVERIFY(r);

  QCOMPARE(r.value<int>(0, 0).expect_success(), 1242);
  QCOMPARE(r.value<QString>(0, 1).expect_success(), QString("azerty"));
  QCOMPARE(r.value<double>(0, 2).expect_success(), -0.00002);
  QCOMPARE(r.value<double>(0, 3).expect_success(), -0.00002);
}

QTEST_MAIN(TestRdfValue)
