CREATE TYPE kdb_revision_path AS(path int[]);
CREATE TYPE kdb_revision_path_multi_return AS(pathes kdb_revision_path[]);


-- 
-- This function is used to find the shortest path and closest common ancestor between multiple revision
-- 
CREATE FUNCTION kdb_revision_path_multi(tablename text, revisions int[]) RETURNS kdb_revision_path_multi_return AS $$
DECLARE
  investigated_path kdb_revision_path_rp_;
  investigated_path_match kdb_revision_path_rp_;
  result_path kdb_revision_path_rp_;
  r_id   int;
  distance_to_root int;
  source_r_id int;
  returned_path kdb_revision_path[];
BEGIN

  CREATE TEMPORARY TABLE __kdb_revision_path__possible_pathes__(
          id SERIAL PRIMARY KEY,
          path_source int NOT NULL,
          path_destination int NOT NULL,
          source_distance_to_root int NOT NULL,
          estimated_length int NOT NULL,
          path int[] NOT NULL,
          investigated boolean DEFAULT false) ;
  -- CREATE INDEX __kdb_revision_path__possible_pathes__index_1__ ON __kdb_revision_path__possible_pathes__ USING btree(path_destination, estimated_length);
  -- CREATE INDEX __kdb_revision_path__possible_pathes__index_2__ ON __kdb_revision_path__possible_pathes__ USING btree(path_source);

  FOREACH r_id IN ARRAY revisions LOOP
    EXECUTE 'SELECT distance_to_root FROM ' || tablename::regclass || '_versioning_revisions WHERE revision = $1' INTO distance_to_root USING r_id;
    INSERT INTO __kdb_revision_path__possible_pathes__(path_source, path_destination, source_distance_to_root, estimated_length, path) VALUES
      (r_id, r_id, distance_to_root, 1, ARRAY[r_id]);

  END LOOP;


  WHILE true LOOP

    SELECT * INTO investigated_path FROM __kdb_revision_path__possible_pathes__ WHERE NOT investigated ORDER BY estimated_length, source_distance_to_root ASC LIMIT 1;

    IF investigated_path IS NULL THEN
      -- then no solution (which is weird, root of the graph should always be the worst solution)
      DROP TABLE __kdb_revision_path__possible_pathes__;
      RETURN null;
    END IF;

    UPDATE __kdb_revision_path__possible_pathes__ SET investigated = true WHERE id = investigated_path.id;

    FOR r_id IN EXECUTE 'SELECT parent FROM ' || tablename::regclass || '_versioning_deltas WHERE child = $1' USING (investigated_path.path_source)
    LOOP
      -- Look if r is already in the possible_pathes
      SELECT * INTO investigated_path_match FROM __kdb_revision_path__possible_pathes__ WHERE path_source = r_id AND path_destination = investigated_path.path_destination;
      IF NOT FOUND THEN
        -- new path candiate first get the distance to root, then insert
        EXECUTE 'SELECT distance_to_root FROM ' || tablename::regclass || '_versioning_revisions WHERE revision = $1' INTO distance_to_root USING r_id;
        INSERT INTO __kdb_revision_path__possible_pathes__(path_source, path_destination, source_distance_to_root, estimated_length, path) VALUES (r_id, investigated_path.path_destination, distance_to_root, array_length(investigated_path.path, 1) + 1, array_prepend(r_id, (investigated_path.path)));
      END IF;
    END LOOP;

    SELECT q1.path_source INTO source_r_id FROM (SELECT path_source, COUNT(*) as c FROM __kdb_revision_path__possible_pathes__ GROUP BY path_source) q1  WHERE q1.c = array_length(revisions, 1) LIMIT 1;

    IF source_r_id IS NOT NULL THEN
      FOREACH r_id IN ARRAY revisions LOOP
        SELECT * INTO result_path FROM __kdb_revision_path__possible_pathes__ WHERE source_r_id = path_source AND r_id = path_destination LIMIT 1;
        returned_path = array_append(returned_path, ROW(result_path.path)::kdb_revision_path);
      END LOOP;
      DROP TABLE __kdb_revision_path__possible_pathes__;
      RETURN ROW(returned_path);
    END IF;

  END LOOP;
END;
$$ LANGUAGE plpgsql;

