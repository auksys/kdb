CREATE EXTENSION pgcrypto;

----------------------------------------------------------------------------
---------------------------- kdb_compute_md5_table_column ------------------
----------------------------------------------------------------------------

CREATE FUNCTION kdb_compute_md5_table_column(table_name text, column_value_name text, column_value_order text) 
       RETURNS bytea
       LANGUAGE plpython3u
AS
$$

  import hashlib
  m = hashlib.md5()

  extra = ""

  if column_value_order != '':
    extra += " ORDER BY " + column_value_order

  rv = plpy.execute("SELECT " + column_value_name + " FROM " + table_name + extra)
  
  for row in rv:
    m.update(row[column_value_name])

  return m.digest()
$$;

