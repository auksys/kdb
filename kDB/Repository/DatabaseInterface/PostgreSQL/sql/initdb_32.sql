------------------------------------------------------------------------------
--------------------------------- kdb_timestamp ------------------------------
------------------------------------------------------------------------------

CREATE DOMAIN kdb_timestamp AS numeric;

CREATE FUNCTION kdb_timestamp_create(value numeric) RETURNS kdb_timestamp AS $$
SELECT value::kdb_timestamp
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_timestamp_text(value kdb_timestamp) RETURNS numeric AS $$
SELECT value::numeric
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (numeric AS kdb_timestamp) WITH FUNCTION kdb_timestamp_create(numeric);

------------------------------------------------------------------------------
------------------------------ kdb_quantity_value ----------------------------
------------------------------------------------------------------------------

CREATE TYPE kdb_quantity_value AS (
  value numeric,
  unit kdb_uri
);

CREATE FUNCTION kdb_quantity_value_create(value numeric, uri kdb_uri) RETURNS kdb_quantity_value AS $$
  SELECT ROW(value, uri)::kdb_quantity_value
$$ LANGUAGE SQL IMMUTABLE;

CREATE TABLE kdb_unit_conversion
(
  id serial PRIMARY KEY,
  source kdb_uri NOT NULL,
  destination kdb_uri NOT NULL,
  factor float8,
  UNIQUE(source, destination)
  
);

CREATE INDEX kdb_unit_conversion_index ON kdb_unit_conversion (source, destination);

CREATE FUNCTION kdb_unit_conversion_factor(source_ kdb_uri, destination_ kdb_uri) RETURNS float8 AS $$
  SELECT factor FROM kdb_unit_conversion WHERE source = source_ AND destination = destination_
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_value_convert(value numeric, source_uri kdb_uri, destination_uri kdb_uri) RETURNS numeric AS $$
  SELECT value * kdb_unit_conversion_factor(source_uri, destination_uri)::numeric
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_quantity_value_convert(value kdb_quantity_value, uri kdb_uri) RETURNS kdb_quantity_value AS $$
  SELECT ROW(kdb_value_convert(value.value, value.unit, uri), uri)::kdb_quantity_value
$$ LANGUAGE SQL IMMUTABLE;

--- SPARQL Variants

CREATE FUNCTION kdb_value_convert(a kdb_rdf_term, source_uri kdb_uri, destination_uri kdb_uri) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN (a).value IS NULL            THEN null
              WHEN ((a).value).type = 'numeric' THEN kdb_rdf_term_create((a).uri, kdb_value_convert(((a).value).numeric, source_uri, destination_uri), (a).lang)
              ELSE null
         END
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_value_convert(value kdb_rdf_term, source_uri kdb_rdf_term, destination_uri kdb_uri) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN kdb_is_uri(source_uri) THEN kdb_value_convert(value, kdb_iri(source_uri), destination_uri)
              ELSE null
         END
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_value_convert(value kdb_rdf_term, source_uri kdb_rdf_term, destination_uri kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN kdb_is_uri(destination_uri) THEN kdb_value_convert(value, source_uri, kdb_iri(destination_uri))
              ELSE null
         END
$$ LANGUAGE SQL IMMUTABLE;

------------------------------------------------------------------------------
---------------------------------- bug fixes ---------------------------------
------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION kdb_is_numeric_type(uri text) RETURNS boolean AS $$
  SELECT uri = 'http://www.w3.org/2001/XMLSchema#integer'
    OR uri = 'http://www.w3.org/2001/XMLSchema#decimal'
    OR uri = 'http://www.w3.org/2001/XMLSchema#byte'
    OR uri = 'http://www.w3.org/2001/XMLSchema#short'
    OR uri = 'http://www.w3.org/2001/XMLSchema#int'
    OR uri = 'http://www.w3.org/2001/XMLSchema#long'
    OR uri = 'http://www.w3.org/2001/XMLSchema#unsignedByte'
    OR uri = 'http://www.w3.org/2001/XMLSchema#unsignedShort'
    OR uri = 'http://www.w3.org/2001/XMLSchema#unsignedInt'
    OR uri = 'http://www.w3.org/2001/XMLSchema#unsignedLong'
    OR uri = 'http://www.w3.org/2001/XMLSchema#float'
    OR uri = 'http://www.w3.org/2001/XMLSchema#double'
$$ LANGUAGE SQL IMMUTABLE;
