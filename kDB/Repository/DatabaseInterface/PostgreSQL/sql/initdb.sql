------------------------------------------------------------------------------
--------------------------------- Extensions ---------------------------------
------------------------------------------------------------------------------

CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION plpython3u;
CREATE EXTENSION "uuid-ossp";

CREATE OR REPLACE FUNCTION raster_send(raster) RETURNS bytea
AS 'SELECT bytea($1)'
LANGUAGE SQL;

UPDATE pg_type SET typsend = 'raster_send' WHERE typname='raster';

------------------------------------------------------------------------------
-------------------------------- notifications -------------------------------
------------------------------------------------------------------------------

CREATE FUNCTION kdb_notify_change() RETURNS trigger AS $$
DECLARE
  channel text;
BEGIN
  channel   := TG_ARGV[0];
  PERFORM pg_notify(channel, '');
  RETURN NULL;
END;
$$ LANGUAGE 'plpgsql';

------------------------------------------------------------------------------
---------------------------------- numeric[] ---------------------------------
------------------------------------------------------------------------------

CREATE FUNCTION kdb_numeric_array_equal(a numeric[], b numeric[]) RETURNS boolean AS $$
  SELECT a::text = b::text
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR = (LEFTARG=numeric[], RIGHTARG=numeric[], PROCEDURE = kdb_numeric_array_equal);

CREATE FUNCTION kdb_numeric_array_different(a numeric[], b numeric[]) RETURNS boolean AS $$
  SELECT a::text != b::text
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR != (LEFTARG=numeric[], RIGHTARG=numeric[], PROCEDURE = kdb_numeric_array_different);

------------------------------------------------------------------------------
----------------------------- kdb_id_to_timestamp ----------------------------
------------------------------------------------------------------------------

CREATE TYPE kdb_id_to_timestamp AS (
  id integer,
  timestamp bigint
);

------------------------------------------------------------------------------
----------------------------------- kdb_uri ---------------------------------
------------------------------------------------------------------------------

CREATE DOMAIN kdb_uri AS text;

CREATE FUNCTION kdb_uri_create(value text) RETURNS kdb_uri AS $$
SELECT value::kdb_uri
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_uri_text(value kdb_uri) RETURNS text AS $$
SELECT value::text
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (text AS kdb_uri) WITH FUNCTION kdb_uri_create(text) AS IMPLICIT;

------------------------------------------------------------------------------
-------------------------------- kdb_rdf_value -------------------------------
------------------------------------------------------------------------------

CREATE TYPE kdb_rdf_value_type AS ENUM ('null');

CREATE TYPE kdb_rdf_value AS (
  type kdb_rdf_value_type
);

CREATE FUNCTION kdb_rdf_value_create() RETURNS kdb_rdf_value AS $$
  SELECT ROW('null')::kdb_rdf_value
$$ LANGUAGE SQL IMMUTABLE;

CREATE TABLE kdb_rdf_value_definitions
(
  id serial PRIMARY KEY,
  name text NOT NULL UNIQUE,
  typename text NOT NULL,
  operators int8,
  features int8
);

------------------------------------------------------------------------------
------------------------------- kdb_rdf_term ------------------------------
------------------------------------------------------------------------------

CREATE TYPE kdb_rdf_term AS (
  uri kdb_uri,
  value kdb_rdf_value,
  lang text
);

CREATE FUNCTION kdb_rdf_term_create(uri kdb_uri, value kdb_rdf_value, lang text) RETURNS kdb_rdf_term AS $$
  SELECT ROW(uri, value, lang)::kdb_rdf_term
$$ LANGUAGE SQL IMMUTABLE;

------------------------------------------------------------------------------
-------------------------------- kdb_rdf_triple ------------------------------
------------------------------------------------------------------------------

CREATE TYPE kdb_rdf_triple AS (
  subject kdb_uri,
  predicate kdb_uri,
  object kdb_rdf_term,
  md5 bytea
);

------------------------------------------------------------------------------
--------------------------------- kdb_rdf_quad -------------------------------
------------------------------------------------------------------------------

CREATE TYPE kdb_rdf_quad AS (
  graph kdb_uri,
  subject kdb_uri,
  predicate kdb_uri,
  object kdb_rdf_term,
  md5 bytea
);

CREATE FUNCTION kdb_rdf_quad_create(graph kdb_uri, subject kdb_uri, predicate kdb_uri, object kdb_rdf_term, md5 bytea) RETURNS kdb_rdf_quad AS $$
  SELECT ROW(graph, subject, predicate, object, md5)::kdb_rdf_quad
$$ LANGUAGE SQL IMMUTABLE;

------------------------------------------------------------------------------
-------------------------- function definitions ------------------------------
------------------------------------------------------------------------------

CREATE TABLE function_definitions
(
  id            bigserial PRIMARY KEY,
  sparql_name   text NOT NULL,
  sql_template  text NOT NULL,
  return        text NOT NULL,
  parameters    text[] NOT NULL,
  
  UNIQUE (sparql_name, sql_template, return, parameters)
);

------------------------------------------------------------------------------
--------------------------------- extensions ---------------------------------
------------------------------------------------------------------------------
-- this table is used to list enabled extensions

CREATE TABLE extensions
(
  id          bigserial PRIMARY KEY,
  name        text NOT NULL,
  UNIQUE (name)
);

CREATE TRIGGER extensions_notify_update AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON extensions FOR EACH STATEMENT EXECUTE PROCEDURE kdb_notify_change("kdb_extensions_changes");

------------------------------------------------------------------------------
-------------------------------- unique_tables -------------------------------
------------------------------------------------------------------------------
-- this table is used to register a unique tables name for a given prefix
-- and a given key

CREATE TABLE unique_tables
(
  id serial PRIMARY KEY,
  prefix text NOT NULL,
  key text NOT NULL,
  UNIQUE(key, prefix)
);

------------------------------------------------------------------------------
----------------------------------- triples ----------------------------------
------------------------------------------------------------------------------

CREATE TABLE triples_stores
(
  id serial PRIMARY KEY,
  name text NOT NULL,
  tablename text,
  options int2,
  meta jsonb,
  UNIQUE(name)
);


CREATE TRIGGER triples_stores_notify_update AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON triples_stores FOR EACH STATEMENT EXECUTE PROCEDURE kdb_notify_change("triples_stores_changes");

CREATE TABLE views
(
  id serial PRIMARY KEY,
  name text NOT NULL,
  definition text NOT NULL,
  UNIQUE (name)
);

CREATE TRIGGER views_notify_update AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON views FOR EACH STATEMENT EXECUTE PROCEDURE kdb_notify_change("views_changes");

CREATE TABLE unions
(
  union_name text NOT NULL,
  graph_name text NOT NULL,
  UNIQUE(union_name, graph_name)
);

CREATE TRIGGER unions_notify_update AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON unions FOR EACH STATEMENT EXECUTE PROCEDURE kdb_notify_change("unions_changes");

------------------------------------------------------------------------------
---------------------------- kdb_is_numeric_type -----------------------------
------------------------------------------------------------------------------

CREATE FUNCTION kdb_is_numeric_type(uri text) RETURNS boolean AS $$
  SELECT uri = 'http://www.w3.org/2001/XMLSchema#integer'
    OR uri = 'http://www.w3.org/2001/XMLSchema#decimal'
    OR uri = 'http://www.w3.org/2001/XMLSchema#byte'
    OR uri = 'http://www.w3.org/2001/XMLSchema#short'
    OR uri = 'http://www.w3.org/2001/XMLSchema#int'
    OR uri = 'http://www.w3.org/2001/XMLSchema#long'
    OR uri = 'http://www.w3.org/2001/XMLSchema#unsignedByte'
    OR uri = 'http://www.w3.org/2001/XMLSchema#unsignedShort'
    OR uri = 'http://www.w3.org/2001/XMLSchema#unsignedInt'
    OR uri = 'http://www.w3.org/2001/XMLSchema#unsignedLong'
    OR uri = 'http://www.w3.org/2001/XMLSchema#float'
    OR uri = 'http://www.w3.org/2001/XMLSchema#double'
$$ LANGUAGE SQL IMMUTABLE;
