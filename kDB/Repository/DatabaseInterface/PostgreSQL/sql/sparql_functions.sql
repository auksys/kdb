CREATE FUNCTION kdb_str(a kdb_uri) RETURNS kdb_rdf_term AS $$
    SELECT kdb_rdf_term_create('http://www.w3.org/2001/XMLSchema#string' , kdb_rdf_value_create(a), '')
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_str(a kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN (a).value IS NULL            THEN kdb_rdf_term_create('http://www.w3.org/2001/XMLSchema#string' , kdb_rdf_value_create((a).uri), '')
              WHEN ((a).value).type = 'text'    THEN kdb_rdf_term_create('http://www.w3.org/2001/XMLSchema#string', (a).value, '')
              WHEN ((a).value).type = 'numeric' THEN kdb_rdf_term_create('http://www.w3.org/2001/XMLSchema#string', kdb_rdf_value_create((((a).value).numeric)::text), '')
              ELSE null -- ('Cannot convert type ' || (a).uri || ' ' || ((a).value).type || ' to string')
         END
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_is_blank(a text) RETURNS boolean AS $$
  SELECT substring(a for 25) = 'http://askco.re/db/blank#'
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_is_blank(a kdb_rdf_term) RETURNS boolean AS $$
  SELECT ((a).value IS NULL OR ((a).value).type = 'null') AND kdb_is_blank((a).uri)
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_is_literal(a kdb_rdf_term) RETURNS boolean AS $$
  SELECT NOT ((a).value IS NULL) AND ((a).value.type != 'null')
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_is_uri(a kdb_rdf_term) RETURNS boolean AS $$
  SELECT ((a).value IS NULL OR ((a).value).type = 'null') AND NOT kdb_is_blank(a)
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_datatype(a kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN (a).value IS NULL            THEN null
              WHEN (a).lang != '' AND (a).uri = 'http://www.w3.org/2001/XMLSchema#string' THEN kdb_rdf_term_create('http://www.w3.org/2001/XMLSchema#string', kdb_rdf_value_create('http://www.w3.org/1999/02/22-rdf-syntax-ns#langString'), '')
              ELSE kdb_rdf_term_create('http://www.w3.org/2001/XMLSchema#string' , (a).uri::text, '')
         END
$$ LANGUAGE SQL IMMUTABLE;

--- kdb_bound

CREATE FUNCTION kdb_bound(a kdb_rdf_term) RETURNS boolean AS $$
  SELECT (a).uri != '' AND NOT ((a).uri IS NULL)
$$ LANGUAGE SQL IMMUTABLE;
--   NOT ((a).value IS NULL OR ((a).value).type = 'null' OR (a).uri IS NULL OR (a).uri = '')

--- kdb_lang

CREATE FUNCTION kdb_lang(a kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN kdb_is_literal(a) THEN kdb_rdf_term_create('http://www.w3.org/2001/XMLSchema#string', (a).lang , '') ELSE null END
$$ LANGUAGE SQL IMMUTABLE;

--- kdb_lang_matches

--- basic matches from http://www.ietf.org/rfc/rfc4647.txt

CREATE FUNCTION kdb_lang_matches(language_tag kdb_rdf_term, language_range kdb_rdf_term) RETURNS boolean AS $$
  SELECT CASE WHEN (language_tag).value IS NULL THEN null
              ELSE ((language_tag).value).text != '' AND (((language_range).value).text = '*' OR substring(lower(((language_tag).value).text) for char_length(((language_range).value).text) ) = lower(((language_range).value).text)) END
$$ LANGUAGE SQL IMMUTABLE;

--- kdb_extended_lang_matches

--- follow http://www.ietf.org/rfc/rfc4647.txt

CREATE FUNCTION kdb_extended_lang_matches(language_tag text[], language_range text[], index_tag int, index_range int) RETURNS boolean AS $$
  SELECT CASE WHEN index_range > array_length(language_range, 1)  THEN true
              WHEN language_range[index_range] = '*'              THEN kdb_extended_lang_matches(language_tag, language_range, index_tag + 1, index_range + 1)
              WHEN index_tag > array_length(language_tag, 1)      THEN false
              WHEN lower(language_tag[index_tag]) = lower(language_range[index_range])
                                                                  THEN kdb_extended_lang_matches(language_tag, language_range, index_tag + 1, index_range + 1)
              WHEN char_length(language_tag[index_tag]) = 1       THEN false
              ELSE kdb_extended_lang_matches(language_tag, language_range, index_tag, index_range + 1) END
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_extended_lang_matches(language_tag text[], language_range text[]) RETURNS boolean AS $$
  SELECT (lower(language_tag[1]) = lower(language_range[1]) or lower(language_range[1]) = '*') AND kdb_extended_lang_matches(language_tag, language_range, 2, 2)
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_extended_lang_matches(language_tag kdb_rdf_term, language_range text) RETURNS boolean AS $$
  SELECT language_range = '*' OR kdb_extended_lang_matches(regexp_split_to_array(((language_tag).value).text, '-'), regexp_split_to_array(language_range, '-'))
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_extended_lang_matches(language_tag kdb_rdf_term, language_range kdb_rdf_term) RETURNS boolean AS $$
  SELECT CASE WHEN (language_tag).value IS NULL THEN null
              ELSE ((language_tag).value).text != '' AND kdb_extended_lang_matches(language_tag, ((language_range).value).text) END
$$ LANGUAGE SQL IMMUTABLE;

-- kdb_same_term

CREATE FUNCTION kdb_same_term(term1 kdb_rdf_term, term2 kdb_rdf_term) RETURNS boolean AS $$
  SELECT term1 = term2
$$ LANGUAGE SQL IMMUTABLE;

-- kdb_bnode

CREATE FUNCTION kdb_bnode() RETURNS text AS $$
  SELECT 'http://askco.re/db/blank#' || uuid_generate_v4();
$$ LANGUAGE SQL;

-- kdb_iri

CREATE FUNCTION kdb_iri(term kdb_rdf_term) RETURNS kdb_uri AS $$
  SELECT (term).uri
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_iri(uri kdb_uri) RETURNS kdb_uri AS $$
  SELECT uri
$$ LANGUAGE SQL IMMUTABLE;

-- kdb_strstarts

CREATE FUNCTION kdb_strstarts(arg1 kdb_rdf_term, arg2 kdb_rdf_term) RETURNS boolean AS $$ 
  SELECT ((arg1).value).text SIMILAR TO (((arg2).value).text || '%')
$$ LANGUAGE SQL IMMUTABLE;

-- kdb_strends

CREATE FUNCTION kdb_strends(arg1 kdb_rdf_term, arg2 kdb_rdf_term) RETURNS boolean AS $$ 
  SELECT ((arg1).value).text SIMILAR TO ('%' || ((arg2).value).text)
$$ LANGUAGE SQL IMMUTABLE;

-- kdb_contains

CREATE FUNCTION kdb_contains(arg1 kdb_rdf_term, arg2 kdb_rdf_term) RETURNS boolean AS $$ 
  SELECT ((arg1).value).text SIMILAR TO ('%' || ((arg2).value).text || '%')
$$ LANGUAGE SQL IMMUTABLE;
