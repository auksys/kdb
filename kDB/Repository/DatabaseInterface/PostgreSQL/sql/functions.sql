--------------------------------
-- kdb_str

CREATE FUNCTION kdb_str(value int) RETURNS text AS $$
SELECT value::text
$$ LANGUAGE SQL IMMUTABLE;

--------------------------------
-- kdb_strcmp

CREATE FUNCTION kdb_strcmp(a text, b text) RETURNS int AS $$
SELECT CASE WHEN a < b THEN -1
            WHEN a > b THEN 1
            ELSE 0 END
$$ LANGUAGE SQL IMMUTABLE;

--------------------------------
-- kdb_jsonb_set_deep

CREATE OR REPLACE FUNCTION kdb_jsonb_set_deep(target jsonb, path text[], val jsonb)
  RETURNS jsonb AS $$
    DECLARE
      k text;
      p text[];
    BEGIN
      -- Create missing objects in the path.
      FOREACH k IN ARRAY path LOOP
        p := p || k;
        IF (target #> p IS NULL) THEN
          target := jsonb_set(target, p, '{}'::jsonb);
        END IF;
      END LOOP;
      -- Set the value like normal.
      RETURN jsonb_set(target, path, val, true);
    END;
  $$ LANGUAGE plpgsql;
