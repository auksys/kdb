------------------------------------------------------------------------------
--------------------------------- constructor --------------------------------
------------------------------------------------------------------------------

-- kdb_rdf_value_create identity

CREATE FUNCTION kdb_rdf_value_create(value kdb_rdf_value) RETURNS kdb_rdf_value AS $$
  SELECT value
$$ LANGUAGE SQL IMMUTABLE;

-- kdb_rdf_value_create for numeric

CREATE FUNCTION kdb_rdf_value_create(value bigint) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::numeric)
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (bigint AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(bigint) AS IMPLICIT;

CREATE FUNCTION kdb_rdf_value_create(value integer) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::numeric)
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (integer AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(integer) AS IMPLICIT;

CREATE FUNCTION kdb_rdf_value_create(value real) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::numeric)
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (real AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(real) AS IMPLICIT;

CREATE FUNCTION kdb_rdf_value_create(value double precision) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::numeric)
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (double precision AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(double precision) AS IMPLICIT;

-- kdb_rdf_value_create for numeric[]

CREATE FUNCTION kdb_rdf_value_create(value bigint[]) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::numeric[])
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (bigint[] AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(bigint[]) AS IMPLICIT;

CREATE FUNCTION kdb_rdf_value_create(value integer[]) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::numeric[])
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (integer[] AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(integer[]) AS IMPLICIT;

CREATE FUNCTION kdb_rdf_value_create(value real[]) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::numeric[])
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (real[] AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(real[]) AS IMPLICIT;

CREATE FUNCTION kdb_rdf_value_create(value double precision[]) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::numeric[])
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (double precision[] AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(double precision[]) AS IMPLICIT;

-- kdb_rdf_value_create for kdb_uri

CREATE FUNCTION kdb_rdf_value_create(value kdb_uri) RETURNS kdb_rdf_value AS $$
  SELECT kdb_rdf_value_create(value::text)
$$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (kdb_uri AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(kdb_uri) AS IMPLICIT;

-- comparison operator

CREATE FUNCTION kdb_rdf_value_equal(a text, b kdb_rdf_value) RETURNS boolean AS $$
  SELECT NOT (b IS NULL) AND (b).type = 'text' AND a = (b).text
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR = (LEFTARG=text, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_equal);

CREATE FUNCTION kdb_rdf_value_equal(a kdb_rdf_value, b text) RETURNS boolean AS $$
  SELECT kdb_rdf_value_equal(b, a)
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR = (LEFTARG=kdb_rdf_value, RIGHTARG=text, PROCEDURE = kdb_rdf_value_equal);

-- operators

CREATE OPERATOR +  (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_addition);
CREATE OPERATOR -  (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_substraction);
CREATE OPERATOR -  (                       RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_minus);
CREATE OPERATOR *  (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_multiplication);
CREATE OPERATOR /  (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_division);
CREATE OPERATOR <  (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_inferior);
CREATE OPERATOR <= (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_inferior_equal);
CREATE OPERATOR >  (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_superior);
CREATE OPERATOR >= (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_superior_equal);
CREATE OPERATOR =  (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_equal);
CREATE OPERATOR != (LEFTARG=kdb_rdf_value, RIGHTARG=kdb_rdf_value, PROCEDURE = kdb_rdf_value_not_equal);
