CREATE FUNCTION kdb_rdf_term_create(value kdb_rdf_term) RETURNS kdb_rdf_term AS $$
SELECT value
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_rdf_term_create(value kdb_uri) RETURNS kdb_rdf_term AS $$
  SELECT kdb_rdf_term_create(value, null, '');
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_rdf_term_create(url text, value kdb_rdf_value, lang text) RETURNS kdb_rdf_term AS $$
  SELECT kdb_rdf_term_create(url::kdb_uri, value, lang);
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_rdf_term_uri(value kdb_rdf_term) RETURNS text AS $$
  SELECT (value).uri;
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_rdf_term_uri(value text) RETURNS text AS $$
    SELECT value;
$$ LANGUAGE SQL IMMUTABLE;

--------------------------------
-- kdb_view_create_typed_literal

CREATE FUNCTION kdb_rdf_term_create_typed_literal(value kdb_rdf_value, uri text) RETURNS kdb_rdf_term AS $$
    SELECT kdb_rdf_term_create(uri, value, '');
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_rdf_term_create_typed_literal(value kdb_rdf_value, object kdb_rdf_term) RETURNS kdb_rdf_term AS $$
    SELECT kdb_rdf_term_create((object).uri, value, '');
$$ LANGUAGE SQL IMMUTABLE;

--------------------------------
-- comparison operators

-- CREATE FUNCTION kdb_rdf_term_equal(a kdb_uri, b kdb_rdf_term) RETURNS boolean AS $$
--   SELECT CASE WHEN b is null or a is null THEN false
--               ELSE (b).value is null OR ((b).value).type = 'null' AND a = (b).uri END
-- $$ LANGUAGE SQL IMMUTABLE;
-- 
-- CREATE OPERATOR = (LEFTARG=kdb_uri, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_equal);
-- 
-- CREATE FUNCTION kdb_rdf_term_equal(a kdb_rdf_term, b kdb_uri) RETURNS boolean AS $$
--   SELECT kdb_rdf_term_equal(b, a)
-- $$ LANGUAGE SQL IMMUTABLE;
-- 
-- CREATE OPERATOR = (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_uri, PROCEDURE = kdb_rdf_term_equal);

CREATE FUNCTION kdb_rdf_term_equal(a text, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT CASE WHEN b is null or a is null THEN false
              WHEN (b).value is null OR ((b).value).type = 'null' THEN a = (b).uri
              ELSE a = (b).value END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR = (LEFTARG=text, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_equal);

CREATE FUNCTION kdb_rdf_term_equal(a kdb_rdf_term, b text) RETURNS boolean AS $$
  SELECT kdb_rdf_term_equal(b, a)
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR = (LEFTARG=kdb_rdf_term, RIGHTARG=text, PROCEDURE = kdb_rdf_term_equal);

CREATE FUNCTION kdb_rdf_term_not_equal(b kdb_rdf_term, a text) RETURNS boolean AS $$
  SELECT NOT (a IS NULL) AND NOT (b IS NULL) AND NOT kdb_rdf_term_equal(a, b)
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR != (LEFTARG=kdb_rdf_term, RIGHTARG=text, PROCEDURE = kdb_rdf_term_not_equal);

CREATE FUNCTION kdb_rdf_term_not_equal(a text, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT kdb_rdf_term_not_equal(b, a)
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR != (LEFTARG=text, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_not_equal);

CREATE FUNCTION kdb_rdf_term_equal(a kdb_rdf_term, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT ((a).uri = (b).uri OR (kdb_is_numeric_type((a).uri) AND kdb_is_numeric_type((b).uri)))
      AND ((a).value = (b).value OR ((a).value IS NULL AND (b).value IS NULL) )
      AND (lower((a).lang) = lower((b).lang) OR (((a).lang = '') IS NOT FALSE AND ((b).lang = '') IS NOT FALSE))
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR = (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_equal);

-- <

CREATE FUNCTION kdb_rdf_term_inferior(a kdb_rdf_term, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT ((a).uri = (b).uri or (kdb_is_numeric_type((a).uri) and kdb_is_numeric_type((b).uri)))
      and ((a).value < (b).value or ((a).value is null and (b).value is null) )
      and (lower((a).lang) = lower((b).lang) or ((a).lang is null and (b).lang is null))
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR < (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_inferior);

-- <=

CREATE FUNCTION kdb_rdf_term_inferior_equal(a kdb_rdf_term, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT ((a).uri = (b).uri or (kdb_is_numeric_type((a).uri) and kdb_is_numeric_type((b).uri)))
      and ((a).value <= (b).value or ((a).value is null and (b).value is null) )
      and (lower((a).lang) = lower((b).lang) or ((a).lang is null and (b).lang is null))
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR <= (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_inferior_equal);

-- >

CREATE FUNCTION kdb_rdf_term_superior(a kdb_rdf_term, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT ((a).uri = (b).uri or (kdb_is_numeric_type((a).uri) and kdb_is_numeric_type((b).uri)))
      and ((a).value > (b).value or ((a).value is null and (b).value is null) )
      and (lower((a).lang) = lower((b).lang) or ((a).lang is null and (b).lang is null))
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR > (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_superior);

-- >=

CREATE FUNCTION kdb_rdf_term_superior_equal(a kdb_rdf_term, b kdb_rdf_term) RETURNS boolean AS $$
  SELECT ((a).uri = (b).uri or (kdb_is_numeric_type((a).uri) and kdb_is_numeric_type((b).uri)))
      and ((a).value >= (b).value or ((a).value is null and (b).value is null) )
      and (lower((a).lang) = lower((b).lang) or ((a).lang is null and (b).lang is null))
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR >= (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_superior_equal);

--------------------------------
-- arithmetic operator

CREATE FUNCTION kdb_rdf_term_addition(a kdb_rdf_term, b kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN (a).lang != (b).lang THEN null -- ('Arithmetic operation error, not the same langauge ' || (a).lang || ' ' || (b).lang)
              WHEN (b).uri = (a).uri OR (kdb_is_numeric_type((a).uri) and kdb_is_numeric_type((b).uri)) THEN kdb_rdf_term_create((a).uri, (a).value + (b).value, (a).lang)
              ELSE null -- ('Arithmetic operation not supported on different datatypes. ' || (a).lang || ' ' || (b).uri)
         END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR + (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_addition);

CREATE FUNCTION kdb_rdf_term_substraction(a kdb_rdf_term, b kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN (a).lang != (b).lang THEN null -- ('Arithmetic operation error, not the same langauge ' || (a).lang || ' ' || (b).lang)
              WHEN (b).uri = (a).uri OR (kdb_is_numeric_type((a).uri) and kdb_is_numeric_type((b).uri)) THEN kdb_rdf_term_create((a).uri, (a).value - (b).value, (a).lang)
              ELSE null -- ('Arithmetic operation not supported on different datatypes. ' || (a).lang || ' ' || (b).uri)
         END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR - (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_substraction);

CREATE FUNCTION kdb_rdf_term_minus(a kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT kdb_rdf_term_create((a).uri, -(a).value, (a).lang)
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR - (RIGHTARG=kdb_rdf_term,                          PROCEDURE = kdb_rdf_term_minus);

CREATE FUNCTION kdb_rdf_term_multiplication(a kdb_rdf_term, b kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN (a).lang != (b).lang THEN null -- ('Arithmetic operation error, not the same langauge ' || (a).lang || ' ' || (b).lang)
              WHEN (b).uri = (a).uri OR (kdb_is_numeric_type((a).uri) and kdb_is_numeric_type((b).uri)) THEN kdb_rdf_term_create((a).uri, (a).value * (b).value, (a).lang)
              ELSE null -- ('Arithmetic operation not supported on different datatypes. ' || (a).lang || ' ' || (b).uri)
         END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR * (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_multiplication);

CREATE FUNCTION kdb_rdf_term_division(a kdb_rdf_term, b kdb_rdf_term) RETURNS kdb_rdf_term AS $$
  SELECT CASE WHEN (a).lang != (b).lang THEN null -- ('Arithmetic operation error, not the same langauge ' || (a).lang || ' ' || (b).lang)
              WHEN (b).uri = (a).uri OR (kdb_is_numeric_type((a).uri) and kdb_is_numeric_type((b).uri)) THEN kdb_rdf_term_create((a).uri, (a).value / (b).value, (a).lang)
              ELSE null -- ('Arithmetic operation not supported on different datatypes. ' || (a).lang || ' ' || (b).uri)
         END
$$ LANGUAGE SQL IMMUTABLE;

CREATE OPERATOR / (LEFTARG=kdb_rdf_term, RIGHTARG=kdb_rdf_term, PROCEDURE = kdb_rdf_term_division);



