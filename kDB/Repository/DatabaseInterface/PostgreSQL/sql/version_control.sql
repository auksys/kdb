CREATE TYPE kdb_revision_path_return AS(path_to_source int[], path_to_destination int[]);
CREATE TYPE kdb_revision_path_fp_ AS(id int, path_destination int, estimated_length int, path int[]);
CREATE TYPE kdb_revision_path_rp_ AS(id int, path_source int, path_destination int, source_distance_to_root int, estimated_length int, path int[]);

CREATE FUNCTION kdb_last_array_element(value int[]) RETURNS int AS $$
  SELECT value[array_length(value, 1)]
$$ LANGUAGE SQL IMMUTABLE;

-- 
-- This function try to find a straight path between two revisions or return empty if it cannot find one
-- 

CREATE FUNCTION kdb_revision_forward_path(tablename text, source_revision int, destination_revision int) RETURNS int[] AS $$
DECLARE
  investigated_path kdb_revision_path_fp_;
  r_id   int;
  r_id_distance_to_root   int;
  destination_distance_to_root int;
BEGIN

  CREATE TEMPORARY TABLE __kdb_revision_forward_path__possible_pathes__(
          id SERIAL PRIMARY KEY,
          path_destination int NOT NULL,
          estimated_length int NOT NULL,
          path int[] NOT NULL,
          investigated boolean DEFAULT false) ;

  CREATE INDEX __kdb_revision_forward_path__possible_pathes__index_1__ ON __kdb_revision_forward_path__possible_pathes__ USING btree(estimated_length);
  CREATE INDEX __kdb_revision_forward_path__possible_pathes__index_2__ ON __kdb_revision_forward_path__possible_pathes__ USING btree(path_destination);
  
  EXECUTE 'SELECT distance_to_root FROM ' || tablename::regclass || '_versioning_revisions WHERE revision = $1' INTO destination_distance_to_root USING destination_revision;

  INSERT INTO __kdb_revision_forward_path__possible_pathes__(path_destination, estimated_length, path) VALUES
      (source_revision, 1, ARRAY[source_revision]);

  WHILE true LOOP
  
    SELECT id, path_destination, estimated_length, path INTO investigated_path FROM __kdb_revision_forward_path__possible_pathes__ WHERE NOT investigated
              ORDER BY estimated_length LIMIT 1;
  
    IF investigated_path IS NULL THEN
      DROP TABLE __kdb_revision_forward_path__possible_pathes__;
      RETURN null;
    END IF;
    
    UPDATE __kdb_revision_forward_path__possible_pathes__ SET investigated = true WHERE id = investigated_path.id;
    
    -- Look for parent of pp
    FOR r_id IN EXECUTE 'SELECT child FROM ' || tablename::regclass || '_versioning_deltas WHERE parent = $1' USING (investigated_path.path_destination)
      LOOP
        -- If r_id is the destination_revision then we have reached our destination
        IF destination_revision = r_id THEN
          DROP TABLE __kdb_revision_forward_path__possible_pathes__;
          RETURN array_append((investigated_path.path), r_id);
        END IF;
        
        EXECUTE 'SELECT distance_to_root FROM ' || tablename::regclass || '_versioning_revisions WHERE revision = $1' INTO r_id_distance_to_root USING r_id;

        IF r_id_distance_to_root < destination_distance_to_root AND NOT EXISTS (SELECT 1 FROM __kdb_revision_forward_path__possible_pathes__ WHERE path_destination = r_id) THEN
          
          INSERT INTO __kdb_revision_forward_path__possible_pathes__(path_destination, estimated_length, path) VALUES
            (r_id, (investigated_path.estimated_length) + 1, array_append((investigated_path.path), r_id));
        END IF; -- Otherwise, just a longer path
      END LOOP;
  END LOOP;
  RETURN null;
END;
$$ LANGUAGE plpgsql;

-- 
-- This function is used to find the shortest path and closest common ancestor between two revision
-- 
CREATE FUNCTION kdb_revision_path(tablename text, source_revision int, destination_revision int) RETURNS kdb_revision_path_return AS $$
DECLARE
  investigated_path kdb_revision_path_rp_;
  investigated_path_source kdb_revision_path_rp_;
  investigated_path_destination kdb_revision_path_rp_;
  investigated_path_match kdb_revision_path_rp_;
  r_id   int;
  r_id_distance_to_root   int;
  source_distance_to_root int;
  destination_distance_to_root int;
BEGIN
  ---
  --- The algorithm start from source_hash and destination_hash and try to move down from each, filling an array called possible_pathes.
  --- 1) The possible_pathes array is initialised with two path containing the source and destination
  --- 2) pathes_to_investigate is initialised with possible_pathes
  --- 3) For each path in pathes_to_investigate
  ---   a) look for the parents of the "source"
  ---   b) for each parent of the source, check if that source is already in the possible path
  ---      If that is the case, we might have found a destination or a longer path
  ---      Otherwise we create a new possible path and add it to the next_pathes_to_investigate
  --- 4) assign next_pathes_to_investigate to pathes_to_investigate and restart from 3)
 
  IF source_revision = destination_revision THEN
    DROP TABLE __kdb_revision_path__possible_pathes__;
    RETURN ROW(ARRAY[source_revision], ARRAY[source_revision]);
  END IF;
 
  CREATE TEMPORARY TABLE __kdb_revision_path__possible_pathes__(
          id SERIAL PRIMARY KEY,
          path_source int NOT NULL,
          path_destination int NOT NULL,
          source_distance_to_root int NOT NULL,
          estimated_length int NOT NULL,
          path int[] NOT NULL,
          investigated boolean DEFAULT false) ;
  CREATE INDEX __kdb_revision_path__possible_pathes__index_1__ ON __kdb_revision_path__possible_pathes__ USING btree(path_destination, estimated_length);
  CREATE INDEX __kdb_revision_path__possible_pathes__index_2__ ON __kdb_revision_path__possible_pathes__ USING btree(path_source);
  
  EXECUTE 'SELECT distance_to_root FROM ' || tablename::regclass || '_versioning_revisions WHERE revision = $1' INTO source_distance_to_root USING source_revision;
  EXECUTE 'SELECT distance_to_root FROM ' || tablename::regclass || '_versioning_revisions WHERE revision = $1' INTO destination_distance_to_root USING destination_revision;

  INSERT INTO __kdb_revision_path__possible_pathes__(path_source, path_destination, source_distance_to_root, estimated_length, path) VALUES
      (destination_revision, destination_revision, destination_distance_to_root, 1, ARRAY[destination_revision]),
      (source_revision, source_revision, source_distance_to_root, 1, ARRAY[source_revision]);
 
  WHILE true LOOP
    SELECT * INTO investigated_path_destination FROM __kdb_revision_path__possible_pathes__ WHERE NOT investigated AND path_destination = destination_revision ORDER BY estimated_length LIMIT 1;
    SELECT * INTO investigated_path_source FROM __kdb_revision_path__possible_pathes__ WHERE NOT investigated AND path_destination = source_revision ORDER BY estimated_length LIMIT 1;
 
    -- select the path to investigate based on the shortest distance to root 
    IF NOT(investigated_path_destination IS NULL) THEN
      IF NOT(investigated_path_source IS NULL) THEN
        IF investigated_path_source.source_distance_to_root < investigated_path_destination.source_distance_to_root
            OR (investigated_path_source.source_distance_to_root = investigated_path_destination.source_distance_to_root
                AND investigated_path_source.estimated_length > investigated_path_destination.source_distance_to_root) THEN
          investigated_path = investigated_path_destination;
        ELSE
          investigated_path = investigated_path_source;
        END IF;
      ELSE
        investigated_path = investigated_path_destination;
      END IF;
    ELSIF NOT(investigated_path_source IS NULL) THEN
      investigated_path = investigated_path_source;
    ELSE
      -- then no solution
      DROP TABLE __kdb_revision_path__possible_pathes__;
      RETURN null;
    END IF;

    UPDATE __kdb_revision_path__possible_pathes__ SET investigated = true WHERE id = investigated_path.id;
    
    -- Look for parent of investigated_path
    FOR r_id IN EXECUTE 'SELECT parent FROM ' || tablename::regclass || '_versioning_deltas WHERE child = $1' USING (investigated_path.path_source)
    LOOP
      -- Look if r is already in the possible_pathes
      SELECT * INTO investigated_path_match FROM __kdb_revision_path__possible_pathes__ WHERE path_source = r_id ;
      IF FOUND THEN
        IF(investigated_path_match.path_destination = destination_revision AND investigated_path.path_destination = source_revision)
        THEN
          DROP TABLE __kdb_revision_path__possible_pathes__;
          RETURN ROW(array_prepend(r_id, (investigated_path.path) ), (investigated_path_match.path));
        ELSIF(investigated_path_match.path_destination = source_revision AND investigated_path.path_destination = destination_revision)
        THEN
          DROP TABLE __kdb_revision_path__possible_pathes__;
          
          RETURN ROW((investigated_path_match.path), array_prepend(r_id, (investigated_path.path)));
          
        END IF;
        -- Just a longer way to reach a node, then we just ignore it
      ELSE
        -- new path candiate first get the distance to root, then insert
        EXECUTE 'SELECT distance_to_root FROM ' || tablename::regclass || '_versioning_revisions WHERE revision = $1' INTO r_id_distance_to_root USING r_id;
        INSERT INTO __kdb_revision_path__possible_pathes__(path_source, path_destination, source_distance_to_root, estimated_length, path) VALUES (r_id, investigated_path.path_destination, r_id_distance_to_root, array_length(investigated_path.path, 1) + 1, array_prepend(r_id, (investigated_path.path)));
      END IF;
      
    END LOOP;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

---
--- This trigger is used to record changes in a triple store into a temporary table
---

CREATE FUNCTION kdb_record_triple_change() RETURNS trigger AS $$
DECLARE
  tablename text;
BEGIN
  tablename   := TG_ARGV[0];
  
  CASE TG_OP
    WHEN 'INSERT' THEN
      EXECUTE 'INSERT INTO ' || tablename || ' VALUES ($1, $2, $3, $4)' USING 'INSERT', NEW.subject, NEW.predicate, NEW.object;
    WHEN 'UPDATE' THEN
      EXECUTE 'INSERT INTO ' || tablename || ' VALUES ($1, $2, $3, $4)' USING 'REMOVE', OLD.subject, OLD.predicate, OLD.object;
      EXECUTE 'INSERT INTO ' || tablename || ' VALUES ($1, $2, $3, $4)' USING 'INSERT', NEW.subject, NEW.predicate, NEW.object;
    WHEN 'DELETE' THEN
      EXECUTE 'INSERT INTO ' || tablename || ' VALUES ($1, $2, $3, $4)' USING 'REMOVE', OLD.subject, OLD.predicate, OLD.object;
  END CASE;
  
  RETURN NULL;
END;
$$ LANGUAGE 'plpgsql';
