--------------------------------
-- kdb_view_uri

CREATE FUNCTION kdb_view_uri(u1 text) RETURNS kdb_rdf_term AS $$
SELECT kdb_rdf_term_create(u1, null, '');
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_view_uri(u1 text, u2 text) RETURNS kdb_rdf_term AS $$
SELECT kdb_rdf_term_create(u1 || u2, null, '');
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_view_uri(u1 kdb_rdf_term, u2 text) RETURNS kdb_rdf_term AS $$
SELECT kdb_rdf_term_create((u1).uri || u2, null, '');
$$ LANGUAGE SQL IMMUTABLE;

CREATE FUNCTION kdb_view_uri(u1 kdb_rdf_term, u2 int4) RETURNS kdb_rdf_term AS $$
SELECT kdb_rdf_term_create((u1).uri || u2::text, null, '');
$$ LANGUAGE SQL IMMUTABLE;
