extern "C"
{
#undef foreach
#include <postgres.h>
#include <postgres_fe.h>
#include <utils/elog.h>

// Must be included after the others
#include <catalog/pg_type.h>
}
