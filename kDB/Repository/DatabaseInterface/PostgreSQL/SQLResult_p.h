#ifndef _KDB_REPOSITORY_SQLRESULT_H_
#define _KDB_REPOSITORY_SQLRESULT_H_

#include <kDB/Forward.h>

#include <knowDBC/Interfaces/Result.h>

namespace kDB::Repository::DatabaseInterface::PostgreSQL
{
  class SQLResult : public knowDBC::Interfaces::Result
  {
    friend class SQLQueryExecutor;
    SQLResult(void* _result, const QString& _query, const QString& _error, Connection _connection);
  public:
    SQLResult() = delete;
    SQLResult(const SQLResult& _rhs) = delete;
    SQLResult& operator=(const SQLResult& _rhs) = delete;
    ~SQLResult();
    int tuples() const final;
    int fields() const final;
    Type type() const final;
    QStringList fieldNames() const final;
    knowCore::Value value(int _tuple, int _field) const final;
    QString query() const final;
    QString error() const final;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL

#endif
