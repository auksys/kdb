#include "RDFTermBinaryMarshal.h"

#include <knowCore/Timestamp.h>
#include <knowCore/Uri.h>

#include <knowRDF/Object.h>
#include <knowRDF/Skolemisation.h>

#include "BinaryInterface_read_p.h"
#include "BinaryInterface_write_p.h"
#include "Connection_p.h"
#include "RDFValueBinaryMarshal.h"
#include "SQLInterface_p.h"

using namespace kDB::Repository::DatabaseInterface::PostgreSQL;

struct RDFTermBinaryMarshal::Private
{
  RDFValueBinaryMarshal* literal_marhal;
  knowCore::WeakReference<Connection> connection;
  Oid rdf_value_oid;

  struct Serialisers
  {
    Serialisers();
    ~Serialisers() { qDeleteAll(serialisers); }
    QHash<knowCore::Uri, AbstractSerialiser*> serialisers;
  };

  static Serialisers s_serialisers;
};

RDFTermBinaryMarshal::Private::Serialisers RDFTermBinaryMarshal::Private::s_serialisers;

namespace
{
  class DateTimeSerialiser : public RDFTermBinaryMarshal::AbstractSerialiser
  {
  public:
    virtual ~DateTimeSerialiser() {}
    cres_qresult<knowCore::Value> serialise(const knowCore::Value& _value) const override
    {
      cres_try(knowCore::Timestamp dt, _value.value<knowCore::Timestamp>());
      return cres_success(knowCore::Value::fromValue(dt.toEpoch<knowCore::NanoSeconds>().count()));
    }
    cres_qresult<knowCore::Value> unserialise(const knowCore::Value& _value) const override
    {
      cres_try(knowCore::BigNumber bn, _value.value<knowCore::BigNumber>());
      knowCore::Timestamp dt = knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(bn);
      return cres_success(knowCore::Value::fromValue(dt));
    }
  };
} // namespace

RDFTermBinaryMarshal::Private::Serialisers::Serialisers()
{
  RDFTermBinaryMarshal::registerSerialiser(knowCore::TimestampMetaTypeInformation::uri(),
                                           new DateTimeSerialiser());
}

bool RDFTermBinaryMarshal::registerSerialiser(const knowCore::Uri& _valueUri,
                                              AbstractSerialiser* _serialiser)
{
  Private::s_serialisers.serialisers[_valueUri] = _serialiser;
  return true;
}

RDFTermBinaryMarshal::AbstractSerialiser::~AbstractSerialiser() {}

RDFTermBinaryMarshal::RDFTermBinaryMarshal(RDFValueBinaryMarshal* _marshal,
                                           const Connection& _connection)
    : AbstractBinaryMarshal("kdb_rdf_term", datatype<knowRDF::Literal>(),
                            Mode::ToVariant | Mode::ToByteArray),
      d(new Private())
{
  d->literal_marhal = _marshal;
  d->connection = _connection;
  d->rdf_value_oid = Connection(d->connection).d->nameToOid("kdb_rdf_value").expect_success();
}

RDFTermBinaryMarshal::~RDFTermBinaryMarshal() { delete d; }

cres_qresult<knowCore::Value>
  RDFTermBinaryMarshal::toValue(const QByteArray& _source,
                                const kDB::Repository::Connection& _connection) const
{
  QString uri;
  QString lang;
  knowCore::Value literal_var;

  const char* data = _source.begin();
  const quint32 fields = BinaryInterface::read<quint32>(data, 4);
  data += 4;
  clog_assert(fields == 3);
  const quint32 uri_oid = BinaryInterface::read<quint32>(data, 4);
  data += 4;
  clog_assert(uri_oid == _connection.d->nameToOid("kdb_uri"));
  const quint32 uri_length = BinaryInterface::read<quint32>(data, 4);
  data += 4;

  // Read uri
  if(uri_length != 0xFFFFFFFF)
  {
    uri = BinaryInterface::read<QString>(data, uri_length);
    data += uri_length;
  }

  // Check if the uri is for a blank node
  if(knowRDF::isBlankNodeSkolemisation(uri))
  {
    return cres_success(knowCore::Value::fromValue(Connection(d->connection).d->blankNode(uri)));
  }

  // Read the OID which should match a literal
  const quint32 literal_oid = BinaryInterface::read<quint32>(data, 4);
  data += 4;
  clog_assert(literal_oid == d->rdf_value_oid);
  const quint32 literal_length = BinaryInterface::read<quint32>(data, 4);
  data += 4;

  // Non-empty literal
  if(literal_length != 0xFFFFFFFF)
  {
    QByteArray literal_data(data, literal_length);
    data += literal_length;
    cres_try(literal_var, d->literal_marhal->toValue(literal_data, _connection));
    if(Private::s_serialisers.serialisers.contains(uri))
    {
      cres_try(literal_var, Private::s_serialisers.serialisers[uri]->unserialise(literal_var));
    }
  }
  else
  {
    return cres_success(knowCore::Value::fromValue(knowCore::Uri(uri)));
  }

  // Invalid literal
  if(literal_var.isEmpty())
  {
    return cres_success(knowCore::Value::fromValue(knowCore::Uri(uri)));
  }

  // Read the lang field
  const quint32 lang_oid = BinaryInterface::read<quint32>(data, 4);
  data += 4;
  clog_assert(lang_oid == TEXTOID);
  const quint32 lang_length = BinaryInterface::read<quint32>(data, 4);
  data += 4;
  if(lang_length != 0xFFFFFFFF)
  {
    lang = BinaryInterface::read<QString>(data, lang_length);
    data += lang_length;
  }

  // Create the literal from its variant form
  clog_assert(data == _source.end());

  auto const& [success, literal, errorMessage]
    = knowRDF::Literal::fromValue(uri, literal_var, lang);
  if(success)
  {
    return cres_success(knowCore::Value::fromValue(literal.value()));
  }
  else if(knowCore::ValueIs<QString> literal_str = literal_var)
  {
    cres_try(knowRDF::Literal literal, knowRDF::Literal::fromRdfLiteral(uri, literal_str, lang));
    return cres_success(knowCore::Value::fromValue(literal));
  }
  else
  {
    return cres_forward_failure(errorMessage.value());
  }
}

cres_qresult<QByteArray>
  RDFTermBinaryMarshal::toByteArray(const knowCore::Value& _source, QString& _oidName,
                                    const kDB::Repository::Connection& _connection) const
{
  _oidName = "kdb_rdf_term";

  knowRDF::Object obj;

  cres_try(Oid KDBURIOID, _connection.d->nameToOid("kdb_uri"));

  if(knowCore::ValueIs<knowRDF::Literal> obj_lit = _source)
  {
    obj = knowRDF::Literal(obj_lit);
  }
  else if(knowCore::ValueCast<knowRDF::Object> obj_obj = _source)
  {
    obj = obj_obj;
  }

  const std::size_t field_header_size = sizeof(quint32) + sizeof(Oid);
  const std::size_t headers_size = sizeof(quint32) + 3 * field_header_size;

  QByteArray destination;

  switch(obj.type())
  {
  case knowRDF::Object::Type::BlankNode:
  {
    QByteArray uri_bn = knowRDF::blankNodeSkolemisation(obj.blankNode()).toUtf8();
    const std::size_t full_size = headers_size + uri_bn.size();
    destination.resize(full_size);
    char* data = destination.begin();
    // Write fields
    BinaryInterface::write<quint32>(3, data);
    data += sizeof(quint32);
    BinaryInterface::write<Oid>(KDBURIOID, data);
    data += sizeof(Oid); // enum oid
    BinaryInterface::write<quint32>(uri_bn.size(), data);
    data += sizeof(quint32);
    BinaryInterface::write<QByteArray>(uri_bn, data);
    data += uri_bn.size();
    // value
    BinaryInterface::write<Oid>(d->rdf_value_oid, data);
    data += sizeof(Oid);
    BinaryInterface::write<quint32>(0xFFFFFFFF, data);
    data += sizeof(quint32);
    // lang
    BinaryInterface::write<Oid>(TEXTOID, data);
    data += sizeof(Oid);
    BinaryInterface::write<quint32>(0xFFFFFFFF, data);
    data += sizeof(quint32);
    clog_assert(data == destination.end());
    break;
  }
  case knowRDF::Object::Type::Uri:
  {
    QByteArray uri_bn = ((QString)obj.uri()).toUtf8();
    const std::size_t full_size = headers_size + uri_bn.size();
    destination.resize(full_size);
    char* data = destination.begin();
    BinaryInterface::write<quint32>(3, data);
    data += sizeof(quint32);
    BinaryInterface::write<Oid>(KDBURIOID, data);
    data += sizeof(Oid); // enum oid
    // uri
    BinaryInterface::write<quint32>(uri_bn.size(), data);
    data += sizeof(quint32);
    BinaryInterface::write<QByteArray>(uri_bn, data);
    data += uri_bn.size();
    // value
    BinaryInterface::write<Oid>(d->rdf_value_oid, data);
    data += sizeof(Oid);
    BinaryInterface::write<quint32>(0xFFFFFFFF, data);
    data += sizeof(quint32);
    // lang
    BinaryInterface::write<Oid>(TEXTOID, data);
    data += sizeof(Oid);
    BinaryInterface::write<quint32>(0xFFFFFFFF, data);
    data += sizeof(quint32);
    clog_assert(data == destination.end());
    break;
  }
  case knowRDF::Object::Type::Variable:
  case knowRDF::Object::Type::Undefined:
  {
    const std::size_t full_size = headers_size;
    destination.resize(full_size);
    char* data = destination.begin();
    BinaryInterface::write<quint32>(3, data);
    data += sizeof(quint32);
    // uri
    BinaryInterface::write<Oid>(KDBURIOID, data);
    data += sizeof(Oid); // enum oid
    BinaryInterface::write<quint32>(0xFFFFFFFF, data);
    data += sizeof(quint32);
    // value
    BinaryInterface::write<Oid>(d->rdf_value_oid, data);
    data += sizeof(Oid);
    BinaryInterface::write<quint32>(0xFFFFFFFF, data);
    data += sizeof(quint32);
    // lang
    BinaryInterface::write<Oid>(TEXTOID, data);
    data += sizeof(Oid);
    BinaryInterface::write<quint32>(0xFFFFFFFF, data);
    data += sizeof(quint32);
    clog_assert(data == destination.end());
    break;
  }
  case knowRDF::Object::Type::Literal:
  {
    QByteArray uri_bn = ((QString)obj.literal().datatype()).toUtf8();
    QByteArray lang = obj.literal().lang().toUtf8();

    QString oidName;
    knowCore::Value literal_value = obj.literal();
    if(Private::s_serialisers.serialisers.contains(literal_value.datatype()))
    {
      cres_try(
        literal_value,
        Private::s_serialisers.serialisers[literal_value.datatype()]->serialise(literal_value));
    }
    cres_try(QByteArray literal_array,
             d->literal_marhal->toByteArray(literal_value, oidName, _connection));

    const std::size_t full_size = headers_size + literal_array.size() + lang.size() + uri_bn.size();
    destination.resize(full_size);
    char* data = destination.begin();
    BinaryInterface::write<quint32>(3, data);
    data += sizeof(quint32);
    BinaryInterface::write<Oid>(KDBURIOID, data);
    data += sizeof(Oid); // enum oid
    BinaryInterface::write<quint32>(uri_bn.size(), data);
    data += sizeof(quint32);
    BinaryInterface::write<QByteArray>(uri_bn, data);
    data += uri_bn.size();
    BinaryInterface::write<Oid>(d->rdf_value_oid, data);
    data += sizeof(Oid);
    BinaryInterface::write<quint32>(literal_array.size(), data);
    data += sizeof(quint32);
    BinaryInterface::write<QByteArray>(literal_array, data);
    data += literal_array.size();
    // lang
    BinaryInterface::write<Oid>(TEXTOID, data);
    data += sizeof(Oid);
    BinaryInterface::write<quint32>(lang.size(), data);
    data += sizeof(quint32);
    BinaryInterface::write<QByteArray>(lang, data);
    data += lang.size();
    clog_assert(data == destination.end());
    break;
  }
  }
  return cres_success(destination);
}
