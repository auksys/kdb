#include "SQLInterface_p.h"

#include <clog_qt>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QUuid>
#include <QVariant>
#include <knowCore/Timestamp.h>

#include <Cyqlops/Crypto/RSAAlgorithm.h>

#include <knowCore/Messages.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Unit.h>
#include <knowCore/Uri.h>
#include <knowCore/ValueHash.h>
#include <knowCore/ValueList.h>

#include <knowCore/Uris/xsd.h>
#include <knowRDF/BlankNode.h>
#include <knowRDF/Object.h>
#include <knowRDF/Skolemisation.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/RDFView/ViewDefinition.h>

#include "Connection_p.h"
#include "Logging.h"
#include "PersistentDatasetsUnion.h"
#include "QueryConnectionInfo.h"
#include "RDFTermBinaryMarshal.h"
#include "SQLCopyData.h"
#include "TemporaryTransaction.h"
#include "TripleStore_p.h"
#include "TriplesView.h"
#include "Utils.h"

#include "VersionControl/Delta_p.h"
#include "VersionControl/Manager.h"
#include "VersionControl/Revision_p.h"
#include "VersionControl/Transaction_p_p.h"

using namespace kDB::Repository::DatabaseInterface::PostgreSQL;

cres_qresult<void> SQLInterface::lock(const Transaction& _transaction, const QString& _tablename,
                                      LockMode _lockMode)
{
  clog_assert(_transaction.isActive());
  QString modestring;

  switch(_lockMode)
  {
  case LockMode::AccessShare:
    modestring = "ACCESS SHARE";
    break;
  case LockMode::RowShare:
    modestring = "ROW SHARE";
    break;
  case LockMode::RowExclusive:
    modestring = "ROW EXCLUSIVE";
    break;
  case LockMode::ShareUpdateExclusive:
    modestring = "SHARE UPDATE EXCLUSIVE";
    break;
  case LockMode::Share:
    modestring = "SHARE";
    break;
  case LockMode::ShareRowExclusive:
    modestring = "SHARE ROW EXCLUSIVE";
    break;
  case LockMode::Exclusive:
    modestring = "EXCLUSIVE";
    break;
  case LockMode::AccessExclusive:
    modestring = "ACCESS EXCLUSIVE";
    break;
  }

  QString query = clog_qt::qformat("LOCK TABLE {} IN {} MODE", _tablename, modestring);

  knowDBC::Query q = _transaction.createSQLQuery(query);
  KDB_REPOSITORY_EXECUTE_QUERY(
    clog_qt::qformat("Failed to lock table {} in mode {}", _tablename, modestring), q, false);
  return cres_success();
}

cres_qresult<void> SQLInterface::lockTripleStore(const Transaction& _transaction,
                                                 const QString& _tablename, bool _has_versioning)
{
  clog_assert(_transaction.isActive());
  cres_try(cres_ignore, lock(_transaction, _tablename, LockMode::Exclusive));
  if(_has_versioning)
  {
    cres_try(cres_ignore,
             lock(_transaction, _tablename + "_versioning_revisions", LockMode::Exclusive));
    cres_try(cres_ignore,
             lock(_transaction, _tablename + "_versioning_deltas", LockMode::Exclusive));
  }
  return cres_success();
}

QString SQLInterface::subjectString(const knowRDF::Subject& _subject)
{
  switch(_subject.type())
  {
  case knowRDF::Subject::Type::BlankNode:
    return knowRDF::blankNodeSkolemisation(_subject.blankNode());
  case knowRDF::Subject::Type::Uri:
    return _subject.uri();
  case knowRDF::Subject::Type::Undefined:
    return QString();
  case knowRDF::Subject::Type::Variable:
    clog_fatal("Variable cannot be used in SQL queries");
  }
  return QString();
}

cres_qresult<void> SQLInterface::dropView(const QueryConnectionInfo& _connection_info,
                                          const QString& _name)
{
  knowDBC::Query query = _connection_info.createSQLQuery("DROP VIEW IF EXISTS " + _name);
  KDB_REPOSITORY_EXECUTE_QUERY(clog_qt::qformat("Failed to drop view {}", _name), query, false);
  return cres_success();
}

cres_qresult<void> SQLInterface::updateUnionView(const QueryConnectionInfo& _connection_info,
                                                 bool _temporary, const QString& _name,
                                                 const QList<RDFDataset>& _documents)
{
  QString q_txt;

  if(_temporary)
  {
    q_txt = "CREATE OR REPLACE TEMPORARY VIEW ";

    if(_documents.isEmpty())
    {
      return cres_failure(
        "Updating a temporary view to an empty list of document is not supported.");
    }
  }
  else
  {
    q_txt = "CREATE OR REPLACE VIEW ";
    if(_documents.isEmpty())
    {
      return dropView(_connection_info, _name);
    }
  }
  q_txt += _name + " AS ";

  // Loop through the datasets
  bool first = true;
  for(const RDFDataset& dataset : _documents)
  {
    bool was_first = first;
    if(not first)
      q_txt += " UNION ";
    else
      first = false;
    QString tn;
    switch(dataset.type())
    {
    case RDFDataset::Type::Invalid:
    {
      return cres_log_failure("Union with an invalid RDF Dataset");
    }
    case RDFDataset::Type::Empty:
    {
      if(was_first)
        first = true;
      break;
    }
    case RDFDataset::Type::PersistentDatasetsUnion:
    {
      tn = dataset.toPersistentDatasetsUnion().tablename();
      break;
    }
    case RDFDataset::Type::DatasetsUnion:
    {
      return cres_log_failure("Internal error DatasetsUnion cannot be part of a DatasetsUnion");
    }
    case RDFDataset::Type::TripleStore:
      tn = dataset.toTripleStore().tablename();
      break;
    case RDFDataset::Type::TriplesView:
      tn = dataset.toTriplesView().tablename();
      break;
    }
    q_txt += "(SELECT subject, predicate, object FROM " + tn + ")";
  }
  knowDBC::Query sql_query = _connection_info.createSQLQuery(q_txt);
  KDB_REPOSITORY_EXECUTE_QUERY("Failed to create DatasetsUnion", sql_query, false);
  return cres_success();
}

QString SQLInterface::persistentDatasetsUnionTableName(const Connection& _connection,
                                                       const QString& _name)
{
  return _connection.d->uniqueTableName("persistent_rdf_doc_union", _name);
}

cres_qresult<void> SQLInterface::addToUnion(const QueryConnectionInfo& _connection_info,
                                            const knowCore::Uri& _union_name,
                                            const knowCore::Uri& _dataset_name)
{
  knowDBC::Query query = _connection_info.createSQLQuery();
  query.setQuery("INSERT INTO unions (union_name, graph_name) VALUES (:union_name, :graph_name)");
  query.bindValues(":union_name", _union_name, ":graph_name", _dataset_name);
  KDB_REPOSITORY_EXECUTE_QUERY(
    clog_qt::qformat("Failed to add <{}> to union <{}>", _union_name, _dataset_name), query, false);
  return cres_success();
}

cres_qresult<void> SQLInterface::clearUnion(const QueryConnectionInfo& _connection_info,
                                            const knowCore::Uri& _union_name)
{
  knowDBC::Query query = _connection_info.createSQLQuery();
  query.setQuery("DELETE FROM unions WHERE union_name = :union_name AND graph_name = :graph_name)");
  query.bindValue(":union_name", _union_name);
  KDB_REPOSITORY_EXECUTE_QUERY(clog_qt::qformat("Failed to remove union <{}>", _union_name), query,
                               false);
  return cres_success();
}

cres_qresult<void> SQLInterface::removeFromUnion(const QueryConnectionInfo& _connection_info,
                                                 const knowCore::Uri& _union_name,
                                                 const knowCore::Uri& _dataset_name)
{
  knowDBC::Query query = _connection_info.createSQLQuery();
  query.setQuery("DELETE FROM unions WHERE union_name = :union_name AND graph_name = :graph_name)");
  query.bindValues(":union_name", _union_name, "graph_name", _dataset_name);
  KDB_REPOSITORY_EXECUTE_QUERY(
    clog_qt::qformat("Failed to add <{}> to union <{}>", _union_name, _dataset_name), query, false);
  return cres_success();
}

QHash<QString, QStringList> SQLInterface::getUnions(const QueryConnectionInfo& _connection_info)
{
  QHash<QString, QStringList> r;
  knowDBC::Query query = _connection_info.createSQLQuery();
  query.setQuery("SELECT union_name, graph_name FROM unions");
  knowDBC::Result res = query.execute();
  if(res)
  {
    for(int i = 0; i < res.tuples(); ++i)
    {
      r[res.value(i, 0).value<QString>().expect_success()].append(
        res.value<QString>(i, 1).expect_success());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("Failed to getUnions: ", res);
  }
  return r;
}

cres_qresult<void> SQLInterface::setDefinition(const QueryConnectionInfo& _connection_info,
                                               const kDB::RDFView::ViewDefinition& _definition)
{
  knowDBC::Query query = _connection_info.createSQLQuery();
  query.setQuery(
    "WITH upsert AS ( UPDATE views SET definition=:def_value WHERE name=:def_name RETURNING * ) "
    "INSERT INTO views (name, definition) SELECT :def_name, :def_value WHERE NOT EXISTS (SELECT * "
    "FROM upsert)");
  query.bindValue(":def_name", knowCore::Value::fromValue(_definition.name()));
  query.bindValue(":def_value", _definition.toString());
  KDB_REPOSITORY_EXECUTE_QUERY("failed to setDefinition", query);
  return cres_success();
}

cres_qresult<void> SQLInterface::removeViewDefinition(const QueryConnectionInfo& _connection_info,
                                                      const knowCore::Uri& _name)
{
  knowDBC::Query query = _connection_info.createSQLQuery();
  query.setQuery("DELETE FROM views WHERE name = :name");
  query.bindValue(":name", (QString)_name);
  KDB_REPOSITORY_EXECUTE_QUERY("failed to remove view definition", query);
  return cres_success();
}

QList<kDB::RDFView::ViewDefinition>
  SQLInterface::getDefinitions(const QueryConnectionInfo& _connection_info)
{
  QList<kDB::RDFView::ViewDefinition> views;
  knowDBC::Query query = _connection_info.createSQLQuery();
  query.setQuery("SELECT name, definition FROM views");
  knowDBC::Result r = query.execute();
  if(r)
  {
    for(int i = 0; i < r.tuples(); ++i)
    {
      knowCore::Messages msgs;
      kDB::RDFView::ViewDefinition def = kDB::RDFView::ViewDefinition::parse(
        r.value(i, 1).value<QString>().expect_success(), &msgs);
      if(def.isValid())
      {
        views.append(def);
      }
      else
      {
        clog_error("invalid view definition {}: {}", r.value(i, 0), msgs.toString());
      }
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("Failed to getDefinitions: ", r);
  }
  return views;
}

cres_qresult<QString> SQLInterface::createTripleStore(const Transaction& _transaction,
                                                      const knowCore::Uri& _name)
{
  knowDBC::Query query = _transaction.createSQLQuery();
  query.setQuery(
    "INSERT INTO triples_stores(name, options, meta) VALUES (:name, :options, '{}') RETURNING id");
  query.bindValue(":name", knowCore::Value::fromValue(_name));
  query.bindValue(":options", (int)TripleStore::Option::None);
  knowDBC::Result result = query.execute();
  if(result and result.tuples() == 1)
  {
    int id = result.value(0, 0).value<int>().expect_success();
    ;
    QString table_name = "triples_store_" + QString::number(id);
    query.setQuery("UPDATE triples_stores SET tablename=:tablename WHERE id=:id");
    query.bindValue(":id", id);
    query.bindValue(":tablename", table_name);
    KDB_REPOSITORY_EXECUTE_QUERY("Failed to update triples_stores table with tablename", query,
                                 QString());
    query.setQuery("CREATE TABLE " + table_name
                   + "\n"
                     "(\n"
                     "  id bigserial PRIMARY KEY,\n"
                     "  subject kdb_uri NOT NULL,\n"
                     "  predicate kdb_uri NOT NULL,\n"
                     "  object kdb_rdf_term NOT NULL,\n"
                     "  md5 bytea NOT NULL,\n"
                     "  UNIQUE (subject, predicate, md5)\n"
                     ");\n"

                     "CREATE INDEX "
                   + table_name
                   + "_subject_index\n"
                     "  ON "
                   + table_name
                   + " USING btree (subject);\n"

                     "CREATE INDEX "
                   + table_name
                   + "_predicate_index\n"
                     "  ON "
                   + table_name
                   + " USING btree (predicate);\n"

                     "CREATE INDEX "
                   + table_name
                   + "_md5_index\n"
                     "  ON "
                   + table_name + " USING btree (md5);\n");
    query.setOption(knowDBC::Query::OptionsKeys::MultiQueries, true);
    KDB_REPOSITORY_EXECUTE_QUERY("failed to create triples store", query, QString());
    return cres_success(table_name);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to register triples store", result);
  }
}

cres_qresult<void> SQLInterface::removeTripleStore(const Transaction& _transaction,
                                                   const knowCore::Uri& _name)
{
  knowDBC::Query query = _transaction.createSQLQuery();
  query.setQuery("SELECT id, tablename FROM triples_stores WHERE name=:name");
  query.bindValue(":name", (QString)_name);
  knowDBC::Result result = query.execute();
  if(result and result.tuples() == 1)
  {
    int id = result.value(0, 0).value<int>().expect_success();
    QString tablename = result.value(0, 1).value<QString>().expect_success();
    query.setQuery("DROP TABLE " + tablename + " CASCADE");
    KDB_REPOSITORY_EXECUTE_QUERY("failed to drop table for triples store", query);
    query.setQuery("DELETE FROM triples_stores WHERE id=:id");
    query.bindValue(":id", id);
    KDB_REPOSITORY_EXECUTE_QUERY("failed to delete triples store info from triples_stores", query);
    return cres_success();
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("failed to find triples store with name ({})", result,
                                             _name);
  }
}

cres_qresult<void> SQLInterface::createIndex(const Transaction& _transaction,
                                             const TripleStore& _store, const QString& _field,
                                             const QString& _index_method)
{
  knowDBC::Query query = _transaction.createSQLQuery();
  query.setQuery("CREATE INDEX " + _store.tablename() + "_object_" + _field + " ON "
                 + _store.tablename() + " USING " + _index_method + " ((((object).value)." + _field
                 + "));");
  KDB_REPOSITORY_EXECUTE_QUERY(
    clog_qt::qformat("Failed to create index on table {} for field {}", _store.tablename(), _field),
    query, false);
  return cres_success();
}

cres_qresult<void> SQLInterface::enableNotifications(const QueryConnectionInfo& _connection_info,
                                                     const TripleStore& _store)
{
  QString trigger_name = notificationsChannel(_store);
  QString query_text
    = clog_qt::qformat("CREATE TRIGGER {} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON {} FOR "
                       "EACH STATEMENT EXECUTE PROCEDURE kdb_notify_change(\"{}\");",
                       trigger_name, _store.tablename(), trigger_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  KDB_REPOSITORY_EXECUTE_QUERY("when enabling notifications", q);

  q.setQuery("UPDATE triples_stores SET options = (options | :flag) WHERE name = :name");
  q.bindValue(":flag", (int)TripleStore::Option::Notifications);
  q.bindValue(":name", knowCore::Value::fromValue(_store.uri()));
  KDB_REPOSITORY_EXECUTE_QUERY("when enabling notifications: ", q);

  return cres_success();
}

cres_qresult<void> SQLInterface::disableNotifications(const QueryConnectionInfo& _connection_info,
                                                      const TripleStore& _store)
{
  QString trigger_name = notificationsChannel(_store);
  QString query_text = clog_qt::qformat("DROP TRIGGER {} ON {};", trigger_name, _store.tablename());

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  KDB_REPOSITORY_EXECUTE_QUERY("when enabling notifications", q);

  q.setQuery("UPDATE triples_stores SET options = (options & ~ :flag) WHERE name = :name");
  q.bindValue(":flag", (int)TripleStore::Option::Notifications);
  q.bindValue(":name", knowCore::Value::fromValue(_store.uri()));
  KDB_REPOSITORY_EXECUTE_QUERY("when enabling notifications: ", q);

  return cres_success();
}

QString SQLInterface::notificationsChannel(const TripleStore& _store)
{
  return clog_qt::qformat("{}_notify_update", _store.tablename());
}

cres_qresult<void> SQLInterface::enableVersioning(const Transaction& _transaction,
                                                  const TripleStore& _store)
{
  QString query_text = R"V0G0N(
        CREATE TABLE $$table$$_versioning_revisions (revision serial PRIMARY KEY, hash bytea NOT NULL, UNIQUE(hash), content_hash bytea NOT NULL, historicity int NOT NULL,
                                                              tags int, distance_to_root int NOT NULL);
        CREATE TABLE $$table$$_versioning_deltas    (parent int, child int, hash bytea NOT NULL, delta bytea NOT NULL,
                                                      PRIMARY KEY(parent, child), FOREIGN KEY (parent) REFERENCES $$table$$_versioning_revisions (revision),
                                                      FOREIGN KEY (child) REFERENCES $$table$$_versioning_revisions (revision));
        CREATE TABLE $$table$$_versioning_deltas_signatures (parent int, child int, author bytea NOT NULL,
                                                      timestamp bigint, signature bytea NOT NULL,
                                                      PRIMARY KEY(parent, child, author), FOREIGN KEY (parent) REFERENCES $$table$$_versioning_revisions (revision),
                                                      FOREIGN KEY (child) REFERENCES $$table$$_versioning_revisions (revision));)V0G0N";

  query_text = query_text.replace("$$table$$", _store.tablename());

  knowDBC::Query q = _transaction.createSQLQuery(query_text);
  q.setOption(knowDBC::Query::OptionsKeys::MultiQueries, true);
  KDB_REPOSITORY_EXECUTE_QUERY("when enabling versioning", q);

  q.setQuery("UPDATE triples_stores SET options = (options | :flag) WHERE name = :name");
  q.unsetOption(knowDBC::Query::OptionsKeys::MultiQueries);
  q.bindValue(":flag", (int)TripleStore::Option::Versioning);
  q.bindValue(":name", knowCore::Value::fromValue(_store.uri()));
  KDB_REPOSITORY_EXECUTE_QUERY("when enabling versioning: ", q);
  QByteArray hash = VersionControl::Revision::initialHash();
  cres_try(int initial_rev, SQLInterface::createRevision(
                              _transaction, _store.tablename(), hash, hash, 0,
                              _store.versionControlManager()
                                ? (int)_store.versionControlManager()->defaultRevisionTags()
                                : 0,
                              0));

  knowCore::ValueHash obj_versioning;
  knowCore::ValueHash obj_versioning_head;
  obj_versioning_head.insert("id", initial_rev);
  obj_versioning_head.insert("hash", QString::fromLatin1(hash.toHex()));
  obj_versioning.insert("head", obj_versioning_head);
  cres_try(cres_ignore, _store.D()->definition->setMeta(QStringList() << "versioning",
                                                        knowCore::Value::fromValue(obj_versioning),
                                                        _transaction));

  return cres_success();
}

cres_qresult<void> SQLInterface::disableVersioning(const QueryConnectionInfo& _connection_info,
                                                   const TripleStore& _store)
{
  QString query_text
    = "DROP TABLE $$table$$_versioning_deltas; DROP TABLE $$table$$_versioning_deltas_signatures; "
      "DROP TABLE $$table$$_versioning_revisions";
  query_text = query_text.replace("$$table$$", _store.tablename());

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.setOption(knowDBC::Query::OptionsKeys::MultiQueries, true);
  KDB_REPOSITORY_EXECUTE_QUERY("when disabling versioning", q, false);
  q.setQuery("UPDATE triples_stores SET options = options & ~:flag WHERE name = :name");
  q.bindValue(":flag", (int)TripleStore::Option::Versioning);
  q.bindValue(":name", knowCore::Value::fromValue(_store.uri()));
  KDB_REPOSITORY_EXECUTE_QUERY("when disabling versioning", q, false);
  SQLInterface::dropMeta(_connection_info, _store.uri(), QStringList() << "versioning");
  return cres_success();
}

cres_qresult<void> SQLInterface::startListeningChanges(const Transaction& _transaction,
                                                       const TripleStore& _store)
{
  QString table_name = _store.tablename() + "_changes";
  knowDBC::Query q = _transaction.createSQLQuery("CREATE TEMPORARY TABLE " + table_name
                                                 + "\n"
                                                   "(\n"
                                                   "  type text NOT NULL,\n"
                                                   "  subject text NOT NULL,\n"
                                                   "  predicate text NOT NULL,\n"
                                                   "  object kdb_rdf_term NOT NULL\n"
                                                   ") ON COMMIT DROP;\n");
  KDB_REPOSITORY_EXECUTE_QUERY("failed to create temporary table for recording changes", q);

  q.setQuery("CREATE TRIGGER " + _store.tablename()
             + "_trigger AFTER INSERT OR UPDATE OR DELETE ON " + _store.tablename()
             + " FOR EACH ROW EXECUTE PROCEDURE kdb_record_triple_change(\"" + table_name + "\");");
  KDB_REPOSITORY_EXECUTE_QUERY("failed to create trigger for recording changes", q);
  return cres_success();
}

cres_qresult<void> SQLInterface::stopListeningChanges(const Transaction& _transaction,
                                                      const TripleStore& _store)
{
  QString table_name = _store.tablename() + "_changes";
  knowDBC::Query q = _transaction.createSQLQuery("DROP TRIGGER " + _store.tablename()
                                                 + "_trigger ON " + _store.tablename());
  KDB_REPOSITORY_EXECUTE_QUERY("failed to drop trigger after recording changes", q);
  q.setQuery("DROP TABLE " + table_name);
  KDB_REPOSITORY_EXECUTE_QUERY("failed to drop temporary table for recording changes", q);
  return cres_success();
}

QList<std::tuple<QString, knowRDF::Triple>>
  SQLInterface::retrieveChanges(const Transaction& _transaction, const TripleStore& _store)
{
  clog_assert(_transaction.isActive());
  knowDBC::Query q
    = _transaction.createSQLQuery("SELECT * FROM " + _store.tablename() + "_changes");

  knowDBC::Result r = q.execute();
  if(r)
  {
    QList<std::tuple<QString, knowRDF::Triple>> results;

    for(int i = 0; i < r.tuples(); ++i)
    {
      results.append(std::make_tuple(r.value(i, 0).value<QString>().expect_success(),
                                     getTriple(_transaction.d->connection, r, i, 1, 2, 3, true)));
    }

    return results;
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("When retrieving list of changes", r);
    return QList<std::tuple<QString, knowRDF::Triple>>();
  }
}

QByteArray SQLInterface::computeContentHash(const QueryConnectionInfo& _transaction,
                                            const QString& _triples_store_table_name)
{
  knowDBC::Query q = _transaction.createSQLQuery("SELECT kdb_compute_md5_table_column('"
                                                 + _triples_store_table_name + "', 'md5', 'md5')");
  knowDBC::Result r = q.execute();
  if(r)
  {
    QByteArray hash = r.value<QByteArray>(0, 0).expect_success();
    if(hash.isEmpty())
    {
      return QByteArray::fromHex("d41d8cd98f00b204e9800998ecf8427e");
    }
    else
    {
      return hash;
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("when computing content hash", r);
    return QByteArray();
  }
}

cres_qresult<int> SQLInterface::createRevision(const Transaction& _transaction,
                                               const QString& _triples_store_table_name,
                                               const QByteArray& _hash,
                                               const QByteArray& _content_hash, int _historicity,
                                               int _tags, int _distance_to_root)
{
  QString query_text = "INSERT INTO $$table$$_versioning_revisions(hash, content_hash, "
                       "historicity, tags, distance_to_root) VALUES (:hash, :content_hash, "
                       ":historicity, :tags, :distance) RETURNING revision";
  query_text = query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _transaction.createSQLQuery(query_text);
  q.bindValue(":hash", _hash);
  q.bindValue(":content_hash", _content_hash);
  q.bindValue(":historicity", _historicity);
  q.bindValue(":tags", _tags);
  q.bindValue(":distance", _distance_to_root);
  knowDBC::Result r = q.execute();
  if(r)
  {
    int revisionId = r.value<int>(0, 0).expect_success();
    return cres_success(revisionId);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("when creating revision", r);
  }
}

cres_qresult<void> SQLInterface::removeRevision(const Transaction& _transaction,
                                                const VersionControl::Revision& _revision)
{
  // First remove the deltas associated with the revision
  {
    QString query_text
      = "DELETE FROM $$table$$_versioning_deltas WHERE parent=:revisionId OR child=:revisionId";
    query_text = query_text.replace("$$table$$", _revision.d->store_table_name);

    knowDBC::Query q = _transaction.createSQLQuery(query_text);
    q.bindValue(":revisionId", _revision.d->revisionId);

    KDB_REPOSITORY_EXECUTE_QUERY("when removing delta for a revision", q);
  }
  // Second remove the signatyres associated with the revision
  {
    QString query_text = "DELETE FROM $$table$$_versioning_deltas_signatures WHERE "
                         "parent=:revisionId OR child=:revisionId";
    query_text = query_text.replace("$$table$$", _revision.d->store_table_name);

    knowDBC::Query q = _transaction.createSQLQuery(query_text);
    q.bindValue(":revisionId", _revision.d->revisionId);

    KDB_REPOSITORY_EXECUTE_QUERY("when removing delta for a revision", q);
  }
  // Then remove the actual revision
  {
    QString query_text = "DELETE FROM $$table$$_versioning_revisions WHERE hash=:hash";
    query_text = query_text.replace("$$table$$", _revision.d->store_table_name);

    knowDBC::Query q = _transaction.createSQLQuery(query_text);
    q.bindValue(":hash", _revision.hash());

    KDB_REPOSITORY_EXECUTE_QUERY("when removing revision", q);
  }
  return cres_success();
}

namespace
{
  QByteArray compress(const QByteArray& _array)
  {
    if(_array.length() > 1000)
    {
      QByteArray c = qCompress(_array);
      c.prepend("[ZZ]");
      return c;
    }
    else
    {
      return _array;
    }
  }
  QByteArray uncompress(const QByteArray& _array)
  {
    if(_array.startsWith("[ZZ]"))
    {
      QByteArray c = _array;
      c.remove(0, 4);
      return qUncompress(c);
    }
    else
    {
      return _array;
    }
  }
} // namespace

cres_qresult<void> SQLInterface::recordDelta(const Transaction& _transaction,
                                             const QString& _triples_store_table_name, int _parent,
                                             int _child, const QByteArray& _hash,
                                             const QByteArray& _delta)
{
  QString query_text = "INSERT INTO $$table$$_versioning_deltas (parent, child, hash, delta) "
                       "VALUES (:parent, :child, :hash, :delta)";
  query_text = query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _transaction.createSQLQuery(query_text);
  q.bindValue(":parent", _parent);
  q.bindValue(":child", _child);
  q.bindValue(":hash", _hash);
  q.bindValue(":delta", compress(_delta));
  KDB_REPOSITORY_EXECUTE_QUERY("when recording delta", q);

  return cres_success();
}

cres_qresult<void> SQLInterface::recordDeltaSignature(const Transaction& _transaction,
                                                      const QString& _triples_store_table_name,
                                                      int _parent, int _child,
                                                      const QByteArray& _author,
                                                      const knowCore::Timestamp& _timestamp,
                                                      const QByteArray& _signature)
{
  QString query_text
    = "INSERT INTO $$table$$_versioning_deltas_signatures (parent, child, author, timestamp, "
      "signature) VALUES (:parent, :child, :author, :timestamp, :signature) ON CONFLICT DO NOTHING";
  query_text = query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _transaction.createSQLQuery(query_text);
  q.bindValue(":parent", _parent);
  q.bindValue(":child", _child);
  q.bindValue(":author", _author);
  q.bindValue(":timestamp", _timestamp.toEpoch<knowCore::NanoSeconds>().count());
  q.bindValue(":signature", _signature);
  KDB_REPOSITORY_EXECUTE_QUERY("when recording delta signature", q);

  return cres_success();
}

cres_qresult<void> SQLInterface::setRevisionHash(const Transaction& _transaction,
                                                 const QString& _triples_store_table_name,
                                                 int _revision, const QByteArray& _hash,
                                                 const QByteArray& _content_hash)
{
  QString query_text = "UPDATE $$table$$_versioning_revisions SET hash = :hash, content_hash = "
                       ":content_hash WHERE revision = :id";
  query_text = query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _transaction.createSQLQuery(query_text);
  q.bindValue(":hash", _hash);
  q.bindValue(":content_hash", _content_hash);
  q.bindValue(":id", _revision);

  KDB_REPOSITORY_EXECUTE_QUERY("when setting revision hash", q, "fail to set revision hash: {}");
  return cres_success();
}

cres_qresult<void> SQLInterface::setRevisionTags(const Transaction& _transaction,
                                                 const QString& _triples_store_table_name,
                                                 const VersionControl::Revision& _revision,
                                                 int _tags)
{
  QString query_text
    = "UPDATE $$table$$_versioning_revisions SET tags = :tags WHERE revision = :id";
  query_text = query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _transaction.createSQLQuery(query_text);
  q.bindValue(":tags", _tags);
  q.bindValue(":id", _revision.d->revisionId);

  KDB_REPOSITORY_EXECUTE_QUERY("when setting revision tags", q);

  return notifyRevisionTagsChanged(_transaction, _triples_store_table_name, _revision.hash());
}

cres_qresult<void> SQLInterface::notifyNewRevision(const Transaction& _transaction,
                                                   const QString& _triples_store_table_name,
                                                   const QByteArray& _hash)
{
  QString query_text = "SELECT pg_notify(:channel, convert_from(:payload, 'LATIN1'))";

  knowDBC::Query q = _transaction.createSQLQuery(query_text);
  q.bindValue(":channel", _triples_store_table_name + "_new_revision");
  q.bindValue(":payload", _hash.toBase64());

  KDB_REPOSITORY_EXECUTE_QUERY("failed to notify new revision", q);
  return cres_success();
}

cres_qresult<void>
  SQLInterface::notifyRevisionTagsChanged(const kDB::Repository::Transaction& _transaction,
                                          const QString& _triples_store_table_name,
                                          const QByteArray& _hash)
{
  QString query_text = "SELECT pg_notify(:channel, convert_from(:payload, 'LATIN1'))";

  knowDBC::Query q = _transaction.createSQLQuery(query_text);
  q.bindValue(":channel", _triples_store_table_name + "_revision_tags_changed");
  q.bindValue(":payload", _hash.toBase64());

  KDB_REPOSITORY_EXECUTE_QUERY("failed to notify new revision", q);
  return cres_success();
}

cres_qresult<QList<kDB::Repository::VersionControl::Signature>>
  SQLInterface::deltaSignatures(const QueryConnectionInfo& _connection_info,
                                const QString& _triples_store_table_name, int _parent, int _child)
{
  QString query_text = "SELECT author, timestamp, signature FROM "
                       "$$table$$_versioning_deltas_signatures WHERE parent "
                       "= :parent and child = :child";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":parent", _parent);
  q.bindValue(":child", _child);
  knowDBC::Result r = q.execute();
  if(r)
  {
    QList<kDB::Repository::VersionControl::Signature> signatures;
    for(int i = 0; i < r.tuples(); ++i)
    {

      signatures.append(
        {QUuid(r.value<QByteArray>(i, 0).expect_success()),
         knowCore::Timestamp::fromEpoch<knowCore::NanoSeconds>(
           r.value<knowCore::BigNumber>(i, 1).expect_success().toUInt64().expect_success()),
         r.value<QByteArray>(i, 2).expect_success()});
    }
    return cres_success(signatures);
  }
  else
  {
    return cres_failure("Failed to get signatures between {} and {}: {}", _parent, _child,
                        r.error());
  }
}

QList<kDB::Repository::VersionControl::Delta>
  SQLInterface::deltas(const QueryConnectionInfo& _connection_info,
                       const QString& _triples_store_table_name, int _revisionId)
{
  QString query_text
    = "SELECT rev_p.hash AS parent, rev_c.hash AS child, deltas.hash, deltas.delta, deltas.parent "
      "FROM $$table$$_versioning_deltas AS deltas"
      " LEFT JOIN $$table$$_versioning_revisions rev_p ON deltas.parent = rev_p.revision"
      " LEFT JOIN $$table$$_versioning_revisions rev_c ON deltas.child = rev_c.revision"
      " WHERE child = :child";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":child", _revisionId);

  QList<VersionControl::Delta> deltas;
  knowDBC::Result r = q.execute();
  if(r)
  {
    for(int t = 0; t < r.tuples(); ++t)
    {
      VersionControl::Delta delta;
      delta.d->parent = r.value<QByteArray>(t, 0).expect_success();
      delta.d->child = r.value<QByteArray>(t, 1).expect_success();
      delta.d->hash = r.value<QByteArray>(t, 2).expect_success();
      delta.d->delta = uncompress(r.value<QByteArray>(t, 3).expect_success());
      delta.d->signatures = deltaSignatures(_connection_info, _triples_store_table_name,
                                            r.value<int>(t, 4).expect_success(), _revisionId)
                              .expect_success();
      deltas.append(delta);
    }
    return deltas;
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR(clog_qt::qformat("when getting deltas for ", _revisionId), r);
    return deltas;
  }
}

cres_qresult<kDB::Repository::VersionControl::Delta>
  SQLInterface::delta(const QueryConnectionInfo& _connection_info,
                      const QString& _triples_store_table_name, int _parent, int _child)
{
  QString query_text
    = "SELECT rev_p.hash AS parent, rev_c.hash AS child, deltas.hash, deltas.delta FROM "
      "$$table$$_versioning_deltas AS deltas"
      " LEFT JOIN $$table$$_versioning_revisions rev_p ON deltas.parent = rev_p.revision"
      " LEFT JOIN $$table$$_versioning_revisions rev_c ON deltas.child = rev_c.revision"
      " WHERE parent = :parent AND child = :child";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":parent", _parent);
  q.bindValue(":child", _child);
  knowDBC::Result r = q.execute();
  if(r)
  {
    clog_assert(r.tuples() == 1);
    VersionControl::Delta delta;
    delta.d->parent = r.value<QByteArray>(0, 0).expect_success();
    delta.d->child = r.value<QByteArray>(0, 1).expect_success();
    delta.d->hash = r.value<QByteArray>(0, 2).expect_success();
    delta.d->delta = uncompress(r.value<QByteArray>(0, 3).expect_success());
    delta.d->signatures
      = deltaSignatures(_connection_info, _triples_store_table_name, _parent, _child)
          .expect_success();
    return cres_success(delta);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN(
      clog_qt::qformat("when getting deltas between {} and {}", _parent, _child), r);
  }
}

cres_qresult<kDB::Repository::VersionControl::Delta> SQLInterface::delta(
  const QueryConnectionInfo& _connection_info, const QString& _triples_store_table_name,
  const VersionControl::Revision& _parent, const VersionControl::Revision& _child)
{
  return delta(_connection_info, _triples_store_table_name, _parent.d->revisionId,
               _child.d->revisionId);
}

cres_qresult<bool> SQLInterface::hasDelta(const QueryConnectionInfo& _connection_info,
                                          const QString& _triples_store_table_name, int _parent,
                                          int _child)
{
  QString query_text = "SELECT COUNT(*) FROM $$table$$_versioning_deltas AS deltas WHERE parent = "
                       ":parent AND child = :child";

  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":parent", _parent);
  q.bindValue(":child", _child);
  knowDBC::Result r = q.execute();
  if(r)
  {
    clog_assert(r.tuples() == 1);
    return cres_success(r.value<int>(0, 0).expect_success() > 0);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN(
      clog_qt::qformat("when checking for deltas existence between {} and {}", _parent, _child), r);
  }
}

cres_qresult<bool> SQLInterface::hasDelta(const QueryConnectionInfo& _connection_info,
                                          const QString& _triples_store_table_name,
                                          const QByteArray& _parent, const QByteArray& _child)
{
  QString query_text = "SELECT COUNT(*) FROM $$table$$_versioning_deltas AS deltas"
                       " INNER JOIN $$table$$_versioning_revisions rev_p ON deltas.parent = "
                       "rev_p.revision AND rev_p.hash = :parent"
                       " INNER JOIN $$table$$_versioning_revisions rev_c ON deltas.child = "
                       "rev_c.revision AND rev_c.hash = :child";

  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":parent", _parent);
  q.bindValue(":child", _child);
  knowDBC::Result r = q.execute();
  if(r)
  {
    clog_assert(r.tuples() == 1);
    return cres_success(r.value<int>(0, 0).expect_success() > 0);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN(
      clog_qt::qformat("when checking for deltas existence between {} and {}", _parent, _child), r);
  }
}

cres_qresult<QList<kDB::Repository::VersionControl::Revision>>
  SQLInterface::queryToRevisions(const QueryConnectionInfo& _connection_info,
                                 const QString& _triples_store_table_name, knowDBC::Query& _query)
{
  QList<VersionControl::Revision> revs;
  knowDBC::Result r = _query.execute();
  if(r)
  {
    for(int t = 0; t < r.tuples(); ++t)
    {
      VersionControl::Revision rev;
      rev.d->revisionId = r.value<int>(t, 0).expect_success();
      rev.d->hash = r.value<QByteArray>(t, 1).expect_success();
      rev.d->content_hash = r.value<QByteArray>(t, 2).expect_success();
      rev.d->historicity = r.value<int>(t, 3).expect_success();
      rev.d->tags = VersionControl::Revision::Tag(r.value<int>(t, 4).expect_success());
      rev.d->distance_to_root = r.value<int>(t, 5).expect_success();
      rev.d->connection = _connection_info.connection();
      rev.d->store_table_name = _triples_store_table_name;
      revs.append(rev);
    }
    return cres_success(revs);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("getting revisions", r);
  }
}

cres_qresult<QList<kDB::Repository::VersionControl::Revision>>
  SQLInterface::revisions(const QueryConnectionInfo& _connection_info,
                          const QString& _triples_store_table_name)
{
  QString query_text = "SELECT revision, hash, content_hash, historicity, tags, distance_to_root "
                       "FROM $$table$$_versioning_revisions";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);

  return queryToRevisions(_connection_info, _triples_store_table_name, q);
}

cres_qresult<QList<kDB::Repository::VersionControl::Revision>>
  SQLInterface::revisionChildren(const QueryConnectionInfo& _connection_info,
                                 const QString& _triples_store_table_name, int _revisionId)
{
  QString query_text = "SELECT revision, hash, content_hash, historicity, tags, distance_to_root "
                       "FROM $$table$$_versioning_revisions WHERE revision IN (SELECT child FROM "
                       "$$table$$_versioning_deltas WHERE parent = :parent_id)";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":parent_id", _revisionId);

  return queryToRevisions(_connection_info, _triples_store_table_name, q);
}

cres_qresult<QList<kDB::Repository::VersionControl::Revision>>
  SQLInterface::heads(const QueryConnectionInfo& _connection_info,
                      const QString& _triples_store_table_name)
{

  QString query_text = "SELECT revision, hash, content_hash, historicity, tags, distance_to_root "
                       "FROM $$table$$_versioning_revisions WHERE revision in (SELECT child FROM "
                       "$$table$$_versioning_deltas WHERE child NOT IN (SELECT parent FROM "
                       "$$table$$_versioning_deltas))";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);

  return queryToRevisions(_connection_info, _triples_store_table_name, q);
}

cres_qresult<kDB::Repository::VersionControl::Revision>
  SQLInterface::revision(const QueryConnectionInfo& _connection_info,
                         const QString& _triples_store_table_name, const QByteArray& _hash)
{
  QString query_text = "SELECT revision, content_hash, historicity, tags, distance_to_root FROM "
                       "$$table$$_versioning_revisions WHERE hash = :hash";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":hash", _hash);

  knowDBC::Result r = q.execute();
  if(r)
  {
    switch(r.tuples())
    {
    case 1:
    {
      VersionControl::Revision rev;
      rev.d->revisionId = r.value<int>(0, 0).expect_success();
      rev.d->hash = _hash;
      rev.d->content_hash = r.value<QByteArray>(0, 1).expect_success();
      rev.d->historicity = r.value<int>(0, 2).expect_success();
      rev.d->tags = VersionControl::Revision::Tag(r.value<int>(0, 3).expect_success());
      rev.d->distance_to_root = r.value<int>(0, 4).expect_success();
      rev.d->connection = _connection_info.connection();
      rev.d->store_table_name = _triples_store_table_name;
      return cres_success(rev);
    }
    default:
      return cres_failure("Multiple entry with the same revision '{}'", kDBppRevHash(_hash));
    case 0:
      return cres_failure("Revision not found '{}'", kDBppRevHash(_hash));
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("getting revision", r);
  }
}

cres_qresult<kDB::Repository::VersionControl::Revision>
  SQLInterface::revision(const QueryConnectionInfo& _connection_info,
                         const QString& _triples_store_table_name, const QByteArray& _content_hash,
                         int _minimum_historicity)
{
  QString query_text = "SELECT revision, hash, historicity, tags, distance_to_root FROM "
                       "$$table$$_versioning_revisions WHERE content_hash = :content_hash AND "
                       "historicity >= :minimum_historicity LIMIT 1";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":content_hash", _content_hash);
  q.bindValue(":minimum_historicity", _minimum_historicity);

  knowDBC::Result r = q.execute();
  if(r)
  {
    switch(r.tuples())
    {
    case 1:
    {
      VersionControl::Revision rev;
      rev.d->revisionId = r.value<int>(0, 0).expect_success();
      rev.d->hash = r.value<QByteArray>(0, 1).expect_success();
      rev.d->content_hash = _content_hash;
      rev.d->historicity = r.value<int>(0, 2).expect_success();
      rev.d->tags = VersionControl::Revision::Tag(r.value<int>(0, 3).expect_success());
      rev.d->distance_to_root = r.value<int>(0, 4).expect_success();
      rev.d->connection = _connection_info.connection();
      rev.d->store_table_name = _triples_store_table_name;
      return cres_success(rev);
    }
    default:
    case 0:
      return cres_success(kDB::Repository::VersionControl::Revision());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("getting revision", r);
  }
}

cres_qresult<int> SQLInterface::revisionId(const QueryConnectionInfo& _connection_info,
                                           const QString& _triples_store_table_name,
                                           const QByteArray& _hash)
{
  QString query_text = "SELECT revision FROM $$table$$_versioning_revisions WHERE hash = :hash";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":hash", _hash);

  knowDBC::Result r = q.execute();
  if(r)
  {
    if(r.tuples() == 1)
    {
      return r.value<int>(0, 0);
    }
    else
    {
      return cres_failure("Failed to retrieve revision id for {}, got {} tuples, exepceted 1.",
                          _hash, r.tuples());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("getting revision id", r);
  }
}

cres_qresult<kDB::Repository::VersionControl::Revision>
  SQLInterface::revision(const QueryConnectionInfo& _connection_info,
                         const QString& _triples_store_table_name, const int _id)
{
  QString query_text = "SELECT hash, content_hash, historicity, tags, distance_to_root FROM "
                       "$$table$$_versioning_revisions WHERE revision = :revision";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":revision", _id);

  knowDBC::Result r = q.execute();
  if(r and r.tuples() == 1)
  {
    VersionControl::Revision rev;
    rev.d->revisionId = _id;
    rev.d->hash = r.value<QByteArray>(0, 0).expect_success();
    rev.d->content_hash = r.value<QByteArray>(0, 1).expect_success();
    rev.d->historicity = r.value<int>(0, 2).expect_success();
    rev.d->tags = VersionControl::Revision::Tag(r.value<int>(0, 3).expect_success());
    rev.d->distance_to_root = r.value<int>(0, 4).expect_success();
    rev.d->connection = _connection_info.connection();
    rev.d->store_table_name = _triples_store_table_name;
    return cres_success(rev);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("getting revision", r);
  }
}
cres_qresult<bool> SQLInterface::hasRevision(const QueryConnectionInfo& _connection_info,
                                             const QString& _triples_store_table_name,
                                             const QByteArray& _hash)
{
  QString query_text = "SELECT COUNT(*) FROM $$table$$_versioning_revisions WHERE hash = :hash";
  query_text.replace("$$table$$", _triples_store_table_name);

  knowDBC::Query q = _connection_info.createSQLQuery(query_text);
  q.bindValue(":hash", _hash);

  knowDBC::Result r = q.execute();
  if(r and r.tuples() == 1)
  {
    return cres_success<bool>(r.value<int>(0, 0).expect_success() == 1);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("checking revision", r);
  }
}

QList<qint32> SQLInterface::forwardRevisionPath(const QueryConnectionInfo& _connection_info,
                                                const QString& _triples_store_table_name,
                                                const QByteArray& _source_hash,
                                                const QByteArray& _destination_hash)
{
  return forwardRevisionPath(
    _connection_info, _triples_store_table_name,
    revisionId(_connection_info, _triples_store_table_name, _source_hash).expect_success(),
    revisionId(_connection_info, _triples_store_table_name, _destination_hash).expect_success());
}

QList<qint32> SQLInterface::forwardRevisionPath(const QueryConnectionInfo& _connection_info,
                                                const QString& _triples_store_table_name,
                                                int _source_id, int _destination_id)
{
  knowDBC::Query q = _connection_info.createSQLQuery(
    "SELECT kdb_revision_forward_path(:tablename, :source_id, :destination_id)");
  q.bindValue(":tablename", _triples_store_table_name);
  q.bindValue(":source_id", _source_id);
  q.bindValue(":destination_id", _destination_id);
  knowDBC::Result r = q.execute();
  if(r and r.tuples() == 1)
  {
    auto const& [success, path, errorMessage] = r.value<QList<qint32>>(0, 0);
    if(success)
    {
      return path.value();
    }
    else
    {
      return QList<qint32>();
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("getting revision path", r);
    return QList<qint32>();
  }
}

QPair<QList<qint32>, QList<qint32>>
  SQLInterface::revisionPath(const QueryConnectionInfo& _connection_info,
                             const QString& _triples_store_table_name,
                             const QByteArray& _source_hash, const QByteArray& _destination_hash)
{
  int source_id
    = revisionId(_connection_info, _triples_store_table_name, _source_hash).expect_success();
  int destination_id
    = revisionId(_connection_info, _triples_store_table_name, _destination_hash).expect_success();

  knowDBC::Query q = _connection_info.createSQLQuery(
    "SELECT (sub.path).path_to_source AS path_to_source, (sub.path).path_to_destination AS "
    "path_to_destination FROM (SELECT kdb_revision_path(:tablename, :source_id, :destination_id) "
    "AS path) AS sub");
  q.bindValue(":tablename", _triples_store_table_name);
  q.bindValue(":source_id", source_id);
  q.bindValue(":destination_id", destination_id);
  knowDBC::Result r = q.execute();
  if(r and r.tuples() == 1)
  {
    QPair<QList<qint32>, QList<qint32>> path(r.value<QList<qint32>>(0, 0).expect_success(),
                                             r.value<QList<qint32>>(0, 1).expect_success());
    clog_assert((path.first.isEmpty() and path.second.isEmpty())
                or path.first.first() == path.second.first());
    return path;
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("getting revision path", r);
    return QPair<QList<qint32>, QList<qint32>>();
  }
}

cres_qresult<QList<QList<qint32>>>
  SQLInterface::revisionPath(const QueryConnectionInfo& _connection_info,
                             const QString& _triples_store_table_name,
                             const QList<QByteArray>& _revisions_hashes)
{
  QList<int> revisions_id;
  for(const QByteArray& rev : _revisions_hashes)
  {
    cres_try(int rid, revisionId(_connection_info, _triples_store_table_name, rev));
    revisions_id.append(rid);
  }
  clog_debug_vn(revisions_id);

  knowDBC::Query q = _connection_info.createSQLQuery(
    "SELECT path FROM unnest((kdb_revision_path_multi(:tablename, :revisions_id)).pathes)");
  q.bindValue(":tablename", _triples_store_table_name);
  q.bindValue(":revisions_id", revisions_id);

  knowDBC::Result r = q.execute();
  if(r)
  {
    if(r.tuples() == _revisions_hashes.size())
    {
      QList<QList<qint32>> result;
      for(int i = 0; i < r.tuples(); ++i)
      {
        result.append(r.value<QList<qint32>>(i, 0).expect_success());
        clog_assert(result[0].first() == result[i].first());
      }
      return cres_success(result);
    }
    else
    {
      return cres_failure("In getting revisions path, got {} tuples, expected {}", r.tuples(),
                          _revisions_hashes.size());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("getting revision paths", r);
  }
}

namespace
{
  cres_qresult<QJsonValue> buildMeta(const knowCore::Value& _value)
  {
    if(knowCore::ValueIs<knowCore::ValueHash> hash = _value)
    {
      QJsonObject obj;
      QHash<QString, knowCore::Value> h = hash->hash();
      for(auto it = h.begin(); it != h.end(); ++it)
      {
        cres_try(QJsonValue jv, buildMeta(it.value()));
        obj[it.key()] = jv;
      }
      return cres_success(obj);
    }
    else if(knowCore::ValueIs<knowCore::ValueList> list = _value)
    {
      QJsonArray arr;
      for(const knowCore::Value& v : list->values())
      {
        cres_try(QJsonValue jv, buildMeta(v));
        arr.append(jv);
      }
      return cres_success(arr);
    }
    else
    {
      return _value.toJsonObject();
    }
  }
} // namespace

cres_qresult<knowCore::Value> SQLInterface::parseMeta(const QJsonValue& _value)
{
  if(_value.isObject())
  {
    QJsonObject object = _value.toObject();
    if(object.contains("value") and object.contains("datatype"))
    {
      return knowCore::Value::fromJsonObject(object);
    }
    knowCore::ValueHash h;
    for(auto it = object.begin(); it != object.end(); ++it)
    {
      cres_try(knowCore::Value v, parseMeta(it.value().toObject()));
      h.insert(it.key(), v);
    }
    return cres_success(h);
  }
  clog_fatal("wip");
}

cres_qresult<knowCore::Value> SQLInterface::getMeta(const QueryConnectionInfo& _connection_info,
                                                    const QString& _store, const QStringList& _path)
{
  knowDBC::Query q = _connection_info.createSQLQuery(
    "SELECT meta #> :path FROM triples_stores WHERE name = :name");
  q.bindValue(":path", _path);
  q.bindValue(":name", _store);

  knowDBC::Result r = q.execute();
  if(r)
  {
    knowCore::Value v = r.value(0, 0);
    if(knowCore::ValueCast<QJsonValue> jv = v)
    {
      return parseMeta(jv);
    }
    else
    {
      return cres_success(knowCore::Value());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("getting metadata", r);
  }
}

cres_qresult<void> SQLInterface::setMeta(const QueryConnectionInfo& _connection_info,
                                         const QString& _store, const QStringList& _path,
                                         const knowCore::Value& _value)
{
  knowDBC::Query q
    = _connection_info.createSQLQuery("UPDATE triples_stores SET meta = kdb_jsonb_set_deep(meta, "
                                      ":path, :value::jsonb) WHERE name = :name");
  q.bindValue(":path", _path);
  cres_try(QJsonValue valueObj, buildMeta(_value));
  q.bindValue<QJsonValue>(":value", valueObj);
  q.bindValue(":name", _store);

  KDB_REPOSITORY_EXECUTE_QUERY("updating metadata at path {} with value {}", q, _path, _value);

  return cres_success();
}

cres_qresult<void> SQLInterface::dropMeta(const QueryConnectionInfo& _connection_info,
                                          const QString& _store, const QStringList& _path)
{
  knowDBC::Query q = _connection_info.createSQLQuery(
    "UPDATE triples_stores SET meta = (meta #- :path) WHERE name = :name");
  q.bindValue(":path", _path);
  q.bindValue(":name", _store);

  KDB_REPOSITORY_EXECUTE_QUERY("dropping metadata", q, false);

  return cres_success();
}

QList<knowCore::Uri> SQLInterface::getGraphUris(const Transaction& _transaction,
                                                const QString& _tablename, const QString& _field)
{
  QList<knowCore::Uri> result;

  knowDBC::Query q
    = _transaction.createSQLQuery("SELECT DISTINCT " + _field + " FROM " + _tablename);

  knowDBC::Result r = q.execute();
  if(r)
  {
    for(int i = 0; i < r.tuples(); ++i)
    {
      result.append(r.value(i, 0).value<knowCore::Uri>().expect_success());
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("getting metadata", r);
  }

  return result;
}

cres_qresult<void> SQLInterface::removeTriples(const Transaction& _transaction, TripleStore* _store,
                                               const knowCore::Uri& _store_uri)
{
  TripleStore::Private* _store_D = static_cast<TripleStore::Private*>(_store->d.data());
  TripleStore::TSTemporaryTransaction tstt = _store_D->acquireTransaction(_store, _transaction);
  knowDBC::Query q = _transaction.createSQLQuery(
    "DELETE FROM " + _store->tablename()
    + " AS ttd USING delete_tmp WHERE delete_tmp.graph=:graph AND ttd.subject = delete_tmp.subject "
      "AND ttd.predicate = delete_tmp.predicate AND ttd.object = delete_tmp.object");
  q.bindValue(":graph", knowCore::Value::fromValue(_store_uri));

  KDB_REPOSITORY_EXECUTE_QUERY("Removing triples", q);

  return cres_success();
}

cres_qresult<void> SQLInterface::insertTriples(const Transaction& _transaction, TripleStore* _store,
                                               const knowCore::Uri& _store_uri)
{
  TripleStore::Private* _store_D = static_cast<TripleStore::Private*>(_store->d.data());
  TripleStore::TSTemporaryTransaction tstt = _store_D->acquireTransaction(_store, _transaction);

  knowDBC::Query q = _transaction.createSQLQuery();

  // Set the missing md5, if any
  q.setQuery("SELECT id, subject, predicate, object FROM insert_tmp WHERE md5 is null");
  knowDBC::Result result = q.execute();
  if(not result)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("While selecting triples with no md5", result);
  }
  q.setQuery("UPDATE insert_tmp SET md5=:md5 WHERE id =:id");
  for(int i = 0; i < result.tuples(); ++i)
  {
    cres_try(int id, result.value<int>(i, 0));
    knowRDF::Triple t = getTriple(_transaction.d->connection, result, i, 1, 2, 3, false);
    q.bindValue(":id", id);
    cres_try(QByteArray md5, t.md5());
    q.bindValue(":md5", md5);
    KDB_REPOSITORY_EXECUTE_QUERY("updating md5", q);
  }

  // Insert in database
  q.setQuery("INSERT INTO " + _store->tablename()
             + " (subject, predicate, object, md5) SELECT subject, predicate, object, md5 FROM "
               "insert_tmp ON CONFLICT DO NOTHING");
  q.bindValue(":graph", knowCore::Value::fromValue(_store_uri));

  KDB_REPOSITORY_EXECUTE_QUERY("Inserting triples", q);

  return cres_success();
}

cres_qresult<void>
  SQLInterface::setUnitConversionFactors(const QueryConnectionInfo& _connection_info)
{
  kDB::Repository::TemporaryTransaction tt(_connection_info);
  knowDBC::Query q = tt.transaction().createSQLQuery(
    "CREATE TEMPORARY TABLE kdb_unit_conversion_tmp ON COMMIT DROP "
    "AS SELECT * FROM kdb_unit_conversion WITH NO DATA;");
  knowDBC::Result r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to create temporary table", r);
    tt.rollback();
    return cres_failure("Failed to create temporary table");
  }
  q.setQuery(
    "COPY kdb_unit_conversion_tmp(source, destination, factor) FROM STDIN WITH (FORMAT binary)");
  r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR(
      clog_qt::qformat("failed to initiate copy of unit scale data"), r);
    tt.rollback();
    return cres_failure("failed to initiate copy of unit scale data");
  }
  SQLCopyData scd(tt.transaction());
  bool s = true;
  s = s and scd.writeHeader();
  s = s and scd.write<quint32>(0, false); // No oid
  s = s and scd.write<quint32>(0, false); // No extra

  if(not s)
  {
    clog_error("while writting header in copy of data, with error {}", scd.errorMessage());
    tt.rollback();
    return cres_failure("while writting header in copy of data, with error {}", scd.errorMessage());
  }
  for(const knowCore::Unit& base_u : knowCore::Unit::baseUnits())
  {
    QList<knowCore::Unit> units = base_u.derivedUnits();
    units.append(base_u);
    for(const knowCore::Unit& source : units)
    {
      for(const knowCore::Unit& destination : units)
      {
        s = s and scd.write<quint16>(3, false);
        s = s and scd.write<knowCore::Uri>(source.uri(), true);
        s = s and scd.write<knowCore::Uri>(destination.uri(), true);
        s = s and scd.write<double>(source.scale() / destination.scale(), true);
        if(not s)
        {
          clog_error("while copy of data, with error {}", scd.errorMessage());
          tt.rollback();
          return cres_failure("while copy of data, with error {}", scd.errorMessage());
        }
      }
    }
  }

  if(not scd.close())
  {
    clog_error("while closing copy of data, with error {}", scd.errorMessage());
    tt.rollback();
    return cres_failure("while closing copy of data, with error {}", scd.errorMessage());
  }
  q.setQuery("INSERT INTO kdb_unit_conversion(source, destination, factor) SELECT source, "
             "destination, factor FROM kdb_unit_conversion_tmp ON CONFLICT DO NOTHING");
  r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to copy from temporary table to triples", r);
    tt.rollback();
    return cres_failure("failed to copy from temporary table to triples");
  }
  q.setQuery("DROP TABLE kdb_unit_conversion_tmp");
  if(not q.execute())
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to drop temporary table", r);
    tt.rollback();
    return cres_failure("failed to drop temporary table");
  }
  return tt.commitIfOwned();
}

knowRDF::Triple SQLInterface::getTriple(const Connection& _connection,
                                        const knowDBC::Result& _result, int _tuple, int _subject,
                                        int _predicate, int _object, bool _blank_as_uri)
{
  knowRDF::Subject subject;
  knowCore::Uri predicate;
  knowRDF::Object object;

  QString subject_uri = _result.value(_tuple, _subject).value<QString>().expect_success();
  ;

  if(knowRDF::isBlankNodeSkolemisation(subject_uri) and not _blank_as_uri)
  {
    subject = _connection.d->blankNode(subject_uri);
  }
  else
  {
    subject = knowCore::Uri(subject_uri);
  }
  predicate = _result.value<QString>(_tuple, _predicate).expect_success();

  knowCore::Value object_var = _result.value(_tuple, _object);

  if(knowCore::ValueIs<knowRDF::BlankNode> bn = object_var)
  {
    if(_blank_as_uri)
    {
      object = knowCore::Uri(knowRDF::blankNodeSkolemisation(bn));
    }
    else
    {
      object = bn.value();
    }
  }
  else if(knowCore::ValueIs<knowCore::Uri> uri = object_var)
  {
    if(_blank_as_uri or not knowRDF::isBlankNodeSkolemisation(uri))
    {
      object = uri.value();
    }
    else
    {
      object = _connection.d->blankNode(uri.value());
    }
  }
  else if(knowCore::ValueIs<knowRDF::Object> obj = object_var)
  {
    object = obj.value();
  }
  else if(knowCore::ValueIs<knowRDF::Literal> lit = object_var)
  {
    object = lit.value();
  }
  else
  {
    object = knowRDF::Literal::fromValue(object_var);
  }
  return (knowRDF::Triple(subject, predicate, object));
}

QString SQLInterface::viewTableName(const Connection& _connection, const QString& _name)
{
  return _connection.d->uniqueTableName("triples_view", _name);
}

cres_qresult<void> SQLInterface::insertOrRemoveTriples(bool _insert,
                                                       const Transaction& _transaction,
                                                       const QList<knowRDF::Triple>& _triples,
                                                       const QString& _table)
{
  knowDBC::Query q = _transaction.createSQLQuery(
    "CREATE TEMPORARY TABLE triples_tmp ON COMMIT DROP AS SELECT * FROM " + _table
    + " WITH NO DATA;");
  knowDBC::Result r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to create temporary table", r);
    _transaction.rollback();
    return cres_failure("Failed to create temporary table");
  }

  q.setQuery("COPY triples_tmp(subject, predicate, object, md5) FROM STDIN WITH (FORMAT binary)");
  r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR(
      clog_qt::qformat("failed to initiate copy of {} triples", _triples.size()), r);
    _transaction.rollback();
    return cres_failure("failed to initiate copy of {} triples", _triples.size());
  }
  SQLCopyData scd(_transaction);
  bool s = true;
  s = s and scd.writeHeader();
  s = s and scd.write<quint32>(0, false); // No oid
  s = s and scd.write<quint32>(0, false); // No extra

  if(not s)
  {
    clog_error("while writting header in copy of data, with error {}", scd.errorMessage());
    _transaction.rollback();
    return cres_failure("while writting header in copy of data, with error {}", scd.errorMessage());
  }

  for(const knowRDF::Triple& t : _triples)
  {
    s = s and scd.write<quint16>(4, false);
    s = s and scd.write<knowCore::Uri>(SQLInterface::subjectString(t.subject()), true);
    s = s and scd.write<knowCore::Uri>(t.predicate(), true);
    QString oidname;
    cres_try(QByteArray dst,
             _transaction.d->connection.d->rdf_term_marshal->toByteArray(
               knowCore::Value::fromValue(t.object()), oidname, _transaction.d->connection));
    s = s and scd.write<quint32>(dst.size(), false);
    s = s and scd.writeBuffer(dst.constBegin(), dst.size());
    s = s and scd.write<QByteArray>(t.md5().expect_success(), true);
    if(not s)
    {
      clog_error("while copy of data, with error {}", scd.errorMessage());
      _transaction.rollback();
      return cres_failure("while copy of data, with error {}", scd.errorMessage());
    }
  }

  if(not scd.close())
  {
    clog_error("while closing copy of data, with error {}", scd.errorMessage());
    _transaction.rollback();
    return cres_failure("while closing copy of data, with error {}", scd.errorMessage());
  }
  if(_insert)
  {
    q.setQuery("INSERT INTO " + _table
               + "(subject, predicate, object, md5) SELECT subject, predicate, object, md5 FROM "
                 "triples_tmp ON CONFLICT DO NOTHING");
  }
  else
  {
    q.setQuery("DELETE FROM " + _table
               + " AS ttd USING triples_tmp WHERE ttd.subject = triples_tmp.subject AND "
                 "ttd.predicate = triples_tmp.predicate AND ttd.object = triples_tmp.object");
  }
  r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to copy from temporary table to triples", r);
    _transaction.rollback();
    return cres_failure("failed to copy from temporary table to triples");
  }
  q.setQuery("DROP TABLE triples_tmp");
  if(not q.execute())
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to drop temporary table", r);
    _transaction.rollback();
    return cres_failure("failed to drop temporary table");
  }
  return cres_success();
}
