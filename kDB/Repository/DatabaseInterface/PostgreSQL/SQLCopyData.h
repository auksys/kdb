#include <QSharedPointer>
#include <kDB/Forward.h>

namespace kDB::Repository::DatabaseInterface::PostgreSQL
{
  class SQLCopyData
  {
  public:
    SQLCopyData(const Transaction& _transaction);
    ~SQLCopyData();
    bool writeHeader();
    bool writeBuffer(const char* _buffer, int _size);
    template<typename _T_>
    bool write(_T_ _value, bool _write_size);
    bool writeNull();
    bool close();
    QString errorMessage() const;
  private:
    template<typename _T_>
    bool write_impl(const _T_& _value, bool _write_size);
    char* buffer(std::size_t _size);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL
