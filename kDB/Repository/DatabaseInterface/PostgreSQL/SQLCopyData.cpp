#include "SQLCopyData.h"

#include "BinaryInterface_write_p.h"
#include "Connection_p.h"
#include "QueryConnectionInfo_p.h"
#include "Transaction_p.h"

#include <knowCore/Uri.h>
#include <knowCore/Value.h>

using namespace kDB::Repository::DatabaseInterface::PostgreSQL;

struct SQLCopyData::Private
{
  Transaction transaction;
  std::vector<char> buffer;
};

SQLCopyData::SQLCopyData(const Transaction& _transaction) : d(new Private())
{
  d->transaction = _transaction;
}

SQLCopyData::~SQLCopyData() { delete d; }

template<typename _T_>
bool SQLCopyData::write_impl(const _T_& _value, bool _write_size)
{
  std::size_t s = BinaryInterface::calculate_size(_value);
  if(_write_size)
  {
    char* b = buffer(s + 4);
    BinaryInterface::write<quint32>(s, b);
    BinaryInterface::write<_T_>(_value, b + 4);
    return writeBuffer(b, s + 4);
  }
  else
  {
    char* b = buffer(s);
    BinaryInterface::write<_T_>(_value, b);
    return writeBuffer(b, s);
  }
}

#define MAKE_WRITE(_TYPE_)                                                                         \
  template<>                                                                                       \
  bool SQLCopyData::write<_TYPE_>(_TYPE_ _value, bool _write_size)                                 \
  {                                                                                                \
    return write_impl(_value, _write_size);                                                        \
  }

namespace kDB
{
  namespace Repository
  {
    MAKE_WRITE(quint16)
    MAKE_WRITE(qint32)
    MAKE_WRITE(quint32)
    MAKE_WRITE(double)
    MAKE_WRITE(QString)
    MAKE_WRITE(QByteArray)

    template<>
    bool SQLCopyData::write<knowCore::Uri>(knowCore::Uri _value, bool _write_size)
    {
      return write_impl<QString>(_value, _write_size);
    }

    template<>
    bool SQLCopyData::write<knowCore::Value>(knowCore::Value _value, bool _write_size)
    {
      QByteArray arr;
      Oid oid;
      bool is_binary;
      bool is_valid;
      d->transaction.d->connection.d->write(_value, &arr, &oid, &is_binary, &is_valid);
      clog_assert(is_binary);
      if(is_valid)
      {
        bool s = true;
        ;
        if(_write_size)
        {
          s = s and write<quint32>(arr.size(), false);
        }
        s = s and writeBuffer(arr.begin(), arr.size());
        return s;
      }
      else
      {
        return write<quint32>(0xFFFFFFFF, false);
      }
    }
  } // namespace Repository
} // namespace kDB

bool SQLCopyData::writeNull()
{
  quint32 v = 0xFFFFFFFF;
  return writeBuffer(reinterpret_cast<const char*>(&v), sizeof(quint32));
}

bool SQLCopyData::writeHeader()
{
  const char* header = "PGCOPY\n\377\r\n\0";
  return writeBuffer(header, 11);
}

bool SQLCopyData::writeBuffer(const char* _buffer, int _size)
{
  return PQputCopyData(d->transaction.d->handle.connection(), _buffer, _size) == PGRES_COMMAND_OK;
}

bool SQLCopyData::close()
{
  write(quint16(0xffff), false);
  if(PQputCopyEnd(d->transaction.d->handle.connection(), NULL) != PGRES_COMMAND_OK)
  {
    return false;
  }

  PGresult* result = PQgetResult(d->transaction.d->handle.connection());
  ExecStatusType est = PQresultStatus(result);
  PQclear(result);
  return est == PGRES_COMMAND_OK;
}

char* SQLCopyData::buffer(std::size_t _size)
{
  if(d->buffer.size() < _size)
  {
    d->buffer.resize(_size);
  }
  return d->buffer.data();
}

QString SQLCopyData::errorMessage() const
{
  return PQerrorMessage(d->transaction.d->handle.connection());
}
