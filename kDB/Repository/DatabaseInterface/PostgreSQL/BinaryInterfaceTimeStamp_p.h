/*-------------------------------------------------------------------------
 *
 * Portions Copyright (c) 1996-2015, PostgreSQL Global Development Group
 * Portions Copyright (c) 1994, Regents of the University of California
 *
 *-------------------------------------------------------------------------
 */

#include "postgresql_p.h"

extern "C"
{
#include <datatype/timestamp.h>
#include <pgtime.h>
#include <utils/datetime.h>
}

namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface
{

  inline void j2date(int jd, int* year, int* month, int* day)
  {
    unsigned int julian;
    unsigned int quad;
    unsigned int extra;
    int y;

    julian = jd;
    julian += 32044;
    quad = julian / 146097;
    extra = (julian - quad * 146097) * 4 + 3;
    julian += 60 + quad * 3 + extra / 146097;
    quad = julian / 1461;
    julian -= quad * 1461;
    y = julian * 4 / 1461;
    julian = ((y != 0) ? (julian + 305) % 365 : (julian + 306) % 366) + 123;
    y += quad * 4;
    *year = y - 4800;
    quad = julian * 2141 / 65536;
    *day = julian - 7834 * quad / 256;
    *month = (quad + 10) % 12 + 1;

    return;
  } /* j2date() */

  inline void dt2time(double jd, int* hour, int* min, int* sec, fsec_t* fsec)
  {
    int64 time;

    time = jd;
    *hour = time / USECS_PER_HOUR;
    time -= (*hour) * USECS_PER_HOUR;
    *min = time / USECS_PER_MINUTE;
    time -= (*min) * USECS_PER_MINUTE;
    *sec = time / USECS_PER_SEC;
    *fsec = time - (*sec * USECS_PER_SEC);
  } /* dt2time() */

  /*
   * timestamp2tm() - Convert timestamp data type to POSIX time structure.
   *
   * Note that year is _not_ 1900-based, but is an explicit full value.
   * Also, month is one-based, _not_ zero-based.
   * Returns:
   *   0 on success
   *  -1 on out of range
   *
   * If attimezone is NULL, the global timezone setting will be used.
   */
  inline int timestamp2tm(qint64 dt, struct pg_tm* tm, fsec_t* fsec, const char** tzn)
  {
    qint64 date;
    qint64 time;

    time = dt;
    TMODULO(time, date, USECS_PER_DAY);

    if(time < (qint64)(0))
    {
      time += USECS_PER_DAY;
      date -= 1;
    }

    /* add offset to go from J2000 back to standard Julian date */
    date += POSTGRES_EPOCH_JDATE;

    /* Julian day routine does not work for negative Julian days */
    if(date < 0 || date > (qint64)INT_MAX)
      return -1;

    j2date((int)date, &tm->tm_year, &tm->tm_mon, &tm->tm_mday);
    dt2time(time, &tm->tm_hour, &tm->tm_min, &tm->tm_sec, fsec);

    /* Done no TZ conversion wanted */
    tm->tm_isdst = -1;
    tm->tm_gmtoff = 0;
    tm->tm_zone = NULL;
    if(tzn != NULL)
      *tzn = NULL;
    return 0;
  }
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface
