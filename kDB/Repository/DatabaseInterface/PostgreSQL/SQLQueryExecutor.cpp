#include "SQLQueryExecutor.h"

#include "postgresql_p.h"

#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Value.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "../../Connection_p.h"
#include "../../QueryConnectionInfo_p.h"
#include "../../Transaction_p.h"

#include "SQLResult_p.h"

#ifndef FLOAT8ARRAYOID
#define FLOAT8ARRAYOID 1022
#endif

using namespace kDB::Repository::DatabaseInterface::PostgreSQL;

struct SQLQueryExecutor::Private
{
  Private() { initUserBindings(); }
  void initUserBindings();

  QueryConnectionInfo connectionInfo;
  QHash<knowCore::Uri, std::function<QByteArray(const knowCore::Value&, bool&, Oid&)>>
    user_bindings;
  QHash<knowCore::Uri, std::function<QString(const knowCore::Value&)>> user_inline_bindings;
};

namespace
{
  template<class TargetType, class FloatType>
  inline void assignSpecialPsqlFloatValue(FloatType val, TargetType* target)
  {
    if(std::isnan(val))
    {
      *target = TargetType("'NaN'");
    }
    else
    {
      if(std::isinf(val))
      {
        if(val > 0)
        {
          *target = TargetType("'Infinity'");
        }
        else
        {
          *target = TargetType("'-Infinity'");
        }
      }
    }
  }

  template<typename _T_, int _dimension, Oid _oid_>
  QByteArray marshal_vector(const knowCore::Value& _variant, bool& _binary, Oid& _oid)
  {
    _binary = false;
    Q_UNUSED(_binary);
    _oid = _oid_;
    QByteArray r = "{";
    knowCore::Vector<_T_, _dimension> vec
      = _variant.value<knowCore::Vector<_T_, _dimension>>().expect_success();
    for(int i = 0; i < _dimension; ++i)
    {
      if(i != 0)
        r += ",";
      QByteArray v;
      assignSpecialPsqlFloatValue<QByteArray>(vec[i], &v);
      if(v.isEmpty())
        v = QString::number(vec[i]).toLatin1();
      r += v;
    }
    r += "}";
    return r;
  }

  template<typename _T_, int _dimension, Oid _oid_>
  QByteArray marshal_vector_list(const knowCore::Value& _variant, bool& _binary, Oid& _oid)
  {
    _binary = false;
    _oid = _oid_;
    QByteArray r = "{";

    QList<knowCore::Vector<_T_, _dimension>> list
      = _variant.value<QList<knowCore::Vector<_T_, _dimension>>>().expect_success();

    for(int i = 0; i < list.size(); ++i)
    {
      if(i != 0)
        r += ", {";
      else
        r += "{";
      const knowCore::Vector<_T_, _dimension>& vec = list[i];
      for(int j = 0; j < _dimension; ++j)
      {
        if(j != 0)
          r += ",";
        QByteArray v;
        assignSpecialPsqlFloatValue<QByteArray>(vec[i], &v);
        if(v.isEmpty())
          v = QString::number(vec[j]).toLatin1();
        r += v;
      }
      r += "}";
    }

    r += "}";
    return r;
  }

  bool checkForOption(const knowCore::ValueHash& _options, const QString& _key)
  {
    if(_options.contains(_key))
    {
      auto const [success, value, errorMessage] = _options.value<bool>(_key);
      if(success)
      {
        return value.value();
      }
      else
      {
        clog_error("Invalid type for option {}, expected boolean got error {}", _key, errorMessage.value());
      }
    }
    return false;
  }

} // namespace

void SQLQueryExecutor::Private::initUserBindings()
{
  user_bindings[knowCore::FloatListMetaTypeInformation::uri()]
    = [](const knowCore::Value& _variant, bool& _binary, Oid& _oid)
  {
    Q_UNUSED(_binary);
    _oid = FLOAT4ARRAYOID;
    QByteArray r = "{";
    knowCore::FloatList list = _variant.value<knowCore::FloatList>().expect_success();

    for(int i = 0; i < list.size(); ++i)
    {
      if(i != 0)
        r += ",";
      QByteArray v;
      assignSpecialPsqlFloatValue<QByteArray>(list[i], &v);
      if(v.isEmpty())
        v = QString::number(list[i]).toLatin1();
      r += v;
    }

    r += "}";
    return r;
  };
  user_bindings[knowCore::DoubleListMetaTypeInformation::uri()]
    = [](const knowCore::Value& _variant, bool& _binary, Oid& _oid)
  {
    Q_UNUSED(_binary);
    _oid = FLOAT8ARRAYOID;
    QByteArray r = "{";
    knowCore::DoubleList list = _variant.value<knowCore::DoubleList>().expect_success();

    for(int i = 0; i < list.size(); ++i)
    {
      if(i != 0)
        r += ",";
      QByteArray v;
      assignSpecialPsqlFloatValue<QByteArray>(list[i], &v);
      if(v.isEmpty())
        v = QString::number(list[i]).toLatin1();
      r += v;
    }

    r += "}";
    return r;
  };
  user_bindings[knowCore::Vector2dMetaTypeInformation::uri()]
    = marshal_vector<double, 2, FLOAT8ARRAYOID>;
  user_bindings[knowCore::Vector3dMetaTypeInformation::uri()]
    = marshal_vector<double, 3, FLOAT8ARRAYOID>;
  user_bindings[knowCore::Vector4dMetaTypeInformation::uri()]
    = marshal_vector<double, 4, FLOAT8ARRAYOID>;
  user_bindings[knowCore::Vector5dMetaTypeInformation::uri()]
    = marshal_vector<double, 5, FLOAT8ARRAYOID>;
  user_bindings[knowCore::Vector6dMetaTypeInformation::uri()]
    = marshal_vector<double, 6, FLOAT8ARRAYOID>;
  user_bindings[knowCore::Vector7dMetaTypeInformation::uri()]
    = marshal_vector<double, 7, FLOAT8ARRAYOID>;
  user_bindings[knowCore::Vector8dMetaTypeInformation::uri()]
    = marshal_vector<double, 8, FLOAT8ARRAYOID>;
  user_bindings[knowCore::Vector9dMetaTypeInformation::uri()]
    = marshal_vector<double, 9, FLOAT8ARRAYOID>;

  user_bindings[knowCore::Vector3dListMetaTypeInformation::uri()]
    = marshal_vector_list<double, 3, FLOAT8ARRAYOID>;
  user_bindings[knowCore::Vector4dListMetaTypeInformation::uri()]
    = marshal_vector_list<double, 4, FLOAT8ARRAYOID>;

  user_bindings[knowCore::UriMetaTypeInformation::uri()]
    = [this](const knowCore::Value& _variant, bool& _binary, Oid& _oid)
  {
    _binary = false;
    _oid = connectionInfo.connection().d->nameToOid("kdb_uri").expect_success();
    return QString(_variant.value<knowCore::Uri>().expect_success()).toUtf8();
  };

  user_inline_bindings[knowCore::UriMetaTypeInformation::uri()]
    = [](const knowCore::Value& _variant)
  {
    QString r = _variant.value<knowCore::Uri>().expect_success();
    return '\'' + r.replace('\'', "\\\'") + '\'';
  };
}

SQLQueryExecutor::SQLQueryExecutor(const QueryConnectionInfo& _connectionInfo) : d(new Private)
{
  d->connectionInfo = _connectionInfo;
}

SQLQueryExecutor::~SQLQueryExecutor() { delete d; }

knowDBC::Result SQLQueryExecutor::execute(const QString& _query,
                                          const knowCore::ValueHash& _options,
                                          const knowCore::ValueHash& _bindings)
{
  QSharedPointer<ConnectionHandle> handle = d->connectionInfo.d->connectionHandle();
  PGresult* result = nullptr;

  QString q = _query;

  QStringList keys = _bindings.keys();
  std::sort(keys.begin(), keys.end(), [](const QString& _a, const QString& _b) { return _a > _b; });

  QList<knowCore::Value> bindings_list;
  for(const QString& key : keys)
  {
    QString oq = q;
    q = q.replace(key, "$" + QString::number(bindings_list.size() + 1));
    if(q != oq)
    {
      bindings_list.append(_bindings.value(key));
    }
  }

  const int number_of_bindings = bindings_list.size();

  const bool inline_arguments
    = checkForOption(_options, knowDBC::Query::OptionsKeys::InlineArguments);
  const bool multi_queries = checkForOption(_options, knowDBC::Query::OptionsKeys::MultiQueries);

  if(inline_arguments or multi_queries)
  {
    for(int i = number_of_bindings - 1; i >= 0; --i)
    {
      QString r("NULL");
      const knowCore::Value& val = bindings_list[i];

      if(knowCore::ValueIs<knowCore::Timestamp> dt = val)
      {
        r = dt->toEpoch<knowCore::NanoSeconds>().count().toString();
      }
      else if(knowCore::ValueIs<bool> b = val)
      {
        if(b)
          r = QByteArray("TRUE");
        else
          r = QByteArray("FALSE");
      }
      else if(knowCore::ValueIs<QByteArray> bytes = val)
      {
        r = bytes->toHex();
      }
      else if(knowCore::ValueIs<float> fl = val)
      {
        assignSpecialPsqlFloatValue<QString, float>(fl, &r);
        if(r.isEmpty())
          r = QString::number(fl.value());
      }
      else if(knowCore::ValueIs<double> dbl = val)
      {
        assignSpecialPsqlFloatValue<QString, double>(dbl, &r);
        if(r.isEmpty())
          r = QString::number(dbl.value());
      }
      else if(knowCore::ValueIs<quint8> n = val)
      {
        r = QString::number(n.value());
      }
      else if(knowCore::ValueIs<quint16> n = val)
      {
        r = QString::number(n.value());
      }
      else if(knowCore::ValueIs<quint32> n = val)
      {
        r = QString::number(n.value());
      }
      else if(knowCore::ValueIs<quint64> n = val)
      {
        r = QString::number(n.value());
      }
      else if(knowCore::ValueIs<qint8> n = val)
      {
        r = QString::number(n.value());
      }
      else if(knowCore::ValueIs<qint16> n = val)
      {
        r = QString::number(n.value());
      }
      else if(knowCore::ValueIs<qint32> n = val)
      {
        r = QString::number(n.value());
      }
      else if(knowCore::ValueIs<qint64> n = val)
      {
        r = QString::number(n.value());
      }
      else if(d->user_inline_bindings.contains(val.datatype()))
      {
        r = d->user_inline_bindings.value(val.datatype())(val);
      }
      else if(knowCore::ValueCast<QString> str = val)
      {
        r = QString(str);
        r.replace('\'', "\\\'");
        r = '\'' + r + '\'';
      }
      else
      {
        clog_error("Unsupported type for inlining {} ", val.datatype());
      }
      q = q.replace("$" + QString::number(i + 1), r);
    }
    if(multi_queries)
    {
      result = PQexec(handle->connection(), qPrintable(q));
    }
    else
    {
      result = PQexecParams(handle->connection(), qPrintable(q), 0, nullptr, nullptr, nullptr,
                            nullptr, 1);
    }
  }
  else
  {
    QVector<Oid> types(number_of_bindings);
    QVector<const char*> values(number_of_bindings);
    QVector<QByteArray> values_(number_of_bindings);
    QVector<int> lengths(number_of_bindings);
    QVector<int> binary(number_of_bindings);

    for(int i = 0; i < number_of_bindings; ++i)
    {
      const knowCore::Value& val = bindings_list[i];
      QByteArray& r = values_[i];
      Oid oid = 0;
      bool is_binary = false;
      bool is_valid;

      d->connectionInfo.d->connection.d->write(val, &r, &oid, &is_binary, &is_valid);
      if(is_valid)
      {
        values[i] = r.data();
      }
      else
      {
        values[i] = nullptr;
      }
      types[i] = oid;
      lengths[i] = r.length();
      binary[i] = is_binary ? 1 : 0;
    }

    result = PQexecParams(handle->connection(), qPrintable(q), number_of_bindings, types.data(),
                          values.data(), lengths.data(), binary.data(), 1);
  }
  QString error;
  if(PQresultStatus(result) != PGRES_COMMAND_OK and PQresultStatus(result) != PGRES_TUPLES_OK)
  {
    error = PQerrorMessage(handle->connection());
  }
  return knowDBC::Result(new SQLResult(result, _query, error, d->connectionInfo.d->connection));
}

knowCore::Uri SQLQueryExecutor::queryLanguage() const
{
  return knowCore::Uris::askcore_db_query_language::SQL;
}
