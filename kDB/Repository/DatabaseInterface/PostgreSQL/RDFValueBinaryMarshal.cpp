#include "RDFValueBinaryMarshal.h"

#include <clog_qt>
#include <knowCore/BigNumberList.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueList.h>

#include <knowRDF/Literal.h>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/xsd.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "BinaryInterface_read_p.h"
#include "BinaryInterface_write_p.h"
#include "BinaryMarshalsRegistry.h"
#include "ConnectionHandle_p.h"
#include "Connection_p.h"
#include "GraphsManager.h"
#include "Logging.h"
#include "NotificationsManager.h"
#include "SQLInterface_p.h"
#include "Transaction_p.h"
#include "TripleStore.h"

using namespace kDB::Repository::DatabaseInterface::PostgreSQL;

struct RDFValueBinaryMarshal::FieldIO::Private
{
  QString typename_;
};

RDFValueBinaryMarshal::FieldIO::FieldIO(const QString& _typename) : d(new Private)
{
  d->typename_ = _typename;
}

RDFValueBinaryMarshal::FieldIO::~FieldIO() { delete d; }

QString RDFValueBinaryMarshal::FieldIO::typeName() const { return d->typename_; }

namespace kDB
{
  namespace Repository
  {
    namespace details
    {
      class BoolFieldIO : public RDFValueBinaryMarshal::FieldIO
      {
      public:
        BoolFieldIO() : RDFValueBinaryMarshal::FieldIO("bool") {}
        bool accept(const knowCore::Value& _variant) const override { return _variant.is<bool>(); }
        quint32 calculateSize(const knowCore::Value&) const override { return 1; }
        knowCore::Value read(const char* _data, int _field_size,
                             const kDB::Repository::Connection& /*_connection*/) const override
        {
          return knowCore::Value::fromValue(BinaryInterface::read<bool>(_data, _field_size));
        }
        void write(char* _data, const knowCore::Value& _value,
                   const kDB::Repository::Connection& /*_connection*/) const override
        {
          BinaryInterface::write<bool>(_value.value<bool>().expect_success(), _data);
        }
      };
      class NumericFieldIO : public RDFValueBinaryMarshal::FieldIO
      {
      public:
        NumericFieldIO() : RDFValueBinaryMarshal::FieldIO("numeric") {}
        bool accept(const knowCore::Value& _variant) const override
        {
          return _variant.datatype() == knowCore::BigNumberMetaTypeInformation::uri()
                 or knowCore::Uris::isNumericType(_variant.datatype());
        }
        quint32 calculateSize(const knowCore::Value& _value) const override
        {
          return BinaryInterface::calculate_size<knowCore::BigNumber>(
            knowCore::BigNumber::fromValue(_value).expect_success());
        }
        knowCore::Value read(const char* _data, int _field_size,
                             const kDB::Repository::Connection& /*_connection*/) const override
        {
          return knowCore::Value::fromValue(
            BinaryInterface::read<knowCore::BigNumber>(_data, _field_size));
        }
        void write(char* _data, const knowCore::Value& _value,
                   const kDB::Repository::Connection& /*_connection*/) const override
        {
          BinaryInterface::write<knowCore::BigNumber>(
            knowCore::BigNumber::fromValue(_value).expect_success(), _data);
        }
      };
      class NumericListFieldIO : public RDFValueBinaryMarshal::FieldIO
      {
      public:
        NumericListFieldIO() : RDFValueBinaryMarshal::FieldIO("numeric[]") {}
        bool accept(const knowCore::Value& _variant) const override
        {
          if(_variant.datatype() == knowCore::BigNumberListMetaTypeInformation::uri()
             or _variant.datatype() == knowCore::Int16ListMetaTypeInformation::uri()
             or _variant.datatype() == knowCore::Int32ListMetaTypeInformation::uri()
             or _variant.datatype() == knowCore::Int64ListMetaTypeInformation::uri()
             or _variant.datatype() == knowCore::FloatListMetaTypeInformation::uri()
             or _variant.datatype() == knowCore::DoubleListMetaTypeInformation::uri())
            return true;

          if(knowCore::ValueIs<knowCore::ValueList> list = _variant)
          {
            return knowCore::ValueList(list).canConvertAllTo<knowCore::BigNumber>();
          }
          return false;
        }
        quint32 calculateSize(const knowCore::Value& _number) const override
        {
          return BinaryInterface::calculate_size<knowCore::BigNumberList>(
            knowCore::BigNumberList::fromValue(_number).expect_success());
        }
        knowCore::Value read(const char* _data, int _field_size,
                             const kDB::Repository::Connection& /*_connection*/) const override
        {
          return knowCore::BigNumberList(
                   BinaryInterface::read<QList<knowCore::BigNumber>>(_data, _field_size))
            .toValue();
        }
        void write(char* _data, const knowCore::Value& _value,
                   const kDB::Repository::Connection& /*_connection*/) const override
        {
          BinaryInterface::write<QList<knowCore::BigNumber>>(
            knowCore::BigNumberList::fromValue(_value).expect_success(), _data);
        }
      };
      class TextFieldIO : public RDFValueBinaryMarshal::FieldIO
      {
      public:
        TextFieldIO() : RDFValueBinaryMarshal::FieldIO("text") {}
        bool accept(const knowCore::Value& _variant) const override
        {
          return _variant.is<QString>();
        }
        quint32 calculateSize(const knowCore::Value& _value) const override
        {
          return BinaryInterface::calculate_size<QString>(_value.value<QString>().expect_success());
        }
        knowCore::Value read(const char* _data, int _field_size,
                             const kDB::Repository::Connection& /*_connection*/) const override
        {
          return knowCore::Value::fromValue(BinaryInterface::read<QString>(_data, _field_size));
        }
        void write(char* _data, const knowCore::Value& _value,
                   const kDB::Repository::Connection& /*_connection*/) const override
        {
          BinaryInterface::write<QString>(_value.value<QString>().expect_success(), _data);
        }
      };
      class ByteaFieldIO : public RDFValueBinaryMarshal::FieldIO
      {
      public:
        ByteaFieldIO() : RDFValueBinaryMarshal::FieldIO("bytea") {}
        bool accept(const knowCore::Value& _variant) const override
        {
          return _variant.is<QByteArray>();
        }
        quint32 calculateSize(const knowCore::Value& _value) const override
        {
          return BinaryInterface::calculate_size<QByteArray>(
            _value.value<QByteArray>().expect_success());
        }
        knowCore::Value read(const char* _data, int _field_size,
                             const kDB::Repository::Connection& /*_connection*/) const override
        {
          return knowCore::Value::fromValue(QByteArray(_data, _field_size));
        }
        void write(char* _data, const knowCore::Value& _value,
                   const kDB::Repository::Connection&) const override
        {
          BinaryInterface::write(_value.value<QByteArray>().expect_success(), _data);
        }
      };
    } // namespace details
  } // namespace Repository
} // namespace kDB

struct RDFValueBinaryMarshal::Private
{
  static cres_qresult<void> init_fields(Transaction _transaction);
  static cres_qresult<void> init_fields_extra(Transaction _transaction);
  static cres_qresult<void> add_field(Transaction _transaction,
                                      const RDFValueBinaryMarshal::FieldIO* _fieldIO,
                                      quint64 _operators, quint64 _features);
  static cres_qresult<void> register_term(Transaction _transaction, const knowCore::Uri& _uri,
                                          const QString& _typename,
                                          const QString& _cast_typename = QString());

  struct FieldIOs
  {
    ~FieldIOs() { qDeleteAll(fields.values()); }
    QHash<QString, const FieldIO*> fields;
    QMutex mutex;
  };
  static FieldIOs fieldsIOs;

  knowCore::WeakReference<Connection> connection;
};

RDFValueBinaryMarshal::Private::FieldIOs RDFValueBinaryMarshal::Private::fieldsIOs;

cres_qresult<void> RDFValueBinaryMarshal::Private::init_fields(Transaction _transaction)
{
  _transaction.d->connection.d->update_kdb_rdf_value_type_enum_values();
  cres_try(cres_ignore, add_field(_transaction, new details::BoolFieldIO(),
                                  (quint64)Operator::Equal | (quint64)Operator::NotEqual,
                                  (quint64)Feature::BtreeIndexable));
  cres_try(cres_ignore,
           add_field(_transaction, new details::NumericFieldIO(),
                     (quint64)Operator::AllArithmetic | (quint64)Operator::AllComparison,
                     (quint64)Feature::BtreeIndexable));
  cres_try(cres_ignore,
           add_field(_transaction, new details::TextFieldIO(), (quint64)Operator::AllComparison,
                     (quint64)Feature::BtreeIndexable));
  cres_try(cres_ignore, add_field(_transaction, new details::ByteaFieldIO(),
                                  (quint64)Operator::Equal | (quint64)Operator::NotEqual,
                                  (quint64)Feature::BtreeIndexable));
  cres_try(cres_ignore, add_field(_transaction, new details::NumericListFieldIO(),
                                  (quint64)Operator::Equal | (quint64)Operator::NotEqual,
                                  (quint64)Feature::BtreeIndexable));
  cres_try(cres_ignore, register_term(_transaction, knowCore::Uris::xsd::integer32, "int4"));
  cres_try(cres_ignore, register_term(_transaction, knowCore::Uris::xsd::integer64, "int8"));
  cres_try(cres_ignore, register_term(_transaction, knowCore::Uris::xsd::string, "text"));
  cres_try(cres_ignore,
           register_term(_transaction, knowCore::Uris::askcore_datatype::decimallist, "numeric[]"));
  return cres_success();
}

cres_qresult<void> RDFValueBinaryMarshal::Private::init_fields_extra(Transaction _transaction)
{
  cres_try(cres_ignore,
           register_term(_transaction, knowCore::Uris::askcore_datatype::floatlist, "float4[]"));
  cres_try(cres_ignore,
           register_term(_transaction, knowCore::Uris::askcore_datatype::doublelist, "float8[]"));
  cres_try(cres_ignore, register_term(_transaction, knowCore::Uris::xsd::float32, "float4"));
  cres_try(cres_ignore, register_term(_transaction, knowCore::Uris::xsd::float64, "float8"));
  return cres_success();
}

cres_qresult<void>
  RDFValueBinaryMarshal::Private::add_field(Transaction _transaction,
                                            const RDFValueBinaryMarshal::FieldIO* _fieldIO,
                                            quint64 _operators, quint64 _features)
{
  QString type_name = _fieldIO->typeName();
  QString field_name = type_name;
  field_name.replace("[]", "_array");
  if(not Private::fieldsIOs.fields.contains(_fieldIO->typeName()))
  {
    Private::fieldsIOs.fields[_fieldIO->typeName()] = _fieldIO;
  }
  else
  {
    delete _fieldIO;
    _fieldIO = nullptr;
  }
  if(not _transaction.d->connection.d->kdb_rdf_value_type_enum_values.contains(type_name))
  {
    knowDBC::Query qenum = _transaction.d->connection.createSQLQuery(
      "ALTER TYPE kdb_rdf_value_type ADD VALUE IF NOT EXISTS '" + type_name + "';");
    knowDBC::Result r = qenum.execute();
    if(not r)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("in adding RDF field", r);
    }

    _transaction.d->connection.d->kdb_rdf_value_type_enum_values.append(type_name);

    knowDBC::Query q = _transaction.createSQLQuery(
      "INSERT INTO kdb_rdf_value_definitions(name, typename, operators, features) VALUES (:name, "
      ":typename, :operators, :features)");
    q.bindValue(":name", field_name);
    q.bindValue(":typename", type_name);
    q.bindValue(":operators", _operators);
    q.bindValue(":features", _features);
    r = q.execute();
    if(not r)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("Failed to insert definition: ", r);
    }

    QString query_str("ALTER TYPE kdb_rdf_value ADD ATTRIBUTE " + field_name + " " + type_name
                      + ";\n");
    for(int i = 1; i < _transaction.d->connection.d->kdb_rdf_value_type_enum_values.size(); ++i)
    {
      query_str += "CREATE OR REPLACE FUNCTION kdb_rdf_value_create(value "
                   + _transaction.d->connection.d->kdb_rdf_value_type_enum_values[i]
                   + ") RETURNS kdb_rdf_value AS $$\n"
                     "SELECT ROW('"
                   + _transaction.d->connection.d->kdb_rdf_value_type_enum_values[i] + "'";
      for(int k = 1; k < i; ++k)
      {
        query_str += ", null";
      }
      query_str += ", value";
      for(int k = i + 1; k < _transaction.d->connection.d->kdb_rdf_value_type_enum_values.size();
          ++k)
      {
        query_str += ", null";
      }
      query_str += ")::kdb_rdf_value\n"
                   "$$ LANGUAGE sql IMMUTABLE;\n";
    }
    query_str += "CREATE CAST (" + type_name
                 + " AS kdb_rdf_value) WITH FUNCTION kdb_rdf_value_create(" + type_name
                 + ") AS IMPLICIT;";

    q.setQuery("SELECT name,typename,operators FROM kdb_rdf_value_definitions");
    knowDBC::Result rdf_value_definitions = q.execute();
    if(not rdf_value_definitions)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR_RETURN("Failed to read definitions: ",
                                               rdf_value_definitions);
    }

    // Arithmetic functions
    typedef std::tuple<Operator, QString, QString> OQ;
    QList<OQ> arithmetic_operator_defs;
    arithmetic_operator_defs << OQ(Operator::Addition, "addition", "+")
                             << OQ(Operator::Substraction, "substraction", "-")
                             << OQ(Operator::Substraction, "multiplication", "*")
                             << OQ(Operator::Substraction, "division", "/");

    for(const OQ& oq : arithmetic_operator_defs)
    {
      Operator op_flag = std::get<0>(oq);
      QString func_name = std::get<1>(oq);
      QString op_name = std::get<2>(oq);
      query_str
        += "CREATE OR REPLACE FUNCTION kdb_rdf_value_" + func_name
           + "(a kdb_rdf_value, b kdb_rdf_value) RETURNS kdb_rdf_value AS $$\n"
             "SELECT CASE WHEN (a).type != (b).type THEN null::kdb_rdf_value\n"; // ('Binary
                                                                                 // arithmetic "
                                                                                 // + op_name + "
                                                                                 // operation on
                                                                                 // value of
                                                                                 // different types'
                                                                                 // ||
                                                                                 // (a).type::text
                                                                                 // || ' and ' ||
                                                                                 // (b).type::text)\n";
      for(int i = 0; i < rdf_value_definitions.tuples(); ++i)
      {
        QString name = rdf_value_definitions.value<QString>(i, 0).expect_success();
        QString type_name = rdf_value_definitions.value<QString>(i, 1).expect_success();
        quint64 operators = rdf_value_definitions.value<quint64>(i, 2).expect_success();
        if(operators & (quint64)op_flag)
        {
          query_str += "            WHEN (a).type = '" + type_name
                       + "' THEN\n"
                         "              kdb_rdf_value_create((a)."
                       + name + " " + op_name + " (b)." + name + ")\n";
        }
      }

      query_str
        += "            ELSE null::kdb_rdf_value\n" // ('Binary arithmetic operation " + op_name + "
                                                    // not supported on type ' || (a).type)\n"
           "          END;\n"
           "$$ LANGUAGE SQL IMMUTABLE;";
    }

    // Unary Arithmetic functions

    QList<OQ> unary_arithmetic_operator_defs;
    unary_arithmetic_operator_defs << OQ(Operator::Minus, "minus", "-");

    for(const OQ& oq : unary_arithmetic_operator_defs)
    {
      Operator op_flag = std::get<0>(oq);
      QString func_name = std::get<1>(oq);
      QString op_name = std::get<2>(oq);
      query_str += "CREATE OR REPLACE FUNCTION kdb_rdf_value_" + func_name
                   + "(a kdb_rdf_value) RETURNS kdb_rdf_value AS $$\n"
                     "SELECT CASE WHEN false THEN null::kdb_rdf_value\n";
      for(int i = 0; i < rdf_value_definitions.tuples(); ++i)
      {
        QString name = rdf_value_definitions.value<QString>(i, 0).expect_success();
        QString type_name = rdf_value_definitions.value<QString>(i, 1).expect_success();
        quint64 operators = rdf_value_definitions.value<quint64>(i, 2).expect_success();
        if(operators & (quint64)op_flag)
        {
          query_str += "            WHEN (a).type = '" + type_name
                       + "' THEN\n"
                         "              kdb_rdf_value_create("
                       + op_name + "(a)." + name + ")\n";
        }
      }
      query_str
        += "            ELSE null::kdb_rdf_value\n" // ('Unary arithmetic operation " + op_name + "
                                                    // not supported on type ' || (a).type)\n"
           "          END;\n"
           "$$ LANGUAGE SQL IMMUTABLE;";
    }

    // Comparison functions
    QList<OQ> comparison_operator_defs;
    comparison_operator_defs << OQ(Operator::Inferior, "inferior", "<")
                             << OQ(Operator::InferiorEqual, "inferior_equal", "<=")
                             << OQ(Operator::Superior, "superior", ">")
                             << OQ(Operator::SuperiorEqual, "superior_equal", ">=")
                             << OQ(Operator::Equal, "equal", "=")
                             << OQ(Operator::NotEqual, "not_equal", "!=");

    for(const OQ& oq : comparison_operator_defs)
    {
      Operator op_flag = std::get<0>(oq);
      QString func_name = std::get<1>(oq);
      QString op_name = std::get<2>(oq);
      query_str += "CREATE OR REPLACE FUNCTION kdb_rdf_value_" + func_name
                   + "(a kdb_rdf_value, b kdb_rdf_value) RETURNS boolean AS $$\n"
                     "SELECT (NOT (a IS NULL) AND NOT (b IS NULL)\n"
                     "        AND ((a).type = (b).type)\n"
                     "        AND NOT ((a).type IS NULL) AND NOT ((b).type IS NULL)) AND (FALSE\n";
      for(int i = 0; i < rdf_value_definitions.tuples(); ++i)
      {
        QString name = rdf_value_definitions.value<QString>(i, 0).expect_success();
        QString type_name = rdf_value_definitions.value<QString>(i, 1).expect_success();
        quint64 operators = rdf_value_definitions.value<quint64>(i, 2).expect_success();
        if(operators & (quint64)op_flag)
        {
          query_str += "            OR ((a).type = '" + type_name + "' AND (a)." + name + " "
                       + op_name + " (b)." + name + ")\n";
        }
      }

      query_str += "          );\n"
                   "$$ LANGUAGE SQL IMMUTABLE;\n";
    }
    PGresult* result = PQexec(_transaction.d->handle.connection(), qPrintable(query_str));
    if(PQresultStatus(result) != PGRES_COMMAND_OK)
    {
      cres_qerror err = cres_log_failure("while executing query {}: {}", query_str,
                                         PQerrorMessage(_transaction.d->handle.connection()));
      PQclear(result);
      return err;
    }
    PQclear(result);

    if(_features & (quint64)Feature::BtreeIndexable)
    {
      for(const TripleStore& ts : _transaction.d->connection.graphsManager()->tripleStores())
      {
        cres_try(cres_ignore, SQLInterface::createIndex(_transaction, ts, type_name, "btree"));
      }
    }
    if(_features & (quint64)Feature::GistIndexable)
    {
      for(const TripleStore& ts : _transaction.d->connection.graphsManager()->tripleStores())
      {
        cres_try(cres_ignore, SQLInterface::createIndex(_transaction, ts, type_name, "gist"));
      }
    }
    q.setQuery("NOTIFY kdb_rdf_value_type_enum_values_updated");
    KDB_REPOSITORY_EXECUTE_QUERY("Failed to notify update to kdb_rdf_value_type", q, false);

    return cres_success();
  }
  else
  {
    // Already there
    return cres_success();
  }
}

cres_qresult<void> RDFValueBinaryMarshal::Private::register_term(Transaction _transaction,
                                                                 const knowCore::Uri& _uri,
                                                                 const QString& _typename,
                                                                 const QString& _cast_typename)
{
  QString qt = "CREATE OR REPLACE FUNCTION kdb_rdf_term_create(value " + _typename +
               R"() RETURNS kdb_rdf_term AS $$
  SELECT kdb_rdf_term_create(')"
               + _uri + "', kdb_rdf_value_create(value"
               + (_cast_typename.isEmpty() ? QString() : "::" + _cast_typename) + R"(), '');
$$ LANGUAGE SQL IMMUTABLE;)";
  knowDBC::Query q = _transaction.createSQLQuery(qt);
  KDB_REPOSITORY_EXECUTE_QUERY("Failed to create 'kdb_rdf_term_create' function", q, false);
  return cres_success();
}

QStringList RDFValueBinaryMarshal::fields(const Connection& _connection)
{
  return _connection.d->kdb_rdf_value_type_enum_values;
}

#define ACTION_FAILURE                                                                             \
  on_failure(transaction.d->connection.d->update_kdb_rdf_value_type_enum_values();                 \
             transaction.rollback();)

cres_qresult<void> RDFValueBinaryMarshal::registerField(
  const Connection& _connection, const knowCore::Uri& _defaultUri,
  const RDFValueBinaryMarshal::FieldIO* _fieldIO, quint64 _operators, quint64 _features)
{
  QMutexLocker l(&Private::fieldsIOs.mutex);
  Transaction transaction(_connection);
  clog_assert(transaction.isActive());
  QString typeName = _fieldIO->typeName();
  cres_try(cres_ignore, _connection.d->graphsManager->lockAll(transaction),
           on_failure(transaction.rollback()));

  cres_try(cres_ignore, Private::init_fields(transaction), ACTION_FAILURE);
  cres_try(cres_ignore, Private::add_field(transaction, _fieldIO, _operators, _features),
           ACTION_FAILURE);
  cres_try(cres_ignore, Private::register_term(transaction, _defaultUri, typeName), ACTION_FAILURE);
  cres_try(cres_ignore, transaction.commit(), ACTION_FAILURE);
  return cres_success();
}

cres_qresult<void> RDFValueBinaryMarshal::registerTerm(const Connection& _connection,
                                                       const knowCore::Uri& _defaultUri,
                                                       const QString& _typename,
                                                       const QString& _cast_typename)
{
  QMutexLocker l(&Private::fieldsIOs.mutex);
  Transaction transaction(_connection);
  clog_assert(transaction.isActive());
  cres_try(cres_ignore, Private::register_term(transaction, _defaultUri, _typename, _cast_typename),
           on_failure(transaction.rollback()));
  cres_try(cres_ignore, transaction.commit());
  return cres_success();
}

RDFValueBinaryMarshal::RDFValueBinaryMarshal(const Connection& _connection)
    : AbstractBinaryMarshal("kdb_rdf_value", knowCore::Uri(), Mode::ToVariant | Mode::ToByteArray),
      d(new Private)
{
  d->connection = _connection;
}

cres_qresult<RDFValueBinaryMarshal*> RDFValueBinaryMarshal::create(const Connection& _connection)
{
  Transaction transaction(_connection);
#define ACTION                                                                                     \
  on_failure(transaction.rollback();                                                               \
             Connection(_connection).d->update_kdb_rdf_value_type_enum_values();                   \
             clog_warning("Failed to initialise RDFValueBinaryMarshal...");)

  cres_try(cres_ignore, Private::init_fields(transaction), ACTION);
  cres_try(cres_ignore, transaction.commit(), ACTION);
  return cres_success(new RDFValueBinaryMarshal(_connection));
}

RDFValueBinaryMarshal::~RDFValueBinaryMarshal() { delete d; }

cres_qresult<knowCore::Value>
  RDFValueBinaryMarshal::toValue(const QByteArray& _source,
                                 const kDB::Repository::Connection& _connection) const
{
  QMutexLocker l(&Private::fieldsIOs.mutex);
  const char* data = _source.data();
  const quint32 fields = BinaryInterface::read<quint32>(data, 4) - 1;
  data += 4;
  const quint32 enum_oid = BinaryInterface::read<quint32>(data, 4);
  data += 4;
  Q_UNUSED(enum_oid);
  const quint32 enum_length = BinaryInterface::read<quint32>(data, 4);
  data += 4;
  if(enum_length == 0xFFFFFFFF)
  {
    return cres_success(knowCore::Value());
  }
  QString enum_name = BinaryInterface::read<QString>(data, enum_length);
  data += enum_length;
  if(enum_name == "null")
  {
    return cres_success(knowCore::Value());
  }
  int field_index = _connection.d->kdb_rdf_value_type_enum_values.indexOf(enum_name) - 1;
  if(field_index == -1)
  {
    return cres_failure("Unkwown literal field/type {}", enum_name);
  }

  for(quint32 i = 0; i < fields; ++i)
  {
    const Oid field_oid = BinaryInterface::read<Oid>(data, sizeof(Oid));
    data += sizeof(Oid);
    Q_UNUSED(field_oid); // Unused
    const quint32 field_size = BinaryInterface::read<quint32>(data, 4);
    data += 4;
    if(field_size != 0xFFFFFFFF)
    {
      if(i == quint32(field_index))
      {
        const FieldIO* fio = Private::fieldsIOs.fields[enum_name];
        if(fio)
        {
          return cres_success(fio->read(data, field_size, _connection));
        }
        else
        {
          return cres_failure("No FieldIO defined for {}", enum_name);
        }
      }
      else
      {
        clog_error("Non-null field {}",
                   _connection.d->kdb_rdf_value_type_enum_values[field_index + 1]);
      }
      data += field_size;
    }
    else
    {
      if(i == quint32(field_index))
      {
        return cres_failure("Empty value for {}", enum_name);
      }
    }
  }
  qFatal("Should this happen?");
  return cres_failure("Internal error in RDFValueBinaryMarshal::toVariant");
}

quint32 RDFValueBinaryMarshal::calculateSize(const knowCore::Value& _value) const
{
  QMutexLocker l(&Private::fieldsIOs.mutex);

  knowCore::Value value;

  if(knowCore::ValueIs<knowRDF::Literal> lit = _value)
  {
    value = lit; // Unpack the literal
  }
  else
  {
    value = _value;
  }

  const std::size_t fields = Connection(d->connection)
                               .d->kdb_rdf_value_type_enum_values
                               .size(); // with the enum (-1 for null enum, +1 for the enum)
  const std::size_t field_size = sizeof(quint32) + sizeof(Oid);
  const std::size_t header_size = sizeof(quint32);

  for(int i = 1; i < Connection(d->connection).d->kdb_rdf_value_type_enum_values.size(); ++i)
  {
    QString enum_name_i = Connection(d->connection).d->kdb_rdf_value_type_enum_values.at(i);
    const FieldIO* fio = Private::fieldsIOs.fields[enum_name_i];
    if(fio and fio->accept(value))
    {
      return header_size + fields * field_size + fio->calculateSize(value) + enum_name_i.size();
    }
  }

  auto const [success, literal, message] = value.toRdfLiteral();

  if(success)
  {
    knowCore::Value literal_value = knowCore::Value::fromValue(literal.value());
    for(int i = 1; i < Connection(d->connection).d->kdb_rdf_value_type_enum_values.size(); ++i)
    {
      QString enum_name_i = Connection(d->connection).d->kdb_rdf_value_type_enum_values.at(i);
      const FieldIO* fio = Private::fieldsIOs.fields[enum_name_i];
      if(fio and fio->accept(literal_value))
      {
        QString enum_name_i = Connection(d->connection).d->kdb_rdf_value_type_enum_values.at(i);
        const FieldIO* fio = Private::fieldsIOs.fields[enum_name_i];
        if(fio and fio->accept(literal_value))
          return header_size + fields * field_size + fio->calculateSize(literal_value)
                 + enum_name_i.size();
      }
    }
    clog_fatal("String FieldIO should be available");
  }

  return header_size + fields * field_size + sizeof("null") - 1;
}

cres_qresult<QByteArray>
  RDFValueBinaryMarshal::toByteArray(const knowCore::Value& _source, QString& _oidName,
                                     const kDB::Repository::Connection& _connection) const
{
  Q_UNUSED(_oidName);
  knowCore::Value value;

  if(knowCore::ValueIs<knowRDF::Literal> lit = _source)
  {
    value = lit; // Unpack the literal
  }
  else
  {
    value = _source;
  }

  QByteArray destination;
  destination.resize(calculateSize(value));
  QMutexLocker l(&Private::fieldsIOs.mutex);
  char* data = destination.begin();

  const std::size_t fields
    = _connection.d->kdb_rdf_value_type_enum_values.size() - 1; // without the enum
  BinaryInterface::write<quint32>(fields + 1, data);
  data += sizeof(quint32);
  cres_try(Oid oid, _connection.d->nameToOid("kdb_rdf_value_type"));
  BinaryInterface::write<Oid>(oid, data);
  data += sizeof(Oid); // enum oid

  QString enum_name = "null";
  int field_index = -1;

  for(int i = 1; i < _connection.d->kdb_rdf_value_type_enum_values.size(); ++i)
  {
    QString enum_name_i = _connection.d->kdb_rdf_value_type_enum_values.at(i);
    const FieldIO* fio = Private::fieldsIOs.fields[enum_name_i];
    if(fio and fio->accept(value))
    {
      field_index = i;
      enum_name = enum_name_i;
      break;
    }
  }
  if(field_index == -1)
  {
    auto const [success, literal, message] = value.toRdfLiteral();
    if(success)
    {
      knowCore::Value literal_value = knowCore::Value::fromValue(literal.value());
      for(int i = 1; i < _connection.d->kdb_rdf_value_type_enum_values.size(); ++i)
      {
        QString enum_name_i = _connection.d->kdb_rdf_value_type_enum_values.at(i);
        const FieldIO* fio = Private::fieldsIOs.fields[enum_name_i];
        if(fio and fio->accept(literal_value))
        {
          field_index = i;
          enum_name = enum_name_i;
          value = literal_value;
          break;
        }
      }
    }
    else
    {
      return cres_failure("Unsupported value '{}' in RDF Value with message '{}'", value, message.value());
    }
  }
  data = BinaryInterface::details::write_enum(qPrintable(enum_name), data);

  for(int i = 1; i < _connection.d->kdb_rdf_value_type_enum_values.size(); ++i)
  {
    QString enum_name_i = _connection.d->kdb_rdf_value_type_enum_values.at(i);
    const FieldIO* fio = Private::fieldsIOs.fields.value(enum_name_i, nullptr);
    clog_assert(fio);
    cres_try(Oid oid, _connection.d->nameToOid(fio->typeName()));

    if(i != field_index)
    {
      data = BinaryInterface::details::write_empty(oid, data);
    }
    else
    {
      if(fio)
      {
        quint32 s = fio->calculateSize(value);
        BinaryInterface::write<Oid>(oid, data);
        data += sizeof(Oid);
        BinaryInterface::write<quint32>(s, data);
        data += sizeof(quint32);
        fio->write(data, value, _connection);
        data += s;
      }
      else
      {
        return cres_failure("Attempt at writting a field whose binary interface is not loaded!");
      }
    }
  }
  clog_assert(data == destination.end());

  return cres_success(destination);
}

cres_qresult<void> RDFValueBinaryMarshal::setupIndexes(const TripleStore& _store,
                                                       const Transaction& _transaction)
{
  knowDBC::Query q
    = _transaction.createSQLQuery("SELECT name, features FROM kdb_rdf_value_definitions");
  knowDBC::Result r = q.execute();
  if(r)
  {
    for(int i = 0; i < r.tuples(); ++i)
    {
      QString name = r.value<QString>(i, 0).expect_success();
      int features = r.value<int>(i, 1).expect_success();
      if(features & (quint64)Feature::BtreeIndexable)
      {
        cres_try(cres_ignore, SQLInterface::createIndex(_transaction, _store, name, "btree"),
                 on_failure(_transaction.rollback()));
      }
      if(features & (quint64)Feature::GistIndexable)
      {
        cres_try(cres_ignore, SQLInterface::createIndex(_transaction, _store, name, "gist"),
                 on_failure(_transaction.rollback()));
      }
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to get fields", r);
  }
  return cres_success();
}

cres_qresult<void> RDFValueBinaryMarshal::finishInitialisation()
{
  Transaction transaction(d->connection);
  cres_try(cres_ignore, d->init_fields_extra(transaction));
  cres_try(cres_ignore, transaction.commit());
  return cres_success();
}
