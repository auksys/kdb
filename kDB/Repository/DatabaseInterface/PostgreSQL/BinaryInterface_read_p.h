#include <arpa/inet.h>

#include <knowCore/Timestamp.h>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include "BinaryInterfaceTimeStamp_p.h"
#include <clog_qt>
#include <knowCore/Array.h>
#include <knowCore/BigNumber.h>
#include <knowCore/Vector.h>

namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface
{
  template<typename _T_>
  struct read_details;

  template<typename _T_>
  inline _T_ read(const char* data, int _size)
  {
    return read_details<_T_>::do_read(data, _size);
  }

  template<>
  inline qint64 read<qint64>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(qint64));
    qint32 h32 = *(qint32*)data;
    qint32 l32 = *(qint32*)(data + 4);

    qint64 result;
    result = ntohl(h32);
    result <<= 32;
    result |= ntohl(l32);

    return result;
  }
  template<>
  inline quint64 read<quint64>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(quint64));
    return read<qint64>(data, _size);
  }
  template<>
  inline qint32 read<qint32>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(qint32));
    return ntohl(*(qint32*)data);
  }
  template<>
  inline quint32 read<quint32>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(quint32));
    return read<qint32>(data, _size);
  }
  template<>
  inline qint16 read<qint16>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(qint16));
    return ntohs(*(qint16*)data);
  }
  template<>
  inline quint16 read<quint16>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(quint16));
    return read<qint16>(data, _size);
  }
  template<>
  inline char read<char>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(char));
    return *data;
  }
  template<>
  inline bool read<bool>(const char* data, int _size)
  {
    clog_assert(_size == 1);
    return *data != 0;
  }
  template<>
  inline float read<float>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(float));
    union
    {
      float f;
      qint32 i;
    } swap;
    swap.i = read<qint32>(data, _size);
    return swap.f;
  }
  template<>
  inline double read<double>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(double));
    union
    {
      double f;
      qint64 i;
    } swap;
    swap.i = read<qint64>(data, _size);
    return swap.f;
  }

  template<>
  inline knowCore::Timestamp read<knowCore::Timestamp>(const char* data, int _size)
  {
    clog_assert(_size == sizeof(qint64));
    qint64 timestamp = read<qint64>(data, _size);
    fsec_t fsec;
    struct pg_tm tt;
    if(BinaryInterface::timestamp2tm(timestamp, &tt, &fsec, NULL) == 0)
    {
      return knowCore::Timestamp::from(
        QDateTime(QDate(tt.tm_year, tt.tm_mon, tt.tm_mday),
                  QTime(tt.tm_hour, tt.tm_min, tt.tm_sec, fsec / 1000)));
    }
    else
    {
      return knowCore::Timestamp();
    }
  }

  template<>
  inline QString read<QString>(const char* data, int _size)
  {
    return QString::fromUtf8(data, _size);
  }

  //       template<>
  //       inline QStringList read<QStringList>(const char* data, int _size)
  //       {
  //         return read<QList<QString>>(data, _size);
  //       }
  template<>
  inline QByteArray read<QByteArray>(const char* data, int _size)
  {
    return QByteArray(data, _size);
  }

  template<>
  inline knowCore::BigNumber read<knowCore::BigNumber>(const char* data, int _size)
  {
    Q_UNUSED(_size);
    quint16 ndigits = BinaryInterface::read<quint16>(data, 2);
    data += 2;
    quint16 weight = BinaryInterface::read<quint16>(data, 2);
    data += 2;
    quint16 sign = BinaryInterface::read<quint16>(data, 2);
    data += 2;
    knowCore::BigNumber::Sign sign_bn;
    switch(sign)
    {
    case 0x4000: // NUMERIC_NEG
      sign_bn = knowCore::BigNumber::Sign::Negative;
      break;
    case 0x0000: // NUMERIC_POS
      sign_bn = knowCore::BigNumber::Sign::Positive;
      break;
    case 0xF000: // NUMERIC_NEG
      sign_bn = knowCore::BigNumber::Sign::NegativeInfinite;
      break;
    case 0xD000: // NUMERIC_POS
      sign_bn = knowCore::BigNumber::Sign::PositiveInfinite;
      break;
    default:
      sign_bn = knowCore::BigNumber::Sign::NaN;
      break;
    }
    qint16 dscale = BinaryInterface::read<quint16>(data, 2);
    data += 2;
    QList<quint16> digits;
    digits.reserve(ndigits);
    for(int i = 0; i < ndigits; ++i)
    {
      digits.append(BinaryInterface::read<quint16>(data, 2));
      data += 2;
    }
    return knowCore::BigNumber(weight, sign_bn, dscale, digits);
  }

  template<typename _T_>
  struct read_details<knowCore::Array<_T_>>
  {
    static knowCore::Array<_T_> do_read(const char* data, int _size)
    {
      Q_UNUSED(_size);

      qint32 ndim = read<qint32>(data, 4);
      data += 4;

      if(ndim == 0)
        return knowCore::Array<_T_>();

      qint32 has_null = read<qint32>(data, 4);
      data += 4;
      Oid type = read<Oid>(data, 4);
      data += 4;
      Q_UNUSED(has_null);
      Q_UNUSED(type);

      Q_UNUSED(ndim);

      QList<qsizetype> dimensions;
      int total_dim = 1;
      for(int i = 0; i < ndim; ++i)
      {
        qint32 dim = read<qint32>(data, 4);
        data += 4;
        qint32 lbound = read<qint32>(data, 4);
        data += 4;
        Q_UNUSED(lbound);
        dimensions.append(dim);
        total_dim *= dim;
      }

      Q_UNUSED(total_dim);
      QVector<_T_> r_data;
      r_data.reserve(total_dim);

      for(int i = 0; i < total_dim; ++i)
      {
        std::size_t s = read<qint32>(data, 4);
        ;
        data += 4;
        r_data.append(read<_T_>(data, s));
        data += s;
      }
      return knowCore::Array<_T_>(r_data, dimensions);
    }
  };
  template<typename _T_>
  struct read_details<QList<_T_>>
  {
    static QList<_T_> do_read(const char* _data, int _size)
    {
      return read<knowCore::Array<_T_>>(_data, _size).toList().expect_success();
    }
  };
  // Json
  template<>
  inline QJsonArray read<QJsonArray>(const char* _data, int _size)
  {
    Q_UNUSED(_data);
    Q_UNUSED(_size);
    qFatal("should not called");
  }
  template<>
  inline QJsonObject read<QJsonObject>(const char* _data, int _size)
  {
    Q_UNUSED(_data);
    Q_UNUSED(_size);
    qFatal("should not called");
  }
  template<>
  inline QJsonValue read<QJsonValue>(const char* _data, int _size)
  {
    QByteArray doc_data(_data, _size);
    QJsonDocument doc = QJsonDocument::fromJson(doc_data);
    if(doc.isEmpty() or doc.isNull())
    {
      if(_size > 0)
      {
        return QJsonValue(QString::fromUtf8(doc_data));
      }
      else
      {
        return QJsonValue();
      }
    }
    else if(doc.isArray())
    {
      return doc.array();
    }
    else if(doc.isObject())
    {
      return doc.object();
    }
    return QJsonValue();
  }
  namespace details
  {
    template<int _N_, typename... T>
    struct struct_reader
    {
      static void do_read(std::tuple<T...>* data, const char* ptr)
      {
        ptr += 4; // skip oid TODO find a way to validate
        quint32 s = read<quint32>(ptr, 4);
        ptr += 4;
        std::get<sizeof...(T) - _N_>(*data)
          = read<std::remove_reference_t<decltype(std::get<sizeof...(T) - _N_>(*data))>>(ptr, s);
        struct_reader<_N_ - 1, T...>::do_read(data, ptr + s);
      }
    };
    template<typename... T>
    struct struct_reader<0, T...>
    {
      static void do_read(std::tuple<T...>* data, const char* ptr)
      {
        Q_UNUSED(data);
        Q_UNUSED(ptr);
      }
    };
  } // namespace details
  template<typename... T>
  struct struct_reader
  {
    struct_reader() {}

    void read(const char* ptr)
    {
      quint32 f = BinaryInterface::read<quint32>(ptr, 4);
      clog_assert(f == sizeof...(T));
      ptr += 4;
      details::struct_reader<sizeof...(T), T...>::do_read(&data, ptr);
    }

    std::tuple<T...> data;
  };

  template<typename... T>
  struct read_details<std::tuple<T...>>
  {
    static std::tuple<T...> do_read(const char* data, int /*_size*/)
    {
      struct_reader<T...> sr;
      sr.read(data);
      return sr.data;
    }
  };
} // namespace kDB::Repository::DatabaseInterface::PostgreSQL::BinaryInterface
