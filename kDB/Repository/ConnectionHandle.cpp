#include "ConnectionHandle_p.h"

#include "Connection_p.h"
#include <clog_qt>

#include "DatabaseInterface/PostgreSQL/Logging.h"

using namespace kDB::Repository;

ConnectionHandle::ConnectionHandle(const Connection& _cp, bool _force) : m_cp(_cp)
{
  if(m_cp.isConnected() or _force)
  {
    QMutexLocker l(&m_cp.d->connections_lock);
    while(m_conn == nullptr and not m_cp.d->available_connections.isEmpty())
    {
      m_conn = m_cp.d->available_connections.front();
      m_cp.d->available_connections.pop_front();
      PQexec(m_conn, ""); // this force checking if the connection is still valid
      if(PQstatus(m_conn) != CONNECTION_OK)
      {
        PQfinish(m_conn);
        m_cp.d->connections.removeAll(m_conn);
        m_conn = nullptr;
      }
    }
    if(m_conn == nullptr)
    {
      QString conninfo = clog_qt::qformat("host={} port={} dbname={} user=kdb", m_cp.d->host,
                                          m_cp.d->port, m_cp.d->database);
      m_conn = PQconnectdb(qPrintable(conninfo));
      if(PQstatus(m_conn) != CONNECTION_OK)
      {
        QString errMsg = PQerrorMessage(m_conn);
        clog_error("Failed to connect to {}:{}, {}: {} {}", m_cp.d->host, m_cp.d->port,
                   m_cp.d->database, PQstatus(m_conn), errMsg);
      }
      else
      {
        m_cp.d->connections.append(m_conn);
      }
    }
  }
  else
  {
    clog_error("Attempting to use a disconnected connection");
    m_conn = nullptr;
  }
}

ConnectionHandle::~ConnectionHandle() { release(); }

void ConnectionHandle::release()
{
  if(m_conn)
  {
    QMutexLocker l(&m_cp.d->connections_lock);
    m_cp.d->available_connections.append(m_conn);
    m_conn = nullptr;
  }
}
