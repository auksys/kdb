#include <knowCore/Global.h>

#include "RDFDataset.h"

namespace kDB::Repository
{
  /**
   * Union of datasets
   */
  class DatasetsUnion : public RDFDataset
  {
    friend class GraphsManager;
  public:
    DatasetsUnion();
    DatasetsUnion(const DatasetsUnion& _rhs);
    DatasetsUnion& operator=(const DatasetsUnion& _rhs);
    ~DatasetsUnion();
    template<typename... _T_>
    DatasetsUnion(_T_... _t) : DatasetsUnion()
    {
      add(_t...);
    }
    DatasetsUnion operator||(const RDFDataset& _graph) const;
    QList<RDFDataset> datasets() const;
    QHash<QString, RDFDataset> namedDatasets() const;
    template<typename... _T_>
    void add(const RDFDataset& _graph, _T_... _t)
    {
      add(_graph);
      add(_t...);
    }
    template<typename... _T_>
    void add(const knowCore::Uri& _uri, const RDFDataset& _graph, _T_... _t)
    {
      add(_uri, _graph);
      add(_t...);
    }
    void add(const RDFDataset& _graph);
    void add(const QList<RDFDataset>& _graphs);
    void add(const knowCore::Uri& _uri, const RDFDataset& _graph);
  private:
    KNOWCORE_D_DECL();
  };
} // namespace kDB::Repository
