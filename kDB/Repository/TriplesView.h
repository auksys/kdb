#include <QSharedDataPointer>
#include <QSharedPointer>

#include <knowCore/Global.h>

#include "RDFDataset.h"

namespace kDB
{
  namespace RDFView
  {
    class ViewDefinition;
  }
  namespace Repository
  {
    class Connection;
    class GraphsManager;
    class TriplesView : public RDFDataset
    {
      friend class GraphsManager;
      TriplesView(const Connection& _connection, const kDB::RDFView::ViewDefinition& _definition);
    public:
      TriplesView();
      TriplesView(const TriplesView& _rhs);
      TriplesView& operator=(const TriplesView& _rhs);
      ~TriplesView();
      kDB::RDFView::ViewDefinition viewDefinition() const;
      bool updateView();
      QString tablename() const;
    private:
      KNOWCORE_D_DECL();
    };
  } // namespace Repository
} // namespace kDB
