#pragma once

#define KDB_REPOSITORY_EXTENSION_EXPORT(_LIBRARY_)                                                 \
  extern "C" cres_qresult<void> _LIBRARY_##_initialise(                                            \
    const ::kDB::Repository::Connection& _connection)                                              \
  {                                                                                                \
    return _LIBRARY_::initialise(_connection);                                                     \
  }
