#ifndef _KDB_REPOSITORY_ABSTRACTTRIPLESGRAPH_H_
#define _KDB_REPOSITORY_ABSTRACTTRIPLESGRAPH_H_

#include <QExplicitlySharedDataPointer>

#include <knowCore/ValueHash.h>

#include "RDFEnvironment.h"

namespace kDB::Repository
{
  class RDFDataset
  {
  protected:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  public:
    enum class Type
    {
      Invalid,
      Empty,
      TripleStore,
      TriplesView,
      DatasetsUnion,
      PersistentDatasetsUnion
    };
  protected:
    RDFDataset(Private* _private);
  public:
    RDFDataset();
    RDFDataset(const RDFDataset& _rhs);
    RDFDataset& operator=(const RDFDataset& _rhs);
    virtual ~RDFDataset();
    /**
     * @return the uri used to reference this dataset
     */
    knowCore::Uri uri() const;
    /**
     * @return the type of dataset
     */
    Type type() const;
    /**
     * @return true if valid
     */
    bool isValid() const;
    DatasetsUnion toDatasetsUnion() const;
    PersistentDatasetsUnion toPersistentDatasetsUnion() const;
    TripleStore toTripleStore() const;
    TriplesView toTriplesView() const;
    Connection connection() const;
    knowDBC::Query createSPARQLQuery(const RDFEnvironment& _environment = RDFEnvironment(),
                                     const QString& _query = QString(),
                                     const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                     const knowCore::ValueHash& _options
                                     = knowCore::ValueHash()) const;
  };
} // namespace kDB::Repository

#include <knowCore/Formatter.h>

clog_format_declare_enum_formatter(kDB::Repository::RDFDataset::Type, Invalid, Empty, TripleStore,
                                   TriplesView, DatasetsUnion, PersistentDatasetsUnion);

#endif
