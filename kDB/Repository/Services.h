#pragma once

#include <QSharedPointer>
#include <kDB/Forward.h>
#include <typeindex>

namespace kDB::Repository
{
  /**
   * Handle \ref AbstractService.
   */
  class Services
  {
  public:
    Services();
    Services(const Services& _rhs);
    Services operator=(const Services& _rhs);
    ~Services();
    /**
     * Add a \p _service , a higher \p _priority is used if two different \ref AbstractService can
     * answer a call, a higher value is tried first
     */
    template<typename _T_>
    void addService(_T_* _service, int _priority = 0)
    {
      addService(_service, _priority, typeid(_T_));
    }
    /**
     * Execute the query \p _query on the service \p _service using the \p _bindings
     * \return the result of the query or an error if the service cannot be called
     */
    knowDBC::Result call(const knowCore::Uri& _service, const QString& _query,
                         const knowCore::ValueHash& _bindings) const;
    /**
     * Remove all the services with a given type
     */
    template<typename _T_>
    void removeAllServices()
    {
      removeAllServices(typeid(_T_));
    }
  private:
    void addService(AbstractService* _service, int _priority, const std::type_index& _type);
    void removeAllServices(const std::type_index& _type);
  private:
    struct Private;
    QSharedPointer<Private> d;
  };
} // namespace kDB::Repository
