#include "SPARQLFunctionDefinition.h"

#include <knowCore/Uri.h>

using namespace kDB::Repository;

struct SPARQLFunctionDefinition::Private : public QSharedData
{
  knowCore::Uri sparql_name;
  QString sql_name;
  knowCore::Uri return_type;
  QList<knowCore::Uri> arguments;
};

SPARQLFunctionDefinition::SPARQLFunctionDefinition() : d(nullptr) {}

SPARQLFunctionDefinition::SPARQLFunctionDefinition(const knowCore::Uri& _sparql_name,
                                                   const QString& _sql_name,
                                                   const knowCore::Uri& _return,
                                                   const QList<knowCore::Uri>& _arguments)
    : d(new Private)
{
  d->sparql_name = _sparql_name;
  d->sql_name = _sql_name;
  d->return_type = _return;
  d->arguments = _arguments;
}

SPARQLFunctionDefinition::SPARQLFunctionDefinition(const SPARQLFunctionDefinition& _rhs) : d(_rhs.d)
{
}

SPARQLFunctionDefinition& SPARQLFunctionDefinition::operator=(const SPARQLFunctionDefinition& _rhs)
{
  d = _rhs.d;
  return *this;
}

SPARQLFunctionDefinition::~SPARQLFunctionDefinition() {}

bool SPARQLFunctionDefinition::isValid() const { return d; }

QList<knowCore::Uri> SPARQLFunctionDefinition::arguments() const { return d->arguments; }

knowCore::Uri SPARQLFunctionDefinition::returnType() const { return d->return_type; }

knowCore::Uri SPARQLFunctionDefinition::sparqlName() const { return d->sparql_name; }

QString SPARQLFunctionDefinition::sqlTemplate() const { return d->sql_name; }

bool SPARQLFunctionDefinition::operator==(const SPARQLFunctionDefinition& _rhs) const
{
  return d->sparql_name == _rhs.d->sparql_name and d->sql_name == _rhs.d->sql_name
         and d->return_type == _rhs.d->return_type and d->arguments == _rhs.d->arguments;
}
