#include "TestStore.h"

#include <clog_qt>
#include <knowCore/Test.h>

#include "../Connection.h"
#include "../Store.h"
#include "../TemporaryStore.h"
#include "../TripleStore.h"

#include <Cyqlops/Crypto/RSAAlgorithm.h>

namespace Repository = kDB::Repository;

void TestStore::testConnection()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  QVERIFY(store.store()->isControlling());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());
  QVERIFY(c.defaultTripleStore().expect_success().isValid());
  QUuid serverUuid = c.serverUuid();
  QByteArray signature = c.rsaAlgorithm().serialise();
  c = Repository::Connection();
  QVERIFY(signature.size() != 0);

  c = store.createConnection();
  CRES_QVERIFY(c.connect());

  QCOMPARE(c.serverUuid(), serverUuid);
  QCOMPARE(signature, c.rsaAlgorithm().serialise());

  CRES_QVERIFY(store.stop());
}

QTEST_MAIN(TestStore)
#include "TestStore.moc"
