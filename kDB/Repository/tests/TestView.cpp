#include "TestView.h"

#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>

#include <knowDBC/Query.h>
#include <knowRDF/Triple.h>

#include <kDB/RDFView/ViewDefinition.h>

#include "../Connection.h"
#include "../GraphsManager.h"
#include "../TemporaryStore.h"
#include "../TriplesView.h"

#include "../DatasetsUnion.h"
#include "CompareSPARQLResults.h"

namespace Repository = kDB::Repository;
namespace RDF = knowRDF;
namespace RDFView = kDB::RDFView;

void TestView::testView()
{
  // Create store
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  // Create table
  knowDBC::Query q = c.createSQLQuery();
  q.setOption(knowDBC::Query::OptionsKeys::MultiQueries, true);
  QFile sqlFile(":/views.sql");
  sqlFile.open(QIODevice::ReadOnly);
  q.setQuery(sqlFile.readAll());
  QVERIFY(q.execute());

  QFile file1(":/emp.sml");
  RDFView::ViewDefinition d1 = RDFView::ViewDefinition::parse(&file1);
  QFile file2(":/dept.sml");
  RDFView::ViewDefinition d2 = RDFView::ViewDefinition::parse(&file2);

  QVERIFY(d1.isValid());
  QCOMPARE(d1.triples().size(), 4);
  QVERIFY(d2.isValid());
  QCOMPARE(d2.triples().size(), 4);

  CRES_QVERIFY(c.graphsManager()->createView(d1));
  CRES_QVERIFY(c.graphsManager()->createView(d2));

  c.disconnect();
  c = store.createConnection();
  CRES_QVERIFY(c.connect());
  QCOMPARE(c.graphsManager()->triplesViews().size(), 2);

  QCOMPARE(d1.toString(),
           CRES_QVERIFY(c.graphsManager()->getTriplesView(knowCore::Uri("http://ex.org/employee")))
             .viewDefinition()
             .toString());
  QCOMPARE(d2.toString(), CRES_QVERIFY(c.graphsManager()->getTriplesView(
                                         knowCore::Uri("http://ex.org/department")))
                            .viewDefinition()
                            .toString());

  Repository::DatasetsUnion employee_department(
    CRES_QVERIFY(c.graphsManager()->getTriplesView(knowCore::Uri("http://ex.org/employee"))),
    CRES_QVERIFY(c.graphsManager()->getTriplesView(knowCore::Uri("http://ex.org/department"))));

  knowDBC::Query sq = employee_department.createSPARQLQuery();
  sq.setQuery("SELECT ?x ?y ?z WHERE { ?x ?y ?z . };");
  knowDBC::Result r = sq.execute();
  QVERIFY(r);
  QCOMPARE(r.tuples(), 20);

  QFile test(":/views_all.json");
  knowDBC::Result r2 = CRES_QVERIFY(knowDBC::Result::read(&test));

  QCOMPARE(r.tuples(), r2.tuples());
  QCOMPARE(r.fields(), r2.fields());

  QVERIFY(compare_tuples(r, r2));

  QFile f(":/views_all.srx");

  knowDBC::Result r3 = CRES_QVERIFY(knowDBC::Result::read(&f, knowCore::FileFormat::SRX));
  //   QVERIFY(r3.read(&f, nullptr, "srx"));
  QCOMPARE(r.tuples(), r3.tuples());
  QCOMPARE(r.fields(), r3.fields());

  QVERIFY(compare_tuples(r, r3));

  sq.setQuery("PREFIX ex: <http://ex.org/> SELECT ?x ?age ?w ?l WHERE {?x ex:age ?age . ?x "
              "ex:department ?w . ?w ex:location ?l. };");
  r = sq.execute();
  QVERIFY(r);
  QCOMPARE(r.tuples(), 3);
}

QTEST_MAIN(TestView)
