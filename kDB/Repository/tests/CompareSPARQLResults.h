#include <QDebug>

#include <clog_print>
#include <clog_qt>

#include <knowCore/Uris/xsd.h>
#include <knowDBC/Result.h>
#include <knowRDF/BlankNode.h>
#include <knowRDF/Graph.h>
#include <knowRDF/Literal.h>
#include <knowRDF/Node.h>

namespace
{
  namespace details
  {
    bool comparable_special(const knowCore::Value& _v1, const knowCore::Value& _v2)
    {
      auto const& [s, v, m] = _v1.compare(_v2, knowCore::ComparisonOperator::AlmostEqual);
      return s and v.value();
    }
  } // namespace details
  void printFields(const knowDBC::Result& r)
  {
    clog_print<clog_print_flag::bold>("Fields: ");
    for(int j = 0; j < r.fields(); ++j)
    {
      clog_print("{}", r.fieldName(j));
    }
  }
  void printResults(const knowDBC::Result& r)
  {
    for(int j = 0; j < r.tuples(); ++j)
    {
      clog_print<clog_print_flag::bold | clog_print_flag::nonewline>("{}th: ", j);
      for(int k = 0; k < r.fields(); ++k)
      {
        clog_print<clog_print_flag::nonewline>("{} ", r.value(j, k));
      }
      clog_print("");
    }
  }
  void printAll(const knowDBC::Result& r1, const knowDBC::Result& r2)
  {
    clog_print<clog_print_flag::blue>("Possible reference values were");
    printFields(r2);
    printResults(r2);
    clog_print<clog_print_flag::red>("Tested values were");
    printFields(r1);
    printResults(r1);
  }
  inline bool compare_tuples(const knowDBC::Result& r1, const knowDBC::Result& r2)
  {
    QVector<bool> indexes(r2.tuples(), false);
    for(int i = 0; i < r1.tuples(); ++i)
    {
      bool found_one = false;
      for(int j = 0; j < r2.tuples(); ++j)
      {
        if(not indexes[j])
        {
          bool equal_tuple = true;
          for(int k = 0; k < r1.fields(); ++k)
          {
            knowCore::Value val1 = r1.value(i, k);
            knowCore::Value val2 = r2.value(j, r2.fieldIndex(r1.fieldName(k)));
            if(val1 != val2
               and (not knowCore::ValueIs<knowRDF::BlankNode>(val1)
                    or not knowCore::ValueIs<knowRDF::BlankNode>(val2))
               and not details::comparable_special(val1, val2))
            {
              equal_tuple = false;
              break;
            }
          }
          if(equal_tuple)
          {
            found_one = true;
            indexes[j] = true;
            break;
          }
        }
      }
      if(not found_one)
      {
        {
          clog_print<clog_print_flag::red | clog_print_flag::nonewline>(
            "Error: No match for {}th tuple: ", i);
          for(int k = 0; k < r1.fields(); ++k)
          {
            clog_print<clog_print_flag::red | clog_print_flag::nonewline>("{} ", r1.value(i, k));
          }
          clog_print("");
        }
        printAll(r1, r2);
        return false;
      }
    }
    return true;
  }
  namespace details
  {
    inline void print_one(const knowRDF::Node* node)
    {
      switch(node->type())
      {
      case knowRDF::Node::Type::Undefined:
        clog_print<clog_print_flag::nonewline>("undefined");
        break;
      case knowRDF::Node::Type::Uri:
        clog_print<clog_print_flag::nonewline>("{}", node->uri());
        break;
      case knowRDF::Node::Type::BlankNode:
        clog_print<clog_print_flag::nonewline>("{}", node->blankNode());
        break;
      case knowRDF::Node::Type::Literal:
        clog_print<clog_print_flag::nonewline>("{}", node->literal());
        break;
      case knowRDF::Node::Type::Variable:
        clog_print<clog_print_flag::nonewline>("{}", node->variable());
        break;
      }
    }
    inline void print(const knowRDF::Node* node)
    {
      QMultiHash<knowCore::Uri, const knowRDF::Node*> children = node->children();
      clog_print<clog_print_flag::nonewline>("{{");
      print_one(node);
      clog_print<clog_print_flag::nonewline>(" -> ");
      for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::const_iterator it = children.begin();
          it != children.end(); ++it)
      {
        clog_print<clog_print_flag::nonewline>("({})", it.key());
        print_one(it.value());
        clog_print<clog_print_flag::nonewline>(")");
      }
      clog_print("}}");
    }
    inline bool similar(const knowRDF::Node* node1, const knowRDF::Node* node2)
    {
      if(node1->type() == node2->type())
      {
        switch(node1->type())
        {
        case knowRDF::Node::Type::Undefined:
          return true;
        case knowRDF::Node::Type::Uri:
          return node1->uri() == node2->uri();
        case knowRDF::Node::Type::BlankNode:
          return true;
        case knowRDF::Node::Type::Literal:
          return node1->literal() == node2->literal();
        case knowRDF::Node::Type::Variable:
          clog_fatal("Impossible");
        }
      }
      return false;
    }
    QList<QPair<knowCore::Uri, const knowRDF::Node*>>
      toPairList(const QMultiHash<knowCore::Uri, const knowRDF::Node*>& _hash)
    {
      QList<QPair<knowCore::Uri, const knowRDF::Node*>> r;
      for(QMultiHash<knowCore::Uri, const knowRDF::Node*>::const_iterator it = _hash.begin();
          it != _hash.end(); ++it)
      {
        r.append(QPair<knowCore::Uri, const knowRDF::Node*>(it.key(), it.value()));
      }
      return r;
    }
  } // namespace details
  inline bool compare_graphs(const knowRDF::Graph* _graph_1, const knowRDF::Graph* _graph_2)
  {
    QList<const knowRDF::Node*> nodes_1 = _graph_1->nodes();
    QList<const knowRDF::Node*> nodes_2 = _graph_2->nodes();

    QVector<bool> indexes(nodes_2.size(), false);

    for(int i = 0; i < nodes_1.size(); ++i)
    {
      bool found_one = false;
      const knowRDF::Node* node1 = nodes_1[i];
      for(int j = 0; j < nodes_2.size(); ++j)
      {
        const knowRDF::Node* node2 = nodes_2[j];
        if(not indexes[j])
        {
          if(details::similar(node1, node2)
             and node1->children().size() == node2->children().size())
          {
            QList<QPair<knowCore::Uri, const knowRDF::Node*>> children1
              = details::toPairList(node1->children());
            QList<QPair<knowCore::Uri, const knowRDF::Node*>> children2
              = details::toPairList(node2->children());
            QVector<bool> indexes_children(children2.size(), false);

            bool all_children_found = true;

            for(int k = 0; k < children1.size(); ++k)
            {
              bool found_one_child = false;
              QPair<knowCore::Uri, const knowRDF::Node*> c1 = children1[k];
              for(int l = 0; l < children2.size(); ++l)
              {
                if(not indexes_children[l])
                {
                  QPair<knowCore::Uri, const knowRDF::Node*> c2 = children2[l];
                  if(c1.first == c2.first and details::similar(c1.second, c2.second))
                  {
                    found_one_child = true;
                    indexes_children[l] = true;
                    break;
                  }
                }
              }
              if(not found_one_child)
              {
                all_children_found = false;
              }
            }

            if(all_children_found)
            {
              found_one = true;
              break;
            }
          }
        }
      }
      if(not found_one)
      {
        {
          clog_print<clog_print_flag::red | clog_print_flag::nonewline>(
            "Error: No match for {}th node: ", i);
          details::print(node1);
          clog_print("");
        }
        clog_print<clog_print_flag::blue>("Possible reference values were");
        for(int j = 0; j < nodes_2.size(); ++j)
        {
          clog_print<clog_print_flag::bold>("{}th: ", j);
          details::print(nodes_2[j]);
          clog_print("");
        }
        clog_print<clog_print_flag::red>("Tested values were");
        for(int j = 0; j < nodes_1.size(); ++j)
        {
          clog_print<clog_print_flag::bold>("{}th: ", j);
          details::print(nodes_1[j]);
          clog_print("");
        }
        while(true)
        {
        }
        return false;
      }
    }

    if(nodes_1.size() != nodes_2.size())
    {
      clog_print<clog_print_flag::red>("Different test results got: {} expecting: ", nodes_1.size(),
                                       nodes_2.size());
      return false;
    }

    return true;
  }
} // namespace
