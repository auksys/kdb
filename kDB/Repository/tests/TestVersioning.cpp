#include "TestVersioning.h"

#include <random>

#include <Cyqlops/Crypto/RSAAlgorithm.h>
#include <QThread>

#include <knowCore/Timestamp.h>
#include <knowCore/Uris/xsd.h>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Literal.h>

#include <kDB/Repository/Utils.h>

#include "../Connection.h"
#include "../DatabaseInterface/PostgreSQL/SQLInterface_p.h"
#include "../GraphsManager.h"
#include "../QueryConnectionInfo.h"
#include "../TemporaryStore.h"
#include "../Transaction.h"
#include "../TripleStore.h"

#include "../VersionControl/Delta.h"
#include "../VersionControl/Manager.h"
#include "../VersionControl/RevisionBuilder.h"
#include "../VersionControl/Signature.h"

#include "../Test.h"
#include "CompareRDFTriples.h"

using namespace kDB;
namespace RVC = kDB::Repository::VersionControl;

#define COMPARE_TRIPLES_STORES(_TS1_, _TS2_, _REV_COUNT_, _HEADS_COUNT_, _TRIPLES_COUNT_)          \
  QVERIFY(CRES_QVERIFY(_TS2_.versionControlManager()->hasRevision(                                 \
    CRES_QVERIFY(_TS1_.versionControlManager()->tipHash()))));                                     \
  QCOMPARE(CRES_QVERIFY(_TS2_.versionControlManager()->revisions()).size(), _REV_COUNT_);          \
  QVERIFY(CRES_QVERIFY(_TS2_.versionControlManager()->hasRevision(                                 \
    CRES_QVERIFY(_TS1_.versionControlManager()->tipHash()))));                                     \
  QVERIFY(CRES_QVERIFY(_TS2_.versionControlManager()->canFastForward(                              \
    CRES_QVERIFY(_TS1_.versionControlManager()->tipHash()))));                                     \
  QCOMPARE(CRES_QVERIFY(_TS2_.versionControlManager()->heads()).size(), _HEADS_COUNT_);            \
  CRES_QVERIFY(_TS2_.versionControlManager()->fastForward(                                         \
    CRES_QVERIFY(_TS1_.versionControlManager()->tipHash())));                                      \
  QCOMPARE(CRES_QVERIFY(_TS2_.versionControlManager()->heads()).size(), _HEADS_COUNT_);            \
  QVERIFY(CRES_QVERIFY(_TS1_.versionControlManager()->tipHash())                                   \
          == CRES_QVERIFY(_TS2_.versionControlManager()->tipHash()));                              \
  QCOMPARE(CRES_QVERIFY(_TS1_.versionControlManager()->tip()).contentHash(), _TS1_.contentHash()); \
  QCOMPARE(CRES_QVERIFY(_TS2_.versionControlManager()->tip()).contentHash(), _TS2_.contentHash()); \
  QCOMPARE(CRES_QVERIFY(_TS1_.triples()).size(), _TRIPLES_COUNT_);                                 \
  QCOMPARE(CRES_QVERIFY(_TS2_.triples()).size(), _TRIPLES_COUNT_);                                 \
  QVERIFY(compare_triples(CRES_QVERIFY(_TS1_.triples()), CRES_QVERIFY(_TS2_.triples())));

void TestVersioning::testEnablingVersioning()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());
  Repository::Connection c2 = store.createConnection();
  CRES_QVERIFY(c2.connect());

  Repository::TripleStore triplesStore = CRES_QVERIFY(c.defaultTripleStore());
  Repository::TripleStore triplesStore3 = CRES_QVERIFY(c.defaultTripleStore());

  QVERIFY(not triplesStore.options().testFlag(Repository::TripleStore::Option::Versioning));
  CRES_QVERIFY(triplesStore.enableVersioning());
  QCOMPARE(triplesStore.versionControlManager()->tipHash(), RVC::Revision::initialHash());
  QVERIFY(triplesStore.options().testFlag(Repository::TripleStore::Option::Versioning));

  Repository::TripleStore triplesStore2 = CRES_QVERIFY(c2.defaultTripleStore());
  for(int i = 0; i < 10; ++i)
  { // it can take some time to propagate
    if(triplesStore2.options().testFlag(Repository::TripleStore::Option::Versioning))
    {
      break;
    }
    QThread::sleep(1);
  }
  QVERIFY(triplesStore2.options().testFlag(Repository::TripleStore::Option::Versioning));
  QVERIFY(triplesStore3.options().testFlag(Repository::TripleStore::Option::Versioning));

  CRES_QVERIFY(triplesStore.disableVersioning());
  QVERIFY(not triplesStore.options().testFlag(Repository::TripleStore::Option::Versioning));
  QVERIFY(not triplesStore3.options().testFlag(Repository::TripleStore::Option::Versioning));
  QThread::sleep(1);
  QVERIFY(not triplesStore2.options().testFlag(Repository::TripleStore::Option::Versioning));
}

void TestVersioning::testEnablingVersioningInitialRevision()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore = CRES_QVERIFY(c.defaultTripleStore());
  triplesStore.insert(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));

  QVERIFY(not triplesStore.options().testFlag(Repository::TripleStore::Option::Versioning));
  CRES_QVERIFY(triplesStore.enableVersioning());
  QVERIFY(triplesStore.options().testFlag(Repository::TripleStore::Option::Versioning));

  QList<RVC::Revision> revisions = CRES_QVERIFY(triplesStore.versionControlManager()->revisions());
  QCOMPARE(revisions.size(), 2);

  QCOMPARE(revisions[0].hash().toHex(),
           QByteArray("cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85"
                      "f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"));
  QCOMPARE(revisions[0].historicity(), 0);
  QCOMPARE(revisions[0].deltas().size(), 0);

  QCOMPARE(revisions[1].hash(), triplesStore.versionControlManager()->tipHash());
  QCOMPARE(revisions[1].deltas().size(), 1);
  QCOMPARE(revisions[1].deltas()[0].parent(), RVC::Revision::initialHash());
  QVERIFY(CRES_QVERIFY(triplesStore.versionControlManager()->tip()).isValid());
  // TODO check deltas and their signature
  // QVERIFY(c.rsaAlgorithm().validate(revisions[1].hash(), revisions[1].signature(),
  // Cyqlops::Crypto::Hash::Algorithm::RAW));

  Repository::TripleStore triplesStore_t2
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t2")));
  triplesStore_t2.enableVersioning();

  RVC::RevisionBuilder rb
    = triplesStore_t2.versionControlManager()->insertRevision(revisions[1].contentHash());
  for(const RVC::Delta& delta : revisions[1].deltas())
  {
    rb.addDelta(delta.parent(), delta.hash(), delta.delta(), delta.signatures());
  }
  {
    Repository::Transaction transaction(c);
    rb.insert(transaction, true);
    CRES_QVERIFY(transaction.commit());
  }

  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 2);

  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);
  CRES_QVERIFY(triplesStore_t2.versionControlManager()->fastForward(revisions[1].hash()));
  QCOMPARE(triplesStore_t2.contentHash(),
           CRES_QVERIFY(triplesStore_t2.versionControlManager()->tip()).contentHash());
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);

  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 2);

  QList<knowRDF::Triple> ts = CRES_QVERIFY(triplesStore_t2.triples());
  QCOMPARE(ts.size(), 1);
  QCOMPARE(ts[0].subject().uri(), knowCore::Uri("a"));
  QCOMPARE(ts[0].predicate(), knowCore::Uri("b"));
  QCOMPARE(ts[0].object().uri(), knowCore::Uri("c"));
}

void TestVersioning::testInsertion()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore = CRES_QVERIFY(c.defaultTripleStore());
  CRES_QVERIFY(triplesStore.enableVersioning());

  QList<RVC::Revision> revisions = CRES_QVERIFY(triplesStore.versionControlManager()->revisions());
  QCOMPARE(revisions.size(), 1);
  QCOMPARE(revisions[0].hash().toHex(),
           QByteArray("cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85"
                      "f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"));
  QCOMPARE(revisions[0].historicity(), 0);
  QCOMPARE(revisions[0].deltas().size(), 0);
  RVC::Revision head = CRES_QVERIFY(triplesStore.versionControlManager()->tip());
  QCOMPARE(revisions[0].hash(), head.hash());
  QCOMPARE(revisions[0].historicity(), head.historicity());
  QCOMPARE(revisions[0].deltas().size(), head.deltas().size());

  knowCore::Timestamp before = knowCore::Timestamp::now();
  triplesStore.insert(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  knowCore::Timestamp after = knowCore::Timestamp::now();

  revisions = CRES_QVERIFY(triplesStore.versionControlManager()->revisions());
  QCOMPARE(revisions.size(), 2);
  QCOMPARE(revisions[0].hash().toHex(),
           QByteArray("cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85"
                      "f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"));
  QCOMPARE(revisions[0].deltas().size(), 0);

  QVERIFY(revisions[1].isSignedBy(c.serverUuid()));
  QCOMPARE(revisions[1].deltas().size(), 1);
  // TODO check delta signature
  RVC::Delta revisions_1_delta = revisions[1].deltas()[0];
  RVC::Signature revisions_1_signature = revisions_1_delta.signatures()[0];
  QVERIFY(c.rsaAlgorithm().validate(
    Cyqlops::Crypto::Hash::md5(
      c.serverUuid().toByteArray(),
      revisions_1_signature.timestamp().toEpoch<knowCore::NanoSeconds>().count(),
      revisions_1_delta.hash()),
    revisions_1_signature.signature(), Cyqlops::Crypto::Hash::Algorithm::RAW));

  head = CRES_QVERIFY(triplesStore.versionControlManager()->tip());
  QCOMPARE(revisions[1].hash(), head.hash());
  QCOMPARE(revisions[1].deltas().size(), head.deltas().size());

  RVC::Delta delta_0_1 = revisions[1].deltas().first();
  QCOMPARE(delta_0_1.parent(), revisions[0].hash());
  QCOMPARE(delta_0_1.child(), revisions[1].hash());
  QVERIFY(delta_0_1.delta().startsWith("\nINSERT DATA"));
  QVERIFY(delta_0_1.delta().size() > 16);

  // Add element in a transactions
  {
    Repository::Transaction t(c);
    triplesStore.insert(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"), t);
    triplesStore.insert(knowCore::Uri("g"), knowCore::Uri("h"), knowCore::Uri("i"), t);
    CRES_QVERIFY(t.commit());
  }
  revisions = CRES_QVERIFY(triplesStore.versionControlManager()->revisions());
  QCOMPARE(revisions.size(), 3);

  // Try to insert an existing element, it should not create a new revision
  triplesStore.insert(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"));
  revisions = CRES_QVERIFY(triplesStore.versionControlManager()->revisions());
  QCOMPARE(revisions.size(), 3);

  // Test tipHah with and without a transaction
  QByteArray arr = CRES_QVERIFY(triplesStore.versionControlManager()->tipHash());
  kDB::Repository::Transaction tiphash_transaction(c);
  QCOMPARE(arr, triplesStore.versionControlManager()->tipHash(tiphash_transaction));
  tiphash_transaction.rollback();
}

void TestVersioning::testRemoval()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore = CRES_QVERIFY(c.defaultTripleStore());
  CRES_QVERIFY(triplesStore.enableVersioning());

  // Insert some stuff

  triplesStore.insert(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));

  QList<RVC::Revision> revisions = CRES_QVERIFY(triplesStore.versionControlManager()->revisions());
  RVC::Revision rev_0 = revisions[0];
  RVC::Revision rev_1 = revisions[1];

  // Remove it
  knowCore::Timestamp before = knowCore::Timestamp::now();
  triplesStore.remove(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  QCOMPARE(CRES_QVERIFY(triplesStore.triples()).size(), 0);
  knowCore::Timestamp after = knowCore::Timestamp::now();

  revisions = CRES_QVERIFY(triplesStore.versionControlManager()->revisions());
  QCOMPARE(revisions.size(), 3);
  QCOMPARE(revisions[0], rev_0);
  QCOMPARE(revisions[1], rev_1);

  QVERIFY(revisions[2].isSignedBy(c.serverUuid()));
  // TODO re-instantiate those checks
  // QVERIFY(revisions[2].timestamp() > before);
  // QVERIFY(revisions[2].timestamp() < after);
  QCOMPARE(revisions[2].deltas().size(), 1);
  // QVERIFY(c.rsaAlgorithm().validate(revisions[2].hash(), revisions[2].signature(),
  // Cyqlops::Crypto::Hash::Algorithm::RAW));

  RVC::Revision head = CRES_QVERIFY(triplesStore.versionControlManager()->tip());
  QCOMPARE(revisions[2].hash(), head.hash());
  // QVERIFY(revisions[2].isSignedBy(head.author()));
  // QCOMPARE(revisions[2].timestamp(), head.timestamp());
  // QCOMPARE(revisions[2].deltas().size(), head.deltas().size());
  // QCOMPARE(revisions[2].signature(), head.signature());

  RVC::Delta delta_1_2 = revisions[2].deltas().first();
  QCOMPARE(delta_1_2.parent(), revisions[1].hash());
  QCOMPARE(delta_1_2.child(), revisions[2].hash());
  QVERIFY(delta_1_2.delta().startsWith("\nDELETE DATA"));
  QVERIFY(delta_1_2.delta().size() > 16);
}

void TestVersioning::testFastForwardOnlyInsertion()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore_t1
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());
  Repository::TripleStore triplesStore_t2
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t2")));
  CRES_QVERIFY(triplesStore_t2.enableVersioning());

  triplesStore_t1.versionControlManager()->listenNewRevision(
    [&triplesStore_t2](const RVC::Revision& _revision)
    {
      QVERIFY(
        not CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(_revision.hash())));
      RVC::RevisionBuilder rb
        = triplesStore_t2.versionControlManager()->insertRevision(_revision.contentHash());
      for(const RVC::Delta& delta : _revision.deltas())
      {
        QCOMPARE(delta.child(), _revision.hash());
        rb.addDelta(delta.parent(), delta.hash(), delta.delta(), delta.signatures());
      }
      {
        Repository::Transaction transaction(triplesStore_t2.connection());
        rb.insert(transaction, true);
        CRES_QVERIFY(transaction.commit());
      }
      QVERIFY(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(_revision.hash())));
      RVC::Revision rev2
        = CRES_QVERIFY(triplesStore_t2.versionControlManager()->revision(_revision.hash()));
      QCOMPARE(rev2.hash(), _revision.hash());
      QCOMPARE(rev2.deltas().size(), _revision.deltas().size());

      for(int i = 0; i < rev2.deltas().size(); ++i)
      {
        RVC::Delta delta_1 = _revision.deltas()[i];
        RVC::Delta delta_2 = rev2.deltas()[i];
        QCOMPARE(delta_1.parent(), delta_2.parent());
        QCOMPARE(delta_1.child(), delta_2.child());
        QCOMPARE(delta_1.hash(), delta_2.hash());
        QCOMPARE(delta_1.delta(), delta_2.delta());
        QCOMPARE(delta_1.signatures().size(), delta_2.signatures().size());
        for(int i = 0; i < delta_1.signatures().size(); ++i)
        {
          RVC::Signature sign_1 = delta_1.signatures()[i];
          RVC::Signature sign_2 = delta_2.signatures()[i];
          QCOMPARE(sign_1.author(), sign_2.author());
          QCOMPARE(sign_1.timestamp(), sign_2.timestamp());
          QCOMPARE(sign_1.signature(), sign_2.signature());
        }
      }
    });

  knowRDF::Triple t1(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  triplesStore_t1.insert(t1);
  knowCore::Value head_1_hash
    = CRES_QVERIFY(kDB::Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
      c, triplesStore_t1.uri(), QStringList() << "versioning" << "head" << "hash"));
  QCOMPARE(triplesStore_t1.versionControlManager()->tipHash(),
           QByteArray::fromHex(CRES_QVERIFY(head_1_hash.value<QString>()).toLatin1()));
  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash())
          != CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash()));
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 2);

  KNOWCORE_TEST_WAIT_FOR(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  COMPARE_TRIPLES_STORES(triplesStore_t1, triplesStore_t2, 2, 1, 1);

  knowRDF::Triple t2(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"));
  triplesStore_t1.insert(t2);
  knowRDF::Triple t3(
    knowCore::Uri("g"), knowCore::Uri("h"),
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"http:g"_kCs).expect_success());
  triplesStore_t1.insert(t3);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);
  head_1_hash = CRES_QVERIFY(kDB::Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
    c, triplesStore_t1.uri(), QStringList() << "versioning" << "head" << "hash"));
  QCOMPARE(triplesStore_t1.versionControlManager()->tipHash(),
           QByteArray::fromHex(CRES_QVERIFY(head_1_hash.value<QString>()).toLatin1()));

  KNOWCORE_TEST_WAIT_FOR(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  COMPARE_TRIPLES_STORES(triplesStore_t1, triplesStore_t2, 4, 1, 3);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 4);
}

void TestVersioning::testFastForwardInsertionRemoval()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore_t1
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());
  Repository::TripleStore triplesStore_t2
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t2")));
  CRES_QVERIFY(triplesStore_t2.enableVersioning());

  triplesStore_t1.versionControlManager()->listenNewRevision(
    [&triplesStore_t2](const RVC::Revision& _revision)
    {
      RVC::RevisionBuilder rb
        = triplesStore_t2.versionControlManager()->insertRevision(_revision.contentHash());
      for(const RVC::Delta& delta : _revision.deltas())
      {
        rb.addDelta(delta.parent(), delta.hash(), delta.delta(), delta.signatures());
      }
      {
        kDB::Repository::Transaction transaction(triplesStore_t2.connection());
        rb.insert(transaction, true);
        transaction.commit();
      }
    });

  knowRDF::Triple t1(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  triplesStore_t1.insert(t1);
  knowRDF::Triple t2(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"));
  triplesStore_t1.insert(t2);
  knowRDF::Triple t3(knowCore::Uri("g"), knowCore::Uri("h"), knowCore::Uri("i"));
  triplesStore_t1.insert(t3);

  triplesStore_t1.remove(t2);

  KNOWCORE_TEST_WAIT_FOR(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  COMPARE_TRIPLES_STORES(triplesStore_t1, triplesStore_t2, 5, 1, 2);

  // Try remove/insert in the same revision
  knowRDF::Triple t4(knowCore::Uri("j"), knowCore::Uri("k"), knowCore::Uri("l"));
  knowRDF::Triple t5(knowCore::Uri("m"), knowCore::Uri("n"), knowCore::Uri("o"));
  knowRDF::Triple t6(knowCore::Uri("p"), knowCore::Uri("q"), knowCore::Uri("r"));

  triplesStore_t1.insert({t4, t5});

  {
    Repository::Transaction transaction(c);
    triplesStore_t1.insert(t6, transaction);
    triplesStore_t1.remove(t5, transaction);
    transaction.commit();
  }

  KNOWCORE_TEST_WAIT_FOR(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  COMPARE_TRIPLES_STORES(triplesStore_t1, triplesStore_t2, 7, 1, 4);

  // Try removing and inserting an existing triple and add an other one
  knowRDF::Triple t7(knowCore::Uri("s"), knowCore::Uri("t"), knowCore::Uri("u"));

  {
    Repository::Transaction transaction(c);
    triplesStore_t1.remove(t6, transaction);
    triplesStore_t1.insert(t6, transaction);
    triplesStore_t1.insert(t7, transaction);
    transaction.commit();
  }

  KNOWCORE_TEST_WAIT_FOR(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  COMPARE_TRIPLES_STORES(triplesStore_t1, triplesStore_t2, 8, 1, 5);

  // Try removing and inserting an existing triple
  {
    Repository::Transaction transaction(c);
    triplesStore_t1.remove(t6, transaction);
    triplesStore_t1.insert(t6, transaction);
    transaction.commit();
  }

  KNOWCORE_TEST_WAIT_FOR(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  COMPARE_TRIPLES_STORES(triplesStore_t1, triplesStore_t2, 8, 1, 5);

  // Try inserting and removing a non existing triple
  knowRDF::Triple t8(knowCore::Uri("v"), knowCore::Uri("w"), knowCore::Uri("z"));

  {
    Repository::Transaction transaction(c);
    triplesStore_t1.insert(t8, transaction);
    triplesStore_t1.remove(t8, transaction);
    transaction.commit();
  }

  KNOWCORE_TEST_WAIT_FOR(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  COMPARE_TRIPLES_STORES(triplesStore_t1, triplesStore_t2, 8, 1, 5);

  // Try removing and inserting a non existing triple
  {
    Repository::Transaction transaction(c);
    triplesStore_t1.remove(t8, transaction);
    triplesStore_t1.insert(t8, transaction);
    transaction.commit();
  }

  KNOWCORE_TEST_WAIT_FOR(CRES_QVERIFY(triplesStore_t2.versionControlManager()->hasRevision(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  COMPARE_TRIPLES_STORES(triplesStore_t1, triplesStore_t2, 9, 1, 6);
}

namespace
{
  cres_qresult<void> copy_revisions(Repository::TripleStore* _src, Repository::TripleStore* _dst)
  {
    QList<RVC::Revision> t2_revisions = CRES_QVERIFY(_src->versionControlManager()->revisions());
    while(not t2_revisions.isEmpty())
    {
      QList<RVC::Revision> delayed_revisions;
      bool has_commited = false;
      for(int i = 0; i < t2_revisions.size(); ++i)
      {
        RVC::Revision rev = t2_revisions[i];
        if(not CRES_QVERIFY(_dst->versionControlManager()->hasRevision(rev.hash())))
        {
          RVC::RevisionBuilder rb
            = _dst->versionControlManager()->insertRevision(rev.contentHash());
          rb.setMetaInformation(rev.hash(), rev.historicity());
          bool can_apply = true;
          for(const RVC::Delta& delta : rev.deltas())
          {
            if(delta.child() != rev.hash())
            {
              return cres_failure("invalid builder");
            }
            rb.addDelta(delta.parent(), delta.hash(), delta.delta(), delta.signatures());
            if(not CRES_QVERIFY(_dst->versionControlManager()->hasRevision(delta.parent())))
            {
              can_apply = false;
            }
          }
          if(can_apply)
          {
            kDB::Repository::Transaction transaction(_dst->connection());
            cres_try(/* TODO use void in 5.0 */ rev, rb.insert(transaction, true));
            cres_try(cres_ignore, transaction.commit());
            has_commited = true;
          }
          else
          {
            rb.discard();
            delayed_revisions.append(rev);
          }
        }
      }
      cres_check(has_commited, "has commited");
      t2_revisions = delayed_revisions;
    }
    return cres_success();
  }
} // namespace

void TestVersioning::testRevisionReuse()
{
  // This test attempt to test the behavior of GoRv2 and in particular, the attempt to GoR v2 in
  // minimizing merge creations The main premise is to create two branches where the triples are
  // added in reverse order, and then check that that merging those branches or adding the triples
  // does not create new revisions.
  //        /----- (t1)  ---- (t2)
  // (root)
  //        \----- (t2)

  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowRDF::Triple t1(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  knowRDF::Triple t2(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"));
  knowRDF::Triple t3(knowCore::Uri("g"), knowCore::Uri("h"), knowCore::Uri("i"));

  Repository::TripleStore triplesStore_t1
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());

  ////// First test merging ((t1),(t2)) with ((t2)), this should not create a new revision

  // Keep root revision
  QByteArray root_tip_hash = CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash());

  // Add two triples in two revisions
  CRES_QVERIFY(triplesStore_t1.insert(t1));
  CRES_QVERIFY(triplesStore_t1.insert(t2));

  QByteArray t1_t2_hash = CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 3);

  // Revert to root, and add t2
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->checkout(root_tip_hash));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 0);

  CRES_QVERIFY(triplesStore_t1.insert(t2));

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 1);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);

  QByteArray t2_hash = CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash());

  // Try to merge (t2) with revision with (t1,t2)
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->merge(t1_t2_hash));

  // This should not have created a new revision
  QCOMPARE(t1_t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);

  // Attempt re-merging

  CRES_QVERIFY(triplesStore_t1.versionControlManager()->checkout(t2_hash));
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->merge(t1_t2_hash));
  // This should not have created a new revision
  QCOMPARE(t1_t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);

  CRES_QVERIFY(triplesStore_t1.versionControlManager()->checkout(t1_t2_hash));
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->merge(t2_hash));

  // This should not have created a new revision
  QCOMPARE(t1_t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);

  ////// Second test merging ((t2)) with ((t1),(t2)), this should not create a new revision

  // Start over from a clean state
  CRES_QVERIFY(c.graphsManager()->removeTripleStore(triplesStore_t1.uri()));

  triplesStore_t1 = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());

  // Add two triples in two revisions
  CRES_QVERIFY(triplesStore_t1.insert(t2));

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 1);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 2);

  QCOMPARE(t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Revert to root, and add t1, t2
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->checkout(root_tip_hash));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 0);

  CRES_QVERIFY(triplesStore_t1.insert(t1));
  CRES_QVERIFY(triplesStore_t1.insert(t2));

  QCOMPARE(t1_t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);

  CRES_QVERIFY(triplesStore_t1.versionControlManager()->merge(t2_hash));

  // This should not have created a new revision
  QCOMPARE(t1_t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);

  ////// Third test creating ((t1),(t2)) then ((t2), (t1)), this should be equivalent to merge

  // Start over from a clean state
  CRES_QVERIFY(c.graphsManager()->removeTripleStore(triplesStore_t1.uri()));

  triplesStore_t1 = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());

  // Add two triples in two revisions
  CRES_QVERIFY(triplesStore_t1.insert(t1));
  CRES_QVERIFY(triplesStore_t1.insert(t2));

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 3);

  QCOMPARE(t1_t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Revert to root, and add t1, t2
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->checkout(root_tip_hash));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 0);

  CRES_QVERIFY(triplesStore_t1.insert(t2));

  QCOMPARE(t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 1);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);

  // Add t1, this should not create any revision
  CRES_QVERIFY(triplesStore_t1.insert(t1));

  // This should not have created a new revision
  QCOMPARE(t1_t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);

  // Try again
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->checkout(t2_hash));

  // Add t1, this should not create any revision
  CRES_QVERIFY(triplesStore_t1.insert(t1));

  // This should not have created a new revision
  QCOMPARE(t1_t2_hash, triplesStore_t1.versionControlManager()->tipHash());

  // Check the number of triples and revisions
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 4);
}

void TestVersioning::testMergeOnlyInsertion()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore_t1
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());
  Repository::TripleStore triplesStore_t2
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t2")));
  CRES_QVERIFY(triplesStore_t2.enableVersioning());

  knowRDF::Triple t1(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  knowRDF::Triple t2(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"));
  knowRDF::Triple t3(knowCore::Uri("g"), knowCore::Uri("h"), knowCore::Uri("i"));

  triplesStore_t1.insert(t2);
  triplesStore_t1.insert(t3);
  triplesStore_t2.insert(t1);
  triplesStore_t2.insert(t3);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 3);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 3);

  CRES_QVERIFY(copy_revisions(&triplesStore_t1, &triplesStore_t2));

  QVERIFY(not CRES_QVERIFY(triplesStore_t2.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QVERIFY(CRES_QVERIFY(triplesStore_t2.versionControlManager()->canMerge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 2);
  CRES_QVERIFY(triplesStore_t2.versionControlManager()->merge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash())));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);
  // QCOMPARE(triplesStore_t2.triples().size(), 3);
  QVERIFY(CRES_QVERIFY(triplesStore_t2.triples()).contains(t1));
  QVERIFY(CRES_QVERIFY(triplesStore_t2.triples()).contains(t2));
  QVERIFY(CRES_QVERIFY(triplesStore_t2.triples()).contains(t3));

  CRES_QVERIFY(copy_revisions(&triplesStore_t2, &triplesStore_t1));

  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash()))));
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->fastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash())));
  QCOMPARE(triplesStore_t1.contentHash(),
           CRES_QVERIFY(triplesStore_t1.versionControlManager()->tip()).contentHash());
  QVERIFY(compare_triples(CRES_QVERIFY(triplesStore_t1.triples()),
                          CRES_QVERIFY(triplesStore_t2.triples())));

  knowRDF::BlankNode bn1, bn2;

  knowRDF::Triple t4(knowCore::Uri("g"), knowCore::Uri("j"), bn1);
  knowRDF::Triple t5(bn1, knowCore::Uri("k"), knowCore::Uri("l"));
  knowRDF::Triple t6(knowCore::Uri("g"), knowCore::Uri("h"), bn2);

  triplesStore_t1.insert({t4, t5});
  triplesStore_t2.insert(t6);

  CRES_QVERIFY(copy_revisions(&triplesStore_t1, &triplesStore_t2));

  QVERIFY(not CRES_QVERIFY(triplesStore_t2.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QVERIFY(CRES_QVERIFY(triplesStore_t2.versionControlManager()->canMerge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 2);
  CRES_QVERIFY(triplesStore_t2.versionControlManager()->merge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash())));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);

  CRES_QVERIFY(copy_revisions(&triplesStore_t2, &triplesStore_t1));

  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash()))));
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->fastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash())));
  QCOMPARE(triplesStore_t1.contentHash(),
           CRES_QVERIFY(triplesStore_t1.versionControlManager()->tip()).contentHash());
  QVERIFY(compare_triples(CRES_QVERIFY(triplesStore_t1.triples()),
                          CRES_QVERIFY(triplesStore_t2.triples())));
}

void TestVersioning::testMergeInsertionRemoval()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore_t1
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());
  Repository::TripleStore triplesStore_t2
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t2")));
  CRES_QVERIFY(triplesStore_t2.enableVersioning());

  knowRDF::Triple t1(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  knowRDF::Triple t2(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"));
  knowRDF::Triple t3(knowCore::Uri("g"), knowCore::Uri("h"), knowCore::Uri("i"));
  knowRDF::Triple t4(knowCore::Uri("j"), knowCore::Uri("k"), knowCore::Uri("l"));
  knowRDF::Triple t5(knowCore::Uri("m"), knowCore::Uri("n"), knowCore::Uri("o"));
  knowRDF::Triple t6(knowCore::Uri("p"), knowCore::Uri("q"), knowCore::Uri("r"));
  knowRDF::Triple t7(knowCore::Uri("s"), knowCore::Uri("t"), knowCore::Uri("u"));

  triplesStore_t1.insert(t2);
  triplesStore_t1.insert(t3);
  triplesStore_t1.insert(t4);
  triplesStore_t1.insert(t5);
  triplesStore_t1.remove(t5);
  triplesStore_t1.insert(t6);
  triplesStore_t2.insert(t1);
  triplesStore_t2.insert(t3);
  triplesStore_t2.insert(t4);
  triplesStore_t2.remove(t4);
  triplesStore_t2.insert(t7);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 7);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 6);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 4);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.triples()).size(), 3);

  // merge in triple store 2
  CRES_QVERIFY(copy_revisions(&triplesStore_t1, &triplesStore_t2));

  QVERIFY(not CRES_QVERIFY(triplesStore_t2.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QVERIFY(CRES_QVERIFY(triplesStore_t2.versionControlManager()->canMerge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 2);
  CRES_QVERIFY(triplesStore_t2.versionControlManager()->merge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash())));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 4);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.triples()).size(), 6);

  // Propagate merge to triple store 1
  CRES_QVERIFY(copy_revisions(&triplesStore_t2, &triplesStore_t1));

  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash()))));
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->fastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash())));
  QCOMPARE(triplesStore_t1.contentHash(),
           CRES_QVERIFY(triplesStore_t1.versionControlManager()->tip()).contentHash());
  QVERIFY(compare_triples(CRES_QVERIFY(triplesStore_t1.triples()),
                          CRES_QVERIFY(triplesStore_t2.triples())));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 13);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 13);

  // Test propagation of removal

  triplesStore_t1.remove(t2);
  triplesStore_t2.insert(t5);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 14);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 14);

  // merge in triple store 2
  CRES_QVERIFY(copy_revisions(&triplesStore_t1, &triplesStore_t2));

  QVERIFY(not CRES_QVERIFY(triplesStore_t2.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QVERIFY(CRES_QVERIFY(triplesStore_t2.versionControlManager()->canMerge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 2);
  CRES_QVERIFY(triplesStore_t2.versionControlManager()->merge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash())));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 5);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.triples()).size(), 6);

  // Propagate merge to triple store 1
  CRES_QVERIFY(copy_revisions(&triplesStore_t2, &triplesStore_t1));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 1);
  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash()))));
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->fastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash())));
  QCOMPARE(triplesStore_t1.contentHash(),
           CRES_QVERIFY(triplesStore_t1.versionControlManager()->tip()).contentHash());
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 1);
  QVERIFY(compare_triples(CRES_QVERIFY(triplesStore_t1.triples()),
                          CRES_QVERIFY(triplesStore_t2.triples())));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 16);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 16);

  // Test with blank nodes!

  knowRDF::BlankNode bn1, bn2;
  knowRDF::Triple t8(knowCore::Uri("g"), knowCore::Uri("j"), bn1);
  knowRDF::Triple t9(bn1, knowCore::Uri("k"), knowCore::Uri("l"));
  knowRDF::Triple t10(knowCore::Uri("g"), knowCore::Uri("h"), bn2);

  triplesStore_t1.insert({t8, t9});
  triplesStore_t2.insert(t10);
  triplesStore_t1.remove(t8);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 7);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.triples()).size(), 7);

  // merge in triple store 2
  CRES_QVERIFY(copy_revisions(&triplesStore_t1, &triplesStore_t2));

  QVERIFY(not CRES_QVERIFY(triplesStore_t2.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QVERIFY(CRES_QVERIFY(triplesStore_t2.versionControlManager()->canMerge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 2);
  CRES_QVERIFY(triplesStore_t2.versionControlManager()->merge(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash())));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 7);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.triples()).size(), 8);

  // Propagate merge to triple store 1
  CRES_QVERIFY(copy_revisions(&triplesStore_t2, &triplesStore_t1));

  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash()))));
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 1);
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->fastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash())));
  QCOMPARE(triplesStore_t1.contentHash(),
           CRES_QVERIFY(triplesStore_t1.versionControlManager()->tip()).contentHash());
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 1);
  QVERIFY(compare_triples(CRES_QVERIFY(triplesStore_t1.triples()),
                          CRES_QVERIFY(triplesStore_t2.triples())));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 20);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 20);

  triplesStore_t1.remove({t9, t1});
  triplesStore_t2.remove({t4, t5, t9});

  // merge in triple store 2
  CRES_QVERIFY(copy_revisions(&triplesStore_t2, &triplesStore_t1));

  QVERIFY(not CRES_QVERIFY(triplesStore_t1.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash()))));
  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->canMerge(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash()))));
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 2);
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->merge(
    CRES_QVERIFY(triplesStore_t2.versionControlManager()->tipHash())));
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 1);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 4);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.triples()).size(), 5);

  // Propagate merge to triple store 1
  CRES_QVERIFY(copy_revisions(&triplesStore_t1, &triplesStore_t2));

  QVERIFY(CRES_QVERIFY(triplesStore_t2.versionControlManager()->canFastForward(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()))));
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);
  CRES_QVERIFY(triplesStore_t2.versionControlManager()->fastForward(
    CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash())));
  QCOMPARE(triplesStore_t2.contentHash(),
           CRES_QVERIFY(triplesStore_t2.versionControlManager()->tip()).contentHash());
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->heads()).size(), 1);
  QVERIFY(compare_triples(CRES_QVERIFY(triplesStore_t1.triples()),
                          CRES_QVERIFY(triplesStore_t2.triples())));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 23);
  QCOMPARE(CRES_QVERIFY(triplesStore_t2.versionControlManager()->revisions()).size(), 23);
}

void TestVersioning::testMergeManyHeads()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore_t1
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());

  QByteArray rootHash = CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash());

  knowRDF::Triple t1(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  knowRDF::Triple t2(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"));
  knowRDF::Triple t3(knowCore::Uri("g"), knowCore::Uri("h"), knowCore::Uri("i"));
  knowRDF::Triple t4(knowCore::Uri("j"), knowCore::Uri("k"), knowCore::Uri("l"));
  knowRDF::Triple t5(knowCore::Uri("m"), knowCore::Uri("n"), knowCore::Uri("o"));
  knowRDF::Triple t6(knowCore::Uri("p"), knowCore::Uri("q"), knowCore::Uri("r"));
  knowRDF::Triple t7(knowCore::Uri("s"), knowCore::Uri("t"), knowCore::Uri("u"));

  triplesStore_t1.insert(t1);
  triplesStore_t1.versionControlManager()->checkout(rootHash);
  triplesStore_t1.insert(t2);
  triplesStore_t1.versionControlManager()->checkout(rootHash);
  triplesStore_t1.insert(t3);
  triplesStore_t1.versionControlManager()->checkout(rootHash);
  triplesStore_t1.insert(t4);
  triplesStore_t1.versionControlManager()->checkout(rootHash);
  triplesStore_t1.insert(t5);
  triplesStore_t1.versionControlManager()->checkout(rootHash);
  triplesStore_t1.insert(t6);
  triplesStore_t1.versionControlManager()->checkout(rootHash);
  triplesStore_t1.insert(t7);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 8);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 7);
  QList<QByteArray> hashes;
  for(const kDB::Repository::VersionControl::Revision& rev_h :
      CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()))
  {
    if(rev_h.hash() != triplesStore_t1.versionControlManager()->tipHash())
    {
      hashes.append(rev_h.hash());
    }
  }
  QCOMPARE(hashes.size(), 6);
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->merge(hashes));
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 9);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 1);

  QList<knowRDF::Triple> triples = CRES_QVERIFY(triplesStore_t1.triples());
  QCOMPARE(triples.size(), 7);
  QVERIFY(triples.contains(t1));
  QVERIFY(triples.contains(t2));
  QVERIFY(triples.contains(t3));
  QVERIFY(triples.contains(t4));
  QVERIFY(triples.contains(t5));
  QVERIFY(triples.contains(t6));
  QVERIFY(triples.contains(t7));
}

void TestVersioning::testRebase(bool _squash, int _final_revisions)
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore triplesStore_t1
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore(knowCore::Uri("t1")));
  CRES_QVERIFY(triplesStore_t1.enableVersioning());
  triplesStore_t1.versionControlManager()->setDefaultRevisionTag(
    kDB::Repository::VersionControl::Revision::Tag::Editable);
  triplesStore_t1.versionControlManager()->setDefaultRevisionTag(
    kDB::Repository::VersionControl::Revision::Tag::Private);

  knowRDF::Triple t1(knowCore::Uri("a"), knowCore::Uri("b"), knowCore::Uri("c"));
  knowRDF::Triple t2(knowCore::Uri("d"), knowCore::Uri("e"), knowCore::Uri("f"));
  knowRDF::Triple t3(knowCore::Uri("g"), knowCore::Uri("h"), knowCore::Uri("i"));
  knowRDF::Triple t4(knowCore::Uri("j"), knowCore::Uri("k"), knowCore::Uri("l"));
  knowRDF::Triple t5(knowCore::Uri("m"), knowCore::Uri("n"), knowCore::Uri("o"));
  knowRDF::Triple t6(knowCore::Uri("p"), knowCore::Uri("q"), knowCore::Uri("r"));
  knowRDF::Triple t7(knowCore::Uri("s"), knowCore::Uri("t"), knowCore::Uri("u"));

  Repository::VersionControl::Revision rootRevision
    = CRES_QVERIFY(triplesStore_t1.versionControlManager()->tip());

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 1);

  triplesStore_t1.insert(t1);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 2);

  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions())
            .last()
            .tags()
            .testFlag(Repository::VersionControl::Revision::Tag::Editable));
  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions())
            .last()
            .tags()
            .testFlag(Repository::VersionControl::Revision::Tag::Private));

  triplesStore_t1.insert(t2);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 3);

  Repository::VersionControl::Revision targetRevision
    = CRES_QVERIFY(triplesStore_t1.versionControlManager()->tip());

  QVERIFY(targetRevision.tags().testFlag(Repository::VersionControl::Revision::Tag::Editable));
  QVERIFY(targetRevision.tags().testFlag(Repository::VersionControl::Revision::Tag::Private));

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 3);
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->checkout(rootRevision.hash()));
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->tipHash()), rootRevision.hash());
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 3);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 0);

  triplesStore_t1.insert(t3);
  triplesStore_t1.insert(t4);

  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(), 5);

  QVERIFY(CRES_QVERIFY(triplesStore_t1.versionControlManager()->canRebase(targetRevision.hash())));
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 2);
  CRES_QVERIFY(triplesStore_t1.versionControlManager()->rebase(targetRevision.hash(), _squash));
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->revisions()).size(),
           _final_revisions);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.triples()).size(), 4);
  QCOMPARE(CRES_QVERIFY(triplesStore_t1.versionControlManager()->heads()).size(), 1);
}

void TestVersioning::testRebaseSquash() { testRebase(true, 4); }

void TestVersioning::testRebaseNoSquash() { testRebase(false, 5); }

struct thread_data
{
  thread_data(int _s) : generator(_s), distribution_0_1(0.0, 1.0) {}
  QMutex revisions_queue_mutex;
  QList<RVC::Revision> revisions_queue;
  kDB::Repository::TripleStore tripleStore;
  std::default_random_engine generator;
  std::uniform_real_distribution<qreal> distribution_0_1;

  qreal nextFloat() { return distribution_0_1(generator); }
  int nextBounded(int _max) { return std::uniform_int_distribution<int>(0, _max - 1)(generator); }
};

class ThreadLambda : public QThread
{
public:
  ThreadLambda(const std::function<void()>& _run) : m_run(_run) {}
  void run() override { m_run(); }
private:
  std::function<void()> m_run;
};

#define TM_COMPARE(actual, expected)                                                               \
  do                                                                                               \
  {                                                                                                \
    if(!QTest::qCompare(actual, expected, #actual, #expected, __FILE__, __LINE__))                 \
    {                                                                                              \
      has_failure = true;                                                                          \
      return;                                                                                      \
    }                                                                                              \
  } while(false)

void TestVersioning::testMultithreaded()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  bool running_insertion = true;
  bool running_merge = true;
  QList<thread_data*> thread_datas;
  QList<ThreadLambda*> threads_insertion;
  QList<ThreadLambda*> threads_merge;
  clog_warning("WARNING: for this test the debug messages have been disabled");
  // clog_disable(clog_level_debug);
  bool has_failure = false;
  for(int thread_index = 0; thread_index < 4; ++thread_index)
  {
    thread_datas.append(new thread_data(thread_index));
    // Insertion thread
    thread_datas[thread_index]->tripleStore = CRES_QVERIFY(
      c.graphsManager()->getOrCreateTripleStore(clog_qt::qformat("store_{}", thread_index)));
    CRES_QVERIFY(thread_datas[thread_index]->tripleStore.enableVersioning());
    for(int k = 0; k < 3; ++k)
    {
      threads_insertion.append(new ThreadLambda(
        [&running_insertion, &thread_datas, thread_index]()
        {
          kDB::Repository::TripleStore triplesStore
            = thread_datas[thread_index]->tripleStore.detach();
          while(running_insertion)
          {
            double value = thread_datas[thread_index]->nextFloat();
            if(value < 0.3)
            {
              QList<knowRDF::Triple> triples = CRES_QVERIFY(triplesStore.triples());
              int removing = (thread_datas[thread_index]->nextFloat() * 0.5 + 0.5) * triples.size();
              for(int i = 0; i < removing; ++i)
              {
                if(triples.size() > 0)
                {
                  int r = thread_datas[thread_index]->nextBounded(triples.size());
                  triples.removeAt(r);
                }
              }
              if(triples.size() > 0)
              {
                CRES_QVERIFY(triplesStore.remove(triples));
              }
            }
            else
            {
              int inserting = thread_datas[thread_index]->nextBounded(20);
              QList<knowRDF::Triple> triples;
              for(int i = 0; i < inserting; ++i)
              {
                triples.append(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                               QUuid::createUuid().toString(),
                                               knowCore::Uri(QUuid::createUuid().toString())));
              }
              CRES_QVERIFY(triplesStore.insert(triples));
            }
          }
        }));
    }
    threads_merge.append(new ThreadLambda(
      [&running_merge, &thread_datas, thread_index, &has_failure]()
      {
        kDB::Repository::TripleStore triplesStore
          = thread_datas[thread_index]->tripleStore.detach();
        while(running_merge)
        {
          QList<RVC::Revision> revisions_to_insert;
          {
            thread_data* td = thread_datas[thread_index];
            QMutexLocker l(&td->revisions_queue_mutex);
            revisions_to_insert = td->revisions_queue;
            td->revisions_queue.clear();
          }

          for(const RVC::Revision& rev : revisions_to_insert)
          {
            if(not CRES_QVERIFY(triplesStore.versionControlManager()->hasRevision(rev.hash())))
            {
              RVC::RevisionBuilder rb
                = triplesStore.versionControlManager()->insertRevision(rev.contentHash());
              rb.setMetaInformation(rev.hash(), rev.historicity());
              for(const RVC::Delta& delta : rev.deltas())
              {
                rb.addDelta(delta.parent(), delta.hash(), delta.delta(), delta.signatures());
              }
              {
                kDB::Repository::Transaction transaction(triplesStore.connection());
                kDB::Repository::VersionControl::Revision inserted_r
                  = CRES_QVERIFY(rb.insert(transaction, true));
                TM_COMPARE(inserted_r.contentHash(), rev.contentHash());
                TM_COMPARE(inserted_r.hash(), rev.hash());
                TM_COMPARE(inserted_r.historicity(), rev.historicity());
                CRES_QVERIFY(transaction.commit());
              }
            }
          }
          QByteArray currentHead = CRES_QVERIFY(triplesStore.versionControlManager()->tipHash());
          for(const RVC::Revision& head :
              CRES_QVERIFY(triplesStore.versionControlManager()->heads()))
          {
            if(head.hash() != currentHead
               and CRES_QVERIFY(triplesStore.versionControlManager()->canFastForward(head.hash())))
            {
              kDB::Repository::Transaction transaction(triplesStore.connection());
              if(triplesStore.versionControlManager()
                   ->fastForward(head.hash(), transaction)
                   .is_successful())
              {
                QByteArray cH = triplesStore.contentHash(transaction);
                if(cH != head.contentHash())
                {
                  transaction.rollback();
                  clog_debug_vn(cH, head.contentHash());
                  TM_COMPARE(cH, head.contentHash());
                }
                else
                {
                  transaction.commit();
                }
              }
            }
          }
          if(thread_index == 0)
          {
            // merge master
            QByteArray ownHead = CRES_QVERIFY(triplesStore.versionControlManager()->tipHash());
            for(const RVC::Revision& head :
                CRES_QVERIFY(triplesStore.versionControlManager()->heads()))
            {
              if(head.hash() != ownHead
                 and CRES_QVERIFY(triplesStore.versionControlManager()->canMerge(head.hash())))
              {
                triplesStore.versionControlManager()->merge(head.hash());
              }
            }
          }
        }
      }));

    thread_datas[thread_index]->tripleStore.versionControlManager()->listenNewRevision(
      [&thread_datas, thread_index](const RVC::Revision& _revision)
      {
        for(int j = 0; j < thread_datas.size(); ++j)
        {
          if(thread_index != j)
          {
            thread_data* td = thread_datas[j];
            QMutexLocker l(&td->revisions_queue_mutex);
            td->revisions_queue.append(_revision);
          }
        }
      });
  }
  // Start threads
  for(QThread* t : threads_merge)
  {
    t->start();
  }
  for(QThread* t : threads_insertion)
  {
    t->start();
  }

  // Wait
  QThread::sleep(1);

  // Stop insertion threads
  clog_info("Stop insertion threads...");
  running_insertion = false;
  for(QThread* thread : threads_insertion)
  {
    thread->wait();
  }
  if(has_failure)
  {
    clog_info("Stop merging threads...");
    running_merge = false;
    for(QThread* thread : threads_merge)
    {
      thread->wait();
    }
    QFAIL("There were failure during the synchronisation.");
  }
  // Stop merge threads
  clog_info("Wait for merging to complete...");
  bool continue_waiting = true;
  while(continue_waiting)
  {
    continue_waiting = false;
    for(int i = 0; i < thread_datas.size(); ++i)
    {
      if(CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->heads()).size() != 1)
      {
        clog_info(
          "Thread {} still has {} heads.", i,
          CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->heads()).size());
        continue_waiting = true;
        break;
      }
      if(CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->revisions()).size()
         != CRES_QVERIFY(thread_datas[0]->tripleStore.versionControlManager()->revisions()).size())
      {
        clog_info(
          "Thread {} has {} revisions but thread 0 has {} revisions", i,
          CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->revisions()).size(),
          CRES_QVERIFY(thread_datas[0]->tripleStore.versionControlManager()->revisions()).size());
        continue_waiting = true;
        break;
      }
    }
    QThread::sleep(2);
  }
  clog_info("Stop merging threads...");
  running_merge = false;
  for(QThread* thread : threads_merge)
  {
    thread->wait();
  }
  QCOMPARE(CRES_QVERIFY(thread_datas[0]->tripleStore.versionControlManager()->heads()).size(), 1);
  for(int i = 1; i < thread_datas.size(); ++i)
  {
    QList<RVC::Revision> heads
      = CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->heads());
    QCOMPARE(heads.size(), 1);
    QByteArray head_hash
      = CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->tipHash());
    if(heads.first().hash() != head_hash)
    {
      if(not CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->canFastForward(
           heads.first().hash())))
      {
        while(true)
        {
          QThread::sleep(10000);
        }
      }
      QVERIFY(CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->canFastForward(
        heads.first().hash())));
      CRES_QVERIFY(
        thread_datas[i]->tripleStore.versionControlManager()->fastForward(heads.first().hash()));
      QCOMPARE(
        thread_datas[i]->tripleStore.contentHash(),
        CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->tip()).contentHash());
    }
    QCOMPARE(thread_datas[0]->tripleStore.versionControlManager()->tipHash(), heads.first().hash());
    QCOMPARE(thread_datas[i]->tripleStore.versionControlManager()->tipHash(), heads.first().hash());
  }

  kDB::Repository::TripleStore tripleStoreMerge
    = CRES_QVERIFY(c.graphsManager()->createTripleStore(QStringLiteral("tripleStoreMerge")));
  tripleStoreMerge.enableVersioning();
  CRES_QVERIFY(copy_revisions(&thread_datas[0]->tripleStore, &tripleStoreMerge));
  CRES_QVERIFY(tripleStoreMerge.versionControlManager()->fastForward(
    CRES_QVERIFY(thread_datas[0]->tripleStore.versionControlManager()->tipHash())));
  QCOMPARE(tripleStoreMerge.contentHash(),
           CRES_QVERIFY(tripleStoreMerge.versionControlManager()->tip()).contentHash());
  QVERIFY(compare_triples(CRES_QVERIFY(thread_datas[0]->tripleStore.triples()),
                          CRES_QVERIFY(tripleStoreMerge.triples())));

  for(int i = 1; i < thread_datas.size(); ++i)
  {
    QCOMPARE(CRES_QVERIFY(thread_datas[0]->tripleStore.versionControlManager()->tipHash()),
             CRES_QVERIFY(thread_datas[i]->tripleStore.versionControlManager()->tipHash()));
    QVERIFY(compare_triples(CRES_QVERIFY(thread_datas[0]->tripleStore.triples()),
                            CRES_QVERIFY(thread_datas[i]->tripleStore.triples())));
  }
}

void TestVersioning::testHash()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore ts1 = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore("ts1"_kCu));
  Repository::TripleStore ts2 = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore("ts2"_kCu));

  CRES_QVERIFY(ts1.enableVersioning());
  CRES_QVERIFY(ts2.enableVersioning());

  ts1.insert("a"_kCu, "b"_kCu, "c"_kCu);
  ts1.insert("d"_kCu, "e"_kCu, "f"_kCu);

  Repository::VersionControl::Revision ts1_tip = CRES_QVERIFY(ts1.versionControlManager()->tip());
  Repository::VersionControl::Revision ts2_tip = CRES_QVERIFY(ts2.versionControlManager()->tip());

  QVERIFY(ts1_tip.contentHash() != ts2_tip.contentHash());
  QCOMPARE(ts1_tip.historicity(), 2);
  QCOMPARE(ts2_tip.historicity(), 0);
  QVERIFY(ts1_tip.hash() != ts2_tip.hash());

  ts2.insert("d"_kCu, "e"_kCu, "f"_kCu);
  ts2.insert("a"_kCu, "b"_kCu, "c"_kCu);

  ts2_tip = CRES_QVERIFY(ts2.versionControlManager()->tip());
  QCOMPARE(ts1_tip.contentHash(), ts2_tip.contentHash());
  QCOMPARE(ts1_tip.historicity(), ts2_tip.historicity());
  QCOMPARE(ts1_tip.historicity(), 2);
  QCOMPARE(ts1_tip.hash(), ts2_tip.hash());
}

QTEST_MAIN(TestVersioning)
