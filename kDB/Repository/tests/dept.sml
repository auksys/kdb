Prefix ex: <http://ex.org/>.
Prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
Prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
Prefix xsd: <http://www.w3.org/2001/XMLSchema#>
 
Create View ex:department As
    Construct {
        ?d  a ex:department;
            ex:label ?l ;
            ex:location ?p ;
            ex:staff ?s .
    }
    With
        ?d = uri(ex:department, ?DEPTNO)
        ?l = plainLiteral(?DNAME)
        ?p = plainLiteral(?LOC)
        ?s = typedLiteral(?STAFF, xsd:int)
    From
       [[ SELECT DEPTNO, DNAME, LOC, (SELECT COUNT(*) FROM EMP WHERE EMP.DEPTNO=DEPT.DEPTNO)::int4 AS STAFF FROM DEPT ]]
