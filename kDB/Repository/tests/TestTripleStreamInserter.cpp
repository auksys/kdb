#include "TestTripleStreamInserter.h"

#include <knowCore/Test.h>
#include <knowCore/Uris/xsd.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>
#include <knowRDF/TripleStream.h>

#include "../Connection.h"
#include "../GraphsManager.h"
#include "../TemporaryStore.h"
#include "../TripleStore.h"
#include "../TripleStreamInserter.h"

namespace Repository = kDB::Repository;

#define COMPARE_T3(__idx__, __subject_url__, __predicate_url__, __value__)                         \
  {                                                                                                \
    knowRDF::Triple t1 = triples[__idx__];                                                         \
    QCOMPARE(t1.subject(), knowRDF::Subject(knowCore::Uri(__subject_url__)));                      \
    QCOMPARE(t1.predicate(), knowCore::Uri(__predicate_url__));                                    \
    QCOMPARE(t1.object(), knowRDF::Object(__value__));                                             \
  }

void TestTripleStreamInserter::testInsertTurtle()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());
  QVERIFY(interface.isValid());

  Repository::TripleStreamInserter tsi(interface);

  QFile file(":/example1.ttl");
  file.open(QIODevice::ReadOnly);
  knowRDF::TripleStream stream;
  stream.addListener(&tsi);
  stream.start(&file);

  QList<knowRDF::Triple> triples = CRES_QVERIFY(interface.triples());
  QCOMPARE(triples.size(), 7);

  COMPARE_T3(0, "http://example.org/#green-goblin",
             "http://www.perceive.net/schemas/relationship/enemyOf",
             knowCore::Uri("http://example.org/#spiderman"));
  COMPARE_T3(1, "http://example.org/#green-goblin",
             "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
             knowCore::Uri("http://xmlns.com/foaf/0.1/Person"));
  COMPARE_T3(2, "http://example.org/#green-goblin", "http://xmlns.com/foaf/0.1/name",
             knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Green Goblin"_kCs)
               .expect_success());
  COMPARE_T3(3, "http://example.org/#spiderman",
             "http://www.perceive.net/schemas/relationship/enemyOf",
             knowCore::Uri("http://example.org/#green-goblin"));
  COMPARE_T3(4, "http://example.org/#spiderman", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
             knowCore::Uri("http://xmlns.com/foaf/0.1/Person"));
  COMPARE_T3(
    5, "http://example.org/#spiderman", "http://xmlns.com/foaf/0.1/name",
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Spiderman"_kCs).expect_success());
  COMPARE_T3(6, "http://example.org/#spiderman", "http://xmlns.com/foaf/0.1/name",
             knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"Человек-паук"_kCs, "ru")
               .expect_success());
}

QTEST_MAIN(TestTripleStreamInserter)
