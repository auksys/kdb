#include "TestMD5.h"

#include <knowCore/Global.h>
#include <knowCore/ValueHash.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "../Connection.h"
#include "../TemporaryStore.h"

#include "../Test.h"

void TestMD5::testBasicTypes()
{
  namespace Repository = kDB::Repository;
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());
  QString text_string = "";
  knowDBC::Query q = c.createSQLQuery("SELECT digest(:value::bytea, 'md5')");

  q.bindValue(":value", text_string);
  COMPARE_SINGLE_QUERY_RESULT(q, Cyqlops::Crypto::Hash::md5(text_string));

  text_string = "Hello world!";
  q.bindValue(":value", text_string);
  COMPARE_SINGLE_QUERY_RESULT(q, Cyqlops::Crypto::Hash::md5(text_string));

  QByteArray text_string_utf8 = "Hello world!"_kCs.toUtf8();
  q.bindValue(":value", text_string);
  COMPARE_SINGLE_QUERY_RESULT(q, Cyqlops::Crypto::Hash::md5(text_string_utf8));
}

void TestMD5::testAggregate()
{
  namespace Repository = kDB::Repository;
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());
  QString av = "";
  QString bv = "Hello world!";
  QByteArray cv = bv.toUtf8();
  knowDBC::Query q = c.createSQLQuery("CREATE TABLE test_data_md5 AS (SELECT 0 AS y, :av::bytea AS "
                                      "x UNION SELECT 1, :bv::bytea UNION SELECT 2, :cv::bytea)");
  q.bindValue(":av", av);
  q.bindValue(":bv", bv);
  q.bindValue(":cv", cv);
  VERIFY_QUERY_EXECUTION(q);
  q.setQuery("SELECT kdb_compute_md5_table_column('test_data_md5', 'x', 'y')");

  COMPARE_SINGLE_QUERY_RESULT(q, Cyqlops::Crypto::Hash::md5(av, bv, cv));
}

QTEST_MAIN(TestMD5)
