#include "TestReadWriteData.h"

#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "../Connection.h"
#include "../DatabaseInterface/PostgreSQL/SQLCopyData.h"
#include "../DatabaseInterface/PostgreSQL/SQLReadData.h"
#include "../QueryConnectionInfo.h"
#include "../TemporaryStore.h"
#include "../Transaction.h"

namespace Repository = kDB::Repository;

void TestReadWriteData::testWrite()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query sqlQuery
    = c.createSQLQuery("CREATE TABLE words (id serial PRIMARY KEY, word text, count int);");
  QVERIFY(sqlQuery.execute());

  {
    Repository::Transaction transaction(c);
    knowDBC::Query sqlQuery2
      = transaction.createSQLQuery("COPY words(word, count) FROM STDIN WITH (FORMAT binary)");
    QVERIFY(sqlQuery2.execute());
    Repository::DatabaseInterface::PostgreSQL::SQLCopyData scd(transaction);
    scd.writeHeader();
    scd.write<quint32>(0, false); // No oid
    scd.write<quint32>(0, false); // No extra

    scd.write<quint16>(2, false);
    scd.write<QString>("Hello", true);
    scd.write<qint32>(1, true);
    scd.write<quint16>(2, false);
    scd.write<QString>("world", true);
    scd.write<qint32>(2, true);
    scd.write<quint16>(2, false);
    scd.write<QString>("people", true);
    scd.write<qint32>(0, true);

    scd.close();
    transaction.commit();
  }

  sqlQuery.setQuery("SELECT word, count FROM words");
  knowDBC::Result result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(result.fields(), 2);
  QCOMPARE(result.tuples(), 3);
  QCOMPARE(result.value<QString>(0, 0).expect_success(), QString("Hello"));
  QCOMPARE(result.value<int>(0, 1).expect_success(), 1);
  QCOMPARE(result.value<QString>(1, 0).expect_success(), QString("world"));
  QCOMPARE(result.value<int>(1, 1).expect_success(), 2);
  QCOMPARE(result.value<QString>(2, 0).expect_success(), QString("people"));
  QCOMPARE(result.value<int>(2, 1).expect_success(), 0);
}

void TestReadWriteData::testRead()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query sqlQuery
    = c.createSQLQuery("CREATE TABLE words (id serial PRIMARY KEY, word text, count int);");
  QVERIFY(sqlQuery.execute());

  sqlQuery.setQuery(
    "INSERT INTO words(word, count) VALUES ('Hello', 1), ('world', 2), ('people', 0)");
  QVERIFY(sqlQuery.execute());

  {
    Repository::Transaction transaction(c);
    knowDBC::Query sqlQuery2 = transaction.createSQLQuery(
      "COPY (SELECT word, count FROM words) TO STDOUT WITH (FORMAT binary)");
    QVERIFY(sqlQuery2.execute());
    Repository::DatabaseInterface::PostgreSQL::SQLReadData scd(transaction);

    QVERIFY(scd.readHeader());
    QVERIFY(not scd.read<quint32>(false));
    QVERIFY(not scd.read<quint32>(false));

    QCOMPARE(scd.read<quint16>(false), quint16(2));
    QCOMPARE(scd.read<QString>(true), QString("Hello"));
    QCOMPARE(scd.read<qint32>(true), 1);
    QCOMPARE(scd.read<quint16>(false), quint16(2));
    QCOMPARE(scd.read<QString>(true), QString("world"));
    QCOMPARE(scd.read<qint32>(true), 2);
    QCOMPARE(scd.read<quint16>(false), quint16(2));
    QCOMPARE(scd.read<QString>(true), QString("people"));
    QCOMPARE(scd.read<qint32>(true), 0);

    QVERIFY(scd.readNull());
    transaction.commit();
  }
}

void TestReadWriteData::testCopy()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query sqlQuery
    = c.createSQLQuery("CREATE TABLE words (id serial PRIMARY KEY, word text, count int);");
  QVERIFY(sqlQuery.execute());

  sqlQuery.setQuery(
    "INSERT INTO words(word, count) VALUES ('Hello', 1), ('world', 2), ('people', 0)");
  QVERIFY(sqlQuery.execute());

  sqlQuery.setQuery("CREATE TABLE words_copy (id serial PRIMARY KEY, word text, count int);");
  QVERIFY(sqlQuery.execute());

  {
    Repository::Transaction transaction2(c);
    Repository::Transaction transaction3(c);
    knowDBC::Query sqlQuery2 = transaction2.createSQLQuery(
      "COPY (SELECT word, count FROM words) TO STDOUT WITH (FORMAT binary)");
    knowDBC::Query sqlQuery3
      = transaction3.createSQLQuery("COPY words_copy(word, count) FROM STDIN WITH (FORMAT binary)");

    QVERIFY(sqlQuery2.execute());
    QVERIFY(sqlQuery3.execute());

    Repository::DatabaseInterface::PostgreSQL::SQLReadData srd(transaction2);
    Repository::DatabaseInterface::PostgreSQL::SQLCopyData scd(transaction3);

    while(srd.status() == Repository::DatabaseInterface::PostgreSQL::SQLReadData::Open)
    {
      Repository::DatabaseInterface::PostgreSQL::SQLReadData::Buffer buffer = srd.readBuffer();
      if(not buffer.isNull())
      {
        scd.writeBuffer(buffer.data(), buffer.size());
      }
    }
    scd.close();
    transaction2.commit();
    transaction3.commit();
  }
  sqlQuery.setQuery("SELECT word, count FROM words_copy");
  knowDBC::Result result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(result.fields(), 2);
  QCOMPARE(result.tuples(), 3);
  QCOMPARE(result.value<QString>(0, 0).expect_success(), QString("Hello"));
  QCOMPARE(result.value<int>(0, 1).expect_success(), 1);
  QCOMPARE(result.value<QString>(1, 0).expect_success(), QString("world"));
  QCOMPARE(result.value<int>(1, 1).expect_success(), 2);
  QCOMPARE(result.value<QString>(2, 0).expect_success(), QString("people"));
  QCOMPARE(result.value<int>(2, 1).expect_success(), 0);
}

QTEST_MAIN(TestReadWriteData)
