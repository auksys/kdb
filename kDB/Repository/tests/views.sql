CREATE TABLE emp (
  empno serial PRIMARY KEY,
  ename text,
  job   text,
  age   int,
  deptno int
);

INSERT INTO emp(empno, ename, job, age, deptno) VALUES (7369, 'SMITH', 'CLERK', 33, 10), (7370, 'SMITH', 'NIGHTGUARD', 28, 20), (7400, 'JONES', 'ENGINEER', 54, 10);

CREATE TABLE dept (
  deptno serial PRIMARY KEY,
  dname text,
  loc text
);

INSERT INTO dept(deptno, dname, loc) VALUES (10, 'APPSERVER', 'NEW YORK'), (20, 'RESEARCH', 'BOSTON');
