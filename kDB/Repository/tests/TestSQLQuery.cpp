#include "TestSQLQuery.h"

#include <knowCore/BigNumber.h>
#include <knowCore/Timestamp.h>
#include <knowCore/TypeDefinitions.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "../Connection.h"
#include "../TemporaryStore.h"

#include "../Test.h"

namespace Repository = kDB::Repository;

void TestSQLQuery::testInsertSelect()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query sqlQuery
    = c.createSQLQuery("CREATE TABLE words (id serial PRIMARY KEY, word text UNIQUE);");
  QVERIFY(sqlQuery.execute());

  int next_index = 1;

  sqlQuery.setQuery("INSERT INTO words(word) VALUES('hello') RETURNING id;");
  knowDBC::Result result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(result.value<int>(0, 0), next_index);
  result = sqlQuery.execute();
  QVERIFY(not result);
  QCOMPARE(result.error(),
           QString("ERROR:  duplicate key value violates unique constraint "
                   "\"words_word_key\"\nDETAIL:  Key (word)=(hello) already exists.\n"));

  sqlQuery.setQuery("SELECT * FROM words WHERE word='hello'");
  result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(result.fields(), 2);
  QCOMPARE(result.tuples(), 1);
  QCOMPARE(result.fieldName(0), QString("id"));
  QCOMPARE(result.fieldName(1), QString("word"));
  QCOMPARE(result.value(0, 0).toVariant(), QVariant(next_index));
  QCOMPARE(result.value(0, 1).toVariant(), QVariant("hello"));
  QCOMPARE(result.value(0, "id").toVariant(), QVariant(next_index));
  QCOMPARE(result.value(0, "word").toVariant(), QVariant("hello"));

  sqlQuery.setQuery("INSERT INTO words(id, word) VALUES(:id, :word);");
  sqlQuery.bindValue(":id", next_index + 1);
  sqlQuery.bindValue(":word", "world"_kCs);
  QVERIFY(sqlQuery.execute());

  sqlQuery.setQuery("SELECT * FROM words WHERE id=:id1 OR id=:id2");
  sqlQuery.bindValue(":id1", next_index);
  sqlQuery.bindValue(":id2", next_index + 1);
  result = sqlQuery.execute();

  QVERIFY(result);
  QCOMPARE(result.fields(), 2);
  QCOMPARE(result.tuples(), 2);
  QCOMPARE(result.fieldName(0), QString("id"));
  QCOMPARE(result.fieldName(1), QString("word"));
  QCOMPARE(result.value(0, 0).toVariant(), QVariant(next_index));
  QCOMPARE(result.value(0, 1).toVariant(), QVariant("hello"));
  QCOMPARE(result.value(0, "id").toVariant(), QVariant(next_index));
  QCOMPARE(result.value(0, "word").toVariant(), QVariant("hello"));
  QCOMPARE(result.value(1, 0).toVariant(), QVariant(next_index + 1));
  QCOMPARE(result.value(1, 1).toVariant(), QVariant("world"));
  QCOMPARE(result.value(1, "id").toVariant(), QVariant(next_index + 1));
  QCOMPARE(result.value(1, "word").toVariant(), QVariant("world"));

  sqlQuery.setQuery("SELECT * FROM words WHERE word=:word");
  sqlQuery.bindValue(":word", "world"_kCs);
  result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(result.fields(), 2);
  QCOMPARE(result.tuples(), 1);
  QCOMPARE(result.fieldName(0), QString("id"));
  QCOMPARE(result.fieldName(1), QString("word"));
  QCOMPARE(result.value(0, 0).toVariant(), QVariant(next_index + 1));
  QCOMPARE(result.value(0, 1).toVariant(), QVariant("world"));
  QCOMPARE(result.value(0, "id").toVariant(), QVariant(next_index + 1));
  QCOMPARE(result.value(0, "word").toVariant(), QVariant("world"));

  sqlQuery.setQuery("SELECT * FROM words WHERE word=:word");
  sqlQuery.bindValue(":word", "world"_kCs);
  result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(result.fields(), 2);
  QCOMPARE(result.tuples(), 1);
  QCOMPARE(result.fieldName(0), QString("id"));
  QCOMPARE(result.fieldName(1), QString("word"));
  QCOMPARE(result.value(0, 0).toVariant(), QVariant(next_index + 1));
  QCOMPARE(result.value(0, 1).toVariant(), QVariant("world"));
  QCOMPARE(result.value(0, "id").toVariant(), QVariant(next_index + 1));
  QCOMPARE(result.value(0, "word").toVariant(), QVariant("world"));

  sqlQuery.setQuery("SELECT * FROM words WHERE word=:word1 or word=:word2 ORDER BY word DESC");
  sqlQuery.bindValue(":word1", "hello"_kCs);
  sqlQuery.bindValue(":word2", "world"_kCs);
  result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(result.fields(), 2);
  QCOMPARE(result.tuples(), 2);
  QCOMPARE(result.fieldName(0), QString("id"));
  QCOMPARE(result.fieldName(1), QString("word"));
  QCOMPARE(result.value(0, 0).toVariant(), QVariant(next_index + 1));
  QCOMPARE(result.value(0, 1).toVariant(), QVariant("world"));
  QCOMPARE(result.value(0, "id").toVariant(), QVariant(next_index + 1));
  QCOMPARE(result.value(0, "word").toVariant(), QVariant("world"));
  QCOMPARE(result.value(1, 0).toVariant(), QVariant(next_index));
  QCOMPARE(result.value(1, 1).toVariant(), QVariant("hello"));
  QCOMPARE(result.value(1, "id").toVariant(), QVariant(next_index));
  QCOMPARE(result.value(1, "word").toVariant(), QVariant("hello"));

  sqlQuery.setQuery("SELECT :value");
  sqlQuery.bindValue(":value", 1.2f);
  result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(CRES_QVERIFY(result.value<float>(0, 0)), 1.2f);

  sqlQuery.setQuery("SELECT :value");
  sqlQuery.bindValue(":value", 1.2);
  result = sqlQuery.execute();
  QVERIFY(result);
  QCOMPARE(CRES_QVERIFY(result.value<double>(0, 0)), 1.2);
}

void TestSQLQuery::testDatetime()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query sqlQuery
    = c.createSQLQuery("CREATE TABLE dates (id int, timestamp kdb_timestamp);");
  VERIFY_QUERY_EXECUTION(sqlQuery);
  VERIFY_QUERY_EXECUTION_SET(sqlQuery, "INSERT INTO dates(id, timestamp) VALUES(1, NULL)");
  VERIFY_QUERY_EXECUTION_SET(
    sqlQuery, "INSERT INTO dates(id, timestamp) VALUES(2, EXTRACT(EPOCH FROM TIMESTAMP '1999-01-02 "
              "04:05:06') * 1e9)"); // timestamp is in UTC and epoch in sec
  sqlQuery.setQuery("INSERT INTO dates(id, timestamp) VALUES(:id, :timestamp)");
  sqlQuery.bindValue(":id", 3);
  sqlQuery.bindValue(":timestamp", knowCore::Timestamp());
  VERIFY_QUERY_EXECUTION(sqlQuery);
  knowCore::Timestamp now = knowCore::Timestamp::now();
  sqlQuery.bindValue(":id", 4);
  sqlQuery.bindValue(":timestamp", now);
  VERIFY_QUERY_EXECUTION(sqlQuery);

  sqlQuery.setQuery("SELECT * FROM dates ORDER BY id");
  knowDBC::Result r = sqlQuery.execute();
  if(r)
  {
    QCOMPARE(r.tuples(), 4);
    for(int t = 0; t < r.tuples(); ++t)
    {
      QCOMPARE(CRES_QVERIFY(r.value<int>(t, 0)), t + 1);
      switch(t + 1)
      {
      case 1:
        QVERIFY(r.value(t, 1).isEmpty());
        break;
      case 2:
        CRES_QCOMPARE(
          r.value<knowCore::Timestamp>(t, 1, knowCore::TypeCheckingMode::Force),
          knowCore::Timestamp::from(QDateTime(QDate(1999, 01, 02), QTime(4, 5, 6), Qt::UTC)));
        break;
      case 3:
        CRES_QCOMPARE(r.value<knowCore::Timestamp>(t, 1, knowCore::TypeCheckingMode::Force),
                      knowCore::Timestamp());
        break;
      case 4:
        CRES_QCOMPARE(r.value<knowCore::Timestamp>(t, 1, knowCore::TypeCheckingMode::Force),
                      now); // postgresql timestamp is rounded to the second
        break;
      }
    }
  }
  else
  {
    QFAIL(qPrintable("Query: '" + r.query() + "' Error: '" + r.error() + "'"));
  }
}

void TestSQLQuery::testBigNumber()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query sqlQuery
    = c.createSQLQuery("SELECT :number::numeric, -0.000020000000000000002::numeric");
  sqlQuery.bindValue(":number", -0.00002);
  knowDBC::Result r = sqlQuery.execute();
  if(r)
  {
    QCOMPARE(r.tuples(), 1);
    QCOMPARE(r.value(0, 0).value<double>(), -0.00002);
    QCOMPARE(CRES_QVERIFY(r.value(0, 1).value<knowCore::BigNumber>()).toString(),
             QString("-0.000020000000000000002"));
  }
  else
  {
    QFAIL(qPrintable("Query: '" + r.query() + "' Error: '" + r.error() + "'"));
  }
}

void TestSQLQuery::testUnitConversion()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query sqlQuery
    = c.createSQLQuery("SELECT (kdb_quantity_value_convert(kdb_quantity_value_create(1, "
                       "'http://qudt.org/vocab/unit/M'::kdb_uri), "
                       "'http://qudt.org/vocab/unit/MilliM'::kdb_uri)).value");

  knowDBC::Result r = sqlQuery.execute();
  if(r)
  {
    QCOMPARE(r.tuples(), 1);
    CRES_QCOMPARE(r.value(0, 0).value<double>(), 1000);
  }
  else
  {
    QFAIL(qPrintable("Query: '" + r.query() + "' Error: '" + r.error() + "'"));
  }
}

void TestSQLQuery::testJson()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query sqlQuery
    = c.createSQLQuery("CREATE TABLE jsons (id int, value json, valueb jsonb);");
  VERIFY_QUERY_EXECUTION(sqlQuery);
  VERIFY_QUERY_EXECUTION_SET(sqlQuery,
                             "INSERT INTO jsons(id, value, valueb) VALUES(1, NULL, NULL)");
  VERIFY_QUERY_EXECUTION_SET(
    sqlQuery,
    "INSERT INTO jsons(id, value, valueb) VALUES(2, '{ \"test\": 1 }', '{ \"test\": 1 }')");
  sqlQuery.setQuery("INSERT INTO jsons(id, value, valueb) VALUES(:id, :value::json, :value)");
  sqlQuery.bindValue(":id", 3);
  sqlQuery.bindValue(":value", QJsonValue());
  VERIFY_QUERY_EXECUTION(sqlQuery);
  sqlQuery.bindValue(":id", 4);
  sqlQuery.bindValue<QJsonValue>(":value", QJsonArray({10, 12, "bouh"}));
  VERIFY_QUERY_EXECUTION(sqlQuery);
  sqlQuery.bindValue(":id", 5);
  sqlQuery.bindValue<QJsonValue>(":value",
                                 QJsonObject({{"k1", 10}, {"k2", 12}, {"kbouh", "bouh"}}));
  VERIFY_QUERY_EXECUTION(sqlQuery);

  sqlQuery.setQuery("SELECT * FROM jsons ORDER BY id");
  knowDBC::Result r = sqlQuery.execute();
  if(r)
  {
    QCOMPARE(r.tuples(), 5);
    for(int t = 0; t < r.tuples(); ++t)
    {
      QCOMPARE(CRES_QVERIFY(r.value<int>(t, 0)), t + 1);
      for(int j = 1; j <= 2; ++j)
      {
        switch(t + 1)
        {
        case 1:
          QVERIFY(r.value(t, j).isEmpty());
          break;
        case 3:
        {
          QJsonObject obj = CRES_QVERIFY(r.value<QJsonValue>(t, j)).toObject();
          QCOMPARE(obj.keys().size(), 0);
        }
        break;
        case 2:
        {
          QJsonObject obj = CRES_QVERIFY(r.value<QJsonValue>(t, j)).toObject();
          QCOMPARE(obj.keys().size(), 1);
          QCOMPARE(obj.value("test").toInt(), 1);
        }
        break;
        case 4:
        {
          QJsonArray arr = CRES_QVERIFY(r.value<QJsonValue>(t, j)).toArray();
          QCOMPARE(arr.size(), 3);
          QCOMPARE(arr[0].toInt(), 10);
          QCOMPARE(arr[1].toInt(), 12);
          QCOMPARE(arr[2].toString(), QString("bouh"));
        }
        break;
        case 5:
        {
          QJsonObject obj = CRES_QVERIFY(r.value<QJsonValue>(t, j)).toObject();
          QCOMPARE(obj.keys().size(), 3);
          QCOMPARE(obj.value("k1").toInt(), 10);
          QCOMPARE(obj.value("k2").toInt(), 12);
          QCOMPARE(obj.value("kbouh").toString(), QString("bouh"));
        }
        break;
        }
      }
    }
  }
  else
  {
    QFAIL(qPrintable("Query: '" + r.query() + "' Error: '" + r.error() + "'"));
  }

  sqlQuery.setQuery("UPDATE jsons SET valueb = jsonb_set(valueb, :path, '{}', true) WHERE id = 2");
  sqlQuery.bindValue(":path", QStringList({"k3"}));
  VERIFY_QUERY_EXECUTION(sqlQuery);

  sqlQuery.setQuery("UPDATE jsons SET valueb = jsonb_set(valueb, :path, to_json(:value)::jsonb, "
                    "true) WHERE id = 2");
  sqlQuery.bindValue(":path", QStringList({"k3", "k4"}));
  sqlQuery.bindValue(":value", "dah");
  VERIFY_QUERY_EXECUTION(sqlQuery);

  sqlQuery.setQuery("SELECT valueb #>> :path FROM jsons WHERE id = 2");
  sqlQuery.bindValue(":path", QStringList({"k3", "k4"}));
  r = sqlQuery.execute();
  if(r)
  {
    QCOMPARE(r.tuples(), 1);
    QCOMPARE(CRES_QVERIFY(r.value<QString>(0, 0)), QString("dah"));
  }
  else
  {
    QFAIL(qPrintable("Query: '" + r.query() + "' Error: '" + r.error() + "'"));
  }
}

QTEST_MAIN(TestSQLQuery)
