#include <QtTest/QtTest>

class TestMD5 : public QObject
{
  Q_OBJECT
private slots:
  void testBasicTypes();
  void testAggregate();
};
