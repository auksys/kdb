// WARNING ANY CHANGES TO THAT FILE NEEDS TO BE REFLECTED IN THE TUTORIAL IN THE LKS DOCUMENTATION
// WEBSITE

#include "TestTutorials.h"

#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/ssn.h>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/Transaction.h>
#include <kDB/Repository/TripleStore.h>

#define CREATE_CONNECTION                                                                          \
  kDB::Repository::TemporaryStore store;                                                           \
  CRES_QVERIFY(store.start());                                                                     \
  kDB::Repository::Connection connection = store.createConnection();                               \
  CRES_QVERIFY(connection.connect());

using rdf = knowCore::Uris::rdf;
using ssn = knowCore::Uris::ssn;

void TestTutorials::testInsertAPI()
{
  CREATE_CONNECTION

  kDB::Repository::TripleStore map_ts = CRES_QVERIFY(
    connection.graphsManager()->getOrCreateTripleStore("http://askco.re/examples#map"_kCu));

  knowCore::Uri object_uri{"http://askore.re/examples#03643194-0cbe-4701-89e5-8dd1f758e09e"};

  map_ts.insert(
    object_uri, rdf::a, "http://askore.re/examples#table"_kCu,
    "http://askore.re/examples#location"_kCu,
    std::make_tuple("http://askore.re/examples#x"_kCu, knowRDF::Object::fromValue(10.0),
                    "http://askore.re/examples#y"_kCu, knowRDF::Object::fromValue(12.0)),
    ssn::hasProperty,
    std::make_tuple(rdf::a, ssn::Property, rdf::a, "http://askore.re/examples#color"_kCu,
                    ssn::hasValue, "http://askore.re/examples#brown"_kCu),
    ssn::hasProperty,
    std::make_tuple(rdf::a, ssn::Property, rdf::a, "http://askore.re/examples#material"_kCu,
                    ssn::hasValue, "http://askore.re/examples#steel"_kCu));
}

QTEST_MAIN(TestTutorials)
