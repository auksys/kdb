#include <QtTest/QtTest>

class TestVersioning : public QObject
{
  Q_OBJECT
private slots:
  void testEnablingVersioning();
  void testEnablingVersioningInitialRevision();
  void testInsertion();
  void testRemoval();
  void testFastForwardOnlyInsertion();
  void testFastForwardInsertionRemoval();
  void testRevisionReuse();
  void testMergeOnlyInsertion();
  void testMergeInsertionRemoval();
  void testMergeManyHeads();
  void testRebaseSquash();
  void testRebaseNoSquash();
  void testMultithreaded();
  void testHash();
private:
  void testRebase(bool _squash, int _final_revisions);
};
