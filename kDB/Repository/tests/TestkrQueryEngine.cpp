#include "TestkrQueryEngine.h"

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/krQuery/Engine.h>
#include <kDB/Repository/krQuery/Interfaces/Action.h>

#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>

class TestFailAction : public kDB::Repository::krQuery::Interfaces::Action
{
public:
  virtual ~TestFailAction() {}
  cres_qresult<knowCore::Value> execute(const kDB::Repository::krQuery::Context&, const QString&,
                                        const knowCore::ValueHash&) override
  {
    return cres_failure("Expected failure.");
  }
};

class TestAction : public kDB::Repository::krQuery::Interfaces::Action
{
public:
  virtual ~TestAction() {}
  cres_qresult<knowCore::Value> execute(const kDB::Repository::krQuery::Context&,
                                        const QString& _key,
                                        const knowCore::ValueHash& _parameters) override
  {
    if(_key == "also fail")
    {
      return cres_failure("Expected failure.");
    }
    else if(_key == "do succeed")
    {
      if(_parameters.contains("uri")
         and _parameters.value("uri") != knowCore::Value::fromValue("hello world"_kCu))
      {
        return cres_failure("Invalid uri.");
      }
      if(_parameters.contains("value")
         and _parameters.value("value") != knowCore::Value::fromValue(12))
      {
        return cres_failure("Invalid value.");
      }
      if(_parameters.contains("text")
         and _parameters.value("text") != knowCore::Value::fromValue("some text"_kCs))
      {
        return cres_failure("Invalid text.");
      }
      return cres_success(knowCore::Value());
    }
    else
    {
      return cres_failure("Should not happen.");
    }
  }
};
void TestEngine::testQuery()
{
  kDB::Repository::krQuery::Engine e{kDB::Repository::Connection{}};
  e.add({"TRIGGER FAIL"}, new TestFailAction, true);
  TestAction ta;
  e.add({"also fail", "do succeed"}, &ta, false);

  CRES_QVERIFY_FAILURE(e.execute("random action: "));
  CRES_QVERIFY_FAILURE(e.execute("trigger fail: "));
  CRES_QVERIFY_FAILURE(e.execute("also fail: "));
  CRES_QVERIFY(e.execute("do succeed: { uri: 'hello world' }"));
  CRES_QVERIFY(e.execute("do succeed: { uri: 'hello world', value: 12 }"));
  CRES_QVERIFY(e.execute("do succeed: { uri: 'hello world', value: 12, text: \"some text\" }"));

  CRES_QVERIFY_FAILURE(e.execute("do succeed: { uri: 'hello' }"));
  CRES_QVERIFY_FAILURE(e.execute("do succeed: { value: 121 }"));
  CRES_QVERIFY_FAILURE(e.execute("do succeed: { text: \"some wrong text\" }"));

  e.remove(&ta);

  CRES_QVERIFY_FAILURE(e.execute("do succeed: { uri: 'hello world' }"));
  CRES_QVERIFY_FAILURE(e.execute("do succeed: { uri: 'hello world', value: 12 }"));
  CRES_QVERIFY_FAILURE(
    e.execute("do succeed: { uri: 'hello world', value: 12, text: \"some text\" }"));
}

QTEST_MAIN(TestEngine)
