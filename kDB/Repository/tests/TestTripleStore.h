#include <QtTest/QtTest>

class TestTripleStore : public QObject
{
  Q_OBJECT
private slots:
  void testSQL();
  void testInsertion();
  void testMeta();
  void testDeletion();
  void testNotifications();
  void testDefinitionChanges();
  void testDropping();
};
