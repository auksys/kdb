#include <QDebug>
#include <QList>

#include <clog_print>
#include <clog_qt>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

namespace
{
  // This comparison function cheat, it should check if it is the same blank node by comparing other
  // connection. However it is a close enough approximation and cover most cases.
  // TODO use a better algorithm for that (ie "Matching RDF Graphs", Jeremy J. Carroll, LNCS, volume
  // 2342
  bool compare_triple(const knowRDF::Triple& _triple1, const knowRDF::Triple& _triple2)
  {
    return _triple1.predicate() == _triple2.predicate()
           and (_triple1.subject() == _triple2.subject()
                or (_triple1.subject().type() == knowRDF::Subject::Type::BlankNode
                    and _triple2.subject().type() == knowRDF::Subject::Type::BlankNode))
           and (_triple1.object() == _triple2.object()
                or (_triple1.object().type() == knowRDF::Object::Type::BlankNode
                    and _triple2.object().type() == knowRDF::Object::Type::BlankNode));
  }
  bool compare_triples(const QList<knowRDF::Triple>& _triples1,
                       const QList<knowRDF::Triple>& _triples2)
  {
    if(_triples1.size() != _triples2.size())
    {
      clog_error("Different number of triples: {} != {}", _triples1.size(), _triples2.size());
      return false;
    }

    QVector<bool> matched(_triples2.size(), false);

    for(const knowRDF::Triple& triple : _triples1)
    {
      bool found_one = false;
      for(int j = 0; j < _triples2.size(); ++j)
      {
        if(not matched[j] and compare_triple(triple, _triples2[j]))
        {
          matched[j] = true;
          found_one = true;
        }
      }
      if(not found_one)
      {
        clog_error("No match found for triple: {}", triple);
        clog_print<clog_print_flag::blue>("Possible reference triples were");
        for(int j = 0; j < _triples2.size(); ++j)
        {
          clog_print<clog_print_flag::bold>("{}th: {}", j, _triples2[j]);
        }
        clog_print<clog_print_flag::red>("Tested triples were");
        for(int j = 0; j < _triples1.size(); ++j)
        {
          clog_print<clog_print_flag::bold>("{}th: {}", j, _triples1[j]);
        }
        return false;
      }
    }

    return true;
  }
} // namespace
