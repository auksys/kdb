#include <QtTest/QtTest>

class TestSPARQLQuery : public QObject
{
  Q_OBJECT
private slots:
  void test();
  void testSelectFromUnknownGraph();
};
