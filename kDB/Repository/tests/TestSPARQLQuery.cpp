#include "TestSPARQLQuery.h"

#include <knowCore/Test.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uris/xsd.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "../Connection.h"
#include "../GraphsManager.h"
#include "../Store.h"
#include "../TemporaryStore.h"
#include "../TripleStore.h"

namespace Repository = kDB::Repository;
namespace RDF = knowRDF;

void TestSPARQLQuery::test()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());

  knowRDF::Triple triple1(knowCore::Uri("http://test/path/to/subject"),
                          knowCore::Uri("http://test/path/2/is"),
                          knowCore::Uri("http://test/path/2/value"));
  interface.insert(triple1);
  knowRDF::Triple triple2(knowCore::Uri("http://test/path/to/subject"),
                          knowCore::Uri("http://test/path/3/is"),
                          knowCore::Uri("http://test/path/4/value"));
  interface.insert(triple2);

  QCOMPARE(CRES_QVERIFY(interface.triples()).size(), 2);

  knowDBC::Query q = c.createSPARQLQuery({}, "SELECT ?x ?y ?z WHERE {?x ?y ?z};");
  knowDBC::Result r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 3);
  QCOMPARE(r.tuples(), 2);
  CRES_QCOMPARE(r.value<QString>(0, 0), (QString)triple1.subject().uri());
  CRES_QCOMPARE(r.value<QString>(0, 1), (QString)triple1.predicate());
  CRES_QCOMPARE(r.value<QString>(0, 2), (QString)triple1.object().uri());
  CRES_QCOMPARE(r.value<QString>(1, 0), (QString)triple2.subject().uri());
  CRES_QCOMPARE(r.value<QString>(1, 1), (QString)triple2.predicate());
  CRES_QCOMPARE(r.value<QString>(1, 2), (QString)triple2.object().uri());

  q.setQuery("SELECT ?x ?z WHERE {?x ?y ?z};");
  r = q.execute();
  QVERIFY(r);
  QCOMPARE(r.fields(), 2);
  QCOMPARE(r.tuples(), 2);
  CRES_QCOMPARE(r.value<QString>(0, 0), (QString)triple1.subject().uri());
  CRES_QCOMPARE(r.value<QString>(0, 1), (QString)triple1.object().uri());
  CRES_QCOMPARE(r.value<QString>(1, 0), (QString)triple2.subject().uri());
  CRES_QCOMPARE(r.value<QString>(1, 1), (QString)triple2.object().uri());

  q.setQuery("SELECT ?x ?z WHERE {?x <http://test/path/3/is> ?z};");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 2);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), (QString)triple2.subject().uri());
  CRES_QCOMPARE(r.value<QString>(0, 1), (QString)triple2.object().uri());

  q.setQuery("SELECT ?x ?z ?q WHERE {?x <http://test/path/3/is> ?z . ?x ?w ?q };");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 3);
  QCOMPARE(r.tuples(), 2);
  CRES_QCOMPARE(r.value<QString>(0, 0), (QString)triple2.subject().uri());
  CRES_QCOMPARE(r.value<QString>(0, 1), (QString)triple2.object().uri());
  CRES_QCOMPARE(r.value<QString>(0, 2), (QString)triple1.object().uri());
  CRES_QCOMPARE(r.value<QString>(1, 0), (QString)triple2.subject().uri());
  CRES_QCOMPARE(r.value<QString>(1, 1), (QString)triple2.object().uri());
  CRES_QCOMPARE(r.value<QString>(1, 2), (QString)triple2.object().uri());

  q.setQuery(
    "SELECT ?x ?z ?q WHERE {?x <http://test/path/3/is> ?z . ?x ?w ?q FILTER (?q != ?z) };");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 3);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), (QString)triple2.subject().uri());
  CRES_QCOMPARE(r.value<QString>(0, 1), (QString)triple2.object().uri());
  QVERIFY(CRES_QVERIFY(r.value<QString>(0, 1)) != CRES_QVERIFY(r.value<QString>(0, 2)));
  CRES_QCOMPARE(r.value<QString>(0, 2), (QString)triple1.object().uri());

  knowRDF::Triple triple3(
    knowCore::Uri("http://test/path/to/subject1"), knowCore::Uri("http://test/path/3/value"),
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::integer, 10).expect_success());
  interface.insert(triple3);
  knowRDF::Triple triple4(
    knowCore::Uri("http://test/path/to/subject2"), knowCore::Uri("http://test/path/3/value"),
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::decimal, -0.00002).expect_success());
  interface.insert(triple4);
  knowRDF::Triple triple5(
    knowCore::Uri("http://test/path/to/subject3"), knowCore::Uri("http://test/path/3/value"),
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"10"_kCs).expect_success());
  interface.insert(triple5);
  knowRDF::Triple triple6(
    knowCore::Uri("http://test/path/to/subject4"), knowCore::Uri("http://test/path/3/value"),
    knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"English"_kCs, "en")
      .expect_success());
  interface.insert(triple6);
  knowRDF::Triple triple7(knowCore::Uri("http://test/path/to/subject5"),
                          knowCore::Uri("http://test/path/3/value"),
                          knowCore::Uri("http://test/path/4/value"));
  interface.insert(triple7);

  QCOMPARE(CRES_QVERIFY(interface.triples()).size(), 7);

  q.setQuery("SELECT ?x ?z WHERE {?x <http://test/path/3/value> ?z };");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 2);
  QCOMPARE(r.tuples(), 5);
  CRES_QCOMPARE(r.value<QString>(0, 0), (QString)triple3.subject().uri());
  CRES_QCOMPARE(r.value(0, 1).value<knowRDF::Literal>(), triple3.object().literal());
  CRES_QCOMPARE(r.value<QString>(1, 0), (QString)triple4.subject().uri());
  CRES_QCOMPARE(r.value(1, 1).value<knowRDF::Literal>(), triple4.object().literal());
  CRES_QCOMPARE(r.value<QString>(2, 0), (QString)triple5.subject().uri());
  CRES_QCOMPARE(r.value(2, 1).value<knowRDF::Literal>(), triple5.object().literal());
  CRES_QCOMPARE(r.value<QString>(3, 0), (QString)triple6.subject().uri());
  QVERIFY(r.value(3, 1).canConvert<knowRDF::Literal>());
  CRES_QCOMPARE(r.value(3, 1).value<knowRDF::Literal>(), triple6.object().literal());
  CRES_QCOMPARE(r.value<QString>(4, 0), (QString)triple7.subject().uri());
  CRES_QCOMPARE(r.value(4, 1).value<knowCore::Uri>(), triple7.object().uri());

  q.setQuery("SELECT ?x ?z WHERE {?x <http://test/path/3/value> ?z FILTER (?x != "
             "<http://test/path/to/subject1>) };");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 2);
  QCOMPARE(r.tuples(), 4);
  CRES_QCOMPARE(r.value<QString>(0, 0), (QString)triple4.subject().uri());
  CRES_QCOMPARE(r.value(0, 1).value<knowRDF::Literal>(), triple4.object().literal());
  CRES_QCOMPARE(r.value<QString>(1, 0), (QString)triple5.subject().uri());
  CRES_QCOMPARE(r.value(1, 1).value<knowRDF::Literal>(), triple5.object().literal());
  CRES_QCOMPARE(r.value<QString>(2, 0), (QString)triple6.subject().uri());
  QVERIFY(r.value(2, 1).canConvert<knowRDF::Literal>());
  CRES_QCOMPARE(r.value(2, 1).value<knowRDF::Literal>(), triple6.object().literal());
  CRES_QCOMPARE(r.value<QString>(3, 0), (QString)triple7.subject().uri());
  CRES_QCOMPARE(r.value<QString>(3, 1), (QString)triple7.object().uri());

  q.setQuery("SELECT ?x WHERE {?x ?y ?z FILTER (?z = 10) . }");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), (QString)triple3.subject().uri());

  // Test for bindings

  q.setQuery("SELECT ?x WHERE { %url <http://test/path/3/value> ?x }");
  q.bindValue("%url", "http://test/path/to/subject1");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value(0, 0).value<knowRDF::Literal>(), triple3.object().literal());

  q.setQuery("SELECT ?x WHERE { ?x %url 10 }");
  q.bindValue("%url", "http://test/path/3/value");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), QStringLiteral("http://test/path/to/subject1"));

  // Test for literal binding
  q.setQuery("SELECT ?x WHERE { ?x <http://test/path/3/value> %value }");
  q.bindValue("%value", 10);
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), QStringLiteral("http://test/path/to/subject1"));

  // Test for filter binding
  q.setQuery(
    "SELECT ?x WHERE { ?x <http://test/path/3/value> ?value . FILTER( ?value = %value ) }");
  q.bindValue("%value", 10);
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), QStringLiteral("http://test/path/to/subject1"));

  // Test for object uri binding
  q.setQuery("SELECT ?x WHERE { ?x <http://test/path/3/value> %value }");
  q.bindValue("%value", "http://test/path/4/value"_kCu);
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), QStringLiteral("http://test/path/to/subject5"));

  // Test for bindings translated to URI

  q.setQuery("SELECT ?x WHERE { ?x <http://test/path/3/value> <%value> }");
  q.bindValue("%value", "http://test/path/4/value");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), QStringLiteral("http://test/path/to/subject5"));

  // Test for bindings translated from curi to uri

  q.setQuery("PREFIX test: <http://test/>"
             "SELECT ?x WHERE { ?x test:%path 10 }");
  q.bindValue("%path", "path/3/value");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value<QString>(0, 0), QStringLiteral("http://test/path/to/subject1"));

  // Test for two bindings

  q.setQuery("PREFIX test: <http://test/>"
             "SELECT ?x WHERE { %url test:%path ?x }");
  q.bindValue("%url", "http://test/path/to/subject1");
  q.bindValue("%path", "path/3/value");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(r.value(0, 0).value<knowRDF::Literal>(), triple3.object().literal());

  // Test for function call

  q.setQuery("PREFIX test: <http://test/>"
             "SELECT (str(?x) AS ?y) WHERE { ?x test:%path 10 }");
  q.bindValue("%path", "path/3/value");
  r = q.execute();

  QVERIFY(r);
  QCOMPARE(r.fields(), 1);
  QCOMPARE(r.tuples(), 1);
  CRES_QCOMPARE(CRES_QVERIFY(r.value(0, 0).value<knowRDF::Literal>()).value<QString>(),
                QStringLiteral("http://test/path/to/subject1"));
}

void TestSPARQLQuery::testSelectFromUnknownGraph()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  knowDBC::Query q = c.createSPARQLQuery({}, "SELECT * FROM <a> WHERE { ?x ?y ?z . }");
  knowDBC::Result r = q.execute();
  QVERIFY(not r);

  QVERIFY(not c.graphsManager()->hasTripleStore("a"_kCu));
}

QTEST_MAIN(TestSPARQLQuery)
