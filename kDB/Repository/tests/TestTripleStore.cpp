#include "TestTripleStore.h"

#include <knowCore/BigNumber.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>

#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/xsd.h>

#include <knowRDF/BlankNode.h>
#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "../Connection.h"
#include "../GraphsManager.h"
#include "../QueryConnectionInfo.h"
#include "../Store.h"
#include "../TemporaryStore.h"
#include "../TripleStore_p.h"

#include "../DatabaseInterface/PostgreSQL/SQLInterface_p.h"

#include "../Test.h"

using namespace kDB;

typedef QPair<int, int> QPairIntInt;
typedef QPair<QString, int> QPairUriInt;

void TestTripleStore::testSQL()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  QString tablename = CRES_QVERIFY(c.defaultTripleStore()).tablename();

  knowDBC::Query sqlQuery = c.createSQLQuery();
  sqlQuery.setQuery("INSERT INTO " + tablename
                    + " (subject, predicate, object, md5) VALUES ('subject', 'predicate1', "
                      "kdb_rdf_term_create('url', null, null), '')");
  VERIFY_QUERY_EXECUTION(sqlQuery);
  sqlQuery.setQuery("INSERT INTO " + tablename
                    + " (subject, predicate, object, md5) VALUES ('subject', 'predicate2', "
                      "kdb_rdf_term_create('url', kdb_rdf_value_create('1'), ''), '')");
  VERIFY_QUERY_EXECUTION(sqlQuery);
  sqlQuery.setQuery("INSERT INTO " + tablename
                    + " (subject, predicate, object, md5) VALUES ('subject', 'predicate3', "
                      "kdb_rdf_term_create('url', kdb_rdf_value_create('wtf'), 'aa'), '')");
  VERIFY_QUERY_EXECUTION(sqlQuery);
  sqlQuery.setQuery("INSERT INTO " + tablename
                    + " (subject, predicate, object, md5) VALUES ('subject4', 'predicate4', "
                      "kdb_rdf_term_create('url4', kdb_rdf_value_create(?a1), ''), '')");
  sqlQuery.bindValue("?a1", u8"-0.00002"_kCs);
  VERIFY_QUERY_EXECUTION(sqlQuery);

  sqlQuery.setQuery("SELECT subject, predicate, object FROM " + tablename);
  knowDBC::Result sqlResult = sqlQuery.execute();
  QVERIFY(sqlResult);
  QCOMPARE(sqlResult.tuples(), 4);

  QCOMPARE(sqlResult.value(0, 0).toVariant(), QVariant("subject"));
  QCOMPARE(sqlResult.value(1, 0).toVariant(), QVariant("subject"));
  QCOMPARE(sqlResult.value(2, 0).toVariant(), QVariant("subject"));
  QCOMPARE(sqlResult.value(3, 0).toVariant(), QVariant("subject4"));
  QCOMPARE(sqlResult.value(0, 1).toVariant(), QVariant("predicate1"));
  QCOMPARE(sqlResult.value(1, 1).toVariant(), QVariant("predicate2"));
  QCOMPARE(sqlResult.value(2, 1).toVariant(), QVariant("predicate3"));
  QCOMPARE(sqlResult.value(3, 1).toVariant(), QVariant("predicate4"));
  knowCore::Uri obj0 = CRES_QVERIFY(sqlResult.value(0, 2).value<knowCore::Uri>());
  knowRDF::Literal obj1 = CRES_QVERIFY(sqlResult.value(1, 2).value<knowRDF::Literal>());
  knowRDF::Literal obj2 = CRES_QVERIFY(sqlResult.value(2, 2).value<knowRDF::Literal>());
  knowRDF::Literal obj3 = CRES_QVERIFY(sqlResult.value(3, 2).value<knowRDF::Literal>());
  QCOMPARE(obj0, "url"_kCu);
  QCOMPARE((QString)obj1.datatype(), QString("url"));
  QCOMPARE(obj1.value<QString>(), u8"1"_kCs);
  QCOMPARE(obj1.lang(), QString());
  QCOMPARE((QString)obj2.datatype(), QString("url"));
  QCOMPARE(obj2.value<QString>(), QStringLiteral("wtf"));
  QCOMPARE(obj2.lang(), QString("aa"));
  QCOMPARE((QString)obj3.datatype(), QString("url4"));
  QCOMPARE(obj3.value<QString>(), u8"-0.00002"_kCs);
  QCOMPARE(obj3.lang(), QString());
}

void TestTripleStore::testInsertion()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());
  QVERIFY(interface.isValid());

  knowRDF::Triple triple1(knowCore::Uri("http://test/path/to/subject"),
                          knowCore::Uri("http://test/path/2/is"),
                          knowCore::Uri("http://test/path/2/value"));

  interface.insert(triple1);

  QVERIFY(CRES_QVERIFY(interface.hasTriple(triple1)));

  knowRDF::BlankNode bn1, bn2;
  interface.insert(bn1, knowCore::Uri("http://test/path/2/has"), bn2);
  interface.insert(bn2, knowCore::Uri("http://test/path/2/is"),
                   CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::integer, 1)));
  interface.insert(
    bn1, knowCore::Uri("http://test/path/2/is"),
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"test"_kCs)));
  knowRDF::Triple triple2(
    knowCore::Uri("http://test/path/to/subject"), "http://test/path/2/is"_kCu,
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"text"_kCs, "en")));
  interface.insert(triple2);

  interface.insert(knowCore::Uri("http://test/path/to/subject"),
                   knowCore::Uri("http://test/path/4/is"),
                   knowCore::Uri("http://test/path/2/value"));
  interface.insert(
    bn2, knowCore::Uri("http://test/path/2/is_then"),
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::decimal, -0.00002)));

  knowRDF::Triple triple3(
    "http://test/path/to/subject"_kCu, "http://test/path/2/date"_kCu,
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::dateTime, "1001"_kCs, "en")));
  interface.insert(triple3);
  knowRDF::Triple triple4(
    "http://test/path/to/subject"_kCu, "http://test/path/3/date"_kCu,
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::dateTime, quint64(1001), "en",
                                             knowCore::TypeCheckingMode::Force)));
  CRES_QVERIFY(interface.insert(triple4));

  QList<knowRDF::Triple> triples = CRES_QVERIFY(interface.triples());
  QCOMPARE(triples.size(), 9);

  QCOMPARE(triples[0], triple1);
  QCOMPARE(triples[1].subject().type(), knowRDF::Subject::Type::BlankNode);
  QCOMPARE(triples[1].predicate(), knowCore::Uri("http://test/path/2/has"));
  QCOMPARE(triples[1].object().type(), knowRDF::Object::Type::BlankNode);
  QVERIFY(triples[1].object().blankNode() != triples[1].subject().blankNode());
  QCOMPARE(triples[1].subject(), triples[3].subject());
  QCOMPARE(triples[1].object().blankNode(), triples[2].subject().blankNode());

  CRES_QCOMPARE(triples[2].object().literal().value<int>(), 1);
  CRES_QCOMPARE(triples[3].object().literal().value<QString>(), QStringLiteral("test"));

  QCOMPARE(triples[4], triple2);
  QCOMPARE(triples[5].subject().uri(), knowCore::Uri("http://test/path/to/subject"));
  QCOMPARE(triples[5].predicate(), knowCore::Uri("http://test/path/4/is"));
  QCOMPARE(triples[5].object().uri(), knowCore::Uri("http://test/path/2/value"));

  knowDBC::Query sqlQuery = c.createSQLQuery();
  sqlQuery.setQuery("SELECT object, ((object).value).numeric::float FROM " + interface.tablename()
                    + " WHERE predicate='http://test/path/2/is_then'");
  knowDBC::Result sqlResult = sqlQuery.execute();
  QVERIFY(sqlResult);
  QCOMPARE(sqlResult.tuples(), 1);
  QCOMPARE(sqlResult.value(0, 1).toVariant(), QVariant(-0.00002));
  CRES_QCOMPARE(CRES_QVERIFY(sqlResult.value(0, 0).value<knowRDF::Literal>()).value<double>(),
                -0.00002);

  QCOMPARE(triples[6].subject().type(), knowRDF::Subject::Type::BlankNode);
  QCOMPARE(triples[6].predicate(), knowCore::Uri("http://test/path/2/is_then"));
  CRES_QCOMPARE(triples[6].object().literal().value<double>(), -0.00002);

  QCOMPARE(triples[7].subject().uri(), "http://test/path/to/subject"_kCu);
  QCOMPARE(triples[7].predicate(), "http://test/path/2/date"_kCu);
  QCOMPARE(triples[7].object().literal().datatype(), knowCore::Uris::xsd::dateTime);
  QCOMPARE(CRES_QVERIFY(triples[7].object().literal().value<knowCore::Timestamp>())
             .toEpoch<knowCore::NanoSeconds>()
             .count(),
           1001);

  QCOMPARE(triples[8].subject().uri(), "http://test/path/to/subject"_kCu);
  QCOMPARE(triples[8].predicate(), "http://test/path/3/date"_kCu);
  QCOMPARE(triples[8].object().type(), knowRDF::Object::Type::Literal);
  QCOMPARE(triples[8].object().literal().datatype(), knowCore::Uris::xsd::dateTime);
  CRES_QCOMPARE(triples[8].object().literal().value<quint64>(knowCore::TypeCheckingMode::Force),
                quint64(1001));

  interface.clear();
  QCOMPARE(CRES_QVERIFY(interface.triples()).size(), 0);
  interface.insert(triples);

  triples = CRES_QVERIFY(interface.triples());

  QCOMPARE(triples[0], triple1);
  QCOMPARE(triples[1].subject().type(), knowRDF::Subject::Type::BlankNode);
  QCOMPARE(triples[1].predicate(), knowCore::Uri("http://test/path/2/has"));
  QCOMPARE(triples[1].object().type(), knowRDF::Object::Type::BlankNode);
  QVERIFY(triples[1].object().blankNode() != triples[1].subject().blankNode());
  QCOMPARE(triples[1].subject(), triples[3].subject());
  QCOMPARE(triples[1].object().blankNode(), triples[2].subject().blankNode());

  CRES_QCOMPARE(triples[2].object().literal().value<int>(), 1);
  CRES_QCOMPARE(triples[3].object().literal().value<QString>(), QStringLiteral("test"));

  QCOMPARE(triples[4], triple2);
  QCOMPARE(triples[5].subject().uri(), knowCore::Uri("http://test/path/to/subject"));
  QCOMPARE(triples[5].predicate(), knowCore::Uri("http://test/path/4/is"));
  QCOMPARE(triples[5].object().uri(), knowCore::Uri("http://test/path/2/value"));

  sqlResult = sqlQuery.execute();
  QVERIFY(sqlResult);
  QCOMPARE(sqlResult.tuples(), 1);
  QCOMPARE(sqlResult.value(0, 1).toVariant(), QVariant(-0.00002));
  CRES_QCOMPARE(CRES_QVERIFY(sqlResult.value<knowRDF::Literal>(0, 0)).value<double>(), -0.00002);

  QCOMPARE(triples[6].subject().type(), knowRDF::Subject::Type::BlankNode);
  QCOMPARE(triples[6].predicate(), knowCore::Uri("http://test/path/2/is_then"));
  CRES_QCOMPARE(triples[6].object().literal().value<double>(), -0.00002);

  QCOMPARE(triples[7].subject().uri(), "http://test/path/to/subject"_kCu);
  QCOMPARE(triples[7].predicate(), "http://test/path/2/date"_kCu);
  QCOMPARE(triples[7].object().literal().datatype(), knowCore::Uris::xsd::dateTime);
  QCOMPARE(CRES_QVERIFY(triples[7].object().literal().value<knowCore::Timestamp>())
             .toEpoch<knowCore::NanoSeconds>()
             .count(),
           1001);

  QCOMPARE(triples[8].subject().uri(), "http://test/path/to/subject"_kCu);
  QCOMPARE(triples[8].predicate(), "http://test/path/3/date"_kCu);
  QCOMPARE(triples[8].object().literal().datatype(), knowCore::Uris::xsd::dateTime);
  CRES_QCOMPARE(triples[8].object().literal().value<quint64>(knowCore::TypeCheckingMode::Force),
                quint64(1001));
}

void TestTripleStore::testMeta()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());
  QVERIFY(interface.isValid());

  Repository::TripleStore::Definition* definition
    = static_cast<Repository::TripleStore::Private*>(interface.d.data())->definition.data();

  // Set meta with SQLInterface
  CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::setMeta(
    c, interface.uri(), QStringList() << "test", knowCore::ValueHash()));
  CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::setMeta(
    c, interface.uri(), QStringList() << "test" << "ah",
    knowCore::Value::fromValue(QStringLiteral("10"))));
  // Check change in db
  knowCore::Value test
    = CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
      c, interface.uri(), QStringList() << "test"));
  CRES_QCOMPARE(CRES_QVERIFY(test.value<knowCore::ValueHash>()).value<int>("ah"), 10);
  knowCore::Value test_ah
    = CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
      c, interface.uri(), QStringList() << "test" << "ah"));
  CRES_QCOMPARE(test_ah.value<int>(), 10);
  // Check definition update
  KNOWCORE_TEST_WAIT_FOR(
    CRES_QVERIFY(CRES_QVERIFY(definition->getMeta(QStringList() << "test" << "ah")).value<int>())
    == 10);
  CRES_QCOMPARE(CRES_QVERIFY(definition->getMeta(QStringList() << "test" << "ah")).value<int>(),
                10);

  // Drop unexisting data
  CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::dropMeta(
    c, interface.uri(), QStringList() << "test2"));
  test = CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
    c, interface.uri(), QStringList() << "test"));
  CRES_QCOMPARE(CRES_QVERIFY(test.value<knowCore::ValueHash>()).value<int>("ah"), 10);
  // Drop existing data
  CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::dropMeta(
    c, interface.uri(), QStringList() << "test"));
  test = CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
    c, interface.uri(), QStringList() << "test"));
  QVERIFY(test.isEmpty());

  // Create with transaction
  {
    Repository::Transaction transaction(c);
    CRES_QVERIFY(definition->setMeta(QStringList() << "test" << "ah",
                                     knowCore::Value::fromValue(QStringLiteral("20")),
                                     transaction));
    CRES_QVERIFY(transaction.commit());
    CRES_QCOMPARE(CRES_QVERIFY(definition->getMeta(QStringList() << "test" << "ah")).value<int>(),
                  20);
    KNOWCORE_TEST_WAIT_FOR_ACT(
      CRES_QVERIFY(test.value<int>()) == 20,
      test = CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
        c, interface.uri(), QStringList() << "test" << "ah")););
    CRES_QCOMPARE(test.value<int>(), 20);
  }
  // Add an other value
  {
    Repository::Transaction transaction(c);
    CRES_QVERIFY(definition->setMeta(QStringList() << "test" << "beh",
                                     knowCore::Value::fromValue(QStringLiteral("10")),
                                     transaction));
    CRES_QVERIFY(transaction.commit());
    CRES_QCOMPARE(CRES_QVERIFY(definition->getMeta(QStringList() << "test" << "ah")).value<int>(),
                  20);
    CRES_QCOMPARE(CRES_QVERIFY(definition->getMeta(QStringList() << "test" << "beh")).value<int>(),
                  10);
    test = CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
      c, interface.uri(), QStringList() << "test" << "ah"));
    CRES_QCOMPARE(test.value<int>(), 20);
    KNOWCORE_TEST_WAIT_FOR_ACT(
      CRES_QVERIFY(test.value<int>()) == 10,
      test = CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::getMeta(
        c, interface.uri(), QStringList() << "test" << "beh")););
    CRES_QCOMPARE(test.value<int>(), 10);
  }
}

void TestTripleStore::testDeletion()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());
  QVERIFY(interface.isValid());

  knowRDF::Triple triple1(knowCore::Uri("http://test/path/to/subject"),
                          knowCore::Uri("http://test/path/2/is"),
                          knowCore::Uri("http://test/path/2/value"));

  interface.insert(triple1);

  knowRDF::BlankNode bn1, bn2;
  interface.insert(bn1, knowCore::Uri("http://test/path/2/has"), bn2);
  interface.insert(bn2, knowCore::Uri("http://test/path/2/is"),
                   CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::integer, 1)));
  interface.insert(
    bn1, knowCore::Uri("http://test/path/2/is"),
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"test"_kCs)));
  knowRDF::Triple triple2(
    knowCore::Uri("http://test/path/to/subject"), knowCore::Uri("http://test/path/2/is"),
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::string, u8"text"_kCs, "en")));
  interface.insert(triple2);

  interface.insert(knowCore::Uri("http://test/path/to/subject"),
                   knowCore::Uri("http://test/path/4/is"),
                   knowCore::Uri("http://test/path/2/value"));
  interface.insert(
    bn2, knowCore::Uri("http://test/path/2/is_then"),
    CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::decimal, -0.00002)));

  QList<knowRDF::Triple> triples = CRES_QVERIFY(interface.triples());
  QCOMPARE(triples.size(), 7);

  interface.remove(bn2, knowCore::Uri("http://test/path/2/is"),
                   CRES_QVERIFY(knowRDF::Literal::fromValue(knowCore::Uris::xsd::integer, 1)));
  triples = CRES_QVERIFY(interface.triples());
  QCOMPARE(triples.size(), 6);

  triples = CRES_QVERIFY(interface.triples());
  knowRDF::Triple triple_kept = triples.takeFirst();
  CRES_QVERIFY(interface.remove(triples));
  triples = CRES_QVERIFY(interface.triples());
  QCOMPARE(triples.size(), 1);
  QCOMPARE(triples[0], triple_kept);
}

void TestTripleStore::testNotifications()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore interface = CRES_QVERIFY(c.defaultTripleStore());
  QVERIFY(interface.isValid());
  QVERIFY(not interface.options().testFlag(Repository::TripleStore::Option::Notifications));
  CRES_QVERIFY(interface.enableNotifications());
  QVERIFY(interface.options().testFlag(Repository::TripleStore::Option::Notifications));

  QAtomicInt notifications_count = 0;
  QMetaObject::Connection con
    = interface.listen([&notifications_count](const QByteArray&) { ++notifications_count; });

  knowRDF::Triple triple1(knowCore::Uri("http://test/path/to/subject"),
                          knowCore::Uri("http://test/path/2/is"),
                          knowCore::Uri("http://test/path/2/value"));

  interface.insert(triple1);

  KNOWCORE_TEST_WAIT_FOR(notifications_count == 1);
  QCOMPARE((int)notifications_count, 1);

  interface.remove(triple1);

  KNOWCORE_TEST_WAIT_FOR(notifications_count == 2);
  QCOMPARE((int)notifications_count, 2);

  QVERIFY(interface.unlisten(con));

  interface.insert(triple1);

  KNOWCORE_TEST_WAIT_FOR(notifications_count == 2);
  QCOMPARE((int)notifications_count, 2);

  interface.listen([&notifications_count](const QByteArray&) { ++notifications_count; });

  interface.remove(triple1);

  KNOWCORE_TEST_WAIT_FOR(notifications_count == 3);
  QCOMPARE((int)notifications_count, 3);

  QVERIFY(interface.options().testFlag(Repository::TripleStore::Option::Notifications));
  interface.disableNotifications();
  QVERIFY(not interface.options().testFlag(Repository::TripleStore::Option::Notifications));

  interface.insert(triple1);

  KNOWCORE_TEST_WAIT_FOR(notifications_count == 3);
  QCOMPARE((int)notifications_count, 3);
}

void TestTripleStore::testDefinitionChanges()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c1 = store.createConnection();
  CRES_QVERIFY(c1.connect());
  Repository::Connection c2 = store.createConnection();
  CRES_QVERIFY(c2.connect());

  Repository::TripleStore interface1 = CRES_QVERIFY(c1.defaultTripleStore());
  QVERIFY(interface1.isValid());
  Repository::TripleStore interface2 = CRES_QVERIFY(c2.defaultTripleStore());
  QVERIFY(interface2.isValid());

  QAtomicInt options_notifications_count = 0;
  QMetaObject::Connection con1 = interface2.listenOptionsChanged(
    [&options_notifications_count]() { ++options_notifications_count; });

  QAtomicInt meta_notifications_count = 0;
  QMetaObject::Connection con2
    = interface2.listenMetaChanged([&meta_notifications_count]() { ++meta_notifications_count; });

  QCOMPARE((int)options_notifications_count, 0);
  QCOMPARE((int)meta_notifications_count, 0);
  CRES_QVERIFY(interface1.enableNotifications());
  QVERIFY(interface1.options().testFlag(Repository::TripleStore::Option::Notifications));
  KNOWCORE_TEST_WAIT_FOR(
    interface2.options().testFlag(Repository::TripleStore::Option::Notifications));
  QVERIFY(interface2.options().testFlag(Repository::TripleStore::Option::Notifications));
  KNOWCORE_TEST_WAIT_FOR(options_notifications_count == 1);
  QCOMPARE((int)options_notifications_count, 1);
  QCOMPARE((int)meta_notifications_count, 0);

  Repository::TripleStore::Definition* definition1
    = static_cast<Repository::TripleStore::Private*>(interface1.d.data())->definition.data();
  Repository::TripleStore::Definition* definition2
    = static_cast<Repository::TripleStore::Private*>(interface2.d.data())->definition.data();
  CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::setMeta(
    c1, interface1.uri(), QStringList() << "test", knowCore::ValueHash()));
  KNOWCORE_TEST_WAIT_FOR(meta_notifications_count == 1);
  CRES_QVERIFY(Repository::DatabaseInterface::PostgreSQL::SQLInterface::setMeta(
    c1, interface1.uri(), QStringList() << "test" << "ah", knowCore::Value::fromValue(10)));
  KNOWCORE_TEST_WAIT_FOR(
    not CRES_QVERIFY(definition2->getMeta(QStringList() << "test" << "ah")).isEmpty());
  KNOWCORE_TEST_WAIT_FOR(
    CRES_QVERIFY(CRES_QVERIFY(definition2->getMeta(QStringList() << "test" << "ah")).value<int>())
    == 10);
  CRES_QCOMPARE(CRES_QVERIFY(definition2->getMeta(QStringList() << "test" << "ah")).value<int>(),
                10);
  KNOWCORE_TEST_WAIT_FOR(definition1->meta == definition2->meta);
  QCOMPARE(definition1->meta, definition2->meta);

  KNOWCORE_TEST_WAIT_FOR(meta_notifications_count == 2);
  QCOMPARE((int)options_notifications_count, 1);
  QCOMPARE((int)meta_notifications_count, 2);
}

void TestTripleStore::testDropping()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c1 = store.createConnection();
  CRES_QVERIFY(c1.connect());

  Repository::TripleStore interface1 = CRES_QVERIFY(c1.graphsManager()->createTripleStore("a"_kCu));
  QVERIFY(interface1.isValid());
  c1.disconnect();
  CRES_QVERIFY(store.store()->stop());
  CRES_QVERIFY(store.store()->start());
  c1 = store.createConnection();
  CRES_QVERIFY(c1.connect());
  interface1 = CRES_QVERIFY(c1.graphsManager()->getTripleStore("a"_kCu));
  CRES_QVERIFY(c1.graphsManager()->removeTripleStore("a"_kCu));
  CRES_QVERIFY_FAILURE(c1.graphsManager()->removeTripleStore("a"_kCu));
  QVERIFY(not interface1.isValid());

  interface1 = CRES_QVERIFY(c1.graphsManager()->createTripleStore("a"_kCu));
  QVERIFY(interface1.isValid());
  QCOMPARE(CRES_QVERIFY(interface1.triples()).size(), 0);

  Repository::Connection c2 = store.createConnection();
  CRES_QVERIFY(c2.connect());
  CRES_QVERIFY(c2.graphsManager()->removeTripleStore("a"_kCu));
  KNOWCORE_TEST_WAIT_FOR(not c1.graphsManager()->hasTripleStore("a"_kCu));
  QVERIFY(not c1.graphsManager()->hasTripleStore("a"_kCu));

  CRES_QVERIFY(c1.graphsManager()->createTripleStore("b"_kCu));
  for(int i = 0; i < 10; ++i)
  {
    CRES_QVERIFY(c2.graphsManager()->removeTripleStore("b"_kCu));
    Repository::TripleStore interface2
      = CRES_QVERIFY(c2.graphsManager()->createTripleStore("b"_kCu));
    interface2.insert("a"_kCu, "b"_kCu, "c"_kCu);
    Repository::TripleStore interface1 = CRES_QVERIFY(c1.graphsManager()->getTripleStore("b"_kCu));
    KNOWCORE_TEST_WAIT_FOR(
      CRES_QVERIFY(c1.graphsManager()->getTripleStore("b"_kCu)).triples().is_successful());
    CRES_QVERIFY(CRES_QVERIFY(c1.graphsManager()->getTripleStore("b"_kCu)).triples());
  }
}

QTEST_MAIN(TestTripleStore)
