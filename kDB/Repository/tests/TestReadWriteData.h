#include <QtTest/QtTest>

class TestReadWriteData : public QObject
{
  Q_OBJECT
private slots:
  void testWrite();
  void testRead();
  void testCopy();
};
