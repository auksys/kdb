#include "TestNotifications.h"

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "../Connection.h"
#include "../DatabaseInterface/PostgreSQL/SQLInterface_p.h"
#include "../GraphsManager.h"
#include "../NotificationsManager.h"
#include "../TemporaryStore.h"
#include "../TripleStore.h"

#include "../Test.h"

namespace kDBr = kDB::Repository;

void TestNotifications::testNotify()
{
  kDBr::TemporaryStore store;
  CRES_QVERIFY(store.start());
  kDBr::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  int counter = 0;
  c.notificationsManager()->listen("test_notification",
                                   [&counter](const QByteArray&) { ++counter; });

  knowDBC::Query q = c.createSQLQuery("NOTIFY test_notification");
  QVERIFY(q.execute());

  KNOWCORE_TEST_WAIT_FOR(counter != 0);
  QCOMPARE(counter, 1);
}

QTEST_MAIN(TestNotifications)
