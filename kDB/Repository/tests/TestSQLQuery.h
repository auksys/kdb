#include <QtTest/QtTest>

class TestSQLQuery : public QObject
{
  Q_OBJECT
private slots:
  void testInsertSelect();
  void testDatetime();
  void testBigNumber();
  void testUnitConversion();
  void testJson();
};
