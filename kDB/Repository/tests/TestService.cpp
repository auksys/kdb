#include "TestService.h"

#include <clog_qt>
#include <cres_qt>

#include <knowCore/Test.h>
#include <knowCore/Uri.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "../AbstractService.h"
#include "../Connection.h"
#include "../GraphsManager.h"
#include "../Services.h"
#include "../TemporaryStore.h"
#include "../TripleStore.h"

using namespace kDB;

struct DummyService : public Repository::AbstractService
{
  Repository::Connection connection;
  bool canCall(const knowCore::Uri& _service) const override { return _service == "service_c"; }
  knowDBC::Result call(const knowCore::Uri& /*_service*/, const QString& _query,
                       const knowCore::ValueHash& _bindings) const override
  {
    knowDBC::Query sq = connection.createSPARQLQuery({}, _query);
    sq.bindValues(_bindings);
    return sq.execute();
  }
};

void TestService::testServiceCall()
{
  Repository::TemporaryStore store;
  CRES_QVERIFY(store.start());
  Repository::Connection c = store.createConnection();
  CRES_QVERIFY(c.connect());

  Repository::TripleStore store_a
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore("sa"_kCu));
  Repository::TripleStore store_b
    = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore("sb"_kCu));

  store_a.insert("a"_kCu, "service_name"_kCu, "service_c"_kCu);
  store_a.insert("a"_kCu, "graph_name"_kCu, "sb"_kCu);
  store_b.insert("a"_kCu, "e"_kCu, "f"_kCu);

  Repository::Services services;
  DummyService* ds = new DummyService;
  ds->connection = c;
  services.addService(ds);

  knowDBC::Query sq
    = c.createSPARQLQuery({services}, "SELECT ?a ?f FROM <sa> WHERE { ?a <service_name> ?c ; "
                                      "<graph_name> ?d SERVICE ?c FROM ?d { ?a <e> ?f } LIMIT 1 }");
  knowDBC::Result sr = sq.execute();
  QVERIFY(sr);
  QCOMPARE(sr.tuples(), 1);
  QCOMPARE(sr.fields(), 2);
  QCOMPARE(sr.value(0, 0).value<knowCore::Uri>(), "a"_kCu);
  QCOMPARE(sr.value(0, 1).value<knowCore::Uri>(), "f"_kCu);
}

QTEST_MAIN(TestService)
