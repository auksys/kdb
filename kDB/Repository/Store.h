#pragma once

#include <QSharedPointer>
#include <kDB/Forward.h>

#include "NamedTypes.h"

class QDir;

namespace kDB
{
  namespace Repository
  {
    /**
     * Represent a postgresql server, allow it to start, stop and create connection to it.
     */
    class Store
    {
      Q_DISABLE_COPY(Store)
    public:
      /**
       * @return a directory with the \p _section and \p _name from the standard directory.
       *         On linux it would be .local/share/auksys/kdb/stores/section/name
       */
      static QDir standardDir(const SectionName& _section, const StoreName& _name);
      static QDir standardDir(const StoreName& _name);
    public:
      Store(const QDir& _storage, int _port = 1242);
      Store(const SectionName& _section, const StoreName& _store, int _port);
      Store(const StoreName& _store, int _port);
      ~Store();
      QDir directory() const;
      int port() const;
      /**
       * Starts the store if needed (if the store is already started, the object does not
       * take control of the instance, and it won't be stopped when the object is deleted)
       *
       * \return true if the server was started or running, false if it failed to start
       */
      cres_qresult<void> startIfNeeded();
      /**
       * Starts the store. And takes control of the server, when this object is deleted the server
       * will be stopped.
       *
       * \return false if the server failed to start or was already running
       */
      cres_qresult<void> start();
      cres_qresult<void> restart(bool _force = false);
      /**
       * Stop the server (even if it is not controlled!).
       */
      cres_qresult<void> stop(bool _force = false);
      /**
       * This will erase all the content of the database.
       */
      cres_qresult<void> erase();
      /**
       * Create a connection to that store.
       */
      Connection createConnection() const;
      /**
       * @return true if this object is controlling the store. If set to true, the store will be
       * stopped when this object is deleted, unless a call to \ref detach is made
       */
      bool isControlling() const;
      /**
       * detach this instance of the store, and release control, i.e., the postgresql database will
       * not be stopped automatically when this object is deleted.
       */
      void detach();
      bool isRunning() const;
      /**
       * Attempt to guess the port, it only works if the store has already been started.
       */
      void autoSelectPort();
      /**
       * Set the configuration of server, convenient overload.
       */
      cres_qresult<void> setConfiguration(const QString& _key, const QString& _value,
                                          bool _restart_server = false);
      /**
       * Set the configuration of the postgresql server.
       *
       * This function start the server if needed (and then stop if it was started this way).
       * Some changes to configuration require the server to be restarted, this can be done
       * if \ref _restart_server is set to true (default is false)
       */
      cres_qresult<void> setConfiguration(const QList<QPair<QString, QString>>& _value,
                                          bool _restart_server = false);
    private:
      struct Private;
      Private* const d;
    };
  } // namespace Repository
} // namespace kDB
