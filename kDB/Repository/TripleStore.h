#pragma once

#include <functional>

#include <QList>
#include <QSharedPointer>

#include <clog_qt>
#include <cres_qt>

#include "RDFDataset.h"
#include "Transaction.h"

class TestTripleStore;

namespace kDB::Repository
{
  class TripleStore : public RDFDataset
  {
    friend class ::TestTripleStore;
    friend class GraphsManager;
    friend class VersionControl::Manager;
    friend class VersionControl::Transaction;
    friend class DatabaseInterface::PostgreSQL::SQLInterface;
    friend class VersionControl::Utils::MetaVersion;
  public:
    enum class Option : quint32
    {
      None = 0x0,
      Versioning = 0x1,
      Notifications = 0x2
    };
    Q_DECLARE_FLAGS(Options, Option)
  private:
    struct Definition;
    TripleStore(const kDB::Repository::Connection& _connection,
                QSharedPointer<Definition> _definition);
  public:
    TripleStore();
    TripleStore(const TripleStore& _rhs);
    TripleStore& operator=(const TripleStore& _rhs);
    ~TripleStore();
    /**
     * @return a TripleStore instance that has seperate local data, can be used in a different
     * thread
     */
    TripleStore detach() const;
    QString tablename() const;
    Options options() const;
    template<typename _ObjectType_>
    cres_qresult<void> insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicateBase,
                              const QString& _predicateSuffix, const _ObjectType_& _object,
                              const Transaction& _transaction = Transaction())
    {
      return insert(_subject, knowCore::Uri(_predicateBase, _predicateSuffix), _object,
                    _transaction);
    }
    template<typename _TUri_>
      requires knowCore::Uris::IsUriDefinition<_TUri_>::value
    cres_qresult<void> insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const _TUri_& _value,
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const knowCore::Uri& _value,
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const knowRDF::Object& _object,
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const knowCore::Uri& _type, const knowCore::Value& _value,
                              const QString& _lang = QString(),
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const knowRDF::BlankNode& _value,
                              const Transaction& _transaction = Transaction());
    template<typename _TPredicate_0_, typename _TObject_0_, typename... _TPredicteObjects_>
    cres_qresult<void>
      insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
             const std::tuple<_TPredicate_0_, _TObject_0_, _TPredicteObjects_...>& _objects,
             const Transaction& _transaction);
    cres_qresult<void> insert(const knowRDF::Triple& _triple,
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> insert(const QList<knowRDF::Triple>& _triple,
                              const Transaction& _transaction = Transaction());

    template<typename _TObject_, typename... _TArgs_>
    cres_qresult<void> insert(const Transaction& _transaction, const knowRDF::Subject& _subject,
                              const knowCore::Uri& _predicate, const _TObject_& _object,
                              const _TArgs_&...);
    template<typename _TObject_, typename... _TArgs_>
    inline cres_qresult<void> insert(const knowRDF::Subject& _subject,
                                     const knowCore::Uri& _predicate, const _TObject_& _object,
                                     const knowCore::Uri& _predicate_next, const _TArgs_&... _args);
  private:
    // Thiss functions is only used as part of the end recursion for the variadic insert above
    cres_qresult<void> insert(const Transaction&, const knowRDF::Subject&);
  public:
    cres_qresult<bool> hasTriple(const knowRDF::Triple& _triple,
                                 const Transaction& _transaction = Transaction());

    cres_qresult<void> remove(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const knowCore::Uri& _value,
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> remove(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const knowRDF::Object& _object,
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> remove(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const knowCore::Uri& _type, const knowCore::Value& _value,
                              const QString& _lang = QString(),
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> remove(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                              const knowRDF::BlankNode& _value,
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> remove(const knowRDF::Triple& _triple,
                              const Transaction& _transaction = Transaction());
    cres_qresult<void> remove(const QList<knowRDF::Triple>& _triple,
                              const Transaction& _transaction = Transaction());

    /**
     * @return the triples from the triple store. Equivalent to "SELECT ?x ?y ?z WHERE { ?x ?y ?z .
     * };" query,
     */
    cres_qresult<QList<knowRDF::Triple>> triples() const;

    /**
     * @return a random triple
     */
    knowRDF::Triple triple(std::size_t _r) const;

    /**
     * @return the number of triples
     */
    std::size_t triplesCount() const;
    /**
     * @return an INSERT DATA query with all the tripless
     */
    QString generateInsertDataQuery(bool _blankNodeAsUri = false) const;

    /**
     * Delete all the triples from the store
     */
    cres_qresult<void> clear(const Transaction& _transaction = Transaction());

    cres_qresult<void> enableVersioning();
    cres_qresult<void> disableVersioning();

    cres_qresult<void> enableNotifications();
    cres_qresult<void> disableNotifications();

    QMetaObject::Connection listen(const QObject* receiver, const char* member,
                                   Qt::ConnectionType _type = Qt::AutoConnection);
    QMetaObject::Connection listen(const std::function<void(const QByteArray&)>& _receiver);

    QMetaObject::Connection listenOptionsChanged(const QObject* receiver, const char* member,
                                                 Qt::ConnectionType _type = Qt::AutoConnection);
    QMetaObject::Connection listenOptionsChanged(const std::function<void()>& _receiver);

    QMetaObject::Connection listenMetaChanged(const QObject* receiver, const char* member,
                                              Qt::ConnectionType _type = Qt::AutoConnection);
    QMetaObject::Connection listenMetaChanged(const std::function<void()>& _receiver);

    bool unlisten(QMetaObject::Connection _connection);

    /**
     * @return the version control manager associated with this triples store, or null, if
     * versionning is not enabled
     */
    VersionControl::Manager* versionControlManager();
    const VersionControl::Manager* versionControlManager() const;

    /**
     * @return the md5 of the content of this triple store.
     */
    QByteArray contentHash(const Transaction& _transaction = Transaction()) const;
  private:
    struct TSTemporaryTransaction;
    KNOWCORE_D_DECL();
  };
  inline cres_qresult<void> TripleStore::insert(const Transaction&, const knowRDF::Subject&)
  {
    return cres_success();
  }
  template<typename _TUri_>
    requires knowCore::Uris::IsUriDefinition<_TUri_>::value
  inline cres_qresult<void>
    TripleStore::insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                        const _TUri_& _value, const Transaction& _transaction)
  {
    return insert(_subject, _predicate, knowCore::Uri(_value), _transaction);
  }

  template<typename _TObject_, typename... _TArgs_>
  inline cres_qresult<void> TripleStore::insert(const Transaction& _transaction,
                                                const knowRDF::Subject& _subject,
                                                const knowCore::Uri& _predicate,
                                                const _TObject_& _object, const _TArgs_&... _args)
  {
    cres_try(cres_ignore, insert(_subject, _predicate, _object, _transaction));
    return insert(_transaction, _subject, _args...);
  }
  template<typename _TObject_, typename... _TArgs_>
  inline cres_qresult<void>
    TripleStore::insert(const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
                        const _TObject_& _object, const knowCore::Uri& _predicate_next,
                        const _TArgs_&... _args)
  {
    Transaction t(connection());
    cres_try(cres_ignore,
             insert(t, _subject, knowCore::Uri(_predicate), _object, _predicate_next, _args...));
    return t.commit();
  }
  template<typename _TPredicate_0_, typename _TObject_0_, typename... _TPredicteObjects_>
  cres_qresult<void> TripleStore::insert(
    const knowRDF::Subject& _subject, const knowCore::Uri& _predicate,
    const std::tuple<_TPredicate_0_, _TObject_0_, _TPredicteObjects_...>& _objects,
    const Transaction& _transaction)
  {
    knowRDF::BlankNode bn;
    cres_try(cres_ignore, insert(_subject, _predicate, bn, _transaction));
    using IFT = cres_qresult<void> (TripleStore::*)(const Transaction&, const knowRDF::Subject&,
                                                    const knowCore::Uri&, const _TObject_0_&,
                                                    const _TPredicteObjects_&...);
    return std::apply(static_cast<IFT>(&TripleStore::insert<_TObject_0_, _TPredicteObjects_...>),
                      std::tuple_cat(std::make_tuple(this, _transaction, bn), _objects));
  }

} // namespace kDB::Repository

Q_DECLARE_OPERATORS_FOR_FLAGS(kDB::Repository::TripleStore::Options)
