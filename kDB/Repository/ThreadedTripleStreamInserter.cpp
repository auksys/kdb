#include "ThreadedTripleStreamInserter.h"

#include <QMutex>
#include <QMutexLocker>
#include <QThread>
#include <QWaitCondition>

#include <clog_chrono>
#include <clog_qt>

#include <knowRDF/Triple.h>

#include "Connection.h"
#include "TripleStore.h"

using namespace kDB::Repository;

struct ThreadedTripleStreamInserter::Private : public QThread
{
  Private(const TripleStore& _triplesStore) : triplesStore(_triplesStore) {}
  virtual void run()
  {
    running = true;
    while(true)
    {
      QList<knowRDF::Triple> queue;
      {
        QMutexLocker l(&m);
        std::swap(queue, triplesQueue);
        condition.wakeAll();
      }
      clog_chrono(ProcessedTriplesQueue);
      if(queue.size() > 0)
      {
        triplesStore.insert(queue);
      }
      else if(exitWhenEmptyQueue)
      {
        running = false;
        return;
      }
      else if(running)
      {
        QMutexLocker l(&m);
        condition.wait(&m);
      }
      else
      {
        return;
      }
    }
  }
  bool running;
  QWaitCondition condition;
  QMutex m;
  QList<knowRDF::Triple> triplesQueue;
  TripleStore triplesStore;
  bool exitWhenEmptyQueue = false;
  int max_queue_size;
};

ThreadedTripleStreamInserter::ThreadedTripleStreamInserter(const TripleStore& _tripleStore,
                                                           int _max_queue_size)
    : d(new Private(_tripleStore))
{
  d->max_queue_size = _max_queue_size;
  d->start();
}

ThreadedTripleStreamInserter::~ThreadedTripleStreamInserter()
{
  d->running = false;
  d->condition.wakeAll();
  d->wait();
  d->deleteLater();
}

void ThreadedTripleStreamInserter::triple(const knowRDF::Triple& _triple)
{
  while(true)
  {
    QMutexLocker l(&d->m);
    if(d->triplesQueue.size() < d->max_queue_size)
    {
      break;
    }
    else
    {
      d->condition.wait(&d->m);
    }
  }
  QMutexLocker l(&d->m);
  d->triplesQueue.append(_triple);
  d->condition.wakeAll();
}

void ThreadedTripleStreamInserter::waitForFinished()
{
  d->exitWhenEmptyQueue = true;
  d->wait();
}
