#include "PLExecutorVisitor.h"

#include "QueryExecutorVisitor_p.h"

using namespace kDB::Repository::SPARQLExecution;

cres_qresult<void> PLExecutorVisitor::executeQuery(const kDB::SPARQL::Algebra::NodeCSP& _query)
{
  QueryExecutorVisitor qev;
  qev.defaultDataset = defaultDataset;
  qev.namedDatasets = namedDatasets;
  qev.connection = transaction;
  qev.query_text = query_text;
  qev.start(_query, QVariant());
  return reportResult(qev.result);
}

#define PL_EXECUTE_QUERY(TYPE)                                                                     \
  cres_qresult<void> PLExecutorVisitor::visit(kDB::SPARQL::Algebra::TYPE##CSP _query)              \
  {                                                                                                \
    return executeQuery(_query);                                                                   \
  }

PL_EXECUTE_QUERY(AskQuery)
PL_EXECUTE_QUERY(InsertData)
PL_EXECUTE_QUERY(DeleteData)
PL_EXECUTE_QUERY(Load)
PL_EXECUTE_QUERY(Drop)
PL_EXECUTE_QUERY(Clear)
PL_EXECUTE_QUERY(Create)
PL_EXECUTE_QUERY(DeleteInsert)
PL_EXECUTE_QUERY(PLQuery)
PL_EXECUTE_QUERY(ConstructQuery)
PL_EXECUTE_QUERY(SelectQuery)
PL_EXECUTE_QUERY(ExplainQuery)

cres_qresult<void> PLExecutorVisitor::visit(kDB::SPARQL::Algebra::ListCSP _query)
{
  Q_UNUSED(_query);
  for(kDB::SPARQL::Algebra::NodeCSP node : _query->list())
  {
    cres_try(cres_ignore, accept(node));
    if(hasFailed())
    {
      return cres_success();
    }
  }
  return cres_success();
}

template<bool _R_, typename _Q_>
cres_qresult<void>
  PLExecutorVisitor::executeIfUnless(knowCore::ConstExplicitlySharedDataPointer<_Q_> _query)
{
  executeQuery(_query->condition());
  if(hasFailed())
    return cres_success();
  clog_assert((result.type() == knowDBC::Result::Type::Boolean));
  if(result.boolean() == _R_)
  {
    return accept(_query->ifStatements());
  }
  else
  {
    return accept(_query->elseStatements());
  }
}

cres_qresult<void> PLExecutorVisitor::visit(kDB::SPARQL::Algebra::IfCSP _query)
{
  return executeIfUnless<true>(_query);
}

cres_qresult<void> PLExecutorVisitor::visit(kDB::SPARQL::Algebra::UnlessCSP _query)
{
  return executeIfUnless<false>(_query);
}
