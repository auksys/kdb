#include <kDB/SPARQL/Algebra/AbstractNodeVisitor.h>

#include <knowDBC/Result.h>

#include "../RDFDataset.h"
#include "../Transaction.h"

namespace kDB::Repository::SPARQLExecution
{
  struct PLExecutorVisitor : public kDB::SPARQL::Algebra::AbstractNodeVisitor<cres_qresult<void>>
  {
    RDFDataset defaultDataset;
    QList<RDFDataset> namedDatasets;
    Transaction transaction;
    QString query_text;
    knowDBC::Result result;
    bool hasFailed() const { return result.type() == knowDBC::Result::Type::Failed; }
    cres_qresult<void> reportResult(const knowDBC::Result& _result)
    {
      result = _result;
      return cres_success();
    }
    cres_qresult<void> reportError(const QString& _errmsg)
    {
      return reportResult(knowDBC::Result::create(query_text, _errmsg));
    }
    cres_qresult<void> executeQuery(const kDB::SPARQL::Algebra::NodeCSP& _query);
    cres_qresult<void> visit(kDB::SPARQL::Algebra::InsertDataCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::DeleteDataCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::LoadCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::DropCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::ClearCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::CreateCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::DeleteInsertCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::PLQueryCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::QuadsCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::QuadsDataCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::ListCSP _query);
    cres_qresult<void> visit(kDB::SPARQL::Algebra::VariableCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::DatasetCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::GroupClausesCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::HavingClausesCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::DescribeQueryCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::DescribeTermCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }

    cres_qresult<void> visit(kDB::SPARQL::Algebra::AskQueryCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::ConstructQueryCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::SelectQueryCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::ExplainQueryCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::OrderClausesCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::OrderClauseCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::LimitOffsetClauseCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::TripleCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::VariableReferenceCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::GraphReferenceCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::TermCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::BlankNodeCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::ServiceCSP _node) override
    {
      Q_UNUSED(_node);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::GroupGraphPatternCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::ValueCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::OptionalCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::UnionCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::MinusCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::LogicalOrCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::LogicalAndCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::RelationalDifferentCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::RelationalEqualCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::RelationalInferiorCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::RelationalInferiorEqualCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::RelationalSuperiorCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::RelationalSuperiorEqualCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::AdditionCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::SubstractionCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::MultiplicationCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::DivisionCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::RelationalInCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::RelationalNotInCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::LogicalNegationCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::NegationCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::FunctionCallCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::ExecuteCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    cres_qresult<void> visit(kDB::SPARQL::Algebra::BindCSP _query) override
    {
      Q_UNUSED(_query);
      return cres_failure("Unimplemented");
    }
    template<bool _R_, typename _Q_>
    cres_qresult<void> executeIfUnless(knowCore::ConstExplicitlySharedDataPointer<_Q_> _query);
    cres_qresult<void> visit(kDB::SPARQL::Algebra::IfCSP _query) override;
    cres_qresult<void> visit(kDB::SPARQL::Algebra::UnlessCSP _query) override;
  };
} // namespace kDB::Repository::SPARQLExecution
