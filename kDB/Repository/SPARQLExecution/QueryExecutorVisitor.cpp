#include "QueryExecutorVisitor_p.h"

#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <knowCore/Timestamp.h>

#include "config.h"
#include <knowCore/Messages.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>
#include <knowCore/ValueList.h>

#include <knowRDF/Skolemisation.h>
#include <knowRDF/TripleStream.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/SPARQL/Algebra/NodeVisitor.h>
#include <kDB/SPARQL/Algebra/Utils.h>
#include <kDB/SPARQL/Algebra/Visitors/Serialiser.h>

#include "../Connection_p.h"
#include "../DatasetsUnion.h"
#include "../EmptyRDFDataset.h"
#include "../GraphsManager.h"
#include "../Logging.h"
#include "../PersistentDatasetsUnion.h"
#include "../TemporaryTransaction.h"
#include "../ThreadedTripleStreamInserter.h"
#include "../Transaction.h"
#include "../TripleStore.h"
#include "../TriplesView.h"

#include "PLExecutorVisitor.h"
#include "SPARQLAlgebraToPostgresSQL_p.h"

#include "../DatabaseInterface/PostgreSQL/SQLInterface_p.h"

#include <knowCore/Uris/askcore_graph.h>

using namespace kDB::Repository::SPARQLExecution;

using askcore_graph = knowCore::Uris::askcore_graph;

namespace kDB::Repository::SPARQLExecution
{
  class InspectQueryVisitor : public kDB::SPARQL::Algebra::NodeVisitor<void>
  {
  public:
    void visit(kDB::SPARQL::Algebra::GroupGraphPatternCSP _node) override
    {
      if(m_top_group_node == nullptr)
      {
        m_top_group_node = _node;
        m_services = _node->services();
      }
      else
      {
        if(not _node->services().isEmpty())
        {
          m_error = "SERVICEs are only allowed on top group.";
        }
      }
    }
    QString errorMessage() const { return m_error; }
    bool hasServices() const { return not m_services.isEmpty(); }
    QList<kDB::SPARQL::Algebra::ServiceCSP> services() const { return m_services; }
  private:
    QString m_error;
    kDB::SPARQL::Algebra::GroupGraphPatternCSP m_top_group_node = nullptr;
    QList<kDB::SPARQL::Algebra::ServiceCSP> m_services;
  };
} // namespace kDB::Repository::SPARQLExecution

#define CHECK_RETURN_VOID_VALUE(_expr_, _msg_, ...)                                                \
  {                                                                                                \
    auto const& [s, m] = _expr_;                                                                   \
    if(not s)                                                                                      \
      return reportError(clog_qt::qformat(_msg_, m.value() __VA_OPT__(, ) __VA_ARGS__));           \
  }

#define CHECK_RETURN_ASSIGN_VALUE(_var_, _expr_, _msg_, ...)                                       \
  {                                                                                                \
    auto const& [s, v, m] = _expr_;                                                                \
    if(not s)                                                                                      \
      return reportError(clog_qt::qformat(_msg_, m.value() __VA_OPT__(, ) __VA_ARGS__));           \
    _var_ = v.value();                                                                             \
  }

#define DEFAULT_TRIPLES_STORE                                                                      \
  TripleStore defaultTripleStore;                                                                  \
  if(not defaultDataset.isValid())                                                                 \
  {                                                                                                \
    CHECK_RETURN_ASSIGN_VALUE(defaultTripleStore, connection.connection().defaultTripleStore(),    \
                              "{}");                                                               \
  }                                                                                                \
  else if(defaultDataset.type() != RDFDataset::Type::TripleStore)                                  \
  {                                                                                                \
    reportError("SPARQL Query update should be applied on a triples store.");                      \
    return QVariant();                                                                             \
  }                                                                                                \
  else                                                                                             \
  {                                                                                                \
    defaultTripleStore = defaultDataset.toTripleStore();                                           \
  }

#define GET_TRIPLES_STORE                                                                          \
  TripleStore ts = defaultTripleStore;                                                             \
  if(q->source())                                                                                  \
  {                                                                                                \
    switch(q->source()->type())                                                                    \
    {                                                                                              \
    case kDB::SPARQL::Algebra::NodeType::VariableReference:                                        \
    {                                                                                              \
      return reportError("Graph should be defined as a URI not a variable");                       \
    }                                                                                              \
    case kDB::SPARQL::Algebra::NodeType::GraphReference:                                           \
    {                                                                                              \
      CHECK_RETURN_ASSIGN_VALUE(                                                                   \
        ts,                                                                                        \
        connection.connection().graphsManager()->getOrCreateTripleStore(                           \
          static_cast<const kDB::SPARQL::Algebra::GraphReference*>(q->source().data())->name()),   \
        "{}");                                                                                     \
      break;                                                                                       \
    }                                                                                              \
    default:                                                                                       \
      qFatal("Internal error!");                                                                   \
    }                                                                                              \
  }

#define EXECUTE_QUERY(_MSG_, _QUERY_, ...)                                                         \
  {                                                                                                \
    auto result = _QUERY_.execute();                                                               \
    if(not result)                                                                                 \
    {                                                                                              \
      return reportError(clog_qt::qformat(KDB_REPOSITORY_REPORT_QUERY_ERROR_FORMATED_MSG(          \
        _MSG_, result __VA_OPT__(, ) __VA_ARGS__)));                                               \
    }                                                                                              \
  }

QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::InsertDataCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);
  DEFAULT_TRIPLES_STORE
  for(kDB::SPARQL::Algebra::QuadsCSP q : _query->quadsData()->quads())
  {
    GET_TRIPLES_STORE
    QList<knowRDF::Triple> triples;
    CHECK_RETURN_VOID_VALUE(kDB::SPARQL::Algebra::Utils::toRDFTriples(q->triples(), &triples),
                            "{}");
    CHECK_RETURN_VOID_VALUE(ts.insert(triples, connection.transaction()),
                            "Failed to insert triples in '{1}' with error '{0}'.", ts.uri());
  }
  return reportSuccess(true);
}
QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::DeleteDataCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);
  DEFAULT_TRIPLES_STORE
  for(kDB::SPARQL::Algebra::QuadsCSP q : _query->quadsData()->quads())
  {
    GET_TRIPLES_STORE
    QList<knowRDF::Triple> triples;
    QString errorMessage;
    CHECK_RETURN_VOID_VALUE(kDB::SPARQL::Algebra::Utils::toRDFTriples(q->triples(), &triples),
                            "{}");
    CHECK_RETURN_VOID_VALUE(ts.remove(triples, connection.transaction()),
                            "Failed to remove triples from {1} with error: {0}.", ts.uri());
  }
  return reportSuccess(true);
}
QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::LoadCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);
  DEFAULT_TRIPLES_STORE
  kDB::Repository::TripleStore tripleStore = defaultTripleStore;
  if(not _query->graph().isEmpty())
  {
    CHECK_RETURN_ASSIGN_VALUE(
      tripleStore, connection.connection().graphsManager()->getOrCreateTripleStore(_query->graph()),
      "{}");
  }

  QString localfile = _query->uri().toLocalFile();
  QFileInfo fi(localfile);
  if(not fi.exists() or not fi.isFile())
  {
    if(_query->silent())
    {
      return reportSuccess(true);
    }
    else
    {
      return reportError(clog_qt::qformat("Cannot load from file '{}'", (QString)_query->uri()));
    }
  }

  kDB::Repository::ThreadedTripleStreamInserter tsi(tripleStore);
  knowRDF::TripleStream ts;
  ts.setBase(_query->uri());
  ts.addListener(&tsi);
  QFile data_file(localfile);
  knowCore::Messages msgs;
  QString format;
  CHECK_RETURN_ASSIGN_VALUE(format, knowCore::formatFor((QString)_query->uri()),
                            "Error: failed to guess format for {} with error {}", _query->uri());
  CHECK_RETURN_VOID_VALUE(ts.start(&data_file, &msgs, format),
                          "Error: failed to parse {} with error: {}", _query->uri());
  return reportSuccess(true);
}
QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::DropCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);
  switch(_query->graphType())
  {
  case kDB::SPARQL::Algebra::GraphType::All:
  {
    for(const kDB::Repository::TripleStore& ts :
        connection.connection().graphsManager()->tripleStores())
    {
      if(ts.uri() != askcore_graph::info)
      {
        CHECK_RETURN_VOID_VALUE(
          connection.connection().graphsManager()->removeTripleStore(ts.uri()),
          "Failed to drop triples store: {}.with error {}", ts.uri());
      }
    }
    kDB::Repository::TripleStore _;
    CHECK_RETURN_ASSIGN_VALUE(
      _, connection.connection().graphsManager()->createTripleStore(askcore_graph::default_),
      "Error when recreating the default graph.");
    break;
  }
  case kDB::SPARQL::Algebra::GraphType::Named:
    for(const kDB::Repository::TripleStore& ts :
        connection.connection().graphsManager()->tripleStores())
    {
      if(ts.uri() != askcore_graph::default_ and ts.uri() != askcore_graph::info)
      {
        connection.connection().graphsManager()->removeTripleStore(ts.uri());
      }
    }
    break;
  case kDB::SPARQL::Algebra::GraphType::Default:
  {
    TripleStore ts;
    CHECK_RETURN_ASSIGN_VALUE(
      ts, connection.connection().graphsManager()->getTripleStore(askcore_graph::default_),
      "Failed to get default triple store: {}");
    CHECK_RETURN_VOID_VALUE(ts.clear(), "Failed to clear default triples store: {}");
  }
  break;
  case kDB::SPARQL::Algebra::GraphType::Graph:
    if(connection.connection().graphsManager()->hasTripleStore(_query->graph()))
    {
      connection.connection().graphsManager()->removeTripleStore(_query->graph());
    }
    else
    {
      if(not _query->silent())
      {
        return reportError(clog_qt::qformat("No graph named '{}'", _query->graph()));
      }
    }
    break;
  }
  return reportSuccess(true);
}

QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::ClearCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);
  switch(_query->graphType())
  {
  case kDB::SPARQL::Algebra::GraphType::All:
    for(kDB::Repository::TripleStore ts : connection.connection().graphsManager()->tripleStores())
    {
      if(ts.uri() != askcore_graph::info)
      {
        CHECK_RETURN_VOID_VALUE(ts.clear(), "Failed to clear default triples store {}: {}",
                                ts.uri());
      }
    }
    break;
  case kDB::SPARQL::Algebra::GraphType::Named:
    for(kDB::Repository::TripleStore ts : connection.connection().graphsManager()->tripleStores())
    {
      if(ts.uri() != askcore_graph::default_ and ts.uri() != askcore_graph::info)
      {
        CHECK_RETURN_VOID_VALUE(ts.clear(), "Failed to clear default triples store {}", ts.uri());
      }
    }
    break;
  case kDB::SPARQL::Algebra::GraphType::Default:
  {
    TripleStore ts;
    CHECK_RETURN_ASSIGN_VALUE(
      ts, connection.connection().graphsManager()->getTripleStore(askcore_graph::default_),
      "Failed to get default triple store: {}");
    CHECK_RETURN_VOID_VALUE(ts.clear(), "Failed to clear default triples store: {}");
  }
  break;
  case kDB::SPARQL::Algebra::GraphType::Graph:
    if(connection.connection().graphsManager()->hasTripleStore(_query->graph()))
    {
      TripleStore ts;
      CHECK_RETURN_ASSIGN_VALUE(
        ts, connection.connection().graphsManager()->getTripleStore(_query->graph()),
        "Failed to get triple store {}: {}", _query->graph());
      CHECK_RETURN_VOID_VALUE(ts.clear(), "Failed to clear default triples store {}: {}",
                              _query->graph());
    }
    else
    {
      if(not _query->silent())
      {
        return reportError(clog_qt::qformat("No graph named '{}'", _query->graph()));
      }
    }
    break;
  }
  return reportSuccess(true);
}

QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::CreateCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);
  if(connection.connection().graphsManager()->hasTripleStore(_query->graph()))
  {
    if(not _query->silent())
    {
      return reportError(clog_qt::qformat("Graph named '{}' already exists", _query->graph()));
    }
  }
  else
  {
    connection.connection().graphsManager()->createTripleStore(_query->graph());
  }
  return reportSuccess(true);
}

QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::DeleteInsertCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_query);
  Q_UNUSED(_parameter);
  TemporaryTransaction tt(connection);
  knowDBC::Query sqlQuery = tt.transaction().createSQLQuery();
  DEFAULT_TRIPLES_STORE
  RDFDataset datasetDD = defaultTripleStore;
  if(not _query->withUri().isEmpty())
  {
    CHECK_RETURN_ASSIGN_VALUE(
      datasetDD, connection.connection().graphsManager()->getTripleStore(_query->withUri()), "{}");
  }
  auto const& [success, sources, sourcesError]
    = generateSources(tt.transaction(), &sqlQuery, _query->datasets(), datasetDD);
  if(not success)
  {
    tt.rollback();
    return reportError(sourcesError.value().get_message());
  }
  QList<TripleStore> defaultTripleStores;
  switch(datasetDD.type())
  {
  case RDFDataset::Type::Invalid:
    tt.rollback();
    return reportError("Inserting and removing in an invalid dataset.");
  case RDFDataset::Type::Empty:
    tt.rollback();
    return reportError("Inserting and removing in an empty dataset.");
  case RDFDataset::Type::TripleStore:
    defaultTripleStores.append(datasetDD.toTripleStore());
    break;
  case RDFDataset::Type::TriplesView:
    tt.rollback();
    return reportError("Inserting and removing is not permitted in a view.");
  case RDFDataset::Type::DatasetsUnion:
  case RDFDataset::Type::PersistentDatasetsUnion:
  {
    for(const RDFDataset& dataset : datasetDD.toDatasetsUnion().datasets())
    {
      switch(dataset.type())
      {
      case RDFDataset::Type::Invalid:
        tt.rollback();
        return reportError("Inserting and removing in an invalid dataset.");
      case RDFDataset::Type::Empty:
      case RDFDataset::Type::TriplesView: // Skipping triples view, they can be used in the where
                                          // statement but not for delete/insert
        break;
      case RDFDataset::Type::TripleStore:
        defaultTripleStores.append(dataset.toTripleStore());
        break;
      case RDFDataset::Type::PersistentDatasetsUnion:
      case RDFDataset::Type::DatasetsUnion:
        tt.rollback();
        return reportError("Nested datasets union.");
      }
    }
  }
  }

  SPARQLAlgebraToPostgresSQL sparql_to_sql(_query, connection.connection(), sources.value());

  auto const& [sqlquery_success, sqlquery_, sqlquery_err] = sparql_to_sql.buildDeleteInsertQuery();
  if(not sqlquery_success)
  {
    clog_warning("No where statement!");
    tt.rollback();
    return reportError(sqlquery_err.value().get_message());
  }

  SPARQLAlgebraToPostgresSQL::SQLDeleteInsertQuery sqlquery = sqlquery_.value();

#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
  clog_debug("Datasets count          : {}", _query->datasets().size());
  clog_debug("Default triples stores  : {}", defaultTripleStores.size());
  clog_debug("Sources                 : {}", sources);
  clog_debug("Where Query             : {}", sqlquery.whereTableQuery);
  clog_debug("Deletion Template Query : {}", sqlquery.deletionTemplate);
  clog_debug("Insertion Template Query: {}", sqlquery.insertionTemplate);
  clog_debug("Mappings: {}", sqlquery.valuesmapping);
  QJsonDocument doc = QJsonDocument(
    knowCore::Value::fromValue(sqlquery.valuesmapping).toJsonValue().expect_success().toObject());
  clog_debug("Mappings (JSon): {}", QString::fromLatin1(doc.toJson(QJsonDocument::Compact)));
#endif

  sqlQuery.setQuery(sqlquery.whereTableQuery);
  sqlQuery.bindValues(sqlquery.valuesmapping);
  EXECUTE_QUERY("Failed to execute where query", sqlQuery,
                reportError(clog_qt::qformat("Failed to execute SQL query: {}", result.error())));

  if(not sqlquery.deletionTemplate.isEmpty())
  {
    sqlQuery.setQuery(sqlquery.deletionTemplate);
    sqlQuery.bindValues(sqlquery.valuesmapping);
    EXECUTE_QUERY("Failed to execute delete view query", sqlQuery,
                  reportError(clog_qt::qformat("Failed to execute SQL query: {}", result.error())));
    QList<knowCore::Uri> delete_graphs = DatabaseInterface::PostgreSQL::SQLInterface::getGraphUris(
      tt.transaction(), "delete_tmp", "graph");
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("Number of graphs in deletion set: {}", delete_graphs.size());
#endif
    for(const knowCore::Uri& _graph_uri : delete_graphs)
    {
      QList<TripleStore> tses;
      if(_graph_uri.isEmpty())
      {
        tses = defaultTripleStores;
      }
      else
      {
        kDB::Repository::TripleStore ts;
        CHECK_RETURN_ASSIGN_VALUE(
          ts, connection.connection().graphsManager()->getTripleStore(_graph_uri), "{}");
        tses.append(ts);
      }
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
      if(tses.isEmpty())
      {
        clog_debug("No graphs matches: {}", _graph_uri);
      }
#endif
      for(TripleStore ts : tses)
      {
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
        clog_debug("Removing from: '{}' ({})", _graph_uri, ts.uri());
#endif
        if(ts.isValid())
        {
          auto const& [s, m] = DatabaseInterface::PostgreSQL::SQLInterface::removeTriples(
            tt.transaction(), &ts, _graph_uri);
          if(not s)
          {
            tt.rollback();
            return reportError(clog_qt::qformat(
              "Failed to remove triples from graph: {} with error: {}", _graph_uri, m.value()));
          }
        }
        else
        {
          tt.rollback();
          return reportError(clog_qt::qformat("Invalid triple store: {}", _graph_uri));
        }
      }
    }
    sqlQuery.setQuery("DROP TABLE delete_tmp");
    EXECUTE_QUERY("Failed to drop delete_tmp view", sqlQuery, reportError("Internal error!"));
  }

  if(not sqlquery.insertionTemplate.isEmpty())
  {
    sqlQuery.setQuery(sqlquery.insertionTemplate);
    sqlQuery.bindValues(sqlquery.valuesmapping);
    EXECUTE_QUERY(
      "Failed to execute insert view query", sqlQuery,
      clog_qt::qformat(clog_qt::qformat("Failed to execute SQL query: {}", result.error())));

    for(const knowCore::Uri& _graph_uri : DatabaseInterface::PostgreSQL::SQLInterface::getGraphUris(
          tt.transaction(), "insert_tmp", "graph"))
    {
      QList<TripleStore> tses;
      if(_graph_uri.isEmpty())
      {
        tses = defaultTripleStores;
      }
      else
      {
        TripleStore ts;
        CHECK_RETURN_ASSIGN_VALUE(
          ts, connection.connection().graphsManager()->getOrCreateTripleStore(_graph_uri), "{}");
        tses.append(ts);
      }

      for(TripleStore ts : tses)
      {

        clog_assert(ts.isValid());
        auto const& [s, m] = DatabaseInterface::PostgreSQL::SQLInterface::insertTriples(
          tt.transaction(), &ts, _graph_uri);
        if(not s)
        {
          tt.rollback();
          return reportError(clog_qt::qformat(
            "Failed to insert triples from graph: {} with error: {}", _graph_uri, m.value()));
        }
      }
    }
    sqlQuery.setQuery("DROP TABLE insert_tmp");
    EXECUTE_QUERY("Failed to drop insert_tmp view", sqlQuery, reportError("Internal error!"));
  }

  sqlQuery.setQuery("DROP TABLE delete_insert_where_tmp");
  EXECUTE_QUERY("Failed to drop delete_insert_where_tmp view", sqlQuery,
                reportError("Internal error!"));

  auto const& [s, m] = tt.commitIfOwned();
  if(s)
  {
    return reportSuccess(true);
  }
  else
  {
    return reportError("Transaction failed to commit: " + m.value().get_message());
  }
}

QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::PLQueryCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_query);
  Q_UNUSED(_parameter);

  TemporaryTransaction tt(connection);

  PLExecutorVisitor plev;
  plev.defaultDataset = defaultDataset;
  plev.namedDatasets = namedDatasets;
  plev.transaction = tt.transaction();
  plev.query_text = query_text;

  auto const& [s_e, m_e] = plev.start(_query->statements());
  if(not s_e)
  {
    tt.rollback();
    return reportError("Execution failed: " + m_e.value().get_message());
  }

  if(plev.result.type() == knowDBC::Interfaces::Result::Type::Failed)
  {
    tt.rollback();
    return reportError("Execution failed: " + plev.result.error());
  }

  auto const& [s, m] = tt.commitIfOwned();
  if(s)
  {
    return reportSuccess(true);
  }
  else
  {
    return reportError("Transaction failed to commit: " + m.value().get_message());
  }
}

cres_qresult<QHash<QString, QString>>
  QueryExecutorVisitor::generateSources(kDB::Repository::Transaction _transaction,
                                        knowDBC::Query* _sql_query,
                                        const QList<kDB::SPARQL::Algebra::DatasetCSP>& _datasets,
                                        const kDB::Repository::RDFDataset& _defaultDataset)
{
  QList<RDFDataset> datasets;
  QList<RDFDataset> named_datasets;

  if(_datasets.isEmpty())
  {
    named_datasets = namedDatasets;
    if(_defaultDataset.isValid())
    {
      datasets.append(_defaultDataset);
    }
    else if(defaultDataset.isValid())
    {
      datasets.append(defaultDataset);
    }
    else
    {
      cres_try(TripleStore dts, connection.connection().defaultTripleStore());
      datasets.append(dts);
      if(named_datasets.isEmpty())
      {
        for(const RDFDataset& dataset : connection.connection().graphsManager()->datasets())
        {
          named_datasets.append(dataset);
        }
      }
    }
  }
  else
  {
    QStringList availableDatasets, availableNamedDatasets;

    switch(defaultDataset.type())
    {
    case RDFDataset::Type::Empty:
    case RDFDataset::Type::Invalid:
      break;
    case RDFDataset::Type::TriplesView:
    case RDFDataset::Type::TripleStore:
    case RDFDataset::Type::PersistentDatasetsUnion:
      availableDatasets.append(defaultDataset.uri());
      break;
    case RDFDataset::Type::DatasetsUnion:
    {
      for(const RDFDataset& dataset : defaultDataset.toDatasetsUnion().datasets())
      {
        availableDatasets.append(dataset.uri());
      }
    }
    }
    for(const RDFDataset& dataset : namedDatasets)
    {
      availableNamedDatasets.append(dataset.uri());
    }

    // Add to named datasets the specified datasets
    for(kDB::SPARQL::Algebra::DatasetCSP dataset_node : _datasets)
    {
      if(dataset_node->uri() == askcore_graph::all)
      {
        switch(dataset_node->datasetType())
        {
        case kDB::SPARQL::Algebra::DatasetType::Anonymous:
          datasets = connection.connection().graphsManager()->datasets();
          break;
        case kDB::SPARQL::Algebra::DatasetType::Named:
          named_datasets = connection.connection().graphsManager()->datasets();
          break;
        }
      }
      else
      {
        RDFDataset sub_dataset;
        if(connection.connection().graphsManager()->hasTripleStore(dataset_node->uri()))
        {
          cres_try(sub_dataset,
                   connection.connection().graphsManager()->getTripleStore(dataset_node->uri()));
        }
        else if(connection.connection().graphsManager()->hasTriplesView(dataset_node->uri()))
        {
          cres_try(sub_dataset,
                   connection.connection().graphsManager()->getTriplesView(dataset_node->uri()));
        }
        else if(connection.connection().graphsManager()->hasUnion(dataset_node->uri()))
        {
          cres_try(sub_dataset,
                   connection.connection().graphsManager()->getUnion(dataset_node->uri()));
        }
        else
        {
          return cres_failure("Unknown graph '{}'", dataset_node->uri());
        }

        switch(dataset_node->datasetType())
        {
        case kDB::SPARQL::Algebra::DatasetType::Anonymous:
          if(availableDatasets.isEmpty() or availableDatasets.contains(sub_dataset.uri())
             or availableNamedDatasets.contains(sub_dataset.uri()))
          {
            datasets.append(sub_dataset);
          }
          break;
        case kDB::SPARQL::Algebra::DatasetType::Named:
          if(availableNamedDatasets.isEmpty() or availableNamedDatasets.contains(sub_dataset.uri()))
          {
            named_datasets.append(sub_dataset);
          }
          break;
        }
      }
    }
  }

  // Generate the default dataset

  RDFDataset dataset;

  switch(datasets.size())
  {
  case 0:
    dataset = connection.connection().graphsManager()->emptyGraph();
    break;
  case 1:
    dataset = datasets.first();
    break;
  default:
  {
    DatasetsUnion ds_union;
    for(const RDFDataset& ds : datasets)
    {
      ds_union.add(ds);
    }
    dataset = ds_union;
    break;
  }
  }

  // Add the default dataset to sources

  QHash<QString, QString> sources;

  switch(dataset.type())
  {
  case RDFDataset::Type::Invalid:
  {
    return cres_failure("Query an invalid RDF Dataset");
  }
  case RDFDataset::Type::Empty:
  {
    sources[QString()] = "(SELECT null::text AS subject, null::text AS predicate, "
                         "null::kdb_rdf_term AS object LIMIT 0)";
    break;
  }
  case RDFDataset::Type::PersistentDatasetsUnion:
    sources[QString()] = dataset.toPersistentDatasetsUnion().tablename();
    break;
  case RDFDataset::Type::DatasetsUnion:
  {
    sources[QString()] = "triples_union_view";
    QList<RDFDataset> datasets = dataset.toDatasetsUnion().datasets();
    if(datasets.isEmpty())
    {
      return cres_failure("Cannot query an empty union!");
    }
    cres_try(cres_ignore,
             DatabaseInterface::PostgreSQL::SQLInterface::updateUnionView(
               _transaction, true, "triples_union_view", datasets),
             message("Failed to create dataset union {}"));
  }
  break;
  case RDFDataset::Type::TripleStore:
    sources[QString()] = dataset.toTripleStore().tablename();
    break;
  case RDFDataset::Type::TriplesView:
    sources[QString()] = dataset.toTriplesView().tablename();
    break;
  }

  // Generate named sources
  if(named_datasets.isEmpty())
  {
    sources["all_namedgraph"] = "(SELECT null::text AS graph, null::text AS subject, null::text AS "
                                "predicate, null::kdb_rdf_term AS object LIMIT 0)";
  }
  else
  {
    QString tmp_view = "CREATE OR REPLACE TEMPORARY VIEW named_triples_union_view AS ";
    sources["all_namedgraph"] = "named_triples_union_view";

    bool first = true;

    for(const RDFDataset& ds : named_datasets)
    {
      if(not first)
        tmp_view += " UNION ";
      else
        first = false;
      QString tn;
      switch(ds.type())
      {
      case RDFDataset::Type::Invalid:
        return cres_failure("Query an invalid RDF Named Dataset");
      case RDFDataset::Type::PersistentDatasetsUnion:
        tn = ds.toPersistentDatasetsUnion().tablename();
        break;
      case RDFDataset::Type::DatasetsUnion:
        return cres_failure("Unions cannot be used as named graph.");
      case RDFDataset::Type::Empty:
        return cres_failure("Empty cannot be used as named graph.");
      case RDFDataset::Type::TripleStore:
        tn = ds.toTripleStore().tablename();
        break;
      case RDFDataset::Type::TriplesView:
        tn = ds.toTriplesView().tablename();
        break;
      }
      clog_assert(not ds.uri().isEmpty());
      tmp_view
        += "(SELECT '" + ds.uri() + "'::text AS graph, subject, predicate, object FROM " + tn + ")";
      sources[ds.uri()] = tn;
    }
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("Named triple view: {}", tmp_view);
#endif
    _sql_query->setQuery(tmp_view);
    knowDBC::Result r_dsu = _sql_query->execute();
    if(not r_dsu)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR("Failed to create named graph union", r_dsu);
      return cres_failure("Failed to create named graph union {}", r_dsu.error());
    }
  }
  return cres_success(sources);
}

QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::AskQueryCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_query);
  Q_UNUSED(_parameter);
  TemporaryTransaction tt(connection);
  knowDBC::Query sqlQuery = tt.transaction().createSQLQuery();

  auto const& [sources_success, sources, sources_error]
    = generateSources(tt.transaction(), &sqlQuery, _query->datasets());
  if(not sources_success)
  {
    tt.rollback();
    return reportError(sources_error.value().get_message());
  }

  SPARQLAlgebraToPostgresSQL sparql_to_sql(_query, connection.connection(), sources.value());
  auto const& [sqlquery_success, sqlquery_, sqlquery_error] = sparql_to_sql.buildSelectQuery();

  if(not sqlquery_success)
  {
    tt.rollback();
    return reportError(sqlquery_error.value().get_message());
  }
  else
  {
    SPARQLAlgebraToPostgresSQL::SQLSelectQuery sqlquery = sqlquery_.value();
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("Query: {}", sqlquery.query);
    clog_debug("Mappings: {}", sqlquery.valuesmapping);
    clog_debug("Sources                 : {}", sources);
    QJsonDocument doc = QJsonDocument(
      knowCore::Value::fromValue(sqlquery.valuesmapping).toJsonValue().expect_success().toObject());
    clog_debug("Mappings (JSon): {}", QString::fromLatin1(doc.toJson(QJsonDocument::Compact)));
#endif

    sqlQuery.setQuery(sqlquery.query);
    sqlQuery.bindValues(sqlquery.valuesmapping);
    knowCore::Timestamp time1 = knowCore::Timestamp::now();
    knowDBC::Result result = sqlQuery.execute();

#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("It took {} ms to execute the query",
               time1.intervalTo<knowCore::MilliSeconds>(knowCore::Timestamp::now()).count());
#endif

    if(result)
    {
      clog_assert(result.tuples() == 1 and result.fields() == 1);
      CHECK_RETURN_VOID_VALUE(tt.commitIfOwned(), "{}");
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
      clog_debug("Result: {}", result.value(0, 0));
#endif
      return reportSuccess(result.value<bool>(0, 0).expect_success());
    }
    else
    {
      tt.rollback();
      return reportError(result.error());
    }
  }
}
QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::ConstructQueryCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);
  TemporaryTransaction tt(connection);
  knowDBC::Query sqlQuery = tt.transaction().createSQLQuery();

  auto const& [sources_success, sources, sources_error]
    = generateSources(tt.transaction(), &sqlQuery, _query->datasets());
  if(not sources_success)
  {
    tt.rollback();
    return reportError(sources_error.value().get_message());
  }

  SPARQLAlgebraToPostgresSQL sparql_to_sql(_query, connection.connection(), sources.value());
  auto const& [sqlquery_success, sqlquery, sqlquery_error] = sparql_to_sql.buildConstructQuery();

  if(not sqlquery_success)
  {
    tt.rollback();
    return reportError(sqlquery_error.value().get_message());
  }
  else
  {
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("SQL Query: {}", sqlquery.query);
#endif
    // Execute query
    sqlQuery.setQuery(sqlquery.value().query);
    sqlQuery.bindValues(sqlquery.value().valuesmapping);

    knowDBC::Result result = sqlQuery.execute();

    if(result)
    {
      QList<knowCore::ValueList> data;
      clog_assert(result.fields() == 3);

      for(int i = 0; i < result.tuples(); ++i)
      {
        knowRDF::Triple triple = DatabaseInterface::PostgreSQL::SQLInterface::getTriple(
          connection.connection(), result, i, 0, 1, 2, false);
        data.append(knowCore::ValueList({knowCore::Value::fromValue(triple)}));
      }

      CHECK_RETURN_VOID_VALUE(tt.commitIfOwned(), "{}");
      return reportSuccess(QStringList() << "triple", data);
    }
    else
    {
      tt.rollback();
      return reportError(result.error());
    }
  }
}

QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::SelectQueryCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);

  InspectQueryVisitor iqv;
  iqv.start(_query);
  if(not iqv.errorMessage().isEmpty())
  {
    return reportError(iqv.errorMessage());
  }

  TemporaryTransaction tt(connection);
  knowDBC::Query sqlQuery = tt.transaction().createSQLQuery();

  auto const& [sources_success, sources, sources_error]
    = generateSources(tt.transaction(), &sqlQuery, _query->datasets());
  if(not sources_success)
  {
    tt.rollback();
    return reportError(sources_error.value().get_message());
  }

#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
  for(auto it = sources.begin(); it != sources.end(); ++it)
  {
    clog_debug("Source: '{}' = '{}'", it.key(), it.value());
  }
#endif

  SPARQLAlgebraToPostgresSQL sparql_to_sql(_query, connection.connection(), sources.value());
  auto const& [sqlquery_success, sqlquery, sqlquery_err] = sparql_to_sql.buildSelectQuery();
  if(not sqlquery_success)
  {
    tt.rollback();
    return reportError(sqlquery_err.value().get_message());
  }
  else
  {
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("Query: {}", sqlquery.value().query);
    clog_debug("Mappings: {}", sqlquery.value().valuesmapping);
    QJsonDocument doc = QJsonDocument(
      knowCore::Value::fromValue(sqlquery.valuesmapping).toJsonValue().expect_success().toObject());
    clog_debug("Mappings (JSon): {}", QString::fromLatin1(doc.toJson(QJsonDocument::Compact)));
#endif

    sqlQuery.setQuery(sqlquery.value().query);
    sqlQuery.bindValues(sqlquery.value().valuesmapping);

    knowCore::Timestamp time1 = knowCore::Timestamp::now();
    knowDBC::Result result = sqlQuery.execute();
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("It took {} ms to execute the query",
               time1.intervalTo<knowCore::MilliSeconds>(knowCore::Timestamp::now()).count());
#endif

    if(result)
    {
      QStringList fields;
      QList<knowCore::ValueList> data;

      QList<int> field_indexes;
      for(kDB::SPARQL::Algebra::VariableCSP var : sqlquery.value().variable_list)
      {
        QString sql_var_name = sqlquery.value().variablesmapping[var->name()];
        int fi = result.fieldIndex(sql_var_name);
        clog_assert(fi >= 0 or iqv.hasServices());
        field_indexes.append(fi);
        fields << var->name();
      }

      // field index for a given variable, only needed in case of services
      QList<QPair<QString, int>> variables_to_field_index;
      if(not iqv.services().isEmpty())
      {
        for(auto it = sqlquery.value().variablesmapping.begin();
            it != sqlquery.value().variablesmapping.end(); ++it)
        {
          int fi = result.fieldIndex(it.value());
          if(fi >= 0)
          {
            variables_to_field_index.append(QPair<QString, int>(it.key(), fi));
          }
        }
      }

      // generate results
      for(int i = 0; i < result.tuples(); ++i)
      {
        auto get_value = [i, result, this](int _f)
        {
          if(_f >= 0)
          {
            knowCore::Value var = result.value(i, _f);
            if(var.canConvert<QString>())
            {
              QString uri = var.value<QString>().expect_success();
              ;
              if(knowRDF::isBlankNodeSkolemisation(uri))
              {
                var = knowCore::Value::fromValue(connection.connection().d->blankNode(uri));
              }
            }
            return var;
          }
          else
          {
            return knowCore::Value();
          }
        };
        if(iqv.services().isEmpty())
        {
          QList<knowCore::Value> row;
          for(int j = 0; j < sqlquery.value().variable_list.size(); ++j)
          {
            row << get_value(field_indexes[j]);
          }
          data.append(row);
        }
        else
        {
          std::function<std::tuple<bool, QString>(const QList<kDB::SPARQL::Algebra::ServiceCSP>&,
                                                  const QHash<QString, knowCore::Value>&)>
            service_caller = [&service_caller, &data, fields, this, sqlquery, result,
                              i](const QList<kDB::SPARQL::Algebra::ServiceCSP>& _services,
                                 const QHash<QString, knowCore::Value>& _row)
          {
            if(_services.isEmpty())
            {
              QList<knowCore::Value> row;
              for(const QString& fn : fields)
              {
                row << _row.value(fn);
              }
              data.append(row);
              return std::make_tuple(true, QString());
            }
            else
            {
              namespace SPARQLAlgebra = kDB::SPARQL::Algebra;
              QList<SPARQLAlgebra::ServiceCSP> next_services = _services;
              SPARQLAlgebra::ServiceCSP service = next_services.takeFirst();

              knowCore::Uri service_uri;
              switch(service->service().type())
              {
              case SPARQLAlgebra::NodeType::VariableReference:
              {
                QString sql_var_name
                  = sqlquery.value().variablesmapping
                      [service->service().value<SPARQLAlgebra::VariableReference>()->name()];
                int fi = result.fieldIndex(sql_var_name);
                clog_assert(fi >= 0);
                service_uri = result.value(i, fi).value<knowCore::Uri>().expect_success();
              }
              break;
              case SPARQLAlgebra::NodeType::Term:
                service_uri = service->service().value<SPARQLAlgebra::Term>()->term();
                break;
              default:
                clog_fatal("Unsupported service type!");
              }
              // Convert the FROM definition to a set of datasets
              QList<SPARQLAlgebra::DatasetCSP> datasets;
              for(auto from : service->froms())
              {
                knowCore::Uri dataset_uri;
                switch(from.type())
                {
                case SPARQLAlgebra::NodeType::VariableReference:
                {
                  QString sql_var_name
                    = sqlquery.value()
                        .variablesmapping[from.value<SPARQLAlgebra::VariableReference>()->name()];
                  int fi = result.fieldIndex(sql_var_name);
                  clog_assert(fi >= 0);
                  dataset_uri = result.value(i, fi).value<knowCore::Uri>().expect_success();
                }
                break;
                case SPARQLAlgebra::NodeType::Term:
                  dataset_uri = from.value<SPARQLAlgebra::Term>()->term();
                  break;
                default:
                  clog_fatal("Unsupported service type!");
                }
                datasets.append(
                  new SPARQLAlgebra::Dataset(SPARQLAlgebra::DatasetType::Anonymous, dataset_uri));
              }
              // Create the query
              SPARQLAlgebra::SelectQueryCSP query = new SPARQLAlgebra::SelectQuery(
                SPARQLAlgebra::SelectModifier::None, QList<SPARQLAlgebra::VariableCSP>(), datasets,
                service->groupGraph(), nullptr, nullptr, service->orderClauses(),
                service->limitOffsetClause());
              SPARQLAlgebra::Visitors::Serialiser::Result query_ser
                = SPARQLAlgebra::Visitors::Serialiser::serialise(query, _row);

              knowDBC::Result r
                = services.call(service_uri, query_ser.queryText, query_ser.bindings);
              if(r)
              {
                QStringList variales = r.fieldNames();
                for(int k = 0; k < r.tuples(); ++k)
                {
                  QHash<QString, knowCore::Value> new_row = _row;
                  for(int l = 0; l < variales.size(); ++l)
                  {
                    clog_assert(not new_row.contains(variales[l]));
                    new_row[variales[l]] = r.value(k, l);
                  }
                  auto [success, errorMsg] = service_caller(next_services, new_row);
                  if(not success)
                    return std::make_tuple(false, errorMsg);
                }
                return std::make_tuple(true, QString());
              }
              else
              {
                return std::make_tuple(false, r.error());
              }
            }
          };
          QHash<QString, knowCore::Value> row;
          for(const QPair<QString, int>& v_t_i : variables_to_field_index)
          {
            row[v_t_i.first] = get_value(v_t_i.second);
          }

          auto [success, errorMsg] = service_caller(iqv.services(), row);
          if(not success)
          {
            tt.rollback();
            return reportError(errorMsg);
          }
        }
      }
      CHECK_RETURN_VOID_VALUE(tt.commitIfOwned(), "{}");
      return reportSuccess(fields, data);
    }
    else
    {
      tt.rollback();
      return reportError(result.error());
    }
  }
}
QVariant QueryExecutorVisitor::visit(kDB::SPARQL::Algebra::ExplainQueryCSP _query,
                                     const QVariant& _parameter)
{
  Q_UNUSED(_parameter);
  TemporaryTransaction tt(connection);
  knowDBC::Query sqlQuery = tt.transaction().createSQLQuery();

  kDB::SPARQL::Algebra::SelectQueryCSP select_query
    = dynamic_cast<const kDB::SPARQL::Algebra::SelectQuery*>(_query->query().data());

  auto const& [sources_success, sources, sources_error]
    = generateSources(tt.transaction(), &sqlQuery, select_query->datasets());
  if(not sources_success)
  {
    tt.rollback();
    return reportError(sources_error.value().get_message());
  }

  SPARQLAlgebraToPostgresSQL sparql_to_sql(select_query, connection.connection(), sources.value());
  auto const& [sqlquery_success, sqlquery, sqlquery_error] = sparql_to_sql.buildSelectQuery();
  if(not sqlquery_success)
  {
    tt.rollback();
    return reportError(sqlquery_error.value().get_message());
  }
  else
  {
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("Query: {}", sqlquery.query);
    clog_debug("Mappings: {}", sqlquery.valuesmapping);
#endif
    QString sql_options = _query->options();

    sqlQuery.setQuery("EXPLAIN " + sql_options + " " + sqlquery.value().query);
    sqlQuery.bindValues(sqlquery.value().valuesmapping);

    knowCore::Timestamp time1 = knowCore::Timestamp::now();
    knowDBC::Result result = sqlQuery.execute();
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    clog_debug("It took {} ms to execute the query",
               time1.intervalTo<knowCore::MilliSeconds>(knowCore::Timestamp::now()).count());
#endif

    if(result)
    {
      if(result.fields() == 1)
      {
        CHECK_RETURN_VOID_VALUE(tt.commitIfOwned(), "{}");
        QList<knowCore::ValueList> reports;
        for(int i = 0; i < result.tuples(); ++i)
        {
          QList<knowCore::Value> report;
          report << result.value(i, 0);
          reports << report;
        }
        return reportSuccess(QStringList() << result.fieldName(0), reports);
      }
      else
      {
        tt.rollback();
        return reportError("Internal error");
      }
    }
    else
    {
      tt.rollback();
      return reportError(result.error());
    }
  }
}
