#include <kDB/SPARQL/Algebra/AbstractNodeVisitor.h>

#include <knowDBC/Result.h>

#include "../QueryConnectionInfo.h"
#include "../RDFDataset.h"
#include "../Services.h"

namespace kDB
{
  namespace Repository
  {
    namespace SPARQLExecution
    {
      struct QueryExecutorVisitor
          : public kDB::SPARQL::Algebra::AbstractNodeVisitor<QVariant, QVariant>
      {
        RDFDataset defaultDataset;
        QList<RDFDataset> namedDatasets;
        QueryConnectionInfo connection;
        QString query_text;
        knowDBC::Result result;
        Services services;
        QVariant reportResult(const knowDBC::Result& _result)
        {
          result = _result;
          return QVariant();
        }
        QVariant reportError(const QString& _errmsg)
        {
          result = knowDBC::Result::create(query_text, _errmsg);
          return QVariant();
        }
        //         QVariant reportSuccess()
        //         {
        //           result = knowDBC::Result::create(query_text);
        //           return QVariant();
        //         }
        QVariant reportSuccess(QStringList _fields, const QList<knowCore::ValueList>& _data)
        {
          result = knowDBC::Result::create(query_text, _fields, _data);
          return QVariant();
        }
        QVariant reportSuccess(bool _result)
        {
          result = knowDBC::Result::create(query_text, _result);
          return QVariant();
        }
        QVariant visit(kDB::SPARQL::Algebra::InsertDataCSP _query,
                       const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::DeleteDataCSP _query,
                       const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::LoadCSP _query, const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::DropCSP _query, const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::ClearCSP _query, const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::CreateCSP _query, const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::DeleteInsertCSP _query,
                       const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::PLQueryCSP _query,
                       const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::QuadsCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::QuadsDataCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ListCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::VariableCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DatasetCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::GroupClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::HavingClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DescribeQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DescribeTermCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }

        cres_qresult<QHash<QString, QString>>
          generateSources(kDB::Repository::Transaction _transaction, knowDBC::Query* _sql_query,
                          const QList<kDB::SPARQL::Algebra::DatasetCSP>& _datasets,
                          const RDFDataset& _defaultDataset = RDFDataset());

        QVariant visit(kDB::SPARQL::Algebra::AskQueryCSP _query,
                       const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::ConstructQueryCSP _query,
                       const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::SelectQueryCSP _query,
                       const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::ExplainQueryCSP _query,
                       const QVariant& _parameter) override;
        QVariant visit(kDB::SPARQL::Algebra::OrderClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::OrderClauseCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LimitOffsetClauseCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::TripleCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::VariableReferenceCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::GraphReferenceCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::TermCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::BlankNodeCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ServiceCSP _node, const QVariant& _parameter) override
        {
          Q_UNUSED(_node);
          Q_UNUSED(_parameter);
          qFatal("wip");
        }
        QVariant visit(kDB::SPARQL::Algebra::GroupGraphPatternCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ValueCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::OptionalCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::UnionCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::MinusCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalOrCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalAndCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalDifferentCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalEqualCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInferiorCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInferiorEqualCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalSuperiorCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalSuperiorEqualCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::AdditionCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::SubstractionCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::MultiplicationCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DivisionCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalNotInCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalNegationCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::NegationCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::FunctionCallCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ExecuteCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::BindCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::IfCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::UnlessCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
      };
    } // namespace SPARQLExecution
  } // namespace Repository
} // namespace kDB
