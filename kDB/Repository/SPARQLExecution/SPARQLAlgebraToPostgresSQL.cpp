#include "SPARQLAlgebraToPostgresSQL_p.h"

#include <QMetaType>

#include <clog_qt>
#include <knowCore/Cast.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/ValueHash.h>

#include <knowRDF/Literal.h>

#include <kDB/SPARQL/Algebra/Visitors/ExpressionType.h>
#include <kDB/SPARQL/Algebra/Visitors/VariablesLister.h>
#include <kDB/SPARQL/Query.h>

#include "Connection_p.h"
#include "RDFDataset.h"
#include "SPARQLFunctionDefinition.h"
#include "SPARQLFunctionsManager.h"

#include "DatabaseInterface/PostgreSQL/RDFValueBinaryMarshal.h"

using namespace kDB::Repository::SPARQLExecution;

namespace kSA = kDB::SPARQL::Algebra;

namespace
{
  class SPARQLFunctionsManagerFunctionTypeInterface
      : public kDB::SPARQL::Algebra::Visitors::FunctionTypeInterface
  {
  public:
    SPARQLFunctionsManagerFunctionTypeInterface(kDB::Repository::SPARQLFunctionsManager* _manager)
        : m_manager(_manager)
    {
    }
    knowCore::Uri type(const knowCore::Uri& _name, const knowCore::UriList& _list) const override
    {
      return m_manager->function(_name, _list).returnType();
    }
  private:
    kDB::Repository::SPARQLFunctionsManager* m_manager;
  };
} // namespace

struct SPARQLAlgebraToPostgresSQL::Private
{
  SPARQL::Algebra::NodeCSP query;
  Connection connection;
  QHash<QString, QString> sources;
};

namespace kDB
{
  namespace Repository
  {
    namespace SPARQLExecution
    {
      struct SPARQLAlgebraToPostgresSQLVisitor
          : public kDB::SPARQL::Algebra::AbstractNodeVisitor<QVariant, QVariant>
      {
        QStringList errorMessages;

        struct QueryPartContext
        {
          QHash<QString, QString> variablescolumn;
          QHash<QString, QString> variablesmapping;
          QHash<knowRDF::BlankNode, QString> blanknodecolumn;
          QHash<knowRDF::BlankNode, QString> blanknodemapping_;
          QString source;
          QString namedSource;
          QStringList serviceVariables; //< list of variables that are not listed but most be
                                        // returned by postgresql
        };
        struct QueryPart
        {
          QString table_alias;
          QString source;
          QStringList conditions;
          QHash<QString, QString> variables;
          QStringList filters;
          bool optional = false;
        };

        void merge_unique(QStringList* _out, const QStringList& _other)
        {
          for(const QString& str : _other)
          {
            if(not _out->contains(str))
            {
              _out->append(str);
            }
          }
        }

        QString generateWhere(const QueryPart& _qp)
        {
          QString where;
          if(not _qp.conditions.isEmpty())
          {
            where += " WHERE " + _qp.conditions.join(" AND ");
          }
          if(not _qp.filters.isEmpty())
          {
            if(_qp.conditions.isEmpty())
            {
              where += " WHERE ";
            }
            else
            {
              where += " AND ";
            }
            where += _qp.filters.join(" AND ");
          }
          return where;
        }
        QStringList generateVariablesList(const QHash<QString, QString>& _variables,
                                          const QStringList& _filter)
        {
          QStringList ret;

          for(const QString& v : _filter)
          {
            clog_assert(not v.isEmpty());
            if(_variables.contains(v))
            {
              ret << _variables.value(v) + " AS " + v;
            }
            else
            {
              clog_warning("No variable name {}", v);
            }
          }

          return ret;
        }

        QString generateVariables(const QHash<QString, QString>& _variables)
        {
          return generateVariables(_variables, _variables.keys());
        }

        QString generateVariables(const QHash<QString, QString>& _variables,
                                  const QStringList& _filter)
        {
          return generateVariablesList(_variables, _filter).join(", ");
        }

        QueryPart assembleQueryPart(const QList<QueryPart>& _parts,
                                    QueryPartContext* _output_context,
                                    const QueryPartContext& _parts_context)
        {
          QueryPart assembly;
          assembly.table_alias = "a" + QString::number(nextTableIndex++);

          if(not _parts.isEmpty())
          {
            assembly.source += "SELECT ";
            QHash<QString, QString> variables;

            for(const QueryPart& qp : _parts)
            {
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
              variables.unite(qp.variables);
#else
              variables.insert(qp.variables);
#endif
            }
            assembly.source += generateVariables(variables);

            QString source = _parts.first().source;
            if(source.startsWith("SELECT"))
            {
              source = "(" + source + ")";
            }
            if(not source.isEmpty())
            {
            }

            int index_in_union = 0;
            const QueryPart* first_part = nullptr;

            for(const QueryPart& qp : _parts)
            {
              QString source = qp.source;
              if(not source.isEmpty())
              {
                if(source.startsWith("SELECT"))
                {
                  source = "(" + source + ")";
                }

                if(index_in_union == 0)
                {
                  assembly.source += " FROM " + source + " " + qp.table_alias;
                  first_part = &qp;
                }
                else
                {
                  if(qp.optional)
                  {
                    assembly.source += " LEFT";
                  }
                  if(index_in_union == 1 and first_part->optional)
                  {
                    assembly.source += " RIGHT";
                  }
                  assembly.source += " JOIN " + source + " " + qp.table_alias;
                  if(not qp.conditions.empty())
                  {
                    assembly.source += " ON (" + qp.conditions.join(") AND (") + ") AND";
                  }
                  else
                  {
                    assembly.source += " ON";
                  }
                  assembly.source += " TRUE ";
                  if(not qp.filters.isEmpty())
                  {
                    assembly.source += "AND ( (" + qp.filters.join(") AND (") + ") )";
                  }
                }
                ++index_in_union;
              }
            }
            if(first_part)
            {
              assembly.source += generateWhere(*first_part);
            }
          }

          for(QHash<QString, QString>::const_iterator it = _parts_context.variablesmapping.begin();
              it != _parts_context.variablesmapping.end(); ++it)
          {
            if(_output_context->variablescolumn.contains(it.key()))
            {
              QString var = _output_context->variablescolumn.value(it.key());
              if(_parts_context.namedSource.isEmpty())
              {
                assembly.conditions << assembly.table_alias + "." + it.value() + " = " + var;
              }
              else
              {
                // In a named graph blank nodes cannot be compared
                assembly.conditions << "(" + assembly.table_alias + "." + it.value() + " = " + var
                                         + " AND NOT kdb_is_blank(" + var + "))";
              }
            }
            else
            {
              QString var = assembly.table_alias + "_" + it.value();
              QString field = assembly.table_alias + "." + it.value();
              _output_context->variablescolumn[it.key()] = field;
              clog_assert(var.toLower() == var);
              _output_context->variablesmapping[it.key()] = var;
              assembly.variables[var] = field;
            }
          }

          // Handle blanknodes

          for(QHash<knowRDF::BlankNode, QString>::const_iterator it
              = _parts_context.blanknodemapping_.begin();
              it != _parts_context.blanknodemapping_.end(); ++it)
          {
            if(_output_context->blanknodecolumn.contains(it.key()))
            {
              assembly.conditions << it.value() + " = "
                                       + _output_context->blanknodecolumn[it.key()];
            }
            else
            {
              QString var = assembly.table_alias + "_" + it.value();
              QString field = assembly.table_alias + "." + it.value();
              _output_context->blanknodecolumn[it.key()] = field;
              _output_context->blanknodemapping_[it.key()] = var;
              assembly.variables[var] = field;
            }
          }

          return assembly;
        }

        QueryPart assembleCombiningQueryPart(const QueryPart& _left,
                                             const QueryPartContext& _left_context,
                                             const QueryPart& _right,
                                             const QueryPartContext& _right_context,
                                             QueryPartContext* _output_context,
                                             const QString& _operator)
        {
          QueryPart assembly;
          assembly.table_alias = "a" + QString::number(nextTableIndex++);

          // First select the variables that will be part of the combination
          QHash<QString, QString> variablesmapping;

          // Generate columns for left and right side of the union
          QStringList left_variables;
          for(QHash<QString, QString>::const_iterator it = _left_context.variablescolumn.begin();
              it != _left_context.variablescolumn.end(); ++it)
          {
            QString var = assembly.table_alias + "_" + it.key().toLower();
            clog_assert(var.toLower() == var);
            variablesmapping[it.key()] = var;
          }
          QStringList right_variables;
          for(QHash<QString, QString>::const_iterator it = _right_context.variablescolumn.begin();
              it != _right_context.variablescolumn.end(); ++it)
          {
            QString var = assembly.table_alias + "_" + it.key().toLower();
            clog_assert(var.toLower() == var);
            variablesmapping[it.key()] = var;
          }

          // Make sure both side of the combination have the same number of columns and in the same
          // order
          for(QHash<QString, QString>::const_iterator it = variablesmapping.begin();
              it != variablesmapping.end(); ++it)
          {
            if(_left_context.variablescolumn.contains(it.key()))
            {
              left_variables << _left_context.variablescolumn[it.key()] + " AS " + it.value();
            }
            else
            {
              left_variables << "NULL AS " + it.value();
            }
            if(_right_context.variablescolumn.contains(it.key()))
            {
              right_variables << _right_context.variablescolumn[it.key()] + " AS " + it.value();
            }
            else
            {
              right_variables << "NULL AS " + it.value();
            }
          }

          // Generate query
          QString left_where = generateWhere(_left);
          QString right_where = generateWhere(_right);

          assembly.source = "SELECT " + left_variables.join(",") + " FROM (" + _left.source + ") "
                            + _left.table_alias + left_where + " " + _operator + " SELECT "
                            + right_variables.join(",") + " FROM (" + _right.source + ") "
                            + _right.table_alias + right_where;

          // Generate outgoing variable mapping
          for(QHash<QString, QString>::const_iterator it = variablesmapping.begin();
              it != variablesmapping.end(); ++it)
          {
            if(_output_context->variablescolumn.contains(it.key()))
            {
              QString var = _output_context->variablescolumn.value(it.key());
              assembly.conditions << assembly.table_alias + "." + it.value() + " = " + var;
            }
            else
            {
              QString var = assembly.table_alias + "_" + it.value();
              QString field = assembly.table_alias + "." + it.value();
              _output_context->variablescolumn[it.key()] = field;
              clog_assert(var.toLower() == var);
              _output_context->variablesmapping[it.key()] = var;
              assembly.variables[var] = field;
            }
          }

          // blank nodes are ignored in union?

          return assembly;
        }

        SPARQLAlgebraToPostgresSQLVisitor(const Connection& _connection,
                                          const QHash<QString, QString>& _sources)
            : connection(_connection), sources(_sources)
        {
        }
        QVariant visit(kDB::SPARQL::Algebra::ExecuteCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::AskQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          SPARQLAlgebraToPostgresSQL::SQLSelectQuery query;

          QueryPartContext query_context;
          query_context.source = sources[QString()];
          query_context.namedSource = QString();
          QueryPart query_where
            = _query->where()
                ? accept(_query->where(), QVariant::fromValue(&query_context)).value<QueryPart>()
                : QueryPart();

          query.valuesmapping = valuesmapping;

          QString where = toSQLWhere(query_where.filters);

          query.query = "SELECT COUNT(*) > 0 FROM (" + query_where.source + ") AS "
                        + query_where.table_alias + where;
          return QVariant::fromValue(query);
        }
        QVariant visit(kDB::SPARQL::Algebra::DescribeQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DescribeTermCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }

        QString toSQLModifier(kDB::SPARQL::Algebra::SelectModifier _modifier)
        {
          switch(_modifier)
          {
          case kDB::SPARQL::Algebra::SelectModifier::Distinct:
          case kDB::SPARQL::Algebra::SelectModifier::Reduced:
            return "DISTINCT ";
            break;
          case kDB::SPARQL::Algebra::SelectModifier::None:
            break;
          }
          return QString();
        }

        QString toSQLWhere(const QStringList& _filters)
        {
          QString where;
          if(not _filters.isEmpty())
          {
            where = " WHERE ";
            for(int i = 0; i < _filters.size(); ++i)
            {
              if(i != 0)
                where += " AND ";
              where += _filters[i];
            }
          }
          return where;
        }
        QString createConstructTemplate(kSA::NodeCSP _node, const QString& _constructor,
                                        const QHash<QString, QString>& variablesmapping,
                                        const QString& _table_alias, const QString& _field,
                                        QHash<knowRDF::BlankNode, QString>* _blankNodes,
                                        QStringList* _blankNodesDef, QStringList* _used_variables,
                                        bool* _has_unbound)
        {
          switch(_node->type())
          {
          case kSA::NodeType::VariableReference:
          {
            const kSA::VariableReference* var
              = static_cast<const kSA::VariableReference*>(_node.data());
            if(not *_has_unbound)
            {
              *_has_unbound = not variablesmapping.contains(var->name());
            }
            if(not _used_variables->contains(var->name()))
              _used_variables->append(var->name());
            return clog_qt::qformat(_constructor, variablesmapping.value(var->name()));
          }
          case kSA::NodeType::BlankNode:
          {
            const kSA::BlankNode* bn = static_cast<const kSA::BlankNode*>(_node.data());
            if(_blankNodes->contains(bn->blankNode()))
            {
              return clog_qt::qformat(_constructor, "wQ." + _blankNodes->value(bn->blankNode()));
            }
            else
            {
              QString name = _table_alias + _field;
              *_blankNodesDef << "kdb_bnode() AS " + name;
              (*_blankNodes)[bn->blankNode()] = name;
              return clog_qt::qformat(_constructor, "wQ." + name);
            }
          }
          case kSA::NodeType::Term:
          case kSA::NodeType::Value:
          {
            QString var = ":" + _table_alias + _field;
            switch(_node->type())
            {
            case kSA::NodeType::Term:
            {
              const kSA::Term* value = static_cast<const kSA::Term*>(_node.data());
              valuesmapping.insert(var, value->term());
              return clog_qt::qformat(_constructor, var);
            }
            case kSA::NodeType::Value:
            {
              clog_assert(_field == QString("object"));
              const kSA::Value* value = static_cast<const kSA::Value*>(_node.data());
              QString var_uri = ":uri_" + _table_alias + "_" + _field;
              QString var_lang = ":lang_" + _table_alias + "_" + _field;
              knowRDF::Literal lit = value->value();
              valuesmapping.insert(var, knowCore::Value(lit));
              valuesmapping.insert(var_uri, lit.datatype());
              valuesmapping.insert(var_lang, lit.lang());
              return clog_qt::qformat(_constructor, "kdb_rdf_term_create(" + var_uri
                                                      + ", kdb_rdf_value_create(" + var + "), "
                                                      + var_lang + ")");
            }
            default:
              qFatal("SHOULD NOT HAPPEN");
            }
            qFatal("HUGH?");
          }
          default:
            qFatal("INVALID QUERY?");
          }
        }
        QVariant visit(kDB::SPARQL::Algebra::ConstructQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);

          SPARQLAlgebraToPostgresSQL::SQLConstructQuery query;

          // Step 1: query for all the bindings for the variables
          QueryPartContext query_context;
          query_context.source = sources[QString()];
          query_context.namedSource = QString();
          QueryPart query_where
            = _query->where()
                ? accept(_query->where(), QVariant::fromValue(&query_context)).value<QueryPart>()
                : QueryPart();

          QHash<QString, QString> variablesmapping;

          QHash<QString, QString> variables;
          for(QHash<QString, QString>::const_iterator it = query_context.variablesmapping.begin();
              it != query_context.variablesmapping.end(); ++it)
          {
            QString variable = "wqd_" + it.value();
            clog_assert(variable == variable.toLower());
            variablesmapping[it.key()] = variable;
            variables[variable] = it.value();
          }

          // Step 2: Generate the template as an array that is then unnested
          QStringList triplesQueryParts;
          QHash<knowRDF::BlankNode, QString> blankNodes;
          QStringList blankNodesDef;
          QStringList used_variables;

          for(int i = 0; i < _query->constructTemplate().size(); ++i)
          {
            kDB::SPARQL::Algebra::TripleCSP triple = _query->constructTemplate()[i];
            QString _table_alias = clog_qt::qformat("tQ{}", i);
            bool has_unbound = false;
            QString template_
              = "("
                + createConstructTemplate(triple->subject(), "{}", variablesmapping, _table_alias,
                                          "subject", &blankNodes, &blankNodesDef, &used_variables,
                                          &has_unbound)
                + ", "
                + createConstructTemplate(triple->predicate(), "{}", variablesmapping, _table_alias,
                                          "predicate", &blankNodes, &blankNodesDef, &used_variables,
                                          &has_unbound)
                + ", "
                + createConstructTemplate(triple->object(), "kdb_rdf_term_create({})",
                                          variablesmapping, _table_alias, "object", &blankNodes,
                                          &blankNodesDef, &used_variables, &has_unbound)
                + ", NULL)::kdb_rdf_triple";
            if(not has_unbound)
            {
              triplesQueryParts.append(template_);
            }
          }

          QStringList where_no_null;
          QStringList where_variables, where_variables_2;
          for(const QString& used_var : used_variables)
          {
            where_no_null << "NOT (" + query_context.variablesmapping[used_var] + " IS NULL)";
            where_variables << query_context.variablesmapping[used_var];
            where_variables_2 << "wqd_" + query_context.variablesmapping[used_var];
          }

          // Step 3: assemble the query
          QString where = toSQLWhere(query_where.filters);
          QString orderClause
            = accept(_query->orderClauses(), QVariant::fromValue(variablesmapping)).toString();

          QString whereQuery
            = "(SELECT "
              + (generateVariablesList(variables, where_variables_2) + blankNodesDef).join(", ")
              + " FROM (SELECT DISTINCT "
              + generateVariables(query_where.variables, where_variables) + " FROM ("
              + query_where.source + ") AS " + query_where.table_alias + where + orderClause
              + ") as wQ2 WHERE " + where_no_null.join(" AND ") + ") as wQ";

          QString limitClause = accept(_query->limitOffsetClause(), QVariant()).toString();

          query.query = "SELECT (arr_unnest).subject AS subject, (arr_unnest).predicate AS "
                        "predicate, (arr_unnest).object AS object FROM (SELECT unnest(tQ.arr) as "
                        "arr_unnest FROM (SELECT ARRAY["
                        + triplesQueryParts.join(",") + "] AS arr FROM " + whereQuery
                        + ") AS tQ) AS tQ2";
          query.valuesmapping = valuesmapping;

          return QVariant::fromValue(query);
        }
        QVariant visit(kDB::SPARQL::Algebra::ExplainQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::SelectQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          SPARQLAlgebraToPostgresSQL::SQLSelectQuery query;

          QueryPartContext query_context;
          query_context.source = sources[QString()];
          query_context.namedSource = QString();
          QueryPart query_where
            = _query->where()
                ? accept(_query->where(), QVariant::fromValue(&query_context)).value<QueryPart>()
                : QueryPart();

          query.variable_list = _query->variables();
          //           query.variablesmapping = query_context.variablesmapping;

          // Prepare the list of variables that needs to be returned by the SQL query
          QHash<QString, QString> query_variables;
          for(const kSA::VariableCSP& var : query.variable_list)
          {
            QString sql_name;
            // If it is an expression, it needs to be converted to SQL
            if(var->expression())
            {
              sql_name = var->name() + "_" + query_where.table_alias;
              query_variables[sql_name]
                = accept(var->expression(), QVariant::fromValue(&query_context)).toString();
            }
            else
            {
              sql_name = query_context.variablesmapping.value(var->name());
              if(not sql_name.isEmpty()) // this can happen with service, if the variable is listed
                                         // in select but comes from a service
              {
                query_variables[sql_name] = query_where.variables[sql_name];
              }
            }
            if(not sql_name.isEmpty())
            {
              query.variablesmapping[var->name()] = sql_name;
            }
          }

          // Add variables that are needed for service call but not explicitely required
          for(const QString& sv_name : query_context.serviceVariables)
          {
            if(not query.variablesmapping.contains(sv_name))
            {
              QString sql_name = query_context.variablesmapping.value(sv_name);
              if(not sql_name.isEmpty())
              {
                query_variables[sql_name] = query_where.variables[sql_name];
                clog_assert(sql_name == sql_name.toLower());
                query.variablesmapping[sv_name] = sql_name;
              }
            }
          }
          // Generate parts of the queries
          query.valuesmapping = valuesmapping;

          QString modifier = toSQLModifier(_query->modifier());

          QString where = toSQLWhere(query_where.filters);

          QString orderClause
            = accept(_query->orderClauses(), QVariant::fromValue(query.variablesmapping))
                .toString();
          QString limitClause = accept(_query->limitOffsetClause(), QVariant()).toString();
          QString groupClause
            = accept(_query->groupClauses(), QVariant::fromValue(&query_context)).toString();

          if(query_where.source.isEmpty())
          {
            query.query = "SELECT " + modifier + generateVariables(query_variables) + orderClause
                          + limitClause + groupClause;
          }
          else
          {
            query.query = "SELECT " + modifier + generateVariables(query_variables) + " FROM ("
                          + query_where.source + ") AS " + query_where.table_alias + where
                          + orderClause + limitClause + groupClause;
          }
          return QVariant::fromValue(query);
        }

        QString
          generateDeleteInsertView(const QString& _view_name,
                                   const SPARQL::Algebra::QuadsDataCSP& _quadsData,
                                   const QHash<QString, QString>& _variablesmapping,
                                   const QHash<QString, QString>& _query_context_variablesmapping,
                                   const QHash<QString, QString>& _variables,
                                   const QHash<QString, QString>& _query_where_variables)
        {
          Q_UNUSED(_query_where_variables);
          //           const QString& _field, QHash<knowRDF::BlankNode, QString>* _blankNodes,
          //           QStringList* _blankNodesDef, QStringList* _used_variables

          QStringList triplesQueryParts;
          QHash<knowRDF::BlankNode, QString> blankNodes;
          QStringList blankNodesDef;
          QStringList used_variables;
          int index = 0;
          for(const SPARQL::Algebra::QuadsCSP& quads : _quadsData->quads())
          {
            QString uri_name;

            if(quads->source())
            {
              switch(quads->source()->type())
              {
              case kDB::SPARQL::Algebra::NodeType::VariableReference:
              {
                uri_name
                  = _variablesmapping[static_cast<const kDB::SPARQL::Algebra::VariableReference*>(
                                        quads->source().data())
                                        ->name()];
                break;
              }
              case kDB::SPARQL::Algebra::NodeType::GraphReference:
              {
                uri_name = ":" + clog_qt::qformat(_view_name + "{}", ++index) + "_graph";
                valuesmapping.insert(
                  uri_name,
                  static_cast<const kDB::SPARQL::Algebra::GraphReference*>(quads->source().data())
                    ->name());
                break;
              }
              default:
                qFatal("Internal error!");
              }
            }
            else
            {
              uri_name = "''::kdb_uri";
            }

            for(const kDB::SPARQL::Algebra::TripleCSP& triple : quads->triples())
            {
              QString _table_alias = clog_qt::qformat(_view_name + "{}", ++index);
              bool has_unbound = false;
              QString template_
                = "kdb_rdf_quad_create(" + uri_name + ", "
                  + createConstructTemplate(triple->subject(), "kdb_iri({})", _variablesmapping,
                                            _table_alias, "subject", &blankNodes, &blankNodesDef,
                                            &used_variables, &has_unbound)
                  + ", "
                  + createConstructTemplate(triple->predicate(), "kdb_iri({})", _variablesmapping,
                                            _table_alias, "predicate", &blankNodes, &blankNodesDef,
                                            &used_variables, &has_unbound)
                  + ", "
                  + createConstructTemplate(triple->object(), "kdb_rdf_term_create({})",
                                            _variablesmapping, _table_alias, "object", &blankNodes,
                                            &blankNodesDef, &used_variables, &has_unbound)
                  + ", NULL)";
              if(not has_unbound)
              {
                triplesQueryParts.append(template_);
              }
            }
          }

          if(triplesQueryParts.isEmpty())
            return QString();

          QStringList where_no_null;
          QStringList where_variables, where_variables_2;
          for(const QString& used_var : used_variables)
          {
            where_no_null << "NOT (" + _query_context_variablesmapping[used_var] + " IS NULL)";
            where_variables << _query_context_variablesmapping[used_var];
            where_variables_2 << "wqd_" + _query_context_variablesmapping[used_var];
          }

          return "CREATE TEMPORARY TABLE " + _view_name
                 + " ON COMMIT DROP AS (SELECT row_number() OVER () AS id, (arr_unnest).graph AS "
                   "graph, (arr_unnest).subject AS subject, (arr_unnest).predicate AS predicate, "
                   "(arr_unnest).object AS object, (arr_unnest).md5 AS md5 FROM (SELECT "
                   "unnest(tQ.arr) as arr_unnest FROM (SELECT ARRAY["
                 + triplesQueryParts.join(",") + "] AS arr FROM (SELECT "
                 + (generateVariablesList(_variables, where_variables_2) + blankNodesDef).join(", ")
                 + " FROM (SELECT DISTINCT " + where_variables.join(", ")
                 + " FROM delete_insert_where_tmp) AS wQ2 WHERE " + where_no_null.join(" AND ")
                 + ") as wQ) AS tQ) AS tQ2)";
        }

        QVariant visit(kDB::SPARQL::Algebra::DeleteInsertCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          SPARQLAlgebraToPostgresSQL::SQLDeleteInsertQuery query;

          // Step 1 generate the where query

          QueryPartContext query_context;
          query_context.source = sources[QString()];
          query_context.namedSource = QString();
          QueryPart query_where
            = _query->where()
                ? accept(_query->where(), QVariant::fromValue(&query_context)).value<QueryPart>()
                : QueryPart();

          QHash<QString, QString> variablesmapping;
          QHash<QString, QString> variables;
          for(QHash<QString, QString>::const_iterator it = query_context.variablesmapping.begin();
              it != query_context.variablesmapping.end(); ++it)
          {
            QString variable = "wqd_" + it.value();
            clog_assert(variable == variable.toLower());
            variablesmapping[it.key()] = variable;
            variables[variable] = it.value();
          }

          // Step 2: generate the templates for deletion
          if(_query->deleteTemplate())
          {
            query.deletionTemplate = generateDeleteInsertView(
              "delete_tmp", _query->deleteTemplate(), variablesmapping,
              query_context.variablesmapping, variables, query_where.variables);
          }

          // Step 3: generate the templates for insertion
          if(_query->insertTemplate())
          {
            query.insertionTemplate = generateDeleteInsertView(
              "insert_tmp", _query->insertTemplate(), variablesmapping,
              query_context.variablesmapping, variables, query_where.variables);
          }

          // Step 4: assemble the where query table
          QString where = toSQLWhere(query_where.filters);

          query.whereTableQuery
            = "CREATE TEMPORARY TABLE delete_insert_where_tmp ON COMMIT DROP AS (SELECT "
              + generateVariables(query_where.variables) + " FROM (" + query_where.source + ") AS "
              + query_where.table_alias + where + ")";
          query.valuesmapping = valuesmapping;

          return QVariant::fromValue(query);
        }
        QVariant visit(kDB::SPARQL::Algebra::PLQueryCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::InsertDataCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DeleteDataCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LoadCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DropCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ClearCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::CreateCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::QuadsCSP _query, const QVariant& _parameter) override
        {
          return visitGroupOrQuad(_query->triples(), QList<kDB::SPARQL::Algebra::NodeCSP>(),
                                  _query->source(), _parameter,
                                  QList<kDB::SPARQL::Algebra::ServiceCSP>());
        }
        QVariant visit(kDB::SPARQL::Algebra::QuadsDataCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          return visitGroupOrQuad(_query->quads(), QList<kDB::SPARQL::Algebra::NodeCSP>(), nullptr,
                                  _parameter, QList<kDB::SPARQL::Algebra::ServiceCSP>());
        }
        QVariant visit(kDB::SPARQL::Algebra::ListCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::VariableCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DatasetCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::GroupClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          QString r;
          if(_query)
          {
            for(kDB::SPARQL::Algebra::NodeCSP gc : _query->list())
            {
              if(not r.isEmpty())
                r += ", ";
              r += accept(gc, _parameter).toString();
            }
            r = " GROUP BY " + r;
          }
          return r;
        }
        QVariant visit(kDB::SPARQL::Algebra::HavingClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::OrderClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          QString r;
          if(_query)
          {
            for(kDB::SPARQL::Algebra::OrderClauseCSP oc : _query->list())
            {
              r += visit(oc, _parameter).toString();
            }
          }
          return r;
        }
        QVariant visit(kDB::SPARQL::Algebra::OrderClauseCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          QString r = QStringLiteral(" ORDER BY ");
          QString direction;
          switch(_query->orderDirection())
          {
          case kDB::SPARQL::Algebra::OrderDirection::Asc:
            break;
          case kDB::SPARQL::Algebra::OrderDirection::Desc:
            direction = QStringLiteral(" DESC ");
            break;
          }
          QString varname;
          switch(_query->node()->type())
          {
          case kSA::NodeType::VariableReference:
          {
            const kSA::VariableReference* var
              = static_cast<const kSA::VariableReference*>(_query->node().data());
            varname = _parameter.value<QHash<QString, QString>>().value(var->name());
            break;
          }
          default:
            qFatal("need variable reference");
          }
          return r + varname + direction;
        }
        QVariant visit(kDB::SPARQL::Algebra::LimitOffsetClauseCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          QString r;
          if(_query)
          {
            if(_query->limit() > -1)
            {
              r += " LIMIT " + QString::number(_query->limit());
            }
            if(_query->offset() > -1)
            {
              r += " OFFSET " + QString::number(_query->offset());
            }
          }
          return r;
        }
        void createColumnWhereExpression(QueryPartContext* _context, QueryPart& _part,
                                         kSA::NodeCSP _node, const QString& _table_alias,
                                         const QString& _field)
        {
          switch(_node->type())
          {
          case kSA::NodeType::VariableReference:
          {
            const kSA::VariableReference* var
              = static_cast<const kSA::VariableReference*>(_node.data());
            QString qvar = var->name();
            if(_context->variablescolumn.contains(qvar))
            {
              QString var = _context->variablescolumn.value(qvar);
              _part.conditions << _table_alias + "." + _field + " = " + var;
            }
            else
            {
              QString var = _table_alias + "_" + _field;
              QString field = _table_alias + "." + _field;
              _context->variablescolumn[qvar] = field;
              clog_assert(var == var.toLower());
              _context->variablesmapping[qvar] = var;
              _part.variables[var] = field;
            }
            break;
          }
          case kSA::NodeType::BlankNode:
          {
            const kSA::BlankNode* bn_node = static_cast<const kSA::BlankNode*>(_node.data());
            knowRDF::BlankNode bn = bn_node->blankNode();
            if(_context->blanknodemapping_.contains(bn))
            {
              _part.conditions << _table_alias + "." + _field + " = "
                                    + _context->blanknodecolumn.value(bn);
            }
            else
            {
              QString var = _table_alias + "_" + _field;
              QString field = _table_alias + "." + _field;
              _context->blanknodecolumn[bn] = field;
              _context->blanknodemapping_[bn] = var;
              _part.variables[var] = field;
            }
            break;
          }
          case kSA::NodeType::Term:
          case kSA::NodeType::Value:
          {
            QString var = ":" + _table_alias + "_" + _field;
            switch(_node->type())
            {
            case kSA::NodeType::Term:
            {
              const kSA::Term* value = static_cast<const kSA::Term*>(_node.data());
              valuesmapping.insert(var, value->term());
              _part.conditions << _table_alias + "." + _field + " = " + var;
              break;
            }
            case kSA::NodeType::Value:
            {
              clog_assert(_field == QString("object"));
              const kSA::Value* value = static_cast<const kSA::Value*>(_node.data());
              QString var_uri = ":uri_" + _table_alias + "_" + _field;
              QString var_lang = ":lang_" + _table_alias + "_" + _field;
              knowRDF::Literal lit = value->value();
              valuesmapping.insert(var, knowCore::Value(lit));
              valuesmapping.insert(var_uri, lit.datatype());
              valuesmapping.insert(var_lang, lit.lang());
              _part.conditions << _table_alias + ".object = kdb_rdf_term_create(" + var_uri
                                    + ", kdb_rdf_value_create(" + var + "), " + var_lang + ")";
              break;
            }
            default:
              qFatal("SHOULD NOT HAPPEN");
            }
            break;
          }
          default:
            qFatal("INVALID QUERY?");
          }
        }
        QVariant createBinaryOperator(const QString& _op, kDB::SPARQL::Algebra::NodeCSP _left,
                                      kDB::SPARQL::Algebra::NodeCSP _right,
                                      const QVariant& _parameter)
        {
          QString left = accept(_left, _parameter).value<QString>();
          QString right = accept(_right, _parameter).value<QString>();

          return QVariant::fromValue(left + _op + right);
        }
        QVariant visit(kDB::SPARQL::Algebra::TripleCSP _query, const QVariant& _parameter) override
        {

          QString table_alias = "a" + QString::number(nextTableIndex++);

          QueryPart part;
          QueryPartContext* context = _parameter.value<QueryPartContext*>();
          part.source = context->source;
          if(not context->namedSource.isEmpty())
          {
            if(context->variablescolumn.contains(context->namedSource))
            {
              QString var = context->variablescolumn.value(context->namedSource);
              part.conditions << table_alias + ".graph = " + var;
            }
            else
            {
              QString var = table_alias + "_graph";
              QString field = table_alias + ".graph";
              context->variablescolumn[context->namedSource] = field;
              clog_assert(var == var.toLower());
              context->variablesmapping[context->namedSource] = var;
              part.variables[var] = field;
            }
          }
          createColumnWhereExpression(context, part, _query->subject(), table_alias, "subject");
          createColumnWhereExpression(context, part, _query->predicate(), table_alias, "predicate");
          createColumnWhereExpression(context, part, _query->object(), table_alias, "object");
          part.table_alias = table_alias;

          return QVariant::fromValue(part);
        }
        QVariant visit(kDB::SPARQL::Algebra::BindCSP _query, const QVariant& _parameter) override
        {
          QueryPart part;
          part.table_alias = "a" + QString::number(nextTableIndex++);
          QueryPartContext* context = _parameter.value<QueryPartContext*>();

          QString qvar = _query->name();
          clog_assert(not context->variablescolumn.contains(qvar));

          QString var = part.table_alias + "_" + qvar;
          QString expr = accept(_query->expression(), _parameter).value<QString>();
          if(expr.isEmpty())
          {
            expr = "NULL";
          }
          else
          {
            expr = "(" + expr + ")";
          }
          part.variables[var] = expr;

          context->variablescolumn[qvar] = expr;
          clog_assert(var == var.toLower());
          context->variablesmapping[qvar] = var;

          return QVariant::fromValue(part);
        }
        QVariant visit(kDB::SPARQL::Algebra::VariableReferenceCSP _query,
                       const QVariant& _parameter) override
        {
          QString prefix
            = _parameter.value<QueryPartContext*>()->variablescolumn.value(_query->name());
          if(prefix.isEmpty())
          {
            return "NULL";
          }
          else
          {
            return QVariant::fromValue(prefix);
          }
        }
        QVariant visit(kDB::SPARQL::Algebra::GraphReferenceCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::TermCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          QString var = clog_qt::qformat(":var_{}", ++next_var_id);
          valuesmapping.insert(var, _query->term());
          return QVariant::fromValue(var);
        }
        QVariant visit(kDB::SPARQL::Algebra::BlankNodeCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }

        template<typename _T_>
        QVariant visitGroupOrQuad(const QList<_T_>& _graphPatterns,
                                  const QList<kDB::SPARQL::Algebra::NodeCSP>& _filters,
                                  kDB::SPARQL::Algebra::NodeCSP _source, const QVariant& _parameter,
                                  const QList<kDB::SPARQL::Algebra::ServiceCSP>& _services)
        {
          QueryPartContext* parentContext = _parameter.value<QueryPartContext*>();
          QList<QueryPart> parts;
          QueryPartContext group_context;

          if(_source)
          {
            switch(_source->type())
            {
            case kDB::SPARQL::Algebra::NodeType::VariableReference:
            {
              QString name = static_cast<const kSA::VariableReference*>(_source.data())->name();
              group_context.source = sources["all_namedgraph"];
              group_context.namedSource = name;
              break;
            }
            case kDB::SPARQL::Algebra::NodeType::GraphReference:
            {
              QString name = static_cast<const kSA::GraphReference*>(_source.data())->name();
              group_context.source
                = sources.value(name, "(SELECT null::text AS subject, null::text AS predicate, "
                                      "null::kdb_rdf_term AS object LIMIT 0)");
              group_context.namedSource = QString();
              break;
            }
            default:
              qFatal("Internal error!");
            }
          }
          else
          {
            group_context.source = parentContext->source;
            group_context.namedSource = parentContext->namedSource;
          }
          clog_assert(not group_context.source.isEmpty());
          for(kSA::NodeCSP node : _graphPatterns)
          {
            parts.append(accept(node, QVariant::fromValue(&group_context)).value<QueryPart>());
          }

          QueryPart part = assembleQueryPart(parts, parentContext, group_context);
          QueryPartContext filter_context = group_context;
          for(const QString& key : filter_context.variablescolumn.keys())
          { // We need to update the variable in group_context to match the one from parentContext
            // (since filters are applied in parentContext)
            filter_context.variablescolumn[key] = parentContext->variablescolumn[key];
          }
          if(not _filters.empty())
          {
            for(kSA::NodeCSP node : _filters)
            {
              part.filters.append(
                accept(node, QVariant::fromValue(&filter_context)).value<QString>());
            }
          }
          merge_unique(&parentContext->serviceVariables, group_context.serviceVariables);
          for(const kDB::SPARQL::Algebra::ServiceCSP& service : _services)
          {
            merge_unique(&parentContext->serviceVariables,
                         kDB::SPARQL::Algebra::Visitors::VariablesLister::list(service));
          }
          return QVariant::fromValue(part);
        }
        QVariant visit(kDB::SPARQL::Algebra::ServiceCSP /*_node*/,
                       const QVariant& /*_parameter*/) override
        {
          qFatal("wip");
        }
        QVariant visit(kDB::SPARQL::Algebra::GroupGraphPatternCSP _groupGraphPattern,
                       const QVariant& _parameter) override
        {
          return visitGroupOrQuad(_groupGraphPattern->graphPatterns(),
                                  _groupGraphPattern->filters(), _groupGraphPattern->source(),
                                  _parameter, _groupGraphPattern->services());
        }
        QVariant visit(kDB::SPARQL::Algebra::ValueCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          QString var_uri = clog_qt::qformat(":var_{}", ++next_var_id);
          QString var_val = clog_qt::qformat(":var_{}", ++next_var_id);
          QString var_lang = clog_qt::qformat(":var_{}", ++next_var_id);

          knowRDF::Literal lit = _query->value();
          valuesmapping.insert(var_uri, lit.datatype());
          valuesmapping.insert(var_val, knowCore::Value(lit));
          valuesmapping.insert(var_lang, lit.lang());

          return QVariant::fromValue("kdb_rdf_term_create(" + var_uri + ", " + var_val + ", "
                                     + var_lang + ")");
        }
        QVariant visit(kDB::SPARQL::Algebra::OptionalCSP _query,
                       const QVariant& _parameter) override
        {
          QueryPart part = accept(_query->group(), _parameter).value<QueryPart>();
          part.optional = true;
          return QVariant::fromValue(part);
        }
        QVariant visit(kDB::SPARQL::Algebra::UnionCSP _query, const QVariant& _parameter) override
        {
          QueryPartContext* parentContext = _parameter.value<QueryPartContext*>();
          QueryPartContext left_context;
          left_context.source = parentContext->source;
          left_context.namedSource = parentContext->namedSource;
          QueryPart left
            = accept(_query->left(), QVariant::fromValue(&left_context)).value<QueryPart>();
          QueryPartContext right_context;
          right_context.source = parentContext->source;
          right_context.namedSource = parentContext->namedSource;
          QueryPart right
            = accept(_query->right(), QVariant::fromValue(&right_context)).value<QueryPart>();

          return QVariant::fromValue(assembleCombiningQueryPart(
            left, left_context, right, right_context, parentContext, "UNION ALL"));
        }
        QVariant visit(kDB::SPARQL::Algebra::MinusCSP _query, const QVariant& _parameter) override
        {
          QueryPartContext* parentContext = _parameter.value<QueryPartContext*>();
          QueryPartContext left_context;
          left_context.source = parentContext->source;
          left_context.namedSource = parentContext->namedSource;
          QueryPart left
            = accept(_query->left(), QVariant::fromValue(&left_context)).value<QueryPart>();
          QueryPartContext right_context;
          right_context.source = parentContext->source;
          right_context.namedSource = parentContext->namedSource;
          QueryPart right
            = accept(_query->right(), QVariant::fromValue(&right_context)).value<QueryPart>();

          return QVariant::fromValue(assembleCombiningQueryPart(
            left, left_context, right, right_context, parentContext, "EXCEPT"));
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalOrCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator(" OR ", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalAndCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator(" AND ", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalEqualCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator("=", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalDifferentCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator("!=", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInferiorCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator("<", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalSuperiorCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator(">", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInferiorEqualCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator("<=", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalSuperiorEqualCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator(">=", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::AdditionCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator("+", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::SubstractionCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator("-", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::MultiplicationCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator("*", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::DivisionCSP _query,
                       const QVariant& _parameter) override
        {
          return createBinaryOperator("/", _query->left(), _query->right(), _parameter);
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalNotInCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalNegationCSP _query,
                       const QVariant& _parameter) override
        {
          QString left = accept(_query->value(), _parameter).value<QString>();
          return QVariant::fromValue("NOT " + left);
        }
        QVariant visit(kDB::SPARQL::Algebra::NegationCSP _query,
                       const QVariant& _parameter) override
        {
          QString left = accept(_query->value(), _parameter).value<QString>();
          return QVariant::fromValue("-" + left);
        }
        QVariant visit(kDB::SPARQL::Algebra::FunctionCallCSP _query,
                       const QVariant& _parameter) override
        {
          knowCore::Uri name = _query->name();

          QStringList arguments;
          knowCore::UriList arguments_types;

          SPARQLFunctionsManagerFunctionTypeInterface interface(
            connection.sparqlFunctionsManager());
          for(kDB::SPARQL::Algebra::NodeCSP node : _query->parameters())
          {
            arguments.push_back(accept(node, _parameter).toString());
            arguments_types.push_back(
              kDB::SPARQL::Algebra::Visitors::ExpressionType::type(node, &interface));
          }
          SPARQLFunctionDefinition fd
            = connection.sparqlFunctionsManager()->function(name, arguments_types);
          if(not fd.isValid())
          {
            QString name_pp = name;
            QString kfb = knowCore::Uris::askcore_sparql_functions::base;
            if(name_pp.startsWith(kfb))
            {
              name_pp = name_pp.right(name_pp.length() - kfb.length());
            }
            errorMessages.append(clog_qt::qformat("No function '{}' matching arguments.", name_pp));
            return QString("null");
          }

          QString functioncall = fd.sqlTemplate();
          for(int i = 0; i < arguments.size(); ++i)
          {
            functioncall = functioncall.replace(clog_qt::qformat("${}", i), arguments[i]);
          }

          return functioncall;
        }
        QVariant visit(kDB::SPARQL::Algebra::IfCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::UnlessCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        int nextTableIndex = 0;
        int next_var_id = 0;
        Connection connection;
        knowCore::ValueHash valuesmapping;
        QHash<QString, QString> sources;
      };
    } // namespace SPARQLExecution
  } // namespace Repository
} // namespace kDB

Q_DECLARE_METATYPE(SPARQLAlgebraToPostgresSQLVisitor::QueryPart)
Q_DECLARE_METATYPE(SPARQLAlgebraToPostgresSQLVisitor::QueryPartContext*)
Q_DECLARE_METATYPE(SPARQLAlgebraToPostgresSQL::SQLDeleteInsertQuery)
Q_DECLARE_METATYPE(SPARQLAlgebraToPostgresSQL::SQLSelectQuery)
Q_DECLARE_METATYPE(SPARQLAlgebraToPostgresSQL::SQLConstructQuery)

SPARQLAlgebraToPostgresSQL::SPARQLAlgebraToPostgresSQL(const SPARQL::Algebra::NodeCSP& _q,
                                                       const Connection& _c,
                                                       const QHash<QString, QString>& _sources)
    : d(new Private)
{
  d->query = _q;
  d->connection = _c;
  d->sources = _sources;
}

SPARQLAlgebraToPostgresSQL::~SPARQLAlgebraToPostgresSQL() { delete d; }

#define VISIT(_RT_)                                                                                \
  SPARQLAlgebraToPostgresSQLVisitor satpsv(d->connection, d->sources);                             \
  SPARQLAlgebraToPostgresSQL::_RT_ r                                                               \
    = satpsv.start(d->query, QVariant()).value<SPARQLAlgebraToPostgresSQL::_RT_>();                \
  if(satpsv.errorMessages.isEmpty())                                                               \
  {                                                                                                \
    return cres_success(r);                                                                        \
  }                                                                                                \
  else                                                                                             \
  {                                                                                                \
    return cres_failure(satpsv.errorMessages.join("\n"));                                          \
  }

cres_qresult<SPARQLAlgebraToPostgresSQL::SQLDeleteInsertQuery>
  SPARQLAlgebraToPostgresSQL::buildDeleteInsertQuery() const
{
  VISIT(SQLDeleteInsertQuery);
}

cres_qresult<SPARQLAlgebraToPostgresSQL::SQLSelectQuery>
  SPARQLAlgebraToPostgresSQL::buildSelectQuery() const
{
  VISIT(SQLSelectQuery);
}

cres_qresult<SPARQLAlgebraToPostgresSQL::SQLConstructQuery>
  SPARQLAlgebraToPostgresSQL::buildConstructQuery() const
{
  VISIT(SQLConstructQuery);
}
