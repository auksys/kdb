#include <knowCore/ValueHash.h>

#include <kDB/SPARQL/Algebra/Nodes.h>

namespace kDB::Repository::SPARQLExecution
{
  class SPARQLAlgebraToPostgresSQL
  {
  public:
    struct SQLDeleteInsertQuery
    {
      knowCore::ValueHash valuesmapping;
      QString whereTableQuery, deletionTemplate, insertionTemplate;
    };
    struct SQLSelectQuery
    {
      QHash<QString, QString> variablesmapping;
      knowCore::ValueHash valuesmapping;
      QList<SPARQL::Algebra::VariableCSP> variable_list;
      QString query;
    };
    struct SQLConstructQuery
    {
      QString query;
      knowCore::ValueHash valuesmapping;
    };
  public:
    SPARQLAlgebraToPostgresSQL(const SPARQL::Algebra::NodeCSP& _q, const Connection& _c,
                               const QHash<QString, QString>& _sources);
    ~SPARQLAlgebraToPostgresSQL();
    cres_qresult<SQLDeleteInsertQuery> buildDeleteInsertQuery() const;
    cres_qresult<SQLSelectQuery> buildSelectQuery() const;
    cres_qresult<SQLConstructQuery> buildConstructQuery() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::SPARQLExecution
