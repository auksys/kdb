#include "QueryExecutor.h"

#include <clog_qt>

#include <QFile>
#include <QUrl>
#include <QVariant>

#include "config.h"
#include <knowCore/Messages.h>
#include <knowCore/Uri.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/ValueHash.h>

#include <kDB/SPARQL/Algebra/Nodes.h>
#include <kDB/SPARQL/Query.h>

#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/xsd.h>
#include <knowRDF/Object.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/SPARQL/Algebra/Visitors/Print.h>

#include "Connection_p.h"
#include "DatasetsUnion.h"
#include "EmptyRDFDataset.h"
#include "GraphsManager.h"
#include "RDFEnvironment.h"
#include "Transaction.h"
#include "TripleStore.h"
#include "TriplesView.h"

#include "../DatabaseInterface/PostgreSQL/BinaryMarshalsRegistry.h"
#include "../DatabaseInterface/PostgreSQL/SQLInterface_p.h"

#include "QueryExecutorVisitor_p.h"

using namespace kDB::Repository::SPARQLExecution;

#include <kDB/Forward.h>

struct QueryExecutor::Private
{
  RDFEnvironment environment;
};

QueryExecutor::QueryExecutor(const RDFEnvironment& _environment) : d(new Private())
{
  d->environment = _environment;
}

QueryExecutor::~QueryExecutor() { delete d; }

knowDBC::Result QueryExecutor::execute(const QString& _query, const knowCore::ValueHash& _options,
                                       const knowCore::ValueHash& _bindings)
{
  Q_UNUSED(_options);
  if(not d->environment.isValid())
  {
    return knowDBC::Result::create(_query, "Not connected to a database.");
  }
  knowCore::Messages messages;
  QList<kDB::SPARQL::Query> qs
    = kDB::SPARQL::Query::parse(_query, _bindings, &messages, d->environment.base());
  if(qs.isEmpty())
  {
    clog_assert(messages.hasErrors());
    return knowDBC::Result::create(_query, messages.toString());
  }

  knowDBC::Result r;
  for(const kDB::SPARQL::Query& q : qs)
  {
#ifdef KDB_ENABLE_SPARQL_QUERY_DEBUG
    if(clog_is_enabled(clog_level_debug))
    {
      kDB::SPARQL::Algebra::Visitors::Print pv;
      pv.start(q.getNode());
    }
#endif
    switch(q.type())
    {
    case kDB::SPARQL::Query::Type::Invalid:
      qFatal("Should not happen");
    case kDB::SPARQL::Query::Type::Execute:
    {
      kDB::SPARQL::Algebra::ExecuteCSP executenode
        = dynamic_cast<const kDB::SPARQL::Algebra::Execute*>(q.getNode());
      knowDBC::Query sub_q = d->environment.connection().createSPARQLQuery(
        RDFEnvironment(d->environment).setBase(executenode->uri()));
      cres_qresult<void> rv = sub_q.loadFromFile(executenode->uri());
      if(rv.is_successful())
      {
        r = sub_q.execute();
      }
      else
      {
        return knowDBC::Result::create(_query, rv.get_error().get_message());
      }
      break;
    }
    case kDB::SPARQL::Query::Type::Explain:
    case kDB::SPARQL::Query::Type::Construct:
    case kDB::SPARQL::Query::Type::Select:
    case kDB::SPARQL::Query::Type::Ask:
    case kDB::SPARQL::Query::Type::Update:
    case kDB::SPARQL::Query::Type::PL:
    {
      QueryExecutorVisitor uev;
      uev.defaultDataset = d->environment.defaultDataset();
      uev.namedDatasets = d->environment.namedDatasets();
      uev.connection = d->environment.connection();
      uev.query_text = _query;
      uev.services = d->environment.services();
      uev.start(q.getNode(), QVariant());
      clog_assert(uev.result.query() == _query);
      r = uev.result;
      break;
    }
    case kDB::SPARQL::Query::Type::Describe:
      return knowDBC::Result::create(_query, "Unsupported query type.");
    }
  }

  return r;
}

knowCore::Uri QueryExecutor::queryLanguage() const
{
  return knowCore::Uris::askcore_db_query_language::SPARQL;
}
