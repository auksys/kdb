#include "QueryConnectionInfo.h"

#include <knowCore/Value.h>
#include <knowDBC/Interfaces/QueryExecutor.h>

#include "Services.h"

namespace kDB::Repository::SPARQLExecution
{
  class QueryExecutor : public knowDBC::Interfaces::QueryExecutor
  {
  public:
    QueryExecutor(const RDFEnvironment& _rdf_environment);
    ~QueryExecutor();
    knowDBC::Result execute(const QString& _query, const knowCore::ValueHash& _options,
                            const knowCore::ValueHash& _bindings) final;
    knowCore::Uri queryLanguage() const final;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::SPARQLExecution
