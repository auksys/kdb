#include "Services.h"

#include <knowDBC/Result.h>

#include "AbstractService.h"

using namespace kDB::Repository;

struct Services::Private
{
  ~Private();
  struct ServiceDefinition
  {
    AbstractService* service;
    int priority;
    std::type_index type_index;
  };
  QList<ServiceDefinition> services;
};

Services::Private::~Private()
{
  for(const ServiceDefinition& def : services)
  {
    delete def.service;
  }
}

Services::Services() : d(new Private) {}

Services::Services(const Services& _rhs) : d(_rhs.d) {}

Services Services::operator=(const Services& _rhs)
{
  d = _rhs.d;
  return *this;
}

Services::~Services() {}

void Services::addService(AbstractService* _service, int _priority, const std::type_index& _type)
{
  Private::ServiceDefinition sd{_service, _priority, _type};
  QList<Private::ServiceDefinition>::iterator it = d->services.begin();
  while(it != d->services.end())
  {
    if(_priority > it->priority)
    {
      d->services.insert(it, sd);
      return;
    }
    ++it;
  }
  d->services.append(sd);
}

knowDBC::Result Services::call(const knowCore::Uri& _service, const QString& _query,
                               const knowCore::ValueHash& _bindings) const
{
  for(const Private::ServiceDefinition& sd : d->services)
  {
    if(sd.service->canCall(_service))
    {
      return sd.service->call(_service, _query, _bindings);
    }
  }
  return knowDBC::Result::create(_query, clog_qt::qformat("No such service '{}'", _service));
}

void Services::removeAllServices(const std::type_index& _type_index)
{
  QList<Private::ServiceDefinition>::iterator it = d->services.begin();
  while(it != d->services.end())
  {
    if(it->type_index == _type_index)
    {
      delete it->service;
      it = d->services.erase(it);
    }
    else
    {
      ++it;
    }
  }
}
