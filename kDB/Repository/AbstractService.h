#pragma once

#include <QVariantList>

#include <kDB/Forward.h>

namespace kDB::Repository
{
  /**
   * \ingroup kDB_Repository
   *
   * Interface a remote service with a SPARQL Query
   */
  class AbstractService
  {
  public:
    virtual ~AbstractService();
    /**
     * \return true if this \ref AbstractService can call the service specified by \ref _service
     */
    virtual bool canCall(const knowCore::Uri& _service) const = 0;
    /**
     * Execute the query \ref _query on the service \ref _service using the \ref _bindings
     * \return the result of the query.
     */
    virtual knowDBC::Result call(const knowCore::Uri& _service, const QString& _query,
                                 const knowCore::ValueHash& _bindings) const
      = 0;
  };
} // namespace kDB::Repository
