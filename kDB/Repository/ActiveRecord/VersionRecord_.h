public:
/**
 * Set the version number to the current one
 */
cres_qresult<void> updateToCurrent();

/**
 * Get the version record (or create one if none exists)
 */
static cres_qresult<VersionRecord> get(const kDB::Repository::QueryConnectionInfo& _connection,
                                       const QString& _name);
