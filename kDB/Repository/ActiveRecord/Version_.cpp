#include <kDB/Repository/ActiveRecord/Version.h>

#include <kDB/Version.h>

using namespace kDB::Repository::ActiveRecord;

bool VersionValue::striclyLessThan(int _major, int _minor, int _release) const
{
  return major() < _major
         or (major() == _major
             and (minor() < _minor or (minor() == _minor and revision() < _release)));
}

bool VersionValue::isNull() const { return major() == 0 and minor() == 0 and revision() == 0; }

cres_qresult<void> VersionRecord::updateToCurrent()
{
  setMajor(KDB_VERSION_MAJOR);
  setMinor(KDB_VERSION_MINOR);
  setRevision(KDB_VERSION_REVISION);
  return record();
}

cres_qresult<VersionRecord>
  VersionRecord::get(const kDB::Repository::QueryConnectionInfo& _connection, const QString& _name)
{
  VersionRecord vr = byName(_connection, _name).first();
  if(not vr.isPersistent())
  {
    return VersionRecord::create(_connection, _name);
  }
  return cres_success(vr);
}
