public:
/**
 * @return true if the current version is strictly inferior to the argument
 */
bool striclyLessThan(int _major, int _minor, int _release) const;
/**
 * @return true if the current version is set to 0.0.0
 */
bool isNull() const;
