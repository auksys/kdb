#ifndef _KDB_REPOSITORY_NOTIFICATIONHANDLER_H_
#define _KDB_REPOSITORY_NOTIFICATIONHANDLER_H_

#include <QObject>

namespace kDB
{
  namespace Repository
  {
    class NotificationsManager;
    class NotificationHandler : public QObject
    {
      Q_OBJECT
      friend class NotificationsManager;
      NotificationHandler();
      ~NotificationHandler();
    public:
    signals:
      void triggered(const QByteArray& _payload);
    };
  } // namespace Repository
} // namespace kDB

#endif
