#pragma once

#include "Services.h"

namespace kDB::Repository
{
  /**
   * @ingroup kDB_Repository
   *
   * It represents the execution environment for SPARQL Query. It contains the base uri, the default
   * RDF Dataset, a list of named datasets, a \ref Services object and a connection (which could be
   * a transaction).
   */
  class RDFEnvironment
  {
  public:
    RDFEnvironment();
    RDFEnvironment(const RDFEnvironment& _rhs);
    RDFEnvironment& operator=(const RDFEnvironment& _rhs);
    RDFEnvironment(const Services& _services);
    RDFEnvironment(const RDFDataset& _dataset, const Services& _services = Services());
    RDFEnvironment(const RDFDataset& _dataset, const QList<RDFDataset>& _named_datasets,
                   const Services& _services = Services());
    RDFEnvironment(const QList<RDFDataset>& _named_datasets,
                   const Services& _services = Services());
    ~RDFEnvironment();
  public:
    knowCore::Uri base() const;
    RDFDataset defaultDataset() const;
    Services services() const;
    QList<RDFDataset> namedDatasets() const;
    QueryConnectionInfo connection() const;
    /**
     * @return true if the environment is valid. (An environment is considered valid if its
     * connection is valid).
     */
    bool isValid() const;
  public:
    RDFEnvironment& setBase(const knowCore::Uri& _base);
    RDFEnvironment& setConnection(const QueryConnectionInfo& _connection);
    RDFEnvironment& setDefaultDataset(const RDFDataset& _dataset);
    RDFEnvironment& addNamedDataset(const RDFDataset& _dataset);
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace kDB::Repository
