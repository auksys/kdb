#include "TemporaryTransaction.h"

#include "QueryConnectionInfo.h"
#include "Transaction.h"

using namespace kDB::Repository;

struct TemporaryTransaction::Private
{
  bool own_transaction = false;
  Transaction transaction;
};

TemporaryTransaction::TemporaryTransaction(const QueryConnectionInfo& _connectionInfo)
    : TemporaryTransaction(_connectionInfo.transaction(), _connectionInfo.connection())
{
}

TemporaryTransaction::TemporaryTransaction(const Transaction& _transaction,
                                           const Connection& _connection)
    : d(new Private)
{
  if(_transaction.isActive())
  {
    d->transaction = _transaction;
  }
  else
  {
    d->transaction = Transaction(_connection);
    d->own_transaction = true;
  }
}

TemporaryTransaction::~TemporaryTransaction() {}

cres_qresult<void> TemporaryTransaction::commitIfOwned()
{
  if(d->own_transaction)
  {
    return d->transaction.commit();
  }
  else
  {
    return cres_success();
  }
}

cres_qresult<void> TemporaryTransaction::rollback() { return d->transaction.rollback(); }

cres_qresult<void> TemporaryTransaction::rollbackIfOwned()
{
  if(d->own_transaction)
  {
    return d->transaction.rollback();
  }
  else
  {
    return cres_success();
  }
}

Transaction TemporaryTransaction::transaction() const { return d->transaction; }
