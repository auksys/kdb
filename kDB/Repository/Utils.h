/**
 * Uniform function to print a revision hash
 */
inline QString kDBppRevHash(const QByteArray& _array)
{
  return QString::fromLatin1(_array.toHex().left(7));
}
