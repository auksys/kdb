#include <knowDBC/Forward.h>

namespace kDB::Repository::Interfaces
{
  /**
   * @ingroup kDB_Repository
   *
   * Interface for extensions that can create queries.
   */
  class QueryFactory
  {
  public:
    virtual ~QueryFactory();
    /**
     * @return true if it can create a query for the given @p _type and @p _environment.
     */
    virtual bool canCreateQuery(const knowCore::Uri& _type,
                                const knowCore::ValueHash& _environment) const
      = 0;
    /**
     * @return a query for the given @p _type and @p _environment.
     */
    virtual cres_qresult<knowDBC::Query> createQuery(const knowCore::Uri& _type,
                                                     const knowCore::ValueHash& _environment) const
      = 0;
  };
} // namespace kDB::Repository::Interfaces
