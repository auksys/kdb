#include <knowCore/Test.h>

#define VERIFY_QUERY_RESULT(_R_)                                                                   \
  if(not _R_)                                                                                      \
  {                                                                                                \
    QFAIL(qPrintable("Query: '" + _R_.query() + "' Error: '" + _R_.error() + "'"));                \
  }

#define VERIFY_QUERY_EXECUTION(_SQLQUERY_)                                                         \
  {                                                                                                \
    auto r = _SQLQUERY_.execute();                                                                 \
    VERIFY_QUERY_RESULT(r);                                                                        \
  }

#define VERIFY_QUERY_EXECUTION_SET(_SQLQUERY_, _QUERY_TEXT_)                                       \
  _SQLQUERY_.setQuery(_QUERY_TEXT_);                                                               \
  VERIFY_QUERY_EXECUTION(_SQLQUERY_)

#define COMPARE_SINGLE_QUERY_RESULT(_SQLQUERY_, _EXPECTED_)                                        \
  {                                                                                                \
    auto r = _SQLQUERY_.execute();                                                                 \
    VERIFY_QUERY_RESULT(r);                                                                        \
    QCOMPARE(r.fields(), 1);                                                                       \
    QCOMPARE(r.tuples(), 1);                                                                       \
    CRES_QCOMPARE(r.value<decltype(_EXPECTED_)>(0, 0), _EXPECTED_);                                \
  }
