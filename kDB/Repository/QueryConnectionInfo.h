#pragma once

#include "Connection.h"

namespace kDB::Repository
{
  class QueryConnectionInfo
  {
    friend class DatabaseInterface::PostgreSQL::SQLCopyData;
    friend class DatabaseInterface::PostgreSQL::SQLQueryExecutor;
  public:
    QueryConnectionInfo(const Connection& _connection = Connection());
    QueryConnectionInfo(const knowCore::WeakReference<Connection>& _connection);
    QueryConnectionInfo(const Transaction& _transaction);
    /**
     * Create the query connection from the transaction if it is active or from the connection
     * otherwise
     */
    QueryConnectionInfo(const Transaction& _transaction, const Connection& _connection);
    QueryConnectionInfo(const QueryConnectionInfo& _rhs);
    QueryConnectionInfo& operator=(const QueryConnectionInfo& _rhs);
    ~QueryConnectionInfo();
  public:
    bool operator==(const kDB::Repository::QueryConnectionInfo& _rhs) const;
    Connection connection() const;
    Transaction transaction() const;
    void setConnection(const Connection& _connection);
    void setTransaction(const Transaction& _transaction);
    void startTransaction();
    void removeTransaction();
    bool isValid() const;
    knowDBC::Query createSQLQuery(const QString& _query = QString(),
                                  const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                  const knowCore::ValueHash& _options
                                  = knowCore::ValueHash()) const;
    knowDBC::Query createSPARQLQuery(const RDFEnvironment& _environment = RDFEnvironment(),
                                     const QString& _query = QString(),
                                     const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                     const knowCore::ValueHash& _options
                                     = knowCore::ValueHash()) const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace kDB::Repository

#include <knowCore/Formatter.h>

clog_format_declare_formatter(kDB::Repository::QueryConnectionInfo)
{
  return format_to(ctx.out(), "connection to ({})", p.connection().serverUuid());
}
