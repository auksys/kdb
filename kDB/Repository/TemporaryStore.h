#pragma once

#include <kDB/Forward.h>

namespace kDB::Repository
{
  /**
   * @ingroup kDB_Repository
   * Use this class to create a temporary store, for instance, used for writing tests.
   */
  class TemporaryStore
  {
  public:
    TemporaryStore();
    ~TemporaryStore();
    cres_qresult<void> start();
    cres_qresult<void> stop(bool _force = false);
    Connection createConnection() const;
    Store* store() const;
    /**
     * Set to auto remove the directory for the store when object is deleted. Set it to false to
     * keep it.
     */
    void setAutoRemove(bool _v);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository
