#include <QSharedPointer>
#include <knowRDF/TripleStreamListener.h>

namespace kDB
{
  namespace Repository
  {
    class TripleStore;
    class TripleStreamInserter : public knowRDF::TripleStreamListener
    {
    public:
      TripleStreamInserter(const TripleStore& _triplesStore);
      virtual ~TripleStreamInserter();
      virtual void triple(const knowRDF::Triple& _triple);
    private:
      struct Private;
      Private* const d;
    };
  } // namespace Repository
} // namespace kDB
