#pragma once

#include "Transaction.h"

#include <kDB/Forward.h>

namespace kDB::Repository
{
  class GraphsManager
  {
    friend class Connection;
    friend class SPARQLQuery;
    friend class DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal;
    GraphsManager(const Connection& _connection);
    ~GraphsManager();
  public:
    /**
     * This function returns the @ref RDFDataset called @p _name.
     */
    cres_qresult<RDFDataset> getDataset(const knowCore::Uri& _name);
    /**
     * @return all datasets
     */
    QList<RDFDataset> datasets();
    QList<RDFDataset> datasets(const QList<knowCore::Uri>& _list);
    // TripleStore
    cres_qresult<TripleStore> createTripleStore(const knowCore::Uri& _name,
                                                const Transaction& _transaction = Transaction());
    cres_qresult<TripleStore> getTripleStore(const knowCore::Uri& _name);
    cres_qresult<TripleStore> getOrCreateTripleStore(const knowCore::Uri& _name,
                                                     const Transaction& _transaction
                                                     = Transaction());
    cres_qresult<void> removeTripleStore(const knowCore::Uri& _name,
                                         const Transaction& _transaction = Transaction());
    bool hasTripleStore(const knowCore::Uri& _name);
    QList<TripleStore> tripleStores() const;
    // TriplesView
    /**
     * Load all the files specified in @p _filters from @p _directory with the @p _format (only
     * support format supported by kDBRDFView)
     */
    cres_qresult<void> loadViewsFrom(const QString& _directory,
                                     const knowCore::ValueHash& _bindings,
                                     const QStringList& _filters = {"*.sml"},
                                     const QString& _format = "SML");
    cres_qresult<TriplesView> createView(const RDFView::ViewDefinition& _definition);
    cres_qresult<void> removeView(const knowCore::Uri& _name);
    QList<RDFView::ViewDefinition> viewDefinitions() const;
    cres_qresult<TriplesView> getTriplesView(const knowCore::Uri& _name);
    bool hasTriplesView(const knowCore::Uri& _name);
    QList<TriplesView> triplesViews() const;
    // Union
    /**
     * @return the union for the given name
     */
    cres_qresult<PersistentDatasetsUnion> getUnion(const knowCore::Uri& _name);
    /**
     * @return create the union for the given name
     */
    cres_qresult<PersistentDatasetsUnion> createUnion(const knowCore::Uri& _name);
    /**
     * @return the union for the given name or create
     */
    cres_qresult<PersistentDatasetsUnion> getOrCreateUnion(const knowCore::Uri& _name);
    cres_qresult<void> clearUnion(const knowCore::Uri& _name);
    bool hasUnion(const knowCore::Uri& _name);
    QList<PersistentDatasetsUnion> unions() const;
    // Generic
    QList<knowCore::Uri> graphs() const;
    EmptyRDFDataset emptyGraph() const;
  private:
    cres_qresult<void> reload();
    cres_qresult<void> lockAll(const Transaction& _transaction);
  private:
    struct Private;
    friend struct Private;
    Private* const d;
  };
} // namespace kDB::Repository
