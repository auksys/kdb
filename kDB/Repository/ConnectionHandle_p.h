#pragma once

#include <libpq-fe.h>

#include "Connection.h"

namespace kDB
{
  namespace Repository
  {
    /**
     * \private
     * Contain a postgresql connection from a pool of connection.
     * Automatically released on destruction or manually by calling \ref release
     */
    class ConnectionHandle
    {
      ConnectionHandle(const ConnectionHandle& _rhs);
      ConnectionHandle& operator=(const ConnectionHandle& _rhs);
    public:
      ConnectionHandle(const Connection& _cp, bool _force = false);
      ~ConnectionHandle();
      PGconn* connection() { return m_conn; }
      void release();
    private:
      Connection m_cp;
      PGconn* m_conn = nullptr;
    };
  } // namespace Repository
} // namespace kDB
