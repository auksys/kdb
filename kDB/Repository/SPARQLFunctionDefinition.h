#include <QList>
#include <QSharedDataPointer>

#include <kDB/Forward.h>

namespace kDB::Repository
{
  class SPARQLFunctionDefinition
  {
  public:
    SPARQLFunctionDefinition();
    SPARQLFunctionDefinition(const knowCore::Uri& _sparql_name, const QString& _sql_name,
                             const knowCore::Uri& _return, const QList<knowCore::Uri>& _arguments);
    SPARQLFunctionDefinition(const SPARQLFunctionDefinition& _rhs);
    SPARQLFunctionDefinition& operator=(const SPARQLFunctionDefinition& _rhs);
    ~SPARQLFunctionDefinition();
    bool isValid() const;
    knowCore::Uri sparqlName() const;
    /**
     * @return a SQL template of the form "function($0, $1..., $n)"
     */
    QString sqlTemplate() const;
    knowCore::Uri returnType() const;
    QList<knowCore::Uri> arguments() const;

    bool operator==(const SPARQLFunctionDefinition& _rhs) const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace kDB::Repository
