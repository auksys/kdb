#include <fstream>
#include <random>

#include <clog_bench>

#include <QCommandLineParser>
#include <QCoreApplication>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/VersionControl/Manager.h>

int main(int _argc, char** _argv)
{
  QCoreApplication app(_argc, _argv);
  QCoreApplication::setApplicationName("BenchmarkTripleStore");
  QCoreApplication::setApplicationVersion("1.0");

  QCommandLineParser parser;
  QCommandLineOption op_ev("enable-versioning", "enable versioning");
  parser.addOption(op_ev);
  QCommandLineOption op_ds("disable-signing", "disable signing");
  parser.addOption(op_ds);
  QCommandLineOption op_oi("only-insertion", "only insertion");
  parser.addOption(op_oi);
  QCommandLineOption op_od("only-deletion", "only deletion");
  parser.addOption(op_od);
  QCommandLineOption op_ctc("constant-triples-count",
                            "use a constant number of triples insertion (1000)");
  parser.addOption(op_ctc);
  parser.addPositionalArgument("output", "output file");

  parser.process(app);

  bool enable_versioning = parser.isSet(op_ev);
  bool disable_signing = parser.isSet(op_ds);
  bool only_insertion = parser.isSet(op_oi);
  bool only_deletion = parser.isSet(op_od);
  bool constant_triples_count = parser.isSet(op_ctc);
  int number_iteratons = 600;
  int insertion_factor = 9;
  int deletion_factor = 3;

  std::default_random_engine generator;

  kDB::Repository::TemporaryStore store;
  store.start();
  kDB::Repository::Connection c = store.createConnection();
  c.connect();

  kDB::Repository::TripleStore ts = c.defaultTripleStore().expect_success();

  knowDBC::Query q
    = c.createSQLQuery("ALTER TABLE " + ts.tablename() + " SET (autovacuum_enabled = false);");
  q.execute();

  if(enable_versioning)
  {
    ts.enableVersioning();
    q.setQuery("ALTER TABLE " + ts.tablename()
               + "_versioning_revisions SET (autovacuum_enabled = false);");
    q.execute();
    q.setQuery("ALTER TABLE " + ts.tablename()
               + "_versioning_deltas SET (autovacuum_enabled = false);");
    q.execute();
    if(disable_signing)
    {
      ts.versionControlManager()->setSigningEnabled(false);
    }
  }

  if(only_deletion)
  {
    QList<knowRDF::Triple> inserted;
    for(int i = 0; i < deletion_factor * number_iteratons * number_iteratons; ++i)
    {
      inserted.append(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                      QUuid::createUuid().toString(),
                                      knowCore::Uri(QUuid::createUuid().toString())));
    }
    ts.insert(inserted);
  }

  clog_bench bench;
  bench.setup_columns("insertions_count", "insertions_time", "deletions_count", "deletions_time");

  for(int i = 1; i <= number_iteratons; ++i)
  {
    int insertions_count = i * insertion_factor;
    int deletions_count = i * deletion_factor;
    if(constant_triples_count)
    {
      insertions_count = 1000;
      deletions_count = 300;
    }

    if(only_insertion)
    {
      deletions_count = 0;
    }
    if(only_deletion)
    {
      insertions_count = 0;
    }

    clog_info("{}: to insert {} to delete {}", i, insertions_count, deletions_count);

    QList<knowRDF::Triple> inserted;
    QList<knowRDF::Triple> deleted;

    QList<knowRDF::Triple> current_triples = ts.triples().expect_success();

    for(int i = 0; i < insertions_count; ++i)
    {
      inserted.append(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                      QUuid::createUuid().toString(),
                                      knowCore::Uri(QUuid::createUuid().toString())));
    }
    for(int j = 0; j < deletions_count and current_triples.size() > 0; ++j)
    {
      deleted.append(current_triples.takeAt(
        std::uniform_int_distribution<int>(0, current_triples.size() - 1)(generator)));
    }

    bench.start_iteration(std::to_string(i));

    ts.insert(inserted);
    std::size_t insertions_time = bench.current_time();
    ts.remove(deleted);
    std::size_t deletions_time = bench.current_time();

    bench.end_iteration(std::to_string(inserted.size()), insertions_time,
                        std::to_string(deleted.size()), deletions_time - insertions_time);
  }
  std::ofstream csv_o;
  csv_o.open(qPrintable(parser.positionalArguments()[0]), std::ios_base::out);
  bench.print_results(csv_o, true);
  return 0;
}
