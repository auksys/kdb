#!/usr/bin/env ruby

require 'csv'

puts("Reading from #{ARGV[0]}")

data = CSV.read(ARGV[0], col_sep: " ", headers: :first_row)

values = []

data.each() do |row|
#   for i in 
  for i in 3...row.length
    if i != 4
      values.push(row[i].to_i)
    end
  end
end

values = values.sort

def mean(array)
  array = array.inject(0) { |sum, x| sum += x } / array.size.to_f
end

def median(array, already_sorted=false)
  return nil if array.empty?
  array = array.sort unless already_sorted
  m_pos = array.size / 2
  return array.size % 2 == 1 ? array[m_pos] : mean(array[m_pos-1..m_pos])
end


def standard_deviation(array)
  m = mean(array)
  variance = array.inject(0) { |variance, x| variance += (x - m) ** 2 }
  standard_deviation = Math.sqrt(variance/(array.size-1))

  # Round floating point to 4 decimals
  format = "{}.4f"
  return format % standard_deviation
end

puts("average = #{mean(values)} ms median = #{median(values, true)} ms standard_deviation = #{standard_deviation(values)} ms ")
