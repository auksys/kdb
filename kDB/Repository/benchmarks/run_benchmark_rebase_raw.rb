#!/usr/bin/env ruby

require 'open3'

puts("changes revisions average_time")

for changes in 10..50 do
  for revisions in 1..10 do
    
    cmd_stdout, cmd_stderr, cmd_status = Open3.capture3("./BenchmarkRebase --raw-performance #{changes} #{revisions} 20")
    Open3.capture3("pkill postgres")
    average_time = 0
    
    cmd_stdout.each_line do |line|
      match = /Average rebase time: (.*)ms/.match(line)
      if match
        average_time = match[1]
      end
    end
    puts("#{changes} #{revisions} #{average_time}")
  end
end
