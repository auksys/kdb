#include <functional>

#include <random>

#include <QCommandLineParser>
#include <QDebug>
#include <QMutex>
#include <QThread>
#include <QTime>
#include <QUuid>
#include <QWaitCondition>

#include <clog_qt>
#include <knowCore/Timestamp.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/Utils.h>

#include <kDB/Repository/VersionControl/Delta.h>
#include <kDB/Repository/VersionControl/Manager.h>
#include <kDB/Repository/VersionControl/Revision.h>
#include <kDB/Repository/VersionControl/RevisionBuilder.h>
#include <kDB/Repository/VersionControl/Signature.h>

#define VERIFY(assrt)                                                                              \
  if(not(assrt))                                                                                   \
  {                                                                                                \
    clog_fatal("assertion failed {}", #assrt);                                                     \
  }

class ThreadLambda : public QThread
{
public:
  ThreadLambda(const std::function<void()>& _run) : m_run(_run) {}
  void run() override { m_run(); }
private:
  std::function<void()> m_run;
};

struct graph_data
{
  kDB::Repository::TripleStore triplesStore;
  QMutex revisions_queue_mutex;
  QList<kDB::Repository::VersionControl::Revision> revisions_queue;
};

struct agent_data
{
  agent_data(int _s) : generator(_s), distribution_0_1(0.0, 1.0) {}
  std::default_random_engine generator;
  std::uniform_real_distribution<qreal> distribution_0_1;

  qreal nextFloat() { return distribution_0_1(generator); }
  int nextBounded(int _max) { return std::uniform_int_distribution<int>(0, _max - 1)(generator); }

  QWaitCondition wakeUpReceiver;
  QMutex wakeUpReceiverMutex;

  QList<graph_data*> graphs;
  ThreadLambda* revision_creation_loop;
  ThreadLambda* revision_receiver_loop;
  kDB::Repository::Connection connection;
};

int main(int _argc, char** _argv)
{
  QCoreApplication app(_argc, _argv);
  QCoreApplication::setApplicationName("BenchmarkRevisionThroughputLimit");
  QCoreApplication::setApplicationVersion("1.0");

  QCommandLineParser parser;
  parser.setApplicationDescription("kDB Benchmark Revision Throughput Limit");
  parser.addHelpOption();
  parser.addPositionalArgument("agents", "number of aggents (required)");
  parser.addPositionalArgument("documents", "number of documents (required)");
  parser.addPositionalArgument("triples", "number of triples per revision (required)");
  parser.addPositionalArgument("revisions", "number of revisions (required)");

  parser.process(app);

  if(parser.positionalArguments().length() < 3 or parser.positionalArguments().length() > 4)
  {
    parser.showHelp(-1);
  }

  const int agents_count = parser.positionalArguments()[0].toInt();
  const int documents_count = parser.positionalArguments()[1].toInt();
  const int triples_count = parser.positionalArguments()[2].toInt();
  const int revisions_count = parser.positionalArguments()[3].toInt();

  QList<kDB::Repository::TemporaryStore> stores(std::max(1, agents_count / 10));

  for(int i = 0; i < stores.size(); ++i)
  {
    stores[i].start().expect_success();
  }

  QList<agent_data*> agents;

  // Setup agents
  for(int agent_index = 0; agent_index < agents_count; ++agent_index)
  {
    agent_data* agent = new agent_data(agent_index);

    agent->connection = stores[agent_index % stores.size()].createConnection();
    agent->connection.connect().expect_success();

    // Create the documents
    for(int document_index = 0; document_index < documents_count; ++document_index)
    {
      graph_data* gd = new graph_data;
      gd->triplesStore
        = agent->connection.graphsManager()
            ->getOrCreateTripleStore(clog_qt::qformat("store_{}_{}", agent_index, document_index))
            .expect_success();
      agent->graphs.append(gd);
      VERIFY(gd->triplesStore.enableVersioning().is_successful());
      gd->triplesStore.versionControlManager()->listenNewRevision(
        [&agents, agent_index,
         document_index](const kDB::Repository::VersionControl::Revision& _revision)
        {
          for(int j = 0; j < agents.size(); ++j)
          {
            if(agent_index != j)
            {
              agent_data* ad = agents[j];
              graph_data* gd = ad->graphs[document_index];
              QMutexLocker l(&gd->revisions_queue_mutex);
              //             gd->revisions_queue.append(_revision);
              QMutexLocker l2(&ad->wakeUpReceiverMutex);
              ad->wakeUpReceiver.wakeAll();
              if(not gd->triplesStore.versionControlManager()
                       ->hasRevision(_revision.hash())
                       .expect_success())
              {
                kDB::Repository::VersionControl::RevisionBuilder rb
                  = gd->triplesStore.versionControlManager()->insertRevision(
                    _revision.contentHash());
                for(const kDB::Repository::VersionControl::Delta& delta : _revision.deltas())
                {
                  rb.addDelta(delta.parent(), delta.hash(), delta.delta(), delta.signatures());
                  qDebug() << "BRTL DS " << delta.delta().size() << " "
                           << _revision.deltas().size();
                }
                {
                  kDB::Repository::Transaction transaction(gd->triplesStore.connection());
                  rb.insert(transaction, true).expect_success();
                  transaction.commit().expect_success();
                }
              }
            }
          }
        });
    }
    agents.append(agent);
  }

  // Agent main loop
  int loop_length = 1000;
  int loop_length_count = 0;
  QMutex wait_condition_mutex;
  QWaitCondition wait_condition;
  bool running = true;
  for(int agent_index = 0; agent_index < agents_count; ++agent_index)
  {
    agent_data* agent = agents[agent_index];

    agent->revision_receiver_loop = new ThreadLambda(
      [&running, agent, documents_count]()
      {
        while(running)
        {
          {
            QMutexLocker l2(&agent->wakeUpReceiverMutex);
            agent->wakeUpReceiver.wait(&agent->wakeUpReceiverMutex);
          }
          for(int document_index = 0; document_index < documents_count; ++document_index)
          {
            graph_data* gd = agent->graphs[document_index];
            QList<kDB::Repository::VersionControl::Revision> revisions_to_insert;
            {
              QMutexLocker l(&gd->revisions_queue_mutex);
              revisions_to_insert = gd->revisions_queue;
              gd->revisions_queue.clear();
            }
            for(const kDB::Repository::VersionControl::Revision& rev : revisions_to_insert)
            {
              if(not gd->triplesStore.versionControlManager()
                       ->hasRevision(rev.hash())
                       .expect_success())
              {
                kDB::Repository::VersionControl::RevisionBuilder rb
                  = gd->triplesStore.versionControlManager()->insertRevision(rev.contentHash());
                for(const kDB::Repository::VersionControl::Delta& delta : rev.deltas())
                {
                  rb.addDelta(delta.parent(), delta.hash(), delta.delta(), delta.signatures());
                  qDebug() << "BRTL DS " << delta.delta().size() << " " << rev.deltas().size();
                }
                {
                  kDB::Repository::Transaction transaction(gd->triplesStore.connection());
                  rb.insert(transaction, true).expect_success();
                  transaction.commit().expect_success();
                }
              }
            }
          }
        }
      });

    agent->revision_creation_loop = new ThreadLambda(
      [&running, agent, agent_index, &loop_length, &loop_length_count, documents_count,
       triples_count, &wait_condition, &wait_condition_mutex, revisions_count]()
      {
        for(int revision = 0; (running and agent_index != 0) or revision < revisions_count;
            ++revision)
        {
          QTime startTime = QTime::currentTime();

          // 1) Handle merge
          if(agent_index == 0)
          {
            for(int document_index = 0; document_index < documents_count; ++document_index)
            {
              graph_data* gd = agent->graphs[document_index];
              // merge master
              QByteArray ownHead
                = gd->triplesStore.versionControlManager()->tipHash().expect_success();
              qDebug() << "BRTL BM " << document_index << " "
                       << gd->triplesStore.versionControlManager()->heads().expect_success().size();
              for(const kDB::Repository::VersionControl::Revision& head : knowCore::dereference(
                    gd->triplesStore.versionControlManager()->heads().expect_success()))
              {
                if(head.hash() != ownHead
                   and gd->triplesStore.versionControlManager()
                         ->canMerge(head.hash())
                         .expect_success())
                {
                  qDebug() << "BRTL MM " << document_index << " "
                           << gd->triplesStore.versionControlManager()
                                ->revisionsPath(head.hash(), ownHead)
                                .expect_success()
                                .size();
                  gd->triplesStore.versionControlManager()->merge(head.hash());
                }
              }
              qDebug() << "BRTL AM " << document_index << " "
                       << gd->triplesStore.versionControlManager()->heads().expect_success().size();
            }
          }

          // 3) Fast-forward
          for(int document_index = 0; document_index < documents_count; ++document_index)
          {
            graph_data* gd = agent->graphs[document_index];

            QByteArray currentHead
              = gd->triplesStore.versionControlManager()->tipHash().expect_success();
            qDebug() << "BRTL: FFB" << agent_index << document_index << kDBppRevHash(currentHead)
                     << gd->triplesStore.versionControlManager()->heads().expect_success().size();
            for(const kDB::Repository::VersionControl::Revision& head : knowCore::dereference(
                  gd->triplesStore.versionControlManager()->heads().expect_success()))
            {
              qDebug() << "BRTL: FF?" << agent_index << document_index << kDBppRevHash(head.hash())
                       << gd->triplesStore.versionControlManager()->canFastForward(head.hash());
              if(head.hash() != currentHead
                 and gd->triplesStore.versionControlManager()
                       ->canFastForward(head.hash())
                       .expect_success())
              {
                qDebug() << "BRTL: FF!" << agent_index << document_index
                         << kDBppRevHash(head.hash());
                gd->triplesStore.versionControlManager()->fastForward(head.hash());
              }
            }
            qDebug() << "BRTL: FFA" << agent_index << document_index
                     << kDBppRevHash(
                          gd->triplesStore.versionControlManager()->tipHash().expect_success())
                     << gd->triplesStore.versionControlManager()->heads().expect_success().size();
          }

          // 4) Create new revisions
          for(int document_index = 0; document_index < documents_count; ++document_index)
          {
            kDB::Repository::Transaction t(agent->connection);
            kDB::Repository::TripleStore ts = agent->graphs[document_index]->triplesStore;

            int number_of_changes = agent->nextBounded(triples_count);
            QList<knowRDF::Triple> triples = ts.triples().expect_success();

            for(int i = 0; i < number_of_changes; ++i)
            {
              double value = agent->nextFloat();
              if(value < 0.3 and triples.size() > 0)
              {
                int r = agent->nextBounded(triples.size());
                ts.remove(triples[r], t);
                triples.removeAt(r);
              }
              else
              {
                ts.insert(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                          QUuid::createUuid().toString(),
                                          knowCore::Uri(QUuid::createUuid().toString())),
                          t);
              }
            }
            t.commit().expect_success();
          }

          // Hand loop length
          QTime endTime = QTime::currentTime();
          int lasted = startTime.msecsTo(endTime);
          if(lasted < loop_length)
          {
            QThread::msleep(loop_length - lasted);
          }
          else
          {
          }
          if(agent_index == 0)
          {
            loop_length = (loop_length_count * loop_length + lasted) / (loop_length_count + 1);
            ++loop_length_count;
            qDebug().nospace() << "BRTL: average_loop_length: " << loop_length
                               << "ms last_loop_length: " << lasted << "ms";
          }
        }
        running = false;
      });
    agent->revision_receiver_loop->start();
  }
  for(agent_data* agent : agents)
  {
    agent->revision_creation_loop->start();
  }

  for(agent_data* agent : agents)
  {
    agent->revision_creation_loop->wait();
    {
      QMutexLocker l2(&agent->wakeUpReceiverMutex);
      agent->wakeUpReceiver.wakeAll();
    }
    agent->revision_receiver_loop->wait();
  }

  return 0;
}
