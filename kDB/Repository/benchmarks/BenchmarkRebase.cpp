#include <iostream>
#include <random>

#include <clog_chrono>

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>
#include <QMutex>
#include <QThread>
#include <QUuid>
#include <QWaitCondition>
#include <knowCore/Timestamp.h>

#include <clog_qt>
#include <knowCore/Locked.h>
#include <knowCore/Uri.h>
#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/Utils.h>

#include <kDB/Repository/VersionControl/Delta.h>
#include <kDB/Repository/VersionControl/Manager.h>
#include <kDB/Repository/VersionControl/Revision.h>
#include <kDB/Repository/VersionControl/RevisionBuilder.h>
#include <kDB/Repository/VersionControl/Signature.h>

#define VERIFY(_id_, assrt)                                                                        \
  {                                                                                                \
    auto r = (assrt);                                                                              \
    if(not r.is_successful())                                                                      \
    {                                                                                              \
      clog_fatal("{} assertion failed {}", _id_, r.get_error());                                   \
    }                                                                                              \
  }

#define VERIFY_AD(_agent_index_, _document_index_, assrt)                                          \
  VERIFY(clog_qt::qformat("{} {}", agent_index, document_index), assrt)

class ThreadLambda : public QThread
{
public:
  ThreadLambda(const std::function<void()>& _run) : m_run(_run) {}
  void run() override { m_run(); }
private:
  std::function<void()> m_run;
};

struct graph_data
{
  kDB::Repository::TripleStore triplesStore;
  QMutex revisions_queue_mutex;
  QList<kDB::Repository::VersionControl::Revision> revisions_queue;
};

struct propagation_data
{
  knowCore::Timestamp creationTime;
  QString id;
  QList<knowCore::Timestamp> availabilityTime;
};

struct all_propagation_data
{
  QMutex mutex;
  QList<propagation_data*> datas;
private:
  friend QtPrivate::QGenericArrayOps<all_propagation_data>;
  all_propagation_data() {}
  all_propagation_data(const all_propagation_data&) {}
};

struct agent_data
{
  agent_data(int _s) : generator(_s), distribution_0_1(0.0, 1.0) {}
  std::default_random_engine generator;
  std::uniform_real_distribution<qreal> distribution_0_1;

  qreal nextFloat() { return distribution_0_1(generator); }
  int nextBounded(int _max) { return std::uniform_int_distribution<int>(0, _max - 1)(generator); }

  QList<graph_data*> graphs;
  ThreadLambda* agent_reception_loop;
  ThreadLambda* agent_creation_loop;
  knowCore::Locked<QList<QByteArray>> lastMergeRevisions;
  kDB::Repository::Connection connection;

  knowCore::Timestamp startTime;

  int number_of_revisions = 0;
};

namespace
{
  bool isPublic(const kDB::Repository::VersionControl::Revision& _revision)
  {
    return not _revision.tags().testFlag(kDB::Repository::VersionControl::Revision::Tag::Editable)
           and not _revision.tags().testFlag(
             kDB::Repository::VersionControl::Revision::Tag::Private);
  }
} // namespace

int main(int _argc, char** _argv)
{
  QCoreApplication app(_argc, _argv);
  QCoreApplication::setApplicationName("BenchmarkRebase");
  QCoreApplication::setApplicationVersion("1.0");

  QCommandLineParser parser;
  parser.setApplicationDescription("kDB Benchmark Revision Throughput Limit");
  parser.addHelpOption();
  QCommandLineOption rawPerformanceOption(
    "raw-performance", QCoreApplication::tr("main", "raw perforance experiment"));
  parser.addOption(rawPerformanceOption);
  QCommandLineOption squashOption("squash", QCoreApplication::tr("main", "squash"));
  parser.addOption(squashOption);
  QCommandLineOption massRebasePerformanceOption(
    "mass-rebase", QCoreApplication::tr("main", "mass rebase perforance experiment"));
  parser.addOption(massRebasePerformanceOption);
  parser.addPositionalArgument("changes", "number of changes per revision (required)");
  parser.addPositionalArgument("revisions", "number of revisions (required)");
  parser.addPositionalArgument(
    "repeat", "number of times the experiment is repeated (required for raw performance)");
  parser.addPositionalArgument("revisions_rate",
                               "number of revisions per seconds (required for mass-rebase)");
  parser.addPositionalArgument("agents", "number of agents (required for mass-rebase)");
  parser.addPositionalArgument("documents", "number of documents (required for mass-rebase)");

  parser.process(app);

  bool squash = parser.isSet(squashOption);

  if(parser.isSet(rawPerformanceOption) and parser.isSet(massRebasePerformanceOption))
  {
    qWarning() << "Can only set one experiment!";
    return -1;
  }

  // BEGIN: raw performance benchmark
  if(parser.isSet(rawPerformanceOption))
  {
    kDB::Repository::TemporaryStore store;
    store.start();
    kDB::Repository::Connection c = store.createConnection();
    c.connect();

    if(parser.positionalArguments().length() < 3 or parser.positionalArguments().length() > 3)
    {
      parser.showHelp(-1);
    }

    const int changes_count = parser.positionalArguments()[0].toInt();
    const int revisions_count = parser.positionalArguments()[1].toInt();
    const int repeat_experiement = parser.positionalArguments()[2].toInt();

    double total_time = 0.0;

    for(int _exp_ = 0; _exp_ < repeat_experiement + 1; ++_exp_)
    {
      kDB::Repository::TripleStore ts
        = c.graphsManager()->getOrCreateTripleStore("ts"_kCu).expect_success();
      ts.enableVersioning().expect_success();
      ts.versionControlManager()->setDefaultRevisionTag(
        kDB::Repository::VersionControl::Revision::Tag::Editable);
      ts.versionControlManager()->setDefaultRevisionTag(
        kDB::Repository::VersionControl::Revision::Tag::Private);

      kDB::Repository::VersionControl::Revision originRev
        = ts.versionControlManager()->tip().expect_success();
      {
        kDB::Repository::Transaction t(c);
        ts.insert(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                  QUuid::createUuid().toString(),
                                  knowCore::Uri(QUuid::createUuid().toString())),
                  t);
        t.commit();
      }
      kDB::Repository::VersionControl::Revision targetRev
        = ts.versionControlManager()->tip().expect_success();

      ts.versionControlManager()->checkout(originRev.hash());

      std::default_random_engine generator;
      std::uniform_real_distribution<qreal> distribution_0_1;

      // Create the revisions
      for(int i = 0; i < revisions_count; ++i)
      {
        kDB::Repository::Transaction t(c);
        QList<knowRDF::Triple> remove_triples, insert_triples;

        for(int j = 0; j < changes_count; ++j)
        {
          double value = distribution_0_1(generator);
          QList<knowRDF::Triple> triples = ts.triples().expect_success();
          if(value < 0.3 and triples.size() > 0)
          {
            int r = std::uniform_int_distribution<int>(0, triples.size() - 1)(generator);
            remove_triples.append(triples.takeAt(r));
          }
          else
          {
            insert_triples.append(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                                  QUuid::createUuid().toString(),
                                                  knowCore::Uri(QUuid::createUuid().toString())));
          }
        }
        ts.remove(remove_triples, t);
        ts.insert(insert_triples, t);
        t.commit();
      }

      auto start_timestamp = std::chrono::high_resolution_clock::now();
      ts.versionControlManager()->rebase(targetRev.hash(), squash);
      auto stop_timestamp = std::chrono::high_resolution_clock::now();
      auto duration
        = std::chrono::duration_cast<std::chrono::milliseconds>(stop_timestamp - start_timestamp);
      if(_exp_ > 0)
      {
        std::cout << "Rebase time: " << duration.count() << "ms" << std::endl;
        total_time += duration.count();
      }
      c.graphsManager()->removeTripleStore(ts.uri());
    }

    std::cout << "Average rebase time: " << (total_time / repeat_experiement) << "ms" << std::endl;

    return 0;
    // END: raw performance benchmark
    // BEGIN: mass performance benchmark
  }
  else if(parser.isSet(massRebasePerformanceOption))
  {

    if(parser.positionalArguments().length() < 5 or parser.positionalArguments().length() > 5)
    {
      qWarning() << "Invalid number of positionalArguments, got: " << parser.positionalArguments();
      parser.showHelp(-1);
    }

    const int changes_count = parser.positionalArguments()[0].toInt();
    const int revisions_count = parser.positionalArguments()[1].toInt();
    const double revisions_rate = parser.positionalArguments()[2].toDouble();
    const int agents_count = parser.positionalArguments()[3].toInt();
    const int documents_count = parser.positionalArguments()[4].toInt();

    QList<kDB::Repository::TemporaryStore> stores(std::max(1, agents_count / 10));

    for(int i = 0; i < stores.size(); ++i)
    {
      VERIFY(i, stores[i].start());
    }

    QList<agent_data*> agents;
    QAtomicInt finished_agents;

    // Setup agents
    for(int agent_index = 0; agent_index < agents_count; ++agent_index)
    {
      agent_data* agent = new agent_data(agent_index);

      agent->connection = stores[agent_index % stores.size()].createConnection();
      VERIFY(agent_index, agent->connection.connect());

      // Create the documents
      for(int document_index = 0; document_index < documents_count; ++document_index)
      {
        graph_data* gd = new graph_data;
        gd->triplesStore
          = agent->connection.graphsManager()
              ->getOrCreateTripleStore(clog_qt::qformat("store_{}_{}", agent_index, document_index))
              .expect_success();
        agent->graphs.append(gd);
        VERIFY_AD(agent_index, document_index, gd->triplesStore.enableVersioning());
        // Setup versioning
        if(agent_index != 0)
        {
          gd->triplesStore.versionControlManager()->setDefaultRevisionTag(
            kDB::Repository::VersionControl::Revision::Tag::Editable);
          gd->triplesStore.versionControlManager()->setDefaultRevisionTag(
            kDB::Repository::VersionControl::Revision::Tag::Private);
        }

        // Handle transfer of revisions
        std::function<void(const kDB::Repository::VersionControl::Revision&)> revision_handler
          = [&agents, agent_index,
             document_index](const kDB::Repository::VersionControl::Revision& _revision)
        {
          if(isPublic(_revision))
          {
            for(int j = 0; j < agents.size(); ++j)
            {
              if(agent_index != j)
              {
                agent_data* ad = agents[j];
                graph_data* gd = ad->graphs[document_index];
                QMutexLocker l(&gd->revisions_queue_mutex);
                gd->revisions_queue.append(_revision);
              }
            }
          }
        };
        gd->triplesStore.versionControlManager()->listenNewRevision(revision_handler);
        gd->triplesStore.versionControlManager()->listenRevisionTagsChanged(revision_handler);
      }
      agents.append(agent);
    }

    // Agent main loop
    const int loop_length = 1000 / revisions_rate; // expressed in ms
    QMutex wait_condition_mutex;
    QWaitCondition wait_condition;
    bool running = true;
    QList<all_propagation_data> all_pd(documents_count);

    for(int agent_index = 0; agent_index < agents_count; ++agent_index)
    {
      agent_data* agent = agents[agent_index];

      agent->lastMergeRevisions.unlocked()->resize(documents_count);

      agent->agent_reception_loop = new ThreadLambda(
        [&running, documents_count, agent_index, agent]()
        {
          while(running)
          {
            knowCore::Timestamp startTime = knowCore::Timestamp::now();
            clog_chrono_msg(agent_reception_loop, "agent {}", agent_index);
            // BEGIN Receive revisions and insert them
            QList<QByteArray> lastMergeRevisions(documents_count);
            kDB::Repository::Transaction transaction(agent->connection);
            bool has_inserted = false;
            for(int document_index = 0; document_index < documents_count; ++document_index)
            {
              clog_chrono_msg(receive_revisions, "agent {} {}", agent_index, document_index);
              graph_data* gd = agent->graphs[document_index];
              QList<kDB::Repository::VersionControl::Revision> revisions_to_insert;
              {
                QMutexLocker l(&gd->revisions_queue_mutex);
                revisions_to_insert = gd->revisions_queue;
                gd->revisions_queue.clear();
              }
              clog_chrono_report(receive_revisions, "after lock");
              for(const kDB::Repository::VersionControl::Revision& rev : revisions_to_insert)
              {
                if(not gd->triplesStore.versionControlManager()
                         ->hasRevision(rev.hash(), transaction)
                         .expect_success())
                {
                  clog_info("agent {} {} try to insert {}", agent_index, document_index,
                            kDBppRevHash(rev.hash()));
                  kDB::Repository::VersionControl::RevisionBuilder rb
                    = gd->triplesStore.versionControlManager()->insertRevision(rev.contentHash());
                  bool can_insert = true;
                  for(const kDB::Repository::VersionControl::Delta& delta : rev.deltas())
                  {
                    if(not gd->triplesStore.versionControlManager()
                             ->hasRevision(delta.parent(), transaction)
                             .expect_success())
                    {
                      clog_info("agent {} {} missing {} for {}", agent_index, document_index,
                                kDBppRevHash(delta.parent()), kDBppRevHash(delta.child()));
                      can_insert = false;
                      break;
                    }
                    rb.addDelta(delta.parent(), delta.hash(), delta.delta(), delta.signatures());
                  }
                  if(can_insert)
                  {
                    has_inserted = true;
                    clog_chrono_report(receive_revisions, "before inserting revision");
                    VERIFY_AD(agent_index, document_index, rb.insert(transaction, true));
                    clog_chrono_report(receive_revisions, "after inserting revision");
                    if(rev.deltas().size() > 1)
                    {
                      lastMergeRevisions[document_index] = rev.hash();
                    }
                  }
                  else
                  {
                    QMutexLocker l(&gd->revisions_queue_mutex);
                    gd->revisions_queue.append(rev);
                  }
                }
                clog_chrono_report(receive_revisions, "after inserting a single revisions");
              }
            }
            if(has_inserted)
            {
              clog_chrono_report(agent_reception_loop, "before commit");
              VERIFY(agent_index, transaction.commit());
              // Set the last merge revisions information in the agent information
              auto agent_lastMergeRevisions = agent->lastMergeRevisions.unlocked();
              for(int document_index = 0; document_index < documents_count; ++document_index)
              {
                const QByteArray& hash = lastMergeRevisions[document_index];
                if(not hash.isEmpty())
                {
                  (*agent_lastMergeRevisions)[document_index] = hash;
                }
              }
            }
            else
            {
              transaction.rollback();
            }
            // END Receive revisions and insert them
            clog_chrono_report(agent_reception_loop, "finished receiving revisions");
          }
        });

      agent->agent_creation_loop = new ThreadLambda(
        [&running, agent, agent_index, documents_count, loop_length, squash, revisions_count,
         changes_count, agents_count, &all_pd, &finished_agents]()
        {
          agent->startTime = knowCore::Timestamp::now();
          while(running)
          {
            knowCore::Timestamp startTime = knowCore::Timestamp::now();
            clog_chrono_msg(agent_creation_loop, "agent {}", agent_index);
            // BEGIN Merge
            if(agent_index == 0)
            {
              QList<int> merge_count(documents_count, 0);
              while(true)
              {
                bool has_merged_something = false;

                for(int document_index = 0; document_index < documents_count; ++document_index)
                {
                  graph_data* gd = agent->graphs[document_index];
                  clog_info(
                    "{} has {} heads", document_index,
                    gd->triplesStore.versionControlManager()->heads().expect_success().size());
                  // merge master
                  QByteArray ownHead
                    = gd->triplesStore.versionControlManager()->tipHash().expect_success();
                  for(const kDB::Repository::VersionControl::Revision& head : knowCore::dereference(
                        gd->triplesStore.versionControlManager()->heads().expect_success()))
                  {
                    if(head.hash() != ownHead
                       and gd->triplesStore.versionControlManager()
                             ->canMerge(head.hash())
                             .expect_success())
                    {
                      clog_chrono(merge);
                      gd->triplesStore.versionControlManager()->merge(head.hash());
                      has_merged_something = true;
                      VERIFY_AD(agent_index, document_index,
                                gd->triplesStore.versionControlManager()->publish(head));
                      ++merge_count[document_index];
                      break; // don't merge everything for the document at once, otherwise we might
                             // do just merging here
                    }
                  }
                }
                if(not has_merged_something)
                {
                  break;
                }
                if(startTime.intervalTo<knowCore::MilliSeconds>(knowCore::Timestamp::now()).count()
                   < 2 * loop_length)
                {
                  // make sure the merge master has time to create its own revisions
                  clog_info("merge master ran out of time to merge!");
                  break;
                }
              }
              for(int document_index = 0; document_index < documents_count; ++document_index)
              {
                graph_data* gd = agent->graphs[document_index];
                clog_info("{} has {} heads left after {} merges", document_index,
                          gd->triplesStore.versionControlManager()->heads().expect_success().size(),
                          merge_count[document_index]);
              }
            }
            // END Merge
            else
            // BEGIN Rebase
            {
              for(int document_index = 0; document_index < documents_count; ++document_index)
              {
                clog_chrono_msg(rebase, "Rebase for agent {} {}", agent_index, document_index);
                graph_data* gd = agent->graphs[document_index];

                QByteArray newMergeRevisionHash
                  = agent->lastMergeRevisions.unlocked()->at(document_index);

                if(not newMergeRevisionHash.isEmpty())
                {
                  QByteArray currentHead
                    = gd->triplesStore.versionControlManager()->tipHash().expect_success();
                  if(gd->triplesStore.versionControlManager()
                       ->canRebaseTo(newMergeRevisionHash)
                       .expect_success())
                  {
                    clog_info("Agent {} {} Attempt to rebase {} on top of {}", agent_index,
                              document_index, kDBppRevHash(currentHead),
                              kDBppRevHash(newMergeRevisionHash));
                    kDB::Repository::Transaction transaction(agent->connection);
                    if(gd->triplesStore.versionControlManager()
                         ->checkout(newMergeRevisionHash, transaction)
                         .is_successful())
                    {
                      clog_chrono_report(rebase, "after checkout");
                      VERIFY_AD(agent_index, document_index,
                                cres_cond<void>(
                                  gd->triplesStore.versionControlManager()->tipHash(transaction)
                                    == newMergeRevisionHash,
                                  "wrong tip hash after checkout"));
                      kDB::Repository::VersionControl::Revision rebase_revision
                        = gd->triplesStore.versionControlManager()
                            ->rebase(currentHead, squash, transaction)
                            .expect_success();
                      clog_chrono_report(rebase, "after rebase");
                      if(rebase_revision.isValid())
                      {
                        VERIFY_AD(agent_index, document_index,
                                  gd->triplesStore.versionControlManager()->publish(
                                    gd->triplesStore.versionControlManager()
                                      ->tipHash(transaction)
                                      .expect_success(),
                                    transaction));
                        VERIFY_AD(agent_index, document_index, transaction.commit());
                      }
                      else
                      {
                        clog_warning(
                          "Agent {} {} fails to rebase {} from {}, we have information for {} "
                          "heads",
                          agent_index, document_index, newMergeRevisionHash, currentHead,
                          gd->triplesStore.versionControlManager()
                            ->heads()
                            .expect_success()
                            .size());
                        transaction.rollback();
                      }
                    }
                    else
                    {
                      clog_warning(
                        "Agent {} {} Fails to checkout {} from {}, we have information for {} "
                        "heads",
                        agent_index, document_index, newMergeRevisionHash, currentHead,
                        gd->triplesStore.versionControlManager()->heads().expect_success().size());
                      transaction.rollback();
                    }
                  }
                  else
                  {
                    clog_info("Agent {} {} Cannot rebase {} on top of {}", agent_index,
                              document_index, kDBppRevHash(currentHead),
                              kDBppRevHash(newMergeRevisionHash));
                  }
                  {
                    auto lastMergeRevisions = agent->lastMergeRevisions.unlocked();
                    if(lastMergeRevisions->at(document_index) == newMergeRevisionHash)
                    {
                      (*lastMergeRevisions)[document_index] = QByteArray();
                    }
                  }
                }
              }
            }
            // END Rebase
            clog_chrono_report(agent_creation_loop, "finished merge or rebase");
            // BEGIN Create new revisions
            if(agent->number_of_revisions < revisions_count)
            {

              //             int count_revision = std::max<int>(1,
              //             agent->startTime.intervalTo<knowCore::MilliSeconds>(knowCore::Timestamp::now())
              //             / loop_length - agent->number_of_revisions); clog_info("agent {}
              //             count_revision = {} number_of_revisions = {} rev/loop = {}",
              //             agent_index, count_revision, agent->number_of_revisions,
              for(int i = 0; i < 1; ++i)
              {

                for(int document_index = 0; document_index < documents_count; ++document_index)
                {
                  clog_chrono_msg(create_revision, "agent {} {}", agent_index, document_index);
                  kDB::Repository::Transaction t(agent->connection);
                  kDB::Repository::TripleStore ts = agent->graphs[document_index]->triplesStore;

                  int number_of_changes = agent->nextBounded(changes_count);

                  propagation_data* pd = nullptr;

                  if(agent_index == 1)
                  {
                    --number_of_changes;
                    pd = new propagation_data;
                    pd->id = QUuid::createUuid().toString();
                    pd->availabilityTime.resize(agents_count);
                    ts.insert(
                      knowRDF::Triple("immutable"_kCu, "has_hash"_kCu, knowCore::Uri(pd->id)), t);
                    pd->creationTime = knowCore::Timestamp::now();
                    QMutexLocker l(&all_pd[document_index].mutex);
                    all_pd[document_index].datas.append(pd);
                  }
                  int triplesCount = ts.triplesCount();
                  for(int i = 0; i < number_of_changes; ++i)
                  {
                    double value = agent->nextFloat();
                    if(value < 0.3 and triplesCount > 0)
                    {
                      int r = agent->nextBounded(triplesCount);
                      knowRDF::Triple triple = ts.triple(r);
                      if(triple.subject().uri()
                         != "immutable"_kCu) // Cannot remove "immutable" we need them to easily
                                             // track revisions
                      {
                        ts.remove(triple, t);
                        --triplesCount;
                      }
                    }
                    else
                    {
                      ts.insert(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                                QUuid::createUuid().toString(),
                                                knowCore::Uri(QUuid::createUuid().toString())),
                                t);
                    }
                  }
                  clog_chrono_report(create_revision, "before commit");
                  VERIFY_AD(agent_index, document_index, t.commit());
                  clog_chrono_report(create_revision, "after commit");
                  clog_chrono_report(agent_creation_loop, "after creating revision");
                  if(agent_index == 1)
                  {
                    VERIFY_AD(agent_index, document_index,
                              ts.hasTriple(knowRDF::Triple("immutable"_kCu, "has_hash"_kCu,
                                                           knowCore::Uri(pd->id))));
                  }
                }
                ++agent->number_of_revisions;
                if(agent->number_of_revisions == revisions_count)
                {
                  break;
                }
              }
              clog_info("agent {} has made {} revisions", agent_index, agent->number_of_revisions);
            }
            else if(agent_index == 0)
            {
              bool not_finished = false;
              for(int document_index = 0; document_index < documents_count; ++document_index)
              {
                graph_data* gd = agent->graphs[document_index];
                if(gd->triplesStore.versionControlManager()->heads().expect_success().size() > 1)
                {
                  not_finished = true;
                  break;
                }
              }
              if(not not_finished)
              {
                clog_info("agent 0 is done");
                if(finished_agents == agents_count)
                {
                  running = false;
                }
                else
                {
                  clog_info("agent 0 is waiting for {} agents to finish",
                            agents_count - finished_agents);
                }
              }
            }
            else
            {
              clog_info("agent {} is done", agent_index);
            }
            if(agent->number_of_revisions == revisions_count)
            {
              ++finished_agents;
              ++agent->number_of_revisions;
              for(int document_index = 0; document_index < documents_count; ++document_index)
              {
                graph_data* gd = agent->graphs[document_index];
                VERIFY_AD(agent_index, document_index,
                          gd->triplesStore.versionControlManager()->publish(
                            gd->triplesStore.versionControlManager()->tipHash().expect_success()));
              }
            }
            // END Create new revisions
            clog_chrono_report(agent_creation_loop, "finished revisions creation");
            // BEGIN Compute statistics
            {
              for(int document_index = 0; document_index < documents_count; ++document_index)
              {
                QList<propagation_data*> document_pds;
                {
                  QMutexLocker l(&all_pd[document_index].mutex);
                  document_pds = all_pd[document_index].datas;
                }
                kDB::Repository::TripleStore ts = agent->graphs[document_index]->triplesStore;
                for(propagation_data* pd : document_pds)
                {
                  if(not pd->availabilityTime[agent_index].isValid()
                     and ts.hasTriple(knowRDF::Triple("immutable"_kCu, "has_hash"_kCu,
                                                      knowCore::Uri(pd->id)))
                           .expect_success())
                  {
                    pd->availabilityTime[agent_index] = knowCore::Timestamp::now();
                  }
                }
              }
            }
            // END Compute statistics
            clog_chrono_report(agent_creation_loop, "finished compute statistics");
            // BEGIN throttle
            knowCore::Timestamp endTime = knowCore::Timestamp::now();
            int lasted = startTime.intervalTo<knowCore::MilliSeconds>(endTime)
                           .count()
                           .toInt64(true)
                           .expect_success();
            if(lasted < loop_length)
            {
              QThread::msleep(loop_length - lasted);
            }
            // END throttle
          }
        });
    }
    for(agent_data* agent : agents)
    {
      agent->agent_reception_loop->start();
      agent->agent_creation_loop->start();
      if(agent != agents[0])
      {
        agent->agent_reception_loop->setPriority(QThread::LowestPriority);
        agent->agent_creation_loop->setPriority(QThread::LowestPriority);
      }
      else
      {
        agent->agent_reception_loop->setPriority(QThread::HighestPriority);
        agent->agent_creation_loop->setPriority(QThread::HighestPriority);
      }
    }
    for(agent_data* agent : agents)
    {
      agent->agent_reception_loop->wait();
      agent->agent_creation_loop->wait();
      clog_info("main loop exited");
    }
    // BEGIN display statistics
    std::cout << "BENCHMARK_RESULT document revision";
    for(int i = 0; i < agents_count; ++i)
    {
      std::cout << " agent_" << i;
    }
    std::cout << std::endl;
    for(int document_index = 0; document_index < documents_count; ++document_index)
    {
      for(int revision_index = 0; revision_index < all_pd[document_index].datas.size();
          ++revision_index)
      {
        std::cout << "BENCHMARK_RESULT " << document_index << " " << revision_index;
        propagation_data* pd = all_pd[document_index].datas[revision_index];

        for(int i = 0; i < agents_count; ++i)
        {
          std::cout << " "
                    << pd->creationTime.intervalTo<knowCore::MilliSeconds>(pd->availabilityTime[i])
                         .count()
                         .toString()
                         .toStdString();
        }
        std::cout << std::endl;
      }
    }
    // END display statistics
    // END: mass performance benchmark
  }
  else
  {
    qWarning() << "No experiment set!";
    return -1;
  }

  return 0;
}
