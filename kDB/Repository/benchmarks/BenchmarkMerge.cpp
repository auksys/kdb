#include <fstream>
#include <random>

#include <clog_bench>

#include <QCommandLineParser>
#include <QCoreApplication>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/VersionControl/Manager.h>

int main(int _argc, char** _argv)
{
  QCoreApplication app(_argc, _argv);
  QCoreApplication::setApplicationName("BenchmarkTripleStore");
  QCoreApplication::setApplicationVersion("1.0");

  QCommandLineParser parser;
  parser.addPositionalArgument("output", "output file");

  parser.process(app);

  int number_iteratons = 600;
  int insertions_count = 1000;

  kDB::Repository::TemporaryStore store;
  store.start();
  kDB::Repository::Connection c = store.createConnection();
  c.connect();

  kDB::Repository::TripleStore ts = c.defaultTripleStore().expect_success();

  knowDBC::Query q
    = c.createSQLQuery("ALTER TABLE " + ts.tablename() + " SET (autovacuum_enabled = false);");
  q.execute();

  ts.enableVersioning();
  q.setQuery("ALTER TABLE " + ts.tablename()
             + "_versioning_revisions SET (autovacuum_enabled = false);");
  q.execute();
  q.setQuery("ALTER TABLE " + ts.tablename()
             + "_versioning_deltas SET (autovacuum_enabled = false);");
  q.execute();

  clog_bench bench;
  bench.setup_columns("triples_count", "insertions_time_1", "insertions_time_2", "merge_time");

  for(int i = 1; i <= number_iteratons; ++i)
  {
    clog_info("{}: to insert {}", i, insertions_count);

    QList<knowRDF::Triple> inserted_1, inserted_2;

    for(int i = 0; i < insertions_count; ++i)
    {
      inserted_1.append(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                        QUuid::createUuid().toString(),
                                        knowCore::Uri(QUuid::createUuid().toString())));
      inserted_2.append(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                        QUuid::createUuid().toString(),
                                        knowCore::Uri(QUuid::createUuid().toString())));
    }

    QByteArray head_hash = ts.versionControlManager()->tipHash().expect_success();

    bench.start_iteration(std::to_string(i));
    ts.insert(inserted_1);
    std::size_t insertions_time_1 = bench.current_time();
    QByteArray head_hash_1 = ts.versionControlManager()->tipHash().expect_success();
    ts.versionControlManager()->checkout(head_hash);
    std::size_t insertions_time_2_before = bench.current_time();
    ts.insert(inserted_2);
    std::size_t insertions_time_2 = bench.current_time();
    ts.versionControlManager()->merge(head_hash_1);
    std::size_t merge_time = bench.current_time();

    bench.end_iteration(std::to_string(ts.triplesCount()), insertions_time_1,
                        insertions_time_2 - insertions_time_2_before,
                        merge_time - insertions_time_2);
  }
  std::ofstream csv_o;
  csv_o.open(qPrintable(parser.positionalArguments()[0]), std::ios_base::out);
  bench.print_results(csv_o, true);
  return 0;
}
