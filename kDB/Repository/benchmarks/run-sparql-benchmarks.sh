#!/bin/bash

if [ -d kdb-sparql-benchmarks ] ; then
  cd kdb-sparql-benchmarks
  git pull
  cd ..
else
  git clone git@gitlab.ida.liu.se:lrs/kdb-sparql-benchmarks.git
fi

cd kdb-sparql-benchmarks
make
cd ..

./kdb_sparql_benchmarks load kdb-sparql-benchmarks/
