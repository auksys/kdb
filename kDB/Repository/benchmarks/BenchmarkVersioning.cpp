#include "BenchmarkVersioning.h"

#include <clog_chrono>

#include <Cyqlops/Crypto/Random.h>

#include <clog_qt>
#include <knowCore/Test.h>
#include <knowCore/Uri.h>

#include <knowRDF/Literal.h>
#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include "../Connection.h"
#include "../GraphsManager.h"
#include "../TemporaryStore.h"
#include "../Transaction.h"
#include "../TripleStore.h"

#include "../VersionControl/Delta.h"
#include "../VersionControl/DeltaBuilder.h"
#include "../VersionControl/Manager.h"
#include "../VersionControl/RevisionBuilder.h"

using namespace kDB;
namespace RVC = Repository::VersionControl;

BenchmarkVersioning::BenchmarkVersioning() { qputenv("QTEST_FUNCTION_TIMEOUT", "10000000"); }

// run with ./BenchmarkVersioning -iterations 10

void BenchmarkVersioning::benchmarkMerging()
{
  QBENCHMARK
  {
    const int revisions_count = 30;
    const int triples_count = 1000;
    clog_disable(clog_level_all);
    Repository::TemporaryStore store;
    CRES_QVERIFY(store.start());
    Repository::Connection c = store.createConnection();
    CRES_QVERIFY(c.connect());

    Repository::TripleStore triplesStore_a
      = CRES_QVERIFY(c.graphsManager()->getOrCreateTripleStore("a"_kCu));
    QVERIFY(triplesStore_a.enableVersioning().is_successful());

    QList<RVC::Revision> merge_list;

    QByteArray head_hash = CRES_QVERIFY(triplesStore_a.versionControlManager()->tipHash());
    QVERIFY(head_hash.size() != 0);

    for(int i = 0; i < revisions_count; ++i)
    {
      QCOMPARE(head_hash, triplesStore_a.versionControlManager()->tipHash());

      // Create reviesion
      RVC::RevisionBuilder rb
        = triplesStore_a.versionControlManager()->insertRevision(Cyqlops::Crypto::Random::generate(
          20)); // Use a random hash, not ideal, but not too relevant for this benchmark
      RVC::DeltaBuilder db;
      {
        int ts = triples_count; // qrand() % 10 + 1;
        for(int i = 0; i < ts; ++i)
        {
          db.reportInsertion(knowRDF::Triple(knowCore::Uri(QUuid::createUuid().toString()),
                                             knowCore::Uri(QUuid::createUuid().toString()),
                                             knowCore::Uri(QUuid::createUuid().toString())));
        }
      }

      rb.addDelta(head_hash, db.delta(), true);

      // Insert revision
      Repository::Transaction transaction(triplesStore_a.connection());
      merge_list.append(CRES_QVERIFY(rb.insert(transaction, true)));
      CRES_QVERIFY(transaction.commit());
    }
    RVC::Revision head = merge_list.takeFirst();
    CRES_QVERIFY(triplesStore_a.versionControlManager()->fastForward(head.hash()));
    clog_chrono(ct);
    {
      for(int i = 0; i < merge_list.size(); ++i)
      {
        clog_chrono(ci);
        CRES_QVERIFY(triplesStore_a.versionControlManager()->merge(merge_list[i].hash()));
        qDebug() << "i = " << i << " total time = " << clog_chrono_current_ms(ct)
                 << "current time = " << clog_chrono_current_ms(ci);
      }
    }
  }
}

QTEST_MAIN(BenchmarkVersioning)
