#!/usr/bin/env ruby

puts("read different variant of the triplestore")

options = ["", "--only-insertion", "--only-deletion"]
options_name = ["in_del", "only_in", "only_del"]
variants = ["", "--enable-versioning", "--disable-signing" ]
variants_name = ["no_versioning", "versioning", "versioning_no_signing"]

for i in 0...3
  
  for j in 0...3

    for k in 0...10
      
      `./BenchmarkTripleStore #{options[i]} #{variants[j]} bts_#{options_name[i]}_#{variants_name[j]}_#{k}.csv`
      
    end
    
  end
  
end

