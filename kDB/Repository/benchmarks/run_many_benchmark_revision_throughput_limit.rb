#!/usr/bin/env ruby

require 'open3'

puts("start agents documents triples average_time")

for agents in 2..20 do
  for documents in 1..20 do
    for triples_base in 1..5 do
      
      triples = triples_base * 10
      
      print(Time.new().strftime("%d:%k:%M:%S"))
      
      cmd_stdout, cmd_stderr, cmd_status = Open3.capture3("./BenchmarkRevisionThroughputLimit #{agents} #{documents} #{triples} 30")
      Open3.capture3("pkill postgres")
      Open3.capture3("rm -rf /tmp/BenchmarkRevision*")
      average_time = 0
      
      cmd_stderr.each_line do |line|
        match = /BRTL: average_loop_length: (.*)ms last_loop_length: .*ms/.match(line)
        if match
          average_time = match[1]
        end
      end
      puts(" #{agents} #{documents} #{triples} #{average_time}")
    end
  end
end
