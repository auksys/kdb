#pragma once

#include <optional>

#include <QExplicitlySharedDataPointer>

#include <kDB/Forward.h>

namespace kDB::Repository::RDF
{
  struct FocusNodeDeclarationsRegistryPrivate;
  /**
   * @ingroup kDB::Repository
   * Represent the declaration of a type of rdf object
   */
  class FocusNodeDeclaration
  {
    friend struct FocusNodeDeclarationsRegistryPrivate;
    struct Private;
  public:
    class Property
    {
      friend struct Private;
      friend struct FocusNodeDeclarationsRegistryPrivate;
    public:
      enum class Variety
      {
        Optional, /// min: 0 max: 1
        Atomic,   /// min: 1 max 1 (default)
        List,     /// min: 0 max > 2
        Map       /// min: 0 max > 2
      };
      enum class Direction
      {
        Direct,
        Reverse
      };
    public:
      Property();
      Property(const Property& _rhs);
      Property& operator=(const Property& _rhs);
      ~Property();
      bool operator==(const Property& _rhs);
      bool operator!=(const Property& _rhs);
      /**
       * @return true if the field is a constant. A constant field can only be set once, it cannot
       * be modified.
       */
      bool isConstant() const;
      /**
       * @return true if the field is optional
       */
      Variety variety() const;
      /**
       * @return the direction of the property.
       */
      Direction direction() const;
      std::optional<std::size_t> minimum() const;
      std::optional<std::size_t> maximum() const;
      QString name() const;
      knowCore::Uri path() const;
      knowCore::Uri datatype() const;
      knowRDF::Literal value() const;
      knowRDF::Literal defaultValue() const;
      /**
       * list of accepted units, if datatype == askcore_datatype::quantityDecimal
       */
      std::optional<QList<knowCore::Uri>> acceptedUnits() const;
      /**
       * List of accepted values
       */
      std::optional<knowCore::ValueList> acceptedValues() const;
    private:
      struct Private;
      QExplicitlySharedDataPointer<Private> d;
    };
  public:
    FocusNodeDeclaration();
    FocusNodeDeclaration(const FocusNodeDeclaration& _rhs);
    FocusNodeDeclaration& operator=(const FocusNodeDeclaration& _rhs);
    ~FocusNodeDeclaration();
    /**
     * @return true if it inherits @ref _uri.
     */
    bool inherits(const knowCore::Uri& _uri);
    /**
     * @return the list of types this declaration inherits
     */
    QList<knowCore::Uri> inheritedTypes() const;
    /**
     * @return the uri of the type of this object.
     */
    knowCore::Uri type() const;
    /**
     * @return true if it has field @ref _uri.
     */
    bool hasField(const knowCore::Uri& _uri);
    /**
     * @return the field for the given uri
     */
    cres_qresult<Property> field(const knowCore::Uri& _uri);
    /**
     * @return the list of possible fields
     */
    QList<Property> fields() const;
  private:
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace kDB::Repository::RDF
