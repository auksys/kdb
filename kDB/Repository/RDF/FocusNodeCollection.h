#pragma once

#include <kDB/Forward.h>

#include <QExplicitlySharedDataPointer>

#include <knowCore/ConstrainedValue.h>
#include <knowCore/UriList.h>

#include <kDB/Repository/Connection.h>

#include "FocusNode.h"

namespace kDB::Repository::RDF
{
  /**
   * Interface a @ref kDB::Repository::TripleStore that contains a set of focus nodes.
   * For instance, datasets, agents, missions, salient regions...
   */
  class FocusNodeCollection
  {
    friend class kDB::Repository::Connection;
  private:
    static cres_qresult<void> initialise(const Connection& _connection);
  public:
    FocusNodeCollection();
    FocusNodeCollection(const FocusNodeCollection& _rhs);
    FocusNodeCollection operator=(const FocusNodeCollection& _rhs);
    ~FocusNodeCollection();
  public:
    bool operator==(const FocusNodeCollection& _collection) const;
  public:
    /**
     * Return an unique instance of the focus nodes stored in \p _collection_uri for a given \p
     * _connection \return the collection or an error (in particular, if the collection does not
     * exists).
     */
    static cres_qresult<FocusNodeCollection> get(const kDB::Repository::Connection& _connection,
                                                 const knowCore::Uri& _collection_uri);
    /**
     * Create a new @ref kDB::Repository::TripleStore with uri \p _graph.
     * \return the collection or an error (in particular, if the collection already exists).
     */
    static cres_qresult<FocusNodeCollection> create(const kDB::Repository::Connection& _connection,
                                                    const knowCore::Uri& _graph,
                                                    const knowCore::Uri& _collection_type,
                                                    const knowCore::UriList& _contained_types);
    /**
     * Create or access the focus node collection specified in \ref _graph
     * \return the collection or an error (in particular, if the collection already exists with
     *          a different type).
     */
    static cres_qresult<FocusNodeCollection>
      getOrCreate(const kDB::Repository::Connection& _connection, const knowCore::Uri& _graph,
                  const knowCore::Uri& _collection_type, const knowCore::UriList& _contained_types);

    /**
     * @return the collection which all the documents.
     */
    static FocusNodeCollection allCollections(const kDB::Repository::Connection& _connection);

    /**
     * @return a collection which is a view over all the focus nodes.
     */
    static FocusNodeCollection allFocusNodes(const kDB::Repository::Connection& _connection);

    /**
     * \return a list of the uris of all collection uris for a given type.
     */
    static cres_qresult<knowCore::UriList>
      allCollectionUris(const kDB::Repository::QueryConnectionInfo& _connection,
                        const knowCore::Uri& _focus_node_type);
    /**
     * \return a list of collection uri which includes a copy of the given focus node
     */
    static cres_qresult<knowCore::UriList>
      allCollectionUrisWith(const kDB::Repository::QueryConnectionInfo& _connection,
                            const knowCore::Uri& _focus_node);
  public:
    static cres_qresult<void>
      registerCollection(const kDB::Repository::QueryConnectionInfo& _connection,
                         const knowCore::Uri& _view_graph, const knowCore::Uri& _collection_type,
                         const knowCore::UriList& _contained_types);
  public:
    /**
     * @return true if it is a valid set of focus nodes
     */
    bool isValid() const;
    /**
     * @return true if it can only be used to access focus nodes and not insert one.
     */
    bool isReadOnly() const;
    /**
     * @return types contained in the collection.
     */
    cres_qresult<knowCore::UriList> containedTypes() const;
    /**
     * @return type of the collection.
     */
    cres_qresult<knowCore::Uri> collectionType() const;
    /**
     * @return the uri for the graph
     */
    knowCore::Uri uri() const;
    /**
     * @return the connection
     */
    kDB::Repository::Connection connection() const;
    /**
     * @return the number of focus nodes in this collection.
     */
    cres_qresult<std::size_t> count() const;
    /**
     * @return the number of focus nodes in this collection of a given type.
     */
    cres_qresult<std::size_t> count(const knowCore::Uri& _type) const;
    /**
     * \return the focus node with the Uri \ref _focusNodeUri
     */
    cres_qresult<FocusNode> focusNode(const knowCore::Uri& _focusNodeUri) const;
    /**
     * @return true if the focus node already exists.
     */
    cres_qresult<bool> hasFocusNode(const knowCore::Uri& _focusNodeUri,
                                    const knowCore::Uri& _typeUri) const;
    /**
     * @return all the focus node contained in this document.
     */
    cres_qresult<QList<FocusNode>> all() const;
    /**
     * @return all the focus node contained in this document.
     */
    cres_qresult<QList<FocusNode>> all(const knowCore::Uri& _typeUri) const;
    struct OperatorOptions
    {
      OperatorOptions() : intersectsPrecision(0.0) {}
      OperatorOptions(double _intersectsPrecision) : intersectsPrecision(_intersectsPrecision) {}
      double intersectsPrecision;
    };
    /**
     * @param _constraints a list of pair of list of uris representing the property uri and a
     * constraint.
     * @param _operatorOptions set the precision used by operators
     *
     * \return the list of agents that satisfies the constraints
     */
    cres_qresult<QList<FocusNode>>
      focusNodes(const knowCore::UriList& _default_datatypes,
                 const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
                 const OperatorOptions& _operatorOptions = OperatorOptions()) const;
    /**
     * Create a new \ref FocusNode of uri type \p _typeUri  and add it to this collection.
     */
    cres_qresult<FocusNode> createFocusNode(const knowCore::Uri& _typeUri,
                                            const knowCore::ValueHash& _properties,
                                            const knowCore::Uri& _focusNodeUri);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
    FocusNodeCollection(const QExplicitlySharedDataPointer<Private>& _d);
    /**
     * \private
     * Clear the cache (only use for testing purposes!)
     */
    static void clearCache();
  };

  template<typename _TDerive_>
  struct FocusNodeCollectionTrait;

  template<typename _TDerive_>
  class FocusNodeCollectionWrapper
  {
  public:
    using Derive = _TDerive_;
    using ValueType = typename FocusNodeCollectionTrait<Derive>::ValueType;
  public:
    /**
     * Return an unique instance of the collection stored in \p _graph for a given \p _connection
     * \return an invalid \ref Collection if it does not exists
     */
    static cres_qresult<Derive> get(const kDB::Repository::Connection& _connection,
                                    const knowCore::Uri& _graph)
    {
      Derive c;
      cres_try(c.m_focus_node_collection, FocusNodeCollection::get(_connection, _graph));
      return cres_success(c);
    }

    /**
     * Create a new @ref kDB::Repository::TripleStore with uri \p _graph, add it to the union of
     * collections \ref Uris::kdb_sensing::datasets. \return an invalid \ref Collection if an error
     * occurs or if the collection already exists
     */
    static cres_qresult<Derive> create(const kDB::Repository::Connection& _connection,
                                       const knowCore::Uri& _graph)
    {
      Derive c;
      cres_try(c.m_focus_node_collection,
               FocusNodeCollection::create(_connection, _graph, Derive::collectionType(),
                                           Derive::containedTypes()));
      return cres_success(c);
    }

    /**
     * Create or access the collection specified in \ref _graph
     * \return an invalid \ref Collection if an error occurs
     */
    static cres_qresult<Derive> getOrCreate(const kDB::Repository::Connection& _connection,
                                            const knowCore::Uri& _graph)
    {
      Derive c;
      cres_try(c.m_focus_node_collection,
               FocusNodeCollection::getOrCreate(_connection, _graph, Derive::collectionType(),
                                                Derive::containedTypes()));
      clog_debug_vn(c.isValid(), c.uri());
      return cres_success(c);
    }
  public:
    bool operator==(const Derive& _rhs) const
    {
      return m_focus_node_collection == _rhs.m_focus_node_collection;
    }
  public:
    /**
     * @return true if it is a valid collection of datasets
     */
    bool isValid() const { return m_focus_node_collection.isValid(); }
    /**
     * @return true if it can only be used to access datasets and not insert one.
     */
    bool isReadOnly() const { return m_focus_node_collection.isReadOnly(); }
    /**
     * @return the uri for the graph
     */
    knowCore::Uri uri() const { return m_focus_node_collection.uri(); }
    /**
     * @return the connection
     */
    kDB::Repository::Connection connection() const { return m_focus_node_collection.connection(); }
    /**
     * @return the number of datasets in this document.
     */
    cres_qresult<std::size_t> count() const
    {
      return m_focus_node_collection.count(Derive::primaryType());
    }
    /**
     * @return all the values contained in this document.
     */
    cres_qresult<QList<ValueType>> all() const
    {
      QList<ValueType> all;
      cres_try(QList<FocusNode> fns, m_focus_node_collection.all());
      for(const FocusNode& node : fns)
      {
        cres_try(ValueType ds, ValueType::fromFocusNode(node));
        all.push_back(ds);
      }
      return cres_success(all);
    }
    static cres_qresult<void>
      registerCollection(const kDB::Repository::QueryConnectionInfo& _connection)
    {
      return FocusNodeCollection::registerCollection(_connection, Derive::allFocusNodesView(),
                                                     Derive::collectionType(),
                                                     Derive::containedTypes());
    }
  protected:
    operator Derive() const { return Derive(*this); }
    cres_qresult<ValueType> focusNode(const knowCore::Uri& _uri) const
    {
      cres_try(FocusNode fn, m_focus_node_collection.focusNode(_uri));
      cres_try(ValueType vt, ValueType::fromFocusNode(fn));
      return cres_success(vt);
    }
    cres_qresult<bool> hasFocusNode(const knowCore::Uri& _focusNodeUri,
                                    const knowCore::Uri& _typeUri = Derive::primaryType()) const
    {
      return m_focus_node_collection.hasFocusNode(_focusNodeUri, _typeUri);
    }
    cres_qresult<QList<ValueType>>
      focusNodes(const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
                 const FocusNodeCollection::OperatorOptions& _operatorOptions) const
    {
      cres_try(knowCore::UriList default_datatypes, Derive::defaultDatatypes(_constraints));
      return focusNodes(default_datatypes, _constraints, _operatorOptions);
    }
    template<typename... _TArgs_>
    cres_qresult<QList<ValueType>> focusNodes(const knowCore::Uri& _uri,
                                              const knowCore::ConstrainedValue& _constraint,
                                              const _TArgs_&... _args) const
    {
      QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>> constraints;
      FocusNodeCollection::OperatorOptions operatorOptions;
      buildFocusNodeConstraints(&constraints, &operatorOptions, _uri, _constraint, _args...);
      cres_try(knowCore::UriList default_datatypes, Derive::defaultDatatypes(constraints));
      return focusNodes(default_datatypes, constraints, operatorOptions);
    }

    cres_qresult<ValueType> createFocusNode(const knowCore::Uri& _typeUri,
                                            const knowCore::ValueHash& _properties,
                                            const knowCore::Uri& _focusNodeUri)
    {
      cres_try(FocusNode fn,
               m_focus_node_collection.createFocusNode(_typeUri, _properties, _focusNodeUri));
      return ValueType::fromFocusNode(fn);
    }
    FocusNodeCollection& focusNodeCollection() { return m_focus_node_collection; }
    const FocusNodeCollection& focusNodeCollection() const { return m_focus_node_collection; }
  private:
    cres_qresult<QList<ValueType>>
      focusNodes(const knowCore::UriList& _default_datatypes,
                 const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
                 const FocusNodeCollection::OperatorOptions& _operatorOptions) const
    {
      cres_try(QList<FocusNode> nodes, m_focus_node_collection.focusNodes(
                                         _default_datatypes, _constraints, _operatorOptions));
      QList<ValueType> rs;
      for(const FocusNode& node : nodes)
      {
        cres_try(ValueType vt, ValueType::fromFocusNode(node));
        rs.append(vt);
      }
      return cres_success(rs);
    }

    template<typename... _TArgs_>
    void buildFocusNodeConstraints(
      QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>* _constraints,
      FocusNodeCollection::OperatorOptions* _operatorOptions, const knowCore::Uri& _uri,
      const knowCore::ConstrainedValue& _constraint, const _TArgs_&... _args) const
    {
      _constraints->append({_uri, _constraint});
      buildFocusNodeConstraints(_constraints, _operatorOptions, _args...);
    }

    void
      buildFocusNodeConstraints(QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>*,
                                FocusNodeCollection::OperatorOptions* _out_oo,
                                const FocusNodeCollection::OperatorOptions& _operatorOptions) const
    {
      *_out_oo = _operatorOptions;
    }

    void buildFocusNodeConstraints(QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>*,
                                   FocusNodeCollection::OperatorOptions*) const
    {
    }
  private:
    FocusNodeCollection m_focus_node_collection;
  };

} // namespace kDB::Repository::RDF
