#include "FocusNode.h"

#include <QCborArray>

#include <knowCore/QuantityValue.h>

#include "Connection.h"
#include "FocusNodeDeclaration.h"
#include "FocusNodeDeclarationsRegistry.h"
#include "FocusNodeQueries.h"
#include "QueryConnectionInfo.h"
#include "TemporaryTransaction.h"
#include "Transaction.h"

#include <knowCore/Uris/askcore_datatype.h>

using namespace kDB::Repository::RDF;

using askcore_datatype = knowCore::Uris::askcore_datatype;

struct FocusNode::Private : QSharedData
{
  knowCore::Uri graph, object_uri;
  FocusNodeDeclaration declaration;
  kDB::Repository::Connection connection;
};

FocusNode::FocusNode() : d(new Private) {}

FocusNode::FocusNode(const FocusNode& _rhs) : d(_rhs.d) {}

FocusNode& FocusNode::operator=(const FocusNode& _rhs)
{
  d = _rhs.d;
  return *this;
}

FocusNode::FocusNode(const kDB::Repository::Connection& _connection, const knowCore::Uri& _graph,
                     const knowCore::Uri& _object_uri, const FocusNodeDeclaration& _declaration)
    : d(new Private{QSharedData(), _graph, _object_uri, _declaration, _connection})
{
}

FocusNode::~FocusNode() {}

bool FocusNode::isValid() const
{
  if(d->connection.isValid())
  {
    cres_qresult<bool> e = exists();
    if(e.is_successful())
    {
      return e.get_value();
    }
    else
    {
      clog_error("An error occured when checking for existance of collection '{}': {}",
                 d->object_uri, e.get_error());
      return false;
    }
  }
  else
  {
    return false;
  }
}

cres_qresult<bool> FocusNode::exists() const
{
  if(d->connection.isValid())
  {
    FocusNodeQueries::FocusNodeSelect::Results r
      = FocusNodeQueries::FocusNodeSelect::execute(d->connection, d->graph, d->object_uri);
    if(r.success)
    {
      return cres_success(r.data.size() == 1);
    }
    else
    {
      return cres_failure("Failed to query for focus node with error {}", r.errorMessage);
    }
  }
  else
  {
    return cres_failure("Invalid focus node");
  }
}

bool FocusNode::operator==(const FocusNode& _rhs) const
{
  return d == _rhs.d or d->object_uri == _rhs.d->object_uri;
}

FocusNodeDeclaration FocusNode::declaration() const { return d->declaration; }

knowCore::Uri FocusNode::uri() const { return d->object_uri; }

knowCore::Uri FocusNode::graph() const { return d->graph; }

kDB::Repository::Connection FocusNode::connection() const { return d->connection; }

cres_qresult<bool> FocusNode::hasProperty(const knowCore::Uri& _property,
                                          const kDB::Repository::Transaction& _transaction) const
{
  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In checking for property: {}"));
  switch(field.direction())
  {
  case FocusNodeDeclaration::Property::Direction::Direct:
  {
    FocusNodeQueries::HasProperty::Results r = FocusNodeQueries::HasProperty::execute(
      {_transaction, d->connection}, d->graph, d->object_uri, d->declaration.type(), field.path());
    if(r.success)
    {
      return cres_success(r.data);
    }
    else
    {
      return cres_failure("Failure in checking for property '{}' existance: '{}'", _property,
                          r.errorMessage);
    }
  }
  case FocusNodeDeclaration::Property::Direction::Reverse:
  {
    FocusNodeQueries::HasPropertyReverse::Results r = FocusNodeQueries::HasPropertyReverse::execute(
      {_transaction, d->connection}, d->graph, d->object_uri, d->declaration.type(), field.path());
    if(r.success)
    {
      return cres_success(r.data);
    }
    else
    {
      return cres_failure("Failure in checking for property '{}' existance: '{}'", _property,
                          r.errorMessage);
    }
  }
  }
  return cres_failure("Internal error, unknown property direction.");
}

cres_qresult<knowCore::Value>
  FocusNode::property(const knowCore::Uri& _property,
                      const kDB::Repository::Transaction& _transaction) const
{
  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In accessing property: {}"));
  switch(field.variety())
  {
  case FocusNodeDeclaration::Property::Variety::Atomic:
  case FocusNodeDeclaration::Property::Variety::Optional:
    if(field.datatype() == askcore_datatype::quantityDecimal)
    {
      FocusNodeQueries::SelectPropertyQuantityValue::Results r
        = FocusNodeQueries::SelectPropertyQuantityValue::execute(
          {_transaction, d->connection}, d->graph, d->object_uri, d->declaration.type(),
          field.path());
      if(r.data.size() == 0)
      {
        return cres_failure("Not set");
      }
      else if(r.data.size() == 1)
      {
        cres_try(knowCore::QuantityNumber value,
                 knowCore::QuantityNumber::create(r.data.first().value(), r.data.first().unit()));
        return cres_success(knowCore::Value::fromValue(value));
      }
      else
      {
        QList<knowCore::Value> values;
        for(int i = 0; i < r.data.size(); ++i)
        {
          cres_try(knowCore::QuantityNumber value,
                   knowCore::QuantityNumber::create(r.data[i].value(), r.data[i].unit()));
          values.append(knowCore::Value::fromValue(value));
        }
        return cres_success(knowCore::Value::fromValue<knowCore::ValueList>(values));
      }
    }
    else
    {
      switch(field.direction())
      {
      case FocusNodeDeclaration::Property::Direction::Direct:
      {
        FocusNodeQueries::SelectProperty::Results r = FocusNodeQueries::SelectProperty::execute(
          d->connection, d->graph, d->object_uri, d->declaration.type(), field.path());
        if(r.data.size() == 0)
        {
          return cres_failure("Not set");
        }
        else if(r.data.size() == 1)
        {
          return cres_success(r.data.first().value());
        }
        else
        {
          return cres_failure("Non-list property {} has been set multiple times.", field.path());
        }
      }
      case FocusNodeDeclaration::Property::Direction::Reverse:
      {
        FocusNodeQueries::SelectPropertyReverse::Results r
          = FocusNodeQueries::SelectPropertyReverse::execute(d->connection, d->graph, d->object_uri,
                                                             d->declaration.type(), field.path());
        if(r.data.size() == 0)
        {
          return cres_failure("Not set");
        }
        else if(r.data.size() == 1)
        {
          return cres_success(r.data.first().value());
        }
        else
        {
          return cres_failure("Non-list property {} has been set multiple times.", field.path());
        }
      }
      }
      return cres_failure("Internal Error: invalid direction.");
    }
  case FocusNodeDeclaration::Property::Variety::List:
    return propertyList(_property, _transaction);
  case FocusNodeDeclaration::Property::Variety::Map:
    return propertyMap(_property, _transaction);
  }
  return cres_failure("Internal Error: invalid variety.");
}

cres_qresult<void> FocusNode::setProperty(const knowCore::Uri& _property,
                                          const knowCore::Value& _value,
                                          const Transaction& _transaction) const
{
  return setProperty(_property, _value, _transaction, false);
}

cres_qresult<void> FocusNode::setProperty(const knowCore::Uri& _property,
                                          const knowCore::Value& _value,
                                          const Transaction& _transaction,
                                          bool _allow_constant_changes) const
{
  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In setting property: {}"));
  kDB::Repository::TemporaryTransaction tt(_transaction, d->connection);

  if(field.isConstant() and not _allow_constant_changes)
  {
    FocusNodeQueries::HasProperty::Results r = FocusNodeQueries::HasProperty::execute(
      tt.transaction(), d->graph, d->object_uri, d->declaration.type(), field.path());
    if(r.success)
    {
      if(r.data)
      {
        tt.rollback();
        return cres_failure("Constant property '{}' is already set", _property);
      }
    }
    else
    {
      tt.rollback();
      return cres_failure("Failure in checking for property '{}' existance: '{}'", _property,
                          r.errorMessage);
    }
  }

  switch(field.variety())
  {
  case FocusNodeDeclaration::Property::Variety::Atomic:
  case FocusNodeDeclaration::Property::Variety::Optional:
  {

    if(field.acceptedValues())
    {
      if(not field.acceptedValues()->contains(_value))
      {
        tt.rollback();
        return cres_failure("Value '{}' is not accepted value for '{}'", _value, _property);
      }
    }

    if(field.datatype() == askcore_datatype::quantityDecimal)
    {
      cres_try(knowCore::QuantityNumber qn, _value.value<knowCore::QuantityNumber>());
      knowCore::Value val_qn = knowCore::Value::fromValue(qn.value());
      clog_assert(bool(field.acceptedUnits()));
      if(field.acceptedUnits()->contains(qn.unit().uri()))
      {
        FocusNodeQueries::SetPropertyQuantityValue::Results r
          = FocusNodeQueries::SetPropertyQuantityValue::execute(
            tt.transaction(), d->graph, d->object_uri, _property, val_qn, qn.unit().uri());
        if(r.success)
        {
          return tt.commitIfOwned();
        }
        else
        {
          tt.rollback();
          clog_error("Failed to set dataset property {} with value {} to dataset {}: {}", _property,
                     _value, d->object_uri, r.errorMessage);
          return cres_failure("Failed to set dataset property {} with value {} to dataset {}: {}",
                              _property, _value, d->object_uri, r.errorMessage);
        }
      }
      else
      {
        tt.rollback();
        return cres_failure("Invalid unit '{}' for property '{}', admissible units are {}",
                            qn.unit(), _property, *field.acceptedUnits());
      }
    }
    else
    {
      switch(field.direction())
      {
      case FocusNodeDeclaration::Property::Direction::Direct:
      {
        FocusNodeQueries::SetProperty::Results r = FocusNodeQueries::SetProperty::execute(
          tt.transaction(), d->graph, d->object_uri, _property, _value);
        if(r.success)
        {
          return tt.commitIfOwned();
        }
        else
        {
          tt.rollback();
          clog_error("Failed to set dataset property {} with value {} to dataset {}: {}", _property,
                     _value, d->object_uri, r.errorMessage);
          return cres_failure("Failed to set dataset property {} with value {} to dataset {}: {}",
                              _property, _value, d->object_uri, r.errorMessage);
        }
      }
      case FocusNodeDeclaration::Property::Direction::Reverse:
      {
        FocusNodeQueries::SetPropertyReverse::Results r
          = FocusNodeQueries::SetPropertyReverse::execute(tt.transaction(), d->graph, d->object_uri,
                                                          _property, _value);
        if(r.success)
        {
          return tt.commitIfOwned();
        }
        else
        {
          tt.rollback();
          clog_error("Failed to set dataset property {} with value {} to dataset {}: {}", _property,
                     _value, d->object_uri, r.errorMessage);
          return cres_failure("Failed to set dataset property {} with value {} to dataset {}: {}",
                              _property, _value, d->object_uri, r.errorMessage);
        }
      }
      }
    }
    break;
  }
  case FocusNodeDeclaration::Property::Variety::List:
  {
    if(knowCore::ValueCast<knowCore::ValueList> list = _value)
    {
      for(const knowCore::Value& value : list.value())
      {
        cres_try(cres_ignore, addPropertyToList(_property, value, _transaction, true));
      }
    }
    else
    {
      cres_try(cres_ignore, addPropertyToList(_property, _value, _transaction, true));
    }
    break;
  }
  case FocusNodeDeclaration::Property::Variety::Map:
  {
    cres_try(knowCore::ValueHash vh, _value.value<knowCore::ValueHash>());
    QHash<QString, knowCore::Value> h = vh.hash();
    for(auto it = h.begin(); it != h.end(); ++it)
    {
      cres_try(cres_ignore, setPropertyInMap(_property, it.key(), it.value(), _transaction, true));
    }
    break;
  }
  }

  return tt.commitIfOwned();
}

cres_qresult<void> FocusNode::addPropertyToList(const knowCore::Uri& _property,
                                                const knowCore::Value& _value,
                                                const Transaction& _transaction) const
{
  return addPropertyToList(_property, _value, _transaction, false);
}

cres_qresult<void> FocusNode::addPropertyToList(const knowCore::Uri& _property,
                                                const knowCore::Value& _value,
                                                const Transaction& _transaction,
                                                bool _allow_constant_changes) const
{

  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In setting property: {}"));

  if(field.variety() != FocusNodeDeclaration::Property::Variety::List)
  {
    return cres_failure("Cannot add a property to non list property '{}'", _property);
  }

  if(field.acceptedValues())
  {
    if(not field.acceptedValues()->contains(_value))
    {
      return cres_failure("Value '{}' is not accepted value for '{}'", _value, _property);
    }
  }

  if(field.isConstant() and _allow_constant_changes == false)
  {
    return cres_failure("'addPropertyToList' cannot be used for property '{}', because it is "
                        "constant and can only be set once.",
                        _property);
  }

  kDB::Repository::TemporaryTransaction tt(_transaction, d->connection);

  if(field.datatype() == askcore_datatype::quantityDecimal)
  {
    cres_try(knowCore::QuantityNumber qn, _value.value<knowCore::QuantityNumber>());
    knowCore::Value val_qn = knowCore::Value::fromValue(qn.value());
    clog_assert(bool(field.acceptedUnits()));
    if(field.acceptedUnits()->contains(qn.unit().uri()))
    {
      FocusNodeQueries::AddPropertyToListQuantityValue::Results r
        = FocusNodeQueries::AddPropertyToListQuantityValue::execute(
          tt.transaction(), d->graph, d->object_uri, _property, val_qn, qn.unit().uri());
      if(r.success)
      {
      }
      else
      {
        tt.rollback();
        clog_error("Failed to add to dataset property {} with value {} to dataset {}: {}",
                   _property, _value, d->object_uri, r.errorMessage);
        return cres_failure("Failed to set dataset property {} with value {} to dataset {}: {}",
                            _property, _value, d->object_uri, r.errorMessage);
      }
    }
    else
    {
      tt.rollback();
      return cres_failure("Invalid unit '{}' for property '{}', admissible units are {}", qn.unit(),
                          _property, *field.acceptedUnits());
    }
  }
  else
  {
    switch(field.direction())
    {
    case FocusNodeDeclaration::Property::Direction::Direct:
    {
      FocusNodeQueries::AddPropertyToList::Results r = FocusNodeQueries::AddPropertyToList::execute(
        tt.transaction(), d->graph, d->object_uri, _property, _value);
      if(r.success)
      {
        return tt.commitIfOwned();
      }
      else
      {
        tt.rollback();
        clog_error("Failed to set dataset property {} with value {} to dataset {}: {}", _property,
                   _value, d->object_uri, r.errorMessage);
        return cres_failure("Failed to add to dataset property {} with value {} to dataset {}: {}",
                            _property, _value, d->object_uri, r.errorMessage);
      }
    }
    case FocusNodeDeclaration::Property::Direction::Reverse:
    {
      FocusNodeQueries::AddPropertyToListReverse::Results r
        = FocusNodeQueries::AddPropertyToListReverse::execute(tt.transaction(), d->graph,
                                                              d->object_uri, _property, _value);
      if(r.success)
      {
        return tt.commitIfOwned();
      }
      else
      {
        tt.rollback();
        clog_error("Failed to set dataset property {} with value {} to dataset {}: {}", _property,
                   _value, d->object_uri, r.errorMessage);
        return cres_failure("Failed to add to dataset property {} with value {} to dataset {}: {}",
                            _property, _value, d->object_uri, r.errorMessage);
      }
    }
    }
  }
  return tt.commitIfOwned();
}

cres_qresult<void> FocusNode::removePropertyFromList(const knowCore::Uri& _property,
                                                     const knowCore::Value& _value,
                                                     const Transaction& _transaction) const
{
  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In setting property: {}"));

  if(field.variety() != FocusNodeDeclaration::Property::Variety::List)
  {
    return cres_failure("Cannot remove a property to non list property '{}'", _property);
  }

  if(field.acceptedValues())
  {
    if(not field.acceptedValues()->contains(_value))
    {
      return cres_failure("Value '{}' is not accepted value for '{}'", _value, _property);
    }
  }

  if(field.isConstant())
  {
    return cres_failure("'addPropertyToList' cannot be used for property '{}', because it is "
                        "constant and can only be set once.",
                        _property);
  }

  kDB::Repository::TemporaryTransaction tt(_transaction, d->connection);

  if(field.datatype() == askcore_datatype::quantityDecimal)
  {
    cres_try(knowCore::QuantityNumber qn, _value.value<knowCore::QuantityNumber>());
    knowCore::Value val_qn = knowCore::Value::fromValue(qn.value());
    clog_assert(bool(field.acceptedUnits()));
    if(field.acceptedUnits()->contains(qn.unit().uri()))
    {
      FocusNodeQueries::RemovePropertyFromListQuantityValue::Results r
        = FocusNodeQueries::RemovePropertyFromListQuantityValue::execute(
          tt.transaction(), d->graph, d->object_uri, _property, val_qn, qn.unit().uri());
      if(r.success)
      {
      }
      else
      {
        tt.rollback();
        clog_error("Failed to remove from dataset property {} with value {} to dataset {}: {}",
                   _property, _value, d->object_uri, r.errorMessage);
        return cres_failure("Failed to set dataset property {} with value {} to dataset {}: {}",
                            _property, _value, d->object_uri, r.errorMessage);
      }
    }
    else
    {
      tt.rollback();
      return cres_failure("Invalid unit '{}' for property '{}', admissible units are {}", qn.unit(),
                          _property, *field.acceptedUnits());
    }
  }
  else
  {
    switch(field.direction())
    {
    case FocusNodeDeclaration::Property::Direction::Direct:
    {
      FocusNodeQueries::RemovePropertyFromList::Results r
        = FocusNodeQueries::RemovePropertyFromList::execute(tt.transaction(), d->graph,
                                                            d->object_uri, _property, _value);
      if(r.success)
      {
        return tt.commitIfOwned();
      }
      else
      {
        tt.rollback();
        clog_error("Failed to set dataset property {} with value {} to dataset {}: {}", _property,
                   _value, d->object_uri, r.errorMessage);
        return cres_failure(
          "Failed to remove from dataset property {} with value {} to dataset {}: {}", _property,
          _value, d->object_uri, r.errorMessage);
      }
    }
    case FocusNodeDeclaration::Property::Direction::Reverse:
    {
      FocusNodeQueries::RemovePropertyFromListReverse::Results r
        = FocusNodeQueries::RemovePropertyFromListReverse::execute(
          tt.transaction(), d->graph, d->object_uri, _property, _value);
      if(r.success)
      {
        return tt.commitIfOwned();
      }
      else
      {
        tt.rollback();
        clog_error("Failed to set dataset property {} with value {} to dataset {}: {}", _property,
                   _value, d->object_uri, r.errorMessage);
        return cres_failure(
          "Failed to remove from dataset property {} with value {} to dataset {}: {}", _property,
          _value, d->object_uri, r.errorMessage);
      }
    }
    }
  }
  return tt.commitIfOwned();
}

cres_qresult<knowCore::ValueList> FocusNode::propertyList(const knowCore::Uri& _property,
                                                          const Transaction& _transaction) const
{
  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In accessing property: {}"));

  if(field.variety() != FocusNodeDeclaration::Property::Variety::List)
  {
    return cres_failure("Property {} is not a list.");
  }

  if(field.datatype() == askcore_datatype::quantityDecimal)
  {

    FocusNodeQueries::SelectAllPropertiesFromListQuantityValue::Results r
      = FocusNodeQueries::SelectAllPropertiesFromListQuantityValue::execute(
        {_transaction, d->connection}, d->graph, d->object_uri, d->declaration.type(),
        field.path());

    if(r.success)
    {
      QList<knowCore::Value> h;
      for(qsizetype i = 0; i < r.data.size(); ++i)
      {
        cres_try(knowCore::QuantityNumber value,
                 knowCore::QuantityNumber::create(r.data[i].value(), r.data[i].unit()));
        h.append(knowCore::Value::fromValue(value));
      }
      return cres_success(h);
    }
    else
    {
      return cres_failure("Failed to get all properties from map {} in focus node {}: {}",
                          _property, d->object_uri, r.errorMessage);
    }
  }
  else
  {
    switch(field.direction())
    {
    case FocusNodeDeclaration::Property::Direction::Direct:
    {
      FocusNodeQueries::SelectProperty::Results r = FocusNodeQueries::SelectProperty::execute(
        d->connection, d->graph, d->object_uri, d->declaration.type(), field.path());
      QList<knowCore::Value> values;
      for(qsizetype i = 0; i < r.data.size(); ++i)
      {
        values.append(r.data[i].value());
      }
      return cres_success(values);
    }
    case FocusNodeDeclaration::Property::Direction::Reverse:
    {
      FocusNodeQueries::SelectPropertyReverse::Results r
        = FocusNodeQueries::SelectPropertyReverse::execute(d->connection, d->graph, d->object_uri,
                                                           d->declaration.type(), field.path());
      QList<knowCore::Value> values;
      for(qsizetype i = 0; i < r.data.size(); ++i)
      {
        values.append(r.data[i].value());
      }
      return cres_success(values);
    }
    }
    return cres_failure("Internal Error: invalid direction.");
  }
}

cres_qresult<void> FocusNode::setPropertyInMap(const knowCore::Uri& _property,
                                               const knowCore::Uri& _key,
                                               const knowCore::Value& _value,
                                               const Transaction& _transaction) const
{
  return setPropertyInMap(_property, _key, _value, _transaction, false);
}

cres_qresult<void> FocusNode::setPropertyInMap(const knowCore::Uri& _property,
                                               const knowCore::Uri& _key,
                                               const knowCore::Value& _value,
                                               const Transaction& _transaction,
                                               bool _allow_constant_changes) const
{
  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In setting property: {}"));

  if(field.variety() != FocusNodeDeclaration::Property::Variety::Map)
  {
    return cres_failure("Cannot set a property to non map property '{}'", _property);
  }

  if(field.acceptedValues())
  {
    if(not field.acceptedValues()->contains(_value))
    {
      return cres_failure("Value '{}' is not accepted value for '{}'", _value, _property);
    }
  }

  if(field.isConstant() and _allow_constant_changes == false)
  {
    return cres_failure("'setPropertyInMap' cannot be used for property '{}', because it is "
                        "constant and can only be set once.",
                        _property);
  }

  kDB::Repository::TemporaryTransaction tt(_transaction, d->connection);

  if(field.datatype() == askcore_datatype::quantityDecimal)
  {
    cres_try(knowCore::QuantityNumber qn, _value.value<knowCore::QuantityNumber>());
    knowCore::Value val_qn = knowCore::Value::fromValue(qn.value());
    clog_assert(bool(field.acceptedUnits()));
    if(field.acceptedUnits()->contains(qn.unit().uri()))
    {
      FocusNodeQueries::SetPropertyInMapQuantityValue::Results r
        = FocusNodeQueries::SetPropertyInMapQuantityValue::execute(
          tt.transaction(), d->graph, d->object_uri, _property, _key, val_qn, qn.unit().uri());
      if(r.success)
      {
      }
      else
      {
        tt.rollback();
        clog_error("Failed to set a value to dataset property {} with value {} to dataset {}: {}",
                   _property, _value, d->object_uri, r.errorMessage);
        return cres_failure("Failed to set dataset property {} with value {} to dataset {}: {}",
                            _property, _value, d->object_uri, r.errorMessage);
      }
    }
    else
    {
      tt.rollback();
      return cres_failure("Invalid unit '{}' for property '{}', admissible units are {}", qn.unit(),
                          _property, *field.acceptedUnits());
    }
  }
  else
  {
    FocusNodeQueries::SetPropertyInMap::Results r = FocusNodeQueries::SetPropertyInMap::execute(
      tt.transaction(), d->graph, d->object_uri, _property, _key, _value);
    if(r.success)
    {
    }
    else
    {
      tt.rollback();
      clog_error("Failed to set dataset property {} with value {} to dataset {}: {}", _property,
                 _value, d->object_uri, r.errorMessage);
      return cres_failure("Failed to add to dataset property {} with value {} to dataset {}: {}",
                          _property, _value, d->object_uri, r.errorMessage);
    }
  }
  return tt.commitIfOwned();
}

cres_qresult<knowCore::ValueHash> FocusNode::propertyMap(const knowCore::Uri& _property,
                                                         const Transaction& _transaction) const
{
  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In accessing property: {}"));

  if(field.variety() != FocusNodeDeclaration::Property::Variety::Map)
  {
    return cres_failure("Property {} is not a map.");
  }

  FocusNodeQueries::SelectAllPropertiesFromMap::Results r
    = FocusNodeQueries::SelectAllPropertiesFromMap::execute(
      {_transaction, d->connection}, d->graph, d->object_uri, d->declaration.type(), field.path());

  if(r.success)
  {
    knowCore::ValueHash h;
    for(qsizetype i = 0; i < r.data.size(); ++i)
    {
      if(r.data[i].unit())
      {
        cres_try(knowCore::QuantityNumber value,
                 knowCore::QuantityNumber::create(r.data[i].value(), *r.data[i].unit()));
        h.insert(r.data[i].keyUri(), knowCore::Value::fromValue(value));
      }
      else
      {
        h.insert(r.data[i].keyUri(), r.data[i].value());
      }
    }
    return cres_success(h);
  }
  else
  {
    return cres_failure("Failed to get all properties from map {} in focus node {}: {}", _property,
                        d->object_uri, r.errorMessage);
  }
}

cres_qresult<knowCore::Value> FocusNode::propertyMapValue(const knowCore::Uri& _property,
                                                          const knowCore::Uri& _key,
                                                          const Transaction& _transaction) const
{
  cres_try(FocusNodeDeclaration::Property field, d->declaration.field(_property),
           message("In accessing property: {}"));

  if(field.variety() != FocusNodeDeclaration::Property::Variety::Map)
  {
    return cres_failure("Property {} is not a map.");
  }

  FocusNodeQueries::SelectAllPropertiesFromMap::Results r
    = FocusNodeQueries::SelectAllPropertiesFromMap::execute(
      {_transaction, d->connection}, d->graph, d->object_uri, d->declaration.type(), field.path());

  if(r.success)
  {
    if(r.data.size() == 1)
    {

      if(r.data[0].unit())
      {
        cres_try(knowCore::QuantityNumber value,
                 knowCore::QuantityNumber::create(r.data[0].value(), *r.data[0].unit()));
        return cres_success(knowCore::Value::fromValue(value));
      }
      else
      {
        return cres_success(r.data[0].value());
      }
    }
    else
    {
      return cres_failure(
        "Expected one answer got {} when accessing key {} in property {} of focus node {}.",
        r.data.size(), _key, _property, d->object_uri);
    }
  }
  else
  {
    return cres_failure("Failed to get all properties from map {} in focus node {}: {}", _property,
                        d->object_uri, r.errorMessage);
  }
}

static QString OBJECT_URI_KEY = u8"object_uri"_kCs;
static QString TYPE_URI_KEY = u8"type_uri"_kCs;
static QString PROPERTIES_KEY = u8"properties"_kCs;
static QString PROPERTY_PATH_KEY = u8"path"_kCs;
static QString PROPERTY_VALUE_KEY = u8"value"_kCs;

cres_qresult<QCborMap> FocusNode::toCborMap() const
{
  QCborMap map;
  map[OBJECT_URI_KEY] = QString(d->object_uri);
  map[TYPE_URI_KEY] = QString(d->declaration.type());

  QCborArray properties_array;

  for(const FocusNodeDeclaration::Property& field : d->declaration.fields())
  {
    QCborMap map;
    cres_try(bool hP, hasProperty(field.path()));
    if(hP)
    {
      cres_try(knowCore::Value value, property(field.path()));
      map[PROPERTY_PATH_KEY] = QString(field.path());
      cres_try(map[PROPERTY_VALUE_KEY], value.toCborMap());
      properties_array.append(map);
    }
  }
  map[PROPERTIES_KEY] = properties_array;

  return cres_success(map);
}

cres_qresult<FocusNode> FocusNode::fromCborMap(const kDB::Repository::Connection& _connection,
                                               const knowCore::Uri& _graph, const QCborMap& _map)
{
  knowCore::Uri object_uri = _map[OBJECT_URI_KEY].toString();
  knowCore::Uri type_uri = _map[TYPE_URI_KEY].toString();

  cres_try(FocusNodeDeclaration declaraion, FocusNodeDeclarationsRegistry::declaration(type_uri));

  kDB::Repository::TemporaryTransaction transaction(_connection);

  FocusNodeQueries::FocusNodeCreate::Results r = FocusNodeQueries::FocusNodeCreate::execute(
    transaction.transaction(), _graph, object_uri, type_uri);
  if(not r.success)
  {
    transaction.rollback();
    return cres_failure("Error while creating object, with error {}", r.errorMessage);
  }
  for(const knowCore::Uri& type : declaraion.inheritedTypes())
  {
    FocusNodeQueries::FocusNodeCreateAddInheritedType::Results r
      = FocusNodeQueries::FocusNodeCreateAddInheritedType::execute(transaction.transaction(),
                                                                   _graph, object_uri, type);
    if(not r.success)
    {
      transaction.rollback();
      return cres_failure("Error while creating object, with error {}", r.errorMessage);
    }
  }

  FocusNode fn(_connection, _graph, object_uri, declaraion);

  QCborArray array = _map[PROPERTIES_KEY].toArray();
  for(const QCborValue& property_value : array)
  {
    QCborMap pm = property_value.toMap();
    cres_try(knowCore::Value value, knowCore::Value::fromCborMap(pm[PROPERTY_VALUE_KEY].toMap()),
             on_failure(transaction.rollback()));
    cres_try(
      cres_ignore,
      fn.setProperty(pm[PROPERTY_PATH_KEY].toString(), value, transaction.transaction(), true),
      on_failure(transaction.rollback()));
  }
  cres_try(cres_ignore, transaction.commitIfOwned());
  return cres_success(fn);
}

cres_qresult<FocusNode> FocusNode::create(const kDB::Repository::QueryConnectionInfo& _connection,
                                          const knowCore::Uri& _graph_uri,
                                          const knowCore::Uri& _typeUri,
                                          const knowCore::ValueHash& _properties,
                                          const knowCore::Uri& _focusNodeUri)
{
  cres_try(FocusNodeDeclaration declaraion, FocusNodeDeclarationsRegistry::declaration(_typeUri));

  // Validate properties
  for(const FocusNodeDeclaration::Property& f : declaraion.fields())
  {
    if(f.variety() == FocusNodeDeclaration::Property::Variety::Atomic
       and not _properties.contains(f.path()) and f.defaultValue().isEmpty())
    {
      clog_debug_vn(f.defaultValue(), f.path(), "-----------------");
      return cres_failure("Missing required property '{}({})'.", f.path(), f.name());
    }
  }

  // Add to the db
  kDB::Repository::TemporaryTransaction transaction(_connection);

  FocusNodeQueries::HasFocusNode::Results hfn_r = FocusNodeQueries::HasFocusNode::execute(
    transaction.transaction(), _graph_uri, _focusNodeUri, _typeUri);
  if(hfn_r.success)
  {
    if(hfn_r.data)
    {
      transaction.rollback();
      return cres_failure("Focus node {} already exists", _focusNodeUri);
    }
  }
  else
  {
    transaction.rollback();
    return cres_failure("Error while querying for node existence, with error {}",
                        hfn_r.errorMessage);
  }

  FocusNodeQueries::FocusNodeCreate::Results r = FocusNodeQueries::FocusNodeCreate::execute(
    transaction.transaction(), _graph_uri, _focusNodeUri, _typeUri);
  if(not r.success)
  {
    transaction.rollback();
    return cres_failure("Error while creating object, with error {}", r.errorMessage);
  }
  for(const knowCore::Uri& type : declaraion.inheritedTypes())
  {
    FocusNodeQueries::FocusNodeCreateAddInheritedType::Results r
      = FocusNodeQueries::FocusNodeCreateAddInheritedType::execute(transaction.transaction(),
                                                                   _graph_uri, _focusNodeUri, type);
    if(not r.success)
    {
      transaction.rollback();
      return cres_failure("Error while creating object, with error {}", r.errorMessage);
    }
  }

  FocusNode fn(_connection.connection(), _graph_uri, _focusNodeUri, declaraion);

  QHash<QString, knowCore::Value> properties = _properties.hash();

  for(auto it = properties.begin(); it != properties.end(); ++it)
  {
    cres_try(cres_ignore, fn.setProperty(it.key(), it.value(), transaction.transaction(), true),
             on_failure(transaction.rollback()));
  }
  cres_try(cres_ignore, transaction.commitIfOwned());
  return cres_success(fn);
}
