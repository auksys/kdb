#include "FocusNodeDeclarationsRegistry.h"

#include "FocusNodeDeclaration_p.h"

#include <knowCore/TypeDefinitions.h>

#include <knowSHACL/Constraint.h>
#include <knowSHACL/Definition.h>
#include <knowSHACL/NodeShape.h>
#include <knowSHACL/Path.h>
#include <knowSHACL/PropertyShape.h>
#include <knowSHACL/Target.h>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/askcore_declaration.h>
#include <knowCore/Uris/qudt.h>
#include <knowCore/Uris/xsd.h>

#include <knowSHACL/Uris/sh.h>

using namespace kDB::Repository::RDF;
using askcore_datatype = knowCore::Uris::askcore_datatype;
using askcore_declaration = knowCore::Uris::askcore_declaration;

struct kDB::Repository::RDF::FocusNodeDeclarationsRegistryPrivate
{
  QList<QUrl> delayed_declarations;

  QHash<knowCore::Uri, QList<knowCore::Uri>> uri2units;
  QHash<knowCore::Uri, knowCore::Uri> property_map_shape;
  QHash<knowCore::Uri, FocusNodeDeclaration> declarations;

  cres_qresult<void> loadDefinitions(const QUrl& _url);
  cres_qresult<QPair<bool, FocusNodeDeclaration::Property>>
    createField(const knowSHACL::Constraint& _c);

  static FocusNodeDeclarationsRegistryPrivate* s_instance();
};

FocusNodeDeclarationsRegistryPrivate* FocusNodeDeclarationsRegistryPrivate::s_instance()
{
  static FocusNodeDeclarationsRegistryPrivate* s_instance
    = new FocusNodeDeclarationsRegistryPrivate;
  return s_instance;
}

#define ERR_MSG message(__KNOWCORE_STR(__FILE__) ":" __KNOWCORE_STR(__LINE__) ": {}")

cres_qresult<QPair<bool, FocusNodeDeclaration::Property>>
  FocusNodeDeclarationsRegistryPrivate::createField(const knowSHACL::Constraint& _c)
{
  if(_c.type() != knowSHACL::Constraint::Type::Property)
  {
    return cres_failure("Invalid type of constraint {}, should be property constraint.",
                        int(_c.type()));
  }
  knowSHACL::PropertyShape ps = _c.property();
  FocusNodeDeclaration::Property::Direction direction;
  switch(ps.path().type())
  {
  case knowSHACL::Path::Type::Inverse:
    direction = FocusNodeDeclaration::Property::Direction::Reverse;
    break;
  case knowSHACL::Path::Type::Predicate:
    direction = FocusNodeDeclaration::Property::Direction::Direct;
    break;
  default:
    return cres_failure("Invalid type of path {}, should be predicate.", (int)ps.path().type());
  }

  FocusNodeDeclaration::Property field;
  field.d = new FocusNodeDeclaration::Property::Private;
  field.d->direction = direction;
  field.d->path = ps.path().predicate();
  field.d->isConstant = ps.types().contains(askcore_declaration::constant);
  bool has_class = false;
  bool has_map = false;
  for(const knowSHACL::Constraint& c : ps.constraints())
  {
    using CT = knowSHACL::Constraint::Type;
    switch(c.type())
    {
    case CT::Empty:
      break;
    case CT::Datatype:
      if(not has_class)
      {
        cres_try(field.d->datatype, c.value<knowCore::Uri>(), ERR_MSG);
      }
      break;
    case CT::NodeKind:
    {
      cres_try(knowCore::Uri uri, c.value<knowCore::Uri>(), ERR_MSG);
      if(not has_class and uri == knowSHACL::Uris::sh::IRI)
      {
        field.d->datatype = knowCore::Uris::xsd::anyURI;
      }
      break;
    }
    case CT::Node:
    {
      knowSHACL::NodeShape ns = c.node();
      knowCore::Uri uri = ns.uri();
      if(uri2units.contains(uri))
      {
        field.d->datatype = knowCore::Uris::askcore_datatype::quantityDecimal;
        field.d->acceptedUnits = uri2units.value(uri);
      }
      else if(property_map_shape.contains(uri))
      {
        has_map = true;
        field.d->datatype = property_map_shape.value(uri);
      }
      break;
    }
    case CT::Class:
    {
      cres_try(field.d->datatype, c.value<knowCore::Uri>(), ERR_MSG);
      has_class = true;
      break;
    }
    case CT::In:
    {
      cres_try(field.d->acceptedLiterals, c.value<knowCore::ValueList>());
      break;
    }
    case CT::HasValue:
    {
      field.d->value = c.value();
      break;
    }
    case CT::DefaultValue:
    {
      clog_debug_vn(c.value(), field.d->path, "-----------------");
      field.d->default_value = c.value();
      break;
    }
    break;
    case CT::Property:
    case CT::Disjoint:
    case CT::Equals:
    case CT::LanguageIn:
    case CT::LessThan:
    case CT::LessThanOrEquals:
      break;
    case CT::MaxCount:
    {
      cres_try(field.d->maximum_count, c.value<quint64>());
      break;
    }
    case CT::MinCount:
    {
      cres_try(field.d->minimum_count, c.value<quint64>());
      break;
    }
    case CT::MaxExclusive:
    case CT::MinExclusive:
    case CT::MaxInclusive:
    case CT::MinInclusive:
    case CT::MaxLength:
    case CT::MinLength:
    case CT::QualifiedValue:
    case CT::Pattern:
    case CT::UniqueLang:
    case CT::And:
    case CT::Or:
    case CT::Not:
    case CT::XOne:
      return cres_failure("Invalid constraint type {}", int(c.type()));
    }
  }

  using Variety = FocusNodeDeclaration::Property::Variety;
  if(field.d->minimum_count or field.d->maximum_count)
  {
    if(field.d->maximum_count)
    {
      if(*field.d->maximum_count <= 1)
      {
        if(field.d->minimum_count.value_or(1) == 0)
        {
          field.d->variety = Variety::Optional;
        }
        else
        {
          field.d->variety = Variety::Atomic;
        }
      }
      else
      {
        field.d->variety = has_map ? Variety::Map : Variety::List;
      }
    }
    else
    {
      field.d->variety = has_map ? Variety::Map : Variety::List;
    }
  }
  else
  {
    field.d->variety = Variety::Atomic;
  }

  return cres_success<QPair<bool, FocusNodeDeclaration::Property>>({true, field});
}

cres_qresult<void> FocusNodeDeclarationsRegistryPrivate::loadDefinitions(const QUrl& _url)
{
  cres_try(knowSHACL::Definition def, knowSHACL::Definition::create(_url), ERR_MSG);
  QHash<knowCore::Uri, FocusNodeDeclaration> declarations_ns_uri;

  for(const knowSHACL::NodeShape& ns : def.nodes())
  {
    if(ns.types().contains(askcore_declaration::quantiy_value_shape))
    {
      for(const knowSHACL::Constraint& c : ns.constraints())
      {
        if(c.type() == knowSHACL::Constraint::Type::Property)
        {
          knowSHACL::PropertyShape ps = c.property();
          knowSHACL::Path p = ps.path();
          if(p.type() == knowSHACL::Path::Type::Predicate
             and p.predicate() == knowCore::Uris::qudt::unit)
          {
            for(const knowSHACL::Constraint& ps_c : ps.constraints())
            {
              if(ps_c.type() == knowSHACL::Constraint::Type::In)
              {
                cres_try(knowCore::ValueList list, ps_c.value<knowCore::ValueList>(), ERR_MSG);
                cres_try(uri2units[ns.uri()], list.values<knowCore::Uri>(), ERR_MSG);
              }
            }
          }
        }
      }
    }
    if(ns.types().contains(askcore_declaration::property_map_shape))
    {
      property_map_shape[ns.uri()] = askcore_datatype::literal;
    }
    if(ns.types().contains(askcore_declaration::focus_node_shape))
    {
      FocusNodeDeclaration decl;
      decl.d = new FocusNodeDeclaration::Private;

      for(const knowSHACL::Target& tar : ns.targets())
      {
        if(tar.type() == knowSHACL::Target::Type::Class and tar.uri() != ns.uri())
        {
          if(decl.d->datatype.isEmpty())
          {
            decl.d->datatype = tar.uri();
          }
          else
          {
            return cres_failure("Two targets where found for '{}': {} and {}", ns.uri(),
                                decl.d->datatype, tar.uri());
          }
        }
      }
      if(decl.d->datatype.isEmpty())
      {
        decl.d->datatype = ns.uri();
      }
      for(const knowCore::Uri& sub : ns.parentClasses())
      {
        FocusNodeDeclaration parentDeclaration;
        if(declarations.contains(sub))
        {
          parentDeclaration = declarations[sub];
        }
        else if(declarations_ns_uri.contains(sub))
        {
          parentDeclaration = declarations_ns_uri[sub];
        }
        else
        {
          return cres_failure("Cannot find parent class {} for {}", sub, ns.uri());
        }
        decl.d->fields.insert(parentDeclaration.d->fields);
        decl.d->inheritance.append(parentDeclaration.d->inheritance);
        decl.d->inheritance.append(parentDeclaration.d->datatype);
      }

      for(const knowSHACL::Constraint& c : ns.constraints())
      {
        switch(c.type())
        {
        case knowSHACL::Constraint::Type::Property:
        {
          knowSHACL::PropertyShape ps = c.property();
          if(ps.path().type() == knowSHACL::Path::Type::Predicate
             and decl.d->fields.contains(ps.path().predicate()))
          {
            FocusNodeDeclaration::Property& fo = decl.d->fields[ps.path().predicate()];
            if(fo.d->ref > 1)
            {
              fo.d = new FocusNodeDeclaration::Property::Private(*fo.d);
            }
            using pair_b_f = QPair<bool, FocusNodeDeclaration::Property>;
            cres_try(pair_b_f f, createField(c), ERR_MSG);
            if(not f.second.d->value.isEmpty())
            {
              fo.d->value = f.second.d->value;
            }
            break;
          }
        }
          Q_FALLTHROUGH();
        default:
        {
          using pair_b_t = QPair<bool, FocusNodeDeclaration::Property>;
          cres_try(pair_b_t f, createField(c), ERR_MSG);
          if(f.first)
          {
            decl.d->fields[f.second.path()] = f.second;
          }
        }
        }
      }
      declarations[decl.d->datatype] = decl;
      declarations_ns_uri[ns.uri()] = decl;
    }
  }
  if(declarations.isEmpty())
  {
    return cres_failure("No 'http://askco.re/declaration#focus_node_shape' was defined.");
  }
  return cres_success();
}

FocusNodeDeclarationsRegistry::FocusNodeDeclarationsRegistry() {}

FocusNodeDeclarationsRegistry::~FocusNodeDeclarationsRegistry() {}

void FocusNodeDeclarationsRegistry::loadDelayedDefinitions()
{
  for(const QUrl& u : FocusNodeDeclarationsRegistryPrivate::s_instance()->delayed_declarations)
  {
    loadDefinitions(u).expect_success();
  }
  FocusNodeDeclarationsRegistryPrivate::s_instance()->delayed_declarations.clear();
}

void FocusNodeDeclarationsRegistry::delayedLoadDefinitions(const QUrl& _definition_file)
{
  FocusNodeDeclarationsRegistryPrivate::s_instance()->delayed_declarations.append(_definition_file);
}

cres_qresult<void> FocusNodeDeclarationsRegistry::loadDefinitions(const QUrl& _definition_file)
{
  return FocusNodeDeclarationsRegistryPrivate::s_instance()->loadDefinitions(_definition_file);
}

cres_qresult<FocusNodeDeclaration>
  FocusNodeDeclarationsRegistry::declaration(const knowCore::Uri& _type_uri)
{
  loadDelayedDefinitions();
  if(FocusNodeDeclarationsRegistryPrivate::s_instance()->declarations.contains(_type_uri))
  {
    return cres_success(
      FocusNodeDeclarationsRegistryPrivate::s_instance()->declarations.value(_type_uri));
  }
  else
  {
    return cres_failure("Unknown type {} possibles are {}", _type_uri,
                        FocusNodeDeclarationsRegistryPrivate::s_instance()->declarations.keys());
  }
}

cres_qresult<FocusNodeDeclaration> FocusNodeDeclarationsRegistry::byConstantField(
  const knowCore::Uri& _type_uri, const knowCore::Uri& _field_uri, const knowRDF::Literal& _literal)
{
  loadDelayedDefinitions();
  for(auto it = FocusNodeDeclarationsRegistryPrivate::s_instance()->declarations.begin();
      it != FocusNodeDeclarationsRegistryPrivate::s_instance()->declarations.end(); ++it)
  {
    if(it->inherits(_type_uri) and it->hasField(_field_uri))
    {
      cres_try(FocusNodeDeclaration::Property f, it->field(_field_uri));
      if(f.isConstant() and f.value() == _literal)
      {
        return cres_success(it.value());
      }
    }
  }
  return cres_failure("No type inherits '{}' with a constant field '{}' equals to '{}'", _type_uri,
                      _field_uri, _literal);
}

cres_qresult<FocusNodeDeclaration::Property>
  FocusNodeDeclarationsRegistry::field(const knowCore::Uri& _type_uri,
                                       const knowCore::Uri& _field_uri)
{
  cres_try(FocusNodeDeclaration decl, declaration(_type_uri));
  return decl.field(_field_uri);
}
