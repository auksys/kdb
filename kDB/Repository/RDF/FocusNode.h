#pragma once

#include <QCborMap>

#include <knowCore/ValueList.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/Transaction.h>

#include "FocusNodeDeclaration.h"

namespace kDB::Repository::RDF
{
  /**
   * \ingroup kDB_Repository
   *
   * FocusNode is a convenient class that treat a uri in a RDF graph as the root of an object with
   * properties.
   *
   * For instance for the following RDF Graph:
   * @code
   * <a> a <object_type> ;
   *     <property_1> "property value 1" ;
   *     <property_2> "property value 2" .
   * @endcode
   *
   * The property values can be manipulated by creating:
   * @code
   * FocusNode a("graph_uri"_kCu, "a"_kCu, Registry::get("object_type"));
   * @endcode
   *
   */
  class FocusNode
  {
    friend class FocusNodeCollection;
  public:
    FocusNode();
    FocusNode(const FocusNode& _rhs);
    FocusNode& operator=(const FocusNode& _rhs);
    FocusNode(const kDB::Repository::Connection& _connection, const knowCore::Uri& _graph,
              const knowCore::Uri& _object_uri, const FocusNodeDeclaration& _declaration);
    ~FocusNode();
    bool operator==(const FocusNode& _rhs) const;
    /**
     * @return true if this focus node exists
     */
    cres_qresult<bool> exists() const;
    /**
     * @return true if this focus node is valid and exists.
     */
    bool isValid() const;
    /**
     * @return the declaration defining this focus node.
     */
    FocusNodeDeclaration declaration() const;
    /**
     * @return the uri of the focus node, i.e. the root of the properties
     */
    knowCore::Uri uri() const;
    /**
     * @return the uri of the graph which contains the focus node, i.e. the root of the properties
     */
    knowCore::Uri graph() const;
    /**
     * @return the connection
     */
    kDB::Repository::Connection connection() const;
    /**
     * @return if the given property is set
     */
    cres_qresult<bool> hasProperty(const knowCore::Uri& _property,
                                   const Transaction& _transaction = Transaction()) const;
    /**
     * @return a value for the given property
     */
    cres_qresult<knowCore::Value> property(const knowCore::Uri& _property,
                                           const Transaction& _transaction = Transaction()) const;
    /**
     * @return a value for the given property
     */
    template<typename _T_>
      requires(not std::is_base_of_v<knowCore::Value, _T_>)
    cres_qresult<_T_> property(const knowCore::Uri& _property) const;
    /**
     * Set the value for the given property
     */
    cres_qresult<void> setProperty(const knowCore::Uri& _property, const knowCore::Value& _value,
                                   const Transaction& _transaction = Transaction()) const;
    /**
     * Set the value for the given property
     */
    cres_qresult<void> addPropertyToList(const knowCore::Uri& _property,
                                         const knowCore::Value& _value,
                                         const Transaction& _transaction = Transaction()) const;
    /**
     * Set the value for the given property
     */
    cres_qresult<void>
      removePropertyFromList(const knowCore::Uri& _property, const knowCore::Value& _value,
                             const Transaction& _transaction = Transaction()) const;
    /**
     * Get the values for the given list property
     */
    cres_qresult<knowCore::ValueList> propertyList(const knowCore::Uri& _property,
                                                   const Transaction& _transaction
                                                   = Transaction()) const;

    /**
     * Set the value for the given property
     */
    cres_qresult<void> setPropertyInMap(const knowCore::Uri& _property, const knowCore::Uri& _key,
                                        const knowCore::Value& _value,
                                        const Transaction& _transaction = Transaction()) const;
    /**
     * Get the values for the given map property
     */
    cres_qresult<knowCore::ValueHash> propertyMap(const knowCore::Uri& _property,
                                                  const Transaction& _transaction
                                                  = Transaction()) const;
    /**
     * Get the values for one element in the given map property
     */
    cres_qresult<knowCore::Value>
      propertyMapValue(const knowCore::Uri& _property, const knowCore::Uri& _key,
                       const Transaction& _transaction = Transaction()) const;

    /**
     * Set the value for the given property
     */
    template<typename _T_>
      requires(not std::is_base_of_v<knowCore::Value, _T_>)
    cres_qresult<void> setProperty(const knowCore::Uri& _property, const _T_& _value,
                                   const Transaction& _transaction = Transaction()) const
    {
      return setProperty(_property, knowCore::Value::fromValue(_value), _transaction);
    }
    /**
     * @return a cbor map that represents the value.
     */
    cres_qresult<QCborMap> toCborMap() const;
    /**
     * @return a cbor map that represents the value.
     */
    static cres_qresult<FocusNode> fromCborMap(const kDB::Repository::Connection& _connection,
                                               const knowCore::Uri& _graph, const QCborMap& _map);
  private:
    /**
     * Set the value for the given property
     */
    cres_qresult<void> setProperty(const knowCore::Uri& _property, const knowCore::Value& _value,
                                   const Transaction& _transaction,
                                   bool _allow_constant_changes) const;
    cres_qresult<void> addPropertyToList(const knowCore::Uri& _property,
                                         const knowCore::Value& _value,
                                         const Transaction& _transaction,
                                         bool _allow_constant_changes) const;
    cres_qresult<void> setPropertyInMap(const knowCore::Uri& _property, const knowCore::Uri& _key,
                                        const knowCore::Value& _value,
                                        const Transaction& _transaction,
                                        bool _allow_constant_changes) const;

    /**
     * Create a new \ref FocusNode of uri type \p _typeUri  and add it to this collection.
     */
    static cres_qresult<FocusNode> create(const kDB::Repository::QueryConnectionInfo& _connection,
                                          const knowCore::Uri& _graph_uri,
                                          const knowCore::Uri& _typeUri,
                                          const knowCore::ValueHash& _properties,
                                          const knowCore::Uri& _focusNodeUri);
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
  template<typename _T_>
    requires(not std::is_base_of_v<knowCore::Value, _T_>)
  inline cres_qresult<_T_> FocusNode::property(const knowCore::Uri& _property) const
  {
    cres_try(knowCore::Value value, property(_property));
    return value.value<_T_>();
  }

  template<typename _TDerive_>
  class FocusNodeWrapper
  {
  protected:
    FocusNodeWrapper() {}
    FocusNodeWrapper(const FocusNodeWrapper& _rhs) : m_focus_node(_rhs.m_focus_node) {}
  public:
    _TDerive_& operator=(const _TDerive_& _rhs)
    {
      m_focus_node = _rhs.m_focus_node;
      return static_cast<_TDerive_&>(*this);
    }

    bool operator==(const FocusNodeWrapper& _rhs) const
    {
      return m_focus_node == _rhs.m_focus_node;
    }
  protected:
    void setFocusNode(const FocusNode& _fn) { m_focus_node = _fn; }
    const FocusNode& focusNodeRef() const { return m_focus_node; }
    FocusNode& focusNodeRef() { return m_focus_node; }
  public:
    /**
     * @return true if this focus node exists
     */
    cres_qresult<bool> exists() const { return m_focus_node.exists(); }
    knowCore::Uri graph() const { return m_focus_node.graph(); }
    knowCore::Uri uri() const { return m_focus_node.uri(); }
    knowCore::Uri type() const { return m_focus_node.declaration().type(); }
    /**
     * @return the connection
     */
    kDB::Repository::Connection connection() const { return m_focus_node.connection(); }
    /**
     * @return if the given property is set
     */
    cres_qresult<bool> hasProperty(const knowCore::Uri& _property) const
    {
      return m_focus_node.hasProperty(_property);
    }
    /**
     * @return a value for the given property
     */
    cres_qresult<knowCore::Value> property(const knowCore::Uri& _property) const
    {
      return m_focus_node.property(_property);
    }
    /**
     * @return a value for the given property
     */
    template<typename _T_>
      requires(not std::is_base_of_v<knowCore::Value, _T_>)
    cres_qresult<_T_> property(const knowCore::Uri& _property) const
    {
      return m_focus_node.property<_T_>(_property);
    }
    /**
     * Set the value for the given property
     */
    cres_qresult<void> setProperty(const knowCore::Uri& _property,
                                   const knowCore::Value& _value) const
    {
      return m_focus_node.setProperty(_property, _value);
    }
    /**
     * Set the value for the given property
     */
    template<typename _T_>
      requires(not std::is_base_of_v<knowCore::Value, _T_>)
    cres_qresult<void> setProperty(const knowCore::Uri& _property, const _T_& _value) const
    {
      return m_focus_node.setProperty(_property, _value);
    }
    /**
     * Set the value for the given property
     */
    cres_qresult<void> addPropertyToList(const knowCore::Uri& _property,
                                         const knowCore::Value& _value,
                                         const Transaction& _transaction = Transaction()) const
    {
      return m_focus_node.addPropertyToList(_property, _value, _transaction);
    }
    /**
     * Set the value for the given property
     */
    cres_qresult<void> removePropertyFromList(const knowCore::Uri& _property,
                                              const knowCore::Value& _value,
                                              const Transaction& _transaction = Transaction()) const
    {
      return m_focus_node.removePropertyFromList(_property, _value, _transaction);
    }
    /**
     * Get the values for the given list property
     */
    cres_qresult<knowCore::ValueList> propertyList(const knowCore::Uri& _property,
                                                   const Transaction& _transaction
                                                   = Transaction()) const
    {
      return m_focus_node.propertyList(_property, _transaction);
    }

    /**
     * Set the value for the given property
     */
    cres_qresult<void> setPropertyInMap(const knowCore::Uri& _property, const knowCore::Uri& _key,
                                        const knowCore::Value& _value,
                                        const Transaction& _transaction = Transaction()) const
    {
      return m_focus_node.setPropertyInMap(_property, _key, _value, _transaction);
    }
    /**
     * Get the values for the given map property
     */
    cres_qresult<knowCore::ValueHash> propertyMap(const knowCore::Uri& _property,
                                                  const Transaction& _transaction
                                                  = Transaction()) const
    {
      return m_focus_node.propertyMap(_property, _transaction);
    }
    /**
     * Get the values for one element in the given map property
     */
    cres_qresult<knowCore::Value>
      propertyMapValue(const knowCore::Uri& _property, const knowCore::Uri& _key,
                       const Transaction& _transaction = Transaction()) const
    {
      return m_focus_node.propertyMapValue(_property, _key, _transaction);
    }
    /**
     * To a cbor map.
     */
    cres_qresult<QCborMap> toCborMap() const { return m_focus_node.toCborMap(); }
  private:
    FocusNode m_focus_node;
  };
} // namespace kDB::Repository::RDF
