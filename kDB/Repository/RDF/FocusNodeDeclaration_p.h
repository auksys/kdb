#include "FocusNodeDeclaration.h"

#include <knowCore/UriList.h>
#include <knowCore/ValueList.h>

#include <knowRDF/Literal.h>

namespace kDB::Repository::RDF
{
  struct FocusNodeDeclaration::Property::Private : public QSharedData
  {
    bool isConstant = false;
    Variety variety;
    Direction direction;
    std::optional<std::size_t> minimum_count, maximum_count;
    QString name;
    knowCore::Uri path, datatype;
    knowRDF::Literal value, default_value;
    std::optional<knowCore::UriList> acceptedUnits;
    std::optional<knowCore::ValueList> acceptedLiterals;
  };

  // BEGIN Declaration::Private
  struct FocusNodeDeclaration::Private : public QSharedData
  {
    QHash<knowCore::Uri, Property> fields;
    knowCore::Uri datatype;
    QList<knowCore::Uri> inheritance;
  };
  // END Declaration::Private
} // namespace kDB::Repository::RDF
