#include "FocusNodeDeclaration.h"

#include <clog>
#include <cres>

namespace kDB::Repository::RDF
{
  namespace details
  {
    class FocusNodeDeclarationsRegister;
  }
  /**
   * Registry with all the focus node declarations
   */
  class FocusNodeDeclarationsRegistry
  {
    friend class details::FocusNodeDeclarationsRegister;
    FocusNodeDeclarationsRegistry();
    ~FocusNodeDeclarationsRegistry();
    static void delayedLoadDefinitions(const QUrl& _definition_file);
    static void loadDelayedDefinitions();
  public:
    static cres_qresult<void> loadDefinitions(const QUrl& _definition_file);
    static cres_qresult<FocusNodeDeclaration> declaration(const knowCore::Uri& _type_uri);
    /**
     * Search for a focus node declaration that inherits the given @p _type_uri where the @p
     * _field_uri is a constant equal to the given @p _literal.
     */
    static cres_qresult<FocusNodeDeclaration> byConstantField(const knowCore::Uri& _type_uri,
                                                              const knowCore::Uri& _field_uri,
                                                              const knowRDF::Literal& _literal);
    static cres_qresult<FocusNodeDeclaration::Property> field(const knowCore::Uri& _type_uri,
                                                              const knowCore::Uri& _field_uri);
  };
  namespace details
  {
    /**
     * Convenient class for loading definitions statically.
     * Do not use directly, use KDB_REPOSITORY_REGISTER_FOCUS_NODE_DECLARATIONS instead.
     */

    class FocusNodeDeclarationsRegister
    {
    public:
      FocusNodeDeclarationsRegister(const QUrl& _definition_file)
      {
        FocusNodeDeclarationsRegistry::delayedLoadDefinitions(_definition_file);
      }
    };
  } // namespace details
} // namespace kDB::Repository::RDF

/// Macro to statically load declarations from a file.
#define KDB_REPOSITORY_REGISTER_FOCUS_NODE_DECLARATIONS(_file_name_)                               \
  namespace                                                                                        \
  {                                                                                                \
    static kDB::Repository::RDF::details::FocusNodeDeclarationsRegister                            \
      __KNOWCORE_UNIQUE_STATIC_NAME(focus_node_declarations_register)(QUrl(_file_name_));          \
  }
