#include "FocusNodeDeclaration_p.h"

using namespace kDB::Repository::RDF;

// BEGIN Declaration::Field

using Property = FocusNodeDeclaration::Property;

Property::Property() {}

Property::Property(const Property& _rhs) : d(_rhs.d) {}
Property& Property::operator=(const Property& _rhs)
{
  d = _rhs.d;
  return *this;
}

Property::~Property() {}

bool Property::operator==(const Property& _rhs)
{
  return d->name == _rhs.d->name and d->path == _rhs.d->path and d->datatype == _rhs.d->datatype
         and d->isConstant == _rhs.d->isConstant and d->value == _rhs.d->value
         and d->variety == _rhs.d->variety and d->minimum_count == _rhs.d->minimum_count
         and d->maximum_count == _rhs.d->maximum_count
         and ((not d->acceptedUnits and not _rhs.d->acceptedUnits)
              or *d->acceptedUnits == *d->acceptedUnits)
         and ((not d->acceptedLiterals and not _rhs.d->acceptedLiterals)
              or *d->acceptedLiterals == *d->acceptedLiterals);
}

bool Property::operator!=(const Property& _rhs) { return not operator==(_rhs); }

bool Property::isConstant() const { return d->isConstant; }

Property::Variety Property::variety() const { return d->variety; }

Property::Direction Property::direction() const { return d->direction; }

std::optional<std::size_t> Property::minimum() const { return d->minimum_count; }

std::optional<std::size_t> Property::maximum() const { return d->maximum_count; }

QString Property::name() const { return d->name; }

knowCore::Uri Property::path() const { return d->path; }

knowCore::Uri Property::datatype() const { return d->datatype; }

knowRDF::Literal Property::value() const { return d->value; }

knowRDF::Literal Property::defaultValue() const { return d->default_value; }

std::optional<QList<knowCore::Uri>> Property::acceptedUnits() const { return d->acceptedUnits; }

std::optional<knowCore::ValueList> Property::acceptedValues() const { return d->acceptedLiterals; }

// END Declaration::Field

// BEGIN Declaration

FocusNodeDeclaration::FocusNodeDeclaration() {}
FocusNodeDeclaration::FocusNodeDeclaration(const FocusNodeDeclaration& _rhs) : d(_rhs.d) {}

FocusNodeDeclaration& FocusNodeDeclaration::operator=(const FocusNodeDeclaration& _rhs)
{
  d = _rhs.d;
  return *this;
}

FocusNodeDeclaration::~FocusNodeDeclaration() {}

bool FocusNodeDeclaration::inherits(const knowCore::Uri& _uri)
{
  return d->inheritance.contains(_uri);
}

QList<knowCore::Uri> FocusNodeDeclaration::inheritedTypes() const { return d->inheritance; }

knowCore::Uri FocusNodeDeclaration::type() const { return d->datatype; }

bool FocusNodeDeclaration::hasField(const knowCore::Uri& _uri) { return d->fields.contains(_uri); }

cres_qresult<Property> FocusNodeDeclaration::field(const knowCore::Uri& _uri)
{
  if(d->fields.contains(_uri))
  {
    return cres_success(d->fields.value(_uri));
  }
  else
  {
    return cres_failure("Unknown field '{}' for object of type '{}'", _uri, d->datatype);
  }
}

QList<Property> FocusNodeDeclaration::fields() const { return d->fields.values(); }

// END Declaration
