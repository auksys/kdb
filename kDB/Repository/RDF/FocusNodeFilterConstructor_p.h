#include <knowCore/ConstrainedValue.h>
#include <knowCore/QuantityValue.h>
#include <knowCore/TypeDefinitions.h>

#include <kDB/SPARQL/Algebra/Builders.h>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/qudt.h>

#include <knowGIS/Uris/geo.h>
#include <knowGIS/Uris/geof.h>

#include "FocusNodeDeclarationsRegistry.h"

namespace
{

  cres_qresult<QList<kDB::SPARQL::Algebra::NodeCSP>> focus_node_filter_constructor(
    const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
    QHash<QString, QList<kDB::SPARQL::Algebra::NodeCSP>>* path2node,
    kDB::SPARQL::Algebra::Builders::Variable* uri_variable, const QList<knowCore::Uri>& datatypes,
    kDB::SPARQL::Algebra::Builders::TriplesList* triples_pattern, double _intersectsPrecision)
  {
    namespace SA = kDB::SPARQL::Algebra;
    namespace SB = kDB::SPARQL::Algebra::Builders;
    using askcore_datatype = knowCore::Uris::askcore_datatype;
    using qudt = knowCore::Uris::qudt;
    using askcore_sparql_functions_extra = knowCore::Uris::askcore_sparql_functions_extra;

    // BEGIN Construct filters
    QList<SA::NodeCSP> filters;
    for(int i = 0; i < _constraints.size(); ++i)
    {
      knowCore::Uri path = _constraints[i].first;

      SA::NodeCSP lhs_node_value;
      SA::NodeCSP lhs_node_unit;

      if((*path2node).contains(path))
      {
        QList<SA::NodeCSP> nodes = (*path2node)[path];
        lhs_node_value = nodes[0];
        if(nodes.size() > 1)
        {
          lhs_node_unit = nodes[1];
        }
      }
      else
      {
        kDB::Repository::RDF::FocusNodeDeclaration::Property field;
        for(int i = 0; i < datatypes.size(); ++i)
        {
          cres_try(kDB::Repository::RDF::FocusNodeDeclaration declaration,
                   kDB::Repository::RDF::FocusNodeDeclarationsRegistry::declaration(datatypes[i]));
          cres_try(kDB::Repository::RDF::FocusNodeDeclaration::Property field_i,
                   declaration.field(path),
                   message("In accessing property {} for type {}: {}", path, datatypes[i]));
          if(i == 0)
          {
            field = field_i;
          }
          else if(field != field_i)
          {
            return cres_failure("Property {} has incompatible field in type {} and type {}", path,
                                datatypes[0], datatypes[i]);
          }
        }

        if(field.datatype() == askcore_datatype::quantityDecimal)
        {
          SB::Variable value_variable_value_i(clog_qt::qformat("?{}_value", i));
          SB::Variable value_variable_unit_i(clog_qt::qformat("?{}_unit", i));

          SB::BlankNode object_node;

          triples_pattern->append((*uri_variable), field.path(), object_node);
          triples_pattern->append(object_node, qudt::value, value_variable_value_i);
          triples_pattern->append(object_node, qudt::unit, value_variable_unit_i);

          lhs_node_value = value_variable_value_i;
          lhs_node_unit = value_variable_unit_i;
          (*path2node)[path] = {lhs_node_value, lhs_node_unit};
        }
        else
        {
          SB::Variable value_variable_i(clog_qt::qformat("?{}", i));
          triples_pattern->append((*uri_variable), field.path(), value_variable_i);
          lhs_node_value = value_variable_i;
          (*path2node)[path] = {lhs_node_value};
        }
      }

      // Build the filter
      for(knowCore::ConstrainedValue::Constraint constraint : _constraints[i].second.constraints())
      {

        SA::NodeCSP lhs_node;
        SA::NodeCSP rhs_node;
        if(lhs_node_unit)
        {
          if(knowCore::ValueIs<knowCore::QuantityNumber> qn = constraint.value)
          {
            lhs_node
              = SB::FunctionCall()
                  .name(askcore_sparql_functions_extra::convertQuantityValue)
                  .parameters(lhs_node_value, lhs_node_unit, SB::Term().term(qn->unit().uri()));
            rhs_node = SB::Value().value(knowRDF::Literal::fromValue(qn->value()));
          }
          else
          {
            return cres_failure("Constraint for property '{}' should be a quantity number", path);
          }
        }
        else
        {
          lhs_node = lhs_node_value;
          if(constraint.value.is<knowCore::Uri>())
          {
            rhs_node = SB::Term().term(constraint.value.value<knowCore::Uri>().expect_success());
          }
          else
          {
            rhs_node = SB::Value().value(constraint.value);
          }
        }
        switch(constraint.type)
        {
        case knowCore::ConstrainedValue::Type::Equal:
          filters.append(SB::RelationalEqual().left(lhs_node).right(rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::Different:
          filters.append(SB::RelationalDifferent().left(lhs_node).right(rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::InferiorEqual:
          filters.append(SB::RelationalInferiorEqual().left(lhs_node).right(rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::Inferior:
          filters.append(SB::RelationalInferior().left(lhs_node).right(rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::SuperiorEqual:
          filters.append(SB::RelationalSuperiorEqual().left(lhs_node).right(rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::Superior:
          filters.append(SB::RelationalSuperior().left(lhs_node).right(rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::GeoOverlaps:
          filters.append(SB::FunctionCall()
                           .name(knowGIS::Uris::geof::sfOverlaps)
                           .parameters(lhs_node, rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::GeoIntersects:
          if(_intersectsPrecision > 0.0)
          {
            filters.append(
              SB::FunctionCall()
                .name(knowGIS::Uris::geof::sfIntersects)
                .parameters(lhs_node, rhs_node, knowRDF::Literal::fromValue(_intersectsPrecision)));
          }
          else
          {
            filters.append(SB::FunctionCall()
                             .name(knowGIS::Uris::geof::sfIntersects)
                             .parameters(lhs_node, rhs_node));
          }
          break;
        case knowCore::ConstrainedValue::Type::GeoWithin:
          filters.append(
            SB::FunctionCall().name(knowGIS::Uris::geof::sfWithin).parameters(lhs_node, rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::GeoContains:
          filters.append(
            SB::FunctionCall().name(knowGIS::Uris::geof::sfWithin).parameters(lhs_node, rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::GeoTouches:
          filters.append(
            SB::FunctionCall().name(knowGIS::Uris::geof::sfTouches).parameters(lhs_node, rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::GeoDisjoint:
          filters.append(SB::FunctionCall()
                           .name(knowGIS::Uris::geof::sfDisjoint)
                           .parameters(lhs_node, rhs_node));
          break;
        case knowCore::ConstrainedValue::Type::In:
        case knowCore::ConstrainedValue::Type::NotIn:
        case knowCore::ConstrainedValue::Type::Contains:
        case knowCore::ConstrainedValue::Type::NotContains:
          return cres_failure("In operator not supported.");
        }
      }
    }
    // END Construct filters
    return cres_success(filters);
  }
} // namespace
