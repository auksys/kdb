#include "FocusNodeCollection.h"

#include <knowCore/UriList.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/askcore_focus_node.h>
#include <knowCore/Uris/askcore_graph.h>
#include <knowCore/Uris/rdf.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/Logging.h>
#include <kDB/Repository/PersistentDatasetsUnion.h>
#include <kDB/Repository/QueryConnectionInfo.h>
#include <kDB/Repository/TripleStore.h>

#include "FocusNode.h"
#include "FocusNodeFilterConstructor_p.h"
#include "FocusNodeQueries.h"

using namespace kDB::Repository::RDF;

using askcore_db_rdf = knowCore::Uris::askcore_db_rdf;
using askcore_graph = knowCore::Uris::askcore_graph;
using askcore_focus_node = knowCore::Uris::askcore_focus_node;

KDB_REPOSITORY_REGISTER_FOCUS_NODE_DECLARATIONS("qrc:///kdb/repository/data/collection_shacl.ttl");

struct FocusNodeCollection::Private : public QSharedData
{
  FocusNode focus_node;
};

cres_qresult<void> FocusNodeCollection::initialise(const Connection& _connection)
{
  cres_try(RDF::FocusNodeDeclaration collection_declaration,
           RDF::FocusNodeDeclarationsRegistry::declaration(askcore_focus_node::collection));

  // Check for existence of askcore_graph::collections
  cres_try(bool exists_agc, RDF::FocusNode(_connection, askcore_graph::collections,
                                           askcore_graph::collections, collection_declaration)
                              .exists());
  if(not exists_agc)
  {
    cres_try(cres_ignore,
             RDF::FocusNode::create(
               _connection, askcore_graph::collections, askcore_focus_node::collection,
               {askcore_focus_node::collection_type, askcore_focus_node::collections_collection,
                askcore_focus_node::contains, knowCore::UriList({askcore_focus_node::collection}),
                askcore_focus_node::dataset_type, askcore_db_rdf::triple_store},
               askcore_graph::collections));
  }

  // Check for existence of askcore_graph::all_focus_nodes
  cres_try(bool exists_afn, RDF::FocusNode(_connection, askcore_graph::collections,
                                           askcore_graph::all_focus_nodes, collection_declaration)
                              .exists());
  if(not exists_afn)
  {
    cres_try(cres_ignore,
             RDF::FocusNode::create(
               _connection, askcore_graph::collections, askcore_focus_node::collection,
               {askcore_focus_node::collection_type, askcore_focus_node::focus_nodes_collection,
                askcore_focus_node::contains, knowCore::UriList({askcore_focus_node::focus_node}),
                askcore_focus_node::dataset_type, askcore_db_rdf::dataset},
               askcore_graph::all_focus_nodes));
  }
  return cres_success();
}

FocusNodeCollection::FocusNodeCollection() : d(new Private) {}

FocusNodeCollection::FocusNodeCollection(const FocusNodeCollection& _rhs) : d(_rhs.d) {}

FocusNodeCollection FocusNodeCollection::operator=(const FocusNodeCollection& _rhs)
{
  d = _rhs.d;
  return *this;
}
FocusNodeCollection::~FocusNodeCollection() {}

bool FocusNodeCollection::operator==(const FocusNodeCollection& _collection) const
{
  return d == _collection.d or d->focus_node == _collection.d->focus_node;
}

cres_qresult<FocusNodeCollection>
  FocusNodeCollection::get(const kDB::Repository::Connection& _connection,
                           const knowCore::Uri& _collection_uri)
{
  FocusNodeCollection c;
  cres_try(FocusNodeDeclaration declaration,
           FocusNodeDeclarationsRegistry::declaration(askcore_focus_node::collection));
  c.d->focus_node
    = FocusNode(_connection, askcore_graph::collections, _collection_uri, declaration);
  cres_try(bool exist, c.d->focus_node.exists());
  if(exist)
  {
    return cres_success(c);
  }
  else
  {
    return cres_failure("No collection named {}.", _collection_uri);
  }
}

cres_qresult<FocusNodeCollection>
  FocusNodeCollection::create(const kDB::Repository::Connection& _connection,
                              const knowCore::Uri& _graph, const knowCore::Uri& _collection_type,
                              const knowCore::UriList& _contained_types)
{
  if(_graph.isEmpty())
  {
    return cresError("Cannot create a collection with an empty name.");
  }
  FocusNodeCollection c;
  Transaction t(_connection);
  // cres_try(c.d->focus_node,
  cres_qresult<FocusNode> fn = FocusNode::create(
    t, askcore_graph::collections, askcore_focus_node::collection,
    {askcore_focus_node::contains, _contained_types, askcore_focus_node::dataset_type,
     askcore_db_rdf::triple_store, askcore_focus_node::collection_type, _collection_type},
    _graph);
  if(fn.is_successful())
  {
    // Update all focus nodes collection
    cres_try(kDB::Repository::PersistentDatasetsUnion all_focus_nodes_union,
             _connection.graphsManager()->getOrCreateUnion(askcore_graph::all_focus_nodes),
             on_failure(t.rollback()));
    cres_try(kDB::Repository::TripleStore ts_graph,
             _connection.graphsManager()->createTripleStore(_graph, t), on_failure(t.rollback()));
    all_focus_nodes_union.add(ts_graph, t);

    // Update all virtual collection of that type
    FocusNodeCollection allCollections = FocusNodeCollection::allCollections(_connection);
    cres_try(QList<FocusNode> focus_nodes,
             allCollections.focusNodes(
               {askcore_focus_node::collection},
               {{knowCore::Uri(askcore_focus_node::collection_type),
                 knowCore::ConstrainedValue().apply(_collection_type,
                                                    knowCore::ConstrainedValue::Type::Equal)},
                {knowCore::Uri(askcore_focus_node::dataset_type),
                 knowCore::ConstrainedValue().apply(askcore_db_rdf::dataset,
                                                    knowCore::ConstrainedValue::Type::Equal)}}));
    for(const FocusNode& fn : focus_nodes)
    {
      cres_try(kDB::Repository::PersistentDatasetsUnion fn_union,
               _connection.graphsManager()->getOrCreateUnion(fn.uri()), on_failure(t.rollback()));
      fn_union.add(ts_graph, t);
    }
    t.commit();
    c.d->focus_node = fn.get_value();
    return cres_success(c);
  }
  else
  {
    t.rollback();
    return fn.get_error();
  }
}

cres_qresult<FocusNodeCollection> FocusNodeCollection::getOrCreate(
  const kDB::Repository::Connection& _connection, const knowCore::Uri& _graph,
  const knowCore::Uri& _collection_type, const knowCore::UriList& _contained_types)
{
  cres_qresult<FocusNodeCollection> c = get(_connection, _graph);
  if(c.is_successful())
  {
    cres_try(knowCore::Uri collection_type, c.get_value().collectionType());
    if(collection_type == _collection_type)
    {
      clog_debug_vn(c.get_value().isValid(), c.get_value().uri());
      return c;
    }
    else
    {
      return cres_failure("Collection {} is not of type {} but {}", _graph, _collection_type,
                          collection_type);
    }
  }
  else
  {
    return create(_connection, _graph, _collection_type, _contained_types);
  }
}

FocusNodeCollection
  FocusNodeCollection::allCollections(const kDB::Repository::Connection& _connection)
{
  FocusNodeCollection col;
  col.d->focus_node = FocusNode(
    _connection, askcore_graph::collections, askcore_graph::collections,
    FocusNodeDeclarationsRegistry::declaration(askcore_focus_node::collection).expect_success());
  return col;
}

FocusNodeCollection
  FocusNodeCollection::allFocusNodes(const kDB::Repository::Connection& _connection)
{
  FocusNodeCollection col;
  col.d->focus_node = FocusNode(
    _connection, askcore_graph::collections, askcore_graph::all_focus_nodes,
    FocusNodeDeclarationsRegistry::declaration(askcore_focus_node::collection).expect_success());
  return col;
}

cres_qresult<knowCore::UriList>
  FocusNodeCollection::allCollectionUris(const kDB::Repository::QueryConnectionInfo& _connection,
                                         const knowCore::Uri& _focus_node_type)
{
  FocusNodeQueries::FocusNodeCollectionSelectAllByType::Results r
    = FocusNodeQueries::FocusNodeCollectionSelectAllByType::execute(
      _connection, askcore_graph::collections, _focus_node_type);
  if(r.success)
  {
    knowCore::UriList uris;
    for(qsizetype i = 0; i < r.data.size(); ++i)
    {
      uris.append(r.data[i].uri());
    }
    return cres_success(uris);
  }
  else
  {
    return cres_failure("Failure in listing all collections of focus node: '{}'.", r.errorMessage);
  }
}

cres_qresult<knowCore::UriList> FocusNodeCollection::allCollectionUrisWith(
  const kDB::Repository::QueryConnectionInfo& _connection, const knowCore::Uri& _focus_node)
{

  FocusNodeQueries::FocusNodeSelectCollections::Results r
    = FocusNodeQueries::FocusNodeSelectCollections::execute(_connection, _focus_node);
  if(r.success)
  {
    knowCore::UriList graphs;
    for(const FocusNodeQueries::FocusNodeSelectCollections::Result& row : r.data)
    {
      graphs.append(row.graphUri());
    }
    return cres_success(graphs);
  }
  else
  {
    return cres_failure("Failed to find all the datasets document holding '{}' with error {}",
                        _focus_node, r.errorMessage);
  }
}

cres_qresult<void> FocusNodeCollection::registerCollection(
  const kDB::Repository::QueryConnectionInfo& _connection, const knowCore::Uri& _view_graph,
  const knowCore::Uri& _collection_type, const knowCore::UriList& _contained_types)
{
  cres_try(FocusNodeDeclaration declaration,
           FocusNodeDeclarationsRegistry::declaration(askcore_focus_node::collection));
  FocusNode fn(_connection.connection(), askcore_graph::collections, _view_graph, declaration);
  cres_try(bool exists, fn.exists());
  if(exists)
  {
    cres_try(knowCore::Uri uri, fn.property<knowCore::Uri>(askcore_focus_node::dataset_type));
    if(uri == askcore_db_rdf::dataset)
    {
      return cres_success();
    }
    else
    {
      return cres_failure("Collection already exists with type {} and not dataset.", uri);
    }
  }
  else
  {
    cres_try(cres_ignore, _connection.connection().graphsManager()->getOrCreateUnion(_view_graph));
    cres_try(cres_ignore, FocusNode::create(
                            _connection, askcore_graph::collections, askcore_focus_node::collection,
                            {askcore_focus_node::collection_type, _collection_type,
                             askcore_focus_node::dataset_type, askcore_db_rdf::dataset,
                             askcore_focus_node::contains, _contained_types},
                            _view_graph));
    return cres_success();
  }
}

bool FocusNodeCollection::isValid() const
{
  clog_debug_vn(d->focus_node.isValid());
  if(d->focus_node.isValid())
  {
    clog_debug_vn(d->focus_node.connection().graphsManager()->hasTripleStore(d->focus_node.uri()));
  }

  return d->focus_node.isValid()
         and d->focus_node.connection().graphsManager()->hasTripleStore(d->focus_node.uri());
}

bool FocusNodeCollection::isReadOnly() const { return false; }

cres_qresult<knowCore::UriList> FocusNodeCollection::containedTypes() const
{
  return d->focus_node.property<knowCore::UriList>(askcore_focus_node::contains);
}

cres_qresult<knowCore::Uri> FocusNodeCollection::collectionType() const
{
  return d->focus_node.property<knowCore::Uri>(askcore_focus_node::collection_type);
}

knowCore::Uri FocusNodeCollection::uri() const { return d->focus_node.uri(); }

kDB::Repository::Connection FocusNodeCollection::connection() const
{
  return d->focus_node.connection();
}

/**
 * @return the number of focus nodes in this collection.
 */
cres_qresult<std::size_t> FocusNodeCollection::count() const
{
  FocusNodeQueries::FocusNodeCount::Results r
    = FocusNodeQueries::FocusNodeCount::execute(d->focus_node.connection(), d->focus_node.uri());

  if(r.success)
  {
    return cres_success(r.data[0].count().toUInt64().expect_success());
  }
  else
  {
    return cres_failure("Failed to query counting of focus nodes: {}", r.errorMessage);
  }
}

cres_qresult<std::size_t> FocusNodeCollection::count(const knowCore::Uri& _type) const
{
  FocusNodeQueries::FocusNodeCountByType::Results r
    = FocusNodeQueries::FocusNodeCountByType::execute(d->focus_node.connection(),
                                                      d->focus_node.uri(), _type);

  if(r.success)
  {
    return cres_success(r.data[0].count().toUInt64().expect_success());
  }
  else
  {
    return cres_failure("Failed to query counting of focus nodes: {}", r.errorMessage);
  }
}

cres_qresult<FocusNode> FocusNodeCollection::focusNode(const knowCore::Uri& _focusNodeUri) const
{
  FocusNodeQueries::FocusNodeSelect::Results r = FocusNodeQueries::FocusNodeSelect::execute(
    d->focus_node.connection(), d->focus_node.uri(), _focusNodeUri);
  if(r.success)
  {
    if(r.data.size() == 1)
    {
      cres_try(FocusNodeDeclaration declaration,
               FocusNodeDeclarationsRegistry::declaration(r.data.first().type()));
      return cres_success(
        FocusNode(d->focus_node.connection(), d->focus_node.uri(), _focusNodeUri, declaration));
    }
    else
    {
      return cres_failure("Focus node {} does not exists.", _focusNodeUri);
    }
  }
  else
  {
    return cres_failure("Failed to query for focus node with error {}", r.errorMessage);
  }
}

cres_qresult<bool> FocusNodeCollection::hasFocusNode(const knowCore::Uri& _focusNodeUri,
                                                     const knowCore::Uri& _typeUri) const
{
  FocusNodeQueries::HasFocusNode::Results r = FocusNodeQueries::HasFocusNode::execute(
    d->focus_node.connection(), d->focus_node.uri(), _focusNodeUri, _typeUri);
  if(r.success)
  {
    return cres_success(r.data);
  }
  else
  {
    return cres_failure("Failed to query for focus node with error {}", r.errorMessage);
  }
}

cres_qresult<QList<FocusNode>> FocusNodeCollection::all() const
{
  FocusNodeQueries::FocusNodeSelectAll::Results r = FocusNodeQueries::FocusNodeSelectAll::execute(
    d->focus_node.connection(), d->focus_node.uri());
  if(r.success)
  {
    QList<FocusNode> focus_nodes;

    for(qsizetype i = 0; i < r.data.size(); ++i)
    {
      cres_try(FocusNodeDeclaration declaration,
               FocusNodeDeclarationsRegistry::declaration(r.data[i].type()));
      focus_nodes.append(
        FocusNode(d->focus_node.connection(), d->focus_node.uri(), r.data[i].uri(), declaration));
    }

    return cres_success(focus_nodes);
  }
  else
  {
    return cres_failure("Failed to query for focus node with error {}", r.errorMessage);
  }
}

cres_qresult<QList<FocusNode>> FocusNodeCollection::all(const knowCore::Uri& _typeUri) const
{
  cres_try(FocusNodeDeclaration declaration, FocusNodeDeclarationsRegistry::declaration(_typeUri));
  FocusNodeQueries::FocusNodeSelectAllByType::Results r
    = FocusNodeQueries::FocusNodeSelectAllByType::execute(d->focus_node.connection(),
                                                          d->focus_node.uri(), _typeUri);
  if(r.success)
  {
    QList<FocusNode> focus_nodes;

    for(qsizetype i = 0; i < r.data.size(); ++i)
    {
      focus_nodes.append(
        FocusNode(d->focus_node.connection(), d->focus_node.uri(), r.data[i].uri(), declaration));
    }

    return cres_success(focus_nodes);
  }
  else
  {
    return cres_failure("Failed to query for focus node with error {}", r.errorMessage);
  }
}

cres_qresult<QList<FocusNode>> FocusNodeCollection::focusNodes(
  const knowCore::UriList& _default_datatypes,
  const QList<QPair<knowCore::Uri, knowCore::ConstrainedValue>>& _constraints,
  const OperatorOptions& _operatorOptions) const
{
  namespace SA = kDB::SPARQL::Algebra;
  namespace SB = kDB::SPARQL::Algebra::Builders;

  SB::Variable uri_variable("?uri");
  SB::Variable type_variable("?type");

  QHash<QString, QList<SA::NodeCSP>> path2node;
  path2node[askcore_focus_node::type].append(type_variable);

  SB::TriplesList triples_pattern;
  triples_pattern.append(uri_variable, knowCore::Uris::rdf::a, askcore_focus_node::focus_node);
  triples_pattern.append(uri_variable, askcore_focus_node::type, type_variable);

  // BEGIN Find datatypes
  knowCore::UriList datatypes;
  for(int i = 0; i < _constraints.size(); ++i)
  {
    knowCore::Uri path = _constraints[i].first;
    if(path == askcore_focus_node::type)
    {
      for(knowCore::ConstrainedValue::Constraint constraint : _constraints[i].second.constraints())
      {
        if(constraint.type == knowCore::ConstrainedValue::Type::Equal)
        {
          cres_try(knowCore::Uri datatype_uri, constraint.value.value<knowCore::Uri>());
          datatypes.append(datatype_uri);
        }
        else
        {
          return cres_failure("Unsupported constraint for datatype: {}", constraint.type);
        }
      }
    }
  }
  if(datatypes.isEmpty())
  {
    datatypes = _default_datatypes;
  }
  // END Find datatypes

  cres_try(QList<kDB::SPARQL::Algebra::NodeCSP> filters,
           focus_node_filter_constructor(_constraints, &path2node, &uri_variable, datatypes,
                                         &triples_pattern, _operatorOptions.intersectsPrecision));

  SB::SelectQuery builder;
  builder.modifier(SA::SelectModifier::Distinct)
    .variables(uri_variable, type_variable)
    .datasets(SB::Dataset().uri(d->focus_node.uri()))
    .where(SB::GroupGraphPattern().graphPatterns(triples_pattern).filters(filters));

  auto const& [queryString, queryBindings] = builder.toQueryString();

  knowDBC::Query q = d->focus_node.connection().createSPARQLQuery({}, queryString);
  q.bindValues(queryBindings);
  knowDBC::Result r = q.execute();

  if(r)
  {
    QList<FocusNode> fns;
    for(int i = 0; i < r.tuples(); ++i)
    {
      cres_try(knowCore::Uri ds_uri, r.value(i, "uri").value<knowCore::Uri>());
      cres_try(knowCore::Uri ds_type, r.value(i, "type").value<knowCore::Uri>());
      cres_try(FocusNodeDeclaration declaration,
               FocusNodeDeclarationsRegistry::declaration(ds_type));

      fns.append(FocusNode(d->focus_node.connection(), d->focus_node.uri(), ds_uri, declaration));
    }
    return cres_success(fns);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("Failed to query for agents", r);
    return cres_failure("Failed to query focus node with error: {}", r.error());
  }
}
/**
 * Create a new \ref FocusNode of uri type \p _typeUri  and add it to this collection.
 */
cres_qresult<FocusNode> FocusNodeCollection::createFocusNode(const knowCore::Uri& _typeUri,
                                                             const knowCore::ValueHash& _properties,
                                                             const knowCore::Uri& _focusNodeUri)
{
  return FocusNode::create(d->focus_node.connection(), d->focus_node.uri(), _typeUri, _properties,
                           _focusNodeUri);
}
