#include <kDB/Forward.h>

#include <knowCore/Uri.h>

class QueryEvaluationTest
{
public:
  QueryEvaluationTest(kDB::Repository::GraphsManager* _graphsManager, const knowRDF::Node* _node,
                      const knowCore::Uri& _base);
  ~QueryEvaluationTest();
  cres_qresult<void> run();
private:
  const knowRDF::Node* m_node;
  kDB::Repository::GraphsManager* m_graphsManager;
  knowCore::Uri m_base;
};
