#include "StoreInterface.h"

#include <QDir>
#include <QTime>

#include <clog_qt>

#include <knowCore/Uris/askcore_graph.h>

#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/Store.h>
#include <kDB/Repository/TemporaryStore.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/TriplesView.h>

struct StoreInterface::Private
{
  kDB::Repository::Connection connection;
  kDB::Repository::TemporaryStore store;
};

StoreInterface::StoreInterface() : d(new Private) {}

StoreInterface::~StoreInterface()
{
  if(d->connection.isValid())
  {
    d->connection.disconnect();
  }
  delete d;
}

cres_qresult<void> StoreInterface::ensureStart(const QStringList& _extensions)
{
  if(d->connection.isValid() and d->connection.isConnected())
    return cres_success();
  // Connect to a repository
  cres_try(cres_ignore, d->store.start());

  d->connection = d->store.createConnection();

  cres_try(cres_ignore, d->connection.connect());

  for(const QString& ext : _extensions)
  {
    cres_try(cres_ignore, d->connection.enableExtension(ext));
  }

  clog_info("Starting store: '{}:{}'", d->store.store()->directory().absolutePath(),
            d->store.store()->port());

  return cres_success();
}

void StoreInterface::clear()
{
  d->connection.defaultTripleStore().expect_success().clear();
  for(kDB::Repository::TripleStore ts : d->connection.graphsManager()->tripleStores())
  {
    if(ts.uri() != knowCore::Uris::askcore_graph::default_
       and ts.uri() != knowCore::Uris::askcore_graph::info)
    {
      d->connection.graphsManager()->removeTripleStore(ts.uri());
    }
  }
  for(kDB::Repository::TriplesView tv : d->connection.graphsManager()->triplesViews())
  {
    d->connection.graphsManager()->removeView(tv.uri());
  }
}

kDB::Repository::Connection StoreInterface::connection() { return d->connection; }
