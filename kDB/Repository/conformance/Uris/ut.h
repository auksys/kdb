#include <knowCore/Uris/Uris.h>

#define KDB_UT_URIS(F, ...)                                                                        \
  F(__VA_ARGS__, data)                                                                             \
  F(__VA_ARGS__, graph)                                                                            \
  F(__VA_ARGS__, graphData)                                                                        \
  F(__VA_ARGS__, request)

KNOWCORE_ONTOLOGY_URIS(kDB::Repository::Conformance::Uris, KDB_UT_URIS, ut,
                       "http://www.w3.org/2009/sparql/tests/test-update#")
