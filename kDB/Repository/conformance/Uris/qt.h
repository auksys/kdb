#include <knowCore/Uris/Uris.h>

#define KDB_QT_URIS(F, ...)                                                                        \
  F(__VA_ARGS__, query)                                                                            \
  F(__VA_ARGS__, data)                                                                             \
  F(__VA_ARGS__, queryForm)                                                                        \
  F(__VA_ARGS__, QuerySelect)                                                                      \
  F(__VA_ARGS__, QueryConstruct)                                                                   \
  F(__VA_ARGS__, QueryDescribe)                                                                    \
  F(__VA_ARGS__, QueryAsk)                                                                         \
  F(__VA_ARGS__, graphData)

KNOWCORE_ONTOLOGY_URIS(kDB::Repository::Conformance::Uris, KDB_QT_URIS, qt,
                       "http://www.w3.org/2001/sw/DataAccess/tests/test-query#")
