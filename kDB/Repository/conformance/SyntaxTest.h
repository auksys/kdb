#include <QSharedPointer>
#include <kDB/Forward.h>

class SyntaxTest
{
public:
  enum class Mode
  {
    Positive,
    Negative
  };
public:
  SyntaxTest(const knowRDF::Node* _node, Mode _mode);
  ~SyntaxTest();
  cres_qresult<void> run();
private:
  const knowRDF::Node* m_node;
  Mode m_mode;
};
