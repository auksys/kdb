#include <iostream>

#include <clog_print>

#include <QCommandLineParser>
#include <QFile>

#include <clog_qt>
#include <knowCore/Messages.h>
#include <knowCore/Uri.h>
#include <knowCore/Uris/mf.h>

#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/rdfs.h>
#include <knowRDF/Graph.h>
#include <knowRDF/Node.h>
#include <knowRDF/Object.h>
#include <knowRDF/TripleStream.h>

#include "QueryEvaluationTest.h"
#include "StoreInterface.h"
#include "SyntaxTest.h"
#include "UpdateEvaluationTest.h"

namespace Uris = knowCore::Uris;

int main(int _argc, const char** _argv)
{
  QStringList args;
  for(int i = 0; i < _argc; ++i)
  {
    args.append(_argv[i]);
  }

  QCommandLineParser parser;
  parser.setApplicationDescription("kDB SPARQL Conformance tool");
  parser.addHelpOption();
  parser.addOption({"extension", "kDB extension to load.", "extension"});
  parser.addPositionalArgument("root", "root directory of the conformance suite (required)");
  parser.addPositionalArgument("testfile", "name of the test file (required)");
  parser.addPositionalArgument("ignorelist", "file with list of ignored test case");

  parser.process(args);

  if(parser.positionalArguments().length() < 2 or parser.positionalArguments().length() > 3)
  {
    parser.showHelp(-1);
  }

  QStringList extensions = parser.values("extension");

  QString root = clog_qt::qformat("{}/", parser.positionalArguments()[0]);
  QStringList ignorelist;

  if(parser.positionalArguments().size() >= 3)
  {
    QFile ignorefile(root + parser.positionalArguments()[2]);
    if(not ignorefile.open(QIODevice::ReadOnly))
    {
      clog_fatal("Failed to open {}", _argv[2]);
    }

    while(not ignorefile.atEnd())
    {
      QString str = ignorefile.readLine();
      if(str.right(1) == "\n")
      {
        str.chop(1);
      }
      ignorelist << str;
    }
  }

  // Parse the test file
  QString testfilename = root + parser.positionalArguments()[1];
  knowCore::Uri testfilenameUri(testfilename);

  QFile testfile(testfilename);

  if(not testfile.open(QIODevice::ReadOnly))
  {
    clog_fatal("Failed to open {}", testfilename);
  }

  knowRDF::TripleStream testfilestream;
  testfilestream.setBase(testfilenameUri);
  knowRDF::Graph manifest_graph;
  testfilestream.addListener(&manifest_graph);

  cres_qresult<void> testfilestream_rv = testfilestream.start(&testfile, nullptr);

  if(not testfilestream_rv.is_successful())
  {
    clog_fatal("Failed to parse: {}: {}", testfilename, testfilestream_rv.get_error());
  }

  const knowRDF::Node* manifest_node = nullptr;

  if(not manifest_node)
  {
    // In some files, the node is in a blank node
    QList<const knowRDF::Node*> manifest_nodes
      = manifest_graph.getSubjects(knowCore::Uris::rdf::a, Uris::mf::Manifest);

    switch(manifest_nodes.size())
    {
    case 0:
      break;
    case 1:
      manifest_node = manifest_nodes.first();
      break;
    case 2:
      clog_fatal("Too many manifest nodes for {}", testfilename);
    }
  }

  if(not manifest_node)
  {
    clog_fatal("No manifest node for {}", testfilename);
  }

  const knowRDF::Node* manifest_name_node
    = manifest_node->getFirstChild(knowCore::Uris::rdfs::label);

  if(not manifest_name_node)
  {
    manifest_name_node = manifest_node->getFirstChild(knowCore::Uris::rdfs::comment);
  }
  if(not manifest_name_node)
  {
    clog_fatal("No name for manifest node");
  }

  clog_print("Running tests for '{}{}{}'", clog_print_flag::green,
             manifest_name_node->literal().value<QString>().expect_success(),
             clog_print_flag::reset);

  QList<const knowRDF::Node*> test_nodes;

  const knowRDF::Node* test_list_element = manifest_node->getFirstChild(Uris::mf::entries);

  while(test_list_element
        and (test_list_element->type() != knowRDF::Node::Type::Uri
             or test_list_element->uri() != knowCore::Uris::rdf::nil))
  {
    const knowRDF::Node* node = test_list_element->getFirstChild(knowCore::Uris::rdf::first);
    test_nodes.append(node);
    test_list_element = test_list_element->getFirstChild(knowCore::Uris::rdf::rest);
  }

  int count_success = 0;
  int count_failure = 0;

  StoreInterface si;

  QStringList failed_tests;

  for(int i = 0; i < test_nodes.size(); ++i)
  {
    const knowRDF::Node* node = test_nodes.at(i);

    const knowRDF::Node* test_name_node = node->getFirstChild(Uris::mf::name);
    QString test_name
      = test_name_node ? test_name_node->literal().value<QString>().expect_success() : "unknown";

    if(ignorelist.contains(test_name))
    {
      clog_print<clog_print_flag::bold>("- Skip test '{}{}{}' ({}/{})", clog_print_flag::green,
                                        test_name, clog_print_flag::default_color, i + 1,
                                        test_nodes.size());
      continue;
    }
    else
    {
      clog_print<clog_print_flag::bold>("- Running test '{}{}{}' ({}/{})", clog_print_flag::green,
                                        qPrintable(test_name), clog_print_flag::default_color,
                                        i + 1, test_nodes.size());
    }
    const knowRDF::Node* test_type_node = node->getFirstChild(knowCore::Uris::rdf::a);

    cres_qresult<void> local_success = cres_failure("Nothing ran");

    if(not test_type_node)
    {
      clog_fatal("No type specified for test!");
    }
    knowCore::Uri test_type = test_type_node->uri();
    if(test_type == Uris::mf::QueryEvaluationTest)
    {
      si.ensureStart(extensions).expect_success();
      si.clear();
      QueryEvaluationTest qet(si.connection().graphsManager(), node, testfilenameUri);
      local_success = qet.run();
    }
    else if(test_type == Uris::mf::PositiveSyntaxTest
            or test_type == Uris::mf::PositiveUpdateSyntaxTest11)
    {
      SyntaxTest qet(node, SyntaxTest::Mode::Positive);
      local_success = qet.run();
    }
    else if(test_type == Uris::mf::NegativeSyntaxTest or test_type == Uris::mf::NegativeSyntaxTest11
            or test_type == Uris::mf::NegativeUpdateSyntaxTest11)
    {
      SyntaxTest qet(node, SyntaxTest::Mode::Negative);
      local_success = qet.run();
    }
    else if(test_type == Uris::mf::UpdateEvaluationTest)
    {
      si.ensureStart(extensions).expect_success();
      si.clear();
      UpdateEvaluationTest uet(si.connection().graphsManager(),
                               si.connection().defaultTripleStore().expect_success(), node);
      local_success = uet.run();
    }
    else
    {
      clog_fatal("Unknown test type: {}", test_type);
    }
    if(local_success.is_successful())
    {
      ++count_success;
    }
    else
    {
      clog_print<clog_print_flag::red>("Test {} has failed with error '{}'!", test_name,
                                       local_success.get_error());
      ++count_failure;
      failed_tests.append(test_name);
    }
  }

  if(count_failure == 0)
  {
    clog_print<clog_print_flag::green>("All {} tests have succeed", count_success);
  }
  else
  {
    clog_print<clog_print_flag::red | clog_print_flag::bold>(
      "{} tests have failed on a total of {}", count_failure, count_failure + count_success);
    clog_print<clog_print_flag::red>("Failed tests: {}", failed_tests.join(", "));
  }

  return -count_failure;
}
