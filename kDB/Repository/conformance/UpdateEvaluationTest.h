#include <kDB/Repository/TripleStore.h>

class UpdateEvaluationTest
{
public:
  UpdateEvaluationTest(kDB::Repository::GraphsManager* _graphsManager,
                       const kDB::Repository::TripleStore& _triplesStore,
                       const knowRDF::Node* _node);
  ~UpdateEvaluationTest();
  cres_qresult<void> run();
private:
  const knowRDF::Node* m_node;
  kDB::Repository::GraphsManager* m_graphsManager;
  kDB::Repository::TripleStore m_triplesStore;
};
