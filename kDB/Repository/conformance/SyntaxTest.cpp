#include "SyntaxTest.h"

#include <QFile>

#include <clog_qt>
#include <kDB/SPARQL/Query.h>
#include <knowCore/Messages.h>
#include <knowCore/Uri.h>
#include <knowCore/ValueHash.h>
#include <knowRDF/Node.h>

#include <knowCore/Uris/mf.h>

namespace Uris = knowCore::Uris;

SyntaxTest::SyntaxTest(const knowRDF::Node* _node, SyntaxTest::Mode _mode)
    : m_node(_node), m_mode(_mode)
{
}

SyntaxTest::~SyntaxTest() {}

namespace
{
  bool all_valid(const QList<kDB::SPARQL::Query>& qs)
  {
    for(const kDB::SPARQL::Query& q : qs)
    {
      if(q.type() == kDB::SPARQL::Query::Type::Invalid)
      {
        return false;
      }
    }
    return true;
  }
  bool all_not_valid(const QList<kDB::SPARQL::Query>& qs)
  {
    for(const kDB::SPARQL::Query& q : qs)
    {
      if(q.type() != kDB::SPARQL::Query::Type::Invalid)
      {
        return false;
      }
    }
    return true;
  }
} // namespace

cres_qresult<void> SyntaxTest::run()
{
  const knowRDF::Node* action_node = m_node->getFirstChild(Uris::mf::action);
  if(not action_node)
  {
    return cres_failure("missing action node");
  }
  QString query_uri = action_node->uri();
  QFile query_file(query_uri);
  query_file.open(QIODevice::ReadOnly);

  knowCore::Messages m;
  QList<kDB::SPARQL::Query> q
    = kDB::SPARQL::Query::parse(query_file.readAll(), knowCore::ValueHash(), &m, knowCore::Uri());
  switch(m_mode)
  {
  case Mode::Positive:
  {
    if(q.isEmpty() or not all_valid(q))
    {
      return cres_failure("failed to parse '{}':{}", query_uri, m.toString());
    }
    return cres_success();
  }
  case Mode::Negative:
  {
    if(not q.isEmpty() or not all_not_valid(q))
    {
      return cres_failure("successfully parse '{}' but should have failed.", query_uri);
    }
    return cres_success();
  }
  }
  clog_fatal("Should NOT happen!");
}
