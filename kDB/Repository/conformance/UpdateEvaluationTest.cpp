#include "UpdateEvaluationTest.h"

#include <iostream>

#include <QFile>

#include <clog_qt>
#include <knowCore/Messages.h>
#include <knowCore/Uris/askcore_graph.h>
#include <knowCore/Uris/rdfs.h>

#include <knowRDF/Node.h>
#include <knowRDF/TripleStream.h>
#include <knowRDF/TriplesAccumulator.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TripleStreamInserter.h>

#include "Uris/ut.h"
#include <knowCore/Uris/mf.h>

#include "../tests/CompareRDFTriples.h"

namespace Uris = kDB::Repository::Conformance::Uris;

UpdateEvaluationTest::UpdateEvaluationTest(kDB::Repository::GraphsManager* _graphsManager,
                                           const kDB::Repository::TripleStore& _triplesStore,
                                           const knowRDF::Node* _node)
    : m_node(_node), m_graphsManager(_graphsManager), m_triplesStore(_triplesStore)
{
}

UpdateEvaluationTest::~UpdateEvaluationTest() {}

namespace
{
  QHash<QString, QString> graph_data(const knowRDF::Node* parent)
  {
    QHash<QString, QString> graph_datas;
    for(const knowRDF::Node* graphDataNode : parent->children(Uris::ut::graphData))
    {
      const knowRDF::Node* graph_node = graphDataNode->getFirstChild(Uris::ut::graph);
      if(not graph_node)
      {
        clog_error("Error: missing graph node");
        return QHash<QString, QString>();
      }
      const knowRDF::Node* label_node = graphDataNode->getFirstChild(knowCore::Uris::rdfs::label);
      if(not graph_node)
      {
        clog_error("Error: missing label node");
        return QHash<QString, QString>();
      }
      graph_datas[label_node->literal().value<QString>().expect_success()] = graph_node->uri();
    }
    return graph_datas;
  }
} // namespace

cres_qresult<void> UpdateEvaluationTest::run()
{
  const knowRDF::Node* action_node = m_node->getFirstChild(knowCore::Uris::mf::action);
  if(not action_node)
  {
    return cres_failure("missing action node");
  }

  const knowRDF::Node* request_node = action_node->getFirstChild(Uris::ut::request);
  if(not request_node)
  {
    return cres_failure("missing request node!");
  }
  QString request_uri = request_node->uri();

  const knowRDF::Node* request_data_node = action_node->getFirstChild(Uris::ut::data);
  QString request_data_uri;
  if(request_data_node)
  {
    request_data_uri = request_data_node->uri();
  }
  QHash<QString, QString> request_graph_data = graph_data(action_node);

  const knowRDF::Node* result_node = m_node->getFirstChild(knowCore::Uris::mf::result);
  if(not result_node)
  {
    return cres_failure("missing result node!");
  }
  const knowRDF::Node* result_data_node = result_node->getFirstChild(Uris::ut::data);
  QString result_data_uri;
  if(result_data_node)
  {
    result_data_uri = result_data_node->uri();
  }
  QHash<QString, QString> result_graph_data = graph_data(result_node);

  // Print some information about the test
  std::cout << "  query: " << qPrintable(request_uri) << std::endl;
  std::cout << "  data: " << qPrintable(request_data_uri) << std::endl;
  std::cout << "  graphData: " << qPrintable(request_graph_data.values().join(", ")) << std::endl;
  std::cout << "  result: " << qPrintable(result_data_uri) << std::endl;
  std::cout << "  result graphData: " << qPrintable(result_graph_data.values().join(", "))
            << std::endl;

  // Load data graph
  if(not request_data_uri.isEmpty())
  {
    kDB::Repository::TripleStreamInserter tsi(m_triplesStore);

    knowRDF::TripleStream ts;
    ts.addListener(&tsi);
    QFile data_file(request_data_uri);
    cres_try(QString format, knowCore::formatFor(request_data_uri));
    cres_try(cres_ignore, ts.start(&data_file, nullptr, format),
             message("Error: failed to parse {} with error: {}", request_data_uri));
  }

  // Load named graphs
  for(QHash<QString, QString>::const_iterator cit = request_graph_data.begin();
      cit != request_graph_data.end(); ++cit)
  {
    cres_try(kDB::Repository::TripleStore triples_store,
             m_graphsManager->getOrCreateTripleStore(cit.key()));
    kDB::Repository::TripleStreamInserter tsi(triples_store);

    knowRDF::TripleStream ts;
    ts.addListener(&tsi);
    QFile data_file(cit.value());
    cres_try(QString format, knowCore::formatFor(cit.value()));
    cres_try(cres_ignore, ts.start(&data_file, nullptr, format),
             message("Error: failed to parse '{}' with error: '{}'", cit.value()));
  }

  // Generate named dataset
  QList<kDB::Repository::RDFDataset> namedDatasets;
  for(const QString& graph_data_uri : request_graph_data.keys())
  {
    cres_try(kDB::Repository::TripleStore triples_store,
             m_graphsManager->getOrCreateTripleStore(graph_data_uri));
    namedDatasets.append(triples_store);
  }

  // Setup query
  QFile request_file(request_uri);
  request_file.open(QIODevice::ReadOnly);

  if(not request_file.isOpen())
  {
    return cres_failure("Error: failed to open '{}' with error '{}'", request_uri,
                        request_file.errorString());
  }

  knowDBC::Query request
    = m_triplesStore.createSPARQLQuery({namedDatasets}, request_file.readAll());

  // Execute query

  knowDBC::Result result = request.execute();

  if(result)
  {
    // Check triples from the default graph
    if(not result_data_uri.isEmpty())
    {
      cres_try(QList<knowRDF::Triple> inserted_triples, m_triplesStore.triples());
      knowRDF::TriplesAccumulator result_accumulator;
      knowRDF::TripleStream ts;
      ts.addListener(&result_accumulator);
      QFile data_file(result_data_uri);
      cres_try(QString format, knowCore::formatFor(result_data_uri));
      cres_try(cres_ignore, ts.start(&data_file, nullptr, format),
               message("Error: failed to parse {} with error: {}", result_data_uri));

      if(not compare_triples(inserted_triples, result_accumulator.triples()))
      {
        return cres_failure("Unexpected result");
      }
    }
    else
    {
      clog_debug_vn(m_triplesStore.isValid());
      // It might happen that the graph was dropped and need to be reloaded
      if(not m_triplesStore.isValid())
      {
        m_triplesStore = m_graphsManager->getTripleStore(m_triplesStore.uri()).expect_success();
      }

      if(not m_triplesStore.triples().expect_success().isEmpty())
      {
        return cres_failure("Error: default graph is not empty.");
      }
    }
    // Check triples from the named graphs
    for(QHash<QString, QString>::const_iterator cit = result_graph_data.begin();
        cit != result_graph_data.end(); ++cit)
    {
      cres_try(kDB::Repository::TripleStore triples_store,
               m_graphsManager->getOrCreateTripleStore(cit.key()));
      cres_try(QList<knowRDF::Triple> inserted_triples, triples_store.triples());
      knowRDF::TriplesAccumulator result_accumulator;
      knowRDF::TripleStream ts;
      ts.addListener(&result_accumulator);
      QFile data_file(cit.value());
      cres_try(QString format, knowCore::formatFor(cit.value()));
      cres_try(cres_ignore, ts.start(&data_file, nullptr, format),
               message("Error: failed to parse {} with error: {}", cit.value()));

      if(not compare_triples(inserted_triples, result_accumulator.triples()))
      {
        return cres_failure("Unexpected result");
      }
    }

    for(const kDB::Repository::TripleStore& ts : m_graphsManager->tripleStores())
    {
      if(ts.uri() != m_triplesStore.uri() and not result_graph_data.contains(ts.uri())
         and ts.uri() != knowCore::Uris::askcore_graph::info
         and ts.uri() != knowCore::Uris::askcore_graph::default_)
      {
        return cres_failure("Error: graph '{}' should not exists.", ts.uri());
      }
    }

    return cres_success();
  }
  else
  {
    return cres_failure("Failed to execute query {} with error: {}", request_uri, result.error());
  }
}
