#include <kDB/Repository/Connection.h>

class StoreInterface
{
public:
  StoreInterface();
  ~StoreInterface();
  cres_qresult<void> ensureStart(const QStringList& _extensions);
  void clear();
  kDB::Repository::Connection connection();
private:
  struct Private;
  Private* const d;
};
