#include "QueryEvaluationTest.h"

#include <iostream>

#include <QFile>

#include <clog_qt>
#include <knowCore/Messages.h>
#include <knowCore/Uri.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/ValueHash.h>

#include <knowRDF/TripleStream.h>

#include <knowDBC/Query.h>

#include <kDB/SPARQL/Algebra/NodeVisitor.h>
#include <kDB/SPARQL/Query.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/DatasetsUnion.h>
#include <kDB/Repository/EmptyRDFDataset.h>
#include <kDB/Repository/GraphsManager.h>
#include <kDB/Repository/TripleStore.h>
#include <kDB/Repository/TripleStreamInserter.h>

#include "Uris/qt.h"
#include <knowCore/Uris/mf.h>

#include "../tests/CompareSPARQLResults.h"

namespace Uris = kDB::Repository::Conformance::Uris;

QueryEvaluationTest::QueryEvaluationTest(kDB::Repository::GraphsManager* _graphsManager,
                                         const knowRDF::Node* _node, const knowCore::Uri& _base)
    : m_node(_node), m_graphsManager(_graphsManager), m_base(_base)
{
}

QueryEvaluationTest::~QueryEvaluationTest() {}

struct FindGraphNodeVisitor : kDB::SPARQL::Algebra::NodeVisitor<void>
{
  QStringList data_to_load;

  void visit(kDB::SPARQL::Algebra::DatasetCSP _node) override { data_to_load.append(_node->uri()); }
};

cres_qresult<void> QueryEvaluationTest::run()
{
  enum class QueryForm
  {
    Select,
    Construct,
    Ask
  };
  QueryForm queryForm = QueryForm::Select;

  // Get query form node
  const knowRDF::Node* query_form_node = m_node->getFirstChild(Uris::qt::queryForm);

  if(query_form_node)
  {
    QString query_form_uri = query_form_node->uri();
    if(query_form_uri == Uris::qt::QueryConstruct)
    {
      queryForm = QueryForm::Construct;
    }
    else if(query_form_uri == Uris::qt::QuerySelect)
    {
      queryForm = QueryForm::Select;
    }
    else if(query_form_uri == Uris::qt::QueryAsk)
    {
      queryForm = QueryForm::Ask;
    }
    else
    {
      clog_fatal("Unknown query form: {}", query_form_uri);
    }
  }
  else
  {
    clog_warning("No queryForm node, assuming Select query");
  }

  // Get action node
  const knowRDF::Node* action_node = m_node->getFirstChild(knowCore::Uris::mf::action);
  if(not action_node)
  {
    return cres_failure("Error: missing action node");
  }

  // Find query
  const knowRDF::Node* query_node = action_node->getFirstChild(Uris::qt::query);
  if(not query_node)
  {
    return cres_failure("Error: missing query node!");
  }
  QString query_uri = query_node->uri();

  // Read query data
  QFile query_file(query_uri);
  query_file.open(QIODevice::ReadOnly);
  QByteArray query_content = query_file.readAll();

  // Find data URIs
  QStringList data_to_load;
  QStringList data_uris;

  for(const knowRDF::Node* data_node : action_node->children(Uris::qt::data))
  {
    knowCore::Uri uri = data_node->uri();
    data_uris.append(data_node->uri());
    if(not data_to_load.contains(uri))
      data_to_load.append(uri);
  }

  // Find graph data uris
  QStringList graph_data_uris;
  for(const knowRDF::Node* graph_data_node : action_node->children(Uris::qt::graphData))
  {
    knowCore::Uri uri = graph_data_node->uri();
    graph_data_uris.append(uri);
    if(not data_to_load.contains(uri))
      data_to_load.append(uri);
  }

  const knowRDF::Node* result_node = m_node->getFirstChild(knowCore::Uris::mf::result);
  if(not result_node)
  {
    return cres_failure("Error: missing result node!");
  }
  QString result_uri = result_node->uri();

  // Find if there is data or named dataset defined in the query
  QList<kDB::SPARQL::Query> sparql_queries
    = kDB::SPARQL::Query::parse(query_content, knowCore::ValueHash(), nullptr, m_base);
  for(const kDB::SPARQL::Query& query : sparql_queries)
  {
    if(query.getNode())
    {
      FindGraphNodeVisitor fgnv;
      fgnv.start(query.getNode());
      for(const QString& uri : fgnv.data_to_load)
      {
        if(not data_to_load.contains(uri))
          data_to_load.append(uri);
      }
    }
  }

  // Print some information about the test
  std::cout << "  query: " << qPrintable(query_uri) << std::endl;
  std::cout << "  data: " << qPrintable(data_uris.join(", ")) << std::endl;
  std::cout << "  graphData: " << qPrintable(graph_data_uris.join(", ")) << std::endl;
  std::cout << "  to be loaded: " << qPrintable(data_to_load.join(", ")) << std::endl;
  std::cout << "  result: " << qPrintable(result_uri) << std::endl;

  // Load data
  for(const QString& uri : data_to_load)
  {
    cres_try(kDB::Repository::TripleStore triples_store,
             m_graphsManager->getOrCreateTripleStore(uri));

    kDB::Repository::TripleStreamInserter tsi(triples_store);

    knowRDF::TripleStream ts;
    ts.addListener(&tsi);
    QFile data_file(uri);
    cres_try(QString format, knowCore::formatFor(uri));
    cres_try(cres_ignore, ts.start(&data_file, nullptr, format),
             message("Error: failed to parse {} with error: {}", uri));
  }

  // Generate default dataset
  kDB::Repository::RDFDataset dataset;

  switch(data_uris.size())
  {
  case 0:
    dataset = m_graphsManager->emptyGraph();
    break;
  case 1:
  {
    cres_try(dataset, m_graphsManager->getTripleStore(data_uris.first()));
  }
  break;
  default:
  {
    kDB::Repository::DatasetsUnion unionDS;
    for(const QString& data_uri : data_uris)
    {
      cres_try(kDB::Repository::TripleStore triples_store,
               m_graphsManager->getOrCreateTripleStore(data_uri));
      unionDS.add(triples_store);
    }
    dataset = unionDS;
  }
  }

  // Generate named dataset
  QList<kDB::Repository::RDFDataset> namedDatasets;
  for(const QString& graph_data_uri : graph_data_uris)
  {
    cres_try(kDB::Repository::TripleStore triples_store,
             m_graphsManager->getOrCreateTripleStore(graph_data_uri));
    namedDatasets.append(triples_store);
  }

  knowDBC::Query query = dataset.createSPARQLQuery(
    kDB::Repository::RDFEnvironment(namedDatasets).setBase(m_base), query_content);
  //   knowDBC::Query query(m_connection, "SELECT ?x ?y ?z WHERE { ?x ?y ?z . }");
  knowDBC::Result result = query.execute();

  if(result)
  {
    switch(queryForm)
    {
    case QueryForm::Select:
    {
      QFile result_file(result_uri);
      cres_try(QString format, knowCore::formatFor(result_uri));
      auto const [success, ref_result, errMsg]
        = knowDBC::Result::read(&result_file, format, m_base);
      if(not success)
      {
        return cres_failure("Failed to parser {} with error {}", result_uri, errMsg.value());
      }

      if(result.fields() != ref_result.value().fields())
      {
        return cres_failure(
          "Error: different number of fields!  query result({}) != reference result({})",
          result.fields(), ref_result.value().fields());
      }
      if(result.tuples() != ref_result.value().tuples())
      {
        return cres_failure(
          "Error: different number of tuples!  query result({}) != reference result({})",
          result.tuples(), ref_result.value().tuples());
      }

      return cres_cond<void>(compare_tuples(result, ref_result.value()), "Tuples are different!");
    }
    case QueryForm::Construct:
    {
      QFile result_file(result_uri);

      knowRDF::Graph graph_result;

      for(int i = 0; i < result.tuples(); ++i)
      {
        knowCore::Value d = result.value(i, 0);
        cres_try(knowRDF::Triple t, d.value<knowRDF::Triple>());
        graph_result.triple(t);
      }

      knowRDF::TripleStream stream;
      knowRDF::Graph graph_ref;
      stream.addListener(&graph_ref);
      result_file.open(QIODevice::ReadOnly);
      cres_try(cres_ignore, stream.start(&result_file, nullptr, knowCore::FileFormat::Turtle),
               message("Error: failed to parse {} with error: {}", result_uri));
      return cres_cond<void>(compare_graphs(&graph_result, &graph_ref),
                             "Graphs are not identical.");
    }
    case QueryForm::Ask:
    {
      QFile result_file(result_uri);
      cres_try(QString format, knowCore::formatFor(result_uri));
      cres_try(knowDBC::Result ref_result, knowDBC::Result::read(&result_file, format, m_base));

      if(result.type() != knowDBC::Result::Type::Boolean)
      {
        return cres_failure("Error: result should be of type boolean");
      }
      if(ref_result.boolean() != result.boolean())
      {
        return cres_failure("Error: result should be {} but got {}", ref_result.boolean(),
                            result.boolean());
      }
      return cres_success();
    }
    }
    qFatal("Internal Error in QueryEvaluationTest");
  }
  else
  {
    return cres_failure("Failed to execute query {} with error {}", query_uri, result.error());
  }
}
