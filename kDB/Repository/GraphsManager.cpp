#include "GraphsManager.h"

#include <QDir>
#include <QHash>
#include <QJsonObject>
#include <QMutex>

#include <knowCore/Messages.h>
#include <knowCore/TypeDefinitions.h>
#include <knowCore/ValueHash.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/RDFView/ViewDefinition.h>

#include "Connection_p.h"
#include "DatabaseInterface/PostgreSQL/RDFValueBinaryMarshal.h"
#include "DatabaseInterface/PostgreSQL/SQLInterface_p.h"
#include "EmptyRDFDataset.h"
#include "Logging.h"
#include "PersistentDatasetsUnion_p.h"
#include "QueryConnectionInfo.h"
#include "TemporaryTransaction.h"
#include "Transaction_p.h"
#include "TripleStore_p.h"
#include "TriplesView.h"

#include <knowCore/Uris/askcore_graph.h>

using namespace kDB::Repository;

struct GraphsManager::Private
{
  QMutex m;
  knowCore::WeakReference<Connection> connection;
  QHash<knowCore::Uri, QSharedPointer<TripleStore::Definition>> tripleStores;
  QHash<knowCore::Uri, RDFView::ViewDefinition> tripleViews;
  QHash<knowCore::Uri, QSharedPointer<PersistentDatasetsUnion::Definition>> unions;
};

GraphsManager::GraphsManager(const Connection& _connection) : d(new Private)
{
  d->connection = _connection;
}

GraphsManager::~GraphsManager() { delete d; }

cres_qresult<RDFDataset> GraphsManager::getDataset(const knowCore::Uri& _name)
{
  QMutexLocker locker(&d->m);
  if(d->tripleStores.contains(_name))
  {
    locker.unlock();
    return getTripleStore(_name);
  }
  if(d->tripleViews.contains(_name))
  {
    locker.unlock();
    return getTriplesView(_name);
  }
  if(d->unions.contains(_name))
  {
    locker.unlock();
    return getUnion(_name);
  }
  return cres_failure("Unknown dataset {}", _name);
}

QList<RDFDataset> GraphsManager::datasets()
{
  QMutexLocker locker(&d->m);
  QList<RDFDataset> l;
  for(const QSharedPointer<TripleStore::Definition>& tsd : d->tripleStores.values())
  {
    l.append(TripleStore(d->connection, tsd));
  }
  for(kDB::RDFView::ViewDefinition tsd : d->tripleViews.values())
  {
    l.append(TriplesView(d->connection, tsd));
  }
  for(const QSharedPointer<PersistentDatasetsUnion::Definition>& tsd : d->unions.values())
  {
    l.append(PersistentDatasetsUnion(d->connection, tsd));
  }
  return l;
}

QList<RDFDataset> GraphsManager::datasets(const QList<knowCore::Uri>& _list)
{
  QList<RDFDataset> l;
  for(const knowCore::Uri& u : _list)
  {
    l.append(getDataset(u).expect_success());
  }
  return l;
}

cres_qresult<TripleStore> GraphsManager::createTripleStore(const knowCore::Uri& _name,
                                                           const Transaction& _transaction)
{
  QMutexLocker locker(&d->m);
  if(d->tripleStores.contains(_name))
  {
    return cres_failure("Triple store with name {} already exists.", _name);
  }
  TemporaryTransaction transaction(_transaction, d->connection);

  // Create the table
  DatabaseInterface::PostgreSQL::SQLInterface::lock(
    transaction.transaction(), "triples_stores",
    DatabaseInterface::PostgreSQL::SQLInterface::LockMode::Exclusive);
  cres_try(QString table_name,
           DatabaseInterface::PostgreSQL::SQLInterface::createTripleStore(transaction.transaction(),
                                                                          _name),
           on_failure(transaction.rollback()));

  // Create the definition
  QSharedPointer<TripleStore::Definition> tsd(new TripleStore::Definition);
  tsd->name = _name;
  tsd->table = table_name;
  tsd->isValid = true;
  clog_assert(not tsd->name.isEmpty());
  clog_assert(not tsd->table.isEmpty());
  tsd->options = TripleStore::Option::None;
  d->tripleStores[tsd->name] = tsd;

  // Setup specific index
  TripleStore store(d->connection, tsd);
  cres_try(
    cres_ignore,
    Connection(d->connection).d->rdf_value_marshal->setupIndexes(store, transaction.transaction()));

  // Transaction is not needed forward
  cres_try(cres_ignore, transaction.commitIfOwned());

  return cres_success(store);
}

cres_qresult<TripleStore> GraphsManager::getTripleStore(const knowCore::Uri& _name)
{
  if(not hasTripleStore(_name))
  {
    cres_try(cres_ignore, reload());
  }
  if(hasTripleStore(_name))
  {
    QMutexLocker locker(&d->m);
    return cres_success(TripleStore(d->connection, d->tripleStores.value(_name)));
  }
  else
  {
    return cres_failure("No triple store with name {}", _name);
  }
}

cres_qresult<TripleStore> GraphsManager::getOrCreateTripleStore(const knowCore::Uri& _name,
                                                                const Transaction& _transaction)
{
  if(hasTripleStore(_name))
  {
    return getTripleStore(_name);
  }
  else
  {
    return createTripleStore(_name, _transaction);
  }
}

cres_qresult<void> GraphsManager::removeTripleStore(const knowCore::Uri& _name,
                                                    const Transaction& _transaction)
{
  if(_name == knowCore::Uris::askcore_graph::info)
  {
    return cres_failure(
      "Removing askcore_graph:info would break the kDB store and is not allowed.");
  }
  cres_try(kDB::Repository::TripleStore ts, getTripleStore(_name));
  QMutexLocker locker(&d->m);
  if(ts.type() == RDFDataset::Type::TripleStore)
  {
    if(ts.options().testFlag(TripleStore::Option::Versioning))
    {
      ts.disableVersioning();
    }
    ts.D()->definition->isValid = false;
    d->tripleStores.remove(_name);
    TemporaryTransaction transaction(_transaction, d->connection);
    cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::removeTripleStore(
                            transaction.transaction(), _name));
    return transaction.commitIfOwned();
  }
  return cres_failure("No such triple store {}", _name);
}

bool GraphsManager::hasTripleStore(const knowCore::Uri& _name)
{
  QMutexLocker locker(&d->m);
  return d->tripleStores.contains(_name);
}

QList<TripleStore> GraphsManager::tripleStores() const
{
  QMutexLocker locker(&d->m);
  QList<TripleStore> l;
  for(const QSharedPointer<TripleStore::Definition>& tsd : d->tripleStores.values())
  {
    l.append(TripleStore(d->connection, tsd));
  }
  return l;
}

cres_qresult<void> GraphsManager::loadViewsFrom(const QString& _directory,
                                                const knowCore::ValueHash& _bindings,
                                                const QStringList& _filters, const QString& _format)
{
  for(const QString& sml_filename : QDir(_directory).entryList(_filters))
  {
    knowCore::Messages lidar_view_definition_messages;
    QFile lidar_view_definition(_directory + "/" + sml_filename);
    kDB::RDFView::ViewDefinition lidarViewDefinition = kDB::RDFView::ViewDefinition::parse(
      &lidar_view_definition, &lidar_view_definition_messages, _bindings, _format);
    if(not lidarViewDefinition.isValid())
    {
      return cres_log_failure("Failed to parse '{}': {}", lidar_view_definition_messages.toString(),
                              sml_filename);
    }
    if(not hasTriplesView(lidarViewDefinition.name()))
    {
      cres_try(cres_ignore, createView(lidarViewDefinition),
               message("Failed to create view '{}' with error '{}'", lidarViewDefinition.name()));
    }
  }
  return cres_success();
}

cres_qresult<TriplesView> GraphsManager::createView(const kDB::RDFView::ViewDefinition& _definition)
{
  QMutexLocker locker(&d->m);
  if(d->tripleViews.contains(_definition.name()))
  {
    return cres_failure("View with name {} already exists", _definition.name());
  }
  d->tripleViews[_definition.name()] = _definition;
  DatabaseInterface::PostgreSQL::SQLInterface::setDefinition(d->connection, _definition);
  TriplesView tv = TriplesView(d->connection, _definition);
  if(tv.updateView())
  {
    return cres_success(tv);
  }
  else
  {
    DatabaseInterface::PostgreSQL::SQLInterface::removeViewDefinition(d->connection, tv.uri());
    return cres_failure("Failed to create view {}", _definition.name());
  }
}

cres_qresult<void> GraphsManager::removeView(const knowCore::Uri& _name)
{
  d->tripleViews.remove(_name);
  return DatabaseInterface::PostgreSQL::SQLInterface::removeViewDefinition(d->connection, _name);
}

cres_qresult<TriplesView> GraphsManager::getTriplesView(const knowCore::Uri& _name)
{
  if(hasTriplesView(_name))
  {
    QMutexLocker locker(&d->m);
    return cres_success(TriplesView(d->connection, d->tripleViews.value(_name)));
  }
  else
  {
    return cres_failure("No such view {}", _name);
  }
}

bool GraphsManager::hasTriplesView(const knowCore::Uri& _name)
{
  QMutexLocker locker(&d->m);
  return d->tripleViews.contains(_name);
}

QList<TriplesView> GraphsManager::triplesViews() const
{
  QList<TriplesView> l;
  for(kDB::RDFView::ViewDefinition tsd : d->tripleViews.values())
  {
    l.append(TriplesView(d->connection, tsd));
  }
  return l;
}

QList<kDB::RDFView::ViewDefinition> GraphsManager::viewDefinitions() const
{
  QMutexLocker locker(&d->m);
  return d->tripleViews.values();
}

cres_qresult<PersistentDatasetsUnion> GraphsManager::getUnion(const knowCore::Uri& _name)
{
  QMutexLocker locker(&d->m);
  if(d->unions.contains(_name))
  {
    return cres_success(PersistentDatasetsUnion(d->connection, d->unions.value(_name)));
  }
  return cres_failure("No union with name {}", _name);
}

cres_qresult<PersistentDatasetsUnion> GraphsManager::createUnion(const knowCore::Uri& _name)
{
  QMutexLocker locker(&d->m);
  if(d->unions.contains(_name))
  {
    return cres_failure("Union {} already exists.", _name);
  }
  QSharedPointer<PersistentDatasetsUnion::Definition> tsd(new PersistentDatasetsUnion::Definition);
  tsd->name = _name;
  d->unions[tsd->name] = tsd;
  return cres_success(PersistentDatasetsUnion(d->connection, tsd));
}

cres_qresult<PersistentDatasetsUnion> GraphsManager::getOrCreateUnion(const knowCore::Uri& _name)
{
  QMutexLocker locker(&d->m);
  if(d->unions.contains(_name))
  {
    return cres_success(PersistentDatasetsUnion(d->connection, d->unions.value(_name)));
  }
  locker.unlock();
  return createUnion(_name);
}

bool GraphsManager::hasUnion(const knowCore::Uri& _name)
{
  QMutexLocker locker(&d->m);
  return d->unions.contains(_name);
}

cres_qresult<void> GraphsManager::clearUnion(const knowCore::Uri& _name)
{
  QMutexLocker locker(&d->m);
  return DatabaseInterface::PostgreSQL::SQLInterface::clearUnion(d->connection, _name);
}

QList<PersistentDatasetsUnion> GraphsManager::unions() const
{
  QMutexLocker locker(&d->m);
  QList<PersistentDatasetsUnion> pdus;
  for(QSharedPointer<PersistentDatasetsUnion::Definition> def : d->unions.values())
  {
    pdus.append(PersistentDatasetsUnion(d->connection, def));
  }
  return pdus;
}

cres_qresult<void> GraphsManager::reload()
{
  QMutexLocker locker(&d->m);
  d->tripleViews.clear();
  Connection connection = d->connection;
  if(not connection.isValid())
    return cres_failure("Invalid connection"); // This might happen if the graphs manager is
                                               // reloaded while the connection is being deleted
  knowDBC::Query q
    = connection.createSQLQuery("SELECT name, tablename, options, meta FROM triples_stores");
  knowDBC::Result r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("Failed to get list of triples store", r);
    return cres_failure("Failed to get list of triples store {}", r.error());
  }
  QList<knowCore::Uri> existingStores = d->tripleStores.keys();
  QHash<knowCore::Uri, QSharedPointer<TripleStore::Definition>> tripleStores;
  for(int i = 0; i < r.tuples(); ++i)
  {
    cres_try(QString name, r.value(i, 0).value<QString>());
    QSharedPointer<TripleStore::Definition> tsd(nullptr);
    if(existingStores.contains(knowCore::Uri(name)))
    {
      tsd = d->tripleStores[name];
      clog_assert(not tsd->name.isEmpty());
    }
    else
    {
      tsd = QSharedPointer<TripleStore::Definition>(new TripleStore::Definition);
      tsd->name = name;
      tsd->isValid = true;
      cres_try(tsd->table, r.value(i, 1).value<QString>());
      clog_assert(not tsd->name.isEmpty());
    }
    cres_try(tsd->table, r.value(i, 1).value<QString>());
    clog_assert(not tsd->table.isEmpty());
    tripleStores[name] = tsd;
    QMutexLocker l(&tsd->mutex);
    TripleStore::Options new_options(r.value<int>(i, 2).expect_success());
    bool notify_options = new_options != tsd->options;
    if(new_options != tsd->options)
    {
      tsd->options = new_options;
    }
    cres_try(QJsonValue new_meta_json, r.value<QJsonValue>(i, 3));
    cres_try(knowCore::Value new_meta_value,
             DatabaseInterface::PostgreSQL::SQLInterface::parseMeta(new_meta_json));
    cres_try(knowCore::ValueHash new_meta, new_meta_value.value<knowCore::ValueHash>());
    bool notify_meta = new_meta != tsd->meta;
    tsd->meta = new_meta;
    tsd->isValid = true;
    l.unlock();
    if(notify_options)
    {
      emit(tsd->notifications.optionsChanged());
    }
    if(notify_meta)
    {
      emit(tsd->notifications.metaChanged());
    }
  }
  for(auto it = d->tripleStores.begin(); it != d->tripleStores.end(); ++it)
  {
    if(not tripleStores.contains(it.key()))
    {
      it.value()->isValid = false;
    }
  }
  d->tripleStores = tripleStores;
  for(const kDB::RDFView::ViewDefinition& def :
      DatabaseInterface::PostgreSQL::SQLInterface::getDefinitions(connection))
  {
    d->tripleViews[def.name()] = def;
  }
  QHash<QString, QStringList> unionDefinitions
    = DatabaseInterface::PostgreSQL::SQLInterface::getUnions(connection);
  for(QHash<QString, QStringList>::iterator uDit = unionDefinitions.begin();
      uDit != unionDefinitions.end(); ++uDit)
  {
    QSharedPointer<PersistentDatasetsUnion::Definition> unionDefinition
      = d->unions.value(uDit.key(), nullptr);
    if(not unionDefinition)
    {
      unionDefinition = QSharedPointer<PersistentDatasetsUnion::Definition>(
        new PersistentDatasetsUnion::Definition);
      unionDefinition->name = uDit.key();
      d->unions[unionDefinition->name] = unionDefinition;
    }
    QMutexLocker l(&unionDefinition->mutex);
    unionDefinition->datasets = uDit.value();
  }
  return cres_success();
}

QList<knowCore::Uri> GraphsManager::graphs() const
{
  QList<knowCore::Uri> graphs = d->tripleStores.keys() + d->tripleViews.keys() + d->unions.keys();
  std::sort(graphs.begin(), graphs.end());
  return graphs;
}

EmptyRDFDataset GraphsManager::emptyGraph() const { return EmptyRDFDataset(d->connection); }

cres_qresult<void> GraphsManager::lockAll(const Transaction& _transaction)
{
  QMutexLocker locker(&d->m);

  cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::lock(
                          _transaction, "triples_stores",
                          DatabaseInterface::PostgreSQL::SQLInterface::LockMode::Exclusive));

  for(const QSharedPointer<TripleStore::Definition>& def : d->tripleStores)
  {
    cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::lock(
                            _transaction, def->table,
                            DatabaseInterface::PostgreSQL::SQLInterface::LockMode::Exclusive));
  }
  for(const RDFView::ViewDefinition& def : d->tripleViews)
  {
    QString view_query = def.sqlView();
    if(view_query.contains("SELECT"))
    {
      clog_warning("Cannot lock view: '{}' insconsitencies might occurs!", def.name());
    }
    else
    {
      cres_try(cres_ignore, DatabaseInterface::PostgreSQL::SQLInterface::lock(
                              _transaction, def.sqlView(),
                              DatabaseInterface::PostgreSQL::SQLInterface::LockMode::Exclusive));
    }
  }

  return cres_success();
}
