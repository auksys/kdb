#include <QSharedPointer>
#include <knowCore/UriList.h>

namespace kDB
{
  namespace Repository
  {
    class Connection;
    class SPARQLFunctionDefinition;
    class SPARQLFunctionsManager
    {
      friend class Connection;
    public:
      SPARQLFunctionsManager(const Connection& _connection);
      ~SPARQLFunctionsManager();
      QList<SPARQLFunctionDefinition> function(const knowCore::Uri& _sparql_name) const;
      SPARQLFunctionDefinition function(const knowCore::Uri& _sparql_name,
                                        const knowCore::UriList& _list) const;
      void registerFunction(const knowCore::Uri& _sparql_name, const QString& _sql_name,
                            const knowCore::Uri& _return, const QList<knowCore::Uri>& _arguments);
      template<class... Args>
      void registerFunction(const knowCore::Uri& _sparql_name, const QString& _sql_name,
                            const knowCore::Uri& _return, const knowCore::Uri& _argument,
                            Args... _args)
      {
        QList<knowCore::Uri> arguments;
        registerFunction(_sparql_name, _sql_name, _return, arguments, _argument, _args...);
      }
    private:
      template<class... Args>
      void registerFunction(const knowCore::Uri& _sparql_name, const QString& _sql_name,
                            const knowCore::Uri& _return, QList<knowCore::Uri>& _arguments,
                            const knowCore::Uri& _argument, Args... _args)
      {
        _arguments.append(_argument);
        registerFunction(_sparql_name, _sql_name, _return, _arguments, _args...);
      }
    private:
      struct Private;
      Private* const d;
    };
  } // namespace Repository
} // namespace kDB
