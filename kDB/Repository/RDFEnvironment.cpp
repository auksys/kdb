#include "RDFEnvironment.h"

#include "Connection.h"
#include "QueryConnectionInfo.h"
#include "RDFDataset.h"
#include "Services.h"
#include "TripleStore.h"

using namespace kDB::Repository;

struct RDFEnvironment::Private : public QSharedData
{
  RDFDataset dataset;
  Services services;
  QList<RDFDataset> named_datasets;
  QueryConnectionInfo query_connection_info;
  knowCore::Uri base;
};

RDFEnvironment::RDFEnvironment() : d(new Private) {}

RDFEnvironment::RDFEnvironment(const RDFEnvironment& _rhs) : d(_rhs.d) {}

RDFEnvironment& RDFEnvironment::operator=(const RDFEnvironment& _rhs)
{
  d = _rhs.d;
  return *this;
}
RDFEnvironment::RDFEnvironment(const Services& _services) : RDFEnvironment(RDFDataset(), _services)
{
}

RDFEnvironment::RDFEnvironment(const RDFDataset& _dataset, const Services& _services)
    : RDFEnvironment(_dataset, QList<RDFDataset>(), _services)
{
}

RDFEnvironment::RDFEnvironment(const RDFDataset& _dataset, const QList<RDFDataset>& _named_datasets,
                               const Services& _services)
    : RDFEnvironment()
{
  d->dataset = _dataset;
  d->named_datasets = _named_datasets;
  d->services = _services;
}

RDFEnvironment::RDFEnvironment(const QList<RDFDataset>& _named_datasets, const Services& _services)
    : RDFEnvironment(RDFDataset(), _named_datasets, _services)
{
}

RDFEnvironment::~RDFEnvironment() {}

knowCore::Uri RDFEnvironment::base() const { return d->base; }

RDFEnvironment& RDFEnvironment::setBase(const knowCore::Uri& _base)
{
  d->base = _base;
  return *this;
}

RDFDataset RDFEnvironment::defaultDataset() const { return d->dataset; }

RDFEnvironment& RDFEnvironment::setDefaultDataset(const RDFDataset& _dataset)
{
  d->dataset = _dataset;
  return *this;
}

Services RDFEnvironment::services() const { return d->services; }

QList<RDFDataset> RDFEnvironment::namedDatasets() const { return d->named_datasets; }

RDFEnvironment& RDFEnvironment::addNamedDataset(const RDFDataset& _dataset)
{
  d->named_datasets.append(_dataset);
  return *this;
}

QueryConnectionInfo RDFEnvironment::connection() const { return d->query_connection_info; }

RDFEnvironment& RDFEnvironment::setConnection(const QueryConnectionInfo& _connection)
{
  d->query_connection_info = _connection;
  return *this;
}

bool RDFEnvironment::isValid() const { return d->query_connection_info.isValid(); }
