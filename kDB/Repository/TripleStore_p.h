#pragma once

#include "TripleStore.h"

#include <QMutex>
#include <QVariantMap>

#include <knowCore/Uri.h>

#include "RDFDataset_p.h"
#include "VersionControl/Transaction_p.h"

namespace kDB::Repository
{
  struct TripleStoreNotifications : public QObject
  {
    Q_OBJECT
  signals:
    void optionsChanged();
    void metaChanged();
  };
  struct TripleStore::Definition
  { // Do not change
    QMutex mutex;
    TripleStoreNotifications notifications;
    knowCore::Uri name;
    QString table;
    TripleStore::Options options;
    bool isValid = false;
    knowCore::ValueHash meta;
    cres_qresult<void> setMeta(const QStringList& _path, const knowCore::Value& _value,
                               const Transaction& _transaction);
    /**
     * @return the metadata entry pointed by @ref path, if a transaction is active
     *         the database is queried, otherwise, it returns the value from the cache
     */
    cres_qresult<knowCore::Value> getMeta(const QStringList& _path,
                                          const Transaction& _transaction = Transaction());
  private:
    QHash<QString, knowCore::Value> setMeta(QHash<QString, knowCore::Value> _map,
                                            const QStringList& _path,
                                            const knowCore::Value& _value);
  };
  struct TripleStore::Private : public RDFDataset::Private
  {
    Private(const Connection& _connection)
        : RDFDataset::Private(RDFDataset::Type::TripleStore, _connection)
    {
    }
    ~Private();
    QSharedPointer<Definition> definition;
    VersionControl::Manager* manager = nullptr;
    bool disable_recording_version = false;
    bool isValid() const override { return definition->isValid; }
    knowCore::Uri uri() const override { return definition->name; }
    TSTemporaryTransaction acquireTransaction(TripleStore* _self, const Transaction& _transaction);
  };
  struct TripleStore::TSTemporaryTransaction
  {
    bool own_sql_transaction = false;
    Transaction sql_transaction;

    cres_qresult<void> commitIfOwn()
    {
      if(own_sql_transaction)
      {
        return sql_transaction.commit();
      }
      else
      {
        return cres_success();
      }
    }
    cres_qresult<void> rollback() { return sql_transaction.rollback(); }
  };
  KNOWCORE_D_FUNC_DEF(TripleStore)
} // namespace kDB::Repository
