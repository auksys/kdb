#include "SPARQLFunctionsManager.h"

#include <QVariant>

#include <clog_qt>

#include <knowCore/TypeDefinitions.h>

#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/xsd.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include "Connection.h"
#include "Logging.h"
#include "SPARQLFunctionDefinition.h"

using namespace kDB::Repository;

struct SPARQLFunctionsManager::Private
{
  knowCore::WeakReference<Connection> connection;
  QHash<knowCore::Uri, QList<SPARQLFunctionDefinition>> function_definitions;
};

SPARQLFunctionsManager::SPARQLFunctionsManager(const Connection& _connection) : d(new Private)
{
  d->connection = _connection;

  knowDBC::Query q
    = Connection(d->connection)
        .createSQLQuery(
          "SELECT sparql_name, sql_template, return, parameters FROM function_definitions");

  knowDBC::Result r = q.execute();
  if(r)
  {
    for(int i = 0; i < r.tuples(); ++i)
    {
      knowCore::Uri sparql_name = r.value<QString>(i, 0).expect_success();
      QString sparql_name_lc = ((QString)sparql_name).toLower();
      QString sql_template = r.value<QString>(i, 1).expect_success();
      knowCore::Uri return_type = r.value<QString>(i, 2).expect_success();
      QStringList arguments_list
        = r.value<knowCore::StringArray>(i, 3).expect_success().toList().expect_success();
      QList<knowCore::Uri> arguments;
      std::transform(arguments_list.begin(), arguments_list.end(), std::front_inserter(arguments),
                     [](const QString& _a) -> knowCore::Uri { return _a; });
      d->function_definitions[sparql_name_lc].append(
        SPARQLFunctionDefinition(sparql_name, sql_template, return_type, arguments));
    }
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("when reading existing function definitions", r);
  }

  using askcore_sparql_functions = knowCore::Uris::askcore_sparql_functions;
  using askcore_sparql_functions_extra = knowCore::Uris::askcore_sparql_functions_extra;
  using askcore_datatype = knowCore::Uris::askcore_datatype;
  using xsd = knowCore::Uris::xsd;
  registerFunction(askcore_sparql_functions::str, "kdb_str", askcore_datatype::literal,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::isBlank, "kdb_is_blank", xsd::boolean,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::bound, "kdb_bound", xsd::boolean,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::isLiteral, "kdb_is_literal", xsd::boolean,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::isURI, "kdb_is_uri", xsd::boolean,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::isIRI, "kdb_is_uri", xsd::boolean,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::datatype, "kdb_datatype", askcore_datatype::literal,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::lang, "kdb_lang", askcore_datatype::literal,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::langMatches, "kdb_lang_matches", xsd::boolean,
                   askcore_datatype::literal, askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::langMatches, "kdb_lang_matches", xsd::boolean,
                   askcore_datatype::literal, xsd::string);
  registerFunction(askcore_sparql_functions::extendedLangMatches, "kdb_extended_lang_matches",
                   xsd::boolean, askcore_datatype::literal, askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::extendedLangMatches, "kdb_extended_lang_matches",
                   xsd::boolean, askcore_datatype::literal, xsd::string);
  registerFunction(askcore_sparql_functions::sameTerm, "kdb_same_term", xsd::boolean,
                   askcore_datatype::literal, xsd::string);
  registerFunction(askcore_sparql_functions::count, "count", askcore_datatype::literal,
                   xsd::integer);
  registerFunction(askcore_sparql_functions::strstarts, "kdb_strstarts", xsd::boolean,
                   askcore_datatype::literal, askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::strends, "kdb_strends", xsd::boolean,
                   askcore_datatype::literal, askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::contains, "kdb_contains", xsd::boolean,
                   askcore_datatype::literal, askcore_datatype::literal);
  registerFunction(askcore_sparql_functions::coalesce, "coalesce", xsd::anyURI, xsd::anyURI,
                   xsd::anyURI);
  registerFunction(askcore_sparql_functions::iri, "kdb_iri", xsd::anyURI, xsd::anyURI);
  registerFunction(askcore_sparql_functions::iri, "kdb_iri", xsd::anyURI,
                   askcore_datatype::literal);
  registerFunction(askcore_sparql_functions_extra::convertQuantityValue, "kdb_value_convert",
                   xsd::decimal, xsd::decimal, xsd::anyURI, xsd::anyURI);
}

SPARQLFunctionsManager::~SPARQLFunctionsManager() { delete d; }

QList<SPARQLFunctionDefinition>
  SPARQLFunctionsManager::function(const knowCore::Uri& _sparql_name) const
{
  QString sparql_name_lc = ((QString)_sparql_name).toLower();
  return d->function_definitions.value(sparql_name_lc);
}

SPARQLFunctionDefinition SPARQLFunctionsManager::function(const knowCore::Uri& _sparql_name,
                                                          const knowCore::UriList& _list) const
{
  SPARQLFunctionDefinition best;

  int best_conversions = std::numeric_limits<int>::max();

  for(SPARQLFunctionDefinition def : function(_sparql_name))
  {
    if(def.arguments().size() == _list.size())
    {
      bool acceptable = true;
      int conversions = 0;
      for(int i = 0; i < def.arguments().size(); ++i)
      {
        knowCore::Uri arg_type = def.arguments()[i];
        knowCore::Uri s_type = _list[i];
        if(arg_type != s_type)
        {
          ++conversions;
          if(arg_type != knowCore::Uris::askcore_datatype::literal
             and s_type != knowCore::Uris::askcore_datatype::literal
             and not knowCore::MetaTypes::canConvert(s_type, arg_type))
          {
            acceptable = false;
            break;
          }
        }
      }
      if(conversions == 0)
      {
        return def;
      }
      if(acceptable and conversions < best_conversions)
      {
        best = def;
        best_conversions = conversions;
      }
    }
  }

  return best;
}

void SPARQLFunctionsManager::registerFunction(const knowCore::Uri& _sparql_name,
                                              const QString& _sql_template,
                                              const knowCore::Uri& _return,
                                              const QList<knowCore::Uri>& _arguments)
{
  QString sparql_name_lc = ((QString)_sparql_name).toLower();

  QString sql_template = _sql_template;
  if(sql_template.contains("$0") or sql_template.contains("("))
  {
    for(int i = 0; i < _arguments.size(); ++i)
    {
      if(not sql_template.contains(clog_qt::qformat("${}", i)))
      {
        clog_error("Unused argument '{}' in SQL definition '{}' for SPARQL function '{}'", i,
                   sql_template, _sparql_name);
        return;
      }
    }
  }
  else
  {
    sql_template += "(";
    for(int i = 0; i < _arguments.size(); ++i)
    {
      if(i != 0)
        sql_template += ", ";
      sql_template += clog_qt::qformat("${}", i);
    }
    sql_template += ")";
  }

  SPARQLFunctionDefinition fd(_sparql_name, sql_template, _return, _arguments);
  if(d->function_definitions[sparql_name_lc].contains(fd))
  {
    return;
  }
  knowDBC::Query sq
    = Connection(d->connection)
        .createSQLQuery(
          "INSERT INTO function_definitions(sparql_name, sql_template, return, parameters) VALUES "
          "(:sparql_name, :sql_template, :return, :parameters) ON CONFLICT DO NOTHING");
  sq.bindValue(":sparql_name", (QString)_sparql_name);
  sq.bindValue(":sql_template", sql_template);
  sq.bindValue(":return", (QString)_return);
  QStringList arguments_list;
  std::transform(_arguments.begin(), _arguments.end(), std::front_inserter(arguments_list),
                 [](const knowCore::Uri& _a) -> QString { return _a; });
  sq.bindValue(":parameters", arguments_list);
  knowDBC::Result sqr = sq.execute();
  if(sqr)
  {
    d->function_definitions[sparql_name_lc].append(fd);
  }
  else
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR(
      clog_qt::qformat("failed to register function {} ({})", _sparql_name, _sql_template), sqr);
  }
}
