#include "Engine.h"

#include <QHash>

#include <knowCore/Value.h>
#include <knowCore/ValueList.h>

#include <knowCore/Uris/askcore_db.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <knowDBC/Interfaces/QueryExecutor.h>

#include <Cyqlops/Yaml/Node.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>

#include "Context.h"
#include "Interfaces/Action.h"
#include "QueryActions.h"

using namespace kDB::Repository::krQuery;

struct Engine::Private
{
  knowCore::WeakReference<Connection> connection;
  QHash<QString, Interfaces::Action*> key2action;
  QList<Interfaces::Action*> actions;

  cres_qresult<knowCore::Value> execute_one(const Context& _context,
                                            const Cyqlops::Yaml::Node& _node);
};

cres_qresult<knowCore::Value> Engine::Private::execute_one(const Context& _context,
                                                           const Cyqlops::Yaml::Node& _node)
{
  using ynType = Cyqlops::Yaml::Node::Type;
  if(_node.type() != ynType::Map)
  {
    return cres_failure("Root node of krQL Query should be a map");
  }
  if(_node.keys().size() != 1)
  {
    return cres_failure("Root node of krQL Query should have a single key");
  }
  QString action = _node.keys().first();
  QVariant action_parameters = _node[action].toVariant();
  cres_try(knowCore::ValueHash action_parameters_vh,
           knowCore::ValueHash::parseVariantMap(action_parameters.toMap()));

  QString action_l = action.toLower();
  if(action_l != action)
  {
    clog_warning("Upper case keys are deprecated in krQL: {}", action);
  }

  if(key2action.contains(action_l))
  {
    return key2action.value(action_l)->execute(_context, action_l, action_parameters_vh);
  }
  else
  {
    return cres_failure("No action for '{}', possible actions are: {}", action_l,
                        key2action.keys());
  }
}

Engine::Engine(const kDB::Repository::Connection& _connection) : d(new Private)
{
  d->connection = _connection;
  add({"test"_kCs}, new QueryActions::Test, true);
  add({"repository"_kCs}, new QueryActions::Repository, true);
}

Engine::~Engine()
{
  qDeleteAll(d->actions);
  delete d;
}

void Engine::add(const QStringList& _keys, Interfaces::Action* _action,
                 bool _action_owned_by_engine)
{
  if(_action_owned_by_engine)
  {
    d->actions.append(_action);
  }
  for(const QString& key : _keys)
  {
    if(key.toLower() != key)
    {
      clog_warning("Upper case keys are deprecated in krQL: {}", key);
    }
    d->key2action[key.toLower()] = _action;
  }
}

void Engine::remove(Interfaces::Action* _action)
{
  d->actions.removeAll(_action);
  for(QHash<QString, Interfaces::Action*>::iterator it = d->key2action.begin();
      it != d->key2action.end();)
  {
    if(it.value() == _action)
    {
      it = d->key2action.erase(it);
    }
    else
    {
      ++it;
    }
  }
}

cres_qresult<knowCore::Value> Engine::execute(const QString& _text)
{
  return execute(Context(d->connection), _text);
}

cres_qresult<knowCore::Value> Engine::execute(const Context& _context, const QString& _text)
{
  Cyqlops::Yaml::YamlParseError ype;
  Cyqlops::Yaml::Node node = Cyqlops::Yaml::Node::fromYaml(_text, &ype);
  if(node.isUndefined())
  {
    return cres_failure("Failed to parse as YAML with error {}.", ype.errorMessage);
  }
  using ynType = Cyqlops::Yaml::Node::Type;

  switch(node.type())
  {
  case ynType::Map:
    return d->execute_one(_context, node);
  case ynType::Sequence:
  {
    QList<knowCore::Value> results;
    for(std::size_t i = 0; i < node.childrenCount(); ++i)
    {
      cres_try(knowCore::Value res, d->execute_one(_context, node[i]));
      results.append(res);
    }
    return cres_success(knowCore::ValueList(results));
  }
  default:
    return cres_failure("Root of krQL Query should be a map or a list of map.");
  }
}

struct QueryExecutor : public knowDBC::Interfaces::QueryExecutor
{
  QueryExecutor(Engine* _engine) : m_engine(_engine) {}
  knowDBC::Result execute(const QString& _query, const knowCore::ValueHash&,
                          const knowCore::ValueHash&) override
  {
    cres_qresult<knowCore::Value> val = m_engine->execute(_query);
    if(val.is_successful())
    {
      return knowDBC::Result::create(_query, {"data"_kCs},
                                     {knowCore::ValueList{{val.get_value()}}});
    }
    else
    {
      return knowDBC::Result::create(_query, val.get_error().get_message());
    }
  }
  knowCore::Uri queryLanguage() const override
  {
    return knowCore::Uris::askcore_db_query_language::krQL;
  }
private:
  Engine* m_engine;
};

knowDBC::Query Engine::createQuery() { return new QueryExecutor(this); }
