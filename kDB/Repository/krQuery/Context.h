#include <kDB/Forward.h>

#include <QExplicitlySharedDataPointer>

namespace kDB::Repository::krQuery
{
  class Context
  {
  public:
    Context(const kDB::Repository::QueryConnectionInfo& _connection);
    ~Context();
    kDB::Repository::QueryConnectionInfo queryConnectionInfo() const;
  private:
    struct Private;
    QExplicitlySharedDataPointer<Private> d;
  };
} // namespace kDB::Repository::krQuery
