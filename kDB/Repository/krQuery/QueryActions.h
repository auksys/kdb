#include <kDB/Repository/krQuery/Interfaces/Action.h>

#include <kDB/Forward.h>

namespace kDB::Repository::krQuery::QueryActions
{
  /**
   * Define a krQL Query that reacts to "test" for testing purposes.
   *
   * For instance:
   * ```yaml
   * test:
   *   print: a message
   *   return: 42
   * ```
   *
   * Print "a message" in a terminal and return 42.
   */
  class Test : public kDB::Repository::krQuery::Interfaces::Action
  {
  public:
    Test();
    virtual ~Test();
    cres_qresult<knowCore::Value> execute(const kDB::Repository::krQuery::Context& _context,
                                          const QString& _key,
                                          const knowCore::ValueHash& _parameters) override;
  private:
    struct Private;
    Private* const d;
  };

  /**
   * Define a krQL Query that handles repository actions.
   *
   * For instance:
   * ```yaml
   * repository:
   *   enable extension: kDBSensing
   * ```
   */
  class Repository : public kDB::Repository::krQuery::Interfaces::Action
  {
  public:
    Repository();
    virtual ~Repository();
    cres_qresult<knowCore::Value> execute(const kDB::Repository::krQuery::Context& _context,
                                          const QString& _key,
                                          const knowCore::ValueHash& _parameters) override;
  private:
    struct Private;
    Private* const d;
  };

} // namespace kDB::Repository::krQuery::QueryActions
