#include <QStringList>

#include <kDB/Forward.h>

namespace kDB::Repository::krQuery
{
  /**
   * @ingroup kDB_Repository_krQuery
   *
   * Execution engine for k(DB) Request Query Language
   */
  class Engine
  {
  public:
    Engine(const kDB::Repository::Connection& _connection);
    ~Engine();
    /**
     * Add an \ref _action for the list of \ref _keys.
     *
     * @param _action_owned_by_engine set to true if the action should be deleted by the engine
     *
     * _keys take the form of "SOME ACTION" and will react to query with "SOME ACTION".
     */
    void add(const QStringList& _keys, Interfaces::Action* _action, bool _action_owned_by_engine);
    /**
     * Remove the action. Do note that if the action will not be owned anymore by the engine, and
     * the responsability for deletion is in the caller.
     */
    void remove(Interfaces::Action* _action);
    /**
     * Execute a query. Using a default context.
     */
    cres_qresult<knowCore::Value> execute(const QString& _text);
    /**
     * Execute a query with the given context.
     */
    cres_qresult<knowCore::Value> execute(const Context& _context, const QString& _text);
    /**
     * Create a query, using knowDBC interface.
     */
    knowDBC::Query createQuery();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository::krQuery
