#include "QueryActions.h"

#include <knowCore/ValueHash.h>

#include <kDB/Repository/Connection.h>
#include <kDB/Repository/QueryConnectionInfo.h>

#include "Context.h"

#include <clog_print>

using namespace kDB::Repository::krQuery::QueryActions;

struct Test::Private
{
};

Test::Test() : d(new Private) {}

Test::~Test() {}

cres_qresult<knowCore::Value> Test::execute(const kDB::Repository::krQuery::Context& _context,
                                            const QString& _key,
                                            const knowCore::ValueHash& _parameters)
{
  Q_UNUSED(_context);
  if(_key == "test")
  {
    if(_parameters.contains("print"))
    {
      clog_print("{}", _parameters.value("print"));
    }
    if(_parameters.contains("return"))
    {
      return cres_success(_parameters.value("return"));
    }
    else
    {
      return cres_success(knowCore::Value());
    }
  }
  else
  {
    return cres_failure("Unhandled key {}", _key);
  }
}

struct Repository::Private
{
};

Repository::Repository() : d(new Private) {}

Repository::~Repository() {}

cres_qresult<knowCore::Value> Repository::execute(const kDB::Repository::krQuery::Context& _context,
                                                  const QString& _key,
                                                  const knowCore::ValueHash& _parameters)
{
  if(_key == "repository")
  {
    if(_parameters.contains("enable extension"))
    {
      cres_try(QString extension, _parameters.value<QString>("enable extension"));
      cres_try(cres_ignore, _context.queryConnectionInfo().connection().enableExtension(extension));
      return cres_success(knowCore::Value());
    }
    else
    {
      return cres_failure("Unknown key {}.", _parameters.keys());
    }
  }
  else
  {
    return cres_failure("Unhandled key {}", _key);
  }
}
