#pragma once

#include <kDB/Forward.h>

namespace kDB::Repository::krQuery::Interfaces
{
  /**
   * @ingroup kDB_Repository_krQuery
   *
   * Define an action to be executed by the krQuery engine. If holding a reference to a Connection,
   * it should use a knowCore::WeakReference<Connection>, as the action will be indirectly owned by
   * the connection object. However, this is not strictly needed as the Connection is given during
   * execution in the Context.
   */
  class Action
  {
  public:
    virtual ~Action();
    /**
     * Execute the action with the given \ref _key and \ref _parameters
     */
    virtual cres_qresult<knowCore::Value> execute(const Context& _context, const QString& _key,
                                                  const knowCore::ValueHash& _parameters)
      = 0;
  };
} // namespace kDB::Repository::krQuery::Interfaces
