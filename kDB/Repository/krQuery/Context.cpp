#include "Context.h"

#include <kDB/Repository/QueryConnectionInfo.h>

using namespace kDB::Repository::krQuery;

struct Context::Private : public QSharedData
{
  QueryConnectionInfo qci;
};

Context::Context(const kDB::Repository::QueryConnectionInfo& _connection) : d(new Private)
{
  d->qci = _connection;
}

Context::~Context() {}

kDB::Repository::QueryConnectionInfo Context::queryConnectionInfo() const { return d->qci; }
