#include "TriplesView.h"

#include <QSharedDataPointer>

#include <knowCore/TypeDefinitions.h>
#include <knowCore/Uri.h>

#include <knowCore/Uris/xsd.h>
#include <knowRDF/Object.h>
#include <knowRDF/Skolemisation.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include <knowDBC/Query.h>
#include <knowDBC/Result.h>

#include <kDB/RDFView/Expression.h>
#include <kDB/RDFView/ViewDefinition.h>

#include "Connection_p.h"
#include "DatabaseInterface/PostgreSQL/SQLInterface_p.h"
#include "Logging.h"

#include "RDFDataset_p.h"

using namespace kDB::Repository;

struct TriplesView::Private : public RDFDataset::Private
{
  Private() : RDFDataset::Private(RDFDataset::Type::TriplesView) {}
  kDB::RDFView::ViewDefinition definition;
  static QHash<QString, QString> view_functions_to_sql_mapping;

  static QString eval_expression(const kDB::RDFView::Expression& _expr, const QString& view_name,
                                 QHash<QString, knowCore::Value>& _query_arguments);
  static QHash<QString, QString> init_mapping();

  bool isValid() const override { return definition.isValid(); }
  knowCore::Uri uri() const override { return definition.name(); }
};

QHash<QString, QString> TriplesView::Private::view_functions_to_sql_mapping
  = TriplesView::Private::init_mapping();

QHash<QString, QString> TriplesView::Private::init_mapping()
{
  QHash<QString, QString> m;
  m["str"] = "kdb_str";
  m["uri"] = "kdb_view_uri";
  m["plainLiteral"] = "kdb_rdf_term_create";
  m["typedLiteral"] = "kdb_rdf_term_create_typed_literal";
  return m;
}

QString TriplesView::Private::eval_expression(const kDB::RDFView::Expression& _expr,
                                              const QString& view_name,
                                              QHash<QString, knowCore::Value>& _query_arguments)
{
  switch(_expr.type())
  {
  case kDB::RDFView::Expression::Type::FunctionCall:
  {
    QStringList arguments;
    for(const kDB::RDFView::Expression& _expression : _expr.arguments())
    {
      arguments.append(eval_expression(_expression, view_name, _query_arguments));
    }
    if(_expr.name() == "typedLiteral")
    {
      arguments[0] = "kdb_rdf_value_create(" + arguments[0] + ")";
    }
    QString function_name = view_functions_to_sql_mapping.value(_expr.name());
    if(function_name.isEmpty())
    {
      return "kdb_rdf_term_create('http://unknown_function/', null, '')";
    }
    else
    {
      return function_name + "(" + arguments.join(", ") + ")";
    }
  }
  default:
  case kDB::RDFView::Expression::Type::Unknown:
    return "kdb_rdf_term_create('http://unknown/', null, '')";
  case kDB::RDFView::Expression::Type::Value:
  {
    knowCore::Value value = _expr.value();
    if(value.datatype() == knowCore::UriMetaTypeInformation::uri())
    {
      QString name = ":arg" + QString::number(_query_arguments.size());
      _query_arguments[name] = value;
      return "kdb_rdf_term_create(" + name + ", null, '')";
    }
    else
    {
      QString name = ":arg" + QString::number(_query_arguments.size());
      _query_arguments[name] = value;
      return name;
    }
  }
  case kDB::RDFView::Expression::Type::Variable:
    return view_name + "." + _expr.name();
  }
}

KNOWCORE_D_FUNC_DEF(TriplesView)

TriplesView::TriplesView() : RDFDataset(new Private) {}

TriplesView::TriplesView(const Connection& _connection,
                         const kDB::RDFView::ViewDefinition& _definition)
    : RDFDataset(new Private)
{
  D()->connection = _connection;
  D()->definition = _definition;
}

TriplesView::TriplesView(const TriplesView& _rhs) : RDFDataset(_rhs) {}

TriplesView& TriplesView::operator=(const TriplesView& _rhs)
{
  d = _rhs.d;
  return *this;
}

TriplesView::~TriplesView() {}

kDB::RDFView::ViewDefinition TriplesView::viewDefinition() const { return D()->definition; }

bool TriplesView::updateView()
{
  QString view_name;
  if(D()->definition.sqlView().contains("SELECT"))
  {
    view_name = d->connection.d->uniqueTableName("triples_sql_view", D()->definition.name());

    knowDBC::Query q = D()->connection.createSQLQuery("CREATE OR REPLACE VIEW " + view_name + " AS "
                                                      + D()->definition.sqlView());
    knowDBC::Result qr = q.execute();
    if(not qr)
    {
      KDB_REPOSITORY_REPORT_QUERY_ERROR(clog_qt::qformat("failed to create view {}", view_name),
                                        qr);
      return false;
    }
  }
  else
  {
    view_name = D()->definition.sqlView();
  }

  QString view_union;
  int alias_count = 0;
  QHash<QString, knowCore::Value> query_arguments;

  for(const knowRDF::Triple& triple : D()->definition.triples())
  {
    QString alias_name = "a" + QString::number(++alias_count);
    if(not view_union.isEmpty())
    {
      view_union += " UNION ";
    }
    view_union += "(SELECT " + alias_name + ".subject::text AS subject, " + alias_name
                  + ".predicate::text AS predicate, kdb_rdf_term_create((" + alias_name
                  + ".object).uri::text, (" + alias_name + ".object).value::kdb_rdf_value, ("
                  + alias_name + ".object).lang) AS object FROM (SELECT ";
    switch(triple.subject().type())
    {
    case knowRDF::Subject::Type::BlankNode:
      view_union += "('" + knowRDF::blankNodeSkolemisation(triple.subject().blankNode()) + "/' || ("
                    + Private::eval_expression(D()->definition.key(), view_name, query_arguments)
                    + ")::text)";
      break;
    case knowRDF::Subject::Type::Undefined:
      view_union += "'##undef##'";
      break;
    case knowRDF::Subject::Type::Uri:
      view_union += "'" + triple.subject().uri() + "'";
      break;
    case knowRDF::Subject::Type::Variable:
    {
      QString variableName = triple.subject().variableName();
      if(D()->definition.expressions().contains(variableName))
      {
        view_union += "kdb_rdf_term_uri("
                      + Private::eval_expression(D()->definition.expressions()[variableName],
                                                 view_name, query_arguments)
                      + ")";
      }
      else
      {
        view_union += view_name + "." + variableName;
      }
    }
    break;
    }
    view_union += " AS subject, '" + triple.predicate() + "' AS predicate, ";
    switch(triple.object().type())
    {
    case knowRDF::Object::Type::BlankNode:
      view_union += "kdb_rdf_term_create(('"
                    + knowRDF::blankNodeSkolemisation(triple.object().blankNode()) + "/' || ("
                    + Private::eval_expression(D()->definition.key(), view_name, query_arguments)
                    + ")::text)::kdb_uri)";
      break;
    case knowRDF::Object::Type::Literal:
    {
      QString name = ":arg" + QString::number(query_arguments.size());
      query_arguments[name] = knowCore::Value::fromValue(triple.object().literal().datatype());
      view_union += "kdb_rdf_term_create(" + name;
      name = ":arg" + QString::number(query_arguments.size());
      query_arguments[name] = triple.object().literal();
      view_union += ", " + name;
      name = ":arg" + QString::number(query_arguments.size());
      query_arguments[name] = knowCore::Value::fromValue(triple.object().literal().lang());
      view_union += ", " + name + ")";
      break;
    }
    case knowRDF::Object::Type::Undefined:
    {
      view_union += "kdb_rdf_term_create('http://undef/', null, '')";
      break;
    }
    case knowRDF::Object::Type::Uri:
    {
      QString name = ":arg" + QString::number(query_arguments.size());
      query_arguments[name] = knowCore::Value::fromValue(triple.object().uri());
      view_union += "kdb_rdf_term_create(" + name + ", null, '')";
      break;
    }
    case knowRDF::Object::Type::Variable:
    {
      QString variableName = triple.object().variableName();
      if(D()->definition.expressions().contains(variableName))
      {
        QString er = Private::eval_expression(D()->definition.expressions()[variableName],
                                              view_name, query_arguments);
        view_union += "kdb_rdf_term_create(" + er + ")";
      }
      else
      {
        view_union += "kdb_rdf_term_create(" + view_name + "." + variableName + ")";
      }
      break;
    }
    }
    view_union += " AS object FROM " + view_name + ") " + alias_name + " ) ";
  }
  knowDBC::Query q
    = D()->connection.createSQLQuery("CREATE OR REPLACE VIEW " + tablename() + " AS " + view_union);
  q.bindValues(query_arguments);
  q.setOption(knowDBC::Query::OptionsKeys::InlineArguments, true);
  knowDBC::Result r = q.execute();
  if(not r)
  {
    KDB_REPOSITORY_REPORT_QUERY_ERROR("failed to update view", r);
    return false;
  }
  return true;
}

QString TriplesView::tablename() const
{
  return DatabaseInterface::PostgreSQL::SQLInterface::viewTableName(d->connection,
                                                                    D()->definition.name());
}
