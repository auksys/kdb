#include "QueryConnectionInfo.h"

#include "Transaction.h"

namespace kDB
{
  namespace Repository
  {
    class ConnectionHandle;
    struct QueryConnectionInfo::Private : public QSharedData
    {
      Private() {}
      Private(const Private& _rhs)
          : QSharedData(), connection(_rhs.connection), transaction(_rhs.transaction)
      {
      }
      Connection connection;
      Transaction transaction;
      QSharedPointer<ConnectionHandle> connectionHandle();
    };
  } // namespace Repository
} // namespace kDB
