#ifndef _KDB_REPOSITORY_ABSTRACT_BINARY_MARSHAL_H_
#define _KDB_REPOSITORY_ABSTRACT_BINARY_MARSHAL_H_

#include <clog_qt>
#include <cres_qt>

#include <QFlags>
#include <QSharedPointer>

#include <knowCore/MetaType.h>

#include <kDB/Forward.h>
namespace kDB::Repository
{
  class AbstractBinaryMarshal
  {
  public:
    enum class Mode
    {
      ToVariant = 0x1,
      ToByteArray = 0x2
    };
    Q_DECLARE_FLAGS(Modes, Mode)
  protected:
    template<typename _T_>
    static knowCore::Uri datatype()
    {
      return knowCore::MetaTypeInformation<_T_>::uri();
    }
    AbstractBinaryMarshal(const QString& _oid, const knowCore::Uri& _datatype, const Modes& _modes);
  public:
    virtual ~AbstractBinaryMarshal();
    QString oid() const;
    knowCore::Uri datatype() const;
    Modes modes() const;
    /**
     * Convert from a postgresql binary representation to a \ref knowCore::Value value.
     */
    virtual cres_qresult<knowCore::Value>
      toValue(const QByteArray& _source, const kDB::Repository::Connection& _connection) const;
    /**
     * Convert a \ref knowCore::Value to a postgresql binary representation.
     * @param _oidName is filled with the postgresql OID name
     */
    virtual cres_qresult<QByteArray>
      toByteArray(const knowCore::Value& _source, QString& _oidName,
                  const kDB::Repository::Connection& _connection) const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::Repository

Q_DECLARE_OPERATORS_FOR_FLAGS(kDB::Repository::AbstractBinaryMarshal::Modes)

#endif
