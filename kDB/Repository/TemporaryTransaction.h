#include <kDB/Forward.h>

#include <QSharedPointer>

namespace kDB::Repository
{
  /**
   * @ingroup kDB_Repository
   * This class allow to create a transaction if one is not allready created.
   * It is usefull for function that need a transaction to perform and can be passed an optional one
   * as argument
   */
  class TemporaryTransaction
  {
  public:
    TemporaryTransaction(const QueryConnectionInfo& _connectionInfo);
    TemporaryTransaction(const Transaction& _transaction, const Connection& _connection);
    ~TemporaryTransaction();
    Transaction transaction() const;
    /**
     * Commit the underlying transaction if it was created in this temporary transaction, otherwise,
     * it does nothing
     */
    cres_qresult<void> commitIfOwned();
    /**
     * Rollback the transaction, no matter if the transaction is created in the TemporaryTransaction
     * or come from somewhere else
     */
    cres_qresult<void> rollback();
    /**
     * Rollback the underlying transaction if it was created in this temporary transaction,
     * otherwise, it does nothing
     */
    cres_qresult<void> rollbackIfOwned();
  private:
    struct Private;
    QSharedPointer<Private> d;
  };
} // namespace kDB::Repository
