#include "EmptyRDFDataset.h"

#include "RDFDataset_p.h"

using namespace kDB::Repository;

EmptyRDFDataset::EmptyRDFDataset(const Connection& _connection)
    : RDFDataset(new Private(Type::Empty, _connection))
{
}

EmptyRDFDataset::~EmptyRDFDataset() {}
