#include "AbstractBinaryMarshal.h"

#include <knowCore/Value.h>

using namespace kDB::Repository;

struct AbstractBinaryMarshal::Private
{
  QString oid;
  knowCore::Uri datatype;
  Modes modes;
};

AbstractBinaryMarshal::AbstractBinaryMarshal(const QString& _oid, const knowCore::Uri& _datatype,
                                             const Modes& _modes)
    : d(new Private)
{
  d->datatype = _datatype;
  d->oid = _oid;
  d->modes = _modes;
}

AbstractBinaryMarshal::~AbstractBinaryMarshal() { delete d; }

knowCore::Uri AbstractBinaryMarshal::datatype() const { return d->datatype; }

QString AbstractBinaryMarshal::oid() const { return d->oid; }

AbstractBinaryMarshal::Modes AbstractBinaryMarshal::modes() const { return d->modes; }

cres_qresult<QByteArray>
  AbstractBinaryMarshal::toByteArray(const knowCore::Value& _source, QString& _oidName,
                                     const kDB::Repository::Connection& _connection) const
{
  Q_UNUSED(_source);
  Q_UNUSED(_oidName);
  Q_UNUSED(_connection);
  return cres_failure("Unsupported conversion to byte array for type '{}'", _source.datatype());
}

cres_qresult<knowCore::Value>
  AbstractBinaryMarshal::toValue(const QByteArray& _source,
                                 const kDB::Repository::Connection& _connection) const
{
  Q_UNUSED(_source);
  Q_UNUSED(_connection);
  return cres_failure("Unsupported conversion from byte array for type {}.", d->datatype);
}
