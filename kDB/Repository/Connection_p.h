#include "Connection.h"

#include <knowRDF/BlankNode.h>

#include <libpq-fe.h>

#include <QHash>
#include <QMutex>
#include <QSharedPointer>
#include <QUuid>

#include <Cyqlops/Crypto/RSAAlgorithm.h>

#include "Interfaces/QueryFactory.h"
#include "krQuery/Engine.h"

namespace kDB
{
  namespace Repository
  {
    struct Connection::Private
    {
      Private();
      ~Private();
      void disconnect();

      void addOid(Oid _oid, const QString& _name) const;

      knowCore::WeakReference<Connection> self_p;
      QAtomicInt self_p_count; // TODO Ugly, duplicate the counting from the shared pointer, maybe
                               // use QSharedDataPointer in the future?

      QString host, database;
      int port;

      QMutex connections_lock;
      QList<PGconn*> connections;
      QList<PGconn*> available_connections;

      QStringList kdb_rdf_value_type_enum_values;

      mutable QMutex metainformation_lock;

      mutable QHash<Oid, QString> oids2name;
      mutable QHash<QString, Oid> name2oid;
      cres_qresult<QString> oidToName(Oid _oid) const;
      cres_qresult<Oid> nameToOid(const QString& _name) const;

      GraphsManager* graphsManager = nullptr;
      SPARQLFunctionsManager* sparqlFunctionsManager = nullptr;
      NotificationsManager* notificationsManager = nullptr;

      cres_qresult<void> load_sql_files(PGconn* _conn, const QStringList& _files);
      cres_qresult<void> execute_sql_query(PGconn* _conn, const QByteArray& _query);

      /**
       * Update kdb_rdf_value_type_enum_values with the enum defined in the database
       */
      void update_kdb_rdf_value_type_enum_values();

      DatabaseInterface::PostgreSQL::RDFValueBinaryMarshal* rdf_value_marshal = nullptr;
      DatabaseInterface::PostgreSQL::RDFTermBinaryMarshal* rdf_term_marshal = nullptr;

      QHash<QString, AbstractBinaryMarshal*> oid2marshals;
      QHash<knowCore::Uri, AbstractBinaryMarshal*> metaTypeId2marshals;

      cres_qresult<knowCore::Value> toValue(const QString& _oid, const QByteArray& _source);
      cres_qresult<QByteArray> toByteArray(const knowCore::Value& _source, QString& _oidName);

      void write(const knowCore::Value& val, QByteArray* r, Oid* oid, bool* is_binary,
                 bool* is_valid);

      QHash<QString, knowRDF::BlankNode> blanknodes;
      knowRDF::BlankNode blankNode(const QString& _url);

      QUuid m_server_uuid;
      Cyqlops::Crypto::RSAAlgorithm m_rsa_algorithm;

      QHash<QString, QHash<QString, QString>> prefix2key2tablename;

      QString uniqueTableName(const QString& _prefix, const QString& _key);

      QStringList loadedExtensions, enabledExtensions;
      cres_qresult<void> loadExtension(const QString& _extension);
      cres_qresult<void> loadExtensions();

      QUuid uuid;

      QHash<QUuid, std::function<void()>> disconnection_listener;
      QHash<QUuid, std::function<void(const Connection&)>> connection_listener;
      QMap<std::type_index, QObject*> extensionObjects;
      QList<const Interfaces::QueryFactory*> query_factories;

      krQuery::Engine* krQueryEngine = nullptr;
    };
  } // namespace Repository
} // namespace kDB
