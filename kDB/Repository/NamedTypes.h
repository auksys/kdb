#pragma once

#include <cext_nt>

namespace kDB::Repository
{
  using SectionName = cext_named_type<QString, struct HostnameTag>;
  using StoreName = cext_named_type<QString, struct StoreNameTag>;
  using HostName = cext_named_type<QString, struct HostnameTag>;
} // namespace kDB::Repository