#include <kDB/Forward.h>

namespace kDB::RDFDB::SPARQLExecution
{
  class UpdateExecutor
  {
  public:
    UpdateExecutor(const QString& _query_text, const SPARQL::Query& _q, const Database& _database,
                   knowRDF::Graph* _defaultGraph);
    ~UpdateExecutor();
    knowDBC::Result execute();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::RDFDB::SPARQLExecution
