#include "UpdateExecutor_p.h"

#include <knowCore/Uris/askcore_graph.h>
#include <knowDBC/Result.h>
#include <knowRDF/Graph.h>

#include <kDB/SPARQL/Algebra/AbstractNodeVisitor.h>
#include <kDB/SPARQL/Algebra/Utils.h>
#include <kDB/SPARQL/Query.h>

#include "../Database.h"

using namespace kDB::RDFDB::SPARQLExecution;

#define CHECK_ERROR_MESSAGE(_expr_, _dst_err_msg)                                                  \
  {                                                                                                \
    auto const& [s, m] = _expr_;                                                                   \
    if(not s)                                                                                      \
    {                                                                                              \
      _dst_err_msg = m.value().get_message();                                                              \
      return false;                                                                                \
    }                                                                                              \
  }

namespace kDB
{
  namespace RDFDB
  {
    namespace SPARQLExecution
    {
      struct UpdateExecutorVisitor
          : public kDB::SPARQL::Algebra::AbstractNodeVisitor<QVariant, QVariant>
      {
        QString errorMessage;
        Database database;
        knowRDF::Graph* defaultGraph;

        knowRDF::Graph* getGraph(kDB::SPARQL::Algebra::NodeCSP _source)
        {
          if(_source)
          {
            switch(_source->type())
            {
            case kDB::SPARQL::Algebra::NodeType::VariableReference:
            {
              errorMessage = "Graph should be defined as a URI not a variable";
              return nullptr;
            }
            case kDB::SPARQL::Algebra::NodeType::GraphReference:
            {
              return database.createOrGetGraph(
                static_cast<const kDB::SPARQL::Algebra::GraphReference*>(_source.data())->name());
            }
            default:
              qFatal("Internal error!");
            }
          }
          else
          {
            return defaultGraph;
          }
        }
        QVariant visit(kDB::SPARQL::Algebra::InsertDataCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          for(kDB::SPARQL::Algebra::QuadsCSP q : _query->quadsData()->quads())
          {
            knowRDF::Graph* graph = getGraph(q->source());
            if(not graph)
              return false;
            QList<knowRDF::Triple> triples;
            CHECK_ERROR_MESSAGE(kDB::SPARQL::Algebra::Utils::toRDFTriples(q->triples(), &triples),
                                errorMessage);
            for(const knowRDF::Triple& t : triples)
            {
              graph->triple(t);
            }
          }
          return true;
        }
        QVariant visit(kDB::SPARQL::Algebra::DeleteDataCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          for(kDB::SPARQL::Algebra::QuadsCSP q : _query->quadsData()->quads())
          {
            knowRDF::Graph* graph = getGraph(q->source());
            if(not graph)
              return false;
            QList<knowRDF::Triple> triples;
            CHECK_ERROR_MESSAGE(kDB::SPARQL::Algebra::Utils::toRDFTriples(q->triples(), &triples),
                                errorMessage);
            for(const knowRDF::Triple& t : triples)
            {
              graph->removeTriple(t);
            }
          }
          return true;
        }
        QVariant visit(kDB::SPARQL::Algebra::LoadCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DropCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          switch(_query->graphType())
          {
          case kDB::SPARQL::Algebra::GraphType::All:
            for(const knowCore::Uri& uri : database.graphs())
            {
              database.dropGraph(uri);
            }
            break;
          case kDB::SPARQL::Algebra::GraphType::Named:
            for(const knowCore::Uri& uri : database.graphs())
            {
              if(uri != knowCore::Uris::askcore_graph::default_)
              {
                database.dropGraph(uri);
              }
            }
            break;
          case kDB::SPARQL::Algebra::GraphType::Default:
            database.dropGraph(knowCore::Uris::askcore_graph::default_);
            break;
          case kDB::SPARQL::Algebra::GraphType::Graph:
            if(not database.dropGraph(_query->graph()) and not _query->silent())
            {
              errorMessage = clog_qt::qformat("No graph named '{}'", _query->graph());
              return false;
            }
            break;
          }
          return true;
        }
        QVariant visit(kDB::SPARQL::Algebra::ClearCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);
          switch(_query->graphType())
          {
          case kDB::SPARQL::Algebra::GraphType::All:
            for(const knowCore::Uri& uri : database.graphs())
            {
              database.clearGraph(uri);
            }
            break;
          case kDB::SPARQL::Algebra::GraphType::Named:
            for(const knowCore::Uri& uri : database.graphs())
            {
              if(uri != knowCore::Uris::askcore_graph::default_)
              {
                database.clearGraph(uri);
              }
            }
            break;
          case kDB::SPARQL::Algebra::GraphType::Default:
            database.clearGraph(knowCore::Uris::askcore_graph::default_);
            break;
          case kDB::SPARQL::Algebra::GraphType::Graph:
            if(not database.clearGraph(_query->graph()) and not _query->silent())
            {
              errorMessage = clog_qt::qformat("No graph named '{}'", _query->graph());
              return false;
            }
            break;
          }
          return true;
        }
        QVariant visit(kDB::SPARQL::Algebra::CreateCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_parameter);

          if(not _query->silent() and database.graphs().contains(_query->graph()))
          {
            errorMessage = clog_qt::qformat("Graph named '{}' already exists", _query->graph());
            return false;
          }
          else
          {
            database.createOrGetGraph(_query->graph());
            return true;
          }
          return true;
        }
        QVariant visit(kDB::SPARQL::Algebra::QuadsCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::QuadsDataCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DeleteInsertCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ListCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::VariableCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DatasetCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::GroupClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::HavingClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::AskQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DescribeQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DescribeTermCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ConstructQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::SelectQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ExplainQueryCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::PLQueryCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::OrderClausesCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::OrderClauseCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LimitOffsetClauseCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::TripleCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::VariableReferenceCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::GraphReferenceCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::TermCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::BlankNodeCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ServiceCSP _node, const QVariant& _parameter) override
        {
          Q_UNUSED(_node);
          Q_UNUSED(_parameter);
          qFatal("wip");
        }
        QVariant visit(kDB::SPARQL::Algebra::GroupGraphPatternCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ValueCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::OptionalCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::UnionCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::MinusCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalOrCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalAndCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalDifferentCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalEqualCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInferiorCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInferiorEqualCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalSuperiorCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalSuperiorEqualCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::AdditionCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::SubstractionCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::MultiplicationCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::DivisionCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalInCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::RelationalNotInCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::LogicalNegationCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::NegationCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::FunctionCallCSP _query,
                       const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::ExecuteCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::BindCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::IfCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
        QVariant visit(kDB::SPARQL::Algebra::UnlessCSP _query, const QVariant& _parameter) override
        {
          Q_UNUSED(_query);
          Q_UNUSED(_parameter);
          qFatal("WIP");
        }
      };
    } // namespace SPARQLExecution
  } // namespace RDFDB
} // namespace kDB

struct UpdateExecutor::Private
{
  QString query_text;
  SPARQL::Query query;
  Database database;
  knowRDF::Graph* defaultGraph;
};

UpdateExecutor::UpdateExecutor(const QString& _query_text, const SPARQL::Query& _q,
                               const Database& _database, knowRDF::Graph* _defaultGraph)
    : d(new Private)
{
  d->query_text = _query_text;
  d->query = _q;
  d->database = _database;
  d->defaultGraph = _defaultGraph;
}

UpdateExecutor::~UpdateExecutor() {}

knowDBC::Result UpdateExecutor::execute()
{
  knowDBC::Result r;

  UpdateExecutorVisitor uev;
  uev.database = d->database;
  uev.defaultGraph = d->defaultGraph;
  QVariant v = uev.start(d->query.getNode(), QVariant());
  if(v.value<bool>())
  {
    return knowDBC::Result::create(d->query_text, true);
  }
  else
  {
    if(uev.errorMessage.isEmpty())
    {
      return knowDBC::Result::create(d->query_text, "Unsupported type of SPARQL Update.");
    }
    else
    {
      return knowDBC::Result::create(d->query_text, uev.errorMessage);
    }
  }
}
