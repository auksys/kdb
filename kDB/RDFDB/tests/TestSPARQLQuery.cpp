#include "TestSPARQLQuery.h"

#include <knowCore/Uri.h>

#include <knowRDF/Graph.h>
#include <knowRDF/Node.h>

#include <knowDBC/Result.h>

#include "../Database.h"
#include "../SPARQLQuery.h"

void TestSPARQLQuery::testInsert()
{
  QFile file(":/insert_data_1.ru");
  file.open(QIODevice::ReadOnly);
  kDB::RDFDB::Database db;
  kDB::RDFDB::SPARQLQuery query(db, file.readAll());
  ;
  knowDBC::Result result = query.execute();
  QVERIFY(result);

  knowRDF::Graph* graph = db.defaultGraph();

  QList<const knowRDF::Node*> nodes = graph->nodes();

  QCOMPARE(nodes.size(), 2);
  QCOMPARE(nodes[0]->type(), knowRDF::Node::Type::Uri);
  QCOMPARE((QString)nodes[0]->uri(), QStringLiteral("http://example.org/ns#s"));
  QCOMPARE(nodes[1]->type(), knowRDF::Node::Type::Uri);
  QCOMPARE((QString)nodes[1]->uri(), QStringLiteral("http://example.org/ns#o"));
  QCOMPARE(nodes[0]->getFirstChild("http://example.org/ns#p"_kCu), nodes[1]);
}

void TestSPARQLQuery::testInsertNamedGraph()
{
  QFile file(":/insert_data_2.ru");
  file.open(QIODevice::ReadOnly);
  kDB::RDFDB::Database db;
  kDB::RDFDB::SPARQLQuery query(db, file.readAll());
  ;
  knowDBC::Result result = query.execute();
  qDebug() << result.error();
  QVERIFY(result);
}

QTEST_MAIN(TestSPARQLQuery)
