#include "TestDatabase.h"

#include <knowCore/Uri.h>
#include <knowCore/Uris/askcore_graph.h>

#include "../Database.h"

void TestDatabase::testGraph()
{
  kDB::RDFDB::Database db;
  QCOMPARE(db.defaultGraph(), db.createOrGetGraph(knowCore::Uris::askcore_graph::default_));
  QCOMPARE(db.createOrGetGraph(QStringLiteral("a")), db.createOrGetGraph(QStringLiteral("a")));
}

QTEST_MAIN(TestDatabase)
