#include <QSharedPointer>
#include <kDB/Forward.h>

namespace kDB::RDFDB
{
  class Database
  {
  public:
    Database();
    Database(const Database& _rhs);
    const Database& operator=(const Database& _rhs);
    ~Database();
    knowRDF::Graph* createOrGetGraph(const knowCore::Uri& _uri);
    knowRDF::Graph* defaultGraph() const;
    bool clearGraph(const knowCore::Uri& _uri);
    bool dropGraph(const knowCore::Uri& _uri);
    QList<knowCore::Uri> graphs() const;
  private:
    struct Private;
    QSharedPointer<Private> d;
  };
} // namespace kDB::RDFDB
