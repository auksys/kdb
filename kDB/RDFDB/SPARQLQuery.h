#include <QString>

#include <kDB/Forward.h>

namespace kDB::RDFDB
{
  class SPARQLQuery
  {
  public:
    SPARQLQuery(const Database& _database, const QString& _query = QString());
    SPARQLQuery(const Database& _database, knowRDF::Graph* _defaultGraph,
                const QString& _query = QString());
    ~SPARQLQuery();
    knowDBC::Result execute();
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::RDFDB
