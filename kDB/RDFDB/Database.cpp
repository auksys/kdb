#include "Database.h"

#include <QHash>

#include <knowCore/Uri.h>
#include <knowCore/Uris/askcore_graph.h>
#include <knowRDF/Graph.h>

using namespace kDB::RDFDB;

struct Database::Private
{
  QHash<knowCore::Uri, knowRDF::Graph*> graphs;
  knowRDF::Graph* defaultGraph = nullptr;
};

Database::Database() : d(new Private)
{
  d->defaultGraph = createOrGetGraph(knowCore::Uris::askcore_graph::default_);
}

Database::Database(const Database& _rhs) : d(_rhs.d) {}

const Database& Database::operator=(const Database& _rhs)
{
  d = _rhs.d;
  return *this;
}

Database::~Database() {}

knowRDF::Graph* Database::createOrGetGraph(const knowCore::Uri& _uri)
{
  knowRDF::Graph* r = d->graphs.value(_uri, nullptr);
  if(not r)
  {
    r = new knowRDF::Graph;
    d->graphs[_uri] = r;
  }
  return r;
}

knowRDF::Graph* Database::defaultGraph() const { return d->defaultGraph; }

bool Database::clearGraph(const knowCore::Uri& _uri)
{
  QHash<knowCore::Uri, knowRDF::Graph*>::iterator it = d->graphs.find(_uri);
  if(it != d->graphs.end())
  {
    it.value()->clear();
    return true;
  }
  else
  {
    return false;
  }
}

bool Database::dropGraph(const knowCore::Uri& _uri)
{
  QHash<knowCore::Uri, knowRDF::Graph*>::iterator it = d->graphs.find(_uri);
  if(it != d->graphs.end())
  {
    delete it.value();
    d->graphs.erase(it);
    return true;
  }
  else
  {
    return false;
  }
}

QList<knowCore::Uri> Database::graphs() const { return d->graphs.keys(); }
