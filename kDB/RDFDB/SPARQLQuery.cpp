#include "SPARQLQuery.h"

using namespace kDB::RDFDB;

#include <knowCore/Messages.h>
#include <knowCore/Uri.h>
#include <knowCore/ValueHash.h>

#include <knowDBC/Result.h>

#include <kDB/SPARQL/Query.h>

#include "Database.h"

#include "SPARQLExecution/UpdateExecutor_p.h"

struct SPARQLQuery::Private
{
  Database db;
  QString query;
  knowRDF::Graph* defaultGraph;
};

SPARQLQuery::SPARQLQuery(const Database& _database, const QString& _query)
    : SPARQLQuery(_database, _database.defaultGraph(), _query)
{
}

SPARQLQuery::SPARQLQuery(const kDB::RDFDB::Database& _database, knowRDF::Graph* _defaultGraph,
                         const QString& _query)
    : d(new Private)
{
  d->db = _database;
  d->defaultGraph = _defaultGraph;
  d->query = _query;
}

SPARQLQuery::~SPARQLQuery() { delete d; }

knowDBC::Result SPARQLQuery::execute()
{
  knowCore::Messages messages;
  QList<kDB::SPARQL::Query> qs = kDB::SPARQL::Query::parse(d->query.toUtf8(), knowCore::ValueHash(),
                                                           &messages, knowCore::Uri());
  if(qs.isEmpty())
  {
    clog_assert(messages.hasErrors());
    return knowDBC::Result::create(d->query, messages.toString());
  }

  knowDBC::Result r;

  for(const kDB::SPARQL::Query& q : qs)
  {
    switch(q.type())
    {
    case kDB::SPARQL::Query::Type::Invalid:
      qFatal("Should not happen");
    case kDB::SPARQL::Query::Type::Update:
    {
      SPARQLExecution::UpdateExecutor updateExecutor(d->query, q, d->db, d->defaultGraph);
      r = updateExecutor.execute();
      break;
    }
    default:
      return knowDBC::Result::create(d->query, "Unsupported query type.");
    }
  }
  return r;
}
