#pragma once

#include <knowDBC/Forward.h>

namespace kDB
{
  namespace Repository
  {
    class AbstractBinaryMarshal;
    class AbstractService;
    class Connection;
    class ConnectionHandle;
    class DatasetsUnion;
    class EmptyRDFDataset;
    class GraphsManager;
    class NotificationsManager;
    class PersistentDatasetsUnion;
    class QueryConnectionInfo;
    class RDFDataset;
    class RDFEnvironment;
    class Service;
    class Services;
    class SPARQLFunctionsManager;
    class Store;
    class Transaction;
    class TripleStore;
    class TriplesView;
    namespace Interfaces
    {
      class QueryFactory;
    }
    namespace RDF
    {
      class FocusNode;
      class FocusNodeDeclaration;
      class FocusNodeDeclarationsRegistry;
    } // namespace RDF
    namespace VersionControl
    {
      class Delta;
      class DeltaBuilder;
      class Manager;
      class Revision;
      class RevisionBuilder;
      class Signature;
      class Transaction;
      namespace Utils
      {
        class MetaVersion;
      }
    } // namespace VersionControl
    namespace SPARQLExecution
    {
      class QueryExecutorVisitor;
      class SPARQLAlgebraToPostgresSQL;
      struct SPARQLAlgebraToPostgresSQLVisitor;
    } // namespace SPARQLExecution
    namespace krQuery
    {
      class Context;
      class Engine;
      namespace Interfaces
      {
        class Action;
      }
    } // namespace krQuery
    namespace DatabaseInterface::PostgreSQL
    {
      class BinaryMarshalsRegistry;
      class RDFTermBinaryMarshal;
      class RDFValueBinaryMarshal;
      class SQLCopyData;
      class SQLQueryExecutor;
      class SQLInterface;
      class SQLReadData;
      class SQLResult;
    } // namespace DatabaseInterface::PostgreSQL
  } // namespace Repository
  namespace RDFView
  {
    class ViewDefinition;
  }
  namespace SPARQL
  {
    class Query;
  }
  namespace RDFDB
  {
    class Database;
  }
} // namespace kDB
