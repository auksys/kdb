#include <QString>

namespace kDB
{
  namespace Utils
  {
    class ProgressBar
    {
    public:
      ProgressBar(const QString& _text = QString(), int _totalWidth = 70);
      ~ProgressBar();
      void update(double _percentage);
    private:
      struct Private;
      Private* const d;
    };
  } // namespace Utils
} // namespace kDB
