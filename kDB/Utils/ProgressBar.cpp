#include "ProgressBar.h"

#include <iostream>

using namespace kDB::Utils;

struct ProgressBar::Private
{
  QString text;
  int totalWidth;
};

ProgressBar::ProgressBar(const QString& _text, int _totalWidth) : d(new Private)
{
  d->text = _text;
  d->totalWidth = _totalWidth;
}

ProgressBar::~ProgressBar() { delete d; }

void ProgressBar::update(double _percentage)
{
  if(_percentage < 1.0)
  {
    int barWidth = d->totalWidth - d->text.length();

    std::cout << qPrintable(d->text) << " [";
    int pos = barWidth * _percentage;
    for(int i = 0; i < barWidth; ++i)
    {
      if(i < pos)
        std::cout << "=";
      else if(i == pos)
        std::cout << ">";
      else
        std::cout << " ";
    }
    std::cout << "] " << int(_percentage * 100.0) << " %\r";
    std::cout.flush();
  }
  else
  {
    std::cout << std::endl;
  }
}
