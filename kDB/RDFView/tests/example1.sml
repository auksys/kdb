Prefix ex: <http://ex.org/>.
Prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
Prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
Prefix xsd: <http://www.w3.org/2001/XMLSchema#>
 
Create View ex:employee_view As
    Construct {
        ?e  a ex:employee;
            rdfs:label ?l ;
            ex:age ?a .
    }
    With
        ?e = uri(ex:employee, ?EMPNO)
        ?l = plainLiteral(?ENAME)
        ?a = typedLiteral(?AGE, xsd:int)
    From
        EMP
