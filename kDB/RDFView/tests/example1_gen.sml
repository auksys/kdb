#// clang-format off

Prefix ex: <http://ex.org/>
Prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
Prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
Prefix xsd: <http://www.w3.org/2001/XMLSchema#>
CREATE VIEW <http://ex.org/employee_view> AS
  CONSTRUCT
  {
    ?e rdf:type ex:employee .
    ?e rdfs:label ?l .
    ?e ex:age ?a .
  }
  WITH
     ?a = typedLiteral(?AGE, <http://www.w3.org/2001/XMLSchema#int>)
     ?e = uri(<http://ex.org/employee>, ?EMPNO)
     ?l = plainLiteral(?ENAME)

  FROM
    EMP
