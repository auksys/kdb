/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "TestParser.h"

#include <knowCore/Messages.h>

#include <knowCore/Uris/rdf.h>
#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>
#include <knowRDF/Triple.h>

#include "../Expression.h"
#include "../ViewDefinition.h"

#include <knowCore/Test.h>

namespace RDFView = kDB::RDFView;

namespace
{
  knowRDF::Subject createSubject(const QString& _uri, knowRDF::Subject::Type _type)
  {
    return knowRDF::Subject(_uri, _type);
  }
#if 0
  knowRDF::Subject createSubject(const knowRDF::BlankNode& _bn, knowRDF::Subject::Type)
  {
    return knowRDF::Subject(_bn);
  }
#endif
  knowRDF::Object createObject(const QString& _string, knowRDF::Object::Type _type)
  {
    return knowRDF::Object(_string, _type);
  }
} // namespace

#define COMPARE_SPO(__idx__, __subject_url__, __subject_type__, __predicate_url__, __value__,      \
                    __object_type__)                                                               \
  {                                                                                                \
    knowRDF::Triple t1 = definition.triples()[__idx__];                                            \
    QCOMPARE(t1.subject(), createSubject(__subject_url__, __subject_type__));                      \
    QCOMPARE(t1.predicate(), knowCore::Uri(__predicate_url__));                                    \
    QCOMPARE(t1.object(), createObject(__value__, __object_type__));                               \
  }

void TestParser::testExample1()
{
  // Read example1.sml
  QFile file(":/example1.sml");
  file.open(QIODevice::ReadOnly);
  knowCore::Messages msgs;
  RDFView::ViewDefinition definition = RDFView::ViewDefinition::parse(&file, &msgs);
  QVERIFY(definition.isValid());
  QCOMPARE(definition.name(), knowCore::Uri("http://ex.org/employee_view"));
  QCOMPARE(definition.triples().size(), 3);

  COMPARE_SPO(0, "e", knowRDF::Subject::Type::Variable, knowCore::Uris::rdf::a,
              "http://ex.org/employee", knowRDF::Object::Type::Uri);
  COMPARE_SPO(1, "e", knowRDF::Subject::Type::Variable,
              "http://www.w3.org/2000/01/rdf-schema#label", "l", knowRDF::Object::Type::Variable);
  COMPARE_SPO(2, "e", knowRDF::Subject::Type::Variable, "http://ex.org/age", "a",
              knowRDF::Object::Type::Variable);

  QCOMPARE(definition.expressions()["e"].toString(),
           QString("uri(<http://ex.org/employee>, ?EMPNO)"));
  QCOMPARE(definition.expressions()["l"].toString(), QString("plainLiteral(?ENAME)"));
  QCOMPARE(definition.expressions()["a"].toString(),
           QString("typedLiteral(?AGE, <http://www.w3.org/2001/XMLSchema#int>)"));

  QCOMPARE(definition.sqlView(), QString("EMP"));

  // Read example1_gen.sml
  QFile file_gen(":/example1_gen.sml");
  file_gen.open(QIODevice::ReadOnly);
  QString string_gen = file_gen.readAll();
  QCOMPARE(definition.toString("SML"), string_gen);
  msgs.clear();
  RDFView::ViewDefinition definition2 = RDFView::ViewDefinition::parse(string_gen, &msgs);
  QVERIFY(definition2.isValid());

  // Read example1a.sml
  QFile filea(":/example1a.sml");
  filea.open(QIODevice::ReadOnly);
  RDFView::ViewDefinition definition_a = RDFView::ViewDefinition::parse(&filea, &msgs);
  QVERIFY(definition_a.isValid());
  QCOMPARE(definition_a.name(), knowCore::Uri("http://ex.org/employee_view"));
  QCOMPARE(definition_a.triples().size(), 3);
  QCOMPARE(definition_a.sqlView(), QString("SELECT EMPNO, ENAME, AGE FROM EMP"));
}

QTEST_MAIN(TestParser)
