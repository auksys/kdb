#include <QSharedDataPointer>

#include <kDB/Forward.h>
#include <knowCore/ValueHash.h>

namespace kDB::RDFView
{
  class Expression;
  class ViewDefinition
  {
  public:
    ViewDefinition();
    ViewDefinition(const knowCore::Uri& _name, const QList<knowRDF::Triple>& _triples,
                   const QHash<QString, Expression>& _expressions, const Expression& _key,
                   const QString& sqlView, const knowCore::UriManager& _uriManager);
    ViewDefinition(const ViewDefinition& _rhs);
    ViewDefinition& operator=(const ViewDefinition& _rhs);
    ~ViewDefinition();
    /**
     * Parse a view definition using the SML languages as defined in @a
     * http://sparqlify.org/wiki/SML
     */
    static ViewDefinition parse(QIODevice* _device, knowCore::Messages* _msgs = nullptr,
                                const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                const QString& _format = "SML");
    static ViewDefinition parse(const QString& _string, knowCore::Messages* _msgs = nullptr,
                                const knowCore::ValueHash& _bindings = knowCore::ValueHash(),
                                const QString& _format = "SML");
    QString toString(const QString& _format = "SML") const;
    /**
     * @return true if the definition is valid
     */
    bool isValid() const;
  public:
    knowCore::Uri name() const;
    QList<knowRDF::Triple> triples() const;
    QHash<QString, Expression> expressions() const;
    /**
     * @return an expression that give a unique key that can be used to generate blank node
     */
    Expression key() const;
    QString sqlView() const;
    knowCore::UriManager uriManager() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };

} // namespace kDB::RDFView
