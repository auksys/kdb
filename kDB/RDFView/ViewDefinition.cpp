#include "ViewDefinition.h"

#include <QBuffer>
#include <QHash>

#include <knowCore/Messages.h>
#include <knowCore/UriManager.h>

#include <knowRDF/Triple.h>

#include "Expression.h"

#include "SML/Generator.h"
#include "SML/Lexer_p.h"
#include "SML/Parser_p.h"

using namespace kDB::RDFView;

struct ViewDefinition::Private : public QSharedData
{
  Private() : valid(false) {}
  bool valid;
  knowCore::Uri name;
  QString sqlView;
  QList<knowRDF::Triple> triples;
  QHash<QString, Expression> expressions;
  Expression key;
  knowCore::UriManager uriManager;
};

ViewDefinition::ViewDefinition() : d(new Private) {}

ViewDefinition::ViewDefinition(const knowCore::Uri& _name, const QList<knowRDF::Triple>& _triples,
                               const QHash<QString, Expression>& _expressions,
                               const Expression& _key, const QString& _sqlView,
                               const knowCore::UriManager& _uriManager)
    : d(new Private)
{
  d->valid = true;
  d->name = _name;
  d->triples = _triples;
  d->expressions = _expressions;
  d->sqlView = _sqlView;
  d->key = _key;
  d->uriManager = _uriManager;
}

ViewDefinition::ViewDefinition(const ViewDefinition& _rhs) : d(_rhs.d) {}

ViewDefinition& ViewDefinition::operator=(const ViewDefinition& _rhs)
{
  d = _rhs.d;
  return *this;
}

ViewDefinition::~ViewDefinition() {}

ViewDefinition ViewDefinition::parse(QIODevice* _device, knowCore::Messages* _msgs,
                                     const knowCore::ValueHash& _bindings, const QString& _format)
{
  Q_UNUSED(_format);
  SML::Lexer l(_device);
  SML::Parser p(&l, _bindings);
  ViewDefinition def = p.parse();
  if(_msgs)
  {
    *_msgs = p.messages();
  }
  return def;
}

ViewDefinition ViewDefinition::parse(const QString& _string, knowCore::Messages* _msgs,
                                     const knowCore::ValueHash& _bindings, const QString& _format)
{
  Q_UNUSED(_format);
  QByteArray arr = _string.toUtf8();
  QBuffer buffer(&arr);
  buffer.open(QIODevice::ReadOnly);
  return parse(&buffer, _msgs, _bindings, _format);
}

QString ViewDefinition::toString(const QString& _format) const
{
  Q_UNUSED(_format);
  SML::Generator gen;
  return gen.generate(*this);
}

bool ViewDefinition::isValid() const { return d->valid; }

knowCore::Uri ViewDefinition::name() const { return d->name; }

QHash<QString, Expression> ViewDefinition::expressions() const { return d->expressions; }

Expression ViewDefinition::key() const { return d->key; }

QString ViewDefinition::sqlView() const { return d->sqlView; }

QList<knowRDF::Triple> ViewDefinition::triples() const { return d->triples; }

knowCore::UriManager ViewDefinition::uriManager() const { return d->uriManager; }
