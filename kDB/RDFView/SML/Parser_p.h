#include <knowCore/Forward.h>

namespace kDB::RDFView
{
  class ViewDefinition;
  namespace SML
  {
    class Lexer;
    class Parser
    {
    public:
      Parser(Lexer* _lexer, const knowCore::ValueHash& _bindings);
      ~Parser();
      ViewDefinition parse();
      const knowCore::Messages& messages() const;
    private:
      struct PrivateBase;
      struct Private;
      Private* const d;
    };
  } // namespace SML
} // namespace kDB::RDFView
