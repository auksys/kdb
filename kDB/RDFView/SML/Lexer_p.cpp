/*
 *  Copyright (c) 2008,2010 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Lexer_p.h"

#include <QIODevice>
#include <QString>
#include <stdio.h>

#include "Token_p.h"
#include <clog_qt>

using namespace kDB::RDFView::SML;

#define IDENTIFIER_IS_KEYWORD(tokenname, tokenid)                                                  \
  if(identifierStr == tokenname)                                                                   \
  {                                                                                                \
    return Token(Token::tokenid, firstChar.line, firstChar.column);                                \
  }

#define CHAR_IS_TOKEN(tokenchar, tokenid)                                                          \
  if(lastChar == tokenchar)                                                                        \
  {                                                                                                \
    return Token(Token::tokenid, firstChar.line, firstChar.column);                                \
  }

#define CHAR_IS_TOKEN_OR_TOKEN(tokenchar, tokendecidechar, tokenid_1, tokenid_2)                   \
  if(lastChar == tokenchar)                                                                        \
  {                                                                                                \
    knowCore::LexerTextStream::Element nextChar = d->stream.getNextChar();                         \
    if(nextChar == tokendecidechar)                                                                \
    {                                                                                              \
      return Token(Token::tokenid_2, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      d->stream.unget(nextChar);                                                                   \
      return Token(Token::tokenid_1, firstChar.line, firstChar.column);                            \
    }                                                                                              \
  }

#define CHAR_IS_TOKEN_OR_TOKEN_OR_TOKEN(tokenchar_1, tokenchar_2, tokenchar_3, tokenid_1,          \
                                        tokenid_2, tokenid_3)                                      \
  if(lastChar == tokenchar_1)                                                                      \
  {                                                                                                \
    knowCore::LexerTextStream::Element nextChar = d->stream.getNextChar();                         \
    if(nextChar == tokenchar_2)                                                                    \
    {                                                                                              \
      return Token(Token::tokenid_2, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else if(nextChar == tokenchar_3)                                                               \
    {                                                                                              \
      return Token(Token::tokenid_3, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      unget(nextChar);                                                                             \
      return Token(Token::tokenid_1, firstChar.line, firstChar.column);                            \
    }                                                                                              \
  }

#define CHAR_IS_TOKEN_OR_TOKEN_OR_TOKEN_OR_TOKEN(                                                  \
  tokenchar_1, tokenchar_2, tokenchar_3, tokenchar_4, tokenid_1, tokenid_2, tokenid_3, tokenid_4)  \
  if(lastChar == tokenchar_1)                                                                      \
  {                                                                                                \
    knowCore::LexerTextStream::Element nextChar = d->stream.getNextChar();                         \
    if(nextChar == tokenchar_2)                                                                    \
    {                                                                                              \
      return Token(Token::tokenid_2, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else if(nextChar == tokenchar_3)                                                               \
    {                                                                                              \
      return Token(Token::tokenid_3, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else if(nextChar == tokenchar_4)                                                               \
    {                                                                                              \
      return Token(Token::tokenid_4, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      unget(nextChar);                                                                             \
      return Token(Token::tokenid_1, firstChar.line, firstChar.column);                            \
    }                                                                                              \
  }

struct Lexer::Private
{
  knowCore::LexerTextStream stream;
  bool curieLexingEnabled = false;
};

Lexer::Lexer(QIODevice* sstream) : d(new Private) { d->stream.setDevice(sstream); }

Lexer::~Lexer() { delete d; }

void Lexer::setCurieLexingEnabled(bool _v) { d->curieLexingEnabled = _v; }

bool Lexer::isCurieLexingEnabled() const { return d->curieLexingEnabled; }

QString Lexer::getIdentifier(knowCore::LexerTextStream::Element lastChar)
{
  QString identifierStr;
  if(lastChar != 0)
  {
    identifierStr = lastChar.content;
  }
  while(not d->stream.eof())
  {
    lastChar = d->stream.getNextChar();
    if(lastChar.isLetterOrDigit() or lastChar == '_')
    {
      identifierStr += lastChar.content;
    }
    else
    {
      d->stream.unget(lastChar);
      break;
    }
  }
  return identifierStr;
}

Token Lexer::getDigit(knowCore::LexerTextStream::Element lastChar)
{
  auto [number, isinteger] = d->stream.getDigit(lastChar);

  if(isinteger)
  {
    return Token(Token::INTEGER_CONSTANT, number.content, number.line, number.column);
  }
  else
  {
    return Token(Token::FLOAT_CONSTANT, number.content, number.line, number.column);
  }
}

Token Lexer::getString(int terminator, Token::Type _type, bool _tripleEnding)
{
  QString term = QChar(terminator);
  if(_tripleEnding)
  {
    term = term + term + term;
  }

  auto [string, finished] = d->stream.getString(term);

  if(finished)
  {
    return Token(_type, string.content, string.line, string.column);
  }
  else
  {
    return Token(Token::UNFINISHED_STRING, string.content, string.line, string.column);
  }
}

bool Lexer::isTriple(const QString& _char)
{
  clog_assert(_char.size() == 1);
  knowCore::LexerTextStream::Element char2 = d->stream.getNextChar();
  if(char2.content != _char)
  {
    d->stream.unget(char2);
    return false;
  }
  knowCore::LexerTextStream::Element char3 = d->stream.getNextChar();
  if(char3.content != _char)
  {
    d->stream.unget(char3);
    d->stream.unget(char2);
    return false;
  }
  return true;
}

Token Lexer::nextToken()
{
  knowCore::LexerTextStream::Element lastChar = d->stream.getNextNonSeparatorChar();
  const knowCore::LexerTextStream::Element firstChar = lastChar;
  if(lastChar.eof)
  {
    return Token(Token::END_OF_FILE, firstChar.line, firstChar.column);
  }
  QString identifierStr;
  // Test for comment

  if(lastChar == '#')
  {
    // Starting a comment
    while(not d->stream.eof())
    {
      lastChar = d->stream.getNextChar();
      if(lastChar == '\n')
      {
        return nextToken();
      }
    }
  }

  // binding
  if(lastChar == '%')
  {
    QString name = '%' + getIdentifier(d->stream.getNextChar());
    return Token(Token::BINDING, name, firstChar.line, firstChar.column);
  }

  if(lastChar == '<')
  {
    // This could be an URI
    knowCore::LexerTextStream::Element nc;
    while((nc = d->stream.getNextChar()).isSpace())
    {
    }
    d->stream.unget(nc);
    return getString('>', Token::URI_CONSTANT, false);
  }

  if(lastChar == '_')
  {
    knowCore::LexerTextStream::Element nextChar = d->stream.getNextChar();
    if(nextChar == ':')
    {
      return Token(Token::UNDERSCORECOLON, firstChar.line, firstChar.column);
    }
    else
    {
      d->stream.unget(nextChar);
    }
  }

  // if it is alpha, it's an identifier or a keyword
  if(lastChar == ':' and d->curieLexingEnabled)
  {
    knowCore::LexerTextStream::Element nc = d->stream.getNextChar();
    if(nc.isSpace())
    {
      d->stream.unget(nc);
      return Token(knowCore::Curie(), firstChar.line, firstChar.column);
    }
    else
    {
      return Token(knowCore::Curie(QString(), getIdentifier(nc)), firstChar.line, firstChar.column);
    }
  }
  else if(lastChar.isLetter() or lastChar == '_' or lastChar == '@')
  {
    identifierStr = getIdentifier(lastChar);

    QString identifier_str_orig = identifierStr;
    // Case sensitive
    IDENTIFIER_IS_KEYWORD("true", TRUE);
    IDENTIFIER_IS_KEYWORD("false", FALSE);

    // Case insensitive
    identifierStr = identifierStr.toUpper();

    IDENTIFIER_IS_KEYWORD("A", A);
    IDENTIFIER_IS_KEYWORD("AS", AS);
    IDENTIFIER_IS_KEYWORD("PREFIX", PREFIX);
    IDENTIFIER_IS_KEYWORD("@PREFIX", PREFIX);
    IDENTIFIER_IS_KEYWORD("BASE", BASE);
    IDENTIFIER_IS_KEYWORD("@BASE", BASE);
    IDENTIFIER_IS_KEYWORD("CONSTRAIN", CONSTRAIN);
    IDENTIFIER_IS_KEYWORD("CONSTRUCT", CONSTRUCT);
    IDENTIFIER_IS_KEYWORD("CREATE", CREATE);
    IDENTIFIER_IS_KEYWORD("FROM", FROM);
    IDENTIFIER_IS_KEYWORD("PREFIX", PREFIX);
    IDENTIFIER_IS_KEYWORD("VIEW", VIEW);
    IDENTIFIER_IS_KEYWORD("WITH", WITH);

    if(identifierStr[0] == '@')
    {
      return Token(Token::LANG_TAG, identifier_str_orig.right(identifier_str_orig.length() - 1),
                   firstChar.line, firstChar.column);
    }
    if(d->curieLexingEnabled)
    {
      knowCore::LexerTextStream::Element nc = d->stream.getNextChar();
      if(nc == ':')
      {
        nc = d->stream.getNextChar();
        if(nc.isSpace())
        {
          d->stream.unget(nc);
          return Token(knowCore::Curie(identifier_str_orig, QString()), firstChar.line,
                       firstChar.column);
        }
        else
        {
          QString name = getIdentifier(nc);
          return Token(knowCore::Curie(identifier_str_orig, name), firstChar.line,
                       firstChar.column);
        }
      }
      else
      {
        d->stream.unget(nc);
      }
    }

    return Token(Token::IDENTIFIER, identifier_str_orig, firstChar.line, firstChar.column);
  }
  else if(lastChar.isDigit())
  {
    // if it's a digit
    return getDigit(lastChar);
  }
  else if(lastChar == '"')
  {
    return getString('"', Token::STRING_CONSTANT, isTriple("\""));
  }
  else
  {
    CHAR_IS_TOKEN(';', SEMI);
    CHAR_IS_TOKEN('.', DOT);
    CHAR_IS_TOKEN(',', COMA);
    CHAR_IS_TOKEN(':', COLON);
    CHAR_IS_TOKEN('(', STARTBRACKET);
    CHAR_IS_TOKEN(')', ENDBRACKET);
    CHAR_IS_TOKEN('[', STARTBOXBRACKET);
    CHAR_IS_TOKEN(']', ENDBOXBRACKET);
    CHAR_IS_TOKEN('{', STARTBRACE);
    CHAR_IS_TOKEN('}', ENDBRACE);
    CHAR_IS_TOKEN('=', EQUAL);
    CHAR_IS_TOKEN('?', QUESTION);
    CHAR_IS_TOKEN_OR_TOKEN('^', '^', UNKNOWN, CIRCUMFLEXCIRCUMFLEX);
  }
  identifierStr = lastChar.content;
  clog_warning("Unknown token: {} '{}' at {}, {} ", lastChar.content, identifierStr, firstChar.line,
               firstChar.column);
  clog_assert(not lastChar.isSpace());
  return Token(Token::UNKNOWN, identifierStr, firstChar.line, firstChar.column);
}

QString Lexer::readUntil(const QString& arg1)
{
  QString text;
  while(true)
  {
    knowCore::LexerTextStream::Element c = d->stream.getNextChar();
    if(c.eof)
    {
      return QString();
    }
    else if(c.content == arg1[0])
    {
      int i = 1;
      QList<knowCore::LexerTextStream::Element> characters;
      for(; i < arg1.length(); ++i)
      {
        knowCore::LexerTextStream::Element nc = d->stream.getNextChar();
        characters.append(nc);
        if(nc.content != arg1[i])
        {
          break;
        }
      }
      for(int j = characters.length() - 1; j >= 0; --j)
      {
        // rewinding
        d->stream.unget(characters[j]);
      }
      if(i == arg1.length()) // We have found the pattern arg1
      {
        d->stream.unget(c); // also rewind the first character
        return text;
      }
      else
      {
        text += c.content;
      }
    }
    else
    {
      text += c.content;
    }
  }
}
