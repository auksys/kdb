#include "Generator.h"

#include <QBuffer>
#include <QString>
#include <QStringList>
#include <QTextStream>

#include <knowCore/Uri.h>
#include <knowCore/UriManager.h>

#include <knowRDF/Serialiser.h>
#include <knowRDF/Triple.h>

#include "../Expression.h"
#include "../ViewDefinition.h"

using namespace kDB::RDFView::SML;

QString Generator::generate(const kDB::RDFView::ViewDefinition& _definition)
{
  QBuffer buffer;
  buffer.open(QIODevice::WriteOnly);
  QTextStream stream(&buffer);
#include "SMLGenerator.h"
  buffer.close();
  return buffer.buffer();
}
