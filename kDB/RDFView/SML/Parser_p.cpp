#include "Parser_p.h"

#include <knowCore/TypeDefinitions.h>

#include "../Expression.h"
#include "../ViewDefinition.h"

#include "Lexer_p.h"
#include <knowRDF/Turtle/BaseParser_p.h>

using namespace kDB::RDFView::SML;

struct Parser::PrivateBase
{
  knowCore::Uri viewName;
  QList<knowRDF::Triple> triples;
  QHash<QString, Expression> expressions;
  QString sqlView;
  Expression key;

  void appendTriple(const knowRDF::Triple& _triple) { triples.append(_triple); }
};

struct Parser::Private : public knowRDF::Turtle::BaseParser<PrivateBase, Lexer, Token, true>
{
  Expression parseExpression();

  void parseConstruct();
  void parseWith();
  void parseFrom();
};

kDB::RDFView::Expression Parser::Private::parseExpression()
{
  using namespace kDB::RDFView;
  switch(currentToken.type)
  {
  case Token::QUESTION:
  {
    isOfType(getNextToken(), Token::IDENTIFIER);
    QString name = currentToken.string;
    getNextToken();
    return Expression(name, Expression::Type::Variable);
  }
  case Token::A:
  case Token::IDENTIFIER:
  {
    QString name = currentToken.type == Token::IDENTIFIER ? currentToken.string : QString("a");
    getNextToken();
    if(currentToken.type == Token::STARTBRACKET)
    {
      getNextToken();
      QList<Expression> arguments;
      while(true)
      {
        switch(currentToken.type)
        {
        case Token::END_OF_FILE:
          reportUnexpected(currentToken);
          return Expression();
        case Token::ENDBRACKET:
        {
          getNextToken();
          return Expression(name, arguments);
        }
        default:
        {
          arguments.append(parseExpression());
          if(currentToken.type == Token::COMA)
          {
            getNextToken();
          }
        }
        }
      }
    }
    else
    {
      isOfType(currentToken, Token::COLON);
      if(isOfType(getNextToken(), Token::IDENTIFIER))
      {
        knowCore::Curie curie(name, currentToken.string);
        if(curie.canResolve(&urlManager))
        {
          knowCore::Uri r = curie.resolve(&urlManager);
          getNextToken();
          return Expression(knowCore::Value::fromValue(r));
        }
        else
        {
          reportError(currentToken, clog_qt::qformat("Unknown curie prefix '{}'", name));
          getNextToken();
          return Expression();
        }
      }
    }
  }
    Q_FALLTHROUGH();
  case Token::CURIE_CONSTANT:
  case Token::URI_CONSTANT:
  {
    return Expression(knowCore::Value::fromValue(parseIri()));
  }
  case Token::STRING_CONSTANT:
  {
    QString val = currentToken.string;
    getNextToken();
    return Expression(val, Expression::Type::Value);
  }
  case Token::INTEGER_CONSTANT:
  {
    knowCore::Value value = knowCore::Value::fromValue(currentToken.string.toLongLong());
    getNextToken();
    return Expression(value);
  }
  case Token::FLOAT_CONSTANT:
  {
    knowCore::Value value = knowCore::Value::fromValue(currentToken.string.toDouble());
    getNextToken();
    return Expression(value);
  }
  case Token::BINDING:
  {
    if(bindings.contains(currentToken.string))
    {
      knowCore::Value value = bindings.value(currentToken.string);
      getNextToken();
      return Expression(value);
    }
    else
    {
      reportError(currentToken, clog_qt::qformat("Unknown binding '{}'", currentToken.string));
      getNextToken();
      return Expression();
    }
  }
  default:
    reportUnexpected(currentToken);
    getNextToken();
    return Expression();
  }
}

void Parser::Private::parseConstruct()
{
  isOfType(currentToken, Token::CONSTRUCT);
  getNextToken();
  isOfType(currentToken, Token::STARTBRACE);
  getNextToken();
  while(true)
  {
    switch(currentToken.type)
    {
    case Token::END_OF_FILE:
      reportUnexpected(currentToken, Token::ENDBRACE);
      return;
    case Token::ENDBRACE:
      getNextToken();
      return;
    case Token::CURIE_CONSTANT:
    case Token::URI_CONSTANT:
    case Token::IDENTIFIER:
    case Token::UNDERSCORECOLON:
    case Token::QUESTION:
    case Token::STARTBOXBRACKET:
      parseSingleSubject(parseSubject(), Token::DOT);
      break;
    default:
      reportUnexpected(currentToken);
      return;
    }
  }
}

void Parser::Private::parseWith()
{
  isOfType(currentToken, Token::WITH);
  getNextToken();
  while(true)
  {
    switch(currentToken.type)
    {
    case Token::QUESTION:
    {
      getNextToken();
      if(currentToken.type != Token::IDENTIFIER and currentToken.type != Token::A)
      {
        reportUnexpected(currentToken);
      }
      QString name = currentToken.type == Token::IDENTIFIER ? currentToken.string : "a";
      isOfType(getNextToken(), Token::EQUAL);
      getNextToken();
      expressions[name] = parseExpression();
      break;
    }
    case Token::IDENTIFIER:
      if(currentToken.string == "key")
      {
        isOfType(getNextToken(), Token::EQUAL);
        getNextToken();
        key = parseExpression();
        break;
      }
    default:
      return;
    }
  }
}

void Parser::Private::parseFrom()
{
  isOfType(currentToken, Token::FROM);
  getNextToken();
  switch(currentToken.type)
  {
  case Token::IDENTIFIER:
    sqlView = currentToken.string;
    break;
  case Token::STARTBOXBRACKET:
    isOfType(getNextToken(), Token::STARTBOXBRACKET);
    sqlView = lexer->readUntil("]]");
    isOfType(getNextToken(), Token::ENDBOXBRACKET);
    isOfType(getNextToken(), Token::ENDBOXBRACKET);
    break;
  default:
    reportUnexpected(currentToken);
    getNextToken();
  }
  isOfType(getNextToken(), Token::END_OF_FILE);
}

Parser::Parser(Lexer* _lexer, const knowCore::ValueHash& _bindings) : d(new Private)
{
  d->lexer = _lexer;
  d->bindings = _bindings;
}

Parser::~Parser() { delete d; }

const knowCore::Messages& Parser::messages() const { return d->messages; }

kDB::RDFView::ViewDefinition Parser::parse()
{
  d->lexer->setCurieLexingEnabled(false);
  d->getNextToken();

  if(d->currentToken.type == Token::BASE)
  {
    if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
    {
      d->urlManager.setBase(d->currentToken.string);
      d->getNextToken();
    }
    else
    {
      return kDB::RDFView::ViewDefinition();
    }
    if(d->currentToken.type == Token::DOT)
    {
      d->getNextToken();
    }
  }

  while(d->currentToken.type == Token::PREFIX)
  {
    switch(d->getNextToken().type)
    {
    case Token::IDENTIFIER:
    {
      QString ns = d->currentToken.string;
      d->isOfType(d->getNextToken(), Token::COLON);
      if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
      {
        d->urlManager.addPrefix(ns, d->currentToken.string);
      }
      else
      {
        return kDB::RDFView::ViewDefinition();
      }
      break;
    }
    case Token::COLON:
    {
      if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
      {
        d->urlManager.addPrefix(QString(), d->currentToken.string);
        d->getNextToken();
      }
      else
      {
        return kDB::RDFView::ViewDefinition();
      }
      break;
    }
    default:
      d->reportUnexpected(d->currentToken);
      return kDB::RDFView::ViewDefinition();
    }
    d->getNextToken();
    if(d->currentToken.type == Token::DOT)
    {
      d->getNextToken();
    }
  }
  d->lexer->setCurieLexingEnabled(true);
  if(not d->isOfType(d->currentToken, Token::CREATE))
  {
    return kDB::RDFView::ViewDefinition();
  }
  d->getNextToken();

  if(not d->isOfType(d->currentToken, Token::VIEW))
  {
    return kDB::RDFView::ViewDefinition();
  }
  d->getNextToken();

  d->viewName = d->parseIri();

  if(not d->isOfType(d->currentToken, Token::AS))
  {
    return kDB::RDFView::ViewDefinition();
  }
  d->getNextToken();

  if(not d->isOfType(d->currentToken, Token::CONSTRUCT))
  {
    return kDB::RDFView::ViewDefinition();
  }

  d->parseConstruct();

  if(not d->isOfType(d->currentToken, Token::WITH))
  {
    return kDB::RDFView::ViewDefinition();
  }

  d->parseWith();

  if(not d->isOfType(d->currentToken, Token::FROM))
  {
    return kDB::RDFView::ViewDefinition();
  }
  d->parseFrom();

  // Check that key is defined if there are blank nodes
  if(d->key.type() == Expression::Type::Unknown)
  {
    for(const knowRDF::Triple& triple : d->triples)
    {
      if(triple.subject().type() == knowRDF::Subject::Type::BlankNode
         or triple.object().type() == knowRDF::Object::Type::BlankNode)
      {
        d->messages.reportError("Missing 'key' needed when using blank nodes.");
        break;
      }
    }
  }

  if(d->messages.hasErrors())
  {
    return kDB::RDFView::ViewDefinition();
  }
  else
  {
    return kDB::RDFView::ViewDefinition(d->viewName, d->triples, d->expressions, d->key, d->sqlView,
                                        d->urlManager);
  }
}
