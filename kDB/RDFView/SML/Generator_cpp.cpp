#// clang-format off

/% if(not _definition.uriManager().base().isEmpty())
{
%/@base </%= (QString)_definition.uriManager().base() %/>
/%
}
QHash<QString, knowCore::Uri> prefixes = _definition.uriManager().prefixes();
QStringList prefixes_keys = prefixes.keys();
prefixes_keys.sort();
for(QString key : prefixes_keys)
{
%/Prefix /%= key %/: </%= (QString)prefixes[key] %/>
/%
}
%/CREATE VIEW </%= _definition.name() %/> AS
  CONSTRUCT
  {
/%
    knowRDF::Serialiser s(&stream, _definition.uriManager());
    for(const knowRDF::Triple& _triple : _definition.triples())
    {
%/    /% s.serialise(_triple);
    }
%/  }
  WITH
/%
    QHash<QString, Expression> expressions = _definition.expressions();
    QStringList expressions_keys = expressions.keys();
    expressions_keys.sort();
    for(QString key : expressions_keys)
    {
%/     ?/%= key %/ = /%= expressions[key].toString() %/
/%
    }
    if(_definition.key().type() != Expression::Type::Unknown)
    {%/
    key = /%= _definition.key().toString() %//%
    }
%/
  FROM/%
  if(_definition.sqlView().contains(' '))
  {%/
    [[/%= _definition.sqlView() %/]]/% 
  } else {%/
    /%= _definition.sqlView() %//%
  }%/
