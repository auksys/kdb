class QString;

namespace kDB
{
  namespace RDFView
  {
    class ViewDefinition;
    namespace SML
    {
      class Generator
      {
      public:
        QString generate(const ViewDefinition& _definition);
      };
    } // namespace SML
  } // namespace RDFView
} // namespace kDB
