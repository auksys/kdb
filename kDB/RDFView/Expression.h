#include <kDB/Forward.h>

#include <QSharedDataPointer>

namespace kDB::RDFView
{
  class Expression
  {
  public:
    enum class Type
    {
      Unknown,
      FunctionCall,
      Variable,
      Value
    };
  public:
    Expression();
    Expression(const QString& _name, const QList<Expression>& _expressions);
    Expression(const QString& _string, Type _type);
    Expression(const knowCore::Value& _value);
    Expression(const Expression& _rhs);
    Expression& operator=(const Expression& _rhs);
    ~Expression();
    Type type() const;
    QString name() const;
    QList<Expression> arguments() const;
    knowCore::Value value() const;
    QString toString() const;
  private:
    struct Private;
    QSharedDataPointer<Private> d;
  };
} // namespace kDB::RDFView

#include <knowCore/MetaType.h>
KNOWCORE_DECLARE_FULL_METATYPE(kDB::RDFView, Expression);
