#include "Expression.h"

#include <clog_qt>
#include <knowCore/Uri.h>
#include <knowCore/Value.h>

using namespace kDB::RDFView;

struct Expression::Private : public QSharedData
{
  Type type;
  knowCore::Value value;
  QString name;
  QList<Expression> arguments;
};

Expression::Expression() : d(new Private) { d->type = Type::Unknown; }

Expression::Expression(const QString& _name, const QList<Expression>& _expressions) : Expression()
{
  d->type = Type::FunctionCall;
  d->name = _name;
  d->arguments = _expressions;
}

Expression::Expression(const QString& _string, Expression::Type _type) : Expression()
{
  d->type = _type;
  switch(d->type)
  {
  case Type::Value:
    d->value = knowCore::Value::fromValue(_string);
    break;
  case Type::Variable:
    d->name = _string;
    break;
  case Type::FunctionCall:
  case Type::Unknown:
    clog_fatal("Unsupported string expression");
  }
}

Expression::Expression(const knowCore::Value& _value) : Expression()
{
  d->type = Type::Value;
  d->value = _value;
}

Expression::Expression(const Expression& _rhs) : d(_rhs.d) {}

Expression& Expression::operator=(const Expression& _rhs)
{
  d = _rhs.d;
  return *this;
}

Expression::~Expression() {}

QList<Expression> Expression::arguments() const { return d->arguments; }

QString Expression::name() const { return d->name; }

Expression::Type Expression::type() const { return d->type; }

knowCore::Value Expression::value() const { return d->value; }

QString Expression::toString() const
{
  switch(d->type)
  {
  case Expression::Type::FunctionCall:
  {
    QString r = d->name + "(";

    for(int i = 0; i < d->arguments.size(); ++i)
    {
      if(i != 0)
        r += ", ";
      r += d->arguments[i].toString();
    }

    return r + ")";
  }
  case Expression::Type::Unknown:
    return "unknown";
  case Expression::Type::Value:
    if(d->value.datatype() == knowCore::UriMetaTypeInformation::uri())
    {
      return "<" + d->value.value<QString>().expect_success() + ">";
    }
    else
    {
      return d->value.value<QString>().expect_success();
    }
  case Expression::Type::Variable:
    return "?" + d->name;
  }
  qFatal("Expression::toString, invalid type");
}

#include <knowCore/MetaTypeImplementation.h>
#include <knowCore/Uris/askcore_datatype.h>
KNOWCORE_DEFINE_QT_METATYPE(kDB::RDFView::Expression, knowCore::MetaTypeTraits::None)
