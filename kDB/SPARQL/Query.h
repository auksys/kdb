/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#ifndef _KDB_SPARQL_QUERY_H_
#define _KDB_SPARQL_QUERY_H_

#include <QSharedDataPointer>

#include <kDB/Forward.h>

namespace kDB::SPARQL
{
  namespace Algebra
  {
    class Node;
  }
  class Query
  {
  protected:
    struct Private;
    Query(Private* _private);
  public:
    enum class Type
    {
      Invalid,
      Select,
      Construct,
      Describe,
      Ask,
      Update,
      Execute,
      Explain,
      PL
    };
  public:
    Query();
    Query(const Query& _rhs);
    Query& operator=(const Query& _rhs);
    ~Query();
    const Algebra::Node* getNode() const;
    Type type() const;
  public:
    static QList<Query> parse(const QByteArray& _query, const knowCore::ValueHash& _bindings,
                              knowCore::Messages* _messages, const knowCore::Uri& _base);
    static QList<Query> parse(const QString& _query, const knowCore::ValueHash& _bindings,
                              knowCore::Messages* _messages, const knowCore::Uri& _base);
  protected:
    QSharedDataPointer<Private> d;
  };
} // namespace kDB::SPARQL

#endif
