#pragma once

#include <QtTest/QtTest>

class TestLexer : public QObject
{
  Q_OBJECT
private slots:
  void testSimpleSelect();
  void testFilter();
};
