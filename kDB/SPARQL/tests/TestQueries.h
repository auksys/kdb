QByteArray simpleSelect = "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n"
                          "SELECT ?name\n"
                          "WHERE {\n"
                          "    ?person foaf:name ?name .\n"
                          "};";

QByteArray twoTriplesSelect = "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n"
                              "SELECT ?firstName ?familyName\n"
                              "WHERE {\n"
                              "    ?person foaf:firstName ?firstName .\n"
                              "    ?person foaf:familyName ?familyName .\n"
                              "};";
