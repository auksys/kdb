#include "TestLexer.h"

#include "../Lexer_p.h"

#include "../Token_p.h"
#include "TestQueries.h"

namespace QTest
{
  template<>
  inline char* toString<kDB::SPARQL::Token::Type>(const kDB::SPARQL::Token::Type& _t)
  {
    return toString(qPrintable(kDB::SPARQL::Token::typeToString(_t)));
  }
} // namespace QTest

void TestLexer::testSimpleSelect()
{
  QBuffer buffer(&simpleSelect);
  kDB::SPARQL::Lexer lexer(&buffer, knowCore::ValueHash());
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::PREFIX);
  kDB::SPARQL::Token tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::NAME);
  QCOMPARE(tok.string, QString("foaf"));
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::COLON);
  tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::URI_CONSTANT);
  QCOMPARE(tok.string, QString("http://xmlns.com/foaf/0.1/"));
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::SELECT);
  tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::VARIABLE);
  QCOMPARE(tok.string, QString("name"));
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::WHERE);
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::STARTBRACE);
  tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::VARIABLE);
  QCOMPARE(tok.string, QString("person"));
  tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::NAME);
  QCOMPARE(tok.string, QString("foaf"));
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::COLON);
  tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::NAME);
  QCOMPARE(tok.string, QString("name"));
  tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::VARIABLE);
  QCOMPARE(tok.string, QString("name"));
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::DOT);
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::ENDBRACE);
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::SEMI);
}

void TestLexer::testFilter()
{
  QByteArray filter = "FILTER (?z = 10) ";
  QBuffer buffer(&filter);
  kDB::SPARQL::Lexer lexer(&buffer, knowCore::ValueHash());
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::FILTER);
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::STARTBRACKET);
  kDB::SPARQL::Token tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::VARIABLE);
  QCOMPARE(tok.string, QString("z"));
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::EQUAL);
  tok = lexer.nextToken();
  QCOMPARE(tok.type, kDB::SPARQL::Token::INTEGER_CONSTANT);
  QCOMPARE(tok.string, QString("10"));
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::ENDBRACKET);
  QCOMPARE(lexer.nextToken().type, kDB::SPARQL::Token::END_OF_FILE);
}

QTEST_MAIN(TestLexer)
#include "moc_TestLexer.cpp"
