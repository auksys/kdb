#pragma once

#include <QtTest/QtTest>

class TestParser : public QObject
{
  Q_OBJECT
private slots:
  void testSimpleSelect();
  void testTwoTriplesSelect();
};
