#include "TestParser.h"

#include "../Lexer_p.h"
#include "../Parser_p.h"

#include "TestQueries.h"

#include <knowCore/Messages.h>
#include <knowCore/Uri.h>
#include <knowCore/UriManager.h>

namespace SPARQL = kDB::SPARQL;

void TestParser::testSimpleSelect()
{
  QBuffer buffer(&simpleSelect);
  SPARQL::Lexer lexer(&buffer, knowCore::ValueHash());
  SPARQL::Parser parser(&lexer, knowCore::ValueHash(), knowCore::Uri());
  SPARQL::Algebra::NodeCSP q = parser.parse().front();
  QVERIFY(q);
  QVERIFY(not parser.messages().hasErrors());
  QVERIFY(not parser.messages().hasWarnings());

#if 0
  SPARQL::SelectQuery* sq = dynamic_cast<SPARQL::SelectQuery*>(q);
  QVERIFY(sq);
  QCOMPARE(sq->uriManager().uri("foaf"), knowCore::Uri("http://xmlns.com/foaf/0.1/"));
  QCOMPARE(sq->variables().size(), 1);
  const SPARQL::Variable* v = sq->variables().front();
  QCOMPARE(v->name(), QString("name"));
  QVERIFY(not v->expression());
  const SPARQL::TripleExpression* triple = dynamic_cast<const SPARQL::TripleExpression*>(sq->where());
  QVERIFY(triple);
  QCOMPARE(triple->subject().type, SPARQL::ExpressionTerm::Type::Variable);
  QCOMPARE(triple->subject().value, QVariant("person"));
  QCOMPARE(triple->verb().type, SPARQL::ExpressionTerm::Type::Value);
  QCOMPARE(triple->verb().value.value<knowCore::Uri>(), knowCore::Uri("http://xmlns.com/foaf/0.1/name"));
  QCOMPARE(triple->object().type, SPARQL::ExpressionTerm::Type::Variable);
  QCOMPARE(triple->object().value, QVariant("name"));

  delete q;
#endif
}

void TestParser::testTwoTriplesSelect()
{
  QBuffer buffer(&twoTriplesSelect);
  SPARQL::Lexer lexer(&buffer, knowCore::ValueHash());
  SPARQL::Parser parser(&lexer, knowCore::ValueHash(), knowCore::Uri());
  SPARQL::Algebra::NodeCSP q = parser.parse().front();

  QVERIFY(q);
  QVERIFY(not parser.messages().hasErrors());
  QVERIFY(not parser.messages().hasWarnings());

#if 0
  SPARQL::SelectQuery* sq = dynamic_cast<SPARQL::SelectQuery*>(q);
  QVERIFY(sq);
  QCOMPARE(sq->uriManager().uri("foaf"), knowCore::Uri("http://xmlns.com/foaf/0.1/"));
  QCOMPARE(sq->variables().size(), 2);
  QCOMPARE(sq->variables()[0]->name(), QString("firstName"));
  QVERIFY(not sq->variables()[0]->expression());
  QCOMPARE(sq->variables()[1]->name(), QString("familyName"));
  QVERIFY(not sq->variables()[1]->expression());
  
  const SPARQL::MatchExpressionGroup* meg = dynamic_cast<const SPARQL::MatchExpressionGroup*>(sq->where());
  QVERIFY(meg);
  QCOMPARE(meg->expressions().size(), 2);
  
  const SPARQL::TripleExpression* triple = dynamic_cast<const SPARQL::TripleExpression*>(meg->expressions()[0]);
  QVERIFY(triple);
  QCOMPARE(triple->subject().type, SPARQL::ExpressionTerm::Type::Variable);
  QCOMPARE(triple->subject().value, QVariant("person"));
  QCOMPARE(triple->verb().type, SPARQL::ExpressionTerm::Type::Value);
  QCOMPARE(triple->verb().value.value<knowCore::Uri>(), knowCore::Uri("http://xmlns.com/foaf/0.1/firstName"));
  QCOMPARE(triple->object().type, SPARQL::ExpressionTerm::Type::Variable);
  QCOMPARE(triple->object().value, QVariant("firstName"));

  triple = dynamic_cast<const SPARQL::TripleExpression*>(meg->expressions()[1]);
  QVERIFY(triple);
  QCOMPARE(triple->subject().type, SPARQL::ExpressionTerm::Type::Variable);
  QCOMPARE(triple->subject().value, QVariant("person"));
  QCOMPARE(triple->verb().type, SPARQL::ExpressionTerm::Type::Value);
  QCOMPARE(triple->verb().value.value<knowCore::Uri>(), knowCore::Uri("http://xmlns.com/foaf/0.1/familyName"));
  QCOMPARE(triple->object().type, SPARQL::ExpressionTerm::Type::Variable);
  QCOMPARE(triple->object().value, QVariant("familyName"));

  delete q;
#endif
}

QTEST_MAIN(TestParser)
#include "moc_TestParser.cpp"
