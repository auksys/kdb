#include "Result_p.h"

using namespace kDB::SPARQL;

Result::Result() : d(new Private) { d->type = Type::Invalid; }

Result::Result(const QString& _query, const QString& _error) : d(new Private)
{
  d->query = _query;
  d->error = _error;
  d->success = false;
  d->type = Type::Failed;
}

Result::Result(const QString& _query, const char* _error) : Result(_query, QString(_error)) {}

Result::Result(const QString& _query) : d(new Private)
{
  d->query = _query;
  d->success = true;
  d->type = Type::Boolean;
  d->boolean = true;
}

Result::Result(const QString& _query, bool _boolean) : d(new Private)
{
  d->query = _query;
  d->success = true;
  d->type = Type::Boolean;
  d->boolean = _boolean;
}

Result::Result(const QString& _query, QStringList _fields, const QList<QVariantList>& _data)
    : d(new Private)
{
  d->fields = _fields;
  d->data = _data;
  d->query = _query;
  d->type = Type::VariableBinding;
  d->success = true;
}

Result::Result(const Result& _rhs) : d(_rhs.d) {}

Result& Result::operator=(const Result& _rhs)
{
  d = _rhs.d;
  return *this;
}

Result::~Result() {}

QString Result::error() const { return d->error; }

QString Result::query() const { return d->query; }

Result::operator bool() const { return d->success; }

int Result::fieldIndex(const QString& _field) const { return d->fields.indexOf(_field); }

QString Result::fieldName(int _index) const { return d->fields.at(_index); }

int Result::fields() const { return d->fields.count(); }

QStringList Result::fieldNames() const { return d->fields; }

int Result::tuples() const { return d->data.count(); }

QVariant Result::value(int _tuple, const char* _field) const
{
  return d->data[_tuple][fieldIndex(_field)];
}

QVariant Result::value(int _tuple, const QByteArray& _field) const
{
  return d->data[_tuple][fieldIndex(_field)];
}

QVariant Result::value(int _tuple, int _field) const { return d->data[_tuple][_field]; }

cres_qresult<void> Result::read(QIODevice* _device, const QString& _format,
                                const knowCore::Uri& _base)
{
  if(_format == knowCore::FileFormat::JSON)
  {
    return d->read_json(_device, _base);
  }
  else if(_format == knowCore::FileFormat::SRX or _format == knowCore::FileFormat::XML)
  {
    return d->read_srx(_device, _base);
  }
  else if(_format == knowCore::FileFormat::Turtle)
  {
    return d->read_rdf(_device, _format, _base);
  }
  else
  {
    return cres_failure("unsupported file format: '{}', supported are: '{}'", _format,
                        QStringList({knowCore::FileFormat::JSON, knowCore::FileFormat::SRX,
                                     knowCore::FileFormat::XML, knowCore::FileFormat::Turtle}));
  }
}

cres_qresult<void> Result::write(QIODevice* _device, const QString& _format) const
{
  if(_format == knowCore::FileFormat::JSON or _format == "application/sparql-results+json")
  {
    return d->write_json(_device);
  }
  else if(_format == knowCore::FileFormat::SRX or _format == knowCore::FileFormat::XML
          or _format == "application/sparql-results+xml")
  {
    return d->write_srx(_device);
  }
  else
  {
    return cres_failure("unsupported file format: '{}', supported are: '{}'", _format,
                        QStringList({knowCore::FileFormat::JSON, knowCore::FileFormat::SRX,
                                     knowCore::FileFormat::XML}));
  }
}

Result::Type Result::type() const { return d->type; }

bool Result::boolean() const { return d->boolean; }
