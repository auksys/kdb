#include <clog_qt>

#define KDB_SPARQL_REPORT_QUERY_ERROR(_MSG_, _RESULT_)                                             \
  clog_error("in SPARQLQuery: {} with error {} in query {}", _MSG_, _RESULT_.error(),              \
             _RESULT_.query())

#define KDB_SPARQL_EXECUTE_QUERY(_MSG_, _QUERY_, _RETURN_VALUE_)                                   \
  {                                                                                                \
    auto result = _QUERY_.execute();                                                               \
    if(not result)                                                                                 \
    {                                                                                              \
      KDB_SPARQL_REPORT_QUERY_ERROR(_MSG_, result);                                                \
      return _RETURN_VALUE_;                                                                       \
    }                                                                                              \
  }
