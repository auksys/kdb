/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Query.h"

#include <QBuffer>

#include <knowCore/Messages.h>

#include "Algebra/Node.h"

#include "Lexer_p.h"
#include "Parser_p.h"
#include <clog_qt>

using namespace kDB::SPARQL;

struct Query::Private : public QSharedData
{
  Algebra::NodeCSP node;
};

Query::Query() : d(nullptr) {}

Query::Query(Private* _private) : d(_private) {}

Query::Query(const Query& _rhs) : d(_rhs.d) {}

Query& Query::operator=(const Query& _rhs)
{
  d = _rhs.d;
  return *this;
}

Query::~Query() {}

const Algebra::Node* Query::getNode() const { return d->node.data(); }

QList<Query> Query::parse(const QByteArray& _query, const knowCore::ValueHash& _bindings,
                          knowCore::Messages* _messages, const knowCore::Uri& _base)
{
  return parse(QString::fromUtf8(_query), _bindings, _messages, _base);
}

QList<Query> Query::parse(const QString& _query, const knowCore::ValueHash& _bindings,
                          knowCore::Messages* _messages, const knowCore::Uri& _base)
{
  Lexer lexer(_query, _bindings);
  Parser parser(&lexer, _bindings, _base);
  QList<Algebra::NodeCSP> q = parser.parse();
  clog_assert(not q.isEmpty() or parser.messages().hasErrors());
  if(_messages)
  {
    *_messages = parser.messages();
  }
  QList<Query> queries;
  for(Algebra::NodeCSP n : q)
  {
    Private* p = new Private;
    p->node = n;
    queries.append(Query(p));
  }
  return queries;
}

Query::Type Query::type() const
{
  if(d)
  {
    switch(d->node->type())
    {
    case Algebra::NodeType::AskQuery:
      return Type::Ask;
    case Algebra::NodeType::ConstructQuery:
      return Type::Construct;
    case Algebra::NodeType::DescribeQuery:
      return Type::Describe;
    case Algebra::NodeType::SelectQuery:
      return Type::Select;
    case Algebra::NodeType::DeleteData:
    case Algebra::NodeType::InsertData:
    case Algebra::NodeType::DeleteInsert:
    case Algebra::NodeType::Drop:
    case Algebra::NodeType::Clear:
    case Algebra::NodeType::Create:
    case Algebra::NodeType::Load:
      return Type::Update;
    case Algebra::NodeType::Execute:
      return Type::Execute;
    case Algebra::NodeType::ExplainQuery:
      return Type::Explain;
    case Algebra::NodeType::PLQuery:
      return Type::PL;
    default:
      return Type::Invalid;
    }
  }
  else
  {
    return Type::Invalid;
  }
}
