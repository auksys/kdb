#include "Utils.h"

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>

namespace kDB
{
  namespace SPARQL
  {
    namespace Algebra
    {
      namespace Utils
      {
        cres_qresult<void> subjectsFromNode(NodeCSP _subject, QList<knowRDF::Subject>* subjects)
        {
          switch(_subject->type())
          {
          case NodeType::VariableReference:
            return cres_failure(
              "Variable references are not accepted in INSERT/DELETE DATA queries");
          case NodeType::Term:
            subjects->append(knowRDF::Subject(dynamic_cast<const Term*>(_subject.data())->term()));
            break;
          case NodeType::List:
          {
            for(NodeCSP node : dynamic_cast<const List*>(_subject.data())->list())
            {
              cres_try(cres_ignore, subjectsFromNode(node, subjects));
            }
            break;
          }
          case NodeType::BlankNode:
            subjects->append(dynamic_cast<const BlankNode*>(_subject.data())->blankNode());
            break;
          default:
            return cres_failure("Internal error: unknown type of subject in subjectsFromNode");
          }
          return cres_success();
        }

        cres_qresult<void> objectsFromNode(NodeCSP _object, QList<knowRDF::Object>* objects)
        {
          switch(_object->type())
          {
          case NodeType::VariableReference:
            return cres_failure("Variable are not accepted in INSERT/DELETE DATA queries");
          case NodeType::Term:
            objects->append(knowRDF::Object(dynamic_cast<const Term*>(_object.data())->term()));
            break;
          case NodeType::Value:
            objects->append(dynamic_cast<const Value*>(_object.data())->value());
            break;
          case NodeType::BlankNode:
            objects->append(dynamic_cast<const BlankNode*>(_object.data())->blankNode());
            break;
          default:
            return cres_failure("Internal error: unknown type of object in objectsFromNode");
          }
          return cres_success();
        }

        cres_qresult<void> toRDFTriples(const QList<TripleCSP>& _triples,
                                        QList<knowRDF::Triple>* _rdf_triples)
        {
          for(TripleCSP triple : _triples)
          {
            QList<knowRDF::Subject> subjects;
            cres_try(cres_ignore, subjectsFromNode(triple->subject(), &subjects));
            if(triple->predicate()->type() != NodeType::Term)
            {
              return cres_failure("predicate in INSERT/DELETE DATA should be a uri");
            }
            knowCore::Uri predicate = dynamic_cast<const Term*>(triple->predicate().data())->term();
            QList<knowRDF::Object> objects;
            cres_try(cres_ignore, objectsFromNode(triple->object(), &objects));

            for(const knowRDF::Subject& subject : subjects)
            {
              for(const knowRDF::Object& object : objects)
              {
                _rdf_triples->append(knowRDF::Triple(subject, predicate, object));
              }
            }
          }
          return cres_success();
        }
      } // namespace Utils
    } // namespace Algebra
  } // namespace SPARQL
} // namespace kDB
