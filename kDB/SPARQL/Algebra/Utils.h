#include "Nodes.h"

namespace kDB
{
  namespace SPARQL
  {
    namespace Algebra
    {
      namespace Utils
      {
        cres_qresult<void> toRDFTriples(const QList<TripleCSP>& _triples,
                                        QList<knowRDF::Triple>* _rdf_triples);
      }
    } // namespace Algebra
  } // namespace SPARQL
} // namespace kDB
