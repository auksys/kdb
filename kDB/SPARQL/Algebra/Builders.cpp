#include "Builders.h"

#include <knowCore/Cast.h>

#include "Visitors/Serialiser.h"

using namespace kDB::SPARQL::Algebra::Builders;

TriplesList::operator QList<kDB::SPARQL::Algebra::NodeCSP>() const
{
  return knowCore::listCast<kDB::SPARQL::Algebra::NodeCSP>(m_triples);
}

#define KDB_SPARQL_ALGEBRA_GENERATE_CONSTRUCTOR_ARGUMENT_CALL(_KLASS_NAME_, _TYPE_, _NAME_)        \
  d->m_##_NAME_,

#define KDB_SPARQL_ALGEBRA_GENERATE_PRIVATE_MEMBER(_KLASS_NAME_, _TYPE_, _NAME_)                   \
  details::field_traits<_TYPE_>::type m_##_NAME_;

#define KDB_SPARQL_ALGEBRA_GENERATE_MEMBER_UPDATE_DEFINITION(_KLASS_NAME_, _TYPE_, _NAME_)         \
  void _KLASS_NAME_::update_##_NAME_(const details::field_traits<_TYPE_>::type& _type)             \
  {                                                                                                \
    d->cache = nullptr;                                                                            \
    d->m_##_NAME_ = _type;                                                                         \
  }

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  struct _KLASS_NAME_::Private                                                                     \
  {                                                                                                \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_PRIVATE_MEMBER)                         \
    mutable kDB::SPARQL::Algebra::_KLASS_NAME_##CSP cache;                                         \
  };                                                                                               \
                                                                                                   \
  _KLASS_NAME_::_KLASS_NAME_() : d(new Private) {}                                                 \
                                                                                                   \
  _KLASS_NAME_::~_KLASS_NAME_() { delete d; }                                                      \
                                                                                                   \
  QPair<QString, knowCore::ValueHash> _KLASS_NAME_::toQueryString() const                          \
  {                                                                                                \
    kDB::SPARQL::Algebra::Visitors::Serialiser::Result r                                           \
      = kDB::SPARQL::Algebra::Visitors::Serialiser::serialise(toNode(), knowCore::ValueHash());    \
    return QPair<QString, knowCore::ValueHash>(r.queryText, r.bindings);                           \
  }                                                                                                \
                                                                                                   \
  kDB::SPARQL::Algebra::_KLASS_NAME_##CSP _KLASS_NAME_::toNode() const                             \
  {                                                                                                \
    if(not d->cache)                                                                               \
    {                                                                                              \
      d->cache = new kDB::SPARQL::Algebra::_KLASS_NAME_(_MEMBER_DEF_(                              \
        _KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_CONSTRUCTOR_ARGUMENT_CALL) nullptr);             \
    }                                                                                              \
    return d->cache;                                                                               \
  }                                                                                                \
                                                                                                   \
  _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_MEMBER_UPDATE_DEFINITION)

#include "NodesDefs.h"
