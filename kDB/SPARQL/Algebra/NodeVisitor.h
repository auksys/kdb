#include "AbstractNodeVisitor.h"

namespace kDB::SPARQL::Algebra
{
  namespace details
  {
    template<typename _TVisitor_, typename _T_>
    struct Call
    {
      template<int... S>
      static _TVisitor_::ReturnType accept(_TVisitor_* _visitor, const _T_& _t,
                                           const _TVisitor_::ParametersTuple& _parameters,
                                           std::index_sequence<S...>)
      {
        return _visitor->visit(_t, std::get<S>(_parameters)...);
      }
    };
    template<typename _TVisitor_>
    struct Call<_TVisitor_, NodeCSP>
    {
      template<int... S>
      static _TVisitor_::ReturnType accept(_TVisitor_* _visitor, const NodeCSP& _t,
                                           const _TVisitor_::ParametersTuple& _parameters,
                                           std::index_sequence<S...>)
      {
        return _visitor->accept(_t, std::get<S>(_parameters)...);
      }
    };

    template<typename _T_>
    struct RemoveConstExplicitlySharedDataPointer
    {
      typedef _T_ type;
    };
    template<typename _T_>
    struct RemoveConstExplicitlySharedDataPointer<knowCore::ConstExplicitlySharedDataPointer<_T_>>
    {
      typedef _T_ type;
    };

    template<typename _T_>
    using RemoveConstExplicitlySharedDataPointerT =
      typename RemoveConstExplicitlySharedDataPointer<_T_>::type;

    template<typename _TVisitor_, typename _T_>
    struct VisitorHelper;

    template<typename _TVisitor_, typename _T_>
      requires std::is_base_of_v<Node, _T_>
    struct VisitorHelper<_TVisitor_, knowCore::ConstExplicitlySharedDataPointer<_T_>>
    {
      static _TVisitor_::ReturnType call(_TVisitor_* _visitor,
                                         const knowCore::ConstExplicitlySharedDataPointer<_T_>& _t,
                                         const _TVisitor_::ParametersTuple& _parameters)
      {
        if(_t)
        {
          return Call<_TVisitor_, knowCore::ConstExplicitlySharedDataPointer<_T_>>::accept(
            _visitor, _t, _parameters, std::make_index_sequence<_TVisitor_::ParametersCount>());
        }
        return typename _TVisitor_::ReturnType();
      }
    };
    template<typename _TVisitor_, typename _T_>
      requires(not std::is_base_of_v<Node, RemoveConstExplicitlySharedDataPointerT<_T_>>
               and not knowCore::details::is_specialisation_of_v<QList, _T_>
               and not knowCore::details::is_specialisation_of_v<Alternative, _T_>)
    struct VisitorHelper<_TVisitor_, _T_>
    {
      static _TVisitor_::ReturnType call(_TVisitor_* _visitor, const _T_& _t,
                                         const _TVisitor_::ParametersTuple& _parameters)
      {
        Q_UNUSED(_t);
        Q_UNUSED(_visitor);
        Q_UNUSED(_parameters);
        return typename _TVisitor_::ReturnType();
      }
    };
    template<typename _TVisitor_, typename... _T_>
    struct VisitorHelper<_TVisitor_, Alternative<_T_...>>
    {
      static _TVisitor_::ReturnType call(_TVisitor_* _visitor, const Alternative<_T_...>& _t,
                                         const _TVisitor_::ParametersTuple& _parameters)
      {
        if(_t.node())
        {
          return Call<_TVisitor_, NodeCSP>::accept(
            _visitor, _t.node(), _parameters,
            std::make_index_sequence<_TVisitor_::ParametersCount>());
        }
        else
        {
          return typename _TVisitor_::ReturnType();
        }
      }
    };
    template<typename _TVisitor_, typename _T_>
      requires(std::is_base_of_v<Node, RemoveConstExplicitlySharedDataPointerT<_T_>>
               or knowCore::details::is_specialisation_of_v<Alternative, _T_>)
    struct VisitorHelper<_TVisitor_, QList<_T_>>
    {
      static _TVisitor_::ReturnType call(_TVisitor_* _visitor, const QList<_T_>& _t,
                                         const _TVisitor_::ParametersTuple& _parameters)
      {
        for(const _T_& t : _t)
        {
          VisitorHelper<_TVisitor_, _T_>::call(_visitor, t, _parameters);
        }
        return typename _TVisitor_::ReturnType();
      }
    };
    template<typename _TVisitor_, typename _T_>
      requires(not std::is_base_of_v<Node, RemoveConstExplicitlySharedDataPointerT<_T_>>
               and not knowCore::details::is_specialisation_of_v<Alternative, _T_>)
    struct VisitorHelper<_TVisitor_, QList<_T_>>
    {
      static _TVisitor_::ReturnType call(_TVisitor_* _visitor, const QList<_T_>& _t,
                                         const _TVisitor_::ParametersTuple& _parameters)
      {
        Q_UNUSED(_t);
        Q_UNUSED(_visitor);
        Q_UNUSED(_parameters);
        return typename _TVisitor_::ReturnType();
      }
    };
  } // namespace details
  // TODO rename to TraverseNodeVisitor?
  template<typename _TR_, typename... _TArgs_>
  class NodeVisitor : public AbstractNodeVisitor<_TR_, _TArgs_...>
  {
    template<typename _TVisitor_, typename _T_>
    friend struct details::Call;
  public:
    NodeVisitor() {}
    ~NodeVisitor() {}
  private:
#define KDB_SPARQL_GENERATE_VISIT_CALL(_KLASS_NAME_, _TYPE_, _NAME_)                               \
  details::VisitorHelper<NodeVisitor<_TR_, _TArgs_...>, _TYPE_>::call(                             \
    this, _node->_NAME_(), std::make_tuple(_parameters...));

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  _TR_ visit(_KLASS_NAME_##CSP _node, const _TArgs_&... _parameters) override                      \
  {                                                                                                \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_GENERATE_VISIT_CALL)                                     \
    return _TR_();                                                                                 \
  }
#include "NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
#undef KDB_SPARQL_GENERATE_VISIT_CALL
  };
} // namespace kDB::SPARQL::Algebra
