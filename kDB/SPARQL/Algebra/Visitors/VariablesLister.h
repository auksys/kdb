#include <kDB/SPARQL/Algebra/NodeVisitor.h>

namespace kDB::SPARQL::Algebra::Visitors
{
  /**
   * This visitor list all the variable names, accessible through the \ref names function.
   *
   * @code
   * Algebra::NodeCSP node = ...;
   * VariablesListerVisitor vlv;
   * node->accept(&vlv, QVariant());
   * clog_debug("Variables: '{}'", vlc.names());
   * @endcode
   */
  class VariablesLister : public Algebra::NodeVisitor<void>
  {
    VariablesLister();
    ~VariablesLister();
  public:
    static QStringList list(NodeCSP _root);
  private:
    void visit(Algebra::VariableReferenceCSP _node) override;
    void visit(kDB::SPARQL::Algebra::BindCSP _node) override;
    QStringList names() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::SPARQL::Algebra::Visitors
