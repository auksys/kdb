#include "Inverter.h"

#include <clog_qt>

using namespace kDB::SPARQL::Algebra::Visitors;

using NodeCSP = kDB::SPARQL::Algebra::NodeCSP;

struct Inverter::Private
{
};

Inverter::Inverter() : d(new Private) {}

Inverter::~Inverter() {}

NodeCSP Inverter::invert(NodeCSP _node)
{
  Inverter s;
  return s.start(_node);
}

NodeCSP Inverter::visitDefault(NodeCSP _node)
{
  clog_fatal("not implemented!: {}", nodeTypeName(_node->type()));
  return nullptr;
}

NodeCSP Inverter::visit(InsertDataCSP _node) { return new DeleteData(_node->quadsData()); }

NodeCSP Inverter::visit(DeleteDataCSP _node) { return new InsertData(_node->quadsData()); }
