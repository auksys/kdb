#include "Serialiser.h"

#include <clog_qt>
#include <knowRDF/Literal.h>

using namespace kDB::SPARQL::Algebra::Visitors;

struct Serialiser::Private
{
  QHash<knowRDF::BlankNode, QString> blankNodes;
  knowCore::ValueHash variables_to_value;
};

Serialiser::Serialiser(const knowCore::ValueHash& _variables_to_value) : d(new Private)
{
  d->variables_to_value = _variables_to_value;
}

Serialiser::~Serialiser() {}

Serialiser::Result Serialiser::serialise(NodeCSP _node, const knowCore::ValueHash& _variables)
{
  Serialiser::Result r;
  Serialiser s(_variables);
  r.queryText = s.start(_node, &r.bindings);
  return r;
}

QString Serialiser::visitDefault(NodeCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  Q_UNUSED(_node);
  Q_UNUSED(_bindings);
  clog_fatal("not implemented!: {}", nodeTypeName(_node->type()));
}

QString Serialiser::visit(FunctionCallCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  QString t = "<" + _node->name() + ">(";
  for(int i = 0; i < _node->parameters().size(); ++i)
  {
    if(i != 0)
      t += ", ";
    t += accept(_node->parameters()[i], _bindings);
  }
  return t + ")";
}

QString Serialiser::visit(GroupGraphPatternCSP _node,
                          QHash<QString, knowCore::Value>* const& _bindings)
{
  QString t;

  if(_node->source())
  {
    t += "GRAPH " + accept(_node->source(), _bindings);
  }
  t += "{ ";

  for(NodeCSP gp : _node->graphPatterns())
  {
    t += accept(gp, _bindings);
  }

  for(NodeCSP fi : _node->filters())
  {
    t += "FILTER (" + accept(fi, _bindings) + ") ";
  }

  for(ServiceCSP s : _node->services())
  {
    t += accept(s, _bindings);
  }

  t += "} ";

  return t;
}

QString Serialiser::visit(LimitOffsetClauseCSP _node,
                          QHash<QString, knowCore::Value>* const& _bindings)
{
  Q_UNUSED(_bindings);
  QString t;
  const int limit = _node->limit();
  const int offset = _node->offset();
  if(limit > 0)
  {
    t += clog_qt::qformat("LIMIT {} ", limit);
  }
  if(offset > 0)
  {
    t += clog_qt::qformat("OFFSET {} ", offset);
  }
  return t;
}

QString Serialiser::visit(SelectQueryCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  Q_UNUSED(_bindings);
  QString t = "SELECT ";

  switch(_node->modifier())
  {
  case SelectModifier::None:
    break;

  case SelectModifier::Distinct:
    t += "DISTINCT ";
    break;

  case SelectModifier::Reduced:
    t += "REDUCED ";
    break;
  }

  if(_node->variables().isEmpty())
  {
    t += "* ";
  }
  else
  {
    for(VariableCSP var : _node->variables())
    {
      t += visit(var, _bindings) + " ";
    }
  }

  if(not _node->datasets().isEmpty())
  {
    t += "FROM ";

    for(DatasetCSP dataset : _node->datasets())
    {
      switch(dataset->datasetType())
      {
      case DatasetType::Anonymous:
        break;

      case DatasetType::Named:
        t += "NAMED ";
        break;
      }

      t += visitUri(dataset->uri(), _bindings) + " ";
    }
  }

  t += accept(_node->where(), _bindings);
  t += accept(_node->groupClauses(), _bindings);
  t += accept(_node->havingClauses(), _bindings);
  t += accept(_node->orderClauses(), _bindings);
  t += accept(_node->limitOffsetClause(), _bindings);

  return t;
}

QString Serialiser::visit(TermCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  Q_UNUSED(_bindings);
  return visitUri(_node->term(), _bindings);
}

QString Serialiser::visit(TripleCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  return accept(_node->subject(), _bindings) + " " + accept(_node->predicate(), _bindings) + " "
         + accept(_node->object(), _bindings) + " . ";
}

QString Serialiser::visit(ValueCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  QString name = clog_qt::qformat("%value{}", _bindings->size());
  (*_bindings)[name] = _node->value();
  return name;
}

QString Serialiser::visit(VariableReferenceCSP _node,
                          QHash<QString, knowCore::Value>* const& _bindings)
{
  QString name = _node->name();
  if(d->variables_to_value.contains(name))
  {
    (*_bindings)["%" + name] = d->variables_to_value.value(name);
    return "%" + name;
  }
  else
  {
    return "?" + name;
  }
}

QString Serialiser::visit(VariableCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  QString t = _node->name();
  if(_node->expression())
  {
    t = "(" + accept(_node->expression(), _bindings) + ") AS " + t;
  }
  return t;
}

QString Serialiser::visit(BlankNodeCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  Q_UNUSED(_bindings);
  knowRDF::BlankNode bn = _node->blankNode();

  if(d->blankNodes.contains(bn))
  {
    return d->blankNodes.value(bn);
  }
  else
  {
    QString t = clog_qt::qformat("_:bn{}", d->blankNodes.size());
    d->blankNodes[bn] = t;
    return t;
  }
}

QString Serialiser::visit(InsertDataCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  QString t = "INSERT DATA {\n";
  t += visit(_node->quadsData(), _bindings);
  t += "}\n";
  return t;
}

QString Serialiser::visit(DeleteDataCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  QString t = "DELETE DATA {\n";
  t += visit(_node->quadsData(), _bindings);
  t += "}\n";
  return t;
}

QString Serialiser::visit(QuadsCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  QString t;
  if(_node->source())
  {
    t += "GRAPH " + visit(_node, _bindings) + "{\n";
  }
  for(TripleCSP triple : _node->triples())
  {
    t += visit(triple, _bindings);
  }
  if(_node->source())
  {
    t += "}\n";
  }
  return t;
}

QString Serialiser::visit(QuadsDataCSP _node, QHash<QString, knowCore::Value>* const& _bindings)
{
  QString t;
  for(QuadsCSP q : _node->quads())
  {
    t += visit(q, _bindings);
  }
  return t;
}

#define BINARY_OP(_NAME_, _OP_)                                                                    \
  QString Serialiser::visit(Relational##_NAME_##CSP _node,                                         \
                            QHash<QString, knowCore::Value>* const& _bindings)                     \
  {                                                                                                \
    return "(" + accept(_node->left(), _bindings) + " " #_OP_ " "                                  \
           + accept(_node->right(), _bindings) + ")";                                              \
  }

BINARY_OP(Equal, =)
BINARY_OP(Different, !=)
BINARY_OP(Inferior, <)
BINARY_OP(InferiorEqual, <=)
BINARY_OP(Superior, <)
BINARY_OP(SuperiorEqual, <=)

QString Serialiser::visitUri(const knowCore::Uri& _uri, QHash<QString, knowCore::Value>* _bindings)
{
  Q_UNUSED(_bindings);
  return "<" + _uri + ">";
}
