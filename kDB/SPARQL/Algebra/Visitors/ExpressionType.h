#include <kDB/SPARQL/Algebra/NodeVisitorImplementation.h>

#include <knowCore/UriList.h>

namespace kDB::SPARQL::Algebra::Visitors
{
  struct FunctionTypeInterface
  {
    virtual ~FunctionTypeInterface();
    virtual knowCore::Uri type(const knowCore::Uri& _name, const knowCore::UriList& _list) const
      = 0;
  };
#undef __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD
#define __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD(_NAME_, _I_)                                    \
  knowCore::Uri visit(_NAME_##CSP _node, const FunctionTypeInterface* const&) override;

  /**
   * Convert a SPARQL Algebra into a SPARQL Query.
   */
  class ExpressionType
      : public kDB::SPARQL::Algebra::NodeVisitorImplementation<knowCore::Uri,
                                                               const FunctionTypeInterface*>
  {
  public:
    /**
     */
    ExpressionType();
    ~ExpressionType();
    /**
     * Convert a @p _node to a SPARQL representation
     */
    static knowCore::Uri type(NodeCSP _node, const FunctionTypeInterface* _interface);
  private:
  protected:
    knowCore::Uri visitDefault(NodeCSP _node,
                               const FunctionTypeInterface* const& _interface) override;
    KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD(FunctionCall, Term, Triple, Value, VariableReference,
                                             LogicalOr, LogicalAnd, RelationalEqual,
                                             RelationalDifferent, RelationalInferior,
                                             RelationalSuperior, RelationalInferiorEqual,
                                             RelationalSuperiorEqual, Addition, Substraction,
                                             Multiplication, Division, RelationalIn,
                                             RelationalNotIn, LogicalNegation, Negation)
    knowCore::Uri visitUri(const knowCore::Uri& _uri, const FunctionTypeInterface* _interface);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::SPARQL::Algebra::Visitors

#undef __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD
