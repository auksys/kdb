#include <kDB/SPARQL/Algebra/NodeVisitorImplementation.h>
#include <knowCore/ValueHash.h>

namespace kDB::SPARQL::Algebra::Visitors
{
// TODO remove and reuse the one from NodeVisitorImplementation
#undef __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD
#define __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD(_NAME_, _I_)                                    \
  QString visit(_NAME_##CSP _node, QHash<QString, knowCore::Value>* const&) override;

  /**
   * Convert a SPARQL Algebra into a SPARQL Query.
   */
  class Serialiser
      : public kDB::SPARQL::Algebra::NodeVisitorImplementation<QString,
                                                               QHash<QString, knowCore::Value>*>
  {
  public:
    struct Result
    {
      QString queryText;
      QHash<QString, knowCore::Value> bindings;
    };
    /**
     * @p _variables_to_value
     */
    Serialiser(const knowCore::ValueHash& _variables_to_value);
    ~Serialiser();
    /**
     * Convert a @p _node to a SPARQL representation
     */
    static Result serialise(NodeCSP _node, const knowCore::ValueHash& _variables);
  private:
  protected:
    QString visitDefault(NodeCSP _node,
                         QHash<QString, knowCore::Value>* const& _parameter) override;
    KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD(BlankNode, GroupGraphPattern, LimitOffsetClause,
                                             FunctionCall, RelationalEqual, RelationalDifferent,
                                             RelationalInferior, RelationalInferiorEqual,
                                             RelationalSuperior, RelationalSuperiorEqual,
                                             SelectQuery, Term, Triple, Value, Variable,
                                             VariableReference, InsertData, DeleteData, QuadsData,
                                             Quads);
    QString visitUri(const knowCore::Uri& _uri, QHash<QString, knowCore::Value>* _parameter);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::SPARQL::Algebra::Visitors

#undef __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD
