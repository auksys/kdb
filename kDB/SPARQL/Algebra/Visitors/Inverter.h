#include <kDB/SPARQL/Algebra/NodeVisitorImplementation.h>

namespace kDB::SPARQL::Algebra::Visitors
{

// TODO remove and reuse the one from NodeVisitorImplementation
#undef __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD
#define __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD(_NAME_, _I_)                                    \
  NodeCSP visit(_NAME_##CSP _node) override;

  /**
   * Invert a SPARQL Algebra. Can only invert InsertData and DeleteData.
   */
  class Inverter : public kDB::SPARQL::Algebra::NodeVisitorImplementation<NodeCSP>
  {
    friend class NodeVisitorImplementation<NodeCSP>;
  public:
    Inverter();
    ~Inverter();
    /**
     * Invert a list of @p _node
     */
    static NodeCSP invert(NodeCSP _node);
  protected:
    NodeCSP visitDefault(NodeCSP _node) override;
    KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD(InsertData, DeleteData);
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::SPARQL::Algebra::Visitors

#undef __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD
