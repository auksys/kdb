#include <kDB/SPARQL/Algebra/AbstractNodeVisitor.h>

namespace kDB::SPARQL::Algebra::Visitors
{
  namespace details
  {
    template<typename _T_, typename = void>
    struct PrintHelper;
  }
  class Print : public AbstractNodeVisitor<void>
  {
    template<typename _T_, typename = void>
    friend class details::PrintHelper;
  public:
    Print();
    ~Print();
#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  virtual void visit(_KLASS_NAME_##CSP _node) override;
#include "../NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::SPARQL::Algebra::Visitors
