#include "ExpressionType.h"

#include <clog_qt>
#include <knowCore/Uris/askcore_datatype.h>
#include <knowCore/Uris/xsd.h>

using namespace kDB::SPARQL::Algebra::Visitors;

struct ExpressionType::Private
{
};

FunctionTypeInterface::~FunctionTypeInterface() {}

ExpressionType::ExpressionType() : d(new Private) {}

ExpressionType::~ExpressionType() {}

knowCore::Uri ExpressionType::type(kDB::SPARQL::Algebra::NodeCSP _node,
                                   const FunctionTypeInterface* _interface)
{
  ExpressionType et;
  return et.start(_node, _interface);
}

knowCore::Uri ExpressionType::visitDefault(kDB::SPARQL::Algebra::NodeCSP _node,
                                           const FunctionTypeInterface* const& _parameter)
{
  Q_UNUSED(_node);
  Q_UNUSED(_parameter);
  clog_fatal("not implemented!: {}", nodeTypeName(_node->type()));
}

knowCore::Uri ExpressionType::visitUri(const knowCore::Uri& _uri,
                                       const FunctionTypeInterface* _parameter)
{
  Q_UNUSED(_uri);
  Q_UNUSED(_parameter);
  return knowCore::Uris::xsd::anyURI;
}

#define F(_TYPE_, _URI_)                                                                           \
  knowCore::Uri ExpressionType::visit(kDB::SPARQL::Algebra::_TYPE_##CSP _node,                     \
                                      const FunctionTypeInterface* const& _parameter)              \
  {                                                                                                \
    Q_UNUSED(_node);                                                                               \
    Q_UNUSED(_parameter);                                                                          \
    return _URI_;                                                                                  \
  }

#define FL(_TYPE_, _I_) F(_TYPE_, knowCore::Uris::askcore_datatype::literal)

F(Triple, knowCore::Uris::askcore_datatype::triple)

KNOWCORE_FOREACH(FL, Term, Value, VariableReference, LogicalOr, LogicalAnd, RelationalEqual,
                 RelationalDifferent, RelationalInferior, RelationalSuperior,
                 RelationalInferiorEqual, RelationalSuperiorEqual, Addition, Substraction,
                 Multiplication, Division, RelationalIn, RelationalNotIn, LogicalNegation, Negation)

knowCore::Uri ExpressionType::visit(kDB::SPARQL::Algebra::FunctionCallCSP _node,
                                    const FunctionTypeInterface* const& _parameter)
{
  knowCore::UriList ul;
  for(kDB::SPARQL::Algebra::NodeCSP node : _node->parameters())
  {
    ul.append(accept(node, _parameter));
  }
  return _parameter->type(_node->name(), ul);
}
