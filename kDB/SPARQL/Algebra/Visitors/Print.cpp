#include "Print.h"

#include <iostream>
#include <type_traits>

#include <QVariant>

#include <clog_qt>
#include <knowRDF/Literal.h>

using namespace kDB::SPARQL::Algebra::Visitors;

clog_format_declare_enum_formatter(kDB::SPARQL::Algebra::DatasetType, Anonymous, Named);
clog_format_declare_enum_formatter(kDB::SPARQL::Algebra::SelectModifier, None, Distinct, Reduced);
clog_format_declare_enum_formatter(kDB::SPARQL::Algebra::OrderDirection, Asc, Desc);
clog_format_declare_enum_formatter(kDB::SPARQL::Algebra::GraphType, All, Default, Named, Graph);

namespace
{
  static void print(const QString& _indentation, const QString& _message)
  {
    std::cerr << qPrintable(_indentation) << qPrintable(_message) << std::endl;
    ;
  }
  template<typename _T_, typename... _TOther_>
  static void print(const QString& _indentation, const QString& _message, _T_ const& _v,
                    _TOther_... _other)
  {
    print(_indentation, clog_qt::qformat(_message, _v, _other...));
  }
} // namespace

namespace kDB::SPARQL::Algebra::Visitors::details
{

  template<typename _T_>
  struct RemoveConstExplicitlySharedDataPointer
  {
    typedef _T_ type;
  };
  template<typename _T_>
  struct RemoveConstExplicitlySharedDataPointer<knowCore::ConstExplicitlySharedDataPointer<_T_>>
  {
    typedef _T_ type;
  };

  template<typename... _T_>
  struct PrintHelper<kDB::SPARQL::Algebra::Alternative<_T_...>>
  {
    static void call(Print* _visitor, QString& _indentation, const char* _name,
                     const kDB::SPARQL::Algebra::Alternative<_T_...>& _t)
    {
      if(_t.isValid())
      {
        if(_name != 0)
        {
          print(_indentation, "{}:", _name);
        }
        QString indentation = _indentation;
        _indentation += "  ";
        _visitor->accept(_t.node());
        _indentation = indentation;
      }
    }
  };
  template<typename _T_>
    requires(std::is_base_of_v<kDB::SPARQL::Algebra::Node, _T_>)
  struct PrintHelper<knowCore::ConstExplicitlySharedDataPointer<_T_>>
  {
    static void call(Print* _visitor, QString& _indentation, const char* _name,
                     knowCore::ConstExplicitlySharedDataPointer<_T_> _t)
    {
      if(_t)
      {
        if(_name != 0)
        {
          print(_indentation, "{}:", _name);
        }
        QString indentation = _indentation;
        _indentation += "  ";
        _visitor->accept(_t);
        _indentation = indentation;
      }
    }
  };
  template<typename _T_, typename>
  struct PrintHelper
  {
    static void call(Print* _visitor, QString& _indentation, const char* _name, const _T_& _t)
    {
      Q_UNUSED(_visitor);
      print(_indentation, "{}: {}", _name, _t);
    }
  };
  template<typename _T_>
  struct PrintHelper<QList<_T_>, void>
  {
    static void call(Print* _visitor, QString& _indentation, const char* _name,
                     const QList<_T_>& _t)
    {
      print(_indentation, "{} ({}): [", _name, _t.size());
      for(const _T_& t : _t)
      {
        PrintHelper<_T_>::call(_visitor, _indentation, "null", t);
      }
      print(_indentation, "]");
    }
  };
} // namespace kDB::SPARQL::Algebra::Visitors::details

struct Print::Private
{
  QString indentation;
};

Print::Print() : d(new Private) {}

Print::~Print() { delete d; }

#define KDB_SPARQL_GENERATE_VISIT_CALL(_KLASS_NAME_, _TYPE_, _NAME_)                               \
  details::PrintHelper<_TYPE_>::call(this, d->indentation, #_NAME_, _node->_NAME_());

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  void Print::visit(_KLASS_NAME_##CSP _node)                                                       \
  {                                                                                                \
    print(d->indentation, #_KLASS_NAME_);                                                          \
    QString oldindentation = d->indentation;                                                       \
    d->indentation += "  ";                                                                        \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_GENERATE_VISIT_CALL)                                     \
    d->indentation = oldindentation;                                                               \
  }

#include "../NodesDefs.h"

#undef KDB_SPARQL_ALGEBRA_GENERATE
