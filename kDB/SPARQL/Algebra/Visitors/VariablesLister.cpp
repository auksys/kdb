#include "VariablesLister.h"

#include <knowRDF/Literal.h>

using namespace kDB::SPARQL::Algebra::Visitors;

struct VariablesLister::Private
{
  QStringList names;
};

VariablesLister::VariablesLister() : d(new Private) {}

VariablesLister::~VariablesLister() { delete d; }

QStringList VariablesLister::list(NodeCSP _root)
{
  VariablesLister l;
  l.start(_root);
  return l.names();
}

void VariablesLister::visit(VariableReferenceCSP _node)
{
  QString name = _node->name();
  if(not d->names.contains(name))
  {
    d->names.append(name);
  }
}

void VariablesLister::visit(BindCSP _node)
{
  QString name = _node->name();
  if(not d->names.contains(name))
  {
    d->names.append(name);
  }
}

QStringList VariablesLister::names() const { return d->names; }
