#ifndef _KDB_SPARQL_ALGEBRA_NODES_H_
#define _KDB_SPARQL_ALGEBRA_NODES_H_

#include "Node.h"
#include <knowCore/Uri.h>
#include <knowRDF/BlankNode.h>
#include <knowRDF/Triple.h>

class QVariant;

namespace kDB::SPARQL
{
  namespace Algebra
  {
    enum class SelectModifier
    {
      None,
      Distinct,
      Reduced
    };
    enum class DatasetType
    {
      Anonymous,
      Named
    };
    enum class OrderDirection
    {
      Asc,
      Desc
    };
    enum class GraphType
    {
      Graph,
      Default,
      Named,
      All
    };
  } // namespace Algebra
} // namespace kDB::SPARQL

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  class _KLASS_NAME_;                                                                              \
  typedef knowCore::ConstExplicitlySharedDataPointer<_KLASS_NAME_> _KLASS_NAME_##CSP;

namespace kDB::SPARQL
{
  namespace Algebra
  {
#include "NodesDefs.h"
  }
} // namespace kDB::SPARQL

#undef KDB_SPARQL_ALGEBRA_GENERATE

#define KDB_SPARQL_ALGEBRA_GENERATE_CONSTRUCTOR_ARGUMENT(_KLASS_NAME_, _TYPE_, _NAME_)             \
  _TYPE_ _##_NAME_,

#define KDB_SPARQL_ALGEBRA_GENERATE_ACCESSOR_DECLARATION(_KLASS_NAME_, _TYPE_, _NAME_, ...)        \
  _TYPE_ _NAME_() const;

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  class _KLASS_NAME_ : public Node                                                                 \
  {                                                                                                \
  public:                                                                                          \
    _KLASS_NAME_(_MEMBER_DEF_(_KLASS_NAME_,                                                        \
                              KDB_SPARQL_ALGEBRA_GENERATE_CONSTRUCTOR_ARGUMENT) void* _            \
                 = nullptr);                                                                       \
    ~_KLASS_NAME_();                                                                               \
  public:                                                                                          \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_ACCESSOR_DECLARATION);                  \
    using Node::accept;                                                                            \
  private:                                                                                         \
    void accept(details::AbstractNodeVisitorAdaptor* _node, void* _r,                              \
                void* _parameter) const override;                                                  \
  private:                                                                                         \
    struct Private;                                                                                \
  };

namespace kDB::SPARQL
{
  namespace Algebra
  {
#include "NodesDefs.h"
  }
} // namespace kDB::SPARQL

#undef KDB_SPARQL_ALGEBRA_GENERATE

#endif
