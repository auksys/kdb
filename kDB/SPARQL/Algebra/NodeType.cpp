#include "NodeType.h"

#include <clog_qt>

namespace kDB::SPARQL::Algebra
{
  const char* nodeTypeName(NodeType _nt)
  {
    switch(_nt)
    {
#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  case NodeType::_KLASS_NAME_:                                                                     \
    return #_KLASS_NAME_;
#include "NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
    case NodeType::__Invalid__:
      return "__Invalid__";
    }
    clog_fatal("impossible");
  }
} // namespace kDB::SPARQL::Algebra
