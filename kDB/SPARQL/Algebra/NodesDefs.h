#define KDB_SPARQL_ALGEBRA_EXECUTE_MEMBERS(_KLASS_NAME_, F) F(_KLASS_NAME_, knowCore::Uri, uri)

KDB_SPARQL_ALGEBRA_GENERATE(Execute, KDB_SPARQL_ALGEBRA_EXECUTE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_LIST_MEMBERS(_KLASS_NAME_, F) F(_KLASS_NAME_, QList<NodeCSP>, list)

KDB_SPARQL_ALGEBRA_GENERATE(List, KDB_SPARQL_ALGEBRA_LIST_MEMBERS)

#define KDB_SPARQL_ALGEBRA_VARIABLE_MEMBERS(_KLASS_NAME_, F)                                       \
  F(_KLASS_NAME_, QString, name)                                                                   \
  F(_KLASS_NAME_, NodeCSP, expression)

KDB_SPARQL_ALGEBRA_GENERATE(Variable, KDB_SPARQL_ALGEBRA_VARIABLE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_DATASET_MEMBERS(_KLASS_NAME_, F)                                        \
  F(_KLASS_NAME_, DatasetType, datasetType)                                                        \
  F(_KLASS_NAME_, knowCore::Uri, uri)

KDB_SPARQL_ALGEBRA_GENERATE(Dataset, KDB_SPARQL_ALGEBRA_DATASET_MEMBERS)

#define KDB_SPARQL_ALGEBRA_ASK_QUERY_MEMBERS(_KLASS_NAME_, F)                                      \
  F(_KLASS_NAME_, QList<DatasetCSP>, datasets)                                                     \
  F(_KLASS_NAME_, NodeCSP, where)

KDB_SPARQL_ALGEBRA_GENERATE(AskQuery, KDB_SPARQL_ALGEBRA_ASK_QUERY_MEMBERS)

#define KDB_SPARQL_ALGEBRA_CONSTRUCT_QUERY_MEMBERS(_KLASS_NAME_, F)                                \
  F(_KLASS_NAME_, QList<TripleCSP>, constructTemplate)                                             \
  F(_KLASS_NAME_, QList<DatasetCSP>, datasets)                                                     \
  F(_KLASS_NAME_, NodeCSP, where)                                                                  \
  F(_KLASS_NAME_, GroupClausesCSP, groupClauses)                                                   \
  F(_KLASS_NAME_, HavingClausesCSP, havingClauses)                                                 \
  F(_KLASS_NAME_, OrderClausesCSP, orderClauses)                                                   \
  F(_KLASS_NAME_, LimitOffsetClauseCSP, limitOffsetClause)

KDB_SPARQL_ALGEBRA_GENERATE(ConstructQuery, KDB_SPARQL_ALGEBRA_CONSTRUCT_QUERY_MEMBERS)

#define KDB_SPARQL_ALGEBRA_DESCRIBE_TERM(_KLASS_NAME_, F)                                          \
  F(_KLASS_NAME_, VariableCSP, variable)                                                           \
  F(_KLASS_NAME_, knowCore::Uri, uri)

KDB_SPARQL_ALGEBRA_GENERATE(DescribeTerm, KDB_SPARQL_ALGEBRA_DESCRIBE_TERM)

#define KDB_SPARQL_ALGEBRA_DESCRIBE_QUERY_MEMBERS(_KLASS_NAME_, F)                                 \
  F(_KLASS_NAME_, QList<DescribeTermCSP>, terms)                                                   \
  F(_KLASS_NAME_, QList<DatasetCSP>, datasets)                                                     \
  F(_KLASS_NAME_, NodeCSP, where)                                                                  \
  F(_KLASS_NAME_, GroupClausesCSP, groupClauses)                                                   \
  F(_KLASS_NAME_, HavingClausesCSP, havingClauses)                                                 \
  F(_KLASS_NAME_, OrderClausesCSP, orderClauses)                                                   \
  F(_KLASS_NAME_, LimitOffsetClauseCSP, limitOffsetClause)

KDB_SPARQL_ALGEBRA_GENERATE(DescribeQuery, KDB_SPARQL_ALGEBRA_DESCRIBE_QUERY_MEMBERS)

#define KDB_SPARQL_ALGEBRA_SELECT_QUERY_MEMBERS(_KLASS_NAME_, F)                                   \
  F(_KLASS_NAME_, SelectModifier, modifier)                                                        \
  F(_KLASS_NAME_, QList<VariableCSP>, variables)                                                   \
  F(_KLASS_NAME_, QList<DatasetCSP>, datasets)                                                     \
  F(_KLASS_NAME_, NodeCSP, where)                                                                  \
  F(_KLASS_NAME_, GroupClausesCSP, groupClauses)                                                   \
  F(_KLASS_NAME_, HavingClausesCSP, havingClauses)                                                 \
  F(_KLASS_NAME_, OrderClausesCSP, orderClauses)                                                   \
  F(_KLASS_NAME_, LimitOffsetClauseCSP, limitOffsetClause)

KDB_SPARQL_ALGEBRA_GENERATE(SelectQuery, KDB_SPARQL_ALGEBRA_SELECT_QUERY_MEMBERS)

KDB_SPARQL_ALGEBRA_GENERATE(GroupClauses, KDB_SPARQL_ALGEBRA_LIST_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(HavingClauses, KDB_SPARQL_ALGEBRA_LIST_MEMBERS)

#define KDB_SPARQL_ALGEBRA_ORDER_CLAUSES_MEMBERS(_KLASS_NAME_, F)                                  \
  F(_KLASS_NAME_, QList<OrderClauseCSP>, list)

KDB_SPARQL_ALGEBRA_GENERATE(OrderClauses, KDB_SPARQL_ALGEBRA_ORDER_CLAUSES_MEMBERS)

#define KDB_SPARQL_ALGEBRA_ORDER_CLAUSE_MEMBERS(_KLASS_NAME_, F)                                   \
  F(_KLASS_NAME_, OrderDirection, orderDirection)                                                  \
  F(_KLASS_NAME_, NodeCSP, node)

KDB_SPARQL_ALGEBRA_GENERATE(OrderClause, KDB_SPARQL_ALGEBRA_ORDER_CLAUSE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_LIMIT_OFFSET_CLAUSE_MEMBERS(_KLASS_NAME_, F)                            \
  F(_KLASS_NAME_, int, limit)                                                                      \
  F(_KLASS_NAME_, int, offset)

KDB_SPARQL_ALGEBRA_GENERATE(LimitOffsetClause, KDB_SPARQL_ALGEBRA_LIMIT_OFFSET_CLAUSE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_TRIPLE_MEMBERS(_KLASS_NAME_, F)                                         \
  F(_KLASS_NAME_, NodeCSP, subject)                                                                \
  F(_KLASS_NAME_, NodeCSP, predicate)                                                              \
  F(_KLASS_NAME_, NodeCSP, object)

KDB_SPARQL_ALGEBRA_GENERATE(Triple, KDB_SPARQL_ALGEBRA_TRIPLE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_VARIABLE_REFERENCE_MEMBERS(_KLASS_NAME_, F)                             \
  F(_KLASS_NAME_, QString, name)

KDB_SPARQL_ALGEBRA_GENERATE(VariableReference, KDB_SPARQL_ALGEBRA_VARIABLE_REFERENCE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_GRAPH_REFERENCE_MEMBERS(_KLASS_NAME_, F)                                \
  F(_KLASS_NAME_, knowCore::Uri, name)

KDB_SPARQL_ALGEBRA_GENERATE(GraphReference, KDB_SPARQL_ALGEBRA_GRAPH_REFERENCE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_TERM_MEMBERS(_KLASS_NAME_, F) F(_KLASS_NAME_, knowCore::Uri, term)

KDB_SPARQL_ALGEBRA_GENERATE(Term, KDB_SPARQL_ALGEBRA_TERM_MEMBERS)

#define KDB_SPARQL_ALGEBRA_BLANK_NODE_MEMBERS(_KLASS_NAME_, F)                                     \
  F(_KLASS_NAME_, knowRDF::BlankNode, blankNode)

KDB_SPARQL_ALGEBRA_GENERATE(BlankNode, KDB_SPARQL_ALGEBRA_BLANK_NODE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_SERVICE_MEMBERS(_KLASS_NAME_, F)                                        \
  F(_KLASS_NAME_, Alternative<KNOWCORE_LIST(VariableReference, Term)>, service)                    \
  F(_KLASS_NAME_, QList<Alternative<KNOWCORE_LIST(VariableReference, Term)>>, froms)               \
  F(_KLASS_NAME_, GroupGraphPatternCSP, groupGraph)                                                \
  F(_KLASS_NAME_, OrderClausesCSP, orderClauses)                                                   \
  F(_KLASS_NAME_, LimitOffsetClauseCSP, limitOffsetClause)

KDB_SPARQL_ALGEBRA_GENERATE(Service, KDB_SPARQL_ALGEBRA_SERVICE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_GROUP_GRAPH_PATTERN_MEMBERS(_KLASS_NAME_, F)                            \
  F(_KLASS_NAME_, QList<NodeCSP>, graphPatterns)                                                   \
  F(_KLASS_NAME_, QList<NodeCSP>, filters)                                                         \
  F(_KLASS_NAME_, QList<ServiceCSP>, services)                                                     \
  F(_KLASS_NAME_, NodeCSP, source)

KDB_SPARQL_ALGEBRA_GENERATE(GroupGraphPattern, KDB_SPARQL_ALGEBRA_GROUP_GRAPH_PATTERN_MEMBERS)

#define KDB_SPARQL_ALGEBRA_VALUE_MEMBERS(_KLASS_NAME_, F) F(_KLASS_NAME_, knowRDF::Literal, value)

KDB_SPARQL_ALGEBRA_GENERATE(Value, KDB_SPARQL_ALGEBRA_VALUE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_OPTIONAL_MEMBERS(_KLASS_NAME_, F)                                       \
  F(_KLASS_NAME_, GroupGraphPatternCSP, group)

KDB_SPARQL_ALGEBRA_GENERATE(Optional, KDB_SPARQL_ALGEBRA_OPTIONAL_MEMBERS)

#define KDB_SPARQL_ALGEBRA_BINARY_MEMBERS(_KLASS_NAME_, F)                                         \
  F(_KLASS_NAME_, NodeCSP, left)                                                                   \
  F(_KLASS_NAME_, NodeCSP, right)

KDB_SPARQL_ALGEBRA_GENERATE(Union, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(Minus, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(LogicalOr, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(LogicalAnd, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(RelationalEqual, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(RelationalDifferent, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(RelationalInferior, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(RelationalSuperior, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(RelationalInferiorEqual, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(RelationalSuperiorEqual, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)

KDB_SPARQL_ALGEBRA_GENERATE(Addition, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(Substraction, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(Multiplication, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(Division, KDB_SPARQL_ALGEBRA_BINARY_MEMBERS)

#define KDB_SPARQL_ALGEBRA_BINARY_IN_MEMBERS(_KLASS_NAME_, F)                                      \
  F(_KLASS_NAME_, NodeCSP, left)                                                                   \
  F(_KLASS_NAME_, QList<NodeCSP>, right)

KDB_SPARQL_ALGEBRA_GENERATE(RelationalIn, KDB_SPARQL_ALGEBRA_BINARY_IN_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(RelationalNotIn, KDB_SPARQL_ALGEBRA_BINARY_IN_MEMBERS)

#define KDB_SPARQL_ALGEBRA_UNARY_MEMBERS(_KLASS_NAME_, F) F(_KLASS_NAME_, NodeCSP, value)

KDB_SPARQL_ALGEBRA_GENERATE(LogicalNegation, KDB_SPARQL_ALGEBRA_UNARY_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(Negation, KDB_SPARQL_ALGEBRA_UNARY_MEMBERS)

#define KDB_SPARQL_ALGEBRA_FUNCTION_CALL_MEMBERS(_KLASS_NAME_, F)                                  \
  F(_KLASS_NAME_, knowCore::Uri, name)                                                             \
  F(_KLASS_NAME_, QList<NodeCSP>, parameters)

KDB_SPARQL_ALGEBRA_GENERATE(FunctionCall, KDB_SPARQL_ALGEBRA_FUNCTION_CALL_MEMBERS)

// An empty Uri for the graph indicates the default graph
#define KDB_SPARQL_ALGEBRA_QUADS_MEMBERS(_KLASS_NAME_, F)                                          \
  F(_KLASS_NAME_, QList<TripleCSP>, triples)                                                       \
  F(_KLASS_NAME_, NodeCSP, source)

KDB_SPARQL_ALGEBRA_GENERATE(Quads, KDB_SPARQL_ALGEBRA_QUADS_MEMBERS)

#define KDB_SPARQL_ALGEBRA_QUADS_DATA_MEMBERS(_KLASS_NAME_, F)                                     \
  F(_KLASS_NAME_, QList<QuadsCSP>, quads)

KDB_SPARQL_ALGEBRA_GENERATE(QuadsData, KDB_SPARQL_ALGEBRA_QUADS_DATA_MEMBERS)

#define KDB_SPARQL_ALGEBRA_INSERT_DELETE_DATA_MEMBERS(_KLASS_NAME_, F)                             \
  F(_KLASS_NAME_, QuadsDataCSP, quadsData)

KDB_SPARQL_ALGEBRA_GENERATE(InsertData, KDB_SPARQL_ALGEBRA_INSERT_DELETE_DATA_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(DeleteData, KDB_SPARQL_ALGEBRA_INSERT_DELETE_DATA_MEMBERS)

#define KDB_SPARQL_ALGEBRA_INSERT_DELETE_INSERT_MEMBERS(_KLASS_NAME_, F)                           \
  F(_KLASS_NAME_, knowCore::Uri, withUri)                                                          \
  F(_KLASS_NAME_, QuadsDataCSP, deleteTemplate)                                                    \
  F(_KLASS_NAME_, QuadsDataCSP, insertTemplate)                                                    \
  F(_KLASS_NAME_, QList<DatasetCSP>, datasets)                                                     \
  F(_KLASS_NAME_, NodeCSP, where)

KDB_SPARQL_ALGEBRA_GENERATE(DeleteInsert, KDB_SPARQL_ALGEBRA_INSERT_DELETE_INSERT_MEMBERS)

#define KDB_SPARQL_ALGEBRA_LOAD_MEMBERS(_KLASS_NAME_, F)                                           \
  F(_KLASS_NAME_, knowCore::Uri, uri)                                                              \
  F(_KLASS_NAME_, knowCore::Uri, graph)                                                            \
  F(_KLASS_NAME_, bool, silent)

KDB_SPARQL_ALGEBRA_GENERATE(Load, KDB_SPARQL_ALGEBRA_LOAD_MEMBERS)

#define KDB_SPARQL_ALGEBRA_DROP_CLEAR_MEMBERS(_KLASS_NAME_, F)                                     \
  F(_KLASS_NAME_, bool, silent)                                                                    \
  F(_KLASS_NAME_, GraphType, graphType)                                                            \
  F(_KLASS_NAME_, knowCore::Uri, graph)

KDB_SPARQL_ALGEBRA_GENERATE(Drop, KDB_SPARQL_ALGEBRA_DROP_CLEAR_MEMBERS)
KDB_SPARQL_ALGEBRA_GENERATE(Clear, KDB_SPARQL_ALGEBRA_DROP_CLEAR_MEMBERS)

#define KDB_SPARQL_ALGEBRA_CREATE_MEMBERS(_KLASS_NAME_, F)                                         \
  F(_KLASS_NAME_, bool, silent)                                                                    \
  F(_KLASS_NAME_, knowCore::Uri, graph)

KDB_SPARQL_ALGEBRA_GENERATE(Create, KDB_SPARQL_ALGEBRA_CREATE_MEMBERS)

#define KDB_SPARQL_ALGEBRA_BIND(_KLASS_NAME_, F)                                                   \
  F(_KLASS_NAME_, QString, name)                                                                   \
  F(_KLASS_NAME_, NodeCSP, expression)

KDB_SPARQL_ALGEBRA_GENERATE(Bind, KDB_SPARQL_ALGEBRA_BIND)

#define KDB_SPARQL_ALGEBRA_EXPLAIN_QUERY(_KLASS_NAME_, F)                                          \
  F(_KLASS_NAME_, QString, options)                                                                \
  F(_KLASS_NAME_, NodeCSP, query)

KDB_SPARQL_ALGEBRA_GENERATE(ExplainQuery, KDB_SPARQL_ALGEBRA_EXPLAIN_QUERY)

// PL/SPARQL Nodes

#define KDB_SPARQL_ALGEBRA_PL_SPARQL_QUERY(_KLASS_NAME_, F) F(_KLASS_NAME_, ListCSP, statements)

KDB_SPARQL_ALGEBRA_GENERATE(PLQuery, KDB_SPARQL_ALGEBRA_PL_SPARQL_QUERY)

#define KDB_SPARQL_ALGEBRA_PL_SPARQL_IF_UNLESS(_KLASS_NAME_, F)                                    \
  F(_KLASS_NAME_, AskQueryCSP, condition)                                                          \
  F(_KLASS_NAME_, ListCSP, ifStatements)                                                           \
  F(_KLASS_NAME_, ListCSP, elseStatements)

KDB_SPARQL_ALGEBRA_GENERATE(If, KDB_SPARQL_ALGEBRA_PL_SPARQL_IF_UNLESS)
KDB_SPARQL_ALGEBRA_GENERATE(Unless, KDB_SPARQL_ALGEBRA_PL_SPARQL_IF_UNLESS)
