#pragma once

#include "Nodes.h"

#include <knowRDF/Literal.h>

namespace kDB::SPARQL::Algebra::Builders
{
  class TriplesList;
#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_) class _KLASS_NAME_;
#include "NodesDefs.h"

#undef KDB_SPARQL_ALGEBRA_GENERATE

  namespace details
  {
    namespace
    {
      template<typename _T_>
      struct field_traits
      {
        using type = _T_;
        static constexpr bool isBuilder = false;
        static const _T_& build(const _T_& _t) { return _t; }
      };

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  template<>                                                                                       \
  struct field_traits<_KLASS_NAME_>                                                                \
  {                                                                                                \
    using type = kDB::SPARQL::Algebra::_KLASS_NAME_##CSP;                                          \
    static constexpr bool isBuilder = false;                                                       \
    template<typename _T_, typename... _TOther_>                                                   \
    static type build(const _T_& _t, const _TOther_&... _others)                                   \
    {                                                                                              \
      return new kDB::SPARQL::Algebra::_KLASS_NAME_(field_traits<_T_>::build(_t),                  \
                                                    field_traits<_TOther_...>::build(_others)...); \
    }                                                                                              \
  };

      template<>
      struct field_traits<NodeCSP>
      {
        using type = NodeCSP;
        static constexpr bool isBuilder = false;
        template<typename _T_>
        static NodeCSP build(const _T_& _t)
        {
          return _t;
        }
      };
      template<>
      inline NodeCSP field_traits<NodeCSP>::build<knowRDF::Literal>(const knowRDF::Literal& _t)
      {
        return new Algebra::Value(_t);
      }

      template<typename... _T_>
      struct field_traits<Alternative<_T_...>>
      {
        using type = Alternative<typename field_traits<_T_>::type::Type...>;
        static constexpr bool isBuilder = false;
        template<typename _TArg_>
          requires(field_traits<_TArg_>::isBuilder)
        static type build(const _TArg_& _t)
        {
          return _t.toNode();
        }
        template<typename _TArg_>
          requires std::is_base_of_v<Node, typename _TArg_::Type>
        static type build(const _TArg_& _t)
        {
          return _t;
        }

        template<typename _TArg_>
          requires(not field_traits<_TArg_>::isBuilder
                   and not std::is_base_of_v<Node, typename _TArg_::Type>)
        static type build(const _TArg_& _t)
        {
          static_assert(true, "Alternative can only be set with a builder or one of the "
                              "alternative value as argument");
        }
      };
      template<typename _T_>
      struct field_traits<QList<_T_>>
      {
        using contained_type = typename field_traits<_T_>::type;
        using type = QList<contained_type>;
        static constexpr bool isBuilder = false;
        template<typename _TArg_, typename... _TOther_>
        static type build(const _TArg_& _t, const _TOther_&... _others)
        {
          type value;
          build(&value, _t, _others...);
          return value;
        }
        template<typename _TArg_>
        static type build(const QList<_TArg_>& _t)
        {
          type value;
          for(int i = 0; i < _t.size(); ++i)
          {
            value.append(field_traits<contained_type>::build(_t));
          }
          return value;
        }
        static type build(const QList<_T_>& _t) { return _t; }
        static type build(const TriplesList& _t);
      private:
        static void build(type*) {}
        template<typename _TArg_, typename... _TOther_>
        static void build(type* _list, const _TArg_& _t, const _TOther_&... _others)
        {
          _list->append(field_traits<contained_type>::build(_t));
          build(_list, _others...);
        }
      };

#include "NodesDefs.h"

#undef KDB_SPARQL_ALGEBRA_GENERATE
    } // namespace

    template<typename _T_>
    struct traits
    {
      static constexpr bool has_custom_constructor = false;
    };
    template<>
    struct traits<Variable>
    {
      static constexpr bool has_custom_constructor = true;
    };
  } // namespace details

#define KDB_SPARQL_ALGEBRA_GENERATE_GENERIC_MEMBER_SETTER(_KLASS_NAME_, _TYPE_, _NAME_, ...)       \
  template<typename... _TOther_>                                                                   \
  _KLASS_NAME_& _NAME_(const _TOther_&... _other);

#define KDB_SPARQL_ALGEBRA_GENERATE_MEMBER_UPDATE(_KLASS_NAME_, _TYPE_, _NAME_, ...)               \
  void update_##_NAME_(const details::field_traits<_TYPE_>::type& _type);

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  class _KLASS_NAME_                                                                               \
  {                                                                                                \
  public:                                                                                          \
    _KLASS_NAME_();                                                                                \
    _KLASS_NAME_(const _KLASS_NAME_&) = delete;                                                    \
    _KLASS_NAME_& operator=(const _KLASS_NAME_&) = delete;                                         \
    template<typename... _TOther_>                                                                 \
      requires(details::traits<_KLASS_NAME_>::has_custom_constructor and sizeof...(_TOther_) != 0) \
    _KLASS_NAME_(const _TOther_&... _other);                                                       \
    ~_KLASS_NAME_();                                                                               \
    QPair<QString, knowCore::ValueHash> toQueryString() const;                                     \
    kDB::SPARQL::Algebra::_KLASS_NAME_##CSP toNode() const;                                        \
    operator kDB::SPARQL::Algebra::_KLASS_NAME_##CSP() const { return toNode(); }                  \
    operator Node##CSP() const { return toNode(); }                                                \
  public:                                                                                          \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_GENERIC_MEMBER_SETTER);                 \
  public:                                                                                          \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_MEMBER_UPDATE);                         \
  private:                                                                                         \
    struct Private;                                                                                \
    Private* const d;                                                                              \
  };

#include "NodesDefs.h"

#undef KDB_SPARQL_ALGEBRA_GENERATE_GENERIC_MEMBER_SETTER
#undef KDB_SPARQL_ALGEBRA_GENERATE_MEMBER_UPDATE
#undef KDB_SPARQL_ALGEBRA_GENERATE

  /**
   * Specialized builder for building a \ref QList<TripleCSP>.
   */
  class TriplesList
  {
  public:
    template<typename _T1_, typename _T2_, typename _T3_>
    void append(const _T1_& _subject, const _T2_& _predicate, const _T3_& _object)
    {
      Triple builder;
      builder.subject(toNode(_subject));
      builder.predicate(toNode(_predicate));
      builder.object(toNode(_object));
      m_triples.append(builder.toNode());
    }
    QList<kDB::SPARQL::Algebra::TripleCSP> toTriples() const { return m_triples; }
    operator QList<kDB::SPARQL::Algebra::TripleCSP>() const { return toTriples(); }
    operator QList<kDB::SPARQL::Algebra::NodeCSP>() const;
  private:
    NodeCSP toNode(const NodeCSP& _node) { return _node; }
    NodeCSP toNode(const knowCore::Uri& _node) { return new Algebra::Term(_node); }

    QList<kDB::SPARQL::Algebra::TripleCSP> m_triples;
  };

  namespace details
  {
    void customConstructor(Variable* _var_builder, const QString& _name)
    {
      _var_builder->name(_name);
    }
  } // namespace details

#define KDB_SPARQL_ALGEBRA_GENERATE_GENERIC_MEMBER_SETTER_DEFINITION(_KLASS_NAME_, _TYPE_, _NAME_, \
                                                                     ...)                          \
  template<typename... _TOther_>                                                                   \
  _KLASS_NAME_& _KLASS_NAME_::_NAME_(const _TOther_&... _other)                                    \
  {                                                                                                \
    update_##_NAME_(details::field_traits<_TYPE_>::build(_other...));                              \
    return *this;                                                                                  \
  }

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  template<typename... _TOther_>                                                                   \
    requires(details::traits<_KLASS_NAME_>::has_custom_constructor and sizeof...(_TOther_) != 0)   \
  _KLASS_NAME_::_KLASS_NAME_(const _TOther_&... _other) : _KLASS_NAME_::_KLASS_NAME_()             \
  {                                                                                                \
    details::customConstructor(this, _other...);                                                   \
  }                                                                                                \
  _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_GENERIC_MEMBER_SETTER_DEFINITION);

#include "NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
#undef KDB_SPARQL_ALGEBRA_GENERATE_GENERIC_MEMBER_SETTER_DEFINITION

  namespace details
  {
    template<typename _T_>
    typename field_traits<QList<_T_>>::type field_traits<QList<_T_>>::build(const TriplesList& _t)
    {
      return _t;
    }
  } // namespace details

} // namespace kDB::SPARQL::Algebra::Builders
