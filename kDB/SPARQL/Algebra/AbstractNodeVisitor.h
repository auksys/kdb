#ifndef _KDB_SPARQL_ALGEBRA_ABSTRACT_NODE_VISITOR_H_
#define _KDB_SPARQL_ALGEBRA_ABSTRACT_NODE_VISITOR_H_

#include "Nodes.h"

namespace kDB::SPARQL::Algebra
{
  namespace details
  {
    class AbstractNodeVisitorAdaptor
    {
    public:
      AbstractNodeVisitorAdaptor();
      ~AbstractNodeVisitorAdaptor();
#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  virtual void call_visit(_KLASS_NAME_##CSP _node, void* _r, void* _parameter) = 0;
#include "NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
      // protected:
      // void accept(NodeCSP _node, void* _r, void* _parameter)
      // {
      //   return accept(_r, _node.data(), _parameter);
      // }
      // void accept(const Node* _node, void* _r, void* _parameter)
      // {
      //   if(_node)
      //   {
      //     _node->accept(this, _r, _parameter);
      //   }
      // }
    };
  } // namespace details

  template<typename _TR_, typename... _TArgs_>
  class AbstractNodeVisitor : details::AbstractNodeVisitorAdaptor
  {
    friend class Node;
  public:
    using ParametersTuple = std::tuple<_TArgs_...>;
    static constexpr std::size_t ParametersCount = sizeof...(_TArgs_);
    using ReturnType = _TR_;
  public:
    AbstractNodeVisitor() {}
    ~AbstractNodeVisitor() {}
  public:
    /**
     * Call this function to start visiting
     */
    _TR_ start(NodeCSP _node, const _TArgs_&... _args) { return accept(_node, _args...); }
#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
private:                                                                                           \
  virtual _TR_ visit(_KLASS_NAME_##CSP _node, const _TArgs_&... _parameter) = 0;                   \
  virtual void call_visit(_KLASS_NAME_##CSP _node, void* _r, void* _parameter) override            \
  {                                                                                                \
    typedef _TR_ (AbstractNodeVisitor::*F)(_KLASS_NAME_##CSP _node, const _TArgs_&...);            \
    *reinterpret_cast<_TR_*>(_r)                                                                   \
      = std::apply(F(&AbstractNodeVisitor::visit),                                                 \
                   std::tuple_cat(std::make_tuple(this, _node),                                    \
                                  *reinterpret_cast<ParametersTuple*>(_parameter)));               \
  }
#include "NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
  protected:
    _TR_ accept(NodeCSP _node, const _TArgs_... _arguments)
    {
      return accept(_node.data(), _arguments...);
    }
    _TR_ accept(const Node* _node, const _TArgs_... _arguments)
    {
      _TR_ r;
      if(_node)
      {
        ParametersTuple pt = std::make_tuple(_arguments...);
        _node->accept(this, &r, &pt);
      }
      return r;
    }
  };

  template<typename... _TArgs_>
  class AbstractNodeVisitor<void, _TArgs_...> : details::AbstractNodeVisitorAdaptor
  {
    friend class Node;
  public:
    using ParametersTuple = std::tuple<_TArgs_...>;
    static constexpr std::size_t ParametersCount = sizeof...(_TArgs_);
    using ReturnType = void;
  public:
    AbstractNodeVisitor() {}
    ~AbstractNodeVisitor() {}
  public:
    /**
     * Call this function to start visiting
     */
    void start(NodeCSP _node, const _TArgs_&... _args) { accept(_node, _args...); }
#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
private:                                                                                           \
  virtual void visit(_KLASS_NAME_##CSP _node, const _TArgs_&... _parameter) = 0;                   \
  virtual void call_visit(_KLASS_NAME_##CSP _node, void*, void* _parameter) override               \
  {                                                                                                \
    typedef void (AbstractNodeVisitor::*F)(_KLASS_NAME_##CSP _node, const _TArgs_&...);            \
    std::apply(F(&AbstractNodeVisitor::visit),                                                     \
               std::tuple_cat(std::make_tuple(this, _node),                                        \
                              *reinterpret_cast<ParametersTuple*>(_parameter)));                   \
  }
#include "NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
  protected:
    void accept(NodeCSP _node, const _TArgs_&... _arguments)
    {
      accept(_node.data(), _arguments...);
    }
    void accept(const Node* _node, const _TArgs_&... _arguments)
    {
      if(_node)
      {
        ParametersTuple pt = std::make_tuple(_arguments...);
        _node->accept(this, nullptr, &pt);
      }
    }
  };
} // namespace kDB::SPARQL::Algebra

#endif
