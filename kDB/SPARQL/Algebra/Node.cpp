#include "Node_p.h"

using namespace kDB::SPARQL::Algebra;

Node::Node(NodeType _type, Node::Private* _d) : d(_d) { d->type = _type; }

Node::~Node() { delete d; }

NodeType Node::type() const { return d->type; }
