#pragma once

#include "AbstractNodeVisitor.h"

#include <knowCore/Global.h>

#define __KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD(_NAME_, _I_)                                    \
  Return visit(_NAME_##CSP _node, const Parameter& _parameter);

#define KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD(...)                                              \
  KNOWCORE_FOREACH(__KDB_SPARQL_ALGEBRA_NODE_VISITOR_OVERLOAD, __VA_ARGS__)

namespace kDB::SPARQL::Algebra
{
  /**
   * @ingroup kDB_SPARQL_Algebra
   * Base AbstractNodeVisitor with empty implementation
   */
  // TODO rename to DefaultNodeVisitor?
  template<typename _TR_, typename... _TArgs_>
  class NodeVisitorImplementation : public AbstractNodeVisitor<_TR_, _TArgs_...>
  {
  protected:
    virtual _TR_ visitDefault(NodeCSP _node, const _TArgs_&...) = 0;
#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  _TR_ visit(_KLASS_NAME_##CSP _node, const _TArgs_&... _args) override                            \
  {                                                                                                \
    return visitDefault(_node, _args...);                                                          \
  }
#include "NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
  };
} // namespace kDB::SPARQL::Algebra
