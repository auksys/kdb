#include "Nodes.h"

#include <QVariant>

#include <knowRDF/Literal.h>

#include "AbstractNodeVisitor.h"

#include "Node_p.h"

using namespace kDB::SPARQL::Algebra;

#define KDB_SPARQL_ALGEBRA_GENERATE_PRIVATE_MEMBER(_KLASS_NAME_, _TYPE_, _NAME_) _TYPE_ m_##_NAME_;

#define KDB_SPARQL_ALGEBRA_GENERATE_ACCESSOR_DEFINITION(_KLASS_NAME_, _TYPE_, _NAME_)              \
  _TYPE_ _KLASS_NAME_::_NAME_() const { return static_cast<Private*>(d)->m_##_NAME_; }

#define KDB_SPARQL_GENERATE_DESTRUCTOR(_KLASS_NAME_, _TYPE_, _NAME_)

#define KDB_SPARQL_ALGEBRA_GENERATE_ASSIGNMENT(_KLASS_NAME_, _TYPE_, _NAME_)                       \
  static_cast<Private*>(d)->m_##_NAME_ = _##_NAME_;

#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_)                                    \
  struct _KLASS_NAME_::Private : public Node::Private                                              \
  {                                                                                                \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_PRIVATE_MEMBER)                         \
  };                                                                                               \
                                                                                                   \
  _KLASS_NAME_::_KLASS_NAME_(                                                                      \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_CONSTRUCTOR_ARGUMENT) void* _)          \
      : Node(NodeType::_KLASS_NAME_, new Private)                                                  \
  {                                                                                                \
    Q_UNUSED(_);                                                                                   \
    _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_ASSIGNMENT)                             \
  }                                                                                                \
                                                                                                   \
  _KLASS_NAME_::~_KLASS_NAME_(){_MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_GENERATE_DESTRUCTOR)}        \
                                                                                                   \
  _MEMBER_DEF_(_KLASS_NAME_, KDB_SPARQL_ALGEBRA_GENERATE_ACCESSOR_DEFINITION)                      \
                                                                                                   \
    void _KLASS_NAME_::accept(details::AbstractNodeVisitorAdaptor* _visitor_adaptor, void* _r,     \
                              void* _parameter) const                                              \
  {                                                                                                \
    return _visitor_adaptor->call_visit(this, _r, _parameter);                                     \
  }

#include "NodesDefs.h"
