#ifndef _KDB_SPARQL_ALGEBRA_ABSTRACT_NODE_H_
#define _KDB_SPARQL_ALGEBRA_ABSTRACT_NODE_H_

#include <knowCore/ConstExplicitlySharedDataPointer.h>
#include <knowCore/Global.h>

#include "NodeType.h"

#include <kDB/Forward.h>

namespace kDB::SPARQL::Algebra
{
  template<typename _TR_, typename... _TArgs_>
  class AbstractNodeVisitor;
  namespace details
  {
    class AbstractNodeVisitorAdaptor;
  }

  class Node : public QSharedData
  {
    template<typename _TR_, typename... _TArgs_>
    friend class AbstractNodeVisitor;
  protected:
    struct Private;
    Private* d;
    Node(NodeType _type, Private*);
  public:
    virtual ~Node();
  public:
    NodeType type() const;
  protected:
    virtual void accept(details::AbstractNodeVisitorAdaptor* _node, void* _r,
                        void* _parameter) const
      = 0;
  };
  typedef knowCore::ConstExplicitlySharedDataPointer<Node> NodeCSP;
  template<typename... _T_>
  class Alternative
  {
    static_assert(std::conjunction_v<std::is_base_of<Node, _T_>...>,
                  "All types should subclass Node");
  public:
    Alternative() : m_type(NodeType::__Invalid__) {}
    template<typename _To_>
      requires knowCore::details::contains_v<_To_, _T_...>
    Alternative(knowCore::ConstExplicitlySharedDataPointer<_To_> _v)
        : m_node(_v), m_type(_v->type())
    {
    }
    template<typename _To_>
      requires knowCore::details::contains_v<_To_, _T_...>
    Alternative(_To_* _v) : m_node(_v), m_type(_v->type())
    {
    }
    NodeType type() const { return m_type; }
    template<typename _To_>
      requires knowCore::details::contains_v<_To_, _T_...>
    knowCore::ConstExplicitlySharedDataPointer<_To_> value() const
    {
      return m_node.s_cast<_To_>();
    }
    bool isValid() const { return m_type != NodeType::__Invalid__; }
    NodeCSP node() const { return m_node; }
  private:
    NodeCSP m_node;
    NodeType m_type;
  };
} // namespace kDB::SPARQL::Algebra

#endif
