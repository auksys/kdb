#include "Node.h"

namespace kDB
{
  namespace SPARQL
  {
    namespace Algebra
    {
      struct Node::Private
      {
        virtual ~Private() {}
        NodeType type;
      };
    } // namespace Algebra
  } // namespace SPARQL
} // namespace kDB
