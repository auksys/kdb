namespace kDB
{
  namespace SPARQL
  {
    namespace Algebra
    {
      enum class NodeType
      {
#define KDB_SPARQL_ALGEBRA_GENERATE(_KLASS_NAME_, _MEMBER_DEF_) _KLASS_NAME_,
#include "NodesDefs.h"
#undef KDB_SPARQL_ALGEBRA_GENERATE
        __Invalid__
      };
      const char* nodeTypeName(NodeType _nt);
    } // namespace Algebra
  } // namespace SPARQL
} // namespace kDB
