/*
 *  Copyright (c) 2008,2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Token_p.h"

#include <clog_qt>

using namespace kDB::SPARQL;

QString Token::typeToString(Type type)
{
  switch(type)
  {
    // Not really token
  case END_OF_FILE:
    return "end of file";
  case END_OF_LINE:
    return "end of line";
  case UNKNOWN:
    return "unknown token";
  case UNKNOWN_BINDING:
    return "unknown binding";
  case INVALID_BINDING:
    return "invalid binding";
  case UNFINISHED_STRING:
    return "unfinished string";
    // Special characters
  case Token::SEMI:
    return ";";
  case Token::COLON:
    return ":";
  case Token::COMA:
    return ",";
  case Token::DOT:
    return ".";
  case Token::STARTBRACE:
    return "{";
  case Token::ENDBRACE:
    return "}";
  case Token::STARTBRACKET:
    return "(";
  case Token::ENDBRACKET:
    return ")";
  case Token::STARTBOXBRACKET:
    return "[";
  case Token::ENDBOXBRACKET:
    return "]";
  case Token::EQUAL:
    return "=";
  case Token::DIFFERENT:
    return "!=";
  case Token::AND:
    return "&&";
  case Token::OR:
    return "||";
  case Token::INFERIOR:
    return "<";
  case Token::INFERIOREQUAL:
    return "<=";
  case Token::SUPPERIOR:
    return ">";
  case Token::SUPPERIOREQUAL:
    return ">=";
  case Token::PLUS:
    return "+";
  case Token::MINUS:
    return "-";
  case Token::MULTIPLY:
    return "*";
  case Token::DIVIDE:
    return "/";
  case Token::EXCLAMATION:
    return "!";
  case LANG_TAG:
    return "language tag";
  case UNDERSCORECOLON:
    return "_:";
  case CIRCUMFLEXCIRCUMFLEX:
    return "^^";
    // Constants
  case FLOAT_CONSTANT:
    return "float constant";
  case INTEGER_CONSTANT:
    return "integer constant";
  case STRING_CONSTANT:
    return "string constant";
  case URI_CONSTANT:
    return "uri constant";
  case NAME:
    return "name";
  case Token::VARIABLE:
    return "variable";
  case Token::CURIE_CONSTANT:
    return "curie constant";
  case Token::BINDING:
    return "binding";
    // Special values,
  case Token::FALSE:
    return "false";
  case Token::TRUE:
    return "true";
    // Keywords
  case Token::A:
    return "a";
#define KDB_SPARQL_KEYWORD(_NAME_)                                                                 \
  case _NAME_:                                                                                     \
    return #_NAME_;
#include "Keywords.h"
#include "PlKeywords.h"
#undef KDB_SPARQL_KEYWORD
  case Token::___:
    clog_fatal("invalid token");
  }
  return clog_qt::qformat("[Unknown token] ({})", int(type));
}

Token::Token() : type(UNKNOWN), line(-1), column(-1) {}

Token::Token(const knowCore::Curie& _curie, int _line, int _column)
    : type(Type::CURIE_CONSTANT), line(_line), column(_column), curie(_curie)
{
}

Token::Token(Type _type, int _line, int _column) : type(_type), line(_line), column(_column) {}

Token::Token(Type _type, const QString& _string, int _line, int _column)
    : type(_type), line(_line), column(_column), string(_string)
{
  clog_assert(_type == STRING_CONSTANT or _type == INTEGER_CONSTANT or _type == FLOAT_CONSTANT
              or type == UNFINISHED_STRING or type == URI_CONSTANT or type == VARIABLE
              or type == NAME or _type == LANG_TAG or _type == BINDING or _type == UNKNOWN_BINDING
              or _type == INVALID_BINDING);
}

bool Token::isConstant() const
{
  return type == Token::INTEGER_CONSTANT or type == Token::FLOAT_CONSTANT
         or type == Token::URI_CONSTANT or type == Token::STRING_CONSTANT;
}

bool Token::isPrimary() const { return isConstant() or type == Token::NAME; }

bool Token::isBinaryOperator() const { return binaryOperationPriority() != -1; }

enum Priority
{
  OR_PRIORITY,
  AND_PRIORITY,
  EQUALITY_PRIORITY,
  RELATIONAL_PRIORITY,
  ADDITIVE_PRIORITY,
  MULTIPLICATIVE_PRIORITY
};

int Token::binaryOperationPriority() const
{
  switch(type)
  {
  case Token::OR:
    return OR_PRIORITY;
  case Token::AND:
    return AND_PRIORITY;
  case Token::EQUAL:
  case Token::DIFFERENT:
    return EQUALITY_PRIORITY;
  case Token::INFERIOREQUAL:
  case Token::INFERIOR:
  case Token::SUPPERIOREQUAL:
  case Token::SUPPERIOR:
    return RELATIONAL_PRIORITY;
  case Token::PLUS:
  case Token::MINUS:
    return ADDITIVE_PRIORITY;
  case Token::MULTIPLY:
  case Token::DIVIDE:
    return MULTIPLICATIVE_PRIORITY;
  default:
    return -1;
  }
}

bool Token::isUnaryOperator() const
{
  return type == Token::PLUS or type == Token::MINUS or type == Token::NOT;
}

bool Token::isOperator() const { return isUnaryOperator() or isBinaryOperator(); }

bool Token::isExpressionTerminal()
{
  return type == Token::SEMI or type == Token::ENDBRACKET or type == Token::COMA
         or type == Token::ENDBOXBRACKET or type == Token::ENDBRACE or type == Token::END_OF_LINE
         or type == Token::END_OF_FILE;
}

QString Token::toString() const
{
  QString str = typeToString(type);
  switch(type)
  {
  case Token::FLOAT_CONSTANT:
  case Token::STRING_CONSTANT:
  case Token::UNFINISHED_STRING:
  case Token::INTEGER_CONSTANT:
  case Token::URI_CONSTANT:
  case Token::VARIABLE:
  case Token::NAME:
  case Token::BINDING:
  case Token::UNKNOWN_BINDING:
    str += "(" + string + ")";
    break;
  case Token::CURIE_CONSTANT:
    str += "(" + curie.prefix() + ":" + curie.suffix() + ")";
  default:
    break;
  }
  return str;
}
