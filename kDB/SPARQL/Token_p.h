/*
 *  Copyright (c) 2008,2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#ifndef _SPARQL_TOKEN_H_
#define _SPARQL_TOKEN_H_

#include <QString>
#include <sstream>

#include <knowCore/Curie.h>

namespace kDB
{
  namespace SPARQL
  {
    struct Token
    {
      /**
       * List of possible token type
       */
      enum Type
      {
        // Not really token
        INVALID_BINDING = -6,
        UNKNOWN_BINDING = -5,
        UNFINISHED_STRING = -4,
        END_OF_FILE = -3,
        END_OF_LINE = -2,
        UNKNOWN = -1,
        // Special characters
        SEMI = 0,             ///< ;
        COLON,                ///< :
        COMA,                 ///< ,
        DOT,                  ///< .
        STARTBRACE,           ///< {
        ENDBRACE,             ///< }
        STARTBRACKET,         ///< (
        ENDBRACKET,           ///< )
        STARTBOXBRACKET,      ///< [
        ENDBOXBRACKET,        ///< ]
        EQUAL,                ///< =
        DIFFERENT,            ///< !=
        AND,                  ///< and &&
        OR,                   ///< or ||
        INFERIOR,             ///< <
        INFERIOREQUAL,        ///< <=
        SUPPERIOR,            ///< >
        SUPPERIOREQUAL,       ///< >=
        PLUS,                 ///< + followed by a space
        MINUS,                ///< -
        MULTIPLY,             ///< *
        DIVIDE,               ///< /
        EXCLAMATION,          ///< not !
        UNDERSCORECOLON,      ///< _:
        CIRCUMFLEXCIRCUMFLEX, ///< ^^
                              // Constants
        FLOAT_CONSTANT,
        INTEGER_CONSTANT,
        STRING_CONSTANT,
        URI_CONSTANT,
        NAME,
        LANG_TAG,
        VARIABLE,
        CURIE_CONSTANT,
        BINDING,
        // Special values,
        FALSE,
        TRUE,
#define KDB_SPARQL_KEYWORD(_NAME_) _NAME_,
        // Keywords
        A,
#include "Keywords.h"
      // PL/SPARQL
#include "PlKeywords.h"
#undef KDB_SPARQL_KEYWORD
        ___
      };
      /// type of the token
      Type type;
      /// line of the token
      int line;
      /// Column of the token
      int column;
      /// String or identifier name
      QString string;
      // Curie
      knowCore::Curie curie;
      Token();
      Token(const knowCore::Curie& _curie, int _line, int _column);
      /**
       * Creates a token of the given type
       */
      Token(Type _type, int _line, int _column);
      /**
       * Creates an identifier or a string constant or a number constant
       */
      Token(Type _type, const QString& _string, int _line, int _column);
      bool isExpressionTerminal();
      bool isConstant() const;
      bool isBinaryOperator() const;
      int binaryOperationPriority() const;
      bool isUnaryOperator() const;
      bool isOperator() const;
      bool isPrimary() const;
      static QString typeToString(Type);
      QString toString() const;
      bool operator==(const Token& _rhs) const
      {
        return type == _rhs.type and line == _rhs.line and column == _rhs.column
               and string == _rhs.string and curie == _rhs.curie;
      }
    };
  } // namespace SPARQL
}; // namespace kDB

#include <knowCore/Formatter.h>

clog_format_declare_formatter(kDB::SPARQL::Token::Type)
{
  return format_to(ctx.out(), "{}", kDB::SPARQL::Token::typeToString(p));
}

#endif
