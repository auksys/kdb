/*
 *  Copyright (c) 2008,2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#ifndef _SPARQL_LEXER_BASE_H_
#define _SPARQL_LEXER_BASE_H_

class QIODevice;

#include <QVariantHash>

#include <knowCore/LexerTextStream.h>
#include <knowCore/ValueHash.h>

#include "Token_p.h"

namespace kDB
{
  namespace SPARQL
  {
    /**
     * @internal
     * This class provide some base functions to lexers. Such as eating spaces,
     * or managing the flow.
     * @ingroup Cauchy
     */
    class Lexer
    {
    public:
      Lexer(QIODevice* sstream, const knowCore::ValueHash& _bindings);
      Lexer(const QString& string, const knowCore::ValueHash& _bindings);
      ~Lexer();
    public:
      Token nextToken();
      void setCurieLexingEnabled(bool _v);
      bool isCurieLexingEnabled() const;
      void setPrefixLexingEnabled(bool _v);
      void setUriLexingEnabled(bool _v);
      bool isUriLexingEnabled() const;
      void setPLLexingEnabled(bool _v);
      bool isPLLexingEnabled() const;
    protected:
      /**
       * Get an identifier (or keyword) in the current flow of character.
       */
      enum NameMode
      {
        PN_LOCAL,
        VARNAME,
        IDENTIFIER
      };
      QString getName(knowCore::LexerTextStream::Element lastChar, NameMode nameMode);
      Token getDigit(knowCore::LexerTextStream::Element lastChar);
      Token getString(const QString& terminator, Token::Type _type);
    private:
      struct Private;
      Private* const d;
    };
  } // namespace SPARQL
} // namespace kDB

#endif
