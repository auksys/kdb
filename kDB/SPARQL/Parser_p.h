/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Algebra/Node.h"
#include <QVariant>

#include <knowCore/Forward.h>

namespace kDB::SPARQL
{
  class Lexer;
  class Parser
  {
  public:
    Parser(Lexer* _lexer, const knowCore::ValueHash& _bindings, const knowCore::Uri& _base);
    ~Parser();
    QList<Algebra::NodeCSP> parse();
    const knowCore::Messages& messages() const;
  private:
    struct Private;
    Private* const d;
  };
} // namespace kDB::SPARQL
