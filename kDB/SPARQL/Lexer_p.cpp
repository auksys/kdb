/*
 *  Copyright (c) 2008,2010 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Lexer_p.h"

#include <QIODevice>
#include <QString>
#include <stdio.h>

#include <clog_qt>

#include "Token_p.h"

using namespace kDB::SPARQL;

#define IDENTIFIER_IS_KEYWORD(tokenname, tokenid)                                                  \
  if(identifierStr == tokenname)                                                                   \
  {                                                                                                \
    return Token(Token::tokenid, firstChar.line, firstChar.column);                                \
  }

#define CHAR_IS_TOKEN(tokenchar, tokenid)                                                          \
  if(lastChar == tokenchar)                                                                        \
  {                                                                                                \
    return Token(Token::tokenid, firstChar.line, firstChar.column);                                \
  }

#define CHAR_IS_TOKEN_OR_TOKEN(tokenchar, tokendecidechar, tokenid_1, tokenid_2)                   \
  if(lastChar == tokenchar)                                                                        \
  {                                                                                                \
    knowCore::LexerTextStream::Element nextChar = d->stream.getNextChar();                         \
    if(nextChar == tokendecidechar)                                                                \
    {                                                                                              \
      return Token(Token::tokenid_2, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      d->stream.unget(nextChar);                                                                   \
      return Token(Token::tokenid_1, firstChar.line, firstChar.column);                            \
    }                                                                                              \
  }

#define CHAR_IS_TOKEN_OR_TOKEN_OR_TOKEN(tokenchar_1, tokenchar_2, tokenchar_3, tokenid_1,          \
                                        tokenid_2, tokenid_3)                                      \
  if(lastChar == tokenchar_1)                                                                      \
  {                                                                                                \
    knowCore::LexerTextStream::Element nextChar = d->stream.getNextChar();                         \
    if(nextChar == tokenchar_2)                                                                    \
    {                                                                                              \
      return Token(Token::tokenid_2, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else if(nextChar == tokenchar_3)                                                               \
    {                                                                                              \
      return Token(Token::tokenid_3, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      d->stream.unget(nextChar);                                                                   \
      return Token(Token::tokenid_1, firstChar.line, firstChar.column);                            \
    }                                                                                              \
  }

#define CHAR_IS_TOKEN_OR_TOKEN_OR_TOKEN_OR_TOKEN(                                                  \
  tokenchar_1, tokenchar_2, tokenchar_3, tokenchar_4, tokenid_1, tokenid_2, tokenid_3, tokenid_4)  \
  if(lastChar == tokenchar_1)                                                                      \
  {                                                                                                \
    knowCore::LexerTextStream::Element nextChar = d->stream.getNextChar();                         \
    if(nextChar == tokenchar_2)                                                                    \
    {                                                                                              \
      return Token(Token::tokenid_2, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else if(nextChar == tokenchar_3)                                                               \
    {                                                                                              \
      return Token(Token::tokenid_3, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else if(nextChar == tokenchar_4)                                                               \
    {                                                                                              \
      return Token(Token::tokenid_4, firstChar.line, firstChar.column);                            \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      d->stream.unget(nextChar);                                                                   \
      return Token(Token::tokenid_1, firstChar.line, firstChar.column);                            \
    }                                                                                              \
  }

struct Lexer::Private
{
  bool curieLexingEnabled = false;
  bool prefixLexingEnabled = false;
  bool uriLexingEnabled = true;
  bool plLexingEnabled = false;
  knowCore::ValueHash bindings;
  knowCore::LexerTextStream stream;
};

Lexer::Lexer(QIODevice* sstream, const knowCore::ValueHash& _bindings) : d(new Private)
{
  d->bindings = _bindings;
  d->stream.setDevice(sstream);
}

Lexer::Lexer(const QString& string, const knowCore::ValueHash& _bindings) : d(new Private)
{
  d->bindings = _bindings;
  d->stream.setString(string);
}

Lexer::~Lexer() { delete d; }

void Lexer::setCurieLexingEnabled(bool _v) { d->curieLexingEnabled = _v; }

bool Lexer::isCurieLexingEnabled() const { return d->curieLexingEnabled; }

void Lexer::setPrefixLexingEnabled(bool _v) { d->prefixLexingEnabled = _v; }

void Lexer::setUriLexingEnabled(bool _v) { d->uriLexingEnabled = _v; }

bool Lexer::isUriLexingEnabled() const { return d->uriLexingEnabled; }

void Lexer::setPLLexingEnabled(bool _v) { d->plLexingEnabled = _v; }

bool Lexer::isPLLexingEnabled() const { return d->plLexingEnabled; }

QString Lexer::getName(knowCore::LexerTextStream::Element lastChar, NameMode nameMode)
{
  QString identifierStr;
  if(lastChar.content != QChar(0))
  {
    identifierStr += lastChar.content;
  }
  while(not d->stream.eof())
  {
    lastChar = d->stream.getNextChar();

    bool exit = true;
    switch(nameMode)
    {
    case PN_LOCAL:
      exit = lastChar.isSpace() or lastChar == '(' or lastChar == ')' or lastChar == '{'
             or lastChar == '}' or lastChar == ';' or lastChar == ',';
      break;
    case IDENTIFIER:
      exit = lastChar != '.';
      if(d->curieLexingEnabled or d->prefixLexingEnabled)
      {
        exit = exit and lastChar != '-';
      }
      Q_FALLTHROUGH();
    case VARNAME:
      exit = exit and lastChar != '_' and not(lastChar.isLetterOrDigit());
      break;
    }
    if(exit)
    {
      d->stream.unget(lastChar);
      break;
    }

    identifierStr += lastChar.content;
  }
  return identifierStr;
}

Token Lexer::getDigit(knowCore::LexerTextStream::Element lastChar)
{
  auto [number, isinteger] = d->stream.getDigit(lastChar);

  if(isinteger)
  {
    return Token(Token::INTEGER_CONSTANT, number.content, number.line, number.column);
  }
  else
  {
    return Token(Token::FLOAT_CONSTANT, number.content, number.line, number.column);
  }
}

Token Lexer::getString(const QString& terminator, Token::Type _type)
{
  auto [string, finished] = d->stream.getString(terminator);

  if(finished)
  {
    return Token(_type, string.content, string.line, string.column);
  }
  else
  {
    return Token(Token::UNFINISHED_STRING, string.content, string.line, string.column);
  }
}

Token Lexer::nextToken()
{
  knowCore::LexerTextStream::Element lastChar = d->stream.getNextNonSeparatorChar();
  const knowCore::LexerTextStream::Element firstChar = lastChar;
  if(lastChar.eof)
    return Token(Token::END_OF_FILE, lastChar.line, lastChar.column);
  QString identifierStr;
  // Test for comment

  if(lastChar == '#')
  { // Starting a comment
    while(not d->stream.eof())
    {
      lastChar = d->stream.getNextChar();
      if(lastChar == '\n')
      {
        return nextToken();
      }
    }
  }
  if(lastChar == '<' and d->uriLexingEnabled)
  { // This could be an URI
    knowCore::LexerTextStream::Element nc;
    while((nc = d->stream.getNextChar()).isSpace())
    {
    }
    if(nc.isDigit())
    {
      d->stream.unget(nc);
    }
    else
    {
      d->stream.unget(nc);
      Token str = getString(">", Token::URI_CONSTANT);
      if(str.string.startsWith('%'))
      {
        if(d->bindings.contains(str.string))
        {
          auto [success, value, errMsg] = d->bindings.value(str.string).value<knowCore::Uri>();
          if(success)
          {
            return Token(Token::URI_CONSTANT, value.value(), firstChar.line, firstChar.column);
          }
          else
          {
            return Token(Token::INVALID_BINDING, value.value(), firstChar.line, firstChar.column);
          }
        }
        else
        {
          return Token(Token::UNKNOWN_BINDING, firstChar.line, firstChar.column);
        }
      }
      else
      {
        return str;
      }
    }
  }

  if(lastChar == '_')
  {
    knowCore::LexerTextStream::Element nextChar = d->stream.getNextChar();
    if(nextChar == ':')
    {
      return Token(Token::UNDERSCORECOLON, firstChar.line, firstChar.column);
    }
    else
    {
      d->stream.unget(nextChar);
    }
  }

  if(lastChar == ':' and d->curieLexingEnabled)
  {
    knowCore::LexerTextStream::Element nc = d->stream.getNextChar();
    if(nc.isSpace())
    {
      d->stream.unget(nc);
      return Token(knowCore::Curie(), firstChar.line, firstChar.column);
    }
    else
    {
      return Token(knowCore::Curie(QString(), getName(nc, PN_LOCAL)), firstChar.line,
                   firstChar.column);
    }
  }
  else if(lastChar == '?' or lastChar == '$')
  // if it is ? and $
  {
    knowCore::LexerTextStream::Element nc = d->stream.getNextChar();
    return Token(Token::VARIABLE, getName(nc, VARNAME), firstChar.line, firstChar.column);
  }
  else if(lastChar == '%')
  // if it is %
  {
    return Token(Token::BINDING, getName(lastChar, VARNAME), firstChar.line, firstChar.column);
  }
  else if(lastChar.isLetter() or lastChar == '_' or lastChar == '@')
  // if it is alpha, it's an identifier or a keyword or a curie
  {
    identifierStr = getName(lastChar, IDENTIFIER);
    if(identifierStr == "a")
    {
      knowCore::LexerTextStream::Element nc = d->stream.getNextChar();
      d->stream.unget(nc);
      if(nc.isSpace())
      {
        return Token(Token::A, firstChar.line, firstChar.column);
      }
    }
    QString identifier_str_orig = identifierStr;
    // Case sensitive
    IDENTIFIER_IS_KEYWORD("true", TRUE);
    IDENTIFIER_IS_KEYWORD("false", FALSE);

    // Case insensitive
    identifierStr = identifierStr.toUpper();

    IDENTIFIER_IS_KEYWORD("AND", AND);
    IDENTIFIER_IS_KEYWORD("IN", IN);
    IDENTIFIER_IS_KEYWORD("NOT", NOT);
    IDENTIFIER_IS_KEYWORD("OR", OR);

#define KDB_SPARQL_KEYWORD(_NAME_) IDENTIFIER_IS_KEYWORD(#_NAME_, _NAME_);
#include "Keywords.h"

    if(d->plLexingEnabled)
    {
#include "PlKeywords.h"
    }
#undef KDB_SPARQL_KEYWORD

    if(identifierStr[0] == '@')
    {
      return Token(Token::LANG_TAG, identifier_str_orig.right(identifier_str_orig.length() - 1),
                   firstChar.line, firstChar.column);
    }
    if(d->curieLexingEnabled)
    {
      knowCore::LexerTextStream::Element nc = d->stream.getNextChar();
      if(nc == ':')
      {
        nc = d->stream.getNextChar();
        if(nc.isSpace())
        {
          d->stream.unget(nc);
          return Token(knowCore::Curie(identifier_str_orig, QString()), firstChar.line,
                       firstChar.column);
        }
        else
        {
          QString name = getName(nc, PN_LOCAL);
          if(name.startsWith('%'))
          {
            if(d->bindings.contains(name))
            {
              const auto [success, value, errMsg] = d->bindings.value(name).value<QString>();
              if(success)
              {
                name = value.value();
              }
              else
              {
                return Token(Token::INVALID_BINDING, value.value(), firstChar.line,
                             firstChar.column);
              }
            }
            else
            {
              return Token(Token::UNKNOWN_BINDING, firstChar.line, firstChar.column);
            }
          }
          return Token(knowCore::Curie(identifier_str_orig, name), firstChar.line,
                       firstChar.column);
        }
      }
      else
      {
        d->stream.unget(nc);
      }
    }
    return Token(Token::NAME, identifier_str_orig, firstChar.line, firstChar.column);
  }
  else if(lastChar.isDigit())
  { // if it's a digit
    return getDigit(lastChar);
  }
  else if(lastChar == '"')
  {
    knowCore::LexerTextStream::Element c1 = d->stream.getNextChar();
    if(c1 == '"')
    {
      knowCore::LexerTextStream::Element c2 = d->stream.getNextChar();
      if(c2 == '"')
      {
        return getString("\"\"\"", Token::STRING_CONSTANT);
      }
      else
      { // Empty string
        d->stream.unget(c2);
        Token tok;
        return Token(Token::STRING_CONSTANT, QString(), firstChar.line, firstChar.column);
      }
    }
    else
    {
      d->stream.unget(c1);
    }
    return getString("\"", Token::STRING_CONSTANT);
  }
  else if(lastChar == '\'')
  {
    knowCore::LexerTextStream::Element c1 = d->stream.getNextChar();
    if(c1 == '\'')
    {
      knowCore::LexerTextStream::Element c2 = d->stream.getNextChar();
      if(c2 == '\'')
      {
        return getString("'''", Token::STRING_CONSTANT);
      }
      else
      { // Empty string
        d->stream.unget(c2);
        Token tok;
        return Token(Token::STRING_CONSTANT, QString(), firstChar.line, firstChar.column);
      }
    }
    else
    {
      d->stream.unget(c1);
    }
    return getString("'", Token::STRING_CONSTANT);
  }
  else
  {
    CHAR_IS_TOKEN(';', SEMI);
    CHAR_IS_TOKEN(',', COMA);
    CHAR_IS_TOKEN('.', DOT);
    CHAR_IS_TOKEN(':', COLON);
    CHAR_IS_TOKEN('{', STARTBRACE);
    CHAR_IS_TOKEN('}', ENDBRACE);
    CHAR_IS_TOKEN('(', STARTBRACKET);
    CHAR_IS_TOKEN(')', ENDBRACKET);
    CHAR_IS_TOKEN('[', STARTBOXBRACKET);
    CHAR_IS_TOKEN(']', ENDBOXBRACKET);
    CHAR_IS_TOKEN('=', EQUAL);
    CHAR_IS_TOKEN_OR_TOKEN('!', '=', EXCLAMATION, DIFFERENT);
    CHAR_IS_TOKEN_OR_TOKEN('&', '&', AND, AND);
    CHAR_IS_TOKEN_OR_TOKEN('|', '|', OR, OR);
    CHAR_IS_TOKEN_OR_TOKEN('<', '=', INFERIOR, INFERIOREQUAL);
    CHAR_IS_TOKEN_OR_TOKEN('>', '=', SUPPERIOR, SUPPERIOREQUAL);
    CHAR_IS_TOKEN('+', PLUS);
    CHAR_IS_TOKEN('-', MINUS);
    CHAR_IS_TOKEN('*', MULTIPLY);
    CHAR_IS_TOKEN('/', DIVIDE);
    CHAR_IS_TOKEN_OR_TOKEN('^', '^', UNKNOWN, CIRCUMFLEXCIRCUMFLEX);
  }
  identifierStr = lastChar.content;
  clog_warning("Unknown token: {} '{}' at {}, {} ", lastChar.content, identifierStr, firstChar.line,
               firstChar.column);
  clog_assert(not lastChar.isSpace());
  return Token(Token::UNKNOWN, firstChar.line, firstChar.column);
}
