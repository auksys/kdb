KDB_SPARQL_KEYWORD(ADD)
KDB_SPARQL_KEYWORD(ALL)
KDB_SPARQL_KEYWORD(AS)
KDB_SPARQL_KEYWORD(ASC)
KDB_SPARQL_KEYWORD(ASK)
KDB_SPARQL_KEYWORD(BASE)
KDB_SPARQL_KEYWORD(BY)
KDB_SPARQL_KEYWORD(BIND)
KDB_SPARQL_KEYWORD(CLEAR)
KDB_SPARQL_KEYWORD(CONSTRUCT)
KDB_SPARQL_KEYWORD(COPY)
KDB_SPARQL_KEYWORD(CREATE)
KDB_SPARQL_KEYWORD(DATA)
KDB_SPARQL_KEYWORD(DEFAULT)
KDB_SPARQL_KEYWORD(DELETE)
KDB_SPARQL_KEYWORD(DESC)
KDB_SPARQL_KEYWORD(DESCRIBE)
KDB_SPARQL_KEYWORD(DROP)
KDB_SPARQL_KEYWORD(DISTINCT)
KDB_SPARQL_KEYWORD(EXPLAIN)
KDB_SPARQL_KEYWORD(EXECUTE)
KDB_SPARQL_KEYWORD(FILTER)
KDB_SPARQL_KEYWORD(FROM)
KDB_SPARQL_KEYWORD(GRAPH)
KDB_SPARQL_KEYWORD(GROUP)
KDB_SPARQL_KEYWORD(IN)
KDB_SPARQL_KEYWORD(INTO)
KDB_SPARQL_KEYWORD(INSERT)
KDB_SPARQL_KEYWORD(LIMIT)
KDB_SPARQL_KEYWORD(LOAD)
KDB_SPARQL_KEYWORD(NAMED)
KDB_SPARQL_KEYWORD(NOT)
KDB_SPARQL_KEYWORD(OFFSET)
KDB_SPARQL_KEYWORD(OPTIONAL)
KDB_SPARQL_KEYWORD(ORDER)
KDB_SPARQL_KEYWORD(PREFIX)
KDB_SPARQL_KEYWORD(REDUCED)
KDB_SPARQL_KEYWORD(SELECT)
KDB_SPARQL_KEYWORD(SERVICE)
KDB_SPARQL_KEYWORD(SILENT)
KDB_SPARQL_KEYWORD(TO)
KDB_SPARQL_KEYWORD(UNION)
KDB_SPARQL_KEYWORD(USING)
KDB_SPARQL_KEYWORD(WHERE)
KDB_SPARQL_KEYWORD(WITH)
