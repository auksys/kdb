/*
 *  Copyright (c) 2015,2016 Cyrille Berger <cberger@cberger.net>
 *
 */

#include "Parser_p.h"

#include <QHash>
#include <QVariant>

#include <clog_qt>
#include <knowCore/BigNumber.h>
#include <knowCore/Cast.h>
#include <knowCore/Messages.h>
#include <knowCore/TypeDefinitions.h>

#include <knowCore/Uris/askcore_db.h>
#include <knowCore/Uris/rdf.h>
#include <knowCore/Uris/xsd.h>

#include <knowRDF/Object.h>
#include <knowRDF/Subject.h>

#include "Algebra/Nodes.h"
#include "Algebra/Visitors/VariablesLister.h"

#include "Lexer_p.h"
#include "Token_p.h"

using namespace kDB::SPARQL;

namespace kDB::SPARQL
{

  struct DisableLexingCurie
  {
    DisableLexingCurie(Lexer* l) : lexer(l), enabled(l->isCurieLexingEnabled())
    {
      lexer->setCurieLexingEnabled(false);
    }
    ~DisableLexingCurie() { lexer->setCurieLexingEnabled(enabled); }
    Lexer* lexer;
    bool enabled;
  };

  struct DisableLexingUri
  {
    DisableLexingUri(Lexer* l) : lexer(l), enabled(l->isUriLexingEnabled())
    {
      lexer->setUriLexingEnabled(false);
    }
    ~DisableLexingUri() { lexer->setUriLexingEnabled(enabled); }
    Lexer* lexer;
    bool enabled;
  };
  struct EnableLexingUri
  {
    EnableLexingUri(Lexer* l) : lexer(l), enabled(l->isUriLexingEnabled())
    {
      lexer->setUriLexingEnabled(true);
    }
    ~EnableLexingUri() { lexer->setUriLexingEnabled(enabled); }
    Lexer* lexer;
    bool enabled;
  };

  struct EnablePLLexing
  {
    EnablePLLexing(Lexer* l, bool _enabled) : lexer(l), enabled(l->isPLLexingEnabled())
    {
      lexer->setPLLexingEnabled(_enabled);
    }
    ~EnablePLLexing() { lexer->setPLLexingEnabled(enabled); }
    Lexer* lexer;
    bool enabled;
  };
} // namespace kDB::SPARQL

struct Parser::Private
{
  Lexer* lexer;
  knowCore::Messages messages;
  knowCore::ValueHash bindings;

  knowCore::UriManager urlManager;

  QHash<QString, knowRDF::BlankNode> blankNodes;

  Token currentToken;
  const Token& getNextToken();
  bool isOfType(const Token& _token, Token::Type _type);
  void reportUnexpected(const Token& _token, Token::Type _expectedType);
  void reportUnexpected(const Token& _token);
  void reportError(const Token& _token, const QString& _errorMsg);
  knowCore::Value getBinding(const Token& _token);
  Algebra::NodeCSP parseQuery();
  Algebra::AskQueryCSP parseAskQuery();
  Algebra::ConstructQueryCSP parseConstructQuery();
  Algebra::DescribeQueryCSP parseDescribeQuery();
  Algebra::SelectQueryCSP parseSelectQuery();
  Algebra::ExplainQueryCSP parseExplainQuery();
  Algebra::NodeCSP parseScriptQuery();
  std::tuple<Algebra::GroupClausesCSP, Algebra::HavingClausesCSP, Algebra::OrderClausesCSP,
             Algebra::LimitOffsetClauseCSP>
    parseSolutionModifiers(bool _hasGroup = true, bool _hasHaving = true, bool _hasOrder = true,
                           bool _hasLimitOffset = true);
  QList<Algebra::DatasetCSP> parseDataset(Token::Type _from_using);
  Algebra::NodeCSP parseDeleteInsertQuery();
  Algebra::LoadCSP parseLoadQuery();
  Algebra::NodeCSP parseDropClearCreateQuery();
  Algebra::ExecuteCSP parseExecute();
  Algebra::QuadsDataCSP parseQuadsData(bool _allow_blank_node);
  QList<Algebra::NodeCSP> parseStatements();
  Algebra::NodeCSP parseIfUnless();
  //   QList<knowRDF::Subject> subjectsFromNode(const Token& _errorToken, Algebra::NodeCSP _subject,
  //   bool _acceptVariable); QList<knowRDF::Object> objectsFromNode(const Token& _errorToken,
  //   Algebra::NodeCSP _object, bool _acceptVariable);
  QList<Algebra::VariableCSP> parseVariables();
  Algebra::NodeCSP parseExpression();
  Algebra::NodeCSP parseConditionalAndExpression();
  Algebra::NodeCSP parseRelationalExpression();
  Algebra::NodeCSP parseAdditiveExpression();
  Algebra::NodeCSP parseMultiplicativeExpression();
  Algebra::NodeCSP parseUnaryExpression();
  Algebra::NodeCSP parsePrimaryExpression();
  Algebra::NodeCSP parseStringConstant();
  QList<Algebra::NodeCSP> parseExpressionList();
  int parseInteger();
  Algebra::GroupGraphPatternCSP parseGroupGraphPattern(const Algebra::NodeCSP& _graphname);
  Algebra::ServiceCSP parseService();
  Algebra::BindCSP parseBind();
  QList<Algebra::TripleCSP> parseTriplesBlock(bool _allow_blank_node);
  QList<Algebra::TripleCSP> parseObjectBlankNode(Algebra::BlankNodeCSP blankNode,
                                                 bool _allow_blank_node);
  QList<Algebra::TripleCSP> parseCollection(Algebra::NodeCSP _subject, Algebra::NodeCSP _verb,
                                            bool _allow_blank_node);
  QList<Algebra::TripleCSP> parseCollection(const knowRDF::BlankNode& _head_node,
                                            bool _allow_blank_node);

  QList<Algebra::TripleCSP> parseVerbObjectTriples(Algebra::NodeCSP _subject,
                                                   bool _allow_blank_node);
  QList<Algebra::TripleCSP> parseObjectTriples(Algebra::NodeCSP _subject, Algebra::NodeCSP _verb,
                                               bool _allow_blank_node);

  bool isSubjectStart(const Token& _token);
  bool isVerbStart(const Token& _token);
  bool isObjectStart(const Token& _token);
  bool isExpressionStart(const Token& _token);

  Algebra::NodeCSP parseSubject(bool _allow_blank_node);
  Algebra::NodeCSP parseVerb();
  Algebra::NodeCSP parseObject(bool _allow_blank_node);
  Algebra::BlankNodeCSP parseBlank(bool _allow_blank_node);

  bool isUri(const Token& _token);
  knowCore::Uri parseUri();
  Algebra::Alternative<Algebra::VariableReference, Algebra::Term> parseVariableOrTerm();
};

namespace
{
  template<typename _T1_, typename _T2_>
  void append_non_null(QList<knowCore::ConstExplicitlySharedDataPointer<_T1_>>& _list,
                       knowCore::ConstExplicitlySharedDataPointer<_T2_> _value)
  {
    if(_value)
    {
      _list.append(_value);
    }
  }
} // namespace

void Parser::Private::reportError(const Token& _token, const QString& _errorMsg)
{
  messages.reportError(_errorMsg, _token.line);
}

void Parser::Private::reportUnexpected(const Token& _token)
{
  reportError(_token, clog_qt::qformat("Unexpected '{}'.", _token.toString()));
}

void Parser::Private::reportUnexpected(const Token& _token, Token::Type _expectedType)
{
  reportError(_token, clog_qt::qformat("Expected '{}' before '{}'.",
                                       Token::typeToString(_expectedType), _token.toString()));
}

knowCore::Value Parser::Private::getBinding(const Token& _token)
{
  if(bindings.contains(_token.string))
  {
    return bindings.value(_token.string);
  }
  else
  {
    reportError(_token, clog_qt::qformat("Unknown binding '{}'", _token.string));
    return knowCore::Value();
  }
}

bool Parser::Private::isOfType(const Token& _token, Token::Type _type)
{
  if(_token.type == _type)
    return true;
  reportUnexpected(_token, _type);
  return false;
}

Algebra::AskQueryCSP kDB::SPARQL::Parser::Private::parseAskQuery()
{
  clog_assert(currentToken.type == Token::ASK);
  getNextToken();
  QList<Algebra::DatasetCSP> datasets;
  while(currentToken.type == Token::FROM)
  {
    getNextToken();
    Algebra::DatasetType type = Algebra::DatasetType::Anonymous;
    if(currentToken.type == Token::NAMED)
    {
      type = Algebra::DatasetType::Named;
      getNextToken();
    }
    knowCore::Uri r = parseUri();
    datasets.append(new Algebra::Dataset(type, r));
  }
  if(currentToken.type == Token::WHERE)
  {
    getNextToken();
  }
  Algebra::NodeCSP matchExpression = parseGroupGraphPattern(nullptr);

  return new Algebra::AskQuery(datasets, matchExpression);
}

Algebra::ConstructQueryCSP kDB::SPARQL::Parser::Private::parseConstructQuery()
{
  clog_assert(currentToken.type == Token::CONSTRUCT);
  getNextToken();

  if(currentToken.type == Token::END_OF_FILE or currentToken.type == Token::SEMI)
  {
    reportUnexpected(currentToken);
    return nullptr;
  }

  QList<Algebra::TripleCSP> triplesTemplate;

  if(currentToken.type == Token::STARTBRACE)
  {
    getNextToken();
    if(currentToken.type != Token::ENDBRACE)
    {
      triplesTemplate = parseTriplesBlock(true);
    }
    if(isOfType(currentToken, Token::ENDBRACE))
    {
      getNextToken();
    }
  }

  QList<Algebra::DatasetCSP> datasets = parseDataset(Token::FROM);

  Algebra::NodeCSP matchExpression = nullptr;
  if(currentToken.type == Token::WHERE)
  {
    getNextToken();
  }
  if(currentToken.type == Token::STARTBRACE)
  {
    matchExpression = parseGroupGraphPattern(nullptr);
  }

  const auto& [groupClauses, havingClauses, orderClauses, limitOffsetClause]
    = parseSolutionModifiers();

  return new Algebra::ConstructQuery(triplesTemplate, datasets, matchExpression, groupClauses,
                                     havingClauses, orderClauses, limitOffsetClause);
}

Algebra::DescribeQueryCSP kDB::SPARQL::Parser::Private::parseDescribeQuery()
{
  clog_assert(currentToken.type == Token::DESCRIBE);
  getNextToken();

  QList<Algebra::DescribeTermCSP> terms;

  while(currentToken.type == Token::VARIABLE or isUri(currentToken))
  {
    if(currentToken.type == Token::VARIABLE)
    {
      terms.append(new Algebra::DescribeTerm(new Algebra::Variable(currentToken.string, nullptr),
                                             knowCore::Uri()));
      getNextToken();
    }
    else
    {
      terms.append(new Algebra::DescribeTerm(nullptr, parseUri()));
    }
  }

  QList<Algebra::DatasetCSP> datasets = parseDataset(Token::FROM);

  Algebra::NodeCSP matchExpression = nullptr;
  switch(currentToken.type)
  {
  case Token::WHERE:
  {
    getNextToken();
    matchExpression = parseGroupGraphPattern(nullptr);
    break;
  }
  case Token::STARTBRACE:
  {
    matchExpression = parseGroupGraphPattern(nullptr);
    break;
  }
  default:
    break;
  }

  const auto& [groupClauses, havingClauses, orderClauses, limitOffsetClause]
    = parseSolutionModifiers();

  return new Algebra::DescribeQuery(terms, datasets, matchExpression, groupClauses, havingClauses,
                                    orderClauses, limitOffsetClause);
}

Algebra::SelectQueryCSP Parser::Private::parseSelectQuery()
{
  Token errorToken = currentToken;
  clog_assert(currentToken.type == Token::SELECT);
  getNextToken();
  Algebra::SelectModifier mod = Algebra::SelectModifier::None;
  switch(currentToken.type)
  {
  case Token::REDUCED:
    mod = Algebra::SelectModifier::Reduced;
    getNextToken();
    break;
  case Token::DISTINCT:
    mod = Algebra::SelectModifier::Distinct;
    getNextToken();
    break;
  default:
    break;
  }
  QList<Algebra::VariableCSP> variables = parseVariables();
  bool all_variables = false;
  if(currentToken.type == Token::MULTIPLY)
  {
    if(not variables.isEmpty())
    {
      reportUnexpected(currentToken);
    }
    all_variables = true;
    getNextToken();
  }
  QList<Algebra::DatasetCSP> datasets = parseDataset(Token::FROM);
  if(currentToken.type == Token::WHERE)
  {
    getNextToken();
  }
  Algebra::NodeCSP matchExpression = parseGroupGraphPattern(nullptr);

  if(matchExpression)
  {
    QStringList matchExpression_names = Algebra::Visitors::VariablesLister::list(matchExpression);
    if(all_variables)
    {
      for(QString varname : matchExpression_names)
      {
        variables.append(new Algebra::Variable(varname, nullptr));
      }
    }
    else
    {
      QStringList variable_names;
      for(Algebra::VariableCSP name : variables)
      {
        if(name->expression())
        {
          variable_names.append(Algebra::Visitors::VariablesLister::list(name->expression()));
        }
        else
        {
          variable_names.append(name->name());
        }
      }
      for(const QString& name : variable_names)
      {
        if(not matchExpression_names.contains(name))
        {
          reportError(
            errorToken,
            clog_qt::qformat("Variable '?{}' is used in the result set but not assigned", name));
        }
      }
    }
  }
  const auto& [groupClauses, havingClauses, orderClauses, limitOffsetClause]
    = parseSolutionModifiers();

  return new Algebra::SelectQuery(mod, variables, datasets, matchExpression, groupClauses,
                                  havingClauses, orderClauses, limitOffsetClause);
}

Algebra::ExplainQueryCSP Parser::Private::parseExplainQuery()
{
  clog_assert(currentToken.type == Token::EXPLAIN);
  getNextToken();
  QString options;
  if(currentToken.type == Token::STRING_CONSTANT)
  {
    options = currentToken.string;
    getNextToken();
  }
  Algebra::NodeCSP query = nullptr;
  switch(currentToken.type)
  {
  case Token::SELECT:
    query = parseSelectQuery();
    break;
  default:
    reportUnexpected(currentToken);
  }
  return new Algebra::ExplainQuery(options, query);
}

Algebra::NodeCSP Parser::Private::parseScriptQuery()
{
  clog_assert(currentToken.type == Token::BEGIN);
  EnablePLLexing epll(lexer, true);

  getNextToken();

  if(not isUri(currentToken))
  {
    reportUnexpected(currentToken);
    return nullptr;
  }
  Token uri_token = currentToken;
  knowCore::Uri script_type = parseUri();

  if(script_type != knowCore::Uris::askcore_db_query_language::pl_sparql)
  {
    reportError(uri_token, clog_qt::qformat("Unsupported script language '{}'", script_type));
  }

  return new Algebra::PLQuery(new Algebra::List(parseStatements()));
}

Algebra::NodeCSP kDB::SPARQL::Parser::Private::parseQuery()
{
  switch(currentToken.type)
  {
  case Token::ASK:
    return parseAskQuery();
  case Token::CONSTRUCT:
    return parseConstructQuery();
  case Token::DESCRIBE:
    return parseDescribeQuery();
  case Token::SELECT:
    return parseSelectQuery();
  case Token::INSERT:
  case Token::WITH:
  case Token::DELETE:
    return parseDeleteInsertQuery();
  case Token::LOAD:
    return parseLoadQuery();
  case Token::DROP:
    return parseDropClearCreateQuery();
  case Token::CREATE:
    return parseDropClearCreateQuery();
  case Token::CLEAR:
    return parseDropClearCreateQuery();
  case Token::EXECUTE:
    return parseExecute();
  case Token::EXPLAIN:
    return parseExplainQuery();
  case Token::BEGIN:
    return parseScriptQuery();
  default:
    reportError(currentToken,
                clog_qt::qformat("Unsupported query {}", Token::typeToString(currentToken.type)));
    return nullptr;
  }
}

std::tuple<Algebra::GroupClausesCSP, Algebra::HavingClausesCSP, Algebra::OrderClausesCSP,
           Algebra::LimitOffsetClauseCSP>
  Parser::Private::parseSolutionModifiers(bool _hasGroup, bool /*_hasHaving*/, bool _hasOrder,
                                          bool _hasLimitOffset)
{
  Algebra::GroupClausesCSP groupClauses = nullptr;
  if(currentToken.type == Token::GROUP and _hasGroup)
  {
    getNextToken();
    isOfType(currentToken, Token::BY);
    getNextToken();
    QList<Algebra::NodeCSP> variables;
    while(currentToken.type == Token::VARIABLE or currentToken.type == Token::STARTBRACKET
          or isUri(currentToken))
    {
      switch(currentToken.type)
      {
      case Token::VARIABLE:
        variables.append(new Algebra::VariableReference(currentToken.string));
        break;
      case Token::STARTBRACKET:
        getNextToken();
        variables.append(parseExpression());
        isOfType(currentToken, Token::ENDBRACKET);
        getNextToken();
        break;
      default:
        clog_assert(isUri(currentToken));
        variables.append(parseExpression());
        break;
      }
      getNextToken();
    }
    groupClauses = new Algebra::GroupClauses(variables);
  }
  Algebra::HavingClausesCSP havingClauses = nullptr;
  //   if(currentToken.type == Token::HAVING and _hasHaving)
  //   {
  //   }
  Algebra::OrderClausesCSP orderClauses = nullptr;
  if(currentToken.type == Token::ORDER and _hasOrder)
  {
    getNextToken();
    if(isOfType(currentToken, Token::BY))
    {
      getNextToken();
      QList<Algebra::OrderClauseCSP> clauses;
      while(currentToken.type != Token::END_OF_FILE)
      {
        Algebra::OrderDirection od = Algebra::OrderDirection::Asc;
        switch(currentToken.type)
        {
        case Token::DESC:
          od = Algebra::OrderDirection::Desc;
          Q_FALLTHROUGH();
        case Token::ASC:
          getNextToken();
        default:
          break;
        }
        clauses.append(new Algebra::OrderClause(od, parseExpression()));
        if(not isExpressionStart(currentToken) and currentToken.type != Token::DESC
           and currentToken.type != Token::ASC)
        {
          break;
        }
      }
      orderClauses = new Algebra::OrderClauses(clauses);
    }
  }
  Algebra::LimitOffsetClauseCSP limitOffsetClause = nullptr;
  int offset = -1;
  int limit = -1;
  while((currentToken.type == Token::OFFSET or currentToken.type == Token::LIMIT)
        and _hasLimitOffset)
  {
    switch(currentToken.type)
    {
    case Token::OFFSET:
      getNextToken();
      offset = parseInteger();
      break;
    case Token::LIMIT:
      getNextToken();
      limit = parseInteger();
      break;
    default:
      qFatal("can't happen");
    }
  }
  if(offset != -1 or limit != -1)
  {
    limitOffsetClause = new Algebra::LimitOffsetClause(limit, offset);
  }
  return std::make_tuple(groupClauses, havingClauses, orderClauses, limitOffsetClause);
}

QList<Algebra::DatasetCSP> Parser::Private::parseDataset(Token::Type _from_using)
{
  QList<Algebra::DatasetCSP> datasets;
  while(currentToken.type == _from_using)
  {
    getNextToken();
    Algebra::DatasetType type = Algebra::DatasetType::Anonymous;
    if(currentToken.type == Token::NAMED)
    {
      type = Algebra::DatasetType::Named;
      getNextToken();
    }
    knowCore::Uri r = parseUri();
    datasets.append(new Algebra::Dataset(type, r));
  }
  return datasets;
}

Algebra::NodeCSP Parser::Private::parseDeleteInsertQuery()
{
  Token errorToken = currentToken;
  clog_assert(currentToken.type == Token::INSERT or currentToken.type == Token::DELETE
              or currentToken.type == Token::WITH);
  knowCore::Uri withUri;
  QList<Algebra::DatasetCSP> datasets;
  if(currentToken.type == Token::WITH)
  {
    getNextToken(); // Skip with
    withUri = parseUri();
  }

  Algebra::QuadsDataCSP deleteQuadsData, insertQuadsData;
  Algebra::NodeCSP whereNode;

  switch(currentToken.type)
  {
  case Token::DELETE:
  {
    getNextToken();
    switch(currentToken.type)
    {
    case Token::DATA:
      if(withUri != "")
        reportUnexpected(currentToken);
      getNextToken();
      return new Algebra::DeleteData(parseQuadsData(false));
    case Token::STARTBRACE:
      deleteQuadsData = parseQuadsData(false);
      break;
    case Token::USING:
      datasets = parseDataset(Token::USING);
      isOfType(currentToken, Token::WHERE);
      // Should go to the next case!
      Q_FALLTHROUGH();
    case Token::WHERE:
      getNextToken();
      deleteQuadsData = parseQuadsData(false);
      whereNode = deleteQuadsData;
      break;
    default:
      reportUnexpected(currentToken);
      return nullptr;
    }
    break;
  }
  case Token::INSERT:
  {
    getNextToken();
    switch(currentToken.type)
    {
    case Token::DATA:
      if(withUri != "")
        reportUnexpected(currentToken);
      getNextToken();
      return new Algebra::InsertData(parseQuadsData(true));
    case Token::STARTBRACE:
      insertQuadsData = parseQuadsData(true);
      break;
    case Token::USING:
      datasets = parseDataset(Token::USING);
      isOfType(currentToken, Token::WHERE);
      // Should go to the next case!
      Q_FALLTHROUGH();
    case Token::WHERE:
      getNextToken();
      insertQuadsData = parseQuadsData(true);
      whereNode = insertQuadsData;
      break;
    default:
      reportUnexpected(currentToken);
      return nullptr;
    }
    break;
  }
  default:
    reportUnexpected(currentToken);
    return nullptr;
  }
  if(not insertQuadsData and currentToken.type == Token::INSERT)
  {
    clog_assert((whereNode == nullptr));
    getNextToken();
    insertQuadsData = parseQuadsData(true);
  }

  if(not whereNode)
  {
    if(currentToken.type == Token::USING)
    {
      datasets = parseDataset(Token::USING);
    }
    if(isOfType(currentToken, Token::WHERE))
    {
      getNextToken();
      whereNode = parseGroupGraphPattern(nullptr);
    }
    else
    {
      return nullptr;
    }
  }
  if(not whereNode)
  {
    reportError(errorToken, "Missing WHERE statement or empty statement.");
  }
  clog_assert((deleteQuadsData or insertQuadsData));
  return new Algebra::DeleteInsert(withUri, deleteQuadsData, insertQuadsData, datasets, whereNode);
}

Algebra::LoadCSP kDB::SPARQL::Parser::Private::parseLoadQuery()
{
  clog_assert(currentToken.type == Token::LOAD);
  getNextToken();
  bool silent = false;
  if(currentToken.type == Token::SILENT)
  {
    silent = true;
    getNextToken();
  }
  knowCore::Uri uri = parseUri();
  knowCore::Uri graph;

  if(currentToken.type == Token::INTO)
  {
    getNextToken();
    if(not isOfType(currentToken, Token::GRAPH))
    {
      return nullptr;
    }
    getNextToken();
    graph = parseUri();
  }
  return new Algebra::Load(uri, graph, silent);
}

Algebra::NodeCSP Parser::Private::parseDropClearCreateQuery()
{
  clog_assert(currentToken.type == Token::DROP or currentToken.type == Token::CLEAR
              or currentToken.type == Token::CREATE);
  Token::Type nodeType = currentToken.type;
  getNextToken();
  bool silent = false;
  if(currentToken.type == Token::SILENT)
  {
    silent = true;
    getNextToken();
  }
  Algebra::GraphType graphType;
  knowCore::Uri graph;
  Token typeToken = currentToken;
  getNextToken();
  if(nodeType == Token::DROP or nodeType == Token::CLEAR)
  {
    switch(typeToken.type)
    {
    case Token::DEFAULT:
      graphType = Algebra::GraphType::Default;
      break;
    case Token::NAMED:
      graphType = Algebra::GraphType::Named;
      break;
    case Token::ALL:
      graphType = Algebra::GraphType::All;
      break;
    case Token::GRAPH:
      graph = parseUri();
      graphType = Algebra::GraphType::Graph;
      break;
    default:
      reportUnexpected(typeToken);
      return nullptr;
    }
  }
  else
  {
    graph = parseUri();
  }
  switch(nodeType)
  {
  case Token::DROP:
    return new Algebra::Drop(silent, graphType, graph);
  case Token::CLEAR:
    return new Algebra::Clear(silent, graphType, graph);
  case Token::CREATE:
    return new Algebra::Create(silent, graph);
  default:
    clog_fatal("Cannot happen");
  }
}

Algebra::ExecuteCSP kDB::SPARQL::Parser::Private::parseExecute()
{
  clog_assert(currentToken.type == Token::EXECUTE);
  getNextToken();
  knowCore::Uri uri = parseUri();
  return new Algebra::Execute(uri);
}

Algebra::QuadsDataCSP Parser::Private::parseQuadsData(bool _allow_blank_node)
{
  QList<kDB::SPARQL::Algebra::QuadsCSP> quads;
  if(isOfType(currentToken, Token::STARTBRACE))
  {
    Token errorToken = currentToken;
    getNextToken();
    if(currentToken.type == Token::ENDBRACE)
    {
      getNextToken();
      return new Algebra::QuadsData(quads);
    }
    if(isSubjectStart(currentToken))
    {
      QList<Algebra::TripleCSP> triples = parseTriplesBlock(_allow_blank_node);
      quads.append(new kDB::SPARQL::Algebra::Quads(triples, nullptr));
      if(isOfType(currentToken, Token::ENDBRACE))
      {
        getNextToken();
      }
    }
    else if(currentToken.type == Token::GRAPH)
    {
      while(currentToken.type == Token::GRAPH)
      {
        getNextToken();
        knowCore::Uri uri = parseUri();
        isOfType(currentToken, Token::STARTBRACE);
        getNextToken();
        if(isSubjectStart(currentToken))
        {
          QList<Algebra::TripleCSP> triples = parseTriplesBlock(_allow_blank_node);
          quads.append(new kDB::SPARQL::Algebra::Quads(triples, new Algebra::GraphReference(uri)));
          if(isOfType(currentToken, Token::ENDBRACE))
          {
            getNextToken();
          }
          else
          {
            return new Algebra::QuadsData(quads);
          }
        }
      }
      if(isOfType(currentToken, Token::ENDBRACE))
      {
        getNextToken();
      }
    }
    else
    {
      reportUnexpected(currentToken);
    }
  }
  return new Algebra::QuadsData(quads);
}

QList<Algebra::NodeCSP> Parser::Private::parseStatements()
{
  QList<Algebra::NodeCSP> statements;
  if(currentToken.type == Token::STARTBRACE)
  {
    getNextToken();
    while(currentToken.type != Token::END_OF_FILE and currentToken.type != Token::ENDBRACE)
    {
      switch(currentToken.type)
      {
      case Token::Type::IF:
      case Token::Type::UNLESS:
      {
        statements.append(parseIfUnless());
        break;
      }
      default:
      {
        Algebra::NodeCSP node = parseQuery();
        if(node)
        {
          statements.append(node);
        }
        else
        {
          return QList<Algebra::NodeCSP>();
        }
        break;
      }
      }
    }
    getNextToken(); // eat '}'
  }
  else
  {
    statements.append(parseQuery());
  }
  return statements;
}

Algebra::NodeCSP Parser::Private::parseIfUnless()
{
  Token::Type type = currentToken.type;

  clog_assert(type == Token::IF or type == Token::UNLESS);
  getNextToken();
  if(not isOfType(currentToken, Token::ASK))
  {
    return nullptr;
  }
  Algebra::AskQueryCSP test = parseAskQuery();

  if(not isOfType(currentToken, Token::THEN))
  {
    return nullptr;
  }
  getNextToken(); // Eat 'then'

  QList<Algebra::NodeCSP> ifStatements = parseStatements();
  ;

  QList<Algebra::NodeCSP> elseStatements;

  if(currentToken.type == Token::ELSE)
  {
    getNextToken(); // Eat 'else'
    elseStatements = parseStatements();
  }

  switch(type)
  {
  case Token::IF:
    return new Algebra::If(test, new Algebra::List(ifStatements),
                           new Algebra::List(elseStatements));
  case Token::UNLESS:
    return new Algebra::Unless(test, new Algebra::List(ifStatements),
                               new Algebra::List(elseStatements));
  default:
    qFatal("Internal error!");
    return nullptr;
  }
}

QList<Algebra::VariableCSP> Parser::Private::parseVariables()
{
  switch(currentToken.type)
  {
  case Token::MULTIPLY:
    return QList<Algebra::VariableCSP>();
  case Token::VARIABLE:
  case Token::STARTBRACKET:
  {
    QList<Algebra::VariableCSP> variables;
    while(currentToken.type == Token::VARIABLE or currentToken.type == Token::STARTBRACKET)
    {
      switch(currentToken.type)
      {
      case Token::VARIABLE:
        variables.append(new Algebra::Variable(currentToken.string, nullptr));
        getNextToken();
        break;
      case Token::STARTBRACKET:
      {
        getNextToken();
        Algebra::NodeCSP expr = parseExpression();
        if(isOfType(currentToken, Token::AS))
        {
          getNextToken();
          if(isOfType(currentToken, Token::VARIABLE))
          {
            variables.append(new Algebra::Variable(currentToken.string, expr));
            getNextToken();
          }
        }
        isOfType(currentToken, Token::ENDBRACKET);
        getNextToken();
        break;
      }
      default:
        reportUnexpected(currentToken);
        return variables;
      }
    }
    return variables;
  }
  default:
    reportUnexpected(currentToken);
    return QList<Algebra::VariableCSP>();
  }
}

bool Parser::Private::isExpressionStart(const Token& _token)
{
  switch(_token.type)
  {
  case Token::STARTBRACKET:
  case Token::CURIE_CONSTANT:
  case Token::VARIABLE:
  case Token::NAME:
  case Token::STRING_CONSTANT:
    return true;
  default:
    return false;
  }
}

Algebra::NodeCSP Parser::Private::parseExpression()
{
  EnableLexingUri elu(lexer);
  Algebra::NodeCSP node = parseConditionalAndExpression();
  if(currentToken.type == Token::OR)
  {
    getNextToken();
    return new Algebra::LogicalOr(node, parseExpression());
  }
  else
  {
    return node;
  }
}

Algebra::NodeCSP Parser::Private::parseConditionalAndExpression()
{
  Algebra::NodeCSP node = parseRelationalExpression();
  if(currentToken.type == Token::AND)
  {
    getNextToken();
    return new Algebra::LogicalAnd(node, parseExpression());
  }
  else
  {
    return node;
  }
}

Algebra::NodeCSP Parser::Private::parseRelationalExpression()
{
  Algebra::NodeCSP node = parseAdditiveExpression();
  switch(currentToken.type)
  {
  case Token::EQUAL:
    getNextToken();
    return new Algebra::RelationalEqual(node, parseAdditiveExpression());
  case Token::DIFFERENT:
    getNextToken();
    return new Algebra::RelationalDifferent(node, parseAdditiveExpression());
  case Token::INFERIOR:
    getNextToken();
    return new Algebra::RelationalInferior(node, parseAdditiveExpression());
  case Token::SUPPERIOR:
    getNextToken();
    return new Algebra::RelationalSuperior(node, parseAdditiveExpression());
  case Token::INFERIOREQUAL:
    getNextToken();
    return new Algebra::RelationalInferiorEqual(node, parseAdditiveExpression());
  case Token::SUPPERIOREQUAL:
    getNextToken();
    return new Algebra::RelationalSuperiorEqual(node, parseAdditiveExpression());
  case Token::IN:
    getNextToken();
    return new Algebra::RelationalIn(node, parseExpressionList());
  case Token::NOT:
    getNextToken();
    if(isOfType(currentToken, Token::IN))
    {
      getNextToken();
    }
    return new Algebra::RelationalNotIn(node, parseExpressionList());
  default:
    return node;
  }
}

Algebra::NodeCSP Parser::Private::parseAdditiveExpression()
{
  Algebra::NodeCSP node = parseMultiplicativeExpression();
  switch(currentToken.type)
  {
  case Token::PLUS:
    getNextToken();
    return new Algebra::Addition(node, parseMultiplicativeExpression());
  case Token::MINUS:
    getNextToken();
    return new Algebra::Substraction(node, parseMultiplicativeExpression());
  default:
    return node;
  }
}

Algebra::NodeCSP Parser::Private::parseMultiplicativeExpression()
{
  Algebra::NodeCSP node = parseUnaryExpression();
  switch(currentToken.type)
  {
  case Token::MULTIPLY:
    getNextToken();
    return new Algebra::Multiplication(node, parseUnaryExpression());
  case Token::DIVIDE:
    getNextToken();
    return new Algebra::Division(node, parseUnaryExpression());
  default:
    return node;
  }
}

Algebra::NodeCSP Parser::Private::parseUnaryExpression()
{
  switch(currentToken.type)
  {
  case Token::EXCLAMATION:
    getNextToken();
    return new Algebra::LogicalNegation(parsePrimaryExpression());
  case Token::MINUS:
    getNextToken();
    return new Algebra::Negation(parsePrimaryExpression());
  case Token::PLUS:
    getNextToken();
    Q_FALLTHROUGH();
  default:
    return parsePrimaryExpression();
  }
}

int Parser::Private::parseInteger()
{
  if(isOfType(currentToken, Token::INTEGER_CONSTANT))
  {
    int a = currentToken.string.toInt();
    getNextToken();
    return a;
  }
  return -1;
}

Algebra::NodeCSP Parser::Private::parseStringConstant()
{
  QString value = currentToken.string;

  QString lang;
  knowCore::Uri datatype_uri = knowCore::Uris::xsd::string;

  getNextToken();
  while(currentToken.type != Token::END_OF_FILE)
  {
    if(currentToken.type == Token::LANG_TAG)
    {
      lang = currentToken.string;
      getNextToken();
    }
    else if(currentToken.type == Token::CIRCUMFLEXCIRCUMFLEX)
    {
      getNextToken();
      datatype_uri = parseUri();
    }
    else
    {
      break;
    }
  }
  const auto [success, literal, message]
    = knowRDF::Literal::fromRdfLiteral(datatype_uri, value, lang);
  if(success)
  {
    return new Algebra::Value(literal.value());
  }
  else
  {
    reportError(currentToken, clog_qt::qformat("Failed to parse literal: '{}'", message.value()));
    return nullptr;
  }
}

Algebra::NodeCSP Parser::Private::parsePrimaryExpression()
{
  switch(currentToken.type)
  {
  case Token::STARTBRACKET:
  {
    getNextToken();
    Algebra::NodeCSP c = parseExpression();
    isOfType(currentToken, Token::ENDBRACKET);
    getNextToken();
    return c;
  }
  case Token::URI_CONSTANT:
  case Token::CURIE_CONSTANT:
  {
    DisableLexingUri dlu(lexer);
    knowCore::Uri uri = parseUri();
    if(currentToken.type == Token::STARTBRACKET)
    {
      return new Algebra::FunctionCall(uri, parseExpressionList());
    }
    else
    {
      return new Algebra::Term(uri);
    }
  }
  case Token::VARIABLE:
  {
    QString varname = currentToken.string;
    DisableLexingUri dlu(lexer);
    getNextToken();
    return new Algebra::VariableReference(varname);
  }
  case Token::NAME:
  {
    knowCore::Uri funcname
      = knowCore::Uris::askcore_sparql_functions::base.resolved(currentToken.string);
    DisableLexingUri dlu(lexer);
    getNextToken();
    return new Algebra::FunctionCall(funcname, parseExpressionList());
  }
  case Token::STRING_CONSTANT:
  {
    return parseStringConstant();
  }
  case Token::INTEGER_CONSTANT:
  case Token::FLOAT_CONSTANT:
  {
    QString value = currentToken.string;
    DisableLexingUri dlu(lexer);
    getNextToken();
    return new Algebra::Value(
      knowRDF::Literal::fromValue(knowCore::Uris::xsd::integer,
                                  knowCore::BigNumber::fromString(value).expect_success())
        .expect_success());
  }
  case Token::TRUE:
  {
    DisableLexingUri dlu(lexer);
    getNextToken();
    return new Algebra::Value(
      knowRDF::Literal::fromValue(knowCore::Uris::xsd::boolean, true).expect_success());
  }
  case Token::FALSE:
  {
    DisableLexingUri dlu(lexer);
    getNextToken();
    return new Algebra::Value(
      knowRDF::Literal::fromValue(knowCore::Uris::xsd::boolean, false).expect_success());
  }
  case Token::BINDING:
  {
    Token bindingToken = currentToken;
    knowCore::Value binding = getBinding(currentToken);
    getNextToken();
    return new Algebra::Value(knowRDF::Literal::fromValue(binding));
  }
  default:
    reportUnexpected(currentToken);
    getNextToken();
    return nullptr;
  }
}

QList<Algebra::NodeCSP> Parser::Private::parseExpressionList()
{
  EnableLexingUri elu(lexer);
  QList<Algebra::NodeCSP> arguments;
  if(isOfType(currentToken, Token::STARTBRACKET))
  {
    getNextToken();
    if(currentToken.type != Token::ENDBRACKET)
    {
      while(currentToken.type != Token::END_OF_FILE)
      {
        arguments.append(parseExpression());
        if(currentToken.type == Token::ENDBRACKET)
        {
          break;
        }
        else if(isOfType(currentToken, Token::COMA))
        {
          getNextToken();
        }
        else
        {
          return QList<Algebra::NodeCSP>();
        }
      }
    }
    if(isOfType(currentToken, Token::ENDBRACKET))
    {
      DisableLexingUri dlu(lexer);
      getNextToken();
      return arguments;
    }
    else
    {
      return QList<Algebra::NodeCSP>();
    }
  }
  return arguments;
}

Algebra::GroupGraphPatternCSP
  Parser::Private::parseGroupGraphPattern(const Algebra::NodeCSP& _graphname)
{
  if(isOfType(currentToken, Token::STARTBRACE))
  {
    QList<Algebra::NodeCSP> children;
    QList<Algebra::NodeCSP> filters;
    QList<Algebra::ServiceCSP> services;
    getNextToken();
    if(currentToken.type == Token::ENDBRACE)
    {
      getNextToken();
      return nullptr;
    }
    if(isSubjectStart(currentToken))
    {
      children.append(knowCore::listCast<Algebra::NodeCSP>(parseTriplesBlock(true)));
    }
    while(currentToken.type != Token::ENDBRACE)
    {
      Token startToken = currentToken;
      bool parseTriplesAfter = true;
      switch(currentToken.type)
      {
      case Token::SERVICE:
        services.append(parseService());
        break;
      case Token::STARTBRACE:
        children.append(parseGroupGraphPattern(nullptr));
        break;
      case Token::OPTIONAL:
        getNextToken();
        children.append(new Algebra::Optional(parseGroupGraphPattern(nullptr)));
        break;
      case Token::UNION:
      {
        if(children.isEmpty())
        {
          reportUnexpected(currentToken);
          return nullptr;
        }
        getNextToken();
        Algebra::NodeCSP a = children.last();
        children.pop_back();
        children.append(new Algebra::Union(a, parseGroupGraphPattern(nullptr)));
        break;
      }
      case Token::FILTER:
      {
        getNextToken();
        switch(currentToken.type)
        {
        case Token::STARTBRACKET:
        case Token::URI_CONSTANT:
        case Token::CURIE_CONSTANT:
        case Token::NAME:
          filters.append(parseExpression());
          break;
        default:
          reportUnexpected(currentToken);
        }
        break;
      }
      case Token::GRAPH:
      {
        getNextToken();
        Algebra::NodeCSP graphname;
        switch(currentToken.type)
        {
        case Token::VARIABLE:
          graphname = new Algebra::VariableReference(currentToken.string);
          getNextToken();
          break;
        case Token::BINDING:
        case Token::NAME:
        case Token::CURIE_CONSTANT:
        case Token::URI_CONSTANT:
          graphname = new Algebra::GraphReference(parseUri());
          break;
        default:
          reportUnexpected(currentToken);
          return nullptr;
        }
        children.append(parseGroupGraphPattern(graphname));
        break;
      }
      case Token::DOT:
      {
        reportUnexpected(currentToken);
        break;
      }
      case Token::BIND:
      {
        children.append(parseBind());
        break;
      }
      default:
        parseTriplesAfter = false;
        break;
      }
      if(currentToken.type == Token::DOT)
      {
        getNextToken();
        if(isSubjectStart(currentToken))
        {
          children.append(knowCore::listCast<Algebra::NodeCSP>(parseTriplesBlock(true)));
        }
      }
      else if(parseTriplesAfter and isSubjectStart(currentToken))
      {
        children.append(knowCore::listCast<Algebra::NodeCSP>(parseTriplesBlock(true)));
      }
      if(startToken == currentToken)
      {
        reportUnexpected(currentToken);
        return nullptr;
      }
    }
    if(isOfType(currentToken, Token::ENDBRACE))
    {
      getNextToken();
      return new Algebra::GroupGraphPattern(children, filters, services, _graphname);
    }
    else
    {
      return nullptr;
    }
  }
  else
  {
    return nullptr;
  }
}

Algebra::ServiceCSP Parser::Private::parseService()
{
  isOfType(currentToken, Token::SERVICE);
  getNextToken();

  Algebra::Alternative<Algebra::VariableReference, Algebra::Term> service = parseVariableOrTerm();
  QList<Algebra::Alternative<Algebra::VariableReference, Algebra::Term>> froms;

  if(currentToken.type == Token::FROM)
  {
    getNextToken();
    froms.append(parseVariableOrTerm());
    while(currentToken.type == Token::COMA)
    {
      getNextToken();
      froms.append(parseVariableOrTerm());
    }
  }
  Algebra::GroupGraphPatternCSP ggp = parseGroupGraphPattern(nullptr);

  const auto& [groupClauses, havingClauses, orderClauses, limitOffsetClause]
    = parseSolutionModifiers(false, false);
  Q_UNUSED(groupClauses);
  Q_UNUSED(havingClauses);
  return new Algebra::Service(service, froms, ggp, orderClauses, limitOffsetClause);
}

Algebra::BindCSP Parser::Private::parseBind()
{
  if(not isOfType(currentToken, Token::BIND))
    return nullptr;
  getNextToken(); // skip 'BIND'
  if(not isOfType(currentToken, Token::STARTBRACKET))
    return nullptr;
  getNextToken(); // skip '('

  Algebra::NodeCSP expression_node = parseExpression();

  if(not isOfType(currentToken, Token::AS))
    return nullptr;
  getNextToken();

  if(not isOfType(currentToken, Token::VARIABLE))
    return nullptr;
  QString variable_name = currentToken.string;
  getNextToken();

  if(not isOfType(currentToken, Token::ENDBRACKET))
    return nullptr;
  getNextToken(); // skip ')'

  return new Algebra::Bind(variable_name, expression_node);
}

QList<Algebra::TripleCSP> Parser::Private::parseTriplesBlock(bool _allow_blank_node)
{
  QList<Algebra::TripleCSP> triples;

  do
  {
    Algebra::TripleCSP last_triple = nullptr;
    switch(currentToken.type)
    {
    case Token::BINDING:
    case Token::CURIE_CONSTANT:
    case Token::VARIABLE:
    case Token::NAME:
    case Token::URI_CONSTANT:
    case Token::UNDERSCORECOLON:
    {
      Algebra::NodeCSP a = parseSubject(_allow_blank_node);
      triples.append(parseVerbObjectTriples(a, _allow_blank_node));
      break;
    }
    case Token::STARTBOXBRACKET:
    {
      getNextToken();
      if(not _allow_blank_node)
      {
        reportUnexpected(currentToken);
        return triples;
      }
      Algebra::BlankNodeCSP bn = new Algebra::BlankNode(knowRDF::BlankNode());
      if(currentToken.type == Token::ENDBOXBRACKET)
      {
        getNextToken();
        triples.append(parseVerbObjectTriples(bn, _allow_blank_node));
      }
      else
      {
        while(currentToken.type != Token::ENDBOXBRACKET)
        {
          Algebra::NodeCSP verb = parseVerb();
          if(verb)
          {
            QList<Algebra::TripleCSP> nts = parseObjectTriples(bn, verb, true);
            if(nts.size() > 0)
            {
              triples.append(nts);
              last_triple = nts.last();
            }
            else
            {
              return QList<Algebra::TripleCSP>();
            }
          }
          else
          {
            return QList<Algebra::TripleCSP>();
          }
          if(currentToken.type == Token::SEMI)
          {
            getNextToken();
          }
        }
        getNextToken();
        if(isVerbStart(currentToken))
        {
          triples.append(parseVerbObjectTriples(bn, _allow_blank_node));
        }
      }
      break;
    }
    case Token::STARTBRACKET:
    {
      if(not _allow_blank_node)
      {
        reportUnexpected(currentToken);
        return triples;
      }
      knowRDF::BlankNode headNode;
      Token tok = currentToken;
      QList<Algebra::TripleCSP> collection = parseCollection(headNode, _allow_blank_node);
      triples.append(collection);
      if(isVerbStart(currentToken))
      {
        triples.append(parseVerbObjectTriples(new Algebra::BlankNode(headNode), _allow_blank_node));
      }
      else if(collection.isEmpty())
      {
        reportUnexpected(tok);
      }
      break;
    }
    default:
      reportUnexpected(currentToken);
      return QList<Algebra::TripleCSP>();
    }
    if(currentToken.type == Token::DOT)
    {
      getNextToken();
      if(not isSubjectStart(currentToken))
      {
        return triples;
      }
    }
    else
    {
      break;
    }
  } while(true);
  return triples;
}

QList<Algebra::TripleCSP> Parser::Private::parseObjectBlankNode(Algebra::BlankNodeCSP blankNode,
                                                                bool _allow_blank_node)
{
  QList<Algebra::TripleCSP> triples;
  isOfType(currentToken, Token::STARTBOXBRACKET);
  getNextToken();
  while(currentToken.type != Token::ENDBOXBRACKET)
  {
    Algebra::NodeCSP verb = parseVerb();
    if(verb)
    {
      triples.append(parseObjectTriples(blankNode, verb, _allow_blank_node));
    }
    else
    {
      return triples;
    }
    if(currentToken.type == Token::SEMI)
    {
      getNextToken();
    }
  }
  getNextToken();
  return triples;
}

QList<Algebra::TripleCSP> Parser::Private::parseCollection(Algebra::NodeCSP _subject,
                                                           Algebra::NodeCSP _verb,
                                                           bool _allow_blank_node)
{
  knowRDF::BlankNode head_node;
  QList<Algebra::TripleCSP> triples = parseCollection(head_node, _allow_blank_node);
  if(triples.isEmpty())
  {
    triples.append(
      new Algebra::Triple(_subject, _verb, new Algebra::Term(knowCore::Uris::rdf::nil)));
  }
  else
  {
    if(not _allow_blank_node)
    {
      reportUnexpected(currentToken);
      return triples;
    }
    triples.append(new Algebra::Triple(_subject, _verb, new Algebra::BlankNode(head_node)));
  }
  return triples;
}

QList<Algebra::TripleCSP> Parser::Private::parseCollection(const knowRDF::BlankNode& _head_node,
                                                           bool _allow_blank_node)
{
  if(not _allow_blank_node)
  {
    reportUnexpected(currentToken);
    return QList<Algebra::TripleCSP>();
  }
  Algebra::BlankNodeCSP previousASTBlankNode = nullptr;
  knowRDF::BlankNode currentNode = _head_node;
  isOfType(currentToken, Token::STARTBRACKET);
  getNextToken();
  QList<Algebra::TripleCSP> triples;
  while(currentToken.type != Token::ENDBRACKET)
  {
    Algebra::BlankNodeCSP currentASTBlankNode = new Algebra::BlankNode(currentNode);
    switch(currentToken.type)
    {
    case Token::STARTBOXBRACKET:
    {
      Algebra::BlankNodeCSP otherBlankNode = new Algebra::BlankNode(knowRDF::BlankNode());
      triples.append(new Algebra::Triple(
        currentASTBlankNode, new Algebra::Term(knowCore::Uris::rdf::first), otherBlankNode));
      triples.append(parseObjectBlankNode(otherBlankNode, _allow_blank_node));
      break;
    }
    case Token::STARTBRACKET:
    {
      knowRDF::BlankNode otherBlankNode;
      triples.append(parseCollection(
        currentASTBlankNode, new Algebra::Term(knowCore::Uris::rdf::first), _allow_blank_node));
      break;
    }
    default:
    {
      Algebra::NodeCSP node_object = parseObject(_allow_blank_node);
      if(node_object)
      {
        triples.append(new Algebra::Triple(
          currentASTBlankNode, new Algebra::Term(knowCore::Uris::rdf::first), node_object));
      }
      else
      {
        return QList<Algebra::TripleCSP>();
      }
      break;
    }
    }
    if(previousASTBlankNode)
    {
      triples.append(new Algebra::Triple(new Algebra::BlankNode(previousASTBlankNode->blankNode()),
                                         new Algebra::Term(knowCore::Uris::rdf::rest),
                                         new Algebra::BlankNode(currentASTBlankNode->blankNode())));
    }
    previousASTBlankNode = currentASTBlankNode;
    currentNode = knowRDF::BlankNode();
  }
  if(previousASTBlankNode)
  {
    triples.append(new Algebra::Triple(new Algebra::BlankNode(previousASTBlankNode->blankNode()),
                                       new Algebra::Term(knowCore::Uris::rdf::rest),
                                       new Algebra::Term(knowCore::Uris::rdf::nil)));
  }
  getNextToken();
  return triples;
}

QList<Algebra::TripleCSP> Parser::Private::parseVerbObjectTriples(Algebra::NodeCSP _subject,
                                                                  bool _allow_blank_node)
{
  QList<Algebra::TripleCSP> triples;
  Algebra::NodeCSP verb = parseVerb();
  if(verb)
  {
    triples.append(parseObjectTriples(_subject, verb, _allow_blank_node));
    if(currentToken.type == Token::SEMI)
    {
      getNextToken();
      if(isVerbStart(currentToken))
      {
        triples.append(parseVerbObjectTriples(_subject, _allow_blank_node));
      }
    }
    return triples;
  }
  else
  {
    return QList<Algebra::TripleCSP>();
  }
}

QList<Algebra::TripleCSP> Parser::Private::parseObjectTriples(Algebra::NodeCSP _subject,
                                                              Algebra::NodeCSP _verb,
                                                              bool _allow_blank_node)
{
  QList<Algebra::TripleCSP> triples;
  switch(currentToken.type)
  {
  case Token::STARTBOXBRACKET:
  {
    if(not _allow_blank_node)
    {
      reportUnexpected(currentToken);
      return triples;
    }
    Algebra::BlankNodeCSP blankNode = new Algebra::BlankNode(knowRDF::BlankNode());
    triples.append(new Algebra::Triple(_subject, _verb, blankNode));
    triples.append(parseObjectBlankNode(blankNode, _allow_blank_node));
    break;
  }
  case Token::STARTBRACKET:
  {
    triples.append(parseCollection(_subject, _verb, _allow_blank_node));
    break;
  }
  default:
  {
    Algebra::NodeCSP c = parseObject(_allow_blank_node);
    if(c)
    {
      Algebra::TripleCSP t(new Algebra::Triple(_subject, _verb, c));
      clog_assert(t->ref == 1);
      triples.append(t);
      clog_assert(t->ref == 2);
    }
    else
    {
      return QList<Algebra::TripleCSP>();
    }
  }
  }
  if(currentToken.type == Token::COMA)
  {
    getNextToken();
    triples.append(parseObjectTriples(_subject, _verb, _allow_blank_node));
  }
  return triples;
}

bool Parser::Private::isSubjectStart(const Token& _token)
{
  switch(_token.type)
  {
  case Token::VARIABLE:
  case Token::BINDING:
  case Token::NAME:
  case Token::CURIE_CONSTANT:
  case Token::URI_CONSTANT:
  case Token::UNDERSCORECOLON:
  case Token::A:
  case Token::STARTBRACKET:
  case Token::STARTBOXBRACKET:
    return true;
  default:
    return false;
  }
}

bool Parser::Private::isVerbStart(const Token& _token)
{
  switch(_token.type)
  {
  case Token::VARIABLE:
  case Token::BINDING:
  case Token::NAME:
  case Token::CURIE_CONSTANT:
  case Token::URI_CONSTANT:
  case Token::UNDERSCORECOLON:
  case Token::A:
    return true;
  default:
    return false;
  }
}

bool Parser::Private::isObjectStart(const Token& _token)
{
  switch(_token.type)
  {
  case Token::VARIABLE:
  case Token::BINDING:
  case Token::NAME:
  case Token::CURIE_CONSTANT:
  case Token::URI_CONSTANT:
  case Token::UNDERSCORECOLON:
  case Token::A:
  case Token::STARTBRACKET:
  case Token::PLUS:
  case Token::INTEGER_CONSTANT:
  case Token::MINUS:
  case Token::STRING_CONSTANT:
  case Token::FLOAT_CONSTANT:
  case Token::TRUE:
  case Token::FALSE:
    return true;
  default:
    return false;
  }
}

Algebra::NodeCSP Parser::Private::parseSubject(bool _allow_blank_node)
{
  switch(currentToken.type)
  {
  case Token::VARIABLE:
  {
    QString name = currentToken.string;
    getNextToken();
    return new Algebra::VariableReference(name);
  }
  case Token::BINDING:
  case Token::NAME:
  case Token::CURIE_CONSTANT:
  case Token::URI_CONSTANT:
    return new Algebra::Term(parseUri());
  case Token::UNDERSCORECOLON:
    return parseBlank(_allow_blank_node);
  case Token::A:
  {
    getNextToken();
    return new Algebra::Term(urlManager.base().resolved("a"));
  }
  case Token::STARTBRACKET:
  {
    QList<Algebra::NodeCSP> list;
    getNextToken();
    while(currentToken.type != Token::END_OF_FILE and currentToken.type != Token::ENDBRACKET)
    {
      Algebra::NodeCSP subk = parseSubject(_allow_blank_node);
      if(subk)
      {
        list.append(subk);
      }
      else
      {
        return nullptr;
      }
    }
    isOfType(currentToken, Token::ENDBRACKET);
    getNextToken();
    return new Algebra::List(knowCore::listCast<Algebra::NodeCSP>(list));
  }
  default:
    reportUnexpected(currentToken);
    return nullptr;
  }
}

Algebra::NodeCSP Parser::Private::parseVerb()
{
  switch(currentToken.type)
  {
  case Token::VARIABLE:
  {
    QString name = currentToken.string;
    getNextToken();
    return new Algebra::VariableReference(name);
  }
  case Token::BINDING:
  case Token::NAME:
  case Token::CURIE_CONSTANT:
  case Token::URI_CONSTANT:
    return new Algebra::Term(parseUri());
  case Token::A:
  {
    getNextToken();
    return new Algebra::Term(knowCore::Uris::rdf::a);
  }
  default:
    reportUnexpected(currentToken);
    return nullptr;
  }
}

Algebra::NodeCSP Parser::Private::parseObject(bool _allow_blank_node)
{
  switch(currentToken.type)
  {
  case Token::VARIABLE:
  {
    QString name = currentToken.string;
    getNextToken();
    return new Algebra::VariableReference(name);
  }
  case Token::BINDING:
  {
    Token bindingToken = currentToken;
    knowCore::Value binding = getBinding(currentToken);
    getNextToken();
    if(knowCore::ValueIs<knowCore::Uri> uri = binding)
    {
      return new Algebra::Term(uri);
    }
    else
    {
      return new Algebra::Value(knowRDF::Literal::fromValue(binding));
    }
  }
  case Token::NAME:
  case Token::CURIE_CONSTANT:
  case Token::URI_CONSTANT:
    return new Algebra::Term(parseUri());
  case Token::UNDERSCORECOLON:
    return parseBlank(_allow_blank_node);
  case Token::A:
  {
    getNextToken();
    return new Algebra::Term(urlManager.base().resolved("a"));
  }
  case Token::TRUE:
    getNextToken();
    return new Algebra::Value(
      knowRDF::Literal::fromValue(knowCore::Uris::xsd::boolean, true).expect_success());
  case Token::FALSE:
    getNextToken();
    return new Algebra::Value(
      knowRDF::Literal::fromValue(knowCore::Uris::xsd::boolean, false).expect_success());
  case Token::PLUS:
    getNextToken();
    Q_FALLTHROUGH();
  case Token::INTEGER_CONSTANT:
  {
    QString val = currentToken.string;
    getNextToken();
    return new Algebra::Value(
      knowRDF::Literal::fromValue(knowCore::Uris::xsd::integer,
                                  knowCore::BigNumber::fromString(val).expect_success().toVariant())
        .expect_success());
  }
  case Token::MINUS:
  {
    getNextToken();
    QString val = "-" + currentToken.string;
    getNextToken();
    return new Algebra::Value(
      knowRDF::Literal::fromValue(knowCore::Uris::xsd::integer,
                                  knowCore::BigNumber::fromString(val).expect_success().toVariant())
        .expect_success());
  }
  case Token::STRING_CONSTANT:
  {
    return parseStringConstant();
  }
  case Token::FLOAT_CONSTANT:
  {
    QString value = currentToken.string;
    getNextToken();
    knowCore::BigNumber bn = knowCore::BigNumber::fromString(value).expect_success();
    return new Algebra::Value(
      knowRDF::Literal::fromValue(knowCore::Uris::xsd::decimal, bn).expect_success());
  }
  default:
  {
    reportUnexpected(currentToken);
    getNextToken();
    return nullptr;
  }
  }
}

Algebra::BlankNodeCSP Parser::Private::parseBlank(bool _allow_blank_node)
{
  if(not _allow_blank_node)
  {
    reportUnexpected(currentToken);
    return nullptr;
  }
  isOfType(currentToken, Token::UNDERSCORECOLON);
  getNextToken();
  QString label;

  switch(currentToken.type)
  {
  case Token::NAME:
    label = currentToken.string;
    getNextToken();
    break;
  case Token::A:
    label = "a";
    getNextToken();
    break;
  case Token::INTEGER_CONSTANT:
  case Token::FLOAT_CONSTANT:
    label = currentToken.string;
    getNextToken();
    if(currentToken.type == Token::NAME)
    {
      label += currentToken.string;
      getNextToken();
    }
    break;
  default:
    reportUnexpected(currentToken);
    return nullptr;
  }

  if(blankNodes.contains(label))
  {
    return new Algebra::BlankNode(blankNodes.value(label));
  }
  else
  {
    knowRDF::BlankNode bn(label);
    blankNodes.insert(label, bn);
    return new Algebra::BlankNode(bn);
  }
}

bool Parser::Private::isUri(const Token& _token)
{
  return _token.type == Token::CURIE_CONSTANT or _token.type == Token::NAME
         or _token.type == Token::URI_CONSTANT or _token.type == Token::BINDING;
}

knowCore::Uri Parser::Private::parseUri()
{
  knowCore::Uri r;
  switch(currentToken.type)
  {
  case Token::CURIE_CONSTANT:
  {
    if(currentToken.curie.canResolve(&urlManager))
    {
      r = currentToken.curie.resolve(&urlManager);
    }
    else
    {
      reportError(currentToken,
                  clog_qt::qformat("Unknown curie prefix '{}'", currentToken.curie.prefix()));
    }
    getNextToken();
    break;
  }
  case Token::NAME:
  {
    r = urlManager.base().resolved(currentToken.string);
    getNextToken();
    break;
  }
  case Token::URI_CONSTANT:
  {
    r = knowCore::Uri(urlManager.base(), currentToken.string);
    getNextToken();
    break;
  }
  case Token::BINDING:
  {
    knowCore::Value val = getBinding(currentToken);
    auto const [success, val_uri, message] = val.value<knowCore::Uri>();
    if(success)
    {
      r = val_uri.value();
    }
    else
    {
      reportError(currentToken, clog_qt::qformat("Binding '{}' is not convertible to uri: {}",
                                                 currentToken.string, message.value()));
    }
    getNextToken();
    break;
  }
  default:
  {
    reportUnexpected(currentToken);
    getNextToken();
  }
  }
  return r;
}

Algebra::Alternative<Algebra::VariableReference, Algebra::Term>
  Parser::Private::parseVariableOrTerm()
{
  switch(currentToken.type)
  {
  case Token::VARIABLE:
  {
    QString varname = currentToken.string;
    DisableLexingUri dlu(lexer);
    getNextToken();
    return new Algebra::VariableReference(varname);
  }
  default:
    if(isUri(currentToken))
    {
      return new Algebra::Term(parseUri());
    }
    reportUnexpected(currentToken);
    getNextToken();
    return Algebra::Alternative<Algebra::VariableReference, Algebra::Term>();
  }
}

const Token& Parser::Private::getNextToken() { return (currentToken = lexer->nextToken()); }

Parser::Parser(Lexer* _lexer, const knowCore::ValueHash& _bindings, const knowCore::Uri& _base)
    : d(new Private)
{
  d->lexer = _lexer;
  d->bindings = _bindings;
  d->urlManager.setBase(_base);
}

Parser::~Parser() { delete d; }

QList<Algebra::NodeCSP> Parser::parse()
{
  d->lexer->setCurieLexingEnabled(false);
  d->getNextToken();
  if(d->currentToken.type == Token::BASE)
  {
    if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
    {
      d->urlManager.setBase(d->currentToken.string);
      d->getNextToken();
    }
    else
    {
      return QList<Algebra::NodeCSP>();
    }
  }
  d->lexer->setPrefixLexingEnabled(true);
  while(d->currentToken.type == Token::PREFIX)
  {
    switch(d->getNextToken().type)
    {
    case Token::A:
    case Token::NAME:
    {
      QString ns = (d->currentToken.type == Token::NAME) ? d->currentToken.string : "a";
      d->isOfType(d->getNextToken(), Token::COLON);
      if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
      {
        d->urlManager.addPrefix(ns, d->currentToken.string);
      }
      else
      {
        return QList<Algebra::NodeCSP>();
      }
      break;
    }
    case Token::COLON:
    {
      if(d->isOfType(d->getNextToken(), Token::URI_CONSTANT))
      {
        d->urlManager.addPrefix(QString(), d->currentToken.string);
      }
      else
      {
        return QList<Algebra::NodeCSP>();
      }
      break;
    }
    default:
    {
      d->reportUnexpected(d->currentToken);
      return QList<Algebra::NodeCSP>();
    }
    }
    d->getNextToken();
  }
  d->lexer->setPrefixLexingEnabled(false);
  d->lexer->setCurieLexingEnabled(true);
  QList<Algebra::NodeCSP> queries;

  while(true)
  {
    if(d->currentToken.type == Token::Type::NAME
       and d->currentToken.string.toUpper()
             == "BEGIN") // Workaround since BEGIN is only available in PL/SPARQL lexing´ mode (END
                         // <- just to avoid IDE's to create comment block...)
    {
      d->currentToken.type = Token::Type::BEGIN;
      d->currentToken.string.clear();
    }
    Algebra::NodeCSP query = d->parseQuery();
    if(query)
    {
      queries.append(query);
    }
    else
    {
      break;
    }
    if(d->currentToken.type == Token::END_OF_FILE)
    {
      break;
    }
    if(d->isOfType(d->currentToken, Token::SEMI))
    {
      d->getNextToken();
    }
    else
    {
      break;
    }
    if(d->currentToken.type == Token::END_OF_FILE)
    {
      break;
    }
  }
  d->isOfType(d->currentToken, Token::END_OF_FILE);
  if(d->messages.hasErrors())
  {
    queries.clear();
  }

  return queries;
}

const knowCore::Messages& Parser::messages() const { return d->messages; }
